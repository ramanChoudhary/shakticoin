//
//  OnBoardUITestCase.swift
//  ShaktiCoinAppUITests
//
//  Created by Pravin Gawale on 31/05/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import XCTest
@testable import ShaktiCoinApp

class OnBoardUITestCase: XCTestCase {

    var app: XCUIApplication!

    override class func setUp() {
        
    }
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        app = XCUIApplication()
        app.launchArguments.append("uitesting")
        continueAfterFailure = false
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testGoingThroughOnboarding() {
        app.launch()
        
        app.swipeLeft()
        app.swipeLeft()
        
        app.buttons["LetsGo"].tap()
        
        app.textFields["EmailAddress"].tap()
        app.textFields["EmailAddress"].typeText("ggiloyan89@gmail.com")
    
        
        sleep(2)
        
        let passwordTextField = app.secureTextFields["pwdField"]
        //passwordTextField.tap()
        
        UIPasteboard.general.string = "Garnik1!"
        passwordTextField.doubleTap()
        app.menuItems["Paste"].tap()
        app.menuItems.element(boundBy: 0).tap()


        sleep(2)
        
        app.buttons["Login"].tap()
        sleep(5)

        //waitForExpectations(timeout: 5, handler: nil)

    }


    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
