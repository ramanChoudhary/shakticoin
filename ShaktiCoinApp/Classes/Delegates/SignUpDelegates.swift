//
//  SignUpDelegates.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 1/25/19.
//  Copyright © 2019 Garnik Giloyan. All rights reserved.
//

import Foundation

protocol SignUpDelegates : class {
    func countryCodePickedUp(value: String)
}
