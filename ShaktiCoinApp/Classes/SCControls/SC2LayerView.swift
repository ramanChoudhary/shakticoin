//
//  SC2LayerView.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/1/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit


class SC2LayerView: UIView {

    // MARK: - @IBInspectable
    @IBInspectable var transparency: CGFloat = 0.70 {
        didSet {
            update()
        }
    }
    
    @IBInspectable var color: UIColor = UIColor.black {
        didSet {
            update()
        }
    }
    
    // MARK: - Methods
    
    private func update() {
        alphaView.alpha = transparency
        alphaView.backgroundColor = color
    }
    
    let alphaView = UIView()
    
    // MARK: - Initializations

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        alphaView.backgroundColor = .black
        alphaView.alpha = 0.70
        addSubview(alphaView)
        
        _ = alphaView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    override func layoutSubviews() {
        alphaView.layer.cornerRadius = 3
    }

}
