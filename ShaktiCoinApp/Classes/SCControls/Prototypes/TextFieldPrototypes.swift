//
//  TextFieldPrototypes.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 10/27/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class TextFieldPrototypes {
    
    static var RegularTextField : SCTextField {
        let textField = SCTextField()
        textField.font = UIFont.lato(fontSize: 14)
        textField.textColor = .white
        return textField
    }
}
