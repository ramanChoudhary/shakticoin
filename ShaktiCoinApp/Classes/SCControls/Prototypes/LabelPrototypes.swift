//
//  LabelPrototypes.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/1/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class LabelPrototypes {
    
    static var Caption : UILabel {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 12, type: .Bold)
        return label
    }
    
    static var Caption24 : UILabel {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 24, type: .Bold) //TODO Medium
        return label
    }

    static var Caption18 : UILabel {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 18, type: .Bold) //TODO Medium
        return label
    }
    
    static var CaptionLight16 : UILabel {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 16, type: .Light)
        return label
    }
    
    static func CaptionMixed() -> UILabel {
        let label = UILabel()
        return label
    }
}
