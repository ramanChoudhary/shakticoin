//
//  SCTextView.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/3/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class SCAttributedTextView: UITextView {

    // MARK: - Initializations
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        
        backgroundColor = .clear
        isEditable = false
        textAlignment = .left
        showsVerticalScrollIndicator = false
        textContainerInset = UIEdgeInsets.zero
        textContainer?.lineFragmentPadding = 0
    }
    
    // MARK: - Declarations
    private var _attrContent = NSMutableAttributedString()
    
    // MARK: - Methods
    func setup(textInfo: String, textColor: UIColor, font: UIFont, lineSpacing: CGFloat = 1.4) {
        
        let attributedText = NSMutableAttributedString(string: textInfo, attributes: [
            NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: textColor])
        
        let textRange = NSRange(location: 0, length: attributedText.length)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 1.4
        attributedText.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range: textRange)
        
        _attrContent = attributedText
    }

    
    func setText(textContent : String, textColor: UIColor, font: UIFont) {
        _attrContent.append(NSAttributedString(string: textContent, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: textColor]))
    }
 
    func setLink(key: String, linkedText: String) {
        let text = _attrContent.string
        let startIndex = text.index(of: linkedText)
        let range = NSRange(location: startIndex!.encodedOffset, length: linkedText.count)
        self._attrContent.addAttributes([NSAttributedString.Key.link : key], range: range)
    }
    
    func setLinkStyles(linkColor: UIColor, underlineColor: UIColor) {
        let linkAttributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.foregroundColor: linkColor,
            NSAttributedString.Key.underlineColor: underlineColor,
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
        self.linkTextAttributes = linkAttributes
    }
    
    private func centerText() {
        textAlignment = .center
        let fittingSize = CGSize(width: bounds.width, height: CGFloat.greatestFiniteMagnitude)
        let size = sizeThatFits(fittingSize)
        let topOffset = (bounds.size.height - size.height * zoomScale) / 2
        let positiveTopOffset = max(1, topOffset)
        contentOffset.y = -positiveTopOffset
    }

    func finilize() {
        attributedText = _attrContent
    }
    
    func reset() {
        self.text = nil;
        self.font = nil;
        self.textColor = nil;
    }
    
    func finilizeWithCenterlize() {
        attributedText = _attrContent
        centerText()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var canBecomeFirstResponder: Bool {
        return false
    }
}
