//
//  SCXMobileField.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/29/19.
//  Copyright © 2019 Garnik Giloyan. All rights reserved.
//

import UIKit
import NKVPhonePicker

class SCXMobileField: NKVPhonePickerTextField {

    // MARK: - @IBInspectable
    @IBInspectable var CornerRadius: CGFloat = 3 {
        didSet {
            layer.cornerRadius = CornerRadius
        }
    }
    
    // MARK: - Declarations
    
    var parentPanel = SCTextFieldPanel()
    var floatingLabelValue: CGFloat = 0
    var topLabelInfo: String = ""
    
    lazy var topLabel : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 8)
        return label
    }()
    
    lazy var toolTip : SCToolTip = {
        let toolTip = SCToolTip()
        return toolTip
    }()
    
    // MARK: - Methods
    
    func setToolTip(scTT: SCToolTip) {
        toolTip = scTT
    }
    
    func removeToolTip() {
        toolTip.removeFromSuperview()
    }

    func setRegularity() {
        font = UIFont.lato(fontSize: 14)
        textColor = .white
    }
    
    func isEmpty () -> Bool {
        let count = self.text?.count ?? 0
        return (count > 0) ? false : true
    }
    
    /*override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 22, dy: 5)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 22, dy: 5)
    }*/
    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        /*let leftPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: frame.height))
        leftView = leftPaddingView
        leftViewMode = .always*/
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}
