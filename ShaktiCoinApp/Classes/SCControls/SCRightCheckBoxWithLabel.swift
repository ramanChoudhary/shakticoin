//
//  SCRightCheckBoxWithLabel.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 8/7/19.
//  Copyright © 2019 Garnik Giloyan. All rights reserved.
//

import UIKit

class SCRightCheckBoxWithLabel: UIView {

    // MARK: - Declarations
    
    lazy var caption: SCAttributedTextView = {
        let label = SCAttributedTextView()
        return label
    }()
    
    lazy var checkBox: SCCheckBox = {
        let checkBox = SCCheckBox()
        checkBox.style = .tick
        checkBox.borderStyle = .square
        checkBox.isUserInteractionEnabled = true
        return checkBox
    }()
    
    // MARK: - Methods
    private func setupViews() {

        addSubview(checkBox)
        
        _ = checkBox.anchor(nil, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 16, heightConstant: 16)
        _ = checkBox.centralizeY(centerYAnchor)
        
        addSubview(caption)
       
        _ = caption.anchor(nil, left: checkBox.rightAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 16)
        _ = caption.centralizeY(centerYAnchor)
    }
    
    func setCaptionHeightConstant(height: CGFloat) {
        
        for c in caption.constraints {
            caption.removeConstraint(c)
        }
        _ = caption.anchor(nil, left: checkBox.rightAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: height)
        _ = caption.centralizeY(centerYAnchor)
    }
    
    // MARK: - Initializations
    override init(frame: CGRect) {
        super.init(frame: frame)
        isUserInteractionEnabled = true
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
