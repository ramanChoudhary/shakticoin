//
//  SMButton.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 10/26/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class SMButton: UIButton {
    
    // Declarations
    var isActive: Bool = false
    var seqNo: Int!
        
    // MARK: - Initializations
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
