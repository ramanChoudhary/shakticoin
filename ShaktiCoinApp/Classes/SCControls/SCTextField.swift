//
//  SCTextEdit.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 10/23/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

@IBDesignable
class SCTextField : UITextField, UITextFieldDelegate {
    
    // MARK: - @IBInspectable
    @IBInspectable var RightIcon: UIImage? {
        didSet {
            update()
        }
    }
    
    @IBInspectable var CornerRadius: CGFloat = 3 {
        didSet {
            layer.cornerRadius = CornerRadius
        }
    }
    
    // MARK: - Declarations
    
    var allowEdit : Bool = true
    
    lazy var RightViewButton : UIButton = {
        let button = UIButton()
        button.tintColor = .white
        return button
    }()
    
    var topLabelInfo: String = ""
    
    lazy var topLabel : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 8)
        return label
    }()
    
    lazy var toolTip : SCToolTip = {
        let toolTip = SCToolTip()
        return toolTip
    }()
    
    // MARK: - Methods
    
    func setToolTip(scTT: SCToolTip) {
        toolTip = scTT
    }

    func removeToolTip() {
        toolTip.removeFromSuperview()
    }
    
    private func update() {
        if RightIcon != nil {
            
            rightViewMode = .always
            
            RightViewButton = UIButton(frame: CGRect(x: 0, y: 0, width: 16, height: 16))
            RightViewButton.setImage(RightIcon, for: .normal)

            let _view = UIView(frame: CGRect(x:0, y:0, width: 30, height:20))
            _view.addSubview(RightViewButton)
            //_view.backgroundColor = .red
            //RightViewButton.backgroundColor = .blue
            
            //_ = RightViewButton.leftAnchor.constraint(equalTo: _view.leftAnchor)
            //_ = RightViewButton.centralizeY(_view.centerYAnchor)
            
            rightView?.isHidden = false
            rightView = _view
        } else {
            rightView = nil
        }
    }
    
    func focus() {
 
        if topLabelInfo.isEmpty {
            topLabel.text = self.attributedPlaceholder?.string
        } else {
            topLabel.text = topLabelInfo
        }
        
        addSubview(topLabel)
        _ = topLabel.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 2, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    func unFocus() {
        topLabel.removeFromSuperview()
        if !isEmpty() {
            self.bounds = bounds.insetBy(dx: 15, dy: 5)
        }
    }
    
    func setCustomTopLabel(value: String) {
        topLabelInfo = value
    }
    
    func setRegularity() {
        font = UIFont.lato(fontSize: 14)
        textColor = .white
    }
    
    func isEmpty () -> Bool {
        let count = self.text?.count ?? 0
        return (count > 0) ? false : true
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 22, dy: 5)
    }
    
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 22, dy: 5)
    }

    override var canBecomeFirstResponder: Bool {
        return allowEdit
    }
    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        layer.cornerRadius = 3
        layer.borderWidth = 1
        layer.borderColor = ColorStyle.gold.cgColor
        
        /*let leftPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: frame.height))
        leftView = leftPaddingView
        leftViewMode = .always*/
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
