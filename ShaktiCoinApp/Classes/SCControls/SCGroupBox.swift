//
//  SCGroupBox.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 10/29/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class SCGroupBox: UIView {
    
    lazy var TextLabel : UILabel = {
        let label = UILabel()
        label.textColor = UIColor.mainColor()
        label.font = UIFont.lato(fontSize: 11, type: .Bold) // TODO Semibold
        return label
    }()

    let CloseButton : UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "Dropdown"), for: .normal)
        button.tintColor = .white
        return button
    }()
    
    let ContainerView : UIView = {
        let view = UIView()
        //view.backgroundColor = .red
        return view
    }()
    
    func setup(label: String) {
        
        if frame.width == 0.0 {return}
        
        addSubview(TextLabel)
        TextLabel.text = label
        
        let leftView = UIView()
        addSubview(leftView)
        
        _ = leftView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 1, heightConstant: 0)
        
        leftView.backgroundColor = UIColor.mainColor()
        
        let rightView = UIView()
        addSubview(rightView)
        
        _ = rightView.anchor(topAnchor, left: nil, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 1, heightConstant: 0)
        rightView.backgroundColor = UIColor.mainColor()
        
        let bottomView = UIView()
        addSubview(bottomView)
        
        _ = bottomView.anchor(nil, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 1)
        bottomView.backgroundColor = UIColor.mainColor()
        
        let topLeftView = UIView()
        addSubview(topLeftView)
        
        _ = topLeftView.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 30, heightConstant: 1)
        
        topLeftView.backgroundColor = UIColor.mainColor()
        
        let topRightView = UIView()
        addSubview(topRightView)
        
        _ = topRightView.anchor(topAnchor, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: frame.width - 30 - 150, heightConstant: 1)
        
        topRightView.backgroundColor = UIColor.mainColor()
        
        _ = TextLabel.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: -7.0, leftConstant: 40, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        addSubview(CloseButton)
        
        _ = CloseButton.anchor(topAnchor, left: nil, bottom: nil, right: rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0)
        
        addSubview(ContainerView)
        
        _ = ContainerView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 10, leftConstant: 10, bottomConstant: 10, rightConstant: 50, widthConstant: 0, heightConstant: 0)
    }
    
    override func draw(_ rect: CGRect) { }

}
