//
//  SCTextFieldPanel.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 8/14/19.
//  Copyright © 2019 Garnik Giloyan. All rights reserved.
//

import UIKit

class SCTextFieldPanel: UIView {
    
    lazy var TextLabel : UILabel = {
        let label = UILabel()
        label.textColor = UIColor.mainColor()
        label.font = UIFont.lato(fontSize: 11, type: .Bold) // TODO Semibold
        label.isHidden = true
        return label
    }()
    
    let ContainerView : UIView = {
        let view = UIView()
        //view.backgroundColor = .red
        return view
    }()
    
    let topRightView = UIView()
    
    func setup(label: String, widthValue: CGFloat = 0.0) {
        
        if frame.width == 0.0 {return}
        
        addSubview(TextLabel)
        TextLabel.text = label
        
        let leftView = UIView()
        addSubview(leftView)
        
        _ = leftView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 1, heightConstant: 0)
        
        leftView.backgroundColor = UIColor.mainColor()
        
        let rightView = UIView()
        addSubview(rightView)
        
        _ = rightView.anchor(topAnchor, left: nil, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 1, heightConstant: 0)
        rightView.backgroundColor = UIColor.mainColor()
        
        let bottomView = UIView()
        addSubview(bottomView)
        
        _ = bottomView.anchor(nil, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 1)
        bottomView.backgroundColor = UIColor.mainColor()
        
        let topLeftView = UIView()
        addSubview(topLeftView)
        
        _ = topLeftView.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 30, heightConstant: 1)
        
        topLeftView.backgroundColor = UIColor.mainColor()
        
        addSubview(topRightView)
        
        _ = topRightView.anchor(topAnchor, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: frame.width - widthValue, heightConstant: 1)
        
        topRightView.backgroundColor = UIColor.mainColor()
        
        _ = TextLabel.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: -7.0, leftConstant: 40, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        addSubview(ContainerView)
        
        _ = ContainerView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 7, leftConstant: 5, bottomConstant: 5, rightConstant: 5, widthConstant: 0, heightConstant: 0)
    }
    
    func setFloatingLabel(label: String, widthValue: CGFloat) {
        TextLabel.text = label
        for c in topRightView.constraints {
            topRightView.removeConstraint(c)
        }
        _ = topRightView.anchor(topAnchor, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: frame.width - widthValue, heightConstant: 1)
    }
    
    func setTopLabel(widthValue: CGFloat, hideText: Bool = false) {
        for c in topRightView.constraints {
            topRightView.removeConstraint(c)
        }
        _ = topRightView.anchor(topAnchor, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: frame.width - widthValue, heightConstant: 1)
        TextLabel.isHidden = hideText
    }
    
    override func draw(_ rect: CGRect) { }
    
}
