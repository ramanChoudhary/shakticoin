//
//  SCCheckBoxWithLabel.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 8/7/19.
//  Copyright © 2019 Garnik Giloyan. All rights reserved.
//

import UIKit

class SCCheckBoxWithLabel: UIView {

    // MARK: - Declarations
    
    lazy var caption: UILabel = {
        let label = UILabel()
        label.text = "Caption"
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 12)
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.numberOfLines = 2
        return label
    }()
    
    lazy var checkBox: SCCheckBox = {
        let checkBox = SCCheckBox()
        checkBox.style = .tick
        checkBox.borderStyle = .square
        checkBox.isUserInteractionEnabled = true
        return checkBox
    }()
    
    // MARK: - Methods
    private func setupViews() {
 
        addSubview(caption)
        _ = caption.anchor(nil, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0)
        _ = caption.centralizeY(centerYAnchor)
        
        addSubview(checkBox)
        _ = checkBox.anchor(nil, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 16, heightConstant: 16)
        _ = checkBox.centralizeY(centerYAnchor)
    }
    
    // MARK: - Initializations
    override init(frame: CGRect) {
        super.init(frame: frame)
        isUserInteractionEnabled = true
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
