//
//  SCButton.swift
//  ShatkiCoinApp
//
//  Created by Garnik Giloyan on 10/23/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

@IBDesignable
class SCButton: UIButton {
    
    @IBInspectable var CornerRadius: CGFloat = 0
    
    override func draw(_ rect: CGRect) {
        layer.cornerRadius = CornerRadius
        applyGradient(colours: [
            UIColor(red:0.68, green:0.62, blue:0.48, alpha:1),
            UIColor(red:0.82, green:0.76, blue:0.61, alpha:1),
            UIColor(red:0.55, green:0.48, blue:0.33, alpha:1)
        ], locations: [0, 0, 1])
    }

    private func applyGradient(colours: [UIColor], locations: [NSNumber]?) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        gradient.startPoint = CGPoint(x: 1, y: 0)
        gradient.endPoint = CGPoint(x: 0, y: 1)
        gradient.cornerRadius = CornerRadius
        self.layer.insertSublayer(gradient, at: 0)
    }
}
