//
//  SCLineTitle.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 10/29/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

@IBDesignable
class SCLineTitle: UIView {

    // MARK: - @IBInspectable
    @IBInspectable var Title: String = "Title" {
        didSet {
            update()
        }
    }

    // MARK: - Methods
    func update(lineSize : CGFloat = 22) {

        print(frame.width)
        if frame.width == 0.0 {return}
        
        let leftView = UIView()
        leftView.backgroundColor = UIColor.mainColor()

        addSubview(leftView)
        
        _ = leftView.anchor(nil, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: (frame.width*lineSize)/100, heightConstant: 1)
        _ = leftView.centralizeY(centerYAnchor)

        let rightView = UIView()
        rightView.backgroundColor = UIColor.mainColor()
        
        addSubview(rightView)
        
        _ = rightView.anchor(nil, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: (frame.width*lineSize)/100, heightConstant: 1)
        _ = rightView.centralizeY(centerYAnchor)
        
        let title = UILabel()
        title.text = Title
        title.textColor = UIColor.mainColor()
        title.font = UIFont.lato(fontSize: 14, type: .Bold) //TODO Semibold
        
        addSubview(title)
        
        _ = title.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        _ = title.centralizeY(centerYAnchor)
        _ = title.centralizeX(centerXAnchor)
    }
     
    func updateWithSpecificWidth(width: CGFloat, lineSize : CGFloat = 22) {

        //375
        let leftView = UIView()
        leftView.backgroundColor = UIColor.mainColor()
        
        addSubview(leftView)
        
        _ = leftView.anchor(nil, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: (width*lineSize)/100, heightConstant: 1)
        _ = leftView.centralizeY(centerYAnchor)
        
        let rightView = UIView()
        rightView.backgroundColor = UIColor.mainColor()
        
        addSubview(rightView)
        
        _ = rightView.anchor(nil, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: (width*lineSize)/100, heightConstant: 1)
        _ = rightView.centralizeY(centerYAnchor)
        
        let title = UILabel()
        title.text = Title
        title.textColor = UIColor.mainColor()
        title.font = UIFont.lato(fontSize: 14, type: .Bold) //TODO Semibold
        
        addSubview(title)
        
        _ = title.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        _ = title.centralizeY(centerYAnchor)
        _ = title.centralizeX(centerXAnchor)
        
    }
    
    // MARK: - Initializations
    override func draw(_ rect: CGRect) { }
    

}
