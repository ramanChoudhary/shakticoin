//
//  SCToolTip.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/21/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit
import NKVPhonePicker

class SCToolTip: UIView {

    // MARK: - Declarations
    
    var title = "" {
        didSet {
            setNeedsDisplay()
        }
    }
    
    lazy var titleLbl : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 10, type: .Bold)
        return label
    }()
    
    // MARK: - Methods
    
    func setRelativeControl(uiControl: SCTextField) {
        backgroundColor = .clear
        _ = anchor(nil, left: uiControl.leftAnchor, bottom: uiControl.topAnchor, right: uiControl.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: -12, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        alpha = 0.70
    }
    
    func setRelativeXControl(uiControl: SCXTextField) {
        backgroundColor = .clear
        _ = anchor(nil, left: uiControl.leftAnchor, bottom: uiControl.topAnchor, right: uiControl.rightAnchor, topConstant: 0, leftConstant: 125, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 32)
        alpha = 0.70
    }
    
    func setRelativeXControlMobile(uiControl: NKVPhonePickerTextField) {
        backgroundColor = .clear
        _ = anchor(nil, left: uiControl.leftAnchor, bottom: uiControl.topAnchor, right: uiControl.rightAnchor, topConstant: 0, leftConstant: 125, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 32)
        alpha = 0.70
    }
    
    func setMessage(message: String) {
        titleLbl.text = message
    }
    
    // MARK: - UIView

    override func draw(_ rect: CGRect) {
        
        let bezierPath = UIBezierPath()
        //Draw main body
        bezierPath.move(to: CGPoint(x: rect.minX, y: rect.minY))
        bezierPath.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
        bezierPath.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY - 10.0))
        bezierPath.addLine(to: CGPoint(x: rect.minX, y: rect.maxY - 10.0))
        bezierPath.addLine(to: CGPoint(x: rect.minX, y: rect.minY))
        //Draw the tail
        bezierPath.move(to: CGPoint(x: rect.maxX - 25.0, y: rect.maxY - 10.0))
        bezierPath.addLine(to: CGPoint(x: rect.maxX - 10.0, y: rect.maxY))
        bezierPath.addLine(to: CGPoint(x: rect.maxX - 10.0, y: rect.maxY - 10.0))
        UIColor.red.setFill()
        bezierPath.fill()
        bezierPath.close()
        
        addSubview(titleLbl)
        
        _ = titleLbl.centralizeX(centerXAnchor)
        _ = titleLbl.centralizeY(centerYAnchor, constant: -5)
        
        backgroundColor = .clear

    }
    
    override func layoutSubviews() {
        layer.cornerRadius = 4
        clipsToBounds = true
    }
}
