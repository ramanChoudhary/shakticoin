//
//  PoEFeatItemView.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 3/6/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import UIKit

class PoEFeatItemView: UIView {

    // MARK: - Declarations

    lazy var itemView : UIView = {
        let view = UIView()
        view.backgroundColor = .black
        return view
    }()
    
    lazy var itemTitle : UILabel = {
        let label = UILabel()
        label.font = .lato(fontSize: 16, type: .Bold)
        label.textColor = .white
        return label
    }()
    
    lazy var addIcon: UIImageView = {
        let iv = UIImageView(image: UIImage(named: "plus"))
        iv.tintColor = .mainColor()
        return iv
    }()
    
    // MARK: - Methods
    
    private func setupViews() {
        
        addSubview(itemView)
        
        _ = itemView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 25, leftConstant: 25, bottomConstant: 25, rightConstant: 25, widthConstant: 0, heightConstant: 0)
        
        itemView.addSubview(itemTitle)
        
        _ = itemTitle.anchor(nil, left: itemView.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = itemTitle.centralizeY(itemView.centerYAnchor)
        
        itemView.addSubview(addIcon)
        _ = addIcon.anchor(nil, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 32, heightConstant: 32)
        _ = addIcon.centralizeY(itemView.centerYAnchor)

    }

    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupViews()
    }

    override func layoutSubviews() {
        self.layer.cornerRadius = 3
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
