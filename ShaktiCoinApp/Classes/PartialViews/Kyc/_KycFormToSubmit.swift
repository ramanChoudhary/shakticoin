//
//  _KycFormToSubmit.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/6/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class SCLeftRadiusedButton: UIButton {
    
    // MARK: - Methods
    
    func setup() {
        let r = self.bounds.size.height / 2
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .bottomLeft], cornerRadii: CGSize(width: r, height: r))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }

    // MARK: - UIView
    override func layoutSubviews() { setup() } // "layoutSubviews" is best
}

class SCRightRadiusedButton: UIButton {
    
    // MARK: - Methods
    
    func setup() {
        let r = self.bounds.size.height / 2
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topRight, .bottomRight], cornerRadii: CGSize(width: r, height: r))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    // MARK: - UIView
    override func layoutSubviews() { setup() } // "layoutSubviews" is best
}

class _KycFormToSubmit: UIView {

    // MARK: - Declarations
    
    lazy var filesReadyLbl : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()
    
    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("Cancel & Discard", for: .normal)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    lazy var nextButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Send Files", for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    lazy var addAnotherFileButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Add another Document", for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    lazy var tempFileView : SC2LayerView = {
        let view = SC2LayerView()
        view.alphaView.alpha = 0.40
        return view
    }()

    lazy var addAnotherFile : UILabel = {
        let view = UILabel()
        view.text = "Would you like to add another file?"
        view.textColor = .white
        view.font = UIFont.lato(fontSize: 14, type: .Bold)
        return view
    }()
    
    // MARK: - Methods
    
    func setupViews() {
        
        addSubview(filesReadyLbl)
        filesReadyLbl.Title = "File is ready to be sent"
        
        _ = filesReadyLbl.anchor(topAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = filesReadyLbl.centralizeX(centerXAnchor)
        
        
        addSubview(tempFileView)
        
        // TEMP
        _ = tempFileView.anchor(filesReadyLbl.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        
        let tempFileName = UILabel()
        tempFileName.text = "camera_picture.jpg"
        tempFileName.textColor = .white
        tempFileName.font = UIFont.lato(fontSize: 14)
        
        tempFileView.addSubview(tempFileName)
        
        _ = tempFileName.centralizeX(tempFileView.centerXAnchor)
        _ = tempFileName.centralizeY(tempFileView.centerYAnchor)
        
        addSubview(cancelButton)
        addSubview(nextButton)
        addSubview(addAnotherFileButton)
        addSubview(addAnotherFile)
        
        _ = cancelButton.anchor(nil, left: nil, bottom: bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 20, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = cancelButton.centralizeX(centerXAnchor)
        
        _ = nextButton.anchor(nil, left: nil, bottom: cancelButton.topAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 25, rightConstant: 0, widthConstant: 330, heightConstant: 36)
        _ = nextButton.centralizeX(centerXAnchor)
        
        _ = addAnotherFileButton.anchor(nil, left: nil, bottom: nextButton.topAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 25, rightConstant: 0, widthConstant: 330, heightConstant: 36)
        _ = addAnotherFileButton.centralizeX(centerXAnchor)
        
        _ = addAnotherFile.anchor(nil, left: nil, bottom: addAnotherFileButton.topAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 25, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = addAnotherFile.centralizeX(centerXAnchor)
    }
    
    func finalizeMe() {
        filesReadyLbl.update()
    }
    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
