//
//  _AdditionalInfoFirst.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/6/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class _AdditionalInfoFirst: UIView {

    // MARK: - Declarations
    
    lazy var subTitle : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()
    
    lazy var emailAddressPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()

    lazy var emailAddress: SCXTextField = {
        let textField = SCXTextField()
        textField.keyboardType = UIKeyboardType.emailAddress
        textField.floatingLabelValue = 120
        textField.attributedPlaceholder = Util.NSMAttrString(text: "EmailAddress".localized, font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var phoneNumberPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()

    lazy var phoneNumber: SCXTextField = {
        let textField = SCXTextField()
        textField.keyboardType = UIKeyboardType.phonePad
        textField.floatingLabelValue = 115
        textField.attributedPlaceholder = Util.NSMAttrString(text: "PhoneNumber".localized, font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var occupationPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var occupation: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 100
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Occupation".localized, font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var highestLevelOfEducationDropDown: SCGroupBox = {
        let groupView = SCGroupBox()
        groupView.backgroundColor = .black
        groupView.alpha = 0.85
        groupView.isHidden = true
        groupView.CloseButton.tag = 1
        return groupView
    }()
    
    lazy var highestLevelOfEducation : SCTextField = {
        let textField = TextFieldPrototypes.RegularTextField
        textField.allowEdit = false
        textField.RightIcon = UIImage(named: "Dropdown")
        textField.RightViewButton.tag = 1
        textField.attributedPlaceholder = Util.NSMAttrString(text: "LevelOfEducation".localized, font: UIFont.lato(fontSize: 14))
        return textField
    }()
    
    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("Cancel".localized, for: .normal)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    lazy var nextButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Next".localized, for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    let emailAddressTT = SCToolTip()
    let phoneNumberTT = SCToolTip()
    let occupationTT = SCToolTip()
    let educationTT = SCToolTip()
    
    // MARK: - Methods
    
    func setupToolTips() {
        emailAddress.setToolTip(scTT: emailAddressTT)
        phoneNumber.setToolTip(scTT: phoneNumberTT)
        occupation.setToolTip(scTT: occupationTT)
        highestLevelOfEducation.setToolTip(scTT: educationTT)
    }
    
    // MARK: - Methods
    
    func setupViews() {
        
        addSubview(subTitle)
        subTitle.Title = "CanYouTellUsMore".localized
        
        _ = subTitle.anchor(topAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = subTitle.centralizeX(centerXAnchor)
        
        addSubview(emailAddressPanel)
        addSubview(phoneNumberPanel)
        addSubview(occupationPanel)
        addSubview(highestLevelOfEducation)
        addSubview(cancelButton)
        addSubview(nextButton)
        
        _ = emailAddressPanel.anchor(subTitle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = emailAddressPanel.centralizeX(centerXAnchor)
        
        emailAddressPanel.ContainerView.addSubview(emailAddress)
        emailAddress.parentPanel = emailAddressPanel
        Util.fullyAnchor(_parnetControl: emailAddressPanel.ContainerView, _childControl: emailAddress)
        
        _ = phoneNumberPanel.anchor(emailAddressPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = phoneNumberPanel.centralizeX(centerXAnchor)
        
        phoneNumberPanel.ContainerView.addSubview(phoneNumber)
        phoneNumber.parentPanel = phoneNumberPanel
        Util.fullyAnchor(_parnetControl: phoneNumberPanel.ContainerView, _childControl: phoneNumber)
        
        _ = occupationPanel.anchor(phoneNumberPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15
            , leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = occupationPanel.centralizeX(centerXAnchor)
        
        occupationPanel.ContainerView.addSubview(occupation)
        occupation.parentPanel = occupationPanel
        Util.fullyAnchor(_parnetControl: occupationPanel.ContainerView, _childControl: occupation)
        
        _ = highestLevelOfEducation.anchor(occupationPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = highestLevelOfEducation.centralizeX(centerXAnchor)
        
        _ = nextButton.anchor(highestLevelOfEducation.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 48, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 36)
        _ = nextButton.centralizeX(centerXAnchor)
        
        _ = cancelButton.anchor(nextButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = cancelButton.centralizeX(centerXAnchor)

        addSubview(highestLevelOfEducationDropDown)
        _ = highestLevelOfEducationDropDown.anchor(highestLevelOfEducation.topAnchor, left: highestLevelOfEducation.leftAnchor, bottom: cancelButton.bottomAnchor, right: highestLevelOfEducation.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    func finalizeMe() {
        subTitle.update()
        emailAddressPanel.setup(label: "EmailAddress".localized)
        phoneNumberPanel.setup(label: "PhoneNumber".localized)
        occupationPanel.setup(label: "Occupation".localized)
    }
    
    func validate() -> Bool {
        return true
        var isValid: Bool = true
        
        if emailAddress.isEmpty() {
            addSubview(emailAddressTT)
            emailAddressTT.setRelativeXControl(uiControl: emailAddress)
            emailAddressTT.setMessage(message: "EmailEmpty".localized)
            isValid = false
        } else {
            if !Validator.shared().isValidEmail(email: emailAddress.text!) {
                addSubview(emailAddressTT)
                emailAddressTT.setRelativeXControl(uiControl: emailAddress)
                emailAddressTT.setMessage(message: "EmailInvalid".localized)
                isValid = false
            }
        }
        
        if phoneNumber.isEmpty() {
            
            addSubview(phoneNumberTT)
            phoneNumberTT.setRelativeXControl(uiControl: phoneNumber)
            phoneNumberTT.setMessage(message: "EmptyPhoneNumber".localized)
            isValid = false
            
        } else {
            
            /*if !Validator.shared().isValidPhoneNumber(phone: phoneNumber.text!) {
                addSubview(phoneNumberTT)
                phoneNumberTT.setRelativeXControl(uiControl: phoneNumber)
                phoneNumberTT.setMessage(message: "Please enter a valid phone number")
                isValid = false
            }*/
        }
  
        if occupation.isEmpty() {
            addSubview(occupationTT)
            occupationTT.setRelativeXControl(uiControl: occupation)
            occupationTT.setMessage(message: "EmptyOccupation".localized)
            isValid = false
        }
        
        if highestLevelOfEducation.isEmpty() {
            addSubview(educationTT)
            educationTT.setRelativeControl(uiControl: highestLevelOfEducation)
            educationTT.setMessage(message: "EmptyEducation".localized)
            isValid = false
        }
        
        return isValid
    }
    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupToolTips()
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
