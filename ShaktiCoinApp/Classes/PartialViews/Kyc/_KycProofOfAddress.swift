//
//  _KycProofOfAddress.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 5/11/19.
//  Copyright © 2019 Garnik Giloyan. All rights reserved.
//

import UIKit

class _KycProofOfAddress: UIView {

    // MARK: - Declarations

    lazy var selectedIndex : Int = 1
    
    lazy var proofOfAddressLabel : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()
    
    let imageList = ["Gas", "Property", "Government"]

    lazy var validationTextView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        
        textView.textAlignment = .center
        textView.setup(textInfo: "To complete your KYC Validation, provide a photo", textColor: UIColor(red:0.85, green:0.85, blue:0.85, alpha:1), font: UIFont.lato(fontSize: 14))
        textView.setText(textContent: "\n  or a scan of a document dated within the past 6", textColor: UIColor(red:0.85, green:0.85, blue:0.85, alpha:1), font: UIFont.lato(fontSize: 14))
        textView.setText(textContent: "\n     months that shows your name and address.", textColor: UIColor(red:0.85, green:0.85, blue:0.85, alpha:1), font: UIFont.lato(fontSize: 14))
        textView.finilize()
        return textView
    }()

    // MARK: - Options View Declarations
    
    lazy var optionsView: UIView = {
        let view = UIView()
        return view
    }()
    
    lazy var stairView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.spacing = 5
        return sv
    }()
    
    lazy var leftCircleViewContainer : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.30)
        //view.alpha = 0.30
        return view
    }()
    
    lazy var leftCircleImageView : UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "Gas")
        view.tintColor = UIColor.mainColor()
        return view
    }()
    
    lazy var leftCircleTextView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.setup(textInfo: "Utility ", textColor: .white, font: UIFont.lato(fontSize: 14))
        textView.setText(textContent: "\nBills", textColor: .white, font: UIFont.lato(fontSize: 14))
        textView.finilizeWithCenterlize()
        return textView
    }()
    
    lazy var rightViewContainer : UIView = {
        let view = UIView()
        return view
    }()

    lazy var rightTextView: SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.setup(textInfo: "Submit an utility bill", textColor: .white, font: UIFont.lato(fontSize: 12))
        textView.setText(textContent: "\nwith your name", textColor: .white, font: UIFont.lato(fontSize: 12))
        textView.setText(textContent: "\nand address.", textColor: .white, font: UIFont.lato(fontSize: 12))
        textView.finilizeWithCenterlize()
        return textView
    }()
    
    lazy var chooseLabel : UILabel = {
        let label = UILabel()
        label.text = "Choose from any three categories."
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 13)
        return label
    }()

    lazy var nextButton : SCButton = {
        let button = SCButton()
        button.setTitle("Next", for: .normal)
        button.CornerRadius = 18
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        //TODO Action
        return button
    }()
    
    lazy var cancelButton : UIButton = {
        let button = UIButton()
        button.setTitle("Cancel", for: .normal)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        button.addTarget(self, action: #selector(onCancel(_:)), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Methods
    
    func setupViews() {
        
        addSubview(proofOfAddressLabel)
        proofOfAddressLabel.Title = "Proof of address"
        
        _ = proofOfAddressLabel.anchor(topAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = proofOfAddressLabel.centralizeX(centerXAnchor)
        
        addSubview(validationTextView)
        
        _ = validationTextView.anchor(proofOfAddressLabel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 12, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 315, heightConstant: 60)
        _ = validationTextView.centralizeX(centerXAnchor)
        
        addSubview(optionsView)
        _ = optionsView.anchor(validationTextView.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 280)

        setupOptionsView()
        
        addSubview(chooseLabel)
        
        var chooseLabelTopConstant : CGFloat = 5
        if UIDevice.current.screenType == .iPhone_XR ||
            UIDevice.current.screenType == .iPhone_XSMax ||
            UIDevice.current.screenType == .iPhones_6Plus_6sPlus_7Plus_8Plus {
            chooseLabelTopConstant = 35
        }
        
        _ = chooseLabel.anchor(optionsView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: chooseLabelTopConstant, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = chooseLabel.centralizeX(centerXAnchor)
        
        addSubview(nextButton)
        
        var nextButtonTopConstant : CGFloat = 20
        if UIDevice.current.screenType == .iPhone_XR ||
            UIDevice.current.screenType == .iPhone_XSMax ||
            UIDevice.current.screenType == .iPhones_6Plus_6sPlus_7Plus_8Plus {
            nextButtonTopConstant = 40
        }
        
        _ = nextButton.anchor(chooseLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: nextButtonTopConstant, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 36)
        
        addSubview(cancelButton)
        
        var cancelButtonTopConstant : CGFloat = 20
        if UIDevice.current.screenType == .iPhone_XR ||
            UIDevice.current.screenType == .iPhone_XSMax ||
            UIDevice.current.screenType == .iPhones_6Plus_6sPlus_7Plus_8Plus {
            cancelButtonTopConstant = 35
        }
        
        _ = cancelButton.anchor(nextButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: cancelButtonTopConstant, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 120, heightConstant: 19)
        _ = cancelButton.centralizeX(centerXAnchor, constant: 0)
        
        //TEMP
        
        /*let checkBox = SCCheckBox()
        addSubview(checkBox)
        checkBox.style = .tick
        checkBox.borderStyle = .square
        
        _ = checkBox.anchor(cancelButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 20, heightConstant: 20)
        _ = checkBox.centralizeX(centerXAnchor)*/
        
    }
    
    func setupOptionsView () {
        
        optionsView.addSubview(stairView)
        optionsView.addSubview(leftCircleViewContainer)
        optionsView.addSubview(rightViewContainer)
        
        _ = leftCircleViewContainer.anchor(nil, left: optionsView.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 132, heightConstant: 132)
        _ = leftCircleViewContainer.centralizeY(optionsView.centerYAnchor)
        
        leftCircleViewContainer.addSubview(leftCircleImageView)
        leftCircleViewContainer.addSubview(leftCircleTextView)
        
        _ = leftCircleImageView.widthAnchor.constraint(equalToConstant: 36).isActive = true
        _ = leftCircleImageView.heightAnchor.constraint(equalToConstant: 36).isActive = true
        
        _ = leftCircleImageView.centralizeX(leftCircleViewContainer.centerXAnchor)
        _ = leftCircleImageView.centralizeY(leftCircleViewContainer.centerYAnchor, constant: -15)
        
        _ = leftCircleTextView.anchor(leftCircleImageView.bottomAnchor, left: leftCircleViewContainer.leftAnchor, bottom: leftCircleViewContainer.bottomAnchor, right: leftCircleViewContainer.rightAnchor, topConstant: 4, leftConstant: 15, bottomConstant: 10, rightConstant: 15, widthConstant: 0, heightConstant: 0)
 
        _ = rightViewContainer.anchor(optionsView.topAnchor, left: nil, bottom: optionsView.bottomAnchor, right: optionsView.rightAnchor, topConstant: 35, leftConstant: 0, bottomConstant: 35, rightConstant: 15, widthConstant: 140, heightConstant: 0)
        
        rightViewContainer.addSubview(rightTextView)
        
        _ = rightTextView.anchor(nil, left: rightViewContainer.leftAnchor, bottom: nil, right: rightViewContainer.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 80)
        
        _ = rightTextView.centralizeX(rightViewContainer.centerXAnchor)
        _ = rightTextView.centralizeY(rightViewContainer.centerYAnchor)
 
        _ = stairView.widthAnchor.constraint(equalToConstant: 60).isActive = true
        _ = stairView.centralizeY(optionsView.centerYAnchor)
        _ = stairView.centralizeX(optionsView.centerXAnchor)
        
        for i in 1...3 {
            
            let optionButton = SMButton()
            let selectedImage: String = (i == 1) ? "Active" : "Inactive"
            optionButton.seqNo = i
            optionButton.isActive = (i == 1) ? true : false
            optionButton.setImage(UIImage(named: selectedImage), for: .normal)
            optionButton.addTarget(self, action: #selector(onOptionButtonClick(_:)), for: .touchUpInside)
            stairView.addArrangedSubview(optionButton)
        }
    }
    
    func finalizeMe() {
        proofOfAddressLabel.update()
    }
    
    func changeTheOption(seqNo: Int) {
        
        selectedIndex = seqNo
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            
            self.leftCircleViewContainer.frame.origin.y -= 100
            self.leftCircleViewContainer.alpha = 0.0
            
            self.rightViewContainer.frame.origin.y += 100
            self.rightViewContainer.alpha = 0.0
            
        }, completion: { (finished: Bool) in
            
            self.leftCircleImageView.isHidden = true
            self.leftCircleImageView.image = UIImage(named: self.imageList[seqNo-1])
            
            self.resetValues(seqNo: seqNo)
        })
    }
    
    private func resetValues(seqNo : Int) {
        
        self.leftCircleViewContainer.frame.origin.y += 130
        self.rightViewContainer.frame.origin.y -= 130
        
        self.leftCircleImageView.isHidden = false
        setActiveText(seqNo: seqNo)
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            
            self.leftCircleViewContainer.frame.origin.y -= 30
            self.leftCircleViewContainer.alpha = 1
            
            self.rightViewContainer.frame.origin.y += 30
            self.rightViewContainer.alpha = 1
            
        }, completion: nil)
    }
    
    private func setActiveText(seqNo : Int) {
        switch seqNo {
        case 1:
            
            leftCircleTextView.setup(textInfo: "Utility ", textColor: .white, font: UIFont.lato(fontSize: 14))
            leftCircleTextView.setText(textContent: "\nBills", textColor: .white, font: UIFont.lato(fontSize: 14))
            
            rightTextView.setup(textInfo: "Submit an utility bill", textColor: .white, font: UIFont.lato(fontSize: 12))
            rightTextView.setText(textContent: "\nwith your name", textColor: .white, font: UIFont.lato(fontSize: 12))
            rightTextView.setText(textContent: "\nand address.", textColor: .white, font: UIFont.lato(fontSize: 12))
            
        case 2:
            
            leftCircleTextView.setup(textInfo: "Property ", textColor: .white, font: UIFont.lato(fontSize: 14))
            leftCircleTextView.setText(textContent: "\n& Finance", textColor: .white, font: UIFont.lato(fontSize: 14))
            
            rightTextView.setup(textInfo: "Submit a property", textColor: .white, font: UIFont.lato(fontSize: 12))
            rightTextView.setText(textContent: "\nor finance related", textColor: .white, font: UIFont.lato(fontSize: 12))
            rightTextView.setText(textContent: "\ndocument with your", textColor: .white, font: UIFont.lato(fontSize: 12))
            rightTextView.setText(textContent: "\nname and address.", textColor: .white, font: UIFont.lato(fontSize: 12))
        
        case 3:
        
            leftCircleTextView.setup(textInfo: "School, Gov. ", textColor: .white, font: UIFont.lato(fontSize: 14))
            leftCircleTextView.setText(textContent: "\n& Other Doc.", textColor: .white, font: UIFont.lato(fontSize: 14))

            rightTextView.setup(textInfo: "Submit a school or", textColor: .white, font: UIFont.lato(fontSize: 12))
            rightTextView.setText(textContent: "\nGov or any other", textColor: .white, font: UIFont.lato(fontSize: 12))
            rightTextView.setText(textContent: "\nbody, that has issued", textColor: .white, font: UIFont.lato(fontSize: 12))
            rightTextView.setText(textContent: "\nand ID with your", textColor: .white, font: UIFont.lato(fontSize: 12))
            rightTextView.setText(textContent: "\nname and address.", textColor: .white, font: UIFont.lato(fontSize: 12))
        
        default:
            break
        }
        
        leftCircleTextView.finilizeWithCenterlize()
        rightTextView.finilizeWithCenterlize()
    }
    
    // MARK: - Events
    
    @objc private func onOptionButtonClick(_ button: SMButton) {
        
        if button.isActive { return }
        
        button.isActive = true
        button.setImage(UIImage(named: "Active"), for: .normal)
        for case let eachButton as SMButton in stairView.subviews {
            
            if eachButton.seqNo != button.seqNo {
                eachButton.isActive = false
                eachButton.setImage(UIImage(named: "Inactive"), for: .normal)
            }
        }
        
        changeTheOption(seqNo: button.seqNo)
    }

    @objc private func onCancel(_ sender: UIButton) {
        //TODO:
    }
    
    // MARK: - UIView
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        leftCircleViewContainer.layer.cornerRadius = leftCircleViewContainer.frame.width/2
        leftCircleViewContainer.clipsToBounds = true
    }

}
