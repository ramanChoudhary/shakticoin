//
//  _DocumentChooser.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/6/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class _DocumentChooser: UIView {

    // MARK: - Declarations
    
    lazy var firstRow : UIStackView = {
        var view = UIStackView()
        return view
    }()
    
    lazy var secondRow : UIStackView = {
        var view = UIStackView()
        return view
    }()
    
    lazy var docItems : [DocumentItem] = {
        let phone = DocumentItem(caption: "Phone", icon: "PhoneGold")
        let gas = DocumentItem(caption: "Gas", icon: "Gas")
        let power = DocumentItem(caption: "Power", icon: "Power")
        let water = DocumentItem(caption: "Water", icon: "Water")
        let propertyTax = DocumentItem(caption: "Property Tax", icon: "Property")
        let municipal = DocumentItem(caption: "Municipal", icon: "Government")
        let postOffice = DocumentItem(caption: "Post Office", icon: "Post")
        let bank = DocumentItem(caption: "Bank", icon: "Bank")
        return [phone,gas,power,water,propertyTax,municipal,postOffice,bank]
    }()
    
    // MARK: - Methods
    
    func setupViews() {
        
        addSubview(firstRow)
        addSubview(secondRow)
        
        _ = firstRow.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 70)
        
        _ = secondRow.anchor(firstRow.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 5, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 70)
        
        firstRow.axis = .horizontal
        firstRow.distribution = .fillEqually
        firstRow.spacing = 5

        secondRow.axis = .horizontal
        secondRow.distribution = .fillEqually
        secondRow.spacing = 5
        
        for i in 0...3 {
            
            let contView = SC2LayerView()
            contView.transparency = 0.70
            
            let imageView = UIImageView()
            imageView.image = UIImage(named: docItems[i].icon)
            
            firstRow.addArrangedSubview(contView)
            
            contView.addSubview(imageView)
            _ = imageView.centralizeX(contView.centerXAnchor)
            _ = imageView.centralizeY(contView.centerYAnchor, constant: -5)
            imageView.tintColor = UIColor.white
            
            let label: UILabel = UILabel()
            label.text = docItems[i].caption
            label.font = UIFont.lato(fontSize: 11)
            label.textColor = .white
            
            contView.addSubview(label)
            
            _ = label.anchor(imageView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
            _ = label.centralizeX(contView.centerXAnchor)
        }
        
        for i in 0...3 {
            
            let secContView = SC2LayerView()
            secContView.transparency = 0.70

            let imageView = UIImageView()
            imageView.image = UIImage(named: docItems[i+4].icon)
            
            secondRow.addArrangedSubview(secContView)
            
            secContView.addSubview(imageView)
            _ = imageView.centralizeX(secContView.centerXAnchor)
            _ = imageView.centralizeY(secContView.centerYAnchor, constant: -5)
            
            imageView.tintColor = UIColor.white
            
            let label: UILabel = UILabel()
            label.text = docItems[i+4].caption
            label.font = UIFont.lato(fontSize: 11)
            label.textColor = .white
            
            secContView.addSubview(label)
            
            _ = label.anchor(imageView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
            _ = label.centralizeX(secContView.centerXAnchor)
        }

    }
    
    func finalizeMe() {}
    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
