//
//  _KycVerified.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/7/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class _KycVerified: UIView {

    // MARK: - Declarations
    
    lazy var verifiedLabel : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()
    
    lazy var messageViewContainer : SC2LayerView = {
        let view = SC2LayerView()
        return view
    }()
    
    lazy var imageProcess : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "Verified")
        imageView.tintColor = UIColor.mainColor()
        return imageView
    }()
    
    lazy var congratsTextView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        let textContent = "Congratulations! Your account has been verified. \nYou've passed all the compliance validations and \n           you're on your way to a great start! \n\n    Your wallet is now active and ready to be used."
        textView.setup(textInfo: textContent, textColor: UIColor.white, font: UIFont.lato(fontSize: 14))
        textView.finilize()
        return textView
    }()
    
    lazy var nextButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Start using my Shakti wallet", for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    // MARK: - Methods
    
    func setupViews() {
        
        addSubview(verifiedLabel)
        verifiedLabel.Title = "Your KYC is being processed"
        
        _ = verifiedLabel.anchor(topAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = verifiedLabel.centralizeX(centerXAnchor)
        
        addSubview(messageViewContainer)
        
        _ = messageViewContainer.anchor(verifiedLabel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 335, heightConstant: 219)
        _ = messageViewContainer.centralizeX(centerXAnchor)
        
        messageViewContainer.addSubview(imageProcess)
        
        _ = imageProcess.centralizeX(messageViewContainer.centerXAnchor)
        _ = imageProcess.centralizeY(messageViewContainer.centerYAnchor, constant: -40)
        
        messageViewContainer.addSubview(congratsTextView)
        
        _ = congratsTextView.anchor(imageProcess.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 315, heightConstant: 90)
        _ = congratsTextView.centralizeX(messageViewContainer.centerXAnchor)
        
        addSubview(nextButton)
        
        _ = nextButton.anchor(nil, left: nil, bottom: bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 20, rightConstant: 0, widthConstant: 330, heightConstant: 36)
        _ = nextButton.centralizeX(centerXAnchor)
        
    }
    
    func finalizeMe() {
        verifiedLabel.update()
    }
    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
