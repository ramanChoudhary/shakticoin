//
//  _KycFilesRejected.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 9/15/19.
//  Copyright © 2019 Garnik Giloyan. All rights reserved.
//

import UIKit

class _KycFilesRejected: UIView {

    // MARK: - Declarations
    lazy var topLabelLine : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()

    lazy var containerView: UIScrollView = {
        let view = UIScrollView()
        view.contentSize.height = 1250
        return view
    }()

    lazy var somethingWrongLbl : UILabel = {
        let label = UILabel()
        label.text = "Something went wrong."
        label.font = UIFont.lato(fontSize: 24, type: .Light)
        label.textColor = .white
        return label
    }()

    lazy var reasonDesc : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.setup(textInfo: "The information you've provided didn't match the government issued ID of your country of residence.", textColor: .white, font: .lato(fontSize: 14))
        textView.setText(textContent: "\n\nPlease review the information provided and apply again. Make sure that all fields are complete and your proof of address is valid and readable. Remember your bounty is still yours to keep and you can continue to work on unlocking it.", textColor: .white, font: .lato(fontSize: 14))
        textView.finilize()
        return textView
    }()
    
    lazy var deliveryImageView : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "Fast Delivery")
        imageView.tintColor = UIColor.mainColor()
        return imageView
    }()
    
    lazy var fastTrackLabel : UILabel = {
        let label = UILabel()
        label.text = "Fast-track your KYC verification!"
        label.textColor = UIColor.white
        label.font = UIFont.lato(fontSize: 18, type: .Bold) //TODO Medium
        return label
    }()

    lazy var kycValidationInfo : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.setup(textInfo: "The KYC Validation can take weeks to be processed.", textColor: .white, font: .lato(fontSize: 14))
        textView.setText(textContent: "\n\nPay a small fee to fast-track your KYC verification. By opting to pay for the processing fee, you agree to pay a non-refundable cost for expediting your KYC verification. This is a cost-recovered service paid to a third-party to expedite, verify, and confirm your ID.", textColor: .white, font: .lato(fontSize: 14))
        textView.finilize()
        return textView
    }()

    lazy var costTextView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.setup(textInfo: "$7.95 USD", textColor: .white, font: .lato(fontSize: 14))
        textView.finilizeWithCenterlize()
        return textView
    }()
    
    lazy var payButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Pay to fast-track your KYC", for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    lazy var orAlternativeLine : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()

    lazy var miningComputerImageView : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "Mining Computer")
        imageView.tintColor = UIColor.mainColor()
        return imageView
    }()
    
    lazy var skipKycTextView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.setup(textInfo: "Skip KYC verification with a Miner License!", textColor: .white, font: .lato(fontSize: 16, type: Lato.Bold)) // TODO Medium
        textView.setText(textContent: "\n\nYou can choose to buy a Mining License and skip the Regular or Fast-Track KYC Verification Process. ", textColor: .white, font: .lato(fontSize: 14))
        textView.finilizeWithCenterlize()
        return textView
    }()
    
    lazy var payforMining : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Pay to fast-track your KYC", for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    lazy var thenFinallyLine : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()

    lazy var reviewTextView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.setup(textInfo: "Review my information and re-send for verification.", textColor: .white, font: .lato(fontSize: 14))
        textView.setLink(key: "reviewLink", linkedText: "Review my information and re-send for verification.")
        textView.setLinkStyles(linkColor: UIColor.white, underlineColor: UIColor.white)
        textView.setText(textContent: "\n\n\nYour wallet is inactive until the Shakti Network completes the KYC, AML and regulatory compliance. Be assured that once you register and claim your bounty, it is yours to keep as long as you comply with your bounty conditions. Shakti will work around the clock to make sure your wallet is activated.", textColor: .white, font: .lato(fontSize: 10))
        textView.finilizeWithCenterlize()
        return textView
    }()
    
    // MARK: - Methods
    func setupViews() {

        addSubview(containerView)
        
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        containerView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        containerView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true

        containerView.addSubview(topLabelLine)
        topLabelLine.Title = "Sorry, your validation was rejected"
        
        _ = topLabelLine.anchor(containerView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = topLabelLine.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(somethingWrongLbl)
        _ = somethingWrongLbl.anchor(topLabelLine.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 35, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = somethingWrongLbl.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(reasonDesc)
        _ = reasonDesc.anchor(somethingWrongLbl.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 140)
        
        containerView.addSubview(deliveryImageView)
        _ = deliveryImageView.anchor(reasonDesc.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 40, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 64, heightConstant: 43)
        _ = deliveryImageView.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(fastTrackLabel)
        _ = fastTrackLabel.anchor(deliveryImageView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 35, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = fastTrackLabel.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(kycValidationInfo)
        _ = kycValidationInfo.anchor(fastTrackLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 25, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 140)
        
        containerView.addSubview(costTextView)
        _ = costTextView.anchor(kycValidationInfo.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 170, heightConstant: 25)
        _ = costTextView.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(payButton)
        _ = payButton.anchor(costTextView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 40, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 36)
        _ = payButton.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(orAlternativeLine)
        orAlternativeLine.Title = "Or alternatively"
        
        _ = orAlternativeLine.anchor(payButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 50, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = orAlternativeLine.centralizeX(containerView.centerXAnchor)

        containerView.addSubview(miningComputerImageView)
        _ = miningComputerImageView.anchor(orAlternativeLine.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 40, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 64, heightConstant: 64)
        _ = miningComputerImageView.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(skipKycTextView)
        _ = skipKycTextView.anchor(miningComputerImageView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 95)
        _ = skipKycTextView.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(payforMining)
        _ = payforMining.anchor(skipKycTextView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 40, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 36)
        _ = payforMining.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(thenFinallyLine)
        thenFinallyLine.Title = "And then finally"
        
        _ = thenFinallyLine.anchor(payforMining.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 50, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = thenFinallyLine.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(reviewTextView)
        _ = reviewTextView.anchor(thenFinallyLine.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 25, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 120)
        _ = reviewTextView.centralizeX(containerView.centerXAnchor)

    }
    
    func finalizeMe() {
        topLabelLine.update(lineSize: 16)
        orAlternativeLine.update()
        thenFinallyLine.update()
    }
    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
