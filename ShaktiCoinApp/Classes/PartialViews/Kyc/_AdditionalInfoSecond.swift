//
//  _AdditionalInfoSecond.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/6/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class _AdditionalInfoSecond: UIView {

    // MARK: - Declarations
    
    lazy var subTitle : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()
    
    lazy var fullNamePanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var fullName: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 115
        textField.attributedPlaceholder = Util.NSMAttrString(text: "FullName".localized, font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var phoneOrEmailPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var phoneNumberOrEmail: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 200
        textField.attributedPlaceholder = Util.NSMAttrString(text: "PhoneNumberOrEmail".localized, font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
//    lazy var relationshipPanel: SCTextFieldPanel = {
//        let panel = SCTextFieldPanel()
//        panel.backgroundColor = .clear
//        return panel
//    }()
//
//    lazy var relationship: SCXTextField = {
//        let textField = SCXTextField()
//        textField.floatingLabelValue = 105
//        textField.attributedPlaceholder = Util.NSMAttrString(text: "Relationship".localized, font: UIFont.lato(fontSize: 14))
//        textField.setRegularity()
//        return textField
//    }()
    
    lazy var relationshipDropDown: SCGroupBox = {
          let groupView = SCGroupBox()
          groupView.backgroundColor = .black
          groupView.alpha = 0.85
          groupView.isHidden = true
          groupView.CloseButton.tag = 1
          return groupView
      }()
      
      lazy var relationship : SCTextField = {
          let textField = TextFieldPrototypes.RegularTextField
          textField.allowEdit = false
          textField.RightIcon = UIImage(named: "Dropdown")
          textField.RightViewButton.tag = 1
          //textField.RightViewButton.addTarget(self, action: #selector(onDropDownIconClick(_button:)), for: .touchUpInside)
          //textField.delegate = self
          textField.attributedPlaceholder = Util.NSMAttrString(text: "Relationship".localized, font: UIFont.lato(fontSize: 14))
          return textField
      }()
    
    
    lazy var highestLevelOfEducationDropDown: SCGroupBox = {
        let groupView = SCGroupBox()
        groupView.backgroundColor = .black
        groupView.alpha = 0.85
        groupView.isHidden = true
        groupView.CloseButton.tag = 1
        return groupView
    }()
    
    lazy var highestLevelOfEducation : SCTextField = {
        let textField = TextFieldPrototypes.RegularTextField
        textField.allowEdit = false
        textField.RightIcon = UIImage(named: "Dropdown")
        textField.RightViewButton.tag = 1
        //textField.RightViewButton.addTarget(self, action: #selector(onDropDownIconClick(_button:)), for: .touchUpInside)
        //textField.delegate = self
        textField.attributedPlaceholder = Util.NSMAttrString(text: "LevelOfEducation".localized, font: UIFont.lato(fontSize: 14))
        return textField
    }()
 
    lazy var udidCheckBox : SCRightCheckBoxWithLabel = {
        let checkBox = SCRightCheckBoxWithLabel()
        checkBox.caption.setup(textInfo: "_CHB_IAuthorize".localized, textColor: .white, font: UIFont.lato(fontSize: 12))
        checkBox.caption.setText(textContent: "_CHB_Security".localized, textColor: .white, font: UIFont.lato(fontSize: 12))
        checkBox.caption.finilize()
        return checkBox
    }()
    
    lazy var shaktiNetwork : SCRightCheckBoxWithLabel = {
        let checkBox = SCRightCheckBoxWithLabel()
        checkBox.caption.setup(textInfo: "_CHB_IConsent".localized, textColor: .white, font: .lato(fontSize: 12))
        checkBox.caption.finilize()
        return checkBox
    }()
    
    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("Cancel".localized, for: .normal)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    lazy var nextButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("UpdateAddInfo".localized, for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
 
    let fullNameTT = SCToolTip()
    let phoneOrEmailTT = SCToolTip()
    let relationshipTT = SCToolTip()
    let educationTT = SCToolTip()
    
    // MARK: - Methods
 
    func setupToolTips() {
        fullName.setToolTip(scTT: fullNameTT)
        phoneNumberOrEmail.setToolTip(scTT: phoneOrEmailTT)
        relationship.setToolTip(scTT: relationshipTT)
        highestLevelOfEducation.setToolTip(scTT: educationTT)
    }
    
    func setupViews() {
        
        addSubview(subTitle)
        subTitle.Title = "YouAreNextKin".localized
        
        _ = subTitle.anchor(topAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = subTitle.centralizeX(centerXAnchor)
        
        addSubview(fullNamePanel)
        addSubview(phoneOrEmailPanel)
        addSubview(relationship)
        addSubview(highestLevelOfEducation)
        addSubview(cancelButton)
        addSubview(nextButton)
        addSubview(udidCheckBox)
        addSubview(shaktiNetwork)
        
        _ = fullNamePanel.anchor(subTitle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = fullNamePanel.centralizeX(centerXAnchor)
        
        fullNamePanel.ContainerView.addSubview(fullName)
        fullName.parentPanel = fullNamePanel
        Util.fullyAnchor(_parnetControl: fullNamePanel.ContainerView, _childControl: fullName)
        
        _ = phoneOrEmailPanel.anchor(fullNamePanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = phoneOrEmailPanel.centralizeX(centerXAnchor)
        
        phoneOrEmailPanel.ContainerView.addSubview(phoneNumberOrEmail)
        phoneNumberOrEmail.parentPanel = phoneOrEmailPanel
        Util.fullyAnchor(_parnetControl: phoneOrEmailPanel.ContainerView, _childControl: phoneNumberOrEmail)
        
//        _ = relationshipPanel.anchor(phoneOrEmailPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15
//            , leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
//        _ = relationshipPanel.centralizeX(centerXAnchor)
//
//        relationshipPanel.ContainerView.addSubview(relationship)
//        relationship.parentPanel = relationshipPanel
//        Util.fullyAnchor(_parnetControl: relationshipPanel.ContainerView, _childControl: relationship)

        _ = relationship.anchor(phoneOrEmailPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = relationship.centralizeX(centerXAnchor)

        _ = highestLevelOfEducation.anchor(relationship.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = highestLevelOfEducation.centralizeX(centerXAnchor)
        
        _ = udidCheckBox.anchor(highestLevelOfEducation.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = udidCheckBox.centralizeX(centerXAnchor)

        _ = shaktiNetwork.anchor(udidCheckBox.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = shaktiNetwork.centralizeX(centerXAnchor)
        
        udidCheckBox.setCaptionHeightConstant(height: 32)
        
        _ = nextButton.anchor(shaktiNetwork.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 48, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 36)
        _ = nextButton.centralizeX(centerXAnchor)
        
        _ = cancelButton.anchor(nextButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = cancelButton.centralizeX(centerXAnchor)
        
        addSubview(relationshipDropDown)
        addSubview(highestLevelOfEducationDropDown)
        
        _ = relationshipDropDown.anchor(relationship.topAnchor, left: relationship.leftAnchor, bottom: nil, right: relationship.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 80)

        _ = highestLevelOfEducationDropDown.anchor(highestLevelOfEducation.topAnchor, left: highestLevelOfEducation.leftAnchor, bottom: nextButton.topAnchor, right: highestLevelOfEducation.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    func finalizeMe() {
        subTitle.update()
        fullNamePanel.setup(label: "FullName".localized)
        phoneOrEmailPanel.setup(label: "PhoneNumberOrEmail".localized)
       // relationshipPanel.setup(label: "Relationship".localized)
    }
    
    func validate() -> Bool {
        return true
        var isValid: Bool = true
        
        if fullName.isEmpty() {
            addSubview(fullNameTT)
            fullNameTT.setRelativeXControl(uiControl: fullName)
            fullNameTT.setMessage(message: "EmptyFullName".localized)
            isValid = false
        }
        
        if phoneNumberOrEmail.isEmpty() {
            addSubview(phoneOrEmailTT)
            phoneOrEmailTT.setRelativeXControl(uiControl: phoneNumberOrEmail)
            phoneOrEmailTT.setMessage(message: "EmptyEmailOrMobile".localized)
            isValid = false
        }
        
        if relationship.isEmpty() {
            addSubview(relationshipTT)
            relationshipTT.setRelativeControl(uiControl: relationship)
            relationshipTT.setMessage(message: "EmptyRelashionship".localized)
            isValid = false
        }
        
        if highestLevelOfEducation.isEmpty() {
            addSubview(educationTT)
            educationTT.setRelativeControl(uiControl: highestLevelOfEducation)
            educationTT.setMessage(message: "EmptyEducation".localized)
            isValid = false
        }
        
        return isValid
    }
    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupToolTips()
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

