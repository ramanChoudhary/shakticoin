//
//  _PersonalInfoSecond.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/6/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class _PersonalInfoSecond: UIView, UITableViewDelegate, UITableViewDataSource,  UITextFieldDelegate {

    // MARK: - Declarations
    
    lazy var subTitle : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()
    
    lazy var countryOfResidence : SCTextField = {
        let textField = TextFieldPrototypes.RegularTextField
        textField.allowEdit = false
        textField.RightIcon = UIImage(named: "Dropdown")
        textField.RightViewButton.tag = 1
        textField.RightViewButton.addTarget(self, action: #selector(onDropDownIconClick(_button:)), for: .touchUpInside)
        textField.delegate = self
        textField.attributedPlaceholder = Util.NSMAttrString(text: "ResidenceCountry".localized, font: UIFont.lato(fontSize: 14))
        return textField
    }()
    
    lazy var dropDownResidency : SCGroupBox = {
        let groupView = SCGroupBox()
        groupView.backgroundColor = .black
        groupView.alpha = 0.85
        groupView.isHidden = true
        groupView.CloseButton.tag = 1
        groupView.CloseButton.addTarget(self, action: #selector(onCloseResidencyDropDownList(_button:)), for: .touchUpInside)
        return groupView
    }()
    
    lazy var countryTableView : UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        return tableView
    }()
    
    lazy var statePanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var state: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 135
        textField.attributedPlaceholder = Util.NSMAttrString(text: "SelectYourState".localized, font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var cityPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var city: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 70
        textField.attributedPlaceholder = Util.NSMAttrString(text: "City".localized, font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var addressLine1Panel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var addressLine1: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 120
        textField.attributedPlaceholder = Util.NSMAttrString(text: "AddressLine1".localized, font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()

    lazy var addressLine2Panel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var addressLine2: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 120
        textField.attributedPlaceholder = Util.NSMAttrString(text: "AddressLine2".localized, font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var zipCodePanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()

    lazy var zipCode: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 155
        textField.attributedPlaceholder = Util.NSMAttrString(text: "ZipPostal".localized, font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("Cancel".localized, for: .normal)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    lazy var nextButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Updatepersonalinformation".localized, for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    let countryCellId : String = "CountryCellId"
    var countryList : [(key: String,value: Any)] = []
    var selectedCountryForResidence: [String : Any] = [:]
    
    let countryOfResidenceTT = SCToolTip()
    let stateTT = SCToolTip()
    let cityTT = SCToolTip()
    let addressLine1TT = SCToolTip()
    let addressLine2TT = SCToolTip()
    let zipCodeTT = SCToolTip()
    
    var zipCodeInfo: String = "ZipPostal".localized

    // MARK: - Methods
    
    func setupToolTips() {
        countryOfResidence.setToolTip(scTT: countryOfResidenceTT)
        state.setToolTip(scTT: stateTT)
        city.setToolTip(scTT: cityTT)
        addressLine1.setToolTip(scTT: addressLine1TT)
        addressLine2.setToolTip(scTT: addressLine2TT)
        zipCode.setToolTip(scTT: zipCodeTT)
    }

    func setupViews() {
        
        addSubview(subTitle)
        subTitle.Title = "Wheredoyoulive".localized
        
        _ = subTitle.anchor(topAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = subTitle.centralizeX(centerXAnchor)
        
        addSubview(countryOfResidence)
        addSubview(statePanel)
        addSubview(cityPanel)
        addSubview(addressLine1Panel)
        addSubview(addressLine2Panel)
        addSubview(zipCodePanel)
        addSubview(cancelButton)
        addSubview(nextButton)
        
        _ = countryOfResidence.anchor(subTitle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = countryOfResidence.centralizeX(centerXAnchor)
        
        _ = statePanel.anchor(countryOfResidence.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = statePanel.centralizeX(centerXAnchor)
        
        statePanel.ContainerView.addSubview(state)
        state.parentPanel = statePanel
        Util.fullyAnchor(_parnetControl: statePanel.ContainerView, _childControl: state)
        
        _ = cityPanel.anchor(statePanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = cityPanel.centralizeX(centerXAnchor)
        
        cityPanel.ContainerView.addSubview(city)
        city.parentPanel = cityPanel
        Util.fullyAnchor(_parnetControl: cityPanel.ContainerView, _childControl: city)
        
        _ = addressLine1Panel.anchor(cityPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = addressLine1Panel.centralizeX(centerXAnchor)

        addressLine1Panel.ContainerView.addSubview(addressLine1)
        addressLine1.parentPanel = addressLine1Panel
        Util.fullyAnchor(_parnetControl: addressLine1Panel.ContainerView, _childControl: addressLine1)
        
        _ = addressLine2Panel.anchor(addressLine1Panel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = addressLine2Panel.centralizeX(centerXAnchor)
        
        addressLine2Panel.ContainerView.addSubview(addressLine2)
        addressLine2.parentPanel = addressLine2Panel
        Util.fullyAnchor(_parnetControl: addressLine2Panel.ContainerView, _childControl: addressLine2)
        
        _ = zipCodePanel.anchor(addressLine2Panel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = zipCodePanel.centralizeX(centerXAnchor)
        
        zipCodePanel.ContainerView.addSubview(zipCode)
        zipCode.parentPanel = zipCodePanel
        Util.fullyAnchor(_parnetControl: zipCodePanel.ContainerView, _childControl: zipCode)
        
        addSubview(dropDownResidency)
        
        _ = dropDownResidency.anchor(countryOfResidence.topAnchor, left: countryOfResidence.leftAnchor, bottom: addressLine1Panel.bottomAnchor, right: countryOfResidence.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = nextButton.anchor(zipCodePanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 48, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 36)
        _ = nextButton.centralizeX(centerXAnchor)
        
        _ = cancelButton.anchor(nextButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = cancelButton.centralizeX(centerXAnchor)
    }
    
    func finalizeMe() {
        subTitle.update()
        dropDownResidency.setup(label: "ResidenceCountry".localized)
        statePanel.setup(label: "SelectYourState".localized)
        cityPanel.setup(label: "City".localized)
        addressLine1Panel.setup(label: "AddressLine1".localized)
        addressLine2Panel.setup(label: "AddressLine2".localized)
        zipCodePanel.setup(label: "ZipPostal".localized)
    }
    
    @objc private func onCloseResidencyDropDownList(_button : UIButton) {
        dropDownResidency.isHidden = !dropDownResidency.isHidden
    }

    func registerCells() {
        countryTableView.register(CountryTableViewCell.self, forCellReuseIdentifier: countryCellId)
    }
    
    func getCountries() {
        
//        CountryService.shared().get{ (output) in
//
//            DispatchQueue.main.async {
//
//                self.countryList = output.sorted { ($0.value as! String)  < ($1.value as! String) }
//            }
//        }
    }
    
    func validate() -> Bool {
        return true
        var isValid: Bool = true
        
        if countryOfResidence.isEmpty() {
            addSubview(countryOfResidenceTT)
            countryOfResidenceTT.setRelativeControl(uiControl: countryOfResidence)
            countryOfResidenceTT.setMessage(message: "EmptyCitizenship".localized)
            isValid = false
        }
        
        if state.isEmpty() {
            addSubview(stateTT)
            stateTT.setRelativeXControl(uiControl: state)
            stateTT.setMessage(message: "EmptyState".localized)
            isValid = false
        }
        
        if city.isEmpty() {
            addSubview(cityTT)
            cityTT.setRelativeXControl(uiControl: city)
            cityTT.setMessage(message: "EmptyCity".localized)
            isValid = false
        }
        
        if addressLine1.isEmpty() {
            addSubview(addressLine1TT)
            addressLine1TT.setRelativeXControl(uiControl: addressLine1)
            addressLine1TT.setMessage(message: "EmptyAddress1".localized)
            isValid = false
        }
        
        if addressLine2.isEmpty() {
            addSubview(addressLine2TT)
            addressLine2TT.setRelativeXControl(uiControl: addressLine2)
            addressLine2TT.setMessage(message: "EmptyAddress2".localized)
            isValid = false
        }
        
        if zipCode.isEmpty() {
            addSubview(zipCodeTT)
            zipCodeTT.setRelativeXControl(uiControl: zipCode)
            zipCodeTT.setMessage(message: "EmptyZipCode".localized)
            isValid = false
        } else {
            
            selectedCountryForResidence.forEach { (dict) in
                
                let (key, _) = dict
                if !key.isEmpty {
                    
                    if !Validator.shared().isValidZipCode(country: key, zipCode: zipCode.text!) {
                        
                        addSubview(zipCodeTT)
                        zipCodeTT.setRelativeXControl(uiControl: zipCode)
                        zipCodeTT.setMessage(message: "InvalidZipCode".localized)
                        isValid = false
                    }
                }
            }
        }

        return isValid
    }
    
    // MARK: - Events
    
    @objc private func onDropDownIconClick(_button: UIButton) {
        
        dropDownResidency.isHidden = true
        
        if _button.tag == 1 {
            dropDownResidency.isHidden = !dropDownResidency.isHidden
            dropDownResidency.layer.zPosition = 1
            dropDownResidency.ContainerView.addSubview(countryTableView)
            _ = countryTableView.anchor(dropDownResidency.ContainerView.topAnchor, left: dropDownResidency.ContainerView.leftAnchor, bottom: dropDownResidency.ContainerView.bottomAnchor, right: dropDownResidency.ContainerView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
            countryTableView.reloadData()
            countryOfResidence.removeToolTip()
        } else if _button.tag == 2 {
            
        }
    }
    
    // MARK: - TableView
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == countryTableView {
            
            zipCodeInfo = "ZipPostal".localized + ": " + Validator.shared().getZipCodeFormatByCountry(country: Array(self.countryList)[indexPath.row].value as? String ?? "")
            zipCode.placeholder = zipCodeInfo

            selectedCountryForResidence.removeAll()
            selectedCountryForResidence[Array(self.countryList)[indexPath.row].key] = Array(self.countryList)[indexPath.row].value as? String
            countryOfResidence.text = Array(self.countryList)[indexPath.row].value as? String
            dropDownResidency.isHidden = true
            countryOfResidence.removeToolTip()
        } else {
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.countryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: countryCellId, for: indexPath as IndexPath) as! CountryTableViewCell
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        cell.countryName.text = Array(self.countryList)[indexPath.row].value as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        registerCells()
        setupToolTips()
        setupViews()
        getCountries()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


}
