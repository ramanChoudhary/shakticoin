//
//  _KycSelectDocumentType.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 8/4/19.
//  Copyright © 2019 Garnik Giloyan. All rights reserved.
//

import UIKit

class _KycSelectDocumentType: UIView {

    // MARK: - Declarations
    
    lazy var pageTitle : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()
    
    let imageList = ["PhoneGold", "Upload", "Fast Delivery"]
    
    lazy var validationTextView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.textAlignment = .center
        textView.setup(textInfo: "You can upload JPEG, PNG, TIFF,  GIF and PDF files", textColor: UIColor(red:0.85, green:0.85, blue:0.85, alpha:1), font: UIFont.lato(fontSize: 14))
        textView.setText(textContent: "\n  from photographed or scanned bills.", textColor: UIColor(red:0.85, green:0.85, blue:0.85, alpha:1), font: UIFont.lato(fontSize: 14))
        textView.finilizeWithCenterlize()
        return textView
    }()
    
    // MARK: - Options View Declarations
    
    lazy var optionsView: UIView = {
        let view = UIView()
        return view
    }()
    
    lazy var stairView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.spacing = 5
        return sv
    }()
    
    lazy var labelOfOptionView : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 12)
        label.text = "Select document type:"
        return label
    }()
    
    lazy var leftCircleViewContainer : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.30)
        return view
    }()
    
    lazy var leftCircleImageView : UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "PhoneGold")
        view.tintColor = UIColor.mainColor()
        return view
    }()
    
    lazy var leftCircleTextView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.setup(textInfo: "Take a ", textColor: .white, font: UIFont.lato(fontSize: 14))
        textView.setText(textContent: "\nPicture", textColor: .white, font: UIFont.lato(fontSize: 14))
        textView.finilizeWithCenterlize()
        return textView
    }()
    
    lazy var rightViewContainer : UIView = {
        let view = UIView()
        return view
    }()
     
    lazy var chooseLabel : UILabel = {
        let label = UILabel()
        label.text = "Your document must match your name & address."
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 13)
        return label
    }()
    
    lazy var nextButton : SCButton = {
        let button = SCButton()
        button.setTitle("Next", for: .normal)
        button.CornerRadius = 18
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        //TODO Action
        return button
    }()
    
    lazy var cancelButton : UIButton = {
        let button = UIButton()
        button.setTitle("Cancel", for: .normal)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        button.addTarget(self, action: #selector(onCancel(_:)), for: .touchUpInside)
        return button
    }()
    
    let firstCheckListView = UIView()
    let optionListCheckBoxView = UIView()
    
    // MARK: - Methods
    
    func setupViews() {
        
        addSubview(pageTitle)
        pageTitle.Title = "Utility Bills"
        
        _ = pageTitle.anchor(topAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = pageTitle.centralizeX(centerXAnchor)
        
        addSubview(validationTextView)
        
        _ = validationTextView.anchor(pageTitle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 12, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 325, heightConstant: 60)
        _ = validationTextView.centralizeX(centerXAnchor)
        
        addSubview(optionsView)
        _ = optionsView.anchor(validationTextView.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 280)
        
        setupOptionsView()
        
        addSubview(chooseLabel)
        
        var chooseLabelTopConstant : CGFloat = 5
        if UIDevice.current.screenType == .iPhone_XR ||
            UIDevice.current.screenType == .iPhone_XSMax ||
            UIDevice.current.screenType == .iPhones_6Plus_6sPlus_7Plus_8Plus {
            chooseLabelTopConstant = 35
        }
        
        _ = chooseLabel.anchor(optionsView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: chooseLabelTopConstant, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = chooseLabel.centralizeX(centerXAnchor)
        
        addSubview(nextButton)
        
        var nextButtonTopConstant : CGFloat = 20
        if UIDevice.current.screenType == .iPhone_XR ||
            UIDevice.current.screenType == .iPhone_XSMax ||
            UIDevice.current.screenType == .iPhones_6Plus_6sPlus_7Plus_8Plus {
            nextButtonTopConstant = 40
        }

        _ = nextButton.anchor(chooseLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: nextButtonTopConstant, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 36)
        
        addSubview(cancelButton)
        
        var cancelButtonTopConstant : CGFloat = 20
        if UIDevice.current.screenType == .iPhone_XR ||
            UIDevice.current.screenType == .iPhone_XSMax ||
            UIDevice.current.screenType == .iPhones_6Plus_6sPlus_7Plus_8Plus {
            cancelButtonTopConstant = 35
        }
        
        _ = cancelButton.anchor(nextButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: cancelButtonTopConstant, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 120, heightConstant: 19)
        _ = cancelButton.centralizeX(centerXAnchor, constant: 0)
    }
    
    func resetConstraintsToLeftImageView(width: CGFloat, height: CGFloat) {
        for c in leftCircleImageView.constraints {
            leftCircleImageView.removeConstraint(c)
        }
        _ = leftCircleImageView.widthAnchor.constraint(equalToConstant: width).isActive = true
        _ = leftCircleImageView.heightAnchor.constraint(equalToConstant: height).isActive = true
        
        _ = leftCircleImageView.centralizeX(leftCircleViewContainer.centerXAnchor)
        _ = leftCircleImageView.centralizeY(leftCircleViewContainer.centerYAnchor, constant: -15)
    }
    
    func setupOptionsView() {
        
        optionsView.addSubview(stairView)
        optionsView.addSubview(leftCircleViewContainer)
        optionsView.addSubview(rightViewContainer)
        optionsView.addSubview(labelOfOptionView)
        
        _ = labelOfOptionView.anchor(optionsView.topAnchor, left: nil, bottom: nil, right: optionsView.rightAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 0)
        
        _ = leftCircleViewContainer.anchor(nil, left: optionsView.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 132, heightConstant: 132)
        _ = leftCircleViewContainer.centralizeY(optionsView.centerYAnchor)
        
        leftCircleViewContainer.addSubview(leftCircleImageView)
        leftCircleViewContainer.addSubview(leftCircleTextView)
        
        _ = leftCircleImageView.widthAnchor.constraint(equalToConstant: 26).isActive = true
        _ = leftCircleImageView.heightAnchor.constraint(equalToConstant: 34).isActive = true
        
        _ = leftCircleImageView.centralizeX(leftCircleViewContainer.centerXAnchor)
        _ = leftCircleImageView.centralizeY(leftCircleViewContainer.centerYAnchor, constant: -15)
        
        _ = leftCircleTextView.anchor(leftCircleImageView.bottomAnchor, left: leftCircleViewContainer.leftAnchor, bottom: leftCircleViewContainer.bottomAnchor, right: leftCircleViewContainer.rightAnchor, topConstant: 4, leftConstant: 15, bottomConstant: 10, rightConstant: 15, widthConstant: 0, heightConstant: 0)
        
        _ = rightViewContainer.anchor(optionsView.topAnchor, left: nil, bottom: optionsView.bottomAnchor, right: optionsView.rightAnchor, topConstant: 35, leftConstant: 0, bottomConstant: 35, rightConstant: 0, widthConstant: 140, heightConstant: 0)
        
        //rightViewContainer.backgroundColor = .red
        
        let phoneCheckBox = SCCheckBoxWithLabel()
        phoneCheckBox.caption.text = "Phone"
 
        let gasCheckBox = SCCheckBoxWithLabel()
        gasCheckBox.caption.text = "Gas"
 
        let electCheckBox = SCCheckBoxWithLabel()
        electCheckBox.caption.text = "Electricity"
        
        let waterCheckBox = SCCheckBoxWithLabel()
        waterCheckBox.caption.text = "Water"
        
        let munCheckBox = SCCheckBoxWithLabel()
        munCheckBox.caption.text = "Municipal Tax"
        
        let insCovCheckBox = SCCheckBoxWithLabel()
        insCovCheckBox.caption.text = "Insurance Coverage"
        
        let cableCheckBox = SCCheckBoxWithLabel()
        cableCheckBox.caption.text = "Cable"
        
        let otherCheckBox = SCCheckBoxWithLabel()
        otherCheckBox.caption.text = "Other"

        let miningLicense = SCCheckBoxWithLabel()
        miningLicense.caption.text = "Purchase a Mining License"
        
        let feeToValidate = SCCheckBoxWithLabel()
        feeToValidate.caption.text = "Pay a small Fee to Validate"
        
        rightViewContainer.addSubview(firstCheckListView)
        rightViewContainer.addSubview(optionListCheckBoxView)
        
        optionListCheckBoxView.isHidden = true
        
        _ = firstCheckListView.anchor(rightViewContainer.topAnchor, left: rightViewContainer.leftAnchor, bottom: rightViewContainer.bottomAnchor, right: rightViewContainer.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

        _ = optionListCheckBoxView.anchor(rightViewContainer.topAnchor, left: rightViewContainer.leftAnchor, bottom: rightViewContainer.bottomAnchor, right: rightViewContainer.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        firstCheckListView.addSubview(phoneCheckBox)
        firstCheckListView.addSubview(gasCheckBox)
        firstCheckListView.addSubview(electCheckBox)
        firstCheckListView.addSubview(waterCheckBox)
        firstCheckListView.addSubview(munCheckBox)
        firstCheckListView.addSubview(insCovCheckBox)
        firstCheckListView.addSubview(cableCheckBox)
        firstCheckListView.addSubview(otherCheckBox)
        
        optionListCheckBoxView.addSubview(miningLicense)
        optionListCheckBoxView.addSubview(feeToValidate)
        
        _ = phoneCheckBox.anchor(firstCheckListView.topAnchor, left: nil, bottom: nil, right: firstCheckListView.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 145, heightConstant: 20)
        
        _ = gasCheckBox.anchor(phoneCheckBox.bottomAnchor, left: nil, bottom: nil, right: firstCheckListView.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 145, heightConstant: 20)
        
        _ = electCheckBox.anchor(gasCheckBox.bottomAnchor, left: nil, bottom: nil, right: firstCheckListView.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 145, heightConstant: 20)
        
        _ = waterCheckBox.anchor(electCheckBox.bottomAnchor, left: nil, bottom: nil, right: firstCheckListView.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 145, heightConstant: 20)
        
        _ = munCheckBox.anchor(waterCheckBox.bottomAnchor, left: nil, bottom: nil, right: firstCheckListView.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 145, heightConstant: 20)

        _ = insCovCheckBox.anchor(munCheckBox.bottomAnchor, left: nil, bottom: nil, right: firstCheckListView.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 145, heightConstant: 20)

        _ = cableCheckBox.anchor(insCovCheckBox.bottomAnchor, left: nil, bottom: nil, right: firstCheckListView.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 145, heightConstant: 20)
        
        _ = otherCheckBox.anchor(cableCheckBox.bottomAnchor, left: nil, bottom: nil, right: firstCheckListView.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 145, heightConstant: 20)

        _ = miningLicense.anchor(optionListCheckBoxView.topAnchor, left: nil, bottom: nil, right: optionListCheckBoxView.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 145, heightConstant: 20)
        
        _ = feeToValidate.anchor(miningLicense.bottomAnchor, left: nil, bottom: nil, right: optionListCheckBoxView.rightAnchor, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 145, heightConstant: 20)
        
        _ = stairView.widthAnchor.constraint(equalToConstant: 60).isActive = true
        _ = stairView.centralizeY(optionsView.centerYAnchor)
        _ = stairView.centralizeX(optionsView.centerXAnchor)
        
        for i in 1...3 {
            
            let optionButton = SMButton()
            let selectedImage: String = (i == 1) ? "Active" : "Inactive"
            optionButton.seqNo = i
            optionButton.isActive = (i == 1) ? true : false
            optionButton.setImage(UIImage(named: selectedImage), for: .normal)
            optionButton.addTarget(self, action: #selector(onOptionButtonClick(_:)), for: .touchUpInside)
            stairView.addArrangedSubview(optionButton)
        }
    }
    
    func finalizeMe() {
        pageTitle.update(lineSize: 32)
    }
    
    func changeTheOption(seqNo: Int) {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            
            self.leftCircleViewContainer.frame.origin.y -= 100
            self.leftCircleViewContainer.alpha = 0.0
            self.rightViewContainer.frame.origin.y += 100
            self.rightViewContainer.alpha = 0.0
            
        }, completion: { (finished: Bool) in
            
            self.leftCircleImageView.isHidden = true
            self.leftCircleImageView.image = UIImage(named: self.imageList[seqNo-1])
            self.resetValues(seqNo: seqNo)
        })
    }
    
    private func resetValues(seqNo : Int) {
        
        if seqNo == 2 || seqNo == 3 {
            resetConstraintsToLeftImageView(width: 38, height: 30)
        } else {
            resetConstraintsToLeftImageView(width: 26, height: 34)
        }
        
        if seqNo == 1 || seqNo == 2 {
            labelOfOptionView.text = "Select document type:"
            optionListCheckBoxView.isHidden = true
            firstCheckListView.isHidden = false
        } else if seqNo == 3 {
            labelOfOptionView.text = "Select an option"
            optionListCheckBoxView.isHidden = false
            firstCheckListView.isHidden = true
        }

        self.leftCircleViewContainer.frame.origin.y += 130
        self.rightViewContainer.frame.origin.y -= 130
        self.leftCircleImageView.isHidden = false
        setActiveText(seqNo: seqNo)
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.leftCircleViewContainer.frame.origin.y -= 30
            self.leftCircleViewContainer.alpha = 1
            self.rightViewContainer.frame.origin.y += 30
            self.rightViewContainer.alpha = 1
        }, completion: nil)
    }
    
    private func setActiveText(seqNo : Int) {
        switch seqNo {
        case 1:
            
            leftCircleTextView.setup(textInfo: "Take a ", textColor: .white, font: UIFont.lato(fontSize: 14))
            leftCircleTextView.setText(textContent: "\nPicture", textColor: .white, font: UIFont.lato(fontSize: 14))
            
        case 2:
            
            leftCircleTextView.setup(textInfo: "Scan &", textColor: .white, font: UIFont.lato(fontSize: 14))
            leftCircleTextView.setText(textContent: "\nUpload a file", textColor: .white, font: UIFont.lato(fontSize: 14))
            
        case 3:
            
            leftCircleTextView.setup(textInfo: "Skip to ", textColor: .white, font: UIFont.lato(fontSize: 14))
            leftCircleTextView.setText(textContent: "\nUpload a file", textColor: .white, font: UIFont.lato(fontSize: 14))
            
        default:
            break
        }
        
        leftCircleTextView.finilizeWithCenterlize()
    }
    
    // MARK: - Events
    
    @objc private func onOptionButtonClick(_ button: SMButton) {
        
        if button.isActive { return }
        
        button.isActive = true
        button.setImage(UIImage(named: "Active"), for: .normal)
        for case let eachButton as SMButton in stairView.subviews {
            
            if eachButton.seqNo != button.seqNo {
                eachButton.isActive = false
                eachButton.setImage(UIImage(named: "Inactive"), for: .normal)
            }
        }
        
        changeTheOption(seqNo: button.seqNo)
    }
    
    @objc private func onCancel(_ sender: UIButton) {
        //TODO:
    }
    
    // MARK: - UIView
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        leftCircleViewContainer.layer.cornerRadius = leftCircleViewContainer.frame.width/2
        leftCircleViewContainer.clipsToBounds = true
    }
}
