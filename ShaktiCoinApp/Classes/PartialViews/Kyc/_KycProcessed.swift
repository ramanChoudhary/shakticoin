//
//  _KycProcessed.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/7/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class _KycProcessed: UIView {

    // MARK: - Declarations
    
    lazy var processedLabel : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()
    
    lazy var processViewContainer : SC2LayerView = {
        let view = SC2LayerView()
        return view
    }()
    
    lazy var imageProcess : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "Process")
        imageView.tintColor = UIColor.mainColor()
        return imageView
    }()
    
    lazy var noteTextView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        let textContent = "Note that your wallet is inactive until we complete \n the KYC, AML and regulatory compliance. Please \n   continue to monitor your progress and update \n        your required documents as necessary. "
        textView.setup(textInfo: textContent, textColor: UIColor.white, font: UIFont.lato(fontSize: 14))
        textView.finilize()
        return textView
    }()
    
    lazy var bottomTextView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        let textContent = "We’re working around the clock to ensure that your wallet is activated as soon as possible. \n\nIn the meantime, keep referring friends to join Shakti to unlock your Bonus Bounty quickly. Remember, your bonus is yours to keep!"
        
        textView.setup(textInfo: textContent, textColor: UIColor.white, font: UIFont.lato(fontSize: 12))
        textView.finilize()
        
        return textView
    }()
    
    lazy var nextButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Invite your friends to Shakti", for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    // MARK: - Methods
    
    func setupViews() {
        
        addSubview(processedLabel)
        processedLabel.Title = "Your KYC is being processed"
        
        _ = processedLabel.anchor(topAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = processedLabel.centralizeX(centerXAnchor)
        
        addSubview(processViewContainer)

        _ = processViewContainer.anchor(processedLabel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 335, heightConstant: 219)
        _ = processViewContainer.centralizeX(centerXAnchor)
        
        processViewContainer.addSubview(imageProcess)
        
        _ = imageProcess.centralizeX(processViewContainer.centerXAnchor)
        _ = imageProcess.centralizeY(processViewContainer.centerYAnchor, constant: -40)
        
        processViewContainer.addSubview(noteTextView)
        
        _ = noteTextView.anchor(imageProcess.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 315, heightConstant: 90)
        _ = noteTextView.centralizeX(processViewContainer.centerXAnchor)
        
        addSubview(bottomTextView)
        
        _ = bottomTextView.anchor(processViewContainer.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 30, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 315, heightConstant: 110)
        _ = bottomTextView.centralizeX(centerXAnchor)
        
        addSubview(nextButton)
        
        _ = nextButton.anchor(nil, left: nil, bottom: bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 20, rightConstant: 0, widthConstant: 330, heightConstant: 36)
        _ = nextButton.centralizeX(centerXAnchor)
        
    }
    
    func finalizeMe() {
        processedLabel.update()
    }
    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
