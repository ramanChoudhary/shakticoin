//
//  _KycVerification.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 9/14/19.
//  Copyright © 2019 Garnik Giloyan. All rights reserved.
//

import UIKit

class _KycVerification: UIView {

    // MARK: - Declarations
    lazy var topLabelLine : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()

    lazy var deliveryImageView : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "Fast Delivery")
        imageView.tintColor = UIColor.mainColor()
        return imageView
    }()

    lazy var fastTrackLabel : UILabel = {
        let label = UILabel()
        label.text = "Fast-track your KYC verification!"
        label.textColor = UIColor.white
        label.font = UIFont.lato(fontSize: 18, type: .Bold) //TODO Medium
        return label
    }()
    
    lazy var textInfo : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.setup(textInfo: "The KYC Validation can take weeks to be processed.", textColor: .white, font: .lato(fontSize: 14))
        textView.setText(textContent: "\n\nPay a small fee to fast-track your KYC verification. By opting to pay for the processing fee, you agree to pay a non-refundable cost for expediting your KYC verification. This is a cost-recovered service paid to a third-party to expedite, verify, and confirm your ID.", textColor: .white, font: .lato(fontSize: 14))
        textView.finilize()
        return textView
    }()
    
    lazy var costTextView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.setup(textInfo: "For only", textColor: .white, font: .lato(fontSize: 14))
        textView.setText(textContent: "  $7.95 USD", textColor: .white, font: .lato(fontSize: 18, type: Lato.Bold)) //TODO Medium
        textView.finilizeWithCenterlize()
        return textView
    }()
    
    lazy var payButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Pay to fast-track your KYC", for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("Cancel & Discard", for: .normal)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    // MARK: - Methods
    func setupViews() {
        
        addSubview(topLabelLine)
        topLabelLine.Title = "File is ready to be sent"
        
        _ = topLabelLine.anchor(topAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = topLabelLine.centralizeX(centerXAnchor)

        addSubview(deliveryImageView)
        _ = deliveryImageView.anchor(topLabelLine.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 40, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 64, heightConstant: 43)
        _ = deliveryImageView.centralizeX(centerXAnchor)
        
        addSubview(fastTrackLabel)
        _ = fastTrackLabel.anchor(deliveryImageView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 35, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = fastTrackLabel.centralizeX(centerXAnchor)
        
        addSubview(textInfo)
        _ = textInfo.anchor(fastTrackLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 25, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 140)
        
        addSubview(costTextView)
        
        _ = costTextView.anchor(textInfo.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 170, heightConstant: 25)
        _ = costTextView.centralizeX(centerXAnchor)
        
        addSubview(payButton)
        
        _ = payButton.anchor(costTextView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 40, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 36)
        
        addSubview(cancelButton)
        
        _ = cancelButton.anchor(payButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 25, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = cancelButton.centralizeX(centerXAnchor)
    }
    
    func finalizeMe() {
        topLabelLine.update()
    }
    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
