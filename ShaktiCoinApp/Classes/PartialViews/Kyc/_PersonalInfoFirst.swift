//
//  VPersonalInfoFirst.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/6/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class _PersonalInfoFirst: UIView {

    // MARK: - Declarations
    
    lazy var subTitle : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()
    
    lazy var firstNamePanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var firstName: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 100
        textField.attributedPlaceholder = Util.NSMAttrString(text: "FirstName".localized, font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var middleNamePanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()

    lazy var middleName: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 115
        textField.attributedPlaceholder = Util.NSMAttrString(text: "MiddleName".localized, font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
 
    lazy var lastNamePanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var lastName: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 100
        textField.attributedPlaceholder = Util.NSMAttrString(text: "LastName".localized, font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var dateOfBirthPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()

    lazy var dateOfBirth: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 115
        textField.attributedPlaceholder = Util.NSMAttrString(text: "DateOfBirth".localized, font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("Cancel".localized, for: .normal)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    lazy var nextButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Next".localized, for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    let firstNameTT = SCToolTip()
    let lastNameTT = SCToolTip()
    let middleNameTT = SCToolTip()
    let dateOfBirthTT = SCToolTip()
    
    // MARK: - Methods
    
    func setupToolTips() {
        firstName.setToolTip(scTT: firstNameTT)
        lastName.setToolTip(scTT: lastNameTT)
        middleName.setToolTip(scTT: middleNameTT)
        dateOfBirth.setToolTip(scTT: dateOfBirthTT)
    }
    
    func setupViews() {
        
        addSubview(subTitle)
        subTitle.Title = "WhoAreYou".localized
        
        _ = subTitle.anchor(topAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = subTitle.centralizeX(centerXAnchor)
        
        addSubview(firstNamePanel)
        addSubview(middleNamePanel)
        addSubview(lastNamePanel)
        addSubview(dateOfBirthPanel)
        addSubview(cancelButton)
        addSubview(nextButton)
        
        _ = firstNamePanel.anchor(subTitle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = firstNamePanel.centralizeX(centerXAnchor)
        
        firstNamePanel.ContainerView.addSubview(firstName)
        firstName.parentPanel = firstNamePanel
        Util.fullyAnchor(_parnetControl: firstNamePanel.ContainerView, _childControl: firstName)
        
        _ = middleNamePanel.anchor(firstNamePanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = middleNamePanel.centralizeX(centerXAnchor)
        
        middleNamePanel.ContainerView.addSubview(middleName)
        middleName.parentPanel = middleNamePanel
        Util.fullyAnchor(_parnetControl: middleNamePanel.ContainerView, _childControl: middleName)
        
        _ = lastNamePanel.anchor(middleNamePanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15
            , leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = lastNamePanel.centralizeX(centerXAnchor)
        
        lastNamePanel.ContainerView.addSubview(lastName)
        lastName.parentPanel = lastNamePanel
        Util.fullyAnchor(_parnetControl: lastNamePanel.ContainerView, _childControl: lastName)
        
        _ = dateOfBirthPanel.anchor(lastNamePanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = dateOfBirthPanel.centralizeX(centerXAnchor)
        
        dateOfBirthPanel.ContainerView.addSubview(dateOfBirth)
        dateOfBirth.parentPanel = dateOfBirthPanel
        Util.fullyAnchor(_parnetControl: dateOfBirthPanel.ContainerView, _childControl: dateOfBirth)
        
        _ = nextButton.anchor(dateOfBirthPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 55, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 36)
        _ = nextButton.centralizeX(centerXAnchor)

        _ = cancelButton.anchor(nextButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = cancelButton.centralizeX(centerXAnchor)
    }
    
    func finalizeMe() {
        subTitle.update()
        firstNamePanel.setup(label: "FirstName".localized)
        middleNamePanel.setup(label: "MiddleName".localized)
        lastNamePanel.setup(label: "LastName".localized)
        dateOfBirthPanel.setup(label: "DateOfBirth".localized)
    }
    
    func validate() -> Bool {
        return true
        var isValid: Bool = true
        
        if firstName.isEmpty() {
            addSubview(firstNameTT)
            firstNameTT.setRelativeXControl(uiControl: firstName)
            firstNameTT.setMessage(message: "FirstNameEmpty".localized)
            isValid = false
        }
        
        if lastName.isEmpty() {
            addSubview(lastNameTT)
            lastNameTT.setRelativeXControl(uiControl: lastName)
            lastNameTT.setMessage(message: "LastNameEmpty".localized)
            isValid = false
        }
        
        if middleName.isEmpty() {
            addSubview(middleNameTT)
            middleNameTT.setRelativeXControl(uiControl: middleName)
            middleNameTT.setMessage(message: "MiddleNameEmpty".localized)
            isValid = false
        }
        
        if dateOfBirth.isEmpty() {
            addSubview(dateOfBirthTT)
            dateOfBirthTT.setRelativeXControl(uiControl: dateOfBirth)
            dateOfBirthTT.setMessage(message: "DateOfBirthEmpty".localized)
            isValid = false
        }
        
        return isValid

    }
    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupToolTips()
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
