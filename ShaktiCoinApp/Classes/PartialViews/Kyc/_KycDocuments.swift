//
//  _KycDocuments.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/6/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class _KycDocuments: UIView {

    // MARK: - Declarations
    
    lazy var editFileTitle : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()
   
    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("Cancel & Discard", for: .normal)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    lazy var nextButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Save File", for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    lazy var orAlternativeLbl : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()
    
    lazy var documentChooser : _DocumentChooser = {
        let docChooser = _DocumentChooser()
        return docChooser
    }()
    
    lazy var documentLbl : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "Choose document type:"
        label.font = UIFont.lato(fontSize: 14)
        return label
    }()
    
    lazy var suffDocLbl : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "2 documents are sufficient."
        label.font = UIFont.lato(fontSize: 14)
        return label
    }()
    
    // MARK: - Methods
    
    func setupViews() {
        
        addSubview(editFileTitle)
        editFileTitle.Title = "Edit your uploaded file below"
        
        _ = editFileTitle.anchor(topAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = editFileTitle.centralizeX(centerXAnchor)
        
        
        addSubview(documentChooser)
        
        _ = documentChooser.anchor(editFileTitle.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 100, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 150)
        
        addSubview(documentLbl)
        _ = documentLbl.anchor(documentChooser.topAnchor, left: nil, bottom: nil, right: nil, topConstant: -30, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = documentLbl.centralizeX(centerXAnchor)

        addSubview(suffDocLbl)
        
        _ = suffDocLbl.anchor(documentChooser.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 55, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = suffDocLbl.centralizeX(centerXAnchor)
        
        addSubview(cancelButton)
        addSubview(nextButton)
        
        _ = cancelButton.anchor(nil, left: nil, bottom: bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 20, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = cancelButton.centralizeX(centerXAnchor)
        
        _ = nextButton.anchor(nil, left: nil, bottom: cancelButton.topAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 25, rightConstant: 0, widthConstant: 330, heightConstant: 36)
        _ = nextButton.centralizeX(centerXAnchor)
    }
    
    func finalizeMe() {
        editFileTitle.update()
    }
    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
