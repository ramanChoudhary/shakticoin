//
//  EmailOrPhoneSwitcher.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 1/17/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import UIKit

extension UISwitch {

    func setOffColor(offTint color: UIColor ) {
        let minSide = min(bounds.size.height, bounds.size.width)
        layer.cornerRadius = minSide / 2
        backgroundColor = color
        tintColor = color
    }
}

class _EmailOrPhoneSwitcher: UIView {

    // MARK: - Declarations
    
    lazy var emailLabel : UILabel = {
        let label = UILabel()
        label.text = "Email Address"
        label.textColor = .white
        label.font = .lato(fontSize: 14)
        return label
    }()
    
    lazy var mobileLabel : UILabel = {
        let label = UILabel()
        label.text = "Phone Number"
        label.textColor = .gray
        label.font = .lato(fontSize: 14)
        return label
    }()
    
    lazy var switcher : UISwitch = {
        let switch_ = UISwitch()
        switch_.isOn = false
        switch_.tintColor = .mainColor()
        switch_.onTintColor = .mainColor()
        switch_.setOffColor(offTint: .mainColor())
        switch_.addTarget(self, action: #selector(switchChanged), for: .valueChanged)
        return switch_
    }()
    
    // MARK: - Methods
    
    private func setupViews() {

        addSubview(emailLabel)
        _ = emailLabel.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = emailLabel.centralizeY(centerYAnchor)
        
        addSubview(switcher)
        _ = switcher.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = switcher.centralizeX(centerXAnchor)
        _ = switcher.centralizeY(centerYAnchor)
        
        addSubview(mobileLabel)
        _ = mobileLabel.anchor(topAnchor, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        _ = mobileLabel.centralizeY(centerYAnchor)
    }
    
    // MARK: - Events
    
    @objc func switchChanged(sender: UISwitch) {
        let value = sender.isOn
        if value {
            mobileLabel.textColor = .white
            emailLabel.textColor = .gray
        } else {
            mobileLabel.textColor = .gray
            emailLabel.textColor = .white
        }
    }
    
    // MARK: - Initializations
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
