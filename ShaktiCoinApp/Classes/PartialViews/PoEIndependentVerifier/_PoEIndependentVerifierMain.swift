//
//  _PoEIndependentVerifierMain.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 3/13/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import UIKit

class _PoEIndependentVerifierMain: UIView {

    // MARK: - Declarations
    
    lazy var subTitle : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()
    
    lazy var verifierWalletIdPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var verifierWalletId: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 125
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Verifier WalletID", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var verifierVaultIdPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var verifierVaultId: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 125
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Verifier Provider bizVaultID", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    
    lazy var primaryEmailPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var primaryEmail: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 125
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Verifier Email", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()

    lazy var primaryPhonePanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var primaryPhone: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 125
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Verifier Primary Phone", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var secondaryPhonePanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var secondaryPhone: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 125
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Verifier Secondary Phone", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var verifierCountryPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var verifierCountry: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 125
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Verifier Country of Residence", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()

    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("Cancel".localized, for: .normal)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    lazy var nextButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Updatepersonalinformation".localized, for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        button.tag = 2
        return button
    }()

    
    // MARK: - Methods
    
    private func setupViews() {
        
        addSubview(subTitle)
        subTitle.Title = "Who are you?"
        
        _ = subTitle.anchor(topAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = subTitle.centralizeX(centerXAnchor)
        
        addSubview(verifierWalletIdPanel)
        
        _ = verifierWalletIdPanel.anchor(subTitle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = verifierWalletIdPanel.centralizeX(centerXAnchor)
        
        verifierWalletIdPanel.ContainerView.addSubview(verifierWalletId)
        verifierWalletId.parentPanel = verifierWalletIdPanel
        Util.fullyAnchor(_parnetControl: verifierWalletIdPanel.ContainerView, _childControl: verifierWalletId)
        
        addSubview(verifierVaultIdPanel)
        
        _ = verifierVaultIdPanel.anchor(verifierWalletIdPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = verifierVaultIdPanel.centralizeX(centerXAnchor)
        
        verifierVaultIdPanel.ContainerView.addSubview(verifierVaultId)
        verifierVaultId.parentPanel = verifierVaultIdPanel
        Util.fullyAnchor(_parnetControl: verifierVaultIdPanel.ContainerView, _childControl: verifierVaultId)
        
        addSubview(primaryEmailPanel)
        
        _ = primaryEmailPanel.anchor(verifierVaultIdPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = primaryEmailPanel.centralizeX(centerXAnchor)
        
        primaryEmailPanel.ContainerView.addSubview(primaryEmail)
        primaryEmail.parentPanel = primaryEmailPanel
        Util.fullyAnchor(_parnetControl: primaryEmailPanel.ContainerView, _childControl: primaryEmail)
        
        addSubview(primaryPhonePanel)
        
        _ = primaryPhonePanel.anchor(primaryEmailPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = primaryPhonePanel.centralizeX(centerXAnchor)
        
        primaryPhonePanel.ContainerView.addSubview(primaryPhone)
        primaryPhone.parentPanel = primaryPhonePanel
        Util.fullyAnchor(_parnetControl: primaryPhonePanel.ContainerView, _childControl: primaryPhone)
        
        addSubview(secondaryPhonePanel)
        
        _ = secondaryPhonePanel.anchor(primaryEmailPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = secondaryPhonePanel.centralizeX(centerXAnchor)
        
        secondaryPhonePanel.ContainerView.addSubview(secondaryPhone)
        secondaryPhone.parentPanel = secondaryPhonePanel
        Util.fullyAnchor(_parnetControl: secondaryPhonePanel.ContainerView, _childControl: secondaryPhone)
        
        addSubview(verifierCountryPanel)
        
        _ = verifierCountryPanel.anchor(secondaryPhonePanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = verifierCountryPanel.centralizeX(centerXAnchor)
        
        verifierCountryPanel.ContainerView.addSubview(verifierCountry)
        verifierCountry.parentPanel = verifierCountryPanel
        Util.fullyAnchor(_parnetControl: verifierCountryPanel.ContainerView, _childControl: verifierCountry)

        addSubview(nextButton)
        
        _ = nextButton.anchor(verifierCountryPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 27, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 36)
        _ = nextButton.centralizeX(centerXAnchor)

        addSubview(cancelButton)
        
        _ = cancelButton.anchor(nextButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = cancelButton.centralizeX(centerXAnchor)

    }
    
    func finalizeMe() {
        subTitle.update()
        verifierWalletIdPanel.setup(label: "Verifier WalletID")
        verifierVaultIdPanel.setup(label: "Verifier bizVaultID")
        primaryEmailPanel.setup(label: "Verifier Email")
        primaryPhonePanel.setup(label: "Verifier Primary Phone")
        secondaryPhonePanel.setup(label: "Verifier Secondary Phone")
        verifierCountryPanel.setup(label: "Verifier Country of Residence")
    }
    
    func validate() -> Bool {
        return true
        var isValid: Bool = true
        return isValid
    }

    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


}
