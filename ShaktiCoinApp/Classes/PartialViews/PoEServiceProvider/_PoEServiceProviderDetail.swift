//
//  _PoEServiceProviderDetail.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 3/10/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import UIKit

class _PoEServiceProviderDetail: UIView {

    // MARK: - Declarations
    
    lazy var subTitle : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()

    lazy var providerWalletIdPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var providerWalletId: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 125
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Service Provider WalletID", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var providerVaultIdPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var providerVaultId: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 125
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Service Provider bizVaultID", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var taxPayingCountryPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var taxPayingCountry: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 125
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Service Provider Tax Paying Country", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var primaryEmailPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var primaryEmail: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 125
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Service Provider Primary Email", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()

    lazy var primaryPhonePanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var primaryPhone: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 125
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Service Provider Primary Phone", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var businessNamePanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var businessName: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 125
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Service Provider Business Name", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()

    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("Cancel".localized, for: .normal)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    lazy var nextButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Updatepersonalinformation".localized, for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        button.tag = 2
        return button
    }()
    
    let providerWalletIdTT = SCToolTip()
    let providerVaultIdTT = SCToolTip()
    let taxPayingCountryTT = SCToolTip()
    let primaryEmailTT = SCToolTip()
    let primaryPhoneTT = SCToolTip()
    let businessNameTT = SCToolTip()
    
    // MARK: - Methods
    
    func setupToolTips() {
        providerWalletId.setToolTip(scTT: providerWalletIdTT)
        providerVaultId.setToolTip(scTT: providerVaultIdTT)
        taxPayingCountry.setToolTip(scTT: taxPayingCountryTT)
        primaryEmail.setToolTip(scTT: primaryEmailTT)
        primaryPhone.setToolTip(scTT: primaryPhoneTT)
        businessName.setToolTip(scTT: businessNameTT)
    }
    
    private func setupViews() {
    
        addSubview(subTitle)
        subTitle.Title = "Service Provider Details"
        
        _ = subTitle.anchor(topAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = subTitle.centralizeX(centerXAnchor)
        
        addSubview(providerWalletIdPanel)
        
        _ = providerWalletIdPanel.anchor(subTitle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = providerWalletIdPanel.centralizeX(centerXAnchor)
        
        providerWalletIdPanel.ContainerView.addSubview(providerWalletId)
        providerWalletId.parentPanel = providerWalletIdPanel
        Util.fullyAnchor(_parnetControl: providerWalletIdPanel.ContainerView, _childControl: providerWalletId)
        
        addSubview(providerVaultIdPanel)
        
        _ = providerVaultIdPanel.anchor(providerWalletIdPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = providerVaultIdPanel.centralizeX(centerXAnchor)
        
        providerVaultIdPanel.ContainerView.addSubview(providerVaultId)
        providerVaultId.parentPanel = providerVaultIdPanel
        Util.fullyAnchor(_parnetControl: providerVaultIdPanel.ContainerView, _childControl: providerVaultId)

        addSubview(taxPayingCountryPanel)
        
        _ = taxPayingCountryPanel.anchor(providerVaultIdPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = taxPayingCountryPanel.centralizeX(centerXAnchor)
        
        taxPayingCountryPanel.ContainerView.addSubview(taxPayingCountry)
        taxPayingCountry.parentPanel = taxPayingCountryPanel
        Util.fullyAnchor(_parnetControl: taxPayingCountryPanel.ContainerView, _childControl: taxPayingCountry)
        
        addSubview(primaryEmailPanel)
        
        _ = primaryEmailPanel.anchor(taxPayingCountryPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = primaryEmailPanel.centralizeX(centerXAnchor)
        
        primaryEmailPanel.ContainerView.addSubview(primaryEmail)
        primaryEmail.parentPanel = primaryEmailPanel
        Util.fullyAnchor(_parnetControl: primaryEmailPanel.ContainerView, _childControl: primaryEmail)
        
        addSubview(primaryPhonePanel)
        
        _ = primaryPhonePanel.anchor(primaryEmailPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = primaryPhonePanel.centralizeX(centerXAnchor)
        
        primaryPhonePanel.ContainerView.addSubview(primaryPhone)
        primaryPhone.parentPanel = primaryPhonePanel
        Util.fullyAnchor(_parnetControl: primaryPhonePanel.ContainerView, _childControl: primaryPhone)
        
        addSubview(businessNamePanel)
        
        _ = businessNamePanel.anchor(primaryPhonePanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = businessNamePanel.centralizeX(centerXAnchor)
        
        businessNamePanel.ContainerView.addSubview(businessName)
        businessName.parentPanel = businessNamePanel
        Util.fullyAnchor(_parnetControl: businessNamePanel.ContainerView, _childControl: businessName)

        addSubview(nextButton)
        
        _ = nextButton.anchor(businessNamePanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 27, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 36)
        _ = nextButton.centralizeX(centerXAnchor)

        addSubview(cancelButton)
        
        _ = cancelButton.anchor(nextButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = cancelButton.centralizeX(centerXAnchor)
    }
    
    func finalizeMe() {
        subTitle.update()
        providerWalletIdPanel.setup(label: "Service Provider WalletID")
        providerVaultIdPanel.setup(label: "Service Provider bizVaultID")
        taxPayingCountryPanel.setup(label: "Service Provider Tax Paying Country")
        primaryEmailPanel.setup(label: "Service Provider Primary Email")
        primaryPhonePanel.setup(label: "Service Provider Primary Phone")
        businessNamePanel.setup(label: "Service Provider Business Name")
    }
    
    func validate() -> Bool {
        return true
        var isValid: Bool = true
        return isValid
    }

    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupViews()
        setupToolTips()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
