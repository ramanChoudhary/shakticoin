//
//  _PoESPWhereYouWork.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 3/8/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import UIKit

class _PoESPWhereYouWork: UIView {

    // MARK: - Declarations
    
    lazy var subTitle : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()
    
    lazy var countryPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var country: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 125
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Tax Paying Country", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var dStatePanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var dState: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 125
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Select Your State", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()

    lazy var cityPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var city: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 70
        textField.attributedPlaceholder = Util.NSMAttrString(text: "City".localized, font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var addressLine1Panel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var addressLine1: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 120
        textField.attributedPlaceholder = Util.NSMAttrString(text: "AddressLine1".localized, font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()

    lazy var addressLine2Panel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var addressLine2: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 120
        textField.attributedPlaceholder = Util.NSMAttrString(text: "AddressLine2".localized, font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var zipCodePanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()

    lazy var zipCode: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 100
        textField.attributedPlaceholder = Util.NSMAttrString(text: "ZipPostal".localized, font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()

    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("Cancel".localized, for: .normal)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    lazy var nextButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Updatepersonalinformation".localized, for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        button.tag = 1
        return button
    }()
    
    let countryTT = SCToolTip()
    let dStateTT = SCToolTip()
    let cityTT = SCToolTip()
    let addressLine1TT = SCToolTip()
    let addressLine2TT = SCToolTip()
    let zipCodeTT = SCToolTip()

    // MARK: - Methods
    func setupToolTips() {
        country.setToolTip(scTT: countryTT)
        dState.setToolTip(scTT: dStateTT)
        city.setToolTip(scTT: cityTT)
        addressLine1.setToolTip(scTT: addressLine1TT)
        addressLine2.setToolTip(scTT: addressLine2TT)
        zipCode.setToolTip(scTT: zipCodeTT)
    }
    
    private func setupViews() {
        
        addSubview(subTitle)
        subTitle.Title = "Where do you work?"
        
        _ = subTitle.anchor(topAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = subTitle.centralizeX(centerXAnchor)
        
        addSubview(countryPanel)
        
        _ = countryPanel.anchor(subTitle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = countryPanel.centralizeX(centerXAnchor)
        
        countryPanel.ContainerView.addSubview(country)
        country.parentPanel = countryPanel
        Util.fullyAnchor(_parnetControl: countryPanel.ContainerView, _childControl: country)
        
        addSubview(dStatePanel)
        
        _ = dStatePanel.anchor(countryPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = dStatePanel.centralizeX(centerXAnchor)
        
        dStatePanel.ContainerView.addSubview(dState)
        dState.parentPanel = dStatePanel
        Util.fullyAnchor(_parnetControl: dStatePanel.ContainerView, _childControl: dState)
        
        addSubview(cityPanel)
        
        _ = cityPanel.anchor(dStatePanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = cityPanel.centralizeX(centerXAnchor)
        
        cityPanel.ContainerView.addSubview(city)
        city.parentPanel = cityPanel
        Util.fullyAnchor(_parnetControl: cityPanel.ContainerView, _childControl: city)
        
        addSubview(addressLine1Panel)
        
        _ = addressLine1Panel.anchor(cityPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = addressLine1Panel.centralizeX(centerXAnchor)

        addressLine1Panel.ContainerView.addSubview(addressLine1)
        addressLine1.parentPanel = addressLine1Panel
        Util.fullyAnchor(_parnetControl: addressLine1Panel.ContainerView, _childControl: addressLine1)
        
        addSubview(addressLine2Panel)
        
        _ = addressLine2Panel.anchor(addressLine1Panel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = addressLine2Panel.centralizeX(centerXAnchor)
        
        addressLine2Panel.ContainerView.addSubview(addressLine2)
        addressLine2.parentPanel = addressLine2Panel
        Util.fullyAnchor(_parnetControl: addressLine2Panel.ContainerView, _childControl: addressLine2)
        
        addSubview(zipCodePanel)

        _ = zipCodePanel.anchor(addressLine2Panel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = zipCodePanel.centralizeX(centerXAnchor)
        
        zipCodePanel.ContainerView.addSubview(zipCode)
        zipCode.parentPanel = zipCodePanel
        Util.fullyAnchor(_parnetControl: zipCodePanel.ContainerView, _childControl: zipCode)

        addSubview(nextButton)
        
        _ = nextButton.anchor(zipCodePanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 27, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 36)
        _ = nextButton.centralizeX(centerXAnchor)

        addSubview(cancelButton)
        
        _ = cancelButton.anchor(nextButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = cancelButton.centralizeX(centerXAnchor)
    }

    func finalizeMe() {
        subTitle.update()
        countryPanel.setup(label: "Tax Paying Country")
        dStatePanel.setup(label: "Select Your State")
        cityPanel.setup(label: "City")
        addressLine1Panel.setup(label: "Address Line 1")
        addressLine2Panel.setup(label: "Address Line 2")
        zipCodePanel.setup(label: "Zip Code")
    }
    
    func validate() -> Bool {
        return true
        var isValid: Bool = true
        return isValid
    }
    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
