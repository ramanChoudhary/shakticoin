//
//  _SubView.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/10/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class _SubView: SC2LayerView {

    // MARK: - Declarations
    
    lazy var icon : UIImageView = {
        let iv = UIImageView()
        iv.tintColor = UIColor.mainColor()
        return iv
    }()
    
    lazy var textInfo : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        return textView
    }()
    
    func setupViews() {
        
        self.transparency = 0.40
        
        addSubview(icon)
        addSubview(textInfo)
        
        _ = icon.anchor(nil, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = icon.centralizeY(centerYAnchor)
        
        _ = textInfo.anchor(nil, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 15, widthConstant: 230, heightConstant: 70)
        _ = textInfo.centralizeY(centerYAnchor)
    }
    
    func finalizeMe() { }
    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //backgroundColor = .black
        //alpha = 0.70
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
