//
//  _CurrentBalance.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/10/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class _CurrentBalance: UIView {

    // MARK: - Declarations
    
    lazy var currentBalanceLbl : UILabel = {
        let label = UILabel()
        label.text = "Current Balance:"
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 12, type: .Bold)
        return label
    }()
    
    lazy var currentBalanceValue : UILabel = {
        let label = UILabel()
        label.text = "SXE 1008.00"
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 36, type: .Bold) //TODO Medium
        return label
    }()
    
    lazy var lockedLink : UILabel = {
        let link = UILabel()
        link.font = UIFont.lato(fontSize: 16, type: .Regular)
        link.text = "Locked"
        link.underline()
        link.textColor = .white
        return link
    }()
    
    lazy var lockIcon : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "Lock Icon Small")
        iv.tintColor = .white
        return iv
    }()
    
    lazy var unlockQuestion : SCLineTitle = {
        let lineTitle = SCLineTitle()
        lineTitle.Title = "Would you like to unlock it?"
        return lineTitle
    }()
        
    func setupViews() {
        
        addSubview(currentBalanceLbl)
        addSubview(currentBalanceValue)
        addSubview(lockedLink)
        addSubview(lockIcon)
        addSubview(unlockQuestion)
        
        _ = currentBalanceLbl.anchorToTop(topAnchor, topConstant: 20)
        _ = currentBalanceLbl.centralizeX(centerXAnchor)
        
        _ = currentBalanceValue.anchorToTop(currentBalanceLbl.topAnchor, topConstant: 25)
        _ = currentBalanceValue.centralizeX(centerXAnchor)
     
        _ = lockedLink.anchorToTop(currentBalanceValue.bottomAnchor, topConstant: 22)
        _ = lockedLink.centralizeX(centerXAnchor, constant: -10)
        
        _ = lockIcon.anchor(currentBalanceValue.bottomAnchor, left: lockedLink.rightAnchor, bottom: nil, right: nil, topConstant: 22, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = unlockQuestion.anchor(lockedLink.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = unlockQuestion.centralizeX(centerXAnchor)
    }

    func finalizeMe() {
        unlockQuestion.updateWithSpecificWidth(width: UIScreen.main.bounds.width, lineSize: 17)
    }
    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
