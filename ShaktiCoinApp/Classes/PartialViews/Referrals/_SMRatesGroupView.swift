//
//  _SMRatesGroupView.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/13/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class _SMRatesGroupView: UIView {

    // MARK: - Declarations

    lazy var convertedCircle : UIButton = {
        let view = UIButton()
        view.backgroundColor = .mainColor()
        return view
    }()
    
    lazy var progressingCircle : UIButton = {
        let view = UIButton()
        view.backgroundColor = .gray
        return view
    }()
    
    lazy var influencedCircle : UIButton = {
        let view = UIButton()
        view.backgroundColor = .darkGray
        return view
    }()
    
    func setupViews() {
    
        addSubview(convertedCircle)
        
        _ = convertedCircle.anchor(nil, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 0, widthConstant: 20, heightConstant: 20)
        _ = convertedCircle.centralizeY(centerYAnchor)
        
        let labelConverted = UILabel()
        labelConverted.text = "Converted"
        labelConverted.textColor = .white
        labelConverted.font = UIFont.lato(fontSize: 12)
        
        addSubview(labelConverted)
        
        _ = labelConverted.anchor(convertedCircle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        addSubview(progressingCircle)
        
        _ = progressingCircle.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 20, heightConstant: 20)
        _ = progressingCircle.centralizeX(centerXAnchor)
        _ = progressingCircle.centralizeY(centerYAnchor)
        
        let labelProgressing = UILabel()
        labelProgressing.text = "Progressing"
        labelProgressing.textColor = .white
        labelProgressing.font = UIFont.lato(fontSize: 12)
        
        addSubview(labelProgressing)
        
        _ = labelProgressing.anchor(progressingCircle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = labelProgressing.centralizeX(centerXAnchor)
        
        addSubview(influencedCircle)
        
        
        _ = influencedCircle.anchor(nil, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 20, heightConstant: 20)
        
        _ = influencedCircle.centralizeY(centerYAnchor)
        
        
        let labelInfluenced = UILabel()
        labelInfluenced.text = "Influenced"
        labelInfluenced.textColor = .white
        labelInfluenced.font = UIFont.lato(fontSize: 12)
        
        addSubview(labelInfluenced)
        
        _ = labelInfluenced.anchor(influencedCircle.bottomAnchor, left: influencedCircle.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: -20, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    func finalizeMe() { }
    
    
    // MARK: - View
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    override func layoutSubviews() {
        
        convertedCircle.layer.cornerRadius = 10
        convertedCircle.clipsToBounds = true

        progressingCircle.layer.cornerRadius = 10
        progressingCircle.clipsToBounds = true
        
        influencedCircle.layer.cornerRadius = 10
        influencedCircle.clipsToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
