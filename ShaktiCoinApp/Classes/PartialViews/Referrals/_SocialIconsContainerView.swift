//
//  _SocialIconsContainerView.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/11/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class _SocialIconsContainerView: UIView {

    // MARK: - Declarations
    
    lazy var firstRow : UIStackView = {
        var view = UIStackView()
        return view
    }()
    
    lazy var secondRow : UIStackView = {
        var view = UIStackView()
        return view
    }()
    
    lazy var thirdRow : UIStackView = {
        var view = UIStackView()
        return view
    }()
    
    lazy var docItems : [DocumentItem] = {
        let fb = DocumentItem(caption: "Facebook", icon: "Facebook")
        let instagram = DocumentItem(caption: "Instagram", icon: "Instagram")
        let google = DocumentItem(caption: "Google+", icon: "GooglePlus")
        let linkedIn = DocumentItem(caption: "LinkedIn", icon: "LinkedIn")
        let twitter = DocumentItem(caption: "Twitter", icon: "Twitter")
        let pinterest = DocumentItem(caption: "Pinterest", icon: "Pinterest")
        let skype = DocumentItem(caption: "Skype", icon: "Skype")
        let vk = DocumentItem(caption: "VK", icon: "WK")
        let weChat = DocumentItem(caption: "WeChat", icon: "WeChat")
        let tumblr = DocumentItem(caption: "Tumblr", icon: "Tumblr")
        let email = DocumentItem(caption: "Email", icon: "Email")
        let other = DocumentItem(caption: "Other", icon: "Other")
        return [fb,instagram,google,linkedIn,twitter,pinterest,skype,vk,weChat,tumblr,email,other]
    }()

    // MARK: - Methods
    
    func setupViews() {
        
        addSubview(firstRow)
        addSubview(secondRow)
        addSubview(thirdRow)
        
        _ = firstRow.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 70)
        
        _ = secondRow.anchor(firstRow.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 5, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 70)
        
        _ = thirdRow.anchor(secondRow.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 5, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 70)
        
        firstRow.axis = .horizontal
        firstRow.distribution = .fillEqually
        firstRow.spacing = 5
        
        secondRow.axis = .horizontal
        secondRow.distribution = .fillEqually
        secondRow.spacing = 5
        
        thirdRow.axis = .horizontal
        thirdRow.distribution = .fillEqually
        thirdRow.spacing = 5
        
        for i in 0...3 {
            
            let contView = UIView()
            contView.backgroundColor = .clear
            
            let imageView = UIImageView()
            imageView.image = UIImage(named: docItems[i].icon)
            
            firstRow.addArrangedSubview(contView)
            
            contView.addSubview(imageView)
            _ = imageView.centralizeX(contView.centerXAnchor)
            _ = imageView.centralizeY(contView.centerYAnchor, constant: -5)
            imageView.tintColor = UIColor.white
            
            let label: UILabel = UILabel()
            label.text = docItems[i].caption
            label.font = UIFont.lato(fontSize: 11)
            label.textColor = .white
            
            contView.addSubview(label)
            
            _ = label.anchor(imageView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
            _ = label.centralizeX(contView.centerXAnchor)
        }
        
        for i in 0...3 {
            
            let secContView = SC2LayerView()
            secContView.transparency = 0.70
            
            let imageView = UIImageView()
            imageView.image = UIImage(named: docItems[i+4].icon)
            
            secondRow.addArrangedSubview(secContView)
            
            secContView.addSubview(imageView)
            _ = imageView.centralizeX(secContView.centerXAnchor)
            _ = imageView.centralizeY(secContView.centerYAnchor, constant: -5)
            
            imageView.tintColor = UIColor.white
            
            let label: UILabel = UILabel()
            label.text = docItems[i+4].caption
            label.font = UIFont.lato(fontSize: 11)
            label.textColor = .white
            
            secContView.addSubview(label)
            
            _ = label.anchor(imageView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
            _ = label.centralizeX(secContView.centerXAnchor)
        }
        
        for i in 0...3 {
            
            let thirdContView = SC2LayerView()
            thirdContView.transparency = 0.70
            
            let imageView = UIImageView()
            imageView.image = UIImage(named: docItems[i+8].icon)
            
            thirdRow.addArrangedSubview(thirdContView)
            
            thirdContView.addSubview(imageView)
            _ = imageView.centralizeX(thirdContView.centerXAnchor)
            _ = imageView.centralizeY(thirdContView.centerYAnchor, constant: -5)
            
            imageView.tintColor = UIColor.white
            
            let label: UILabel = UILabel()
            label.text = docItems[i+8].caption
            label.font = UIFont.lato(fontSize: 11)
            label.textColor = .white
            
            thirdContView.addSubview(label)
            
            _ = label.anchor(imageView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
            _ = label.centralizeX(thirdContView.centerXAnchor)
        }
        
    }
    
    override func layoutSubviews() {
        
        
        
    }
    
    func finalizeMe() {}
    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


}
