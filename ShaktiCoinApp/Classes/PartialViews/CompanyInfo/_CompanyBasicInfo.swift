//
//  _CompanyBasicInfo.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 2/27/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import UIKit

class _CompanyBasicInfo: UIView {

    // MARK: - Declarations
    
    lazy var subTitle : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()
    
    lazy var companyNamePanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var companyName: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 125
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Company Name", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()

    lazy var tradeNamePanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()

    lazy var tradeName: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 105
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Trade Name", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var structurePanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var structure: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 145
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Company Structure", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()

    lazy var dateOfEPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var dateOfE: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 160
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Date of Establishment", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var iCodePanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var iCode: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 160
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Industry Code", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var vaultIdPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var vaultId: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 160
        textField.attributedPlaceholder = Util.NSMAttrString(text: "VaultID", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var tradeBindLabel : UILabel = {
        let label = UILabel()
        label.font = UIFont.lato(fontSize: 12)
        label.textColor = .white
        label.text = "Bind Trade Name"
        return label
    }()
    
    lazy var tradeSwitcher : UISwitch = {
        let switch_ = UISwitch()
        switch_.isOn = false
        switch_.tintColor = .mainColor()
        switch_.onTintColor = .mainColor()
        switch_.setOffColor(offTint: .gray)
        //switch_.addTarget(self, action: #selector(switchChanged), for: .valueChanged)
        return switch_
    }()
    
    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("Cancel".localized, for: .normal)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    lazy var nextButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Next".localized, for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        button.tag = 1
        return button
    }()

    
    let companyNameTT = SCToolTip()
    let tradeNameTT = SCToolTip()
    let structureTT = SCToolTip()
    let dateOfETT = SCToolTip()
    let iCodeTT = SCToolTip()
    let vaultIDTT = SCToolTip()
    
    // MARK: - Methods
    func setupToolTips() {
        companyName.setToolTip(scTT: companyNameTT)
        tradeName.setToolTip(scTT: tradeNameTT)
        structure.setToolTip(scTT: structureTT)
        dateOfE.setToolTip(scTT: dateOfETT)
        iCode.setToolTip(scTT: iCodeTT)
        vaultId.setToolTip(scTT: vaultIDTT)
    }

    func setupViews() {
        
        addSubview(subTitle)
        subTitle.Title = "WhoAreYou".localized
        
        _ = subTitle.anchor(topAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = subTitle.centralizeX(centerXAnchor)
        
        addSubview(companyNamePanel)
        
        _ = companyNamePanel.anchor(subTitle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = companyNamePanel.centralizeX(centerXAnchor)
        
        companyNamePanel.ContainerView.addSubview(companyName)
        companyName.parentPanel = companyNamePanel
        Util.fullyAnchor(_parnetControl: companyNamePanel.ContainerView, _childControl: companyName)
        
        addSubview(tradeNamePanel)
        
        _ = tradeNamePanel.anchor(companyNamePanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = tradeNamePanel.centralizeX(centerXAnchor)
        
        tradeNamePanel.ContainerView.addSubview(tradeName)
        tradeName.parentPanel = tradeNamePanel
        Util.fullyAnchor(_parnetControl: tradeNamePanel.ContainerView, _childControl: tradeName)

        addSubview(structurePanel)
        
        _ = structurePanel.anchor(tradeNamePanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = structurePanel.centralizeX(centerXAnchor)
        
        structurePanel.ContainerView.addSubview(structure)
        structure.parentPanel = structurePanel
        Util.fullyAnchor(_parnetControl: structurePanel.ContainerView, _childControl: structure)

        addSubview(dateOfEPanel)
        
        _ = dateOfEPanel.anchor(structurePanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = dateOfEPanel.centralizeX(centerXAnchor)
        
        dateOfEPanel.ContainerView.addSubview(dateOfE)
        dateOfE.parentPanel = dateOfEPanel
        Util.fullyAnchor(_parnetControl: dateOfEPanel.ContainerView, _childControl: dateOfE)
        
        addSubview(iCodePanel)
        
        _ = iCodePanel.anchor(dateOfEPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = iCodePanel.centralizeX(centerXAnchor)
        
        iCodePanel.ContainerView.addSubview(iCode)
        iCode.parentPanel = iCodePanel
        Util.fullyAnchor(_parnetControl: iCodePanel.ContainerView, _childControl: iCode)
        
        addSubview(vaultIdPanel)

        _ = vaultIdPanel.anchor(iCodePanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = vaultIdPanel.centralizeX(centerXAnchor)
        
        vaultIdPanel.ContainerView.addSubview(vaultId)
        vaultId.parentPanel = vaultIdPanel
        Util.fullyAnchor(_parnetControl: vaultIdPanel.ContainerView, _childControl: vaultId)
        
        addSubview(tradeBindLabel)
        _ = tradeBindLabel.anchor(vaultIdPanel.bottomAnchor, left: vaultIdPanel.leftAnchor, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        addSubview(tradeSwitcher)
        _ = tradeSwitcher.anchor(vaultIdPanel.bottomAnchor, left: tradeBindLabel.rightAnchor, bottom: nil, right: nil, topConstant: 15, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        addSubview(nextButton)
        addSubview(cancelButton)
        
        _ = nextButton.anchor(tradeSwitcher.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 27, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 36)
        _ = nextButton.centralizeX(centerXAnchor)

        _ = cancelButton.anchor(nextButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = cancelButton.centralizeX(centerXAnchor)
    }
    
    func finalizeMe() {
        subTitle.update()
        companyNamePanel.setup(label: "Company Name")
        tradeNamePanel.setup(label: "Trade Name")
        structurePanel.setup(label: "Company Structure")
        dateOfEPanel.setup(label: "Date of Establishment")
        iCodePanel.setup(label: "Industry Code")
        vaultIdPanel.setup(label: "VaultID")
    }
    
    func validate() -> Bool {
        return true
        var isValid: Bool = true
        
        if companyName.isEmpty() {
            addSubview(companyNameTT)
            companyNameTT.setRelativeXControl(uiControl: companyName)
            companyNameTT.setMessage(message: "Please fill company name")
            isValid = false
        }
        
        if tradeName.isEmpty() {
            addSubview(companyNameTT)
            companyNameTT.setRelativeXControl(uiControl: companyName)
            companyNameTT.setMessage(message: "Please fill trade name")
            isValid = false
        }
        
        if structure.isEmpty() {
           addSubview(structureTT)
           structureTT.setRelativeXControl(uiControl: structure)
           structureTT.setMessage(message: "Please fill company structure")
           isValid = false
        }
        
        if dateOfE.isEmpty() {
           addSubview(dateOfETT)
           dateOfETT.setRelativeXControl(uiControl: dateOfE)
           dateOfETT.setMessage(message: "Please fill company structure")
           isValid = false
        }
        return isValid
    }
    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupToolTips()
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
