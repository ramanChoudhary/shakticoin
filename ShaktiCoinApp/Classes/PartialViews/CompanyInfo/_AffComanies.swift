//
//  _AffComanies.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 3/1/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import UIKit

class _AffComanies: UIView {

    // MARK: - Declarations
    
    lazy var subTitle : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()
    
    lazy var corporateNamePanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
 
    lazy var corporateName: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 125
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Corporate Name", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var emailAddressPanel: SCTextFieldPanel = {
       let panel = SCTextFieldPanel()
       panel.backgroundColor = .clear
       return panel
    }()

    lazy var emailAddress: SCXTextField = {
       let textField = SCXTextField()
       textField.floatingLabelValue = 125
       textField.attributedPlaceholder = Util.NSMAttrString(text: "Email Address", font: UIFont.lato(fontSize: 14))
       textField.setRegularity()
       return textField
    }()
    
    lazy var relashionshipPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()

    lazy var relashionship: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 125
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Relashionship", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()

    lazy var sectorPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()

    lazy var sector: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 125
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Sector", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()

    lazy var udidCheckBox : SCRightCheckBoxWithLabel = {
        let checkBox = SCRightCheckBoxWithLabel()
        checkBox.caption.setup(textInfo: "_CHB_IAuthorize".localized, textColor: .white, font: UIFont.lato(fontSize: 12))
        checkBox.caption.setText(textContent: "_CHB_Security".localized, textColor: .white, font: UIFont.lato(fontSize: 12))
        checkBox.caption.finilize()
        return checkBox
    }()
    
    lazy var shaktiNetwork : SCRightCheckBoxWithLabel = {
        let checkBox = SCRightCheckBoxWithLabel()
        checkBox.caption.setup(textInfo: "_CHB_IConsent".localized, textColor: .white, font: .lato(fontSize: 12))
        checkBox.caption.finilize()
        return checkBox
    }()
    
    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("Cancel".localized, for: .normal)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    lazy var nextButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Update Additional Info", for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        button.tag = 4
        return button
    }()
    
    let corporateNameTT = SCToolTip()
    let emailAddressTT = SCToolTip()
    let relashioshipTT = SCToolTip()
    let sectorTT = SCToolTip()
    
    // MARK: - Methods
    func setupToolTips() {
        corporateName.setToolTip(scTT: corporateNameTT)
        emailAddress.setToolTip(scTT: emailAddressTT)
        relashionship.setToolTip(scTT: relashioshipTT)
        sector.setToolTip(scTT: sectorTT)
    }

    func setupViews() {
        
        addSubview(subTitle)
        subTitle.Title = "Affiliated companies"
        
        _ = subTitle.anchor(topAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = subTitle.centralizeX(centerXAnchor)
        
        addSubview(corporateNamePanel)
        
        _ = corporateNamePanel.anchor(subTitle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = corporateNamePanel.centralizeX(centerXAnchor)
        
        corporateNamePanel.ContainerView.addSubview(corporateName)
        corporateName.parentPanel = corporateNamePanel
        Util.fullyAnchor(_parnetControl: corporateNamePanel.ContainerView, _childControl: corporateName)
        
        addSubview(emailAddressPanel)
        
        _ = emailAddressPanel.anchor(corporateNamePanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = emailAddressPanel.centralizeX(centerXAnchor)
        
        emailAddressPanel.ContainerView.addSubview(emailAddress)
        emailAddress.parentPanel = emailAddressPanel
        Util.fullyAnchor(_parnetControl: emailAddressPanel.ContainerView, _childControl: emailAddress)
         
        addSubview(relashionshipPanel)
        
        _ = relashionshipPanel.anchor(emailAddressPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = relashionshipPanel.centralizeX(centerXAnchor)
        
        relashionshipPanel.ContainerView.addSubview(relashionship)
        relashionship.parentPanel = relashionshipPanel
        Util.fullyAnchor(_parnetControl: relashionshipPanel.ContainerView, _childControl: relashionship)

        addSubview(sectorPanel)
        
        _ = sectorPanel.anchor(relashionshipPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = sectorPanel.centralizeX(centerXAnchor)
        
        sectorPanel.ContainerView.addSubview(sector)
        sector.parentPanel = sectorPanel
        Util.fullyAnchor(_parnetControl: sectorPanel.ContainerView, _childControl: sector)

        addSubview(udidCheckBox)
        addSubview(shaktiNetwork)
        
        _ = udidCheckBox.anchor(sectorPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = udidCheckBox.centralizeX(centerXAnchor)

        _ = shaktiNetwork.anchor(udidCheckBox.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = shaktiNetwork.centralizeX(centerXAnchor)
        
        udidCheckBox.setCaptionHeightConstant(height: 32)

        addSubview(nextButton)
        addSubview(cancelButton)
        
        _ = nextButton.anchor(shaktiNetwork.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 48, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 36)
        _ = nextButton.centralizeX(centerXAnchor)
        
        _ = cancelButton.anchor(nextButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = cancelButton.centralizeX(centerXAnchor)

    }
    
    func finalizeMe() {
        subTitle.update()
        corporateNamePanel.setup(label: "Corporate Name")
        emailAddressPanel.setup(label: "Email Address")
        relashionshipPanel.setup(label: "Type of relationship")
        sectorPanel.setup(label: "Sector")
    }
    
    func validate() -> Bool {
        return true
        var isValid: Bool = true
        
        if corporateName.isEmpty() {
            addSubview(corporateNameTT)
            corporateNameTT.setRelativeXControl(uiControl: corporateName)
            corporateNameTT.setMessage(message: "Please fill corporate name")
            isValid = false
        }
        
        return isValid

    }
    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupToolTips()
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
