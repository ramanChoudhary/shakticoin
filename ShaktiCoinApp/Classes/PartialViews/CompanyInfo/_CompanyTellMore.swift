//
//  _CompanyTellMore.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 3/1/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import UIKit

class _CompanyTellMore: UIView {

    // MARK: - Declarations
    
    lazy var subTitle : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()

    lazy var positionPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var position: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 125
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Title / Position", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()

    lazy var emailAddressPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var emailAddress: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 110
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Email Address", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var phoneNumberPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var phoneNumber: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 110
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Phone Number", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var lastNamePanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var lastName: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 110
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Employee Last Name", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var walletIdPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var walletId: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 110
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Employee WalletID", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()

    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("Cancel".localized, for: .normal)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    lazy var nextButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Next".localized, for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        button.tag = 3
        return button
    }()
    
    let positionTT = SCToolTip()
    let emailAddressTT = SCToolTip()
    let phoneNumberTT = SCToolTip()
    let lastNameTT = SCToolTip()
    let walletIdTT = SCToolTip()
    
    // MARK: - Methods
    func setupToolTips() {
        position.setToolTip(scTT: positionTT)
        emailAddress.setToolTip(scTT: emailAddressTT)
        phoneNumber.setToolTip(scTT: phoneNumberTT)
        lastName.setToolTip(scTT: lastNameTT)
        walletId.setToolTip(scTT: walletIdTT)
    }

    func setupViews() {
        
        addSubview(subTitle)
        subTitle.Title = "Can you tell us more?"
        
        _ = subTitle.anchor(topAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = subTitle.centralizeX(centerXAnchor)
        
        addSubview(positionPanel)
        
        _ = positionPanel.anchor(subTitle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = positionPanel.centralizeX(centerXAnchor)
        
        positionPanel.ContainerView.addSubview(position)
        position.parentPanel = positionPanel
        Util.fullyAnchor(_parnetControl: positionPanel.ContainerView, _childControl: position)
        
        addSubview(emailAddressPanel)
        
        _ = emailAddressPanel.anchor(positionPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = emailAddressPanel.centralizeX(centerXAnchor)
        
        emailAddressPanel.ContainerView.addSubview(emailAddress)
        emailAddress.parentPanel = emailAddressPanel
        Util.fullyAnchor(_parnetControl: emailAddressPanel.ContainerView, _childControl: emailAddress)
        
        addSubview(phoneNumberPanel)
        
        _ = phoneNumberPanel.anchor(emailAddressPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = phoneNumberPanel.centralizeX(centerXAnchor)
        
        phoneNumberPanel.ContainerView.addSubview(phoneNumber)
        phoneNumber.parentPanel = phoneNumberPanel
        Util.fullyAnchor(_parnetControl: phoneNumberPanel.ContainerView, _childControl: phoneNumber)
        
        addSubview(lastNamePanel)
        
        _ = lastNamePanel.anchor(phoneNumberPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = lastNamePanel.centralizeX(centerXAnchor)
        
        lastNamePanel.ContainerView.addSubview(lastName)
        lastName.parentPanel = lastNamePanel
        Util.fullyAnchor(_parnetControl: lastNamePanel.ContainerView, _childControl: lastName)

        addSubview(walletIdPanel)
        
        _ = walletIdPanel.anchor(lastNamePanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = walletIdPanel.centralizeX(centerXAnchor)
        
        walletIdPanel.ContainerView.addSubview(walletId)
        walletId.parentPanel = walletIdPanel
        Util.fullyAnchor(_parnetControl: walletIdPanel.ContainerView, _childControl: walletId)
        
        addSubview(nextButton)
        addSubview(cancelButton)
        
        _ = nextButton.anchor(walletIdPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 27, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 36)
        _ = nextButton.centralizeX(centerXAnchor)

        _ = cancelButton.anchor(nextButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = cancelButton.centralizeX(centerXAnchor)
    }
    
    func finalizeMe() {
        subTitle.update()
        positionPanel.setup(label: "Title / Position")
        emailAddressPanel.setup(label: "Email Address")
        phoneNumberPanel.setup(label: "Phone Number")
        lastNamePanel.setup(label: "Employee Last Name")
        walletIdPanel.setup(label: "Employee WalletID")
    }
    
    func validate() -> Bool {
        return true
        var isValid: Bool = true
        return isValid
    }
    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setupToolTips()
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


}
