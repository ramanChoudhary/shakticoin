//
//  Contact.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/14/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import Foundation

struct Contact {
    var profilePicture : String
    var fullName : String
    var email : String
    var mobile : String
}
