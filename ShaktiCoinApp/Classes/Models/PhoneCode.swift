//
//  PhoneCode.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 1/19/19.
//  Copyright © 2019 Garnik Giloyan. All rights reserved.
//

import Foundation

struct PhoneCode : Decodable {
    let name:String
    let dial_code: String
    let code: String
}
