//
//  Country.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/19/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import Foundation

struct CountrySH : Decodable {
    var code : String
    var country : String
}
