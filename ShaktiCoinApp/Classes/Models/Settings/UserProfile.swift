//
//  UserProfile.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 1/9/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import Foundation

struct UserProfile {
    var index: Int
    var rowTitle: String
    var rowValue: String
    var rowHeight: Int
}
