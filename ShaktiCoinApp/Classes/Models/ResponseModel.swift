//
//  ResponseModel.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

struct ResponseModel: MapRepresentable {
    var responseCode: Int
    var responseMsg: String
    
    init(map: Map) throws {
        responseCode = try map.get("responseCode")
        responseMsg = try map.get("responseMsg")
    }
}
