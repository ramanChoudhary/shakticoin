//
//  EffortRate.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/13/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import Foundation

struct EffortRate {
    var index : Int
    var icon: String
    var converted: Int
    var progressing: Int
    var influenced: Int
}

