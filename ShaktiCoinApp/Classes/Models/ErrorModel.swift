//
//  ErrorModel.swift
//  ShaktiCoinApp
//
//  Created by sperev on 13/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

protocol ErrorModelProtocol {
    var string: String { get }
}

extension ErrorModelProtocol {
    func isEqual(_ lhs: ErrorModelProtocol) -> Bool {
        return type(of: lhs) == type(of: self) && lhs.string == self.string
    }
}

extension ErrorModelProtocol where Self: RawRepresentable, Self.RawValue == String {
    var string: String {
        return self.rawValue
    }
}

struct ErrorModel: LocalizedError, Equatable {
    let value: ErrorModelProtocol

    init(_ value: ErrorModelProtocol) {
        self.value = value
    }

    static func == (_ lhs: ErrorModel, _ rhs: ErrorModel) -> Bool {
        return lhs.value.isEqual(rhs.value)
    }
    
    var errorDescription: String? {
        return value.string.localized
    }
}
