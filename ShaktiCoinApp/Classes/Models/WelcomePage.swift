//
//  WelcomePage.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 10/22/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import Foundation

struct WelcomePage {
    let index: Int
    let title: String
    let description: String
    let image: String
    let buttonTitle: String?
}
