//
//  BonusModel.swift
//  ShaktiCoinApp
//
//  Created by sperev on 16/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

struct BonusModel: MapRepresentable {
    var bonusId: String
    var expiryDate: String
    var shaktiCoins: Int
    var dollarAmount: Int
    var expired: Bool
    init(map: Map) throws {
        bonusId = try map.get("id")
        expiryDate = try map.get("expiryDate")
        shaktiCoins = try map.get("shaktiCoins")
        dollarAmount = try map.get("dollarAmount")
        expired = try map.get("expired")
    }
}
