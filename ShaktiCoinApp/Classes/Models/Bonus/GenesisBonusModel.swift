//
//  GenesisBonusModel.swift
//  ShaktiCoinApp
//
//  Created by sperev on 22/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

struct GenesisBonusModel: MapRepresentable {
    var bonusId: String
    var shaktiID: String
    var walletType: String
    var walletOwnerAnniversary: String
    var promisedGenisisBounty: Int
    var lockedGenesisBounty: Int
    var remainingMonths: Int
    var successfulReferralCount: Int
    var offerId: String
    
    init(map: Map) throws {
        bonusId = try map.get("id")
        shaktiID = try map.get("shaktiID")
        walletType = try map.get("walletType")
        walletOwnerAnniversary = try map.get("walletOwnerAnniversary")
        promisedGenisisBounty = try map.get("promisedGenisisBounty")
        lockedGenesisBounty = try map.get("lockedGenesisBounty")
        remainingMonths = try map.get("remainingMonths")
        successfulReferralCount = try map.get("successfulReferralCount")
        offerId = try map.get("offerID")
    }
    
    var json: [String : Any] {
        var data: JSONObject = [:]
        data["id"] = bonusId
        data["shaktiID"] = shaktiID
        data["walletType"] = walletType
        data["walletOwnerAnniversary"] = walletOwnerAnniversary
        data["promisedGenisisBounty"] = promisedGenisisBounty
        data["lockedGenesisBounty"] = lockedGenesisBounty
        data["remainingMonths"] = remainingMonths
        data["successfulReferralCount"] = successfulReferralCount
        data["offerID"] = offerId
        return data
    }
}
