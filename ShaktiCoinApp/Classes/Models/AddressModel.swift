//
//  AddressModel.swift
//  ShaktiCoinApp
//
//  Created by sperev on 13/10/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

struct AddressModel: MapRepresentable {
    var country: String
    var countryCode: String
    var province: String
    var city: String
    var street: String
    var zipCode: String

    init(country: String, countryCode: String, province: String, city: String, street: String, zipCode: String) {
        self.country = country
        self.countryCode = countryCode
        self.province = province
        self.city = city
        self.street = street
        self.zipCode = zipCode
    }
    
    init(map: Map) throws {
        country = try map.get("country")
        countryCode = try map.get("countryCode")
        province = try map.get("province")
        city = try map.get("city")
        street = try map.get("street")
        zipCode = try map.get("zipCode")
    }
}
