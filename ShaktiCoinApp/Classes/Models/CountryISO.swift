//
//  CountryISO.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 1/9/19.
//  Copyright © 2019 Garnik Giloyan. All rights reserved.
//

import Foundation

struct CountryISO:Decodable {
    let Country:String
    let Format:String
    let ISO:String
    let Note: String
    let Regex: String
}
