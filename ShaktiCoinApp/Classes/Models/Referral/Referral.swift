//
//  Referral.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 2/9/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import Foundation

struct Referral: Codable {
    var firstName, lastName, email: String
    var phone: Int
    var referrer: String
    var media: String?
    var createdDate : String?
    var socialMedia : String?
    var status: String?
}

struct ListOfReferrals: Codable {
    let referrals: [Referral]
    let referralsCount: Int
}

