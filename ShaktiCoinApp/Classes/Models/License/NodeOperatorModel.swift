//
//  NodeOperatorModel.swift
//  ShaktiCoinApp
//
//  Created by sperev on 13/10/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

struct NodeOperatorModel: MapRepresentable {
    var walletId: String?
    var bizVaultId: String?
    var brandId: String?
    var email: String?
    var mobileNumber: String?
    var address: AddressModel?
    var subscribedLicenses: [PurchasedLicenseModel] = []
    var occupiedLicenses: [PurchasedLicenseModel] = []
    var activeLicense: PurchasedLicenseModel?
    
    init(map: Map) throws {
        walletId = try? map.get("walletID")
        bizVaultId = try? map.get("bizVaultID")
        email = try? map.get("email")
        mobileNumber = try? map.get("mobileNumber")
        address = try? map.get("address")
        subscribedLicenses = try map.get("subscribedLicenses")
        occupiedLicenses = try map.get("occupiedLicenses")
        activeLicense = try? map.get("activeLicense")
    }
    
    static func json(_ id: Int) -> JSONObject {
        return [
            "walletID": "walletID\(id)",
            "bizVaultID": "bizVaultID\(id)",
            "email": "email\(id)",
            "mobileNumber": "66666666\(id)",
            "address": "Barcelona 0802\(id)",
            "subscribedLicenses": [PurchasedLicenseModel.json(1), PurchasedLicenseModel.json(2), PurchasedLicenseModel.json(3)],
            "occupiedLicenses": [PurchasedLicenseModel.json(2)],
            "activeLicense": PurchasedLicenseModel.json(1)
        ]
    }
    
    static func mock(_ id: Int) -> NodeOperatorModel {
        return try! Mapper<NodeOperatorModel>.map(object: json(id))
    }
}
