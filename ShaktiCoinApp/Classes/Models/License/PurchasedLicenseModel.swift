//
//  SubscribedLicenseModel.swift
//  ShaktiCoinApp
//
//  Created by sperev on 13/10/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum SubscribtionAction: String {
    case initiated = "INITIATED"
    case joined = "JOINED"
    case upgraded = "UPGRADED"
    case downgraded = "DOWNGRADED"
    case renewed = "RENEWED"
    case cancelled = "CANCELLED"
    case expired = "EXPIRED"
}

enum PaymentStatus: String {
    case initiated = "INITIATED"
    case success = "SUCCESS"
    case failed = "FAILED"
}

struct PurchasedLicenseModel: MapRepresentable {
    var subscriptionId: String
    var planCode: String
    var action: SubscribtionAction
    var active: Bool
    var paymentStatus: PaymentStatus
    var dateOfPurchase: Int?
    var licensePurchasedEmail: String?

    init(map: Map) throws {
        subscriptionId = try map.get("subscriptionId")
        planCode = try map.get("planCode")
        action = try map.get("action")
        active = try map.get("active")
        paymentStatus = try map.get("paymentStatus")
        dateOfPurchase = try? map.get("dateOfPurchase")
        licensePurchasedEmail = try? map.get("licensePurchasedEmail")
    }
    
    static func json(_ id: Int) -> JSONObject {
        return [
            "subscriptionId": "subscriptionId\(id)",
            "planCode": "planCode",
            "action": "INITIATED",
            "active": true,
            "paymentStatus": "SUCCESS",
            "dateOfPurchase": 1245120391,
            "licensePurchasedEmail": "licensePurchasedEmail\(id)"
        ]
    }
    
    static func mock(_ id: Int) -> PurchasedLicenseModel {
        return try! Mapper<PurchasedLicenseModel>.map(object: json(id))
    }
}
