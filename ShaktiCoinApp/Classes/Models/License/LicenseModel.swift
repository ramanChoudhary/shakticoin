//
//  LicenseModel.swift
//  ShaktiCoinApp
//
//  Created by sperev on 13/10/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum LicenseModeType: String {
    case weekly = "W"
    case monthly = "M"
    case yearly = "Y"
}

struct LicenseModel: MapRepresentable {
    struct Save: MapRepresentable {
        var monthly: Int
        var yearly: Int
        var weekly: Int
        init(map: Map) throws {
            monthly = try map.get("monthly")
            yearly = try map.get("yearly")
            weekly = try map.get("weekly")
        }
    }
    
    var licenseId: String
    var planCode: String
    var bonus: String
    var licFeatures: [String]
    var orderNumber: Int
    //var lastModifiedBy: Any?
    var licName: String
    var licCategory: String
    var lastModification: Int
    //var creator: Any?
    var modeType: LicenseModeType
    var planType: String
    var cycle: Int
    var price: Double
    var creationDate: Double
    var save: Save
    
    init(map: Map) throws {
        licenseId = try map.get("id")
        planCode = try map.get("planCode")
        bonus = try map.get("bonus")
        licFeatures = try map.get("licFeatures")
        orderNumber = try map.get("orderNumber")
        //var lastModifiedBy: Any?
        licName = try map.get("licName")
        licCategory = try map.get("licCategory")
        lastModification = try map.get("lastModification")
        //var creator: Any?
        modeType = try map.get("modeType")
        planType = try map.get("planType")
        cycle = try map.get("cycle")
        price = try map.get("price")
        creationDate = try map.get("creationDate")
        save = try map.get("save")
    }
    
    static func json(_ index: Int, prefix: String, modeType: LicenseModeType) -> JSONObject {
        return [
            "id": "Plan \(index) ID",
            "planCode": "Plan \(index) Code",
            "bonus": "Plan \(index) Bonus",
            "licFeatures": [
                "Plan \(index) Feature 1",
                "Plan \(index) Feature 2"
            ],
            "orderNumber": index,
            "additionalPay": index * 10,
            "licName": "Plan \(index) Lic Name",
            "licCategory": "Plan \(index) Lic Category",
            "lastModification": index+1,
            "modeType": modeType.rawValue,
            "planType": "\(modeType.rawValue)\(index)PlanType",
            "cycle": index,
            "price": Double(index) * 0.99,
            "creationDate": Double(index)
        ]
    }
    
    static func mock(_ id: Int, prefix: String, modeType: LicenseModeType) -> LicenseModel {
        return try! Mapper<LicenseModel>.map(object: json(id, prefix: prefix, modeType: modeType))
    }
}
