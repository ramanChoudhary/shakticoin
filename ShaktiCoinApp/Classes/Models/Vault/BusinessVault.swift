//
//  BusinessVault.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 2/1/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import Foundation

// MARK: - WelcomeElement
struct BusinessVault: Decodable {
    let id: Int
    let name, description: String
    let features: [String]
    let bonus: BVBonus?
    let transition_to_view, transition_button_label: String
    let order_no: Int
}

// MARK: - Bonus
struct BVBonus: Decodable {
    let id: Int
    let expiry_date: String
    let description: String
    let fiat_bonus, sxe_bonus: Int
}
