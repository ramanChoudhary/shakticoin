//
//  OnboardApiErrorModel.swift
//  ShaktiCoinApp
//
//  Created by sperev on 17/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

struct OnboardApiErrorModel: MapRepresentable {
    var message: String
    var details: String
    
    var status: Int?
    var path: String?
    var timestamp: String?
    var error: String?
    
    init(map: Map) throws {
        message = try map.get("message")
        details = try map.get("details")
        status = try? map.get("status")
        timestamp = try? map.get("timestamp")
        path = try? map.get("path")
        error = try? map.get("error")
    }
    
}
