//
//  CompanyDetail.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 3/5/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import Foundation

struct CompanyDetail {
    let field: String
    let value: String
}
