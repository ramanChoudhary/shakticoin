//
//  DataModel.swift
//  ShaktiCoinApp
//
//  Created by sperev on 22/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

struct DataModel<T: MapRepresentable>: MapRepresentable {
    var data: T
    var message: String?
    init(map: Map) throws {
        data = try map.get("data")
        message = try? map.get("message")
    }
}
