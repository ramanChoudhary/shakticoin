//
//  DocumentItem.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/6/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import Foundation

struct DocumentItem {
    var caption: String
    var icon: String
}
