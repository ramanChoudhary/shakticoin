//
//  Setting.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/16/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import Foundation

struct Setting {
    var leftText : String
    var rightText : String
    var isSwitch : Bool
    var isDisabled : Bool
}
