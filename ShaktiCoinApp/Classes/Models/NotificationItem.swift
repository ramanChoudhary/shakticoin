//
//  NotificationItem.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/2/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import Foundation

struct NotificationItem {
    let title: String
    let message: String
    let date: String
    let isRead: Bool?
}
