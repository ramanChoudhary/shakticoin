//
//  TokenModel.swift
//  ShaktiCoinApp
//
//  Created by sperev on 13/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

struct TokenModel: MapRepresentable {
    let accessToken, refreshToken, tokenType: String
    let expiresIn: Int

    init(map: Map) throws {
        accessToken = try map.get("access_token")
        refreshToken = try map.get("refresh_token")
        let type: String = try map.get("token_type")
        tokenType = type.capitalized
        expiresIn = try map.get("expires_in")
    }
    
    var json: [String : Any] {
        var data: JSONObject = [:]
        data["access_token"] = accessToken
        data["refresh_token"] = refreshToken
        data["token_type"] = tokenType
        data["expires_in"] = expiresIn
        return data
    }
}
