//
//  DataArrayModel.swift
//  ShaktiCoinApp
//
//  Created by sperev on 22/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

struct DataArrayModel<T: MapRepresentable>: MapRepresentable {
    var data: [T]
    init(map: Map) throws {
        data = try map.get("data")
    }
}
