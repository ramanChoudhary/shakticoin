//
//  UserToken.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 1/5/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import Foundation

struct UserToken : Codable {
    var refresh: String
    var access: String
}
