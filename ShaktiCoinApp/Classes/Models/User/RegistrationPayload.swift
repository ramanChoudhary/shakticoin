//
//  RegistrationPayload.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/30/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import Foundation

struct UserCitizenship : Codable {
    var country_code : String
    var country_name : String
}

struct UserResidence : Codable {
    var address_line_1 : String
    var city : String
    var zip_code : String
    var country_code : String
    var country_name : String
    var subdivision_id : Int = 0
    var subdivision_name : String = "US"
}

struct UserRegistration : Codable {
    
    var email, password, first_name, middle_name: String
    var last_name, mobile : String
    var citizenship: [UserCitizenship]?
    var residence: [UserResidence]?
}


