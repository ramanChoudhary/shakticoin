//
//  AlertModel.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

struct AlertModel {
    var title: String
    var message: String
    var style: UIAlertController.Style
    var actions: [UIAlertAction]
}
