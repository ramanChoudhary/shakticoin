//
//  AppDelegate.swift
//  ShatkiCoinApp
//
//  Created by Garnik Giloyan on 10/20/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SideMenu
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    static var shared: AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()

        LanguageManager.shared().SetDefaultLanguage(langauge: .English)

        IQKeyboardManager.shared.enable = true

        //showPowerMiner()
        
        //showPaymentOption()

        showRoomController()

        return true
    }

    private func showPowerMiner() {
        window?.rootViewController = PowerMinerRouter.createModule()
    }
    
    private func showRoomController() {
        
        let controller = BusinessVaultRouter.createBusinessVaultModule()
        let navigationController = UINavigationController(rootViewController: controller)
        navigationController.navigationBar.isHidden = true
        window?.rootViewController = navigationController
//        if AuthorizationDataManager.shared.isAuthorized == true {
//            let wallet = MyWalletRouter.createMyWalletModule()
//            window?.rootViewController = SideMenuNavigationController(rootViewController: wallet)
//        } else if UserDefaults.standard.bool(forKey: "welcome_shown") == true {
//            window?.rootViewController = LoginViewController()
//        } else {
//            UserDefaults.standard.set(true, forKey: "welcome_shown")
//            window?.rootViewController = WelcomeViewController()
//        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Senst when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        if url.scheme == "shaktiapp" {
            perform(url)
        }
        return true
    }

    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb,
            let url = userActivity.webpageURL {
            perform(url)
        }
        return true
    }


    private func perform(_ url: URL) {
        guard let deeplink = Deeplink(with: url),
            let route = DeeplinkHelper.route(deeplink) else { return }
        perform(route)
    }

    func perform(_ route: Route) {
        RoutePerformer(rootViewController: window?.rootViewController).perform(route: route)
    }
}
