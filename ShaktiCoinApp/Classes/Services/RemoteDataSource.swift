//
//  ApiDataManager.swift
//  ShaktiCoinApp
//
//  Created by sperev on 13/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation
import KeychainAccess

enum APIRequestType {
    case json
    case multiPart(boundary: String)
    case custom(contentType: String)
    case formUrlencoded

    var headerValue: String {
        switch self {
        case .json: return "application/json; charset=utf-8"
        case .formUrlencoded: return "application/x-www-form-urlencoded; charset=utf-8"
        case .multiPart(let boundary): return "multipart/form-data; boundary=\(boundary)"
        case .custom(let contentType): return contentType
        }
    }
}

struct APIRequest {
    let endpoint: EndpointProtocol
    let parameters: Any?
    weak var delegate: URLSessionTaskDelegate?
    let headers: [String: String]?

    init(endpoint: EndpointProtocol, headers: [String: String]? = nil, parameters: Any? = nil, delegate: URLSessionTaskDelegate? = nil) {
        self.endpoint = endpoint
        self.parameters = parameters
        self.delegate = delegate
        self.headers = headers
    }
}

protocol APIDataManagerProtocol: class {
    func makeAPIRequest(_ request: APIRequest, result: @escaping APIDataManagerObjectResultBlock)
    func makeAPIRequest(_ request: APIRequest, result: @escaping APIDataManagerResultBlock)
    func reset()
}

private class APIDataManagerDefault: APIDataManagerProtocol {
    private var appKey: String {
        switch Environment.current {
        case .staging:
            return "MTY0MjA3N2QtM2ExYi00MTBlLTk5N2UtMDQxYTg4ODhkMDJhOnFLY0RKdyZoNEEzSQ=="
        default:
            return "NmFmYzM0MjEtMWRjNC00MjE3LWE3ODYtOGE0MTNkZGUzMmQ2OmdpQVpxSUVlaTdzOFBsaEtBcmdBc0FUcllicFladGw3cjd1U1pnMDk="
        }
    }
    var authorizationManager = AuthorizationDataManager.shared
    
    // MARK: - APIDataManagerProtocol implementation

    func makeAPIRequest(_ request: APIRequest, result: @escaping APIDataManagerObjectResultBlock) {
        makeAPICall(request, result)
    }

    func makeAPIRequest(_ request: APIRequest, result: @escaping APIDataManagerResultBlock) {
        makeAPICall(request, result)
    }

    func reset() {
        isTreatingUnauthorised = false
        authorizationManager.reset()
        
        removePendingCalls()
        
        ViewDispatcher.shared.execute {
            let delegate = UIApplication.shared.delegate as? AppDelegate
            delegate?.window?.rootViewController = LoginViewController()
        }
        
        BountyBonusLocalDataManager.shared.reset()
    }

    // MARK: - Private stuff

    private var isTreatingUnauthorised: Bool = false
    private var pendingObjectCalls: [(APIRequest, APIDataManagerObjectResultBlock)] = []
    private var pendingArrayCalls: [(APIRequest, APIDataManagerResultBlock)] = []

    private func makeAPICall<T>(_ apiRequest: APIRequest, refresh: Bool = false,  _ resultBlock: ((Result<T, ErrorModel>) -> Void)?) {
        
        // Saving the request for later in case we are unauthorised
        if !apiRequest.endpoint.isAuthorizingCall &&
            isTreatingUnauthorised {
            addPendingCall(apiRequest, resultBlock: resultBlock)
            return
        }

        let request = createRequest(for: apiRequest)
        if LocalFlagManager.shared.value(.logApiCalls) {
            print(request)
            
            if let heads = apiRequest.headers {
                print(heads)
            }
            
            if let params = apiRequest.parameters {
                print(params)
            }
        }
        
        let session = URLSession(configuration: .default, delegate: apiRequest.delegate, delegateQueue: nil)
        let task = session.dataTask(with: request) { [weak self] data, response, _ in
            if let httpResponse = response as? HTTPURLResponse {
                if LocalFlagManager.shared.value(.logApiCalls) {
                    print("\(httpResponse.statusCode): \(httpResponse.url?.absoluteString ?? "")")
                }
                let error = APIErrorBuilder.build(data: data)
                switch (httpResponse.statusCode, error.value) {
                case (200 ... 299, _):
                    self?.treatSuccess(with: data, authorization: apiRequest.endpoint.isAuthorizingCall, and: resultBlock)
                case (401, _):
                    if refresh {
                        self?.reset()
                    } else if apiRequest.endpoint.isAuthorizingCall {
                        resultBlock?(.failure(error))
                    } else {
                        self?.addPendingCall(apiRequest, resultBlock: resultBlock)
                        self?.treatUnauthorised()
                    }
                default:
                    resultBlock?(.failure(error))
                }
            } else {
                if LocalFlagManager.shared.value(.logApiCalls) {
                    print(ShaktiError.API.timeout.string)
                }
                resultBlock?(.failure(ErrorModel(ShaktiError.API.timeout)))
            }
        }
        task.resume()
    }

    private func createRequest(for apiRequest: APIRequest) -> URLRequest {

        let url = URL(string: "\(apiRequest.endpoint.baseUrl)\(apiRequest.endpoint.value)")!
        var request = URLRequest(url: url)
        if apiRequest.endpoint.isAuthorizingCall {
            request.addValue("Basic \(appKey)", forHTTPHeaderField: "Authorization")
        } else if apiRequest.endpoint.authenticated {
            let token = authorizationManager.authorizationToken
            if LocalFlagManager.shared.value(.logApiCalls) {
                print(token)
            }
            request.addValue(token, forHTTPHeaderField: "Authorization")
        }
        
        request.httpMethod = apiRequest.endpoint.method.rawValue
        request.setValue(apiRequest.endpoint.contentType.headerValue, forHTTPHeaderField: "Content-Type")
        if let parameters = apiRequest.parameters {
            switch apiRequest.endpoint.method {
            case .post, .put, .delete:
                if case APIRequestType.formUrlencoded = apiRequest.endpoint.contentType,
                    let dict = parameters as? JSONObject {
                    request.httpBody = dict.stringFromHttpParameters().data(using: .utf8)
                } else if let data = parameters as? Data {
                    request.httpBody = data
                } else if let dict = parameters as? [String: Any] {
                    request.httpBody = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
                }
            case .get:
                if let dict = parameters as? [String: Any] {
                    request.url = URL(string: "\(apiRequest.endpoint.baseUrl)\(apiRequest.endpoint.value)?\(dict.stringFromHttpParameters())")!
                }
            default: break
            }
        }

        for (key, value) in apiRequest.headers ?? [:] {
            request.addValue(value, forHTTPHeaderField: key)
        }

        return request
    }

    private func treatSuccess<T>(with data: Data?, authorization: Bool, and resultBlock: (((Result<T, ErrorModel>) -> Void))?) {
        if let data = data,
            let dict = (try? JSONSerialization.jsonObject(with: data, options: [])) as? T {
            
            if let json = dict as? JSONObject,
                let token = try? Mapper<TokenModel>.map(object: json) {
                authorizationManager.set(token: token)
            }
            
            if LocalFlagManager.shared.value(.logApiCalls) {
                print(dict)
            }
            
            resultBlock?(.success(dict))
        } else if let emptyDict = [String: Any]() as? T {
            if LocalFlagManager.shared.value(.logApiCalls) {
                print(emptyDict)
            }
            
            resultBlock?(.success(emptyDict))
        } else if let emptyArray = [[String: Any]]() as? T {
            if LocalFlagManager.shared.value(.logApiCalls) {
                print(emptyArray)
            }
            resultBlock?(.success(emptyArray))
        }
    }

    private func treatUnauthorised() {
        let token = authorizationManager.refreshToken
        guard !isTreatingUnauthorised else { return }
        isTreatingUnauthorised = true
        guard !token.isEmpty else {
            reset()
            return
        }

        refresh(token: token) {
            switch $0 {
            case .success:
                self.isTreatingUnauthorised = false
                self.handlePendingCalls()
            case .failure:
                self.reset()
            }
        }
    }

    private func addPendingCall<T>(_ apiRequest: APIRequest, resultBlock: ((Result<T, ErrorModel>) -> Void)?) {
        if let resultBlock = resultBlock as? APIDataManagerObjectResultBlock {
            pendingObjectCalls.append((apiRequest, resultBlock))
        } else if let resultBlock = resultBlock as? APIDataManagerResultBlock {
            pendingArrayCalls.append((apiRequest, resultBlock))
        }
    }

    private func handlePendingCalls() {
        for (request, resultBlock) in pendingObjectCalls {
            makeAPIRequest(request, result: resultBlock)
        }

        for (request, resultBlock) in pendingArrayCalls {
            makeAPIRequest(request, result: resultBlock)
        }
        removePendingCalls()
    }

    private func removePendingCalls() {
        pendingObjectCalls.removeAll()
        pendingArrayCalls.removeAll()
    }
    
    private func refresh(token: String, callBack: @escaping(Result<JSONObject, ErrorModel>) -> Void) {
        
        let params = [
            "grant_type": "refresh_token",
            "refresh_token": token
        ]

        let request = APIRequest(endpoint: Endpoint.IAMEndpoint.token, parameters: params)

        makeAPICall(request, callBack)
    }
}

class RemoteDataSource {

    static let shared = RemoteDataSource()
    var apiDataManager: APIDataManagerProtocol = APIDataManagerDefault()

    // MARK: - Public methods

    func requestObject(forEndpoint endpoint: EndpointProtocol, headers: [String: String]? = nil, parameters: [String: Any]? = nil, result: @escaping APIDataManagerObjectResultBlock) {
        apiDataManager.makeAPIRequest(APIRequest(endpoint: endpoint, headers: headers, parameters: parameters), result: result)
    }

    func requestArray(forEndpoint endpoint: EndpointProtocol, headers: [String: String]? = nil, parameters: [String: Any]? = nil, result: @escaping APIDataManagerResultBlock) {
        apiDataManager.makeAPIRequest(APIRequest(endpoint: endpoint, headers: headers, parameters: parameters), result: result)
    }

    func reset() {
        apiDataManager.reset()
    }
}
