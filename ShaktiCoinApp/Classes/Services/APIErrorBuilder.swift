//
//  RemoteErrorBuilder.swift
//  ShaktiCoinApp
//
//  Created by sperev on 13/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum ShaktiError {
    enum General: ErrorModelProtocol {
        case unknown
        case missingData
        case missingPhone
        case missingEmail
        case missingCountryCode
        case passwordsNotMatch
        case passwordTooWeak
        var string: String {
            switch self {
            case .unknown: return "error_unknown"
            case .missingData: return "error_missing_data"
            case .missingPhone: return "error_missing_phone"
            case .missingEmail: return "error_missing_email"
            case .missingCountryCode: return "error_missing_country_code"
            case .passwordsNotMatch: return "passwords_not_match"
            case .passwordTooWeak: return "password_too_weak"
            }
        }
    }
    
    enum API: ErrorModelProtocol {
        case json(JSONObject)
        case response(ResponseModel)
        case timeout
        case unknown
        var string: String {
            switch self {
            case .response(let model): return model.responseMsg
            case .timeout: return "api_error_timeout"
            case .json(let model):
                var error = ""
                for (key, value) in model {
                    if !error.isEmpty {
                        error += "\n"
                    }
                    error.append("\(key): \(String(describing: value))")
                }
                return error
            default: return "api_error_unknown"
            }
        }
    }
}

struct APIErrorBuilder {
    private static func errorFromData(_ data: Data) -> ShaktiError.API {
        guard let dataString = String(data: data, encoding: .utf8),
            let json = StringJSONUtils.convertStringToDictionary(text: dataString) as? JSONObject
            else { return .unknown }
        
        if LocalFlagManager.shared.value(.logApiCalls) {
            print(json)
        }
        
        if let model = try? Mapper<ResponseModel>.map(object: json) {
            return ShaktiError.API.response(model)
        }
        
        return ShaktiError.API.json(json)
    }

    static func build(data: Data?) -> ErrorModel {
        guard let data = data else { return ErrorModel(ShaktiError.General.unknown) }
        return ErrorModel(errorFromData(data))
    }
}
