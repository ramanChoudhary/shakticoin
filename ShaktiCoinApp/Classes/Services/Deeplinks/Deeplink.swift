//
//  Deeplink.swift
//  ShaktiCoinApp
//
//  Created by sperev on 27/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class Deeplink {
    var type: DeeplinkType = .unknown
    var value: Any?
    
    init?(with url: URL) {
        let urlString = url.absoluteString
        var found = false
        
        for link in DeeplinkType.allCases {
            for path in link.paths {
                if urlString.contains(path) {
                    found = true
                    break
                }
            }
            
            if found {
                self.type = link
                self.value = buildValue(for: urlString, with: link.parameters)
                break
            }
        }
        
        if !found {
            print("Route not found:\n\(urlString)")
            return nil
        }
    }
    
    private func buildValue(for urlString: String, with parameters: [String]) -> String {
        return urlRightPart(url: urlString, parameters: parameters)
    }
    
    private func urlRightPart(url: String, parameters: [String]) -> String {
        var rightPart = ""
        for param in parameters {
            let components = url.components(separatedBy: param)
            if components.count > 1, let halfComponent = components.last {
                rightPart = halfComponent.components(separatedBy: "&").first ?? ""
                break
            }
        }
        return rightPart
    }
}
