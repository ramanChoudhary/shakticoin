//
//  DeeplinkType.swift
//  ShaktiCoinApp
//
//  Created by sperev on 27/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum DeeplinkType: String, CaseIterable {
    case unknown
    case verifyEmail
    
    var paths: [String] {
        switch self {
        case .verifyEmail: return ["/registration/confirm-registration"]
        default: return []
        }
    }
    
    var parameters: [String] {
        switch self {
        case .verifyEmail: return ["token="]
        default: return []
        }
    }
}
