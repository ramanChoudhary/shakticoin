//
//  DeeplinkHelper.swift
//  ShaktiCoinApp
//
//  Created by sperev on 27/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class DeeplinkHelper {
    static func route(_ deeplink: Deeplink) -> Route? {
        switch deeplink.type {
        case .verifyEmail:
            if let token = deeplink.value as? String {
                return Route(type: .loggedOut, sections: [
                    RoutePath(action: .verifyEmail(token), navigationMode: .custom)
                ])
            } else {
                return nil
            }
        default: return nil
        }
    }
}
