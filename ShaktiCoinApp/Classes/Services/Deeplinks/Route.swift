//
//  Route.swift
//  ShaktiCoinApp
//
//  Created by sperev on 27/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

struct Route {
    let type: RouteType
    let sections: [RoutePath]
    
    static var myWallet: Route {
        return Route(type: .loggedIn, sections: [
            RoutePath(action: .myWallet, navigationMode: .side)
        ])
    }
    
    static var minerIntro: Route {
        return Route(type: .loggedIn, sections: [
            RoutePath(action: .minerIntro, navigationMode: .side)
        ])
    }
}

enum RouteType {
    case none
    case loggedIn
    case loggedOut
}

struct RoutePath {
    var action: RouteAction
    var navigationMode: NavigationMode
}

enum RouteAction: Equatable {
    case none
    case verifyEmail(String)
    case myWallet
    case minerIntro

    var viewController: UIViewController? {
        switch self {
        case .verifyEmail(let token):
            return OnboardingVerifyEmailWireframe.createModule(token)
        case .myWallet:
            return MyWalletRouter.createMyWalletModule()
        case .minerIntro:
            return ShaktiMinerIntroRouter.createModule()
        default:
            return nil
        }
    }
    
    var customAction: ((UIViewController) -> Void)? {
        return {
            switch self {
            case .verifyEmail(let token):
                if let viewController = $0 as? OnboardingVerifyEmailView {
                    viewController.sendEmailVerification(token)
                } else {
                    let delegate = UIApplication.shared.delegate as! AppDelegate
                    delegate.window?.rootViewController = self.viewController
                }
            default: break
            }
        }
    }
    
    var tabIndex: Int? {
        return nil
    }
}

enum NavigationMode {
    case modal
    case push
    case tab
    case custom
    case side
}

