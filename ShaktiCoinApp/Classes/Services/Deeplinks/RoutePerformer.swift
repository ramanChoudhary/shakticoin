//
//  RoutePerformer.swift
//  ShaktiCoinApp
//
//  Created by sperev on 27/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit
import SideMenu

struct RoutePerformer {
    var rootViewController: UIViewController?
    
    func perform(route: Route) {
        guard let rootViewController = rootViewController else { return }
        prepareHierachy(with: rootViewController, and: route)
    }
    
    func checkType(of rootViewController: UIViewController, against desiredType: AnyClass) -> Bool {
        if rootViewController.isKind(of: desiredType) {
            return true
        } else if let navigationController = rootViewController as? UINavigationController,
            navigationController.children.count > 0 &&
                navigationController.children[0].isKind(of: desiredType) {
            return true
        } else if let tabBarController = rootViewController as? UITabBarController,
            tabBarController.children[tabBarController.selectedIndex].isKind(of: desiredType) {
            return true
        }
        return false
    }
    
    func prepareHierachy(with rootViewController: UIViewController, and route: Route) {
        for viewController in (rootViewController as? UITabBarController)?.children ?? [] {
            (viewController as? UINavigationController)?.popToRootViewController(animated: false)
            viewController.navigationController?.popToRootViewController(animated: false)
        }
        rootViewController.navigationController?.popToRootViewController(animated: false)
        if rootViewController.presentedViewController != nil {
            rootViewController.dismiss(animated: false) {
                self.startRoute(with: rootViewController, and: route)
            }
        } else {
            startRoute(with: rootViewController, and: route)
        }
    }
    
    func startRoute(with fromViewController: UIViewController, and route: Route) {
        continueRoute(with: fromViewController, and: route.sections)
    }
    
    func continueRoute(with fromViewController: UIViewController, and route: [RoutePath]) {
        if let firstPath = route.first {
            let remainingRoute = Array(route.dropFirst())
            let nextActionBlock = { viewController in
                self.continueRoute(with: viewController, and: remainingRoute)
            }
            switch firstPath.navigationMode {
            case .tab:
                performTabNavigation(on: fromViewController, with: firstPath.action, and: nextActionBlock)
            case .push:
                performPushNavigation(on: fromViewController, with: firstPath.action, and: nextActionBlock)
            case .modal:
                performModalNavigation(on: fromViewController, with: firstPath.action, and: nextActionBlock)
            case .custom:
                performCustomNavigation(on: fromViewController, with: firstPath.action, and: nextActionBlock)
            case .side:
                performSideNavigation(on: fromViewController, with: firstPath.action, and: nextActionBlock)
            }
        }
    }
    
    func performTabNavigation(on viewController: UIViewController, with action: RouteAction, and completion: @escaping (UIViewController) -> Void) {
        if let tabBarController = viewController as? UITabBarController ?? viewController.tabBarController,
            let index = action.tabIndex,
            tabBarController.children.count > index {
            tabBarController.selectedIndex = index
            completion(viewController.children[index])
        }
    }
    
    func performPushNavigation(on viewController: UIViewController, with action: RouteAction, and completion: (UIViewController) -> Void) {
        if let navigationController = viewController as? UINavigationController ?? viewController.navigationController,
            let nextViewController = action.viewController {
            navigationController.pushViewController(nextViewController, animated: false)
            completion(viewController)
        }
    }
    
    func performModalNavigation(on viewController: UIViewController, with action: RouteAction, and completion: (UIViewController) -> Void) {
        if let nextViewController = action.viewController {
            viewController.present(nextViewController, animated: false, completion: nil)
            completion(nextViewController)
        }
    }
    
    func performCustomNavigation(on viewController: UIViewController, with action: RouteAction, and completion: (UIViewController) -> Void) {
        action.customAction?(viewController)
        completion(viewController)
    }
    
    func performSideNavigation(on viewController: UIViewController, with action: RouteAction, and completion: (UIViewController) -> Void) {
        if let nextViewController = action.viewController {
            let delegate = UIApplication.shared.delegate as! AppDelegate
            delegate.window?.rootViewController =  SideMenuNavigationController(rootViewController: nextViewController)
            completion(nextViewController)
        }
    }
}
