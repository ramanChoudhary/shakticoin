//
//  AccountService.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 12/7/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import Foundation
import UIKit

class AccountService {
    var dataSource = RemoteDataSource.shared
    // MARK: - Actions
    
    func login(username: String, password: String, callback: @escaping (ErrorModel?) -> Void) {
       
        let params = [
            "grant_type": "password",
            "scope": "openid + profile",
            "username": username,
            "password": password
        ]

        dataSource.requestObject(forEndpoint: Endpoint.IAMEndpoint.token, parameters: params) {
            switch $0 {
            case .success:
                callback(nil)
            case .failure(let error):
                callback(error)
            }
        }
    }
    
    func resetPassword(payload: [String: String], callBack: @escaping(_ statusCode: Int) -> ()) {
        
        var jsonBody: Data?
        do {
            jsonBody = try JSONEncoder().encode(payload)
        } catch {
            print(error)
        }
        let apiUrl = "http://userservice.test.shakticoin.com/userservice/api/v1"
        let endPoint = URL(string: apiUrl + "/users/password/reset/")
        let httpRequest = G2XHttpRequest(_httpMethod: .POST, _endPoint: endPoint!)
        httpRequest.setHeaders(_headers: ["Content-Type" : "application/json",
                                          "accept": "application/json"])

        httpRequest.setJsonData(_data: jsonBody!)
        
        httpRequest.runWithNothing { (statusCode) in
            callBack(statusCode)
        }
    }
}
