//
//  CurrentUserDataManager.swift
//  ShaktiCoinApp
//
//  Created by sperev on 08/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation
import KeychainAccess

final class CurrentUserDataManager {
    private let emailKey = "email"
    private let phoneKey = "phone"
    private let countryCodeKey = "country_code"
    private var service = Keychain(service: "current_user_data")
    
    static let shared = CurrentUserDataManager()
    
    // MARK: - Methods
    
    private func get(_ key: String) -> String? {
        return try? service.get(key)
    }
    
    private func set(_ value: String?, key: String) {
        if let value = value {
            try? service.set(value, key: key)
        } else {
            try? service.remove(key)
        }
    }
    
    var email: String? {
        get {
            return get(emailKey)
        }
        set {
            set(newValue, key: emailKey)
        }
    }
    
    var phone: String? {
        get {
            return get(phoneKey)
        }
        set {
            set(newValue, key: phoneKey)
        }
    }
    
    var countryCode: String? {
        get {
            return get(countryCodeKey)
        }
        set {
            set(newValue, key: countryCodeKey)
        }
    }
    
    func reset() {
        try? service.remove(emailKey)
        try? service.remove(phoneKey)
        try? service.remove(countryCodeKey)
    }
}
