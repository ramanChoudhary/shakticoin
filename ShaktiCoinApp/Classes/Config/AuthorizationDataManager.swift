//
//  AuthorizationDataManager.swift
//  ShaktiCoinApp
//
//  Created by sperev on 08/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation
import KeychainAccess

class AuthorizationDataManager {
    private let keychainKey = "authorization_tokens"
    private var service = Keychain(service: "authorization")
    
    static let shared = AuthorizationDataManager()
    
    // MARK: - Methods
    private var token: TokenModel?
    
    init() {
        do {
            if let data = try service.getData(keychainKey),
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? JSONObject  {
                token = try? Mapper<TokenModel>.map(object: json)
            }
        } catch let error {
            print(error)
        }
    }
    
    var isAuthorized: Bool {
        return !authorizationToken.isEmpty
    }
    
    var authorizationToken: String {
        guard let access = token?.accessToken,
            let type = token?.tokenType else { return "" }
        return "\(type.capitalized) \(access)"
    }
    
    var refreshToken: String {
        return token?.refreshToken ?? ""
    }
    
    func set(token: TokenModel) {
        self.token = token
        if let data = try? JSONSerialization.data(withJSONObject: token.json, options: .prettyPrinted) {
            do {
                try service.set(data, key: keychainKey)
            } catch let error {
                print(error)
            }
        }
    }
    
    func reset() {
        try? service.remove(keychainKey)
    }
}
