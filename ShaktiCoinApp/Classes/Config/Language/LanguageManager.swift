//
//  LanguageManager.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 12/3/19.
//  Copyright © 2019 Garnik Giloyan. All rights reserved.
//

import Foundation

/*
 English, French, Spanish, Portugese, Mandarin, Romanian, German, Russian, Japanese, south Korean(hanguk-eo) ...in the order based on crypto usage stats (edited)
*/

enum Langauge : Int {
    case English = 1
    case French = 2
    case Spanish = 3
}

class LanguageManager {
    
    // MARK: - Accessors
    
    private static var sharedInstance: LanguageManager = {
        let config = LanguageManager()
        return config
    }()
    
    class func shared() -> LanguageManager {
        return sharedInstance
    }

    // MARK: - Declarations
    
    private let defaultLng: String = "DefaultLng"

    // MARK: - Methods
    
    public func DefaultLanguage() -> Langauge {
        
        if let defaultLanguage = UserDefaults.standard.integer(forKey: defaultLng) as Int? {
            return Langauge(rawValue: defaultLanguage)!
        } else { return Langauge.English }
    }
    
    public func SetDefaultLanguage(langauge: Langauge) {
        let defaults = UserDefaults.standard
        defaults.set(langauge.rawValue, forKey: defaultLng)
        defaults.synchronize()
    }
}
