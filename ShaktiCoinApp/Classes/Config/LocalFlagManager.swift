//
//  LocalFlagManager.swift
//  ShaktiCoinApp
//
//  Created by sperev on 07/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum LocalFlag: String, CaseIterable {
    case example
    case logApiCalls = "log_api_calls"
}

class LocalFlagManager {

    static let shared = LocalFlagManager()

    func value(_ localFlag: LocalFlag) -> Bool {
        #if DEBUG
        return getDefaultsValue(localFlag)
        #else
        return false
        #endif
    }

    func setValue(_ value: Bool, localFlag: LocalFlag) {
        UserDefaults.standard.set(value, forKey: localFlag.rawValue)
    }

    private func getDefaultsValue(_ localFlag: LocalFlag) -> Bool {
        let value = UserDefaults.standard.bool(forKey: localFlag.rawValue)
        return value
    }
}

