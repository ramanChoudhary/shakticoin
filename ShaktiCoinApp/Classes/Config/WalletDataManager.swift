//
//  WalletDataManager.swift
//  ShaktiCoinApp
//
//  Created by sperev on 08/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation
import KeychainAccess

struct WalletDataManager {
    private let keychainWalletBalanceKey = "wallet_balance_key"
    private let keychainWalletKey = "wallet_bytes"
    private let keychainPassphraseKey = "wallet_passphrase"
    private let keychainReferralCodeKey = "referral_code"
    private var service = Keychain(service: "wallet")
    
    static let shared = WalletDataManager()
    
    // MARK: - Methods
    
    var wallet: String? {
        return try? service.get(keychainWalletKey)
    }
    
    func set(wallet: String) {
        try? service.set(wallet, key: keychainWalletKey)
    }
    
    var passphrase: String? {
        return try? service.get(keychainPassphraseKey)
    }
    
    func set(passphrase: String) {
        try? service.set(passphrase, key: keychainPassphraseKey)
    }
    
    var walletBalance:String? {
        return try? service.get(keychainWalletBalanceKey)
    }
    
    func set(walletBalance:String) {
        try? service.set(walletBalance, key: keychainWalletBalanceKey)
    }
    
    var promotionalRefferalCode:String? {
        return try? service.get(keychainReferralCodeKey)
    }
    
    func set(referralCode:String) {
        try? service.set(referralCode, key: keychainReferralCodeKey)
    }
    
    func reset() {
        try? service.remove(keychainWalletKey)
        try? service.remove(keychainPassphraseKey)
        try? service.remove(keychainWalletBalanceKey)
        try? service.remove(keychainReferralCodeKey)
    }
}
