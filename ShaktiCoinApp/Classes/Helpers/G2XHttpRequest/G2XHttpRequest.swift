//
//  G2XHttpRequest.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 12/25/19.
//  Copyright © 2019 Garnik Giloyan. All rights reserved.
//

import Foundation




public enum G2XHTTPRequestMethod : String {
    case GET,POST,PUT,DELETE
}

class G2XHttpRequest {

    private var httpMethod: G2XHTTPRequestMethod = G2XHTTPRequestMethod.GET
    private var endPoint: URL!
    private var request: NSMutableURLRequest!
    private var jsonData : Data?

    init(_httpMethod: G2XHTTPRequestMethod, _endPoint: URL) {
        httpMethod = _httpMethod
        endPoint = _endPoint
        request = NSMutableURLRequest(url: endPoint)
    }
    
    func setHeaders(_headers: [String: String]) {
        for (key, value) in _headers {
            request.addValue(value, forHTTPHeaderField: key)
        }
    }
    
    func setJsonData(_data : Data) {
        jsonData = _data
    }
    
    func printResponse(httpResponse:HTTPURLResponse, data:Data) {
        print("-----------------*******************----------------\n")
        
    
        

        print("-----------------Request Header----------------\n\n"+"\(String(describing: self.request.allHTTPHeaderFields!.debugDescription))")
        
        
        if let rbody = self.request.httpBody {
            let reqBody = String(data: rbody, encoding: .utf8)

            print("\n\n\n-----------------Request Body----------------\n"+"\(String(describing: reqBody!))")

        }

        print("\n\n-----------------Request----------------\n"+"\(String(describing: self.request.url!))")
        
        print("\n\n-----------------Response Status code ----------------\n"+"\(httpResponse.statusCode)")

        do {
            if let responseDictionary = try JSONSerialization.jsonObject(with: data, options:[]) as?  [String: AnyObject] {
                print("\n\n-----------------Response----------------\n"+"\(responseDictionary)")
            }
        } catch {
            print("Errror")
        }
        
        let responseString = String(data: data, encoding: .utf8)

       // print("\n\n\n-----------------Response String----------------\n"+"\(String(describing: responseString))")

        print("\n\n\n-----------------*******************----------------\n")

    }
    
    func runWithNothing(callBack: @escaping(_ statusCode: Int) -> ()) {
           
           request.httpMethod = httpMethod.rawValue
    
           if jsonData != nil {
               request.httpBody = jsonData
           }
    
           URLSession.shared.dataTask(with: request as URLRequest) { (data, response, err) in
               let httpResponse = response as? HTTPURLResponse
            
            self.printResponse(httpResponse: httpResponse!, data: data!)

               callBack(httpResponse?.statusCode ?? 500)
           }.resume()
       }
    
    func run(callBack: @escaping([String: Any]?, _ statusCode: Int) -> ()) {
        
        request.httpMethod = httpMethod.rawValue
 
        if jsonData != nil {
            request.httpBody = jsonData
        }
 
        URLSession.shared.dataTask(with: request as URLRequest) { (data, response, err) in
            
            guard let data = data else { return }
            let httpResponse = response as? HTTPURLResponse
            
            self.printResponse(httpResponse: httpResponse!, data: data)

            do {
                if let json =  try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] {
                    callBack(json, httpResponse?.statusCode ?? 500)
                }
            } catch let jsonErr {
                print("Error serializing json:", jsonErr)
            }
        }.resume()
    }
    
    func runWithReturnAny(callBack: @escaping([Any]?, _ statusCode: Int) -> ()) {
           
           request.httpMethod = httpMethod.rawValue
    
           if jsonData != nil {
               request.httpBody = jsonData
           }
    
           URLSession.shared.dataTask(with: request as URLRequest) { (data, response, err) in
               
               guard let data = data else { return }
               let httpResponse = response as? HTTPURLResponse
            self.printResponse(httpResponse: httpResponse!, data: data)

               do {
                   if let json =  try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [Any] {
                       callBack(json, httpResponse?.statusCode ?? 500)
                   }
               } catch let jsonErr {
                   print("Error serializing json:", jsonErr)
               }
           }.resume()
    }
    
    func runWithReturnData(callBack: @escaping(Data?, _ statusCode: Int) -> ()) {
           
           request.httpMethod = httpMethod.rawValue
    
           if jsonData != nil {
               request.httpBody = jsonData
           }
    
           URLSession.shared.dataTask(with: request as URLRequest) { (data, response, err) in
            self.printResponse(httpResponse: response as? HTTPURLResponse ?? HTTPURLResponse(), data: data ?? Data())

            guard let data = data else { callBack(nil, 500); return }
                let httpResponse = response as? HTTPURLResponse
                callBack(data, httpResponse?.statusCode ?? 500)
            
           }.resume()
    }
    
}
