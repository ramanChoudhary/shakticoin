//
//  UIFont.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 10/23/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

extension UIFont {
    
    static func lato(fontSize: CGFloat, type: Lato = Lato.Regular) -> UIFont {
        return UIFont(name: "Lato-" + type.rawValue, size: fontSize)!
    }
    
    static func futura(fontSize: CGFloat, type: FuturaPT = FuturaPT.Book) -> UIFont {
        return UIFont(name: "FuturaPT-" + type.rawValue, size: fontSize)!
    }
}
