//
//  Dictionary.swift
//  ShaktiCoinApp
//
//  Created by sperev on 13/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

extension Dictionary {

    /// Build string representation of HTTP parameter dictionary of keys and objects
    ///
    /// This percent escapes in compliance with RFC 3986
    ///
    /// http://www.ietf.org/rfc/rfc3986.txt
    ///
    /// :returns: String representation in the form of key1=value1&key2=value2 where the keys and values are percent escaped

    func stringFromHttpParameters() -> String {
        let parameterArray = self.map { (key, value) -> String in
            if let key = key as? String {
                let percentEscapedKey = key.addingPercentEncodingForURLQueryValue()!
                let percentEscapedValue = String(describing: value).addingPercentEncodingForURLQueryValue()!
                return "\(percentEscapedKey)=\(percentEscapedValue)"
            }
            return ""
        }

        return parameterArray.joined(separator: "&")
    }

}
