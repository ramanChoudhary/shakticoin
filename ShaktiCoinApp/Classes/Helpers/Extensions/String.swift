//
//  String.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        
        var lng: String!
        
        switch LanguageManager.shared().DefaultLanguage() {
        case .English:
            lng = "en"
            break
        case .French:
            lng = "fr"
            break
        case .Spanish:
            lng = "es"
            break
        }
        
        let path = Bundle.main.path(forResource: lng, ofType: "lproj")
        let bundle = Bundle(path: path!)
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}

extension String {

    /// Percent escapes values to be added to a URL query as specified in RFC 3986
    ///
    /// This percent-escapes all characters besides the alphanumeric character set and "-", ".", "_", and "~".
    ///
    /// http://www.ietf.org/rfc/rfc3986.txt
    ///
    /// :returns: Returns percent-escaped string.

    func addingPercentEncodingForURLQueryValue() -> String? {
        let allowedCharacters = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~")
        return self.addingPercentEncoding(withAllowedCharacters: allowedCharacters)
    }

}

extension String {
    var alphanumericsUnicodeScalarsLength: Int {
        var characterCount = 0
        for character in unicodeScalars {
            if CharacterSet.alphanumerics.contains(character) {
                characterCount += 1
            }
        }
        return characterCount
    }
    
    var passwordStrength: (Bool, Bool, Bool, Bool) {
        var upp = false
        var low = false
        var num = false
        var sym = false
        
        let set = CharacterSet(charactersIn: self)
        low = !CharacterSet.lowercaseLetters.isDisjoint(with: set)
        upp = !CharacterSet.uppercaseLetters.isDisjoint(with: set)
        num = !CharacterSet.decimalDigits.isDisjoint(with: set)
        sym = !CharacterSet(charactersIn: "!@#$&*").isDisjoint(with: set)
        
        return (upp, num, low, sym)
    }
}

extension String {
    func convertToToshi() -> String {
       if let doubleValue = Double(self) {
            let toshi = Int(doubleValue * 100000)
            return "\(toshi)"
        } else  if let intValue = Int(self) {
            let toshi = intValue * 100000
            return "\(toshi)"
        }
        return "0.0"
    }
}
