//
//  UIView.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 10/24/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

extension UIView {
    
    @discardableResult
    func centralizeX(_ centerX: NSLayoutXAxisAnchor, constant: CGFloat = 0) -> NSLayoutConstraint {
        translatesAutoresizingMaskIntoConstraints = false
        
        var anchor = NSLayoutConstraint()
        anchor = centerXAnchor.constraint(equalTo: centerX, constant: constant)
        anchor.isActive = true
        return anchor
    }
    
    @discardableResult
    func centralizeY(_ centerY: NSLayoutYAxisAnchor, constant: CGFloat = 0) -> NSLayoutConstraint {
        translatesAutoresizingMaskIntoConstraints = false
        
        var anchor = NSLayoutConstraint()
        anchor = centerYAnchor.constraint(equalTo: centerY, constant: constant)
        anchor.isActive = true
        return anchor
    }
    
    @discardableResult
    func anchorToTop(_ top: NSLayoutYAxisAnchor? = nil, topConstant: CGFloat = 0) -> [NSLayoutConstraint] {
        translatesAutoresizingMaskIntoConstraints = false
        
        var anchors = [NSLayoutConstraint]()
        
        if let top = top {
            anchors.append(topAnchor.constraint(equalTo: top, constant: topConstant))
        }
        
        anchors.forEach({$0.isActive = true})
        
        return anchors
    }
    
    @discardableResult
    func anchorToBottom(_ bottom: NSLayoutYAxisAnchor? = nil, bottomConstant: CGFloat = 0) -> [NSLayoutConstraint] {
        translatesAutoresizingMaskIntoConstraints = false
        
        var anchors = [NSLayoutConstraint]()
        
        if let bottom = bottom {
            anchors.append(bottomAnchor.constraint(equalTo: bottom, constant: bottomConstant))
        }
        
        anchors.forEach({$0.isActive = true})
        
        return anchors
    }
    
    @discardableResult
    func anchorToLeft(_ anchor: NSLayoutXAxisAnchor? = nil, constant: CGFloat = 0) -> [NSLayoutConstraint] {
        translatesAutoresizingMaskIntoConstraints = false
        
        var anchors = [NSLayoutConstraint]()
        
        if let anchor = anchor {
            anchors.append(leftAnchor.constraint(equalTo: anchor, constant: constant))
        }
        
        anchors.forEach({$0.isActive = true})
        
        return anchors
    }
    
    @discardableResult
    func anchorToRight(_ anchor: NSLayoutXAxisAnchor? = nil, constant: CGFloat = 0) -> [NSLayoutConstraint] {
        translatesAutoresizingMaskIntoConstraints = false
        
        var anchors = [NSLayoutConstraint]()
        
        if let anchor = anchor {
            anchors.append(rightAnchor.constraint(equalTo: anchor, constant: constant))
        }
        
        anchors.forEach({$0.isActive = true})
        
        return anchors
    }
    
    func anchor(_ top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil, topConstant: CGFloat = 0, leftConstant: CGFloat = 0, bottomConstant: CGFloat = 0, rightConstant: CGFloat = 0, widthConstant: CGFloat = 0, heightConstant: CGFloat = 0) -> [NSLayoutConstraint] {
        translatesAutoresizingMaskIntoConstraints = false
        
        var anchors = [NSLayoutConstraint]()
        
        if let top = top {
            anchors.append(topAnchor.constraint(equalTo: top, constant: topConstant))
        }
        
        if let left = left {
            anchors.append(leftAnchor.constraint(equalTo: left, constant: leftConstant))
        }
        
        if let bottom = bottom {
            anchors.append(bottomAnchor.constraint(equalTo: bottom, constant: -bottomConstant))
        }
        
        if let right = right {
            anchors.append(rightAnchor.constraint(equalTo: right, constant: -rightConstant))
        }
        
        if widthConstant > 0 {
            anchors.append(widthAnchor.constraint(equalToConstant: widthConstant))
        }
        
        if heightConstant > 0 {
            anchors.append(heightAnchor.constraint(equalToConstant: heightConstant))
        }
        
        anchors.forEach({$0.isActive = true})
        
        return anchors
    }
}

extension UIView {
    
    @discardableResult
    func roundedCorners(withRadius radius: CGFloat = 12.0, and cornerPositions: UIRectCorner) -> Self {
        guard self.bounds.height != 0 else { return self }
        roundedCorners(withRadius: radius, corners: cornerPositions)
        return self
    }
    
    @discardableResult
    func roundedTopCorners(withRadius radius: CGFloat = 12.0) -> Self {
        guard self.bounds.height != 0 else { return self }
        roundedCorners(withRadius: radius, corners: [.topLeft, .topRight])
        return self
    }
    
    @discardableResult
    func roundedBottomCorners(withRadius radius: CGFloat = 12.0) -> Self {
        guard self.bounds.height != 0 else { return self }
        roundedCorners(withRadius: radius, corners: [.bottomLeft, .bottomRight])
        return self
    }
    
    func roundedCorners(withRadius radius: CGFloat, corners: UIRectCorner) {
        let maskPath = UIBezierPath(roundedRect: self.bounds,
                                    byRoundingCorners: corners,
                                    cornerRadii: CGSize(width: radius, height: radius))
        let shape = CAShapeLayer()
        shape.frame = self.bounds
        shape.path = maskPath.cgPath
        self.layer.mask = shape
    }
    
    @discardableResult
    func roundedCorners(withRadius radius: CGFloat = 6.0) -> Self {
        self.clipsToBounds = true
        self.layer.cornerRadius = radius
        return self
    }
    
    @discardableResult
    func roundedCornersWithMask(with radius: CGFloat = 6.0) -> Self {
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: [.topLeft, .bottomLeft, .topRight, .bottomRight], cornerRadii: CGSize(width: radius, height: radius)).cgPath
        layer.mask = maskLayer
        return self
    }
    
    @discardableResult
    func roundedCorners() -> Self {
        roundedCorners(withRadius: self.bounds.size.height / 2)
        return self
    }
    
    @discardableResult
    func borderWidth(_ width: CGFloat) -> Self {
        self.clipsToBounds = true
        self.layer.borderWidth = width
        return self
    }
    
    @discardableResult
    func borderColor(_ color: UIColor) -> Self {
        self.clipsToBounds = true
        self.layer.borderColor = color.cgColor
        return self
    }
    
    @discardableResult
    func border(color: UIColor, and width: CGFloat) -> Self {
        self.clipsToBounds = true
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
        return self
    }
    
    var hasSomeMask: Bool {
        guard let mask = self.layer.mask,
            mask.frame.width == self.frame.width
        else {
            return false
        }
        return true
    }
    
    func removeMask() {
        self.layer.mask = nil
    }
}

extension UIView {
    
    func autosize(maxWidth: CGFloat, maxHeight: CGFloat = 300) {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        let dummyContainerView = UIView(frame: CGRect(x: 0, y: 0, width: maxWidth, height: maxHeight))
        dummyContainerView.addSubview(self)
        dummyContainerView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        dummyContainerView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        dummyContainerView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
        setNeedsLayout()
        layoutIfNeeded()
        
        removeFromSuperview()
        
        frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
        translatesAutoresizingMaskIntoConstraints = true
    }
}
