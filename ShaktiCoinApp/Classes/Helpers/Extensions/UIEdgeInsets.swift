//
//  UIEdge.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

extension UIEdgeInsets {
    
    init(top: CGFloat = 0, left: CGFloat =  0, bottom: CGFloat =  0, right: CGFloat =  0) {
        self.init()
        self.top = top
        self.left = left
        self.right = right
        self.bottom = bottom
    }
    
    init(vertical: CGFloat = 0, left: CGFloat =  0, right: CGFloat =  0) {
        self.init()
        self.top = vertical
        self.left = left
        self.right = right
        self.bottom = vertical
    }
    
    init(top: CGFloat = 0, horizontal: CGFloat =  0, bottom: CGFloat =  0) {
        self.init()
        self.top = top
        self.left = horizontal
        self.right = horizontal
        self.bottom = bottom
    }
    
    init(vertical: CGFloat = 0, horizontal: CGFloat =  0) {
        self.init()
        self.top = vertical
        self.left = horizontal
        self.right = horizontal
        self.bottom = vertical
    }
    
    static func leading(_ value: CGFloat) -> UIEdgeInsets {
        return UIEdgeInsets(left: value)
    }
    
    static func top(_ value: CGFloat) -> UIEdgeInsets {
        return UIEdgeInsets(top: value)
    }
    
    static func trailing(_ value: CGFloat) -> UIEdgeInsets {
        return UIEdgeInsets(right: value)
    }
    
    static func bottom(_ value: CGFloat) -> UIEdgeInsets {
        return UIEdgeInsets(bottom: value)
    }
    
    static func vertical(_ value: CGFloat) -> UIEdgeInsets {
        return UIEdgeInsets(top: value, bottom: value)
    }
    
    static func horizontal(_ value: CGFloat) -> UIEdgeInsets {
        return UIEdgeInsets(left: value, right: value)
    }
    
    func vertical(_ value: CGFloat) -> UIEdgeInsets {
        var insets = self
        insets.bottom = value
        insets.top = value
        return insets
    }
    
    func horizontal(_ value: CGFloat) -> UIEdgeInsets {
        var insets = self
        insets.left = value
        insets.right = value
        return insets
    }
}
