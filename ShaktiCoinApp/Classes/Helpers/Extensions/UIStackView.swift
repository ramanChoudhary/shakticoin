//
//  UIStackView.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

extension UIStackView {
    
    var verticalHeight: CGFloat {
        
        var heights: CGFloat = 0
        self.arrangedSubviews.forEach { (view) in
            heights += view.intrinsicContentSize.height
        }
        
        let spaces = self.spacing * CGFloat(self.arrangedSubviews.count - 1)
        
        return (heights + spaces)
    }
    
    var horitzontalHeight: CGFloat {
        
        let orderedByHeight = self.arrangedSubviews.sorted { (view1, view2) -> Bool in
            return view1.intrinsicContentSize.height > view2.intrinsicContentSize.height
        }
        
        guard let biggerView = orderedByHeight.first else {
            return 0
        }
        
        return biggerView.intrinsicContentSize.height
    }
}

extension UIStackView {
    
    public func addView(_ view: UIView, below subview: UIView? = nil, edgeInsets: UIEdgeInsets = UIEdgeInsets.zero, backgroundColor: UIColor = .clear) {
        addView(view, searching: subview, adding: 1, edgeInsets: edgeInsets, backgroundColor: backgroundColor)
    }
    
     public func addView(_ view: UIView, above subview: UIView?, edgeInsets: UIEdgeInsets = UIEdgeInsets.zero, backgroundColor: UIColor = .clear) {
        addView(view, searching: subview, adding: 0, edgeInsets: edgeInsets, backgroundColor: backgroundColor)
    }
    
    private func addView(_ view: UIView, searching subview: UIView? = nil, adding offset: Int = 0, edgeInsets: UIEdgeInsets, backgroundColor: UIColor) {
        let auxView = UIView()
        auxView.addSubview(view)
        auxView.backgroundColor = backgroundColor
        ViewHelper.configure(view, in: auxView, edgeInstes: edgeInsets)
        if let subview = subview,
            let parent = subview.superview,
            let index = self.arrangedSubviews.firstIndex(of: parent) {
            self.insertArrangedSubview(auxView, at: index + offset)
        } else {
            self.addArrangedSubview(auxView)
        }
    }
    
    public func removeView(_ view: UIView) {
        if let superview = view.superview {
            self.removeArrangedSubview(superview)
            superview.removeFromSuperview()
        }
    }
    
    public func removeAllViews() {
        self.arrangedSubviews.forEach { (view) in
            self.removeArrangedSubview(view)
            view.removeFromSuperview()
        }
    }
    
    public func containsView(_ view: UIView) -> Bool {
        if let superview = view.superview {
            return self.arrangedSubviews.filter({ $0 == superview }).count > 0
        }
        return false
    }
    
    public func hideView(_ view: UIView) {
        if let superview = view.superview {
            superview.isHidden = true
        }
    }

    public func showView(_ view: UIView) {
        if let superview = view.superview {
            superview.isHidden = false
        }
    }
}
