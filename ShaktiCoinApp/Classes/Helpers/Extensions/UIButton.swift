//
//  UIButton.swift
//  ShaktiCoinApp
//
//  Created by sperev on 18/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

extension UIButton {
    func setup(text: String, style: TextStyle, state: UIControl.State) {
        var attributes: [NSAttributedString.Key: Any] = style.attributes
        attributes[.font] = nil
        let attributedText = NSAttributedString(string: text, attributes: attributes)
        setAttributedTitle(attributedText, for: state)
    }

    func setup(_ viewModel: TextViewModel, state: UIControl.State) {
        titleLabel?.setup(viewModel)
        setup(text: viewModel.value, style: viewModel.style, state: state)
    }
}

