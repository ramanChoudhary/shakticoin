//
//  UILabel.swift
//  ShaktiCoinApp
//
//  Created by sperev on 18/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

extension UILabel {
    func setup(text: String, style: TextStyle) {
        let attributes: [NSAttributedString.Key: Any] = style.attributes
        self.numberOfLines = style.numberOfLines
        self.attributedText = NSAttributedString(string: text, attributes: attributes)
    }
    
    func setup(_ viewModel: TextViewModel) {
        setup(text: viewModel.value, style: viewModel.style)
    }
}
