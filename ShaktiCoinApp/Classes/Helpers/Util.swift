//
//  Util.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 10/24/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class Util {
    
    static func NSMAttrString(text: String, font: UIFont, color: UIColor = .white) -> NSMutableAttributedString {
        
        return NSMutableAttributedString(string: text,
                                  attributes: [NSAttributedString.Key.foregroundColor: color,
                                               NSAttributedString.Key.font: font])
    }
    
    static func NSAttrString(text: String, font: UIFont, color: UIColor = .white) -> NSAttributedString {
        return NSAttributedString(string: text, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: color])
    }
    
    static func loadPostalCodes() -> [CountryISO]? {
        if let url = Bundle.main.url(forResource: "postal-codes", withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode([CountryISO].self, from: data)
                return jsonData
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }
    
    static func loadPhoneCodes() -> [PhoneCode]? {
        if let url = Bundle.main.url(forResource: "phone-codes", withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode([PhoneCode].self, from: data)
                return jsonData
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }
    
    static func fullyAnchor(_parnetControl: UIView, _childControl: UIView) {
        _ = _childControl.anchor(_parnetControl.topAnchor, left: _parnetControl.leftAnchor, bottom: _parnetControl.bottomAnchor, right: _parnetControl.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    static func getCurrentCountryCode() -> String {
        let countryLocale = NSLocale.current
        let countryCode = countryLocale.regionCode
        return countryCode!
    }

}
