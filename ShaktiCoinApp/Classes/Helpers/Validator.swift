//
//  Validator.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/22/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import Foundation

class Validator {
    
    // MARK: - Accessors
    
    private static var sharedInstance: Validator = {
        let sharedInstance = Validator()
        return sharedInstance
    }()
    
    class func shared() -> Validator {
        return sharedInstance
    }
    
    // MARK: - Methods
    
    func isValidPassword(password: String) -> Bool {
        //Minimum 8 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character
        let passwordRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[d$@$!%*?&#])[A-Za-z\\dd$@$!%*?&#]{8,}"
        return true
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: password)
    }

    func isValidEmail(email: String) -> Bool {
        let emailRegEx = "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"+"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"+"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"+"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"+"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"+"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"+"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    func isValidPhoneNumber(phone: String) -> Bool {
        let phoneRegEx = "^(\\(?\\+?[0-9]*\\)?)?[0-9_\\- \\(\\)]{9,15}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES[c] %@", phoneRegEx)
        return phoneTest.evaluate(with: phone)
    }
    
    func isValidZipCode(country: String, zipCode: String) -> Bool {
        
        let listOfCountries = Util.loadPostalCodes()
        let selectedCountry = listOfCountries?.filter{$0.ISO == country}.first
        
        if !selectedCountry!.Regex.isEmpty {
        
            let zipCodeRegEx = selectedCountry!.Regex
            
            let zipCodeTest = NSPredicate(format:"SELF MATCHES[c] %@", zipCodeRegEx)
            return zipCodeTest.evaluate(with: zipCode)
        } else {
            return true
        }
    }
    
    func getZipCodeFormatByCountry(country: String) -> String {
        let listOfCountries = Util.loadPostalCodes()
        let selectedCountry = listOfCountries?.filter{$0.Country == country}.first
        if selectedCountry == nil {
            return "- no codes-"
        }
        return selectedCountry!.Format
    }
    
}
