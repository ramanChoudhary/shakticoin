//
//  ViewDispatcher.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class ViewDispatcher {

    static var shared: ViewDispatcher = ViewDispatcher()
    var dispatcher: ViewDipatcherProtocol = DispatcherDefault()

    func execute(_ action: @escaping () -> Void) {
        dispatcher.execute(action)
    }
}

protocol ViewDipatcherProtocol: class {
    func execute(_ action: @escaping () -> Void)
}

class DispatcherDefault: ViewDipatcherProtocol {
    
    func execute(_ action: @escaping () -> Void) {
        DispatchQueue.main.async(execute: action)
    }
}

class DispatcherMock: ViewDipatcherProtocol {
    
    func execute(_ action: @escaping () -> Void) {
        action()
    }
}
