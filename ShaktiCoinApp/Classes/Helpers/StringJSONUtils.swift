//
//  StringJSON.swift
//  ShaktiCoinApp
//
//  Created by sperev on 13/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

class StringJSONUtils {
    
    static func convertStringToDictionary(text: String) -> AnyObject? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                let dict = try JSONSerialization.jsonObject(with: data, options: [JSONSerialization.ReadingOptions.mutableContainers])
                return dict as AnyObject?
            } catch { }
        }
        return nil
    }
    
    static func convertStringifyToDictionary(text: String) -> AnyObject? {
        return convertStringToDictionary(text: text)
    }
    
    static func convertDictionaryToJSON(dict: [String: Any]) -> String? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: [])
            return String(data: jsonData, encoding: .utf8)
        } catch {}
        return nil
    }
}
