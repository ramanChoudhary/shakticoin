//
//  ViewHelper.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

class ViewHelper {
    
    static func configure(_ view: UIView, in ownerView: UIView, edgeInstes: UIEdgeInsets) {
        ownerView.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.frame = ownerView.bounds
        ownerView.addConstraints([NSLayoutConstraint(item: view,
                                                     attribute: NSLayoutConstraint.Attribute.leading,
                                                     relatedBy: NSLayoutConstraint.Relation.equal,
                                                     toItem: ownerView,
                                                     attribute: NSLayoutConstraint.Attribute.leading,
                                                     multiplier: 1,
                                                     constant: edgeInstes.left),
                                  NSLayoutConstraint(item: view,
                                                     attribute: NSLayoutConstraint.Attribute.top,
                                                     relatedBy: NSLayoutConstraint.Relation.equal,
                                                     toItem: ownerView,
                                                     attribute: NSLayoutConstraint.Attribute.top,
                                                     multiplier: 1,
                                                     constant: edgeInstes.top),
                                  NSLayoutConstraint(item: view,
                                                     attribute: NSLayoutConstraint.Attribute.trailing,
                                                     relatedBy: NSLayoutConstraint.Relation.equal,
                                                     toItem: ownerView,
                                                     attribute: NSLayoutConstraint.Attribute.trailing,
                                                     multiplier: 1,
                                                     constant: -edgeInstes.right),
                                  NSLayoutConstraint(item: view,
                                                     attribute: NSLayoutConstraint.Attribute.bottom,
                                                     relatedBy: NSLayoutConstraint.Relation.equal,
                                                     toItem: ownerView,
                                                     attribute: NSLayoutConstraint.Attribute.bottom,
                                                     multiplier: 1,
                                                     constant: -edgeInstes.bottom)
            ])
    }
    
    static func configure(_ view: UIView, in ownerView: UIView) {
        ownerView.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.frame = ownerView.bounds
        ownerView.addConstraints([NSLayoutConstraint(item: view,
                                                     attribute: NSLayoutConstraint.Attribute.leading,
                                                     relatedBy: NSLayoutConstraint.Relation.equal,
                                                     toItem: ownerView,
                                                     attribute: NSLayoutConstraint.Attribute.leading,
                                                     multiplier: 1,
                                                     constant: 0),
                                  NSLayoutConstraint(item: view,
                                                     attribute: NSLayoutConstraint.Attribute.top,
                                                     relatedBy: NSLayoutConstraint.Relation.equal,
                                                     toItem: ownerView,
                                                     attribute: NSLayoutConstraint.Attribute.top,
                                                     multiplier: 1,
                                                     constant: 0),
                                  NSLayoutConstraint(item: view,
                                                     attribute: NSLayoutConstraint.Attribute.trailing,
                                                     relatedBy: NSLayoutConstraint.Relation.equal,
                                                     toItem: ownerView,
                                                     attribute: NSLayoutConstraint.Attribute.trailing,
                                                     multiplier: 1,
                                                     constant: 0),
                                  NSLayoutConstraint(item: view,
                                                     attribute: NSLayoutConstraint.Attribute.bottom,
                                                     relatedBy: NSLayoutConstraint.Relation.equal,
                                                     toItem: ownerView,
                                                     attribute: NSLayoutConstraint.Attribute.bottom,
                                                     multiplier: 1,
                                                     constant: 0)
            ])
    }
    
    static func configure(_ view: UIView, in ownerView: UIView, withLeading leading: CGFloat) {
        ownerView.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.frame = ownerView.bounds
        ownerView.addConstraints([NSLayoutConstraint(item: view,
                                                     attribute: NSLayoutConstraint.Attribute.leading,
                                                     relatedBy: NSLayoutConstraint.Relation.equal,
                                                     toItem: ownerView,
                                                     attribute: NSLayoutConstraint.Attribute.leading,
                                                     multiplier: 1,
                                                     constant: leading),
                                  NSLayoutConstraint(item: view,
                                                     attribute: NSLayoutConstraint.Attribute.top,
                                                     relatedBy: NSLayoutConstraint.Relation.equal,
                                                     toItem: ownerView,
                                                     attribute: NSLayoutConstraint.Attribute.top,
                                                     multiplier: 1,
                                                     constant: 0),
                                  NSLayoutConstraint(item: view,
                                                     attribute: NSLayoutConstraint.Attribute.trailing,
                                                     relatedBy: NSLayoutConstraint.Relation.equal,
                                                     toItem: ownerView,
                                                     attribute: NSLayoutConstraint.Attribute.trailing,
                                                     multiplier: 1,
                                                     constant: -leading),
                                  NSLayoutConstraint(item: view,
                                                     attribute: NSLayoutConstraint.Attribute.bottom,
                                                     relatedBy: NSLayoutConstraint.Relation.equal,
                                                     toItem: ownerView,
                                                     attribute: NSLayoutConstraint.Attribute.bottom,
                                                     multiplier: 1,
                                                     constant: 0)
            ])
    }

    @discardableResult
    static func configure(_ view: UIView, in ownerView: UIView, withBottom bottom: CGFloat)
        -> NSLayoutConstraint {

            let bottomConstraint = NSLayoutConstraint(item: view,
                                                      attribute: NSLayoutConstraint.Attribute.bottom,
                                                      relatedBy: NSLayoutConstraint.Relation.equal,
                                                      toItem: ownerView,
                                                      attribute: NSLayoutConstraint.Attribute.bottom,
                                                      multiplier: 1,
                                                      constant: bottom)

            ownerView.addSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false
            view.frame = ownerView.bounds
            ownerView.addConstraints([NSLayoutConstraint(item: view,
                                                         attribute: NSLayoutConstraint.Attribute.leading,
                                                         relatedBy: NSLayoutConstraint.Relation.equal,
                                                         toItem: ownerView,
                                                         attribute: NSLayoutConstraint.Attribute.leading,
                                                         multiplier: 1,
                                                         constant: 0),
                                      NSLayoutConstraint(item: view,
                                                         attribute: NSLayoutConstraint.Attribute.top,
                                                         relatedBy: NSLayoutConstraint.Relation.equal,
                                                         toItem: ownerView,
                                                         attribute: NSLayoutConstraint.Attribute.top,
                                                         multiplier: 1,
                                                         constant: 0),
                                      NSLayoutConstraint(item: view,
                                                         attribute: NSLayoutConstraint.Attribute.trailing,
                                                         relatedBy: NSLayoutConstraint.Relation.equal,
                                                         toItem: ownerView,
                                                         attribute: NSLayoutConstraint.Attribute.trailing,
                                                         multiplier: 1,
                                                         constant: 0),
                                      bottomConstraint
                ])

            return bottomConstraint
    }
    
    static func configure(_ view: UIView, in ownerView: UIView, withTop top: CGFloat, withBottom bottom: CGFloat, withLeading leading: CGFloat) {
        ownerView.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.frame = ownerView.bounds
        ownerView.addConstraints([NSLayoutConstraint(item: view,
                                                     attribute: NSLayoutConstraint.Attribute.leading,
                                                     relatedBy: NSLayoutConstraint.Relation.equal,
                                                     toItem: ownerView,
                                                     attribute: NSLayoutConstraint.Attribute.leading,
                                                     multiplier: 1,
                                                     constant: leading),
                                  NSLayoutConstraint(item: view,
                                                     attribute: NSLayoutConstraint.Attribute.top,
                                                     relatedBy: NSLayoutConstraint.Relation.equal,
                                                     toItem: ownerView,
                                                     attribute: NSLayoutConstraint.Attribute.top,
                                                     multiplier: 1,
                                                     constant: top),
                                  NSLayoutConstraint(item: view,
                                                     attribute: NSLayoutConstraint.Attribute.trailing,
                                                     relatedBy: NSLayoutConstraint.Relation.equal,
                                                     toItem: ownerView,
                                                     attribute: NSLayoutConstraint.Attribute.trailing,
                                                     multiplier: 1,
                                                     constant: -leading),
                                  NSLayoutConstraint(item: view,
                                                     attribute: NSLayoutConstraint.Attribute.bottom,
                                                     relatedBy: NSLayoutConstraint.Relation.equal,
                                                     toItem: ownerView,
                                                     attribute: NSLayoutConstraint.Attribute.bottom,
                                                     multiplier: 1,
                                                     constant: -bottom)
            ])
    }

    static func configureWithSafeArea(_ view: UIView, in ownerView: UIView) {
        ownerView.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.frame = ownerView.bounds
        ownerView.addConstraints([NSLayoutConstraint(item: view,
                                                 attribute: NSLayoutConstraint.Attribute.leading,
                                                 relatedBy: NSLayoutConstraint.Relation.equal,
                                                 toItem: ownerView,
                                                 attribute: NSLayoutConstraint.Attribute.leading,
                                                 multiplier: 1,
                                                 constant: 0),
                              NSLayoutConstraint(item: view,
                                                 attribute: NSLayoutConstraint.Attribute.top,
                                                 relatedBy: NSLayoutConstraint.Relation.equal,
                                                 toItem: ownerView,
                                                 attribute: NSLayoutConstraint.Attribute.top,
                                                 multiplier: 1,
                                                 constant: 0),
                              NSLayoutConstraint(item: view,
                                                 attribute: NSLayoutConstraint.Attribute.trailing,
                                                 relatedBy: NSLayoutConstraint.Relation.equal,
                                                 toItem: ownerView,
                                                 attribute: NSLayoutConstraint.Attribute.trailing,
                                                 multiplier: 1,
                                                 constant: 0),
                              NSLayoutConstraint(item: view,
                                                 attribute: NSLayoutConstraint.Attribute.bottom,
                                                 relatedBy: NSLayoutConstraint.Relation.equal,
                                                 toItem: ownerView.safeAreaLayoutGuide,
                                                 attribute: NSLayoutConstraint.Attribute.bottom,
                                                 multiplier: 1,
                                                 constant: 0)
        ])
    }
    
    static func addConstraintsTo(_ arrayImages: [UIImageView], in scrollView: UIScrollView) {
        for i in 0 ..< arrayImages.count {
            let image = arrayImages[i]
            image.translatesAutoresizingMaskIntoConstraints = false
            scrollView.addConstraint(NSLayoutConstraint(item: image,
                                                        attribute: .width,
                                                        relatedBy: .equal,
                                                        toItem: scrollView,
                                                        attribute: .width,
                                                        multiplier: 1,
                                                        constant: 0))
            scrollView.addConstraint(NSLayoutConstraint(item: image,
                                                        attribute: .height,
                                                        relatedBy: .equal,
                                                        toItem: scrollView,
                                                        attribute: .height,
                                                        multiplier: 1,
                                                        constant: 0))
            scrollView.addConstraint(NSLayoutConstraint(item: scrollView,
                                                        attribute: .top,
                                                        relatedBy: .equal,
                                                        toItem: image,
                                                        attribute: .top,
                                                        multiplier: 1,
                                                        constant: 0))
            scrollView.addConstraint(NSLayoutConstraint(item: scrollView,
                                                        attribute: .bottom,
                                                        relatedBy: .equal,
                                                        toItem: image,
                                                        attribute: .bottom,
                                                        multiplier: 1,
                                                        constant: 0))
            if i == 0 {
                scrollView.addConstraint(NSLayoutConstraint(item: scrollView,
                                                            attribute: .leading,
                                                            relatedBy: .equal,
                                                            toItem: image,
                                                            attribute: .leading,
                                                            multiplier: 1,
                                                            constant: 0))
            }
            if i > 0 {
                let previousImage = arrayImages[i - 1]
                scrollView.addConstraint(NSLayoutConstraint(item: image,
                                                            attribute: .leading,
                                                            relatedBy: .equal,
                                                            toItem: previousImage,
                                                            attribute: .trailing,
                                                            multiplier: 1,
                                                            constant: 0))
            }
            if i == arrayImages.count - 1 {
                scrollView.addConstraint(NSLayoutConstraint(item: scrollView,
                                                            attribute: .trailing,
                                                            relatedBy: .equal,
                                                            toItem: image,
                                                            attribute: .trailing,
                                                            multiplier: 1,
                                                            constant: 0))
            }
        }
    }
}
