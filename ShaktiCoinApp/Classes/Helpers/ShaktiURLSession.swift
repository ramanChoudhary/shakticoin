//
//  ShaktiURLSession.swift
//  ShaktiCoinApp
//
//  Created by Pravin Gawale on 30/06/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import UIKit

enum apiType:String {
    case none = ""
    case kyc_get = "kyc_get"
}

class ShaktiURLSession {

    
    static let shared = ShaktiURLSession()
       
    private init(){}

    let session = URLSession.shared
    
    var type : apiType = .none

    
    func loadJson(completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> Void {
        let jsonFileName = self.type.rawValue
        self.type = .none
        if let path = Bundle.main.path(forResource: jsonFileName, ofType: "json") {
            
            do {
                let jdata = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                print("jsonData:\(jdata)")
                let httpResponse = HTTPURLResponse(url: URL(string: "https://google.com")!, statusCode: 200, httpVersion: nil, headerFields: nil)
                completionHandler(jdata, httpResponse, nil)
            } catch let error {
                print("parse error: \(error.localizedDescription)")
                completionHandler(nil,nil, nil)
            }
        }
    }
    
}
