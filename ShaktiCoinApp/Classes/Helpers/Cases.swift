//
//  Cases.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 10/23/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import Foundation

public enum Lato : String {
    case Black,BlackItalic,Bold,BoldItalic,Hairline,HairlineItalic,Italic,Light,LightItalic,Regular
}

public enum FuturaPT : String {
    case Bold,BoldOblique,Book,BookOblique,CondBold,CondBoldOblique,CondBook,CondBookOblique,
    CondExtraBoldOblique,CondMedium,CondMediumOblique,Demi,DemiOblique,ExtraBold,ExtraBoldOblique,Heavy,HeavyOblique,Light,
    Medium,MediumOblique
}

public enum Menu : Int {
    case wallet = 1
    case vault = 2
    case miner = 3
    case poe = 4
    case addFriendsAndFamily = 5
    case referrals = 6
    case settings = 7
    case home = 8
    case familyTree = 9
    case company = 10
    case grabBonusBounty = 11
    case claimBonusBounty = 12

    static var rows: [Menu] {
        return [.wallet, .vault, .miner, .referrals, .settings, .home, .familyTree, .company]
    }
    
    static var dropdownRows: [Menu] {
        return [.wallet, .vault, .miner,.poe,.addFriendsAndFamily, .referrals, .settings, .home, .familyTree, .company]
    }

}

public enum PopUp {
    case BasicMiner
    case BusinessVault
    case IndividualWallet
}

public enum SeqNo : Int {
    case First = 0
    case Second = 1
    case Third = 2
}

public enum SettingsTableSection : String {
    case Account,Security,NotificationPreferences,HelpAndPolicies
}
