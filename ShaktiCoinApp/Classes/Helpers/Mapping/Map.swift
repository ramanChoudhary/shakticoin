//
//  Map.swift
//  Badi
//
//  Created by Sergei Perevoznikov on 8/23/18.
//  Copyright © 2018 Badi. All rights reserved.
//

import Foundation

enum MappingError: ErrorModelProtocol {
    case parsing(String)
    case unknown
    
    var string: String {
        switch self {
        case .parsing(let key): return "mapping_\(key)"
        case .unknown: return "mapping_unknown"
        }
    }
}

class Map {
    var report: Bool
    var className: String
    var object: JSONObject
    init(_ object: JSONObject, className: String, report: Bool) {
        self.object = object
        self.className = className
        self.report = report
    }
    
    @discardableResult func error(_ key: String, _ value: Any?) -> Error {
        return ErrorModel(MappingError.parsing(key))
    }
    
    func get<T>(_ key: String) throws -> T {
        if let value = object[key] as? T {
            return value
        }
        throw error(key, object[key])
    }
    
    func get<T: MapRepresentable>(_ key: String) throws -> [T] {
        guard let value = object[key] as? JSONArray else {
            throw error(key, object[key])
        }
        return Mapper<T>.map(array: value, report: report)
    }
    
    func get<T: MapRepresentable>(_ key: String) throws -> [T]? {
        guard let value = object[key] as? JSONArray else {
            throw error(key, object[key])
        }
        return Mapper<T>.map(array: value, report: report)
    }
    
    func get<T: MapRepresentable>(_ key: String) throws -> T {
        guard let value = object[key] as? JSONObject else {
            throw error(key, object[key])
        }
        return try Mapper<T>.map(object: value, report: self.report)
    }
    
    func get<T: MapRepresentable>(_ key: String) throws -> T? {
        guard let value = object[key] as? JSONObject else {
            throw error(key, object[key])
        }
        return try Mapper<T>.map(object: value, report: self.report)
    }
    
    func get<T: RawRepresentable>(_ key: String) throws -> T {
        if let rawValue = object[key] as? T.RawValue,
            let value = T(rawValue: rawValue) {
            return value
        }
        throw error(key, object[key])
    }
    
    func get<T: RawRepresentable>(_ key: String) throws -> T? {
        if let rawValue = object[key] as? T.RawValue,
            let value = T(rawValue: rawValue) {
            return value
        }
        throw error(key, object[key])
    }
    
    func get<T: RawRepresentable>(_ key: String) throws -> [T] {
        if let rawValues = object[key] as? [T.RawValue] {
            return rawValues.compactMap { rawValue in
                if let value = T(rawValue: rawValue) {
                    return value
                }
                error(key, object[key])
                return nil
            }
        }
        throw error(key, object[key])
    }
    
    func get<T: RawRepresentable>(_ key: String) throws -> [T]? {
        if let rawValues = object[key] as? [T.RawValue] {
            return rawValues.compactMap { rawValue in
                if let value = T(rawValue: rawValue) {
                    return value
                }
                error(key, object[key])
                return nil
            }
        }
        throw error(key, object[key])
    }
    
    func get<T, V>(_ key: String, transform: (V) throws -> T) throws -> T {
        if let value = object[key] as? V {
            return try transform(value)
        }
        throw error(key, object[key])
    }
}
