//
//  Mapper.swift
//  Badi
//
//  Created by Sergei Perevoznikov on 8/24/18.
//  Copyright © 2018 Badi. All rights reserved.
//

import Foundation

final class Mapper<T: MapRepresentable> {
    class func map(object: JSONObject, report: Bool = true) throws -> T {
        return try T(map: Map(object, className: String(describing: T.self), report: report))
    }
    
    class func map(array: JSONArray, report: Bool = true) -> [T] {
        return array.compactMap({ try? map(object: $0, report: report) })
    }
}
