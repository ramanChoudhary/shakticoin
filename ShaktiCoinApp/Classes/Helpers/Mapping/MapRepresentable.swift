//
//  MapRepresentable.swift
//  Badi
//
//  Created by Sergei Perevoznikov on 8/24/18.
//  Copyright © 2018 Badi. All rights reserved.
//

import Foundation

protocol MapRepresentable {
    init(map: Map) throws
    var json: [String: Any] { get }
}

extension MapRepresentable {
    var json: [String: Any] {
        fatalError("get json property should be overwritten")
    }
}
