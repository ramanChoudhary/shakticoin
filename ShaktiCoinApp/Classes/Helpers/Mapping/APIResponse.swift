//
//  APIResponse.swift
//  Badi
//
//  Created by Sergei Perevoznikov on 2/19/19.
//  Copyright © 2019 Badi. All rights reserved.
//

import UIKit

struct APIResponse {
    static func handle<T: MapRepresentable>(_ resultBlock: @escaping ((Result<T, ErrorModel>) -> Void), onSuccess: @escaping (T) -> Void = { _ in }) -> APIDataManagerObjectResultBlock {
        return { result in
            switch result {
            case .success(let jsonObject):
                do {
                    let object = try Mapper<T>.map(object: jsonObject)
                    onSuccess(object)
                    resultBlock(.success(object))
                } catch {
                    resultBlock(.failure(ErrorModel(ShaktiError.General.unknown)))
                }
            case .failure(let error):
                resultBlock(.failure(error))
            }
        }
    }
    
    static func handle<T: MapRepresentable>(_ resultBlock: @escaping ((Result<[T], ErrorModel>) -> Void)) -> APIDataManagerResultBlock {
        return { result in
            switch result {
            case .success(let jsonArray): resultBlock(.success(Mapper<T>.map(array: jsonArray)))
            case .failure(let error): resultBlock(.failure(error))
            }
        }
    }
}
