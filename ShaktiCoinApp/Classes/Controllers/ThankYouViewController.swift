//
//  ThankYouViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 10/29/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class ThankYouViewController: UIViewController {

    // MARK: - Declarations
    
    lazy var backgroundImage: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "Register Referral Thanks Image")
        //iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    lazy var coverView: UIView = {
        let cv = UIView()
        //cv.contentMode = .scaleAspectFill
        cv.alpha = 0.75
        cv.backgroundColor = UIColor.black
        return cv
    }()

    lazy var pageTitle : UILabel = {
        let label = UILabel()
        label.text = "ThankYou".localized
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 24, type: .Light)
        return label
    }()
    
    lazy var textInfo : UITextView = {
        let textView = UITextView()
        textView.backgroundColor = .clear
        textView.isUserInteractionEnabled = false
        textView.isEditable = false
        
        let attributedText = NSMutableAttributedString(string: "_TTextInfoSuccess".localized, attributes: [NSAttributedString.Key.font: UIFont.lato(fontSize: 14, type: .Bold), NSAttributedString.Key.foregroundColor: UIColor.white])
        
        attributedText.append(NSAttributedString(string: "_TTextInfoWithYour".localized, attributes: [NSAttributedString.Key.font: UIFont.lato(fontSize: 14, type: .Regular), NSAttributedString.Key.foregroundColor: UIColor.white]))
        
        attributedText.append(NSAttributedString(string: "_TTextInfoUnlock".localized, attributes: [NSAttributedString.Key.font: UIFont.lato(fontSize: 14, type: .Regular), NSAttributedString.Key.foregroundColor: UIColor.white]))
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        let length = attributedText.string.count
        attributedText.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: length))
        textView.attributedText = attributedText
        
        return textView
    }()
    
    lazy var lineView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.mainColor()
        return view
    }()
    
    lazy var finishRegView : UITextView = {
        let textView = UITextView()
        textView.backgroundColor = .clear
        textView.isUserInteractionEnabled = false
        textView.isEditable = false
        textInfo.showsHorizontalScrollIndicator = false
        
        let attributedText = NSMutableAttributedString(string: "_TTRFinish".localized, attributes: [NSAttributedString.Key.font: UIFont.lato(fontSize: 14, type: .Bold), NSAttributedString.Key.foregroundColor: UIColor.white])
        
        attributedText.append(NSAttributedString(string: "_TTRYourBonus".localized, attributes: [NSAttributedString.Key.font: UIFont.lato(fontSize: 14, type: .Bold), NSAttributedString.Key.foregroundColor: UIColor.white]))
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        let length = attributedText.string.count
        attributedText.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: length))

        textView.attributedText = attributedText
        
        return textView
    }()
    
    lazy var trophyImg : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "Trophy")
        return imageView
    }()
    
    lazy var gotoWallet : SCButton = {
        let button = SCButton()
        button.setTitle("CoolToWallet".localized, for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        button.CornerRadius = 18
        button.addTarget(self, action: #selector(onGoToWallet), for: .touchUpInside)
        return button
    }()
    // MARK: - Methods
    
    private func setupViews() {
        
        view.addSubview(backgroundImage)
        view.addSubview(coverView)
        view.addSubview(pageTitle)
        view.addSubview(textInfo)
        view.addSubview(lineView)
        view.addSubview(finishRegView)
        view.addSubview(trophyImg)
        view.addSubview(gotoWallet)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = coverView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = pageTitle.anchor(view.safeAreaLayoutGuide.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 50, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = pageTitle.centralizeX(view.centerXAnchor)
        
        _ = textInfo.anchor(pageTitle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 340, heightConstant: 85)
        _ = textInfo.centralizeX(view.centerXAnchor)
        
        _ = lineView.anchor(textInfo.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: (view.frame.width*90)/100, heightConstant: 2)
        _ = lineView.centralizeX(view.centerXAnchor)
        
        _ = finishRegView.anchor(lineView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 30, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 65)
        _ = finishRegView.centralizeX(view.centerXAnchor)
        
        _ = trophyImg.centralizeX(view.centerXAnchor)
        _ = trophyImg.centralizeY(view.centerYAnchor, constant: 70)
        
        _ = gotoWallet.anchor(nil, left: nil, bottom: view.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 85, rightConstant: 0, widthConstant: 280, heightConstant: 36)
        _ = gotoWallet.centralizeX(view.centerXAnchor)
    }
    
    // MARK: - Events
    
    @objc private func onGoToWallet() {
        AppDelegate.shared?.perform(.myWallet)
    }
    
    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
}
