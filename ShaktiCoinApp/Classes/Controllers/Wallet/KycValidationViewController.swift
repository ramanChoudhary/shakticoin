//
//  KycValidationViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/5/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class KycValidationViewController: KycBaseViewController, UITableViewDelegate, UITableViewDataSource {

    
//    var viewModel = KYCViewModel()
    
    // MARK: - Declarations

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        let service = KycService()
//        showLoader()
//
//
//        service.getUserInfo { (data, status) in
//            DispatchQueue.main.async {
//                hideLoader()
//            }
//            if let info = data {
//                self.viewModel.kycDetails = info
//                self.updateUI()
//            }
//            print("Data = \(data?.firstName)")
//        }
    }
    
    func updateUI() {
//        DispatchQueue.main.async {
//
//            if let info = self.viewModel.kycDetails {
//
//                self.firstPersonalInfoView.firstName.text = info.firstName
//                self.firstPersonalInfoView.lastName.text = info.lastName
//                self.firstPersonalInfoView.middleName.text = info.middleName
//                self.firstPersonalInfoView.dateOfBirth.text = info.dob
//
//
//                print("info contry code country = \(info.address?.countryCode)")
                
//                countryOfResidence.text = Array(self.countryList)[indexPath.row].value as? String

//                if let countryCode = info.address?.country {
//
//                    if let country = self.secondPersonalInfoView.countryList["\(countryCode)"] {
//
//                    }
//                }
//                self.secondPersonalInfoView.countryList[info.address?.country]
//
//                self.secondPersonalInfoView.countryOfResidence.text =
                
//                self.secondPersonalInfoView.countryOfResidence.text = info.address?.country
//                
//                self.secondPersonalInfoView.state.text =  info.address?.stateProvinceCode
//                self.secondPersonalInfoView.city.text = info.address?.city
//                self.secondPersonalInfoView.addressLine1.text = info.address?.address1
//                self.secondPersonalInfoView.addressLine2.text = info.address?.address2
//                self.secondPersonalInfoView.zipCode.text = info.address?.zipCode
//
//                
//                self.firstAdditionalInfoView.emailAddress.text = info.primaryEmail
//                self.firstAdditionalInfoView.phoneNumber.text = info.primaryMobile
//                self.firstAdditionalInfoView.occupationTT.title = info.occupation ?? ""
//                self.firstAdditionalInfoView.educationTT.title = info.education1 ?? ""
//                self.firstAdditionalInfoView.highestLevelOfEducation.text = info.education1
//                
//                self.secondAdditionalInfoView.fullName.text = info.fullName
//                self.secondAdditionalInfoView.phoneNumberOrEmail.text = info.primaryMobile ?? info.primaryEmail
//                self.secondAdditionalInfoView.relationship.text = info.relationshipStatus
//                self.secondAdditionalInfoView.highestLevelOfEducation.text = info.education2
//
//            }
//        }
    }
    
    lazy var firstPersonalInfoView : _PersonalInfoFirst = {
        let view = _PersonalInfoFirst()
        view.firstName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.lastName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.middleName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.dateOfBirth.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.nextButton.addTarget(self, action: #selector(onGoSecondPersonalInfo), for: .touchUpInside)
        view.cancelButton.addTarget(self, action: #selector(goToWallet), for: .touchUpInside)
        return view
    }()
    
    lazy var secondPersonalInfoView : _PersonalInfoSecond = {
        let view = _PersonalInfoSecond()
        //view.countryOfResidence.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.state.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.city.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.addressLine1.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.addressLine2.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.zipCode.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.nextButton.addTarget(self, action: #selector(onUpdatePersonalInfo), for: .touchUpInside)
        view.cancelButton.addTarget(self, action: #selector(onGoToFirstPersonalInfo), for: .touchUpInside)
        return view
    }()
    
    lazy var firstAdditionalInfoView : _AdditionalInfoFirst = {
        let view = _AdditionalInfoFirst()
        view.emailAddress.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.phoneNumber.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.occupation.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.nextButton.addTarget(self, action: #selector(onAddInfoNext), for: .touchUpInside)
        view.highestLevelOfEducation.RightViewButton.addTarget(self, action: #selector(onFirstEducationDropDownIconClick(_button:)), for: .touchUpInside)
        view.highestLevelOfEducationDropDown.CloseButton.addTarget(self, action: #selector(onFirstCloseDropDownList(_button:)), for: .touchUpInside)
        view.cancelButton.addTarget(self, action: #selector(onGoSecondPersonalInfo), for: .touchUpInside)
        return view
    }()
    
    lazy var secondAdditionalInfoView : _AdditionalInfoSecond = {
        let view = _AdditionalInfoSecond()
        view.highestLevelOfEducation.isUserInteractionEnabled = true
        view.fullName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.phoneNumberOrEmail.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
       // view.relationship.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.relationship.RightViewButton.addTarget(self, action: #selector(relationshipDropDownIconClick), for: .touchUpInside)
        view.relationshipDropDown.CloseButton.addTarget(self, action: #selector(onCloseDropDownList(_button:)), for: .touchUpInside)
        view.nextButton.addTarget(self, action: #selector(onSaveAdditionalInfo), for: .touchUpInside)
        view.highestLevelOfEducation.RightViewButton.addTarget(self, action: #selector(onEducationDropDownIconClick(_button:)), for: .touchUpInside)
        view.highestLevelOfEducationDropDown.CloseButton.addTarget(self, action: #selector(onCloseDropDownList(_button:)), for: .touchUpInside)
        view.cancelButton.addTarget(self, action: #selector(onUpdatePersonalInfo), for: .touchUpInside)
        return view
    }()
    
    lazy var kycProofOfAddress : _KycProofOfAddress = {
        let view = _KycProofOfAddress()
        view.nextButton.addTarget(self, action: #selector(onPOfANext), for: .touchUpInside)
        view.cancelButton.addTarget(self, action: #selector(onAddInfoNext), for: .touchUpInside)
        return view
    }()
    
    lazy var kycSelectDocument: _KycSelectDocumentType = {
        let view = _KycSelectDocumentType()
        view.nextButton.addTarget(self, action: #selector(onGoToDocuments), for: .touchUpInside)
        view.cancelButton.addTarget(self, action: #selector(onSaveAdditionalInfo), for: .touchUpInside)
        return view
    }()
  
    lazy var kycPropertyAndFinance : _KycPropertyAndFinance = {
        let view = _KycPropertyAndFinance()
        view.nextButton.addTarget(self, action: #selector(onGoToDocuments), for: .touchUpInside)
        view.cancelButton.addTarget(self, action: #selector(onSaveAdditionalInfo), for: .touchUpInside)
        return view
    }()
    
    lazy var kycSchoolGov : _KycSchoolGov = {
        let view = _KycSchoolGov()
        view.nextButton.addTarget(self, action: #selector(onGoToDocuments), for: .touchUpInside)
        view.cancelButton.addTarget(self, action: #selector(onSaveAdditionalInfo), for: .touchUpInside)
        return view
    }()
    
    lazy var kycSubmitForm : _KycFormToSubmit = {
        let view = _KycFormToSubmit()
        view.nextButton.addTarget(self, action: #selector(onGoToKycVerification), for: .touchUpInside)
        view.cancelButton.addTarget(self, action: #selector(onPOfANext), for: .touchUpInside)
        return view
    }()
    
    lazy var kycVerification : _KycVerification = {
        let view = _KycVerification()
        view.payButton.addTarget(self, action: #selector(onGoToProcessed), for: .touchUpInside)
        view.cancelButton.addTarget(self, action: #selector(onGoToDocuments), for: .touchUpInside)
        return view
    }()
    
    lazy var kycBeingProcessed : _KycProcessed = {
        let view = _KycProcessed()
        view.nextButton.addTarget(self, action: #selector(onGoToVerified), for: .touchUpInside)
        return view
    }()
    
    lazy var kycVerified : _KycVerified = {
        let view = _KycVerified()
        view.nextButton.addTarget(self, action: #selector(onGoToFilesRejected), for: .touchUpInside)
        return view
    }()
    
    lazy var kycFilesRejected : _KycFilesRejected = {
        let view = _KycFilesRejected()
        view.payButton.addTarget(self, action: #selector(onGoToMyReferrals), for: .touchUpInside)
        return view
    }()

    lazy var firstEducationTableView : UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        return tableView
    }()

    lazy var secondRelationShipTableView : UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        return tableView
    }()

    lazy var educationTableView : UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        return tableView
    }()
  
    let listOfEducation = ["PhD", "High school diploma", "Vocational school diploma", "Associate degree", "Graduate", "Master's degree", "Professional degree", "Doctorate degree"]
    
    let listOfRelationShips = ["Single", "Married"]

    let educationCellId = "educationCellId"

    // MARK: - Methods
    
    private func observeKeyboardNotifications() {
       // NotificationCenter.default.addObserver(self, selector: #selector(KeyboardShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(KeyboardHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func KeyboardShow(_ notification: Notification)  {
        
        var y : CGFloat = -120
        if UIDevice.current.screenType == .iPhone_XR ||
            UIDevice.current.screenType == .iPhone_XSMax ||
            UIDevice.current.screenType == .iPhones_6Plus_6sPlus_7Plus_8Plus {
            y  = -100
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.view.frame = CGRect(x: 0, y: y, width: self.view.frame.width, height: self.view.frame.height)
        }, completion: nil)
    }
    
    @objc func KeyboardHide() {
        
        UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        }, completion: nil)
    }

    
    private func registerationOfCells() {
        educationTableView.register(CountryTableViewCell.self, forCellReuseIdentifier: educationCellId)
        firstEducationTableView.register(CountryTableViewCell.self, forCellReuseIdentifier: educationCellId)
        secondRelationShipTableView.register(CountryTableViewCell.self, forCellReuseIdentifier: educationCellId)
    }

    
    private func setupViews() {

        containerView.addSubview(firstPersonalInfoView)
         _ = firstPersonalInfoView.anchor(selectionMenu.bottomAnchor, left: nil, bottom: containerView.bottomAnchor, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 340, heightConstant: 60)
        _ = firstPersonalInfoView.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(secondPersonalInfoView)
        
        _ = secondPersonalInfoView.anchor(selectionMenu.bottomAnchor, left: nil, bottom: containerView.bottomAnchor, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 340, heightConstant: 60)
        _ = secondPersonalInfoView.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(firstAdditionalInfoView)
        
        _ = firstAdditionalInfoView.anchor(selectionMenu.bottomAnchor, left: nil, bottom: containerView.bottomAnchor, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 340, heightConstant: 60)
        _ = firstAdditionalInfoView.centralizeX(containerView.centerXAnchor)

        containerView.addSubview(secondAdditionalInfoView)
        
        _ = secondAdditionalInfoView.anchor(selectionMenu.bottomAnchor, left: nil, bottom: containerView.bottomAnchor, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 340, heightConstant: 60)
        _ = secondAdditionalInfoView.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(kycProofOfAddress)
        
        _ = kycProofOfAddress.anchor(selectionMenu.bottomAnchor, left: containerView.leftAnchor, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, topConstant: 15, leftConstant: 10, bottomConstant: 25, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        _ = kycProofOfAddress.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(kycSelectDocument)
        
        _ = kycSelectDocument.anchor(selectionMenu.bottomAnchor, left: containerView.leftAnchor, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, topConstant: 15, leftConstant: 10, bottomConstant: 25, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        _ = kycSelectDocument.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(kycPropertyAndFinance)
        
        _ = kycPropertyAndFinance.anchor(selectionMenu.bottomAnchor, left: containerView.leftAnchor, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, topConstant: 15, leftConstant: 10, bottomConstant: 25, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        _ = kycPropertyAndFinance.centralizeX(containerView.centerXAnchor)

        containerView.addSubview(kycSchoolGov)
        
        _ = kycSchoolGov.anchor(selectionMenu.bottomAnchor, left: containerView.leftAnchor, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, topConstant: 15, leftConstant: 10, bottomConstant: 25, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        _ = kycSchoolGov.centralizeX(containerView.centerXAnchor)

        containerView.addSubview(kycSubmitForm)
        
        _ = kycSubmitForm.anchor(selectionMenu.bottomAnchor, left: nil, bottom: containerView.bottomAnchor, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 340, heightConstant: 60)
        _ = kycSubmitForm.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(kycVerification)
        
        //kycVerification
        
        _ = kycVerification.anchor(selectionMenu.bottomAnchor, left: nil, bottom: containerView.bottomAnchor, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 340, heightConstant: 60)
        _ = kycVerification.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(kycBeingProcessed)
        
        _ = kycBeingProcessed.anchor(selectionMenu.bottomAnchor, left: nil, bottom: containerView.bottomAnchor, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 340, heightConstant: 60)
        _ = kycBeingProcessed.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(kycVerified)
        
        _ = kycVerified.anchor(selectionMenu.bottomAnchor, left: nil, bottom: containerView.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 340, heightConstant: 60)
        _ = kycVerified.centralizeX(containerView.centerXAnchor)

        containerView.addSubview(kycFilesRejected)
        _ = kycFilesRejected.anchor(selectionMenu.bottomAnchor, left: nil, bottom: containerView.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 340, heightConstant: 60)
        _ = kycFilesRejected.centralizeX(containerView.centerXAnchor)
        
        
        
        containerView.isUserInteractionEnabled = true
        kycSelectDocument.isUserInteractionEnabled = true
        
        firstPersonalInfoView.isHidden = false
        secondPersonalInfoView.isHidden = true
        firstAdditionalInfoView.isHidden = true
        secondAdditionalInfoView.isHidden = true
        kycProofOfAddress.isHidden = true
        kycSelectDocument.isHidden =  true
        kycPropertyAndFinance.isHidden = true
        kycSchoolGov.isHidden = true
        kycSubmitForm.isHidden = true
        kycVerification.isHidden = true
        kycBeingProcessed.isHidden = true
        kycVerified.isHidden = true
        kycFilesRejected.isHidden = true
    }
    
    // MARK: - Navigation Bar
    
    private func initNavBar() {
        self.navigationItem.title = "Personal Info"
    }
    
    // MARK: - TableView
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == firstEducationTableView {
            
            firstAdditionalInfoView.highestLevelOfEducation.text = listOfEducation[indexPath.row]
            firstAdditionalInfoView.highestLevelOfEducationDropDown.isHidden = true
            firstAdditionalInfoView.highestLevelOfEducation.removeToolTip()
            
        } else if tableView == secondRelationShipTableView {
            secondAdditionalInfoView.relationship.text = listOfRelationShips[indexPath.row]
            secondAdditionalInfoView.relationshipDropDown.isHidden = true
            secondAdditionalInfoView.relationship.removeToolTip()
        } else {
        
            secondAdditionalInfoView.highestLevelOfEducation.text = listOfEducation[indexPath.row]
            secondAdditionalInfoView.highestLevelOfEducationDropDown.isHidden = true
            secondAdditionalInfoView.highestLevelOfEducation.removeToolTip()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == secondRelationShipTableView {
            return self.listOfRelationShips.count
        }
        return self.listOfEducation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: educationCellId, for: indexPath as IndexPath) as! CountryTableViewCell
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        if tableView == secondRelationShipTableView {
            cell.countryName.text = listOfRelationShips[indexPath.row]
        } else {
            cell.countryName.text = listOfEducation[indexPath.row]
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    
    // MARK: - Events

    @objc private func goToWallet() {
        AppDelegate.shared?.perform(.myWallet)
    }
    
    @objc private func onGoToFirstPersonalInfo() {
        
        containerView.endEditing(true)
        firstPersonalInfoView.isHidden = false
        secondPersonalInfoView.isHidden = true
        menuItems[1].isSelected = true
        selectionMenuViewCollection.reloadData()
    }
    
    @objc private func onGoSecondPersonalInfo() {
        
        containerView.endEditing(true)
        if firstPersonalInfoView.validate() {
            firstPersonalInfoView.isHidden = true
            secondPersonalInfoView.isHidden = false
            firstAdditionalInfoView.isHidden = true
            menuItems[1].isSelected = true
            selectionMenuViewCollection.reloadData()
        }
    }
    
    @objc private func onUpdatePersonalInfo() {
        
        containerView.endEditing(true)
        if secondPersonalInfoView.validate() {
            firstPersonalInfoView.isHidden = true
            secondPersonalInfoView.isHidden = true
            firstAdditionalInfoView.isHidden = false
            secondAdditionalInfoView.isHidden = true
            menuItems[2].isSelected = true
            selectionMenuViewCollection.reloadData()
            backgroundImage.image = UIImage(named: "AdditionalInfoImage")
            self.navigationItem.title = "Additional Info"
        }
    }
    
    @objc private func onAddInfoNext() {
        
        view.endEditing(true)
        if firstAdditionalInfoView.validate() {
            firstPersonalInfoView.isHidden = true
            secondPersonalInfoView.isHidden = true
            firstAdditionalInfoView.isHidden = true
            secondAdditionalInfoView.isHidden = false
            kycProofOfAddress.isHidden = true
            menuItems[3].isSelected = true
            selectionMenuViewCollection.reloadData()
        }
    }
    
    @objc private func onSaveAdditionalInfo() {
        
        view.endEditing(true)
        if secondAdditionalInfoView.validate() {
            firstPersonalInfoView.isHidden = true
            secondPersonalInfoView.isHidden = true
            firstAdditionalInfoView.isHidden = true
            secondAdditionalInfoView.isHidden = true
            kycProofOfAddress.isHidden = false
            kycSelectDocument.isHidden = true
            kycPropertyAndFinance.isHidden = true
            kycSchoolGov.isHidden = true
            menuItems[4].isSelected = true
            selectionMenuViewCollection.reloadData()
            backgroundImage.image = UIImage(named: "KYCValidationImage")
            self.navigationItem.title = "KYC Validation"
        }
    }
    
    @objc private func onPOfANext() {
        
        view.endEditing(true)
        
        if kycProofOfAddress.selectedIndex == 1 {
            
            firstPersonalInfoView.isHidden = true
            secondPersonalInfoView.isHidden = true
            firstAdditionalInfoView.isHidden = true
            secondAdditionalInfoView.isHidden = true
            kycProofOfAddress.isHidden = true
            kycSelectDocument.isHidden = false
            kycPropertyAndFinance.isHidden = true
            kycSchoolGov.isHidden = true
            kycSubmitForm.isHidden = true
            selectionMenuViewCollection.reloadData()
            
        } else if kycProofOfAddress.selectedIndex == 2 {
            
            firstPersonalInfoView.isHidden = true
            secondPersonalInfoView.isHidden = true
            firstAdditionalInfoView.isHidden = true
            secondAdditionalInfoView.isHidden = true
            kycProofOfAddress.isHidden = true
            kycSelectDocument.isHidden = true
            kycPropertyAndFinance.isHidden = false
            kycSchoolGov.isHidden = true
            kycSubmitForm.isHidden = true
            selectionMenuViewCollection.reloadData()
            
        } else if kycProofOfAddress.selectedIndex == 3 {
            
            firstPersonalInfoView.isHidden = true
            secondPersonalInfoView.isHidden = true
            firstAdditionalInfoView.isHidden = true
            secondAdditionalInfoView.isHidden = true
            kycProofOfAddress.isHidden = true
            kycSelectDocument.isHidden = true
            kycPropertyAndFinance.isHidden = true
            kycSchoolGov.isHidden = false
            kycSubmitForm.isHidden = true
            selectionMenuViewCollection.reloadData()
        }
    }
    
    @objc private func onGoToDocuments() {
        
        view.endEditing(true)
        firstPersonalInfoView.isHidden = true
        secondPersonalInfoView.isHidden = true
        firstAdditionalInfoView.isHidden = true
        secondAdditionalInfoView.isHidden = true
        kycProofOfAddress.isHidden = true
        kycSelectDocument.isHidden = true
        kycPropertyAndFinance.isHidden = true
        kycSchoolGov.isHidden = true
        kycSubmitForm.isHidden = false
        kycVerification.isHidden = true
        selectionMenuViewCollection.reloadData()
    }
    
    @objc private func onGoToKycVerification() {
        
        view.endEditing(true)
        firstPersonalInfoView.isHidden = true
        secondPersonalInfoView.isHidden = true
        firstAdditionalInfoView.isHidden = true
        secondAdditionalInfoView.isHidden = true
        kycProofOfAddress.isHidden = true
        kycVerification.isHidden = false
        kycSubmitForm.isHidden = true
        selectionMenuViewCollection.reloadData()
    }
    
    @objc private func onGoToProcessed() {
        
        view.endEditing(true)
        firstPersonalInfoView.isHidden = true
        secondPersonalInfoView.isHidden = true
        firstAdditionalInfoView.isHidden = true
        secondAdditionalInfoView.isHidden = true
        kycProofOfAddress.isHidden = true
        kycSubmitForm.isHidden = true
        kycVerification.isHidden = true
        kycBeingProcessed.isHidden = false
        selectionMenuViewCollection.reloadData()
        self.navigationItem.title = "Verifying KYC Files"
    }
 
    @objc private func onGoToVerified() {
        view.endEditing(true)
        firstPersonalInfoView.isHidden = true
        secondPersonalInfoView.isHidden = true
        firstAdditionalInfoView.isHidden = true
        secondAdditionalInfoView.isHidden = true
        kycProofOfAddress.isHidden = true
        kycSubmitForm.isHidden = true
        kycBeingProcessed.isHidden = true
        kycVerified.isHidden = false
        selectionMenuViewCollection.reloadData()
        self.navigationItem.title = "KYC Files Verified"
    }
    
    @objc private func onGoToFilesRejected() {
        view.endEditing(true)
        firstPersonalInfoView.isHidden = true
        secondPersonalInfoView.isHidden = true
        firstAdditionalInfoView.isHidden = true
        secondAdditionalInfoView.isHidden = true
        kycProofOfAddress.isHidden = true
        kycSubmitForm.isHidden = true
        kycBeingProcessed.isHidden = true
        kycVerified.isHidden = true
        kycFilesRejected.isHidden = false
        selectionMenuViewCollection.reloadData()
        self.navigationItem.title = "KYC Files Rejected"
    }
 
    @objc private func onGoToMyReferrals() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MyReferralsViewController") as! MyReferralsViewController
        let navigationController = UINavigationController(rootViewController: vc)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navigationController
    }
    
    @objc private func onFirstEducationDropDownIconClick(_button: UIButton) {
        
        if firstAdditionalInfoView.highestLevelOfEducationDropDown.isHidden {
            firstAdditionalInfoView.highestLevelOfEducationDropDown.isHidden = false
            firstAdditionalInfoView.highestLevelOfEducationDropDown.layer.zPosition = 1
            firstAdditionalInfoView.highestLevelOfEducationDropDown.ContainerView.addSubview(firstEducationTableView)
            
            _ = firstEducationTableView.anchor(firstAdditionalInfoView.highestLevelOfEducationDropDown.ContainerView.topAnchor, left: firstAdditionalInfoView.highestLevelOfEducationDropDown.ContainerView.leftAnchor, bottom: firstAdditionalInfoView.highestLevelOfEducationDropDown.ContainerView.bottomAnchor, right: firstAdditionalInfoView.highestLevelOfEducationDropDown.ContainerView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
            
            educationTableView.reloadData()
            firstAdditionalInfoView.highestLevelOfEducation.removeToolTip()
        }
    }
    
    @objc private func onFirstCloseDropDownList(_button : UIButton) {
        firstAdditionalInfoView.highestLevelOfEducationDropDown.isHidden = true
    }
    
    @objc private func relationshipDropDownIconClick(_button: UIButton) {
        
        if secondAdditionalInfoView.relationshipDropDown.isHidden {
            secondAdditionalInfoView.relationshipDropDown.isHidden = false
            secondAdditionalInfoView.relationshipDropDown.layer.zPosition = 1
            secondAdditionalInfoView.relationshipDropDown.ContainerView.addSubview(secondRelationShipTableView)
            
            _ = secondRelationShipTableView.anchor(secondAdditionalInfoView.relationshipDropDown.ContainerView.topAnchor, left: secondAdditionalInfoView.relationshipDropDown.ContainerView.leftAnchor, bottom: secondAdditionalInfoView.relationshipDropDown.ContainerView.bottomAnchor, right: secondAdditionalInfoView.relationshipDropDown.ContainerView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
            
            secondRelationShipTableView.reloadData()
            secondAdditionalInfoView.relationship.removeToolTip()
        }
    }
    
    @objc private func onEducationDropDownIconClick(_button: UIButton) {
        
        if secondAdditionalInfoView.highestLevelOfEducationDropDown.isHidden {
            secondAdditionalInfoView.highestLevelOfEducationDropDown.isHidden = false
            secondAdditionalInfoView.highestLevelOfEducationDropDown.layer.zPosition = 1
            secondAdditionalInfoView.highestLevelOfEducationDropDown.ContainerView.addSubview(educationTableView)
            
            _ = educationTableView.anchor(secondAdditionalInfoView.highestLevelOfEducationDropDown.ContainerView.topAnchor, left: secondAdditionalInfoView.highestLevelOfEducationDropDown.ContainerView.leftAnchor, bottom: secondAdditionalInfoView.highestLevelOfEducationDropDown.ContainerView.bottomAnchor, right: secondAdditionalInfoView.highestLevelOfEducationDropDown.ContainerView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
            
            educationTableView.reloadData()
            secondAdditionalInfoView.highestLevelOfEducation.removeToolTip()
        }
    }
    
    @objc private func onCloseDropDownList(_button : UIButton) {
        secondAdditionalInfoView.highestLevelOfEducationDropDown.isHidden = true
        secondAdditionalInfoView.relationshipDropDown.isHidden = true
    }
    
    // MARK: - TextField
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }

    @objc func textFieldDidChange(_ textField: SCXTextField) {
        if !textField.isEmpty() {
            textField.removeToolTip()
            textField.parentPanel.setTopLabel(widthValue: textField.floatingLabelValue)
            
            if textField == secondPersonalInfoView.zipCode {
                
                let zipCodeTextLength = secondPersonalInfoView.zipCodeInfo.count
                var widthValueOfLabel = 175
                if zipCodeTextLength >= 18 && zipCodeTextLength <= 24 {
                    widthValueOfLabel = 175
                } else if zipCodeTextLength > 24 && zipCodeTextLength <= 30 {
                    widthValueOfLabel = 205
                } else if zipCodeTextLength > 30 && zipCodeTextLength <= 40 {
                    widthValueOfLabel = 220
                } else if zipCodeTextLength > 40 {
                    widthValueOfLabel = 290
                }
                
                textField.parentPanel.setFloatingLabel(label: secondPersonalInfoView.zipCodeInfo, widthValue: CGFloat(widthValueOfLabel))
            }

        } else {
            textField.parentPanel.setTopLabel(widthValue: 0, hideText: true)
        }
    }
    
    // MARK: - ViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        registerationOfCells()
        initNavBar()
        setupViews()
        observeKeyboardNotifications()
    }
    
    override func viewDidLayoutSubviews() {
        firstPersonalInfoView.finalizeMe()
        secondPersonalInfoView.finalizeMe()
        firstAdditionalInfoView.finalizeMe()
        secondAdditionalInfoView.finalizeMe()
        kycProofOfAddress.finalizeMe()
        kycSelectDocument.finalizeMe()
        kycPropertyAndFinance.finalizeMe()
        kycSchoolGov.finalizeMe()
        kycVerification.finalizeMe()
        kycSubmitForm.finalizeMe()
        kycBeingProcessed.finalizeMe()
        kycFilesRejected.finalizeMe()
        secondAdditionalInfoView.relationshipDropDown.setup(label: "Relationship")
        secondAdditionalInfoView.highestLevelOfEducationDropDown.setup(label: "Highest Level of Education")
        firstAdditionalInfoView.highestLevelOfEducationDropDown.setup(label: "Highest Level of Education")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}
