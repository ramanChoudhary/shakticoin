//
//  CongratViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 10/27/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class CongratsViewController: UIViewController, UITextViewDelegate {

    // MARK: - Declarations
    var bonusAmount: Int?
    
    lazy var backgroundImage: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "New Bounty Image")
        //iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    lazy var coverView: UIView = {
        let cv = UIView()
        //cv.contentMode = .scaleAspectFill
        cv.alpha = 0.75
        cv.backgroundColor = UIColor.black
        return cv
    }()

    lazy var congratsLbl : UILabel = {
        let label = UILabel()
        label.text = "Congrats".localized
        label.font = UIFont.lato(fontSize: 24, type: .Light)
        label.textColor = .white
        return label
    }()
    
    lazy var bbBalanceLbl : UILabel = {
        let label = UILabel()
        label.text = "YourNewBB".localized
        label.font = UIFont.lato(fontSize: 12, type: .Bold)
        label.textColor = .white
        return label
    }()
    
    lazy var currentBalance : UILabel = {
        let label = UILabel()
        label.text = "SXE \(bonusAmount ?? 0).00"
        label.font = UIFont.lato(fontSize: 36, type: .Bold) //TODO medium
        label.textColor = .white
        return label
    }()
    
    lazy var moneyImg : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "Money")
        imageView.tintColor = UIColor.mainColor()
        return imageView
    }()
    
    lazy var everyoneIsWinnerLbl : UILabel = {
        let label = UILabel()
        let attributedText = Util.NSMAttrString(text: "Everyone".localized, font: UIFont.lato(fontSize: 24, type: .Bold)) // TODO medium
        attributedText.append(Util.NSAttrString(text: "AWinner".localized, font: UIFont.lato(fontSize: 24, type: .Light)))
        label.attributedText = attributedText
        return label
    }()
    
    lazy var registerMyReferral : SCButton = {
        let button = SCButton()
        button.setTitle("RegisterMyRef".localized, for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        button.titleLabel?.textColor = .white
        button.CornerRadius = 18
        button.addTarget(self, action: #selector(onReferralRegistration), for: .touchUpInside)
        return button
    }()
    
    lazy var viewTermsTextView: UITextView = {
        let textView = UITextView()
        textView.delegate = self
        textView.isEditable = false
        textView.backgroundColor = .clear
        
        let attributedText = NSMutableAttributedString(string: "* View terms associated with wallet usage.", attributes: [NSAttributedString.Key.font: UIFont.lato(fontSize: 10, type: .Light), NSAttributedString.Key.foregroundColor: UIColor.white])

        attributedText.append(NSAttributedString(string: "\nRefer to the terms for more detailed conditions associated with", attributes: [NSAttributedString.Key.font: UIFont.lato(fontSize: 10, type: .Light), NSAttributedString.Key.foregroundColor: UIColor.white]))
        
        attributedText.append(NSAttributedString(string: "\nclaiming your bounty. You must sign up for a wallet first and", attributes: [NSAttributedString.Key.font: UIFont.lato(fontSize: 10, type: .Light), NSAttributedString.Key.foregroundColor: UIColor.white]))
        
        attributedText.append(NSAttributedString(string: "\nensure you have completed KYC to be eligible.", attributes: [NSAttributedString.Key.font: UIFont.lato(fontSize: 10, type: .Light), NSAttributedString.Key.foregroundColor: UIColor.white]))
        
        attributedText.addAttributes([NSAttributedString.Key.link : "_viewTerms"], range: NSRange(location: 2, length: 10))
        attributedText.addAttributes([NSAttributedString.Key.link : "_referToTerms"], range: NSRange(location: 42, length: 20))

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        
        let length = attributedText.string.count
        
        let linkAttributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.underlineColor: UIColor.lightGray,
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]

        attributedText.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: length))
        
        textView.linkTextAttributes = linkAttributes
        textView.attributedText = attributedText
        textView.isUserInteractionEnabled = false
        
        return textView
    }()
   
    // MARK: - Methods
    
    private func setupViews() {
        
        view.addSubview(backgroundImage)
        view.addSubview(coverView)
        view.addSubview(congratsLbl)
        view.addSubview(bbBalanceLbl)
        view.addSubview(currentBalance)
        view.addSubview(moneyImg)
        view.addSubview(everyoneIsWinnerLbl)
        view.addSubview(registerMyReferral)
        view.addSubview(viewTermsTextView)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = coverView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = congratsLbl.anchor(view.safeAreaLayoutGuide.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 55, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = congratsLbl.centralizeX(view.centerXAnchor)
        
        _ = bbBalanceLbl.anchor(congratsLbl.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = bbBalanceLbl.centralizeX(view.centerXAnchor)
        
        _ = currentBalance.anchor(bbBalanceLbl.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 30, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = currentBalance.centralizeX(view.centerXAnchor)
        
        _ = moneyImg.centralizeX(view.centerXAnchor)
        _ = moneyImg.centralizeY(view.centerYAnchor, constant: -15)
        
        _ = everyoneIsWinnerLbl.anchorToTop(moneyImg.bottomAnchor, topConstant: 45)
        _ = everyoneIsWinnerLbl.centralizeX(view.centerXAnchor)
        
        _ = registerMyReferral.anchor(everyoneIsWinnerLbl.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 115, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 280, heightConstant: 36)
        
        _ = registerMyReferral.centralizeX(view.centerXAnchor)
        
        _ = viewTermsTextView.anchor(registerMyReferral.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 25, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 280, heightConstant: 75)
        _ = viewTermsTextView.centralizeX(view.centerXAnchor)
    }
    
    // MARK: - Events
    
    @objc private func onReferralRegistration() {
        let referralRegistrationVC: ReferralRegistrationViewController = ReferralRegistrationViewController()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = referralRegistrationVC
    }
    
    // MARK: - ViewController
 
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    // MARK: - Delegates
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        print(URL)
        return false
    }
}
