//
//  LoginViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 10/23/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit
import SideMenu

class SCLeftRadiusedButton1: UIView {
    
    // MARK: - Methods
    
    func setup() {
        let r = self.bounds.size.height / 2
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .bottomLeft], cornerRadii: CGSize(width: r, height: r))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    // MARK: - UIView
    override func layoutSubviews() { setup() } // "layoutSubviews" is best
}

class SCRightRadiusedButton1: UIView {
    
    // MARK: - Methods
    
    func setup() {
        let r = self.bounds.size.height / 2
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topRight, .bottomRight], cornerRadii: CGSize(width: r, height: r))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    // MARK: - UIView
    override func layoutSubviews() { setup() } // "layoutSubviews" is best
}


class LoginViewController: UIViewController, UITextFieldDelegate {
    // MARK: - Declarations
    
    lazy var logoView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "Logo")
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    lazy var backgroundImage: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "Login Image")
        //iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    lazy var coverView: UIView = {
        let cv = UIView()
        //cv.contentMode = .scaleAspectFill
        cv.alpha = 0.75
        cv.backgroundColor = UIColor.black
        return cv
    }()
    
    lazy var welcomeTextView: UILabel = {
        let tv = UILabel()
        tv.text = "WelcomeToShaktiCoin".localized
        tv.font = UIFont.lato(fontSize: 24, type: Lato.Light)
        tv.textColor = .white
        tv.textAlignment = .center
        return tv
    }()

    lazy var emailAddressPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var emailAddressField : SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 120
        textField.delegate = self
        textField.font = UIFont.lato(fontSize: 14)
        textField.textColor = .white
        textField.isSecureTextEntry = false
        textField.attributedPlaceholder = Util.NSMAttrString(text: "EmailAddress".localized, font: UIFont.lato(fontSize: 14))
        textField.isAccessibilityElement = true
        textField.accessibilityIdentifier = "EmailAddress"
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField.keyboardType = UIKeyboardType.emailAddress
        textField.autocapitalizationType = .none
        //textField.text = "testUser_pooja2@gmail.com"
        return textField
    }()
    
    lazy var pwdFieldPanel : SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var pwdField: SCXTextField = {
        let textField = SCXTextField()
        textField.delegate = self
        textField.floatingLabelValue = 97
        textField.isSecureTextEntry = true
        textField.font = UIFont.lato(fontSize: 14)
        textField.textColor = .white
        textField.isAccessibilityElement = true
        textField.accessibilityIdentifier = "pwdField"
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Password".localized, font: UIFont.lato(fontSize: 14))
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        //textField.text = "123"
        return textField
    }()
    
    lazy var forgotPwdLbl : UILabel = {
        let label = UILabel()
        label.text = "ForgotPassword".localized
        label.underline()
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 14)
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(onPasswordRecovery))
        label.addGestureRecognizer(tap)
        return label
    }()

    lazy var loginButtonContainer : SCLeftRadiusedButton1 = {
        let view = SCLeftRadiusedButton1()
        view.backgroundColor = .gray
        return view
    }()
    
    lazy var registerButtonContainer : SCRightRadiusedButton1 = {
        let view = SCRightRadiusedButton1()
        view.backgroundColor = .gray
        return view
    }()
    
    lazy var btnLogin : SCButton = {
        let btn = SCButton()
        btn.backgroundColor = .red
        btn.setTitle("Login".localized, for: .normal)
        btn.accessibilityIdentifier = "Login"
        btn.isAccessibilityElement = true
        btn.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        btn.titleLabel?.textColor = .white
        btn.addTarget(self, action: #selector(onLogin), for: .touchUpInside)
        return btn
    }()
    
    lazy var btnRegister : SCButton = {
        let btn = SCButton()
        btn.backgroundColor = .red
        btn.setTitle("CreateShaktiID".localized, for: .normal)
        btn.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        btn.titleLabel?.textColor = .white
        btn.addTarget(self, action: #selector(gotoRegistrationScreen), for: .touchUpInside)
        return btn
    }()

    lazy var waitingIndicator:UIActivityIndicatorView = {
        let wi: UIActivityIndicatorView = UIActivityIndicatorView()
        wi.frame = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: 40.0, height: 40.0));
        wi.center = self.view.center
        wi.hidesWhenStopped = true
        if #available(iOS 13.0, *) {
            wi.style = UIActivityIndicatorView.Style.large
        }
        return wi
    }()
    
    let userNameTT = SCToolTip()
    let passwordTT = SCToolTip()
   
    // MARK: - Methods

    fileprivate func observeKeyboardNotifications(){
        
       // NotificationCenter.default.addObserver(self, selector: #selector(KeyboardShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(KeyboardHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func KeyboardShow() {
        
        registerButtonContainer.frame = CGRect(x: registerButtonContainer.frame.origin.x, y: registerButtonContainer.frame.origin.y-50, width: registerButtonContainer.frame.size.width, height: registerButtonContainer.frame.size.height)
        
        loginButtonContainer.frame = CGRect(x: loginButtonContainer.frame.origin.x, y: loginButtonContainer.frame.origin.y-50, width: loginButtonContainer.frame.size.width, height: loginButtonContainer.frame.size.height)
        
        var y: CGFloat = -145
        if UIDevice.current.screenType == .iPhone_XR ||
            UIDevice.current.screenType == .iPhone_XSMax ||
            UIDevice.current.screenType == .iPhones_X_XS ||
            UIDevice.current.screenType == .iPhones_6Plus_6sPlus_7Plus_8Plus {
            y = -170
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.view.frame = CGRect(x: 0, y: y, width: self.view.frame.width, height: self.view.frame.height)
        }, completion: nil)
    }
    
    @objc func KeyboardHide() {
        
        registerButtonContainer.frame = CGRect(x: registerButtonContainer.frame.origin.x, y: registerButtonContainer.frame.origin.y+50, width: registerButtonContainer.frame.size.width, height: registerButtonContainer.frame.size.height)
        
        loginButtonContainer.frame = CGRect(x: loginButtonContainer.frame.origin.x, y: loginButtonContainer.frame.origin.y+50, width: loginButtonContainer.frame.size.width, height: loginButtonContainer.frame.size.height)
        
        UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        }, completion: nil)
    }

    private func setupViews() {
        
        observeKeyboardNotifications()
        
        view.addSubview(backgroundImage)
        view.addSubview(coverView)
        view.addSubview(logoView)
        view.addSubview(welcomeTextView)
        //view.addSubview(emailAddressField)
        view.addSubview(emailAddressPanel)
        //view.addSubview(pwdField)
        view.addSubview(pwdFieldPanel)
        //view.addSubview(checkBoxView)
        //view.addSubview(rememberMeLbl)
        view.addSubview(forgotPwdLbl)
        view.addSubview(loginButtonContainer)
        view.addSubview(registerButtonContainer)
        view.addSubview(waitingIndicator)
        //view.addSubview(registrationLabel)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = coverView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = logoView.anchorToTop(view.safeAreaLayoutGuide.topAnchor, topConstant: 70)
        _ = logoView.centralizeX(view.centerXAnchor, constant: 0)
        
        _ = welcomeTextView.anchor(logoView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
        _ = welcomeTextView.centralizeX(view.centerXAnchor, constant: 0)
        
        /*_ = emailAddressField.anchor(welcomeTextView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 75, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = emailAddressField.centralizeX(view.centerXAnchor, constant: 0)*/
        
        _ = emailAddressPanel.anchor(welcomeTextView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 75, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = emailAddressPanel.centralizeX(view.centerXAnchor, constant: 0)
        
        emailAddressPanel.ContainerView.addSubview(emailAddressField)
        emailAddressField.parentPanel = emailAddressPanel
        Util.fullyAnchor(_parnetControl: emailAddressPanel.ContainerView, _childControl: emailAddressField)

        _ = pwdFieldPanel.anchor(emailAddressPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = pwdFieldPanel.centralizeX(view.centerXAnchor, constant: 0)
        
        pwdFieldPanel.ContainerView.addSubview(pwdField)
        pwdField.parentPanel = pwdFieldPanel
        Util.fullyAnchor(_parnetControl: pwdFieldPanel.ContainerView, _childControl: pwdField)
        
        _ = forgotPwdLbl.anchor(pwdFieldPanel.topAnchor, left: nil, bottom: nil, right: pwdFieldPanel.rightAnchor, topConstant: 60, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
       
        // Buttons ------------
        
        var defaultWidth: CGFloat = 148
        var bottomValue : CGFloat = 120
        if UIDevice.current.screenType == .iPhone_XR ||
            UIDevice.current.screenType == .iPhone_XSMax ||
            UIDevice.current.screenType == .iPhones_6Plus_6sPlus_7Plus_8Plus {
            defaultWidth = 160//166.8
            bottomValue = 195
        }
 
        _ = loginButtonContainer.anchor(nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: nil, topConstant: 0, leftConstant: 39, bottomConstant: bottomValue, rightConstant: 0, widthConstant: defaultWidth, heightConstant: 36)
        
        loginButtonContainer.addSubview(btnLogin)
        
        _ = btnLogin.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: defaultWidth, heightConstant: 36)
        _ = btnLogin.centralizeX(loginButtonContainer.centerXAnchor)
        _ = btnLogin.centralizeY(loginButtonContainer.centerYAnchor)
        
        _ = registerButtonContainer.anchor(nil, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: bottomValue, rightConstant: 39, widthConstant: defaultWidth, heightConstant: 36)
        
        registerButtonContainer.backgroundColor = .red
        
        registerButtonContainer.addSubview(btnRegister)
        
        _ = btnRegister.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: defaultWidth, heightConstant: 36)
        _ = btnRegister.centralizeX(registerButtonContainer.centerXAnchor)
        _ = btnRegister.centralizeY(registerButtonContainer.centerYAnchor)

        // End Buttons ------------

    }
    
    private func setupToolTips() {
        emailAddressField.setToolTip(scTT: userNameTT)
        pwdField.setToolTip(scTT: passwordTT)
    }
    
    func validate() -> Bool {
        
        var isValid: Bool = true
        
        if emailAddressField.isEmpty() {
            view.addSubview(userNameTT)
            userNameTT.setRelativeXControl(uiControl: emailAddressField)
            userNameTT.setMessage(message: "EmailEmpty".localized)
            isValid = false
            
        } else {
            
            if !Validator.shared().isValidEmail(email: emailAddressField.text!) {
                view.addSubview(userNameTT)
                userNameTT.setRelativeXControl(uiControl: emailAddressField)
                userNameTT.setMessage(message: "EmailInvalid".localized)
                isValid = false
            }
        }
        
        if pwdField.isEmpty() {
            view.addSubview(passwordTT)
            passwordTT.setRelativeXControl(uiControl: pwdField)
            passwordTT.setMessage(message: "PasswordEmpty".localized)
            isValid = false
        } else {
            if !Validator.shared().isValidPassword(password: pwdField.text!) {
                view.addSubview(passwordTT)
                passwordTT.setRelativeXControl(uiControl: pwdField)
                passwordTT.setMessage(message: "PasswordInvalid".localized)
                isValid = false
            }
        }
        
        return isValid
    }
    
    func onlineValidation() {
        
        if !emailAddressField.isEmpty() {
            if !Validator.shared().isValidEmail(email: emailAddressField.text!) {
                emailAddressField.RightIcon = UIImage(named: "Invalid")
            } else {
                emailAddressField.RightIcon = UIImage(named: "Valid")
            }
        } else {
            emailAddressField.RightIcon = nil
        }
        
        if !pwdField.isEmpty() {
            if !Validator.shared().isValidPassword(password: pwdField.text!) {
                pwdField.RightIcon = UIImage(named: "Invalid")
            } else {
                pwdField.RightIcon = UIImage(named: "Valid")
            }
        } else {
            pwdField.RightIcon = nil
        }
    }
    
    // MARK: - Events
    
    @objc func onPasswordRecovery(sender:UITapGestureRecognizer) {
        self.view.endEditing(true)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = PasswordRecoveryEnterEmailWireframe.createModule()
    }
    
    @objc func gotoRegistrationScreen() {
        self.view.endEditing(true)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = OnboardingEnterEmailWireframe.createModule()
    }

    @objc func onLogin() {
        view.endEditing(true)
        if validate() {
            showLoader()
            self.signIn {
                self.hideLoader()
                if let error = $0 {
                    let alert = UIAlertController(title: "Login", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = SideMenuNavigationController(rootViewController: MyWalletRouter.createMyWalletModule())
                }
            }
        }
    }
    
    func signIn(callback: @escaping (ErrorModel?) -> Void) {
        let service = AccountService()
        service.login(username: emailAddressField.text!, password: pwdField.text!) { res in
            ViewDispatcher.shared.execute {
                callback(res)
            }
        }
    }
    
    // MARK: - TextField
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
    @objc func textFieldDidChange(_ textField: SCXTextField) {
        onlineValidation()
        if !textField.isEmpty() {
            textField.removeToolTip()
            textField.parentPanel.setTopLabel(widthValue: textField.floatingLabelValue)
        } else {
            textField.parentPanel.setTopLabel(widthValue: 0, hideText: true)
        }
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {}
    
    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupToolTips()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    override func viewDidLayoutSubviews() {
        emailAddressPanel.setup(label: "EmailAddress".localized)
        pwdFieldPanel.setup(label: "Password".localized)
    }
    
}
