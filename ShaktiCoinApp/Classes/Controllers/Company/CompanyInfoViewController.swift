//
//  CompanyInfoViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 2/27/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import UIKit

class CompanyInfoViewController: SMBaseViewController {

    // MARK: - Declarations

    lazy var companyBasicInfo : _CompanyBasicInfo = {
        let view = _CompanyBasicInfo()
        view.companyName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.tradeName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.structure.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.dateOfE.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.iCode.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.vaultId.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.nextButton.addTarget(self, action: #selector(nextButton(_sender:)), for: .touchUpInside)
        return view
    }()
    
    lazy var companyResInfo : _CompanyResInfo = {
        let view = _CompanyResInfo()
        view.dCountry.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.dState.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.city.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.addressLine1.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.addressLine2.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.zipCode.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.nextButton.addTarget(self, action: #selector(nextButton(_sender:)), for: .touchUpInside)
        view.isHidden = true
        return view
    }()
    
    lazy var companyTellMore : _CompanyTellMore = {
        let view = _CompanyTellMore()
        view.position.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.emailAddress.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.phoneNumber.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.lastName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.walletId.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.nextButton.addTarget(self, action: #selector(nextButton(_sender:)), for: .touchUpInside)
        view.isHidden = true
        return view
    }()
    
    lazy var affCompanies : _AffComanies = {
        let view = _AffComanies()
        view.corporateName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.emailAddress.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.relashionship.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.sector.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.nextButton.addTarget(self, action: #selector(nextButton(_sender:)), for: .touchUpInside)
        view.isHidden = true
        return view
    }()
    
    // MARK: - Methods
    
    private func initViews() {
        
        containerView.addSubview(companyBasicInfo)
         _ = companyBasicInfo.anchor(selectionMenu.bottomAnchor, left: nil, bottom: containerView.bottomAnchor, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 340, heightConstant: 60)
        _ = companyBasicInfo.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(companyResInfo)
        _ = companyResInfo.anchor(selectionMenu.bottomAnchor, left: nil, bottom: containerView.bottomAnchor, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 340, heightConstant: 60)
        _ = companyResInfo.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(companyTellMore)
        _ = companyTellMore.anchor(selectionMenu.bottomAnchor, left: nil, bottom: containerView.bottomAnchor, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 340, heightConstant: 60)
        _ = companyTellMore.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(affCompanies)
        _ = affCompanies.anchor(selectionMenu.bottomAnchor, left: nil, bottom: containerView.bottomAnchor, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 340, heightConstant: 60)
        _ = affCompanies.centralizeX(containerView.centerXAnchor)
    }
    
    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initNavBar()
        initViews()
        
        //let vc = CompanyCheckListViewController()
        //self.navigationController?.pushViewController(vc, animated: true)
    }

    override func viewDidLayoutSubviews() {
        companyBasicInfo.finalizeMe()
        companyResInfo.finalizeMe()
        companyTellMore.finalizeMe()
        affCompanies.finalizeMe()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    // MARK: - NavBar
    
    private func initNavBar() {
        self.navigationItem.title = "Company Info"
    }
        
    // MARK: - TextField
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }

    @objc func textFieldDidChange(_ textField: SCXTextField) {
        if !textField.isEmpty() {
            textField.removeToolTip()
            textField.parentPanel.setTopLabel(widthValue: textField.floatingLabelValue)
        } else {
            textField.parentPanel.setTopLabel(widthValue: 0, hideText: true)
        }
    }
    
    // MARK: Events
    
    @objc func nextButton(_sender: SCButton) {
        
        if _sender.tag == 1 {
            
            companyBasicInfo.isHidden = true
            companyResInfo.isHidden = false
            companyTellMore.isHidden = true
            affCompanies.isHidden = true
            
        } else if _sender.tag == 2 {
            
            companyBasicInfo.isHidden = true
            companyResInfo.isHidden = true
            companyTellMore.isHidden = false
            affCompanies.isHidden = true
            
        } else if _sender.tag == 3 {
            
            companyBasicInfo.isHidden = true
            companyResInfo.isHidden = true
            companyTellMore.isHidden = true
            affCompanies.isHidden = false
        
        } else if _sender.tag == 4 {
            
            let vc = CompanyCheckListViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
