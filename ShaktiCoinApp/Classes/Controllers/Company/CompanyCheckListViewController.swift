//
//  CompanyCheckListViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 3/5/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import UIKit

class CompanyCheckListViewController: SingleBaseViewController, UITableViewDelegate, UITableViewDataSource  {

    // MARK: - Declarations

    lazy var companyTitle : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "Company XYZ"
        label.font = UIFont.lato(fontSize: 36, type: .Regular) //TODO Medium
        return label
    }()
    
    lazy var tableView : UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        return tableView
    }()
    
    let companyCellId: String = "companyCellId"
    
    /*
     let notificationItems: [NotificationItem] = {
         let first = NotificationItem(title: "_FirstTitle".localized, message: "_FirstMessage".localized, date: "06/10/2018", isRead: nil)
         let second = NotificationItem(title: "_SecondTitle".localized, message: "_SecondMessage".localized, date: "05/10/2018", isRead: nil)
         let third = NotificationItem(title: "_ThirdTitle".localized, message: "_ThirdMessage".localized, date: "05/10/2018", isRead: nil)
         return [first, second, third]
     }()
     */
    
    let companyDetailList: [CompanyDetail] = {
        let vaultId = CompanyDetail(field: "VaultID", value: "Lorem ipsum dolor")
        let name = CompanyDetail(field: "Company Name", value: "Lorem ipsum")
        let trade = CompanyDetail(field: "Trade Name", value: "Lorem ipsum")
        let structure = CompanyDetail(field: "Company Structure", value: "35446")
        let dateOfE = CompanyDetail(field: "Date of Establishment", value: "02-02-2020")
        let iCode = CompanyDetail(field: "Industry Code", value: "Locked")
        let dCountry = CompanyDetail(field: "Domiciled Country", value: "US")
        let state = CompanyDetail(field: "Company Name", value: "MN")
        let city = CompanyDetail(field: "Company Name", value: "New York")
        let email = CompanyDetail(field: "Company Name", value: "loremipsum@gmail.com")
        let phone = CompanyDetail(field: "Phone Number", value: "365 356-587")
        return [vaultId, trade, structure, dateOfE, iCode, dCountry, state, city, email, phone]
    }()
    
    // MARK: - Methods
    
    private func initUI() {
        
        coverView.addSubview(companyTitle)
        coverView.addSubview(tableView)
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        _ = companyTitle.anchor(coverView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: topBarHeight + 35, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = companyTitle.centralizeX(coverView.centerXAnchor)
        
        _ = tableView.anchor(companyTitle.bottomAnchor, left: coverView.leftAnchor, bottom: coverView.bottomAnchor, right: coverView.rightAnchor, topConstant: 45, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    private func registerCells() {
        tableView.register(CompanyDetailTableViewCell.self, forCellReuseIdentifier: companyCellId)
    }
    
    // MARK: - TableView
    
    var titleList = ["", "Status", "Bonus Bounty", "KYC Status", "Password Recovery", "Digital Nation", "Date of Birth", "Micro Lock", "Age Lock", "US Validation"]
    var isGrayed : Bool = true
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var rowColor : UIColor
        if !isGrayed {
            rowColor = UIColor(red:0.07, green:0.07, blue:0.07, alpha:1.0)
            isGrayed = true
        } else {
            rowColor = .clear
            isGrayed = false
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: companyCellId, for: indexPath as IndexPath) as! CompanyDetailTableViewCell
        cell.selectionStyle = .none
        cell.backgroundColor = rowColor
        cell.fieldLbl.text = companyDetailList[indexPath.row].field
        cell.vaultLbl.text = companyDetailList[indexPath.row].value
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    // MARK: - ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        initNavBar()
        initUI()
        registerCells()
    }

    // MARK: - NavBar
    private func initNavBar() {
        self.navigationItem.title = "Due-Dilligence Checklist"
    }
}
