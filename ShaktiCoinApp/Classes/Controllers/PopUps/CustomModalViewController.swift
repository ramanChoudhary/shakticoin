//
//  CustomModalViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/14/19.
//  Copyright © 2019 Garnik Giloyan. All rights reserved.
//

import UIKit

class CustomModalViewController: UIViewController {

    // MARK: - Declarations
    
    @IBOutlet weak var mainView: UIView!
    
    lazy var headerView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red:0.85, green:0.85, blue:0.85, alpha:1)
        view.alpha = 0.50
        return view
    }()
    
    lazy var xButton : UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "XButton"), for: .normal)
        button.addTarget(self, action: #selector(onClose), for: .touchUpInside)
        return button
    }()
    
    lazy var iconContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        return view
    }()
    
    lazy var iconImageView : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "MinerPopUp")
        return iv
    }()
    
    lazy var titleTextView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.backgroundColor = .clear
        return textView
    }()
    
    // MARK: - Methods
    
    private func showWithAnimate() {
        
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25) {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    private func closeWithAnimation() {
 
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0

        }) { (finished: Bool) in
            if finished {
                self.view.removeFromSuperview()
            }
        }
    }
    
    func clearMainViewConstraints() {
        for c in mainView.constraints {
            mainView.removeConstraint(c)
        }
    }
    
    func setMainViewBottomConstraintValue(value: CGFloat = 45) {
        _ = mainView.anchor(view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.rightAnchor, topConstant: 45, leftConstant: 15, bottomConstant: value, rightConstant: 15, widthConstant: 0, heightConstant: 0)
    }
    
    func setupViews(value: CGFloat = 45) {
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        mainView.backgroundColor = .white
        mainView.layer.cornerRadius = 3
        
        _ = mainView.anchor(view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.rightAnchor, topConstant: 45, leftConstant: 15, bottomConstant: value, rightConstant: 15, widthConstant: 0, heightConstant: 0)
        
        mainView.addSubview(headerView)
        
        _ = headerView.anchor(mainView.topAnchor, left: mainView.leftAnchor, bottom: nil, right: mainView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 90)
     
        mainView.addSubview(xButton)
        
        _ = xButton.anchor(headerView.topAnchor, left: nil, bottom: nil, right: headerView.rightAnchor, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0)
        
        mainView.addSubview(iconContainer)
        
        _ = iconContainer.anchor(headerView.topAnchor, left: headerView.leftAnchor, bottom: nil, right: nil, topConstant: 15, leftConstant: 20, bottomConstant: 0, rightConstant: 0, widthConstant: 330.0, heightConstant: 60)
        
        iconContainer.addSubview(iconImageView)
        _ = iconImageView.centralizeX(iconContainer.centerXAnchor)
        _ = iconImageView.centralizeY(iconContainer.centerYAnchor)
        
        mainView.addSubview(titleTextView)
        
        _ = titleTextView.anchor(headerView.topAnchor, left: iconContainer.rightAnchor, bottom: nil, right: nil, topConstant: 20, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 185, heightConstant: 50)
    }
    
    func setIcon(imageName: String) {
        iconImageView.image = UIImage(named: imageName)
    }
    
    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showWithAnimate()
        //setupViews()
    }
    
    override func viewDidLayoutSubviews() {
        iconContainer.layer.cornerRadius = iconContainer.frame.width/2
    }
    
    // MARK: - Events
    
    @objc func onCancel() {
        closeWithAnimation()
    }

    
    @objc private func onClose() {
        closeWithAnimation()
    }

}

