//
//  PasswordRecoveryViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/17/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class PasswordRecoveryViewController: UIViewController, UITextFieldDelegate {

    // MARK: - Declarations
    
    lazy var backgroundImage: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "Sign Up Image")
        //iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    lazy var coverView: UIView = {
        let cv = UIView()
        //cv.contentMode = .scaleAspectFill
        cv.alpha = 0.80
        cv.backgroundColor = .black
        return cv
    }()
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()

    lazy var iconImageView : UIImageView = {
        let imageView = UIImageView()
        imageView.tintColor = .white
        imageView.image = UIImage(named: "Lock Icon")
        return imageView
    }()
    
    lazy var titleLbl : UILabel = {
        let label = UILabel()
        label.text = "Password Recovery"
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 24, type: .Light)
        return label
    }()
    
    lazy var subTitleLbl : UILabel = {
        let label = UILabel()
        label.text = "We will send a link to your email address below."
        label.textColor = UIColor(red:0.85, green:0.85, blue:0.85, alpha:1)
        label.font = UIFont.lato(fontSize: 14, type: .Bold) //TODO Semibold
        return label
    }()
    
    lazy var emailAddressPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var emailTextField : SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 120
        textField.delegate = self
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Email Address", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField.keyboardType = UIKeyboardType.emailAddress
        textField.autocapitalizationType = .none
        return textField
    }()
 
    lazy var submitBtn : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Reset Password", for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        button.titleLabel?.textColor = .white
        button.addTarget(self, action: #selector(onSubmit(_button:)), for: .touchUpInside)
        return button
    }()
    
    lazy var cancelLbl : UILabel = {
        let label = UILabel()
        label.text = "Cancel"
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 14)
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(onCancel))
        label.addGestureRecognizer(tap)
        return label
    }()
    
    lazy var waitingIndicator:UIActivityIndicatorView = {
        let wi: UIActivityIndicatorView = UIActivityIndicatorView()
        wi.frame = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: 40.0, height: 40.0));
        wi.center = self.view.center
        wi.hidesWhenStopped = true
        if #available(iOS 13.0, *) {
            wi.style = UIActivityIndicatorView.Style.large
        } else {
            // Fallback on earlier versions
        }
        return wi
    }()
    
    func startWI() {
        waitingIndicator.startAnimating()
        view.isUserInteractionEnabled = false
    }
    
    func stopWI() {
        waitingIndicator.stopAnimating()
        view.isUserInteractionEnabled = false
    }
    
    let emailTT = SCToolTip()

    var isFirstScreen : Bool = true
    
    // MARK: - Methods
    
    private func setupToolTips() {
        emailTextField.setToolTip(scTT: emailTT)
    }
     
    func validate() -> Bool {
     
        var isValid: Bool = true
        
        if emailTextField.isEmpty() {
            view.addSubview(emailTT)
            emailTT.setRelativeXControl(uiControl: emailTextField)
            emailTT.setMessage(message: "Please fill email address")
            isValid = false
        } else {
            if !Validator.shared().isValidEmail(email: emailTextField.text!) {
                 view.addSubview(emailTT)
                 emailTT.setRelativeXControl(uiControl: emailTextField)
                 emailTT.setMessage(message: "Uh-oh - Invalid address")
                 isValid = false
            }
        }
        return isValid
    }

    
    private func setupViews() {
        
        view.addSubview(backgroundImage)
        view.addSubview(coverView)
        view.addSubview(containerView)
        view.addSubview(emailAddressPanel)
        view.addSubview(waitingIndicator)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = coverView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = containerView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        containerView.addSubview(iconImageView)
        _ = iconImageView.anchor(containerView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 100, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = iconImageView.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(titleLbl)
        _ = titleLbl.anchor(iconImageView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 25, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = titleLbl.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(subTitleLbl)
        _ = subTitleLbl.anchor(titleLbl.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = subTitleLbl.centralizeX(containerView.centerXAnchor)
        
        _ = emailAddressPanel.anchor(subTitleLbl.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 35, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = emailAddressPanel.centralizeX(containerView.centerXAnchor)
        
        emailAddressPanel.ContainerView.addSubview(emailTextField)
        emailTextField.parentPanel = emailAddressPanel
        Util.fullyAnchor(_parnetControl: emailAddressPanel.ContainerView, _childControl: emailTextField)
        
        containerView.addSubview(cancelLbl)
        
        _ = cancelLbl.anchor(nil, left: nil, bottom: containerView.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 35, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = cancelLbl.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(submitBtn)
        
        _ = submitBtn.anchor(nil, left: nil, bottom: cancelLbl.topAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 25, rightConstant: 0, widthConstant: 330, heightConstant: 36)
        _ = submitBtn.centralizeX(containerView.centerXAnchor)
    }

    fileprivate func observeKeyboardNotifications(){
        
       // NotificationCenter.default.addObserver(self, selector: #selector(KeyboardShow), name: UIResponder.keyboardWillShowNotification, object: nil)
       // NotificationCenter.default.addObserver(self, selector: #selector(KeyboardHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func KeyboardShow() {
        UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.view.frame = CGRect(x: 0, y: -145, width: self.view.frame.width, height: self.view.frame.height)
        }, completion: nil)
    }
    
    @objc func KeyboardHide() {
        UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        }, completion: nil)
    }
    
    // MARK: - Events
    @objc private func onSubmit(_button : UIButton) {
        
        if validate() {
            view.endEditing(true)
            
            let emailDict : [String: String] = ["email": emailTextField.text!]
            
            showLoader()
            DispatchQueue.main.async {
                self.hideLoader()
                let accountService = AccountService()
                accountService.resetPassword(payload: emailDict) { (statusCode) in
                    print(statusCode)
                }
            }
            
            /*
             startWI()
             DispatchQueue.main.async {
                 self.signIn { (statusCode, errorMessage) in
                     self.stopWI()
                     if statusCode == 200 {
                         let bonusBountyVC: BonusBountyViewController = BonusBountyViewController()
                         let appDelegate = UIApplication.shared.delegate as! AppDelegate
                         appDelegate.window?.rootViewController = bonusBountyVC
                     } else {
                         let alert = UIAlertController(title: "Login", message: errorMessage, preferredStyle: UIAlertController.Style.alert)
                         alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                         self.present(alert, animated: true, completion: nil)
                     }
                 }
             }

             */
            
            if isFirstScreen {
                
                iconImageView.image = UIImage(named: "Sent")
                titleLbl.text = "Link Sent"
                subTitleLbl.text = "Be sure to check your spam or junk mail."
                submitBtn.setTitle("Resend", for: .normal)
                isFirstScreen = false
            }
        }
    }
    
    @objc private func onCancel() {
        view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }

    // MARK: - TextField Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
    @objc func textFieldDidChange(_ textField: SCXTextField) {
        if !textField.isEmpty() {
            textField.removeToolTip()
            textField.parentPanel.setTopLabel(widthValue: textField.floatingLabelValue)
        } else {
            textField.parentPanel.setTopLabel(widthValue: 0, hideText: true)
        }
    }
    
    // MARK: - ViewContoller
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupToolTips()
        observeKeyboardNotifications()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    override func viewDidLayoutSubviews() {
        emailAddressPanel.setup(label: "Email Address")
    }
}
