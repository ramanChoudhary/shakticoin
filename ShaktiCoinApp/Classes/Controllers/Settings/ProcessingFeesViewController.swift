//
//  ProcessingFeesViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/18/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class ProcessingFeesViewController: UIViewController,UICollectionViewDelegate,
UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    // MARK: - Declarations
    lazy var backgroundImage: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "SettingsImage")
        return iv
    }()
    
    lazy var coverView: UIView = {
        let cv = UIView()
        //cv.contentMode = .scaleAspectFill
        cv.alpha = 0.50
        cv.backgroundColor = .black
        return cv
    }()
    
    lazy var containerView: UIScrollView = {
        let view = UIScrollView()
        view.contentSize.height = 835
        return view
    }()
    
    lazy var notificationContainer : UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.75
        return view
    }()
    
    lazy var notificationViewCollection : UICollectionView = {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isPagingEnabled = true
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = .clear
        cv.showsHorizontalScrollIndicator = false
        
        return cv
    }()
    
    let notificationItems: [NotificationItem] = {
        
        let first = NotificationItem(title: "A friend has signed up.", message: "Alan Reynolds has registered you as a referral upon signing up.", date: "06/10/2018", isRead: nil)
        let second = NotificationItem(title: "Congratulations!", message: "You’ve chosen the 1008.00 SXE Bonus Bounty. Refer friends to unlock it sooner!", date: "05/10/2018", isRead: nil)
        let third = NotificationItem(title: "Welcome to your Wallet.", message: "Complete your KYC verification to fully unlock it and get ready for your Bonus Bounty!", date: "05/10/2018", isRead: nil)
        
        return [first, second, third]
    }()
    
    var isNotifcationBarOpened : Bool = false
    let notificationCellId: String = "notificationCellId"
    
    lazy var taxInfoContainerView : SC2LayerView = {
        let view = SC2LayerView()
        return view
    }()
    
    lazy var infoTextView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        
        let color = UIColor.white
        let font = UIFont.lato(fontSize: 13)
        let bold = UIFont.lato(fontSize: 13, type: .Bold)
        
        textView.setup(textInfo: "Miners with SXE Network IDs earn fees for validating transactions by lending their computing power (mining) to the Shakti Network. For that service, they are compensated with fixed transaction processing fees.", textColor: color, font: font)
        
        textView.setText(textContent: "\n\nTransaction processing fees are earned by the initiating miner and the first set of confirming miners. Fees are paid in Toshi, a microdenomination of SXE Coin.", textColor: color, font: font)
        textView.setText(textContent: "\n\nThe first miner who initiates the consensus earn 5% of the transaction fee and the set of first miners who confirm the consensus earn the remainder of the fees at the rate of 25 Toshi (0.00005 SXE or US 0.025¢) per consensus until the sum of fees earned reaches 90% of the total.", textColor: color, font: font)
        
        textView.setText(textContent: "\n\nThe final 10% of total fees is paid to the Shakti Network to enforce Global Compliance and SNMP Policy and continue to further propagate the SXE monetary policy and to promote Shakti Coin adoption globally.", textColor: color, font: font)
        textView.setText(textContent: "\n\nAny payment up to and including two (2) Shakti Coins is considered a micro-transaction. The cost of a micro-transaction is fixed and it is set at 500 Toshi (= US 2.5¢).", textColor: color, font: font)
        textView.setText(textContent: "\n\nThe processing payment cost (Y) is calculated as Y = SXE 0.005 when transaction size is SXE ≤ 2; and Y = k*Log2(SXE) when SXE > 2.", textColor: color, font: font)
        textView.setText(textContent: "\n\n1 Chai (=US 5¢) is the minimum amount that any node can transact over the Shakti Network.", textColor: color, font: font)
        textView.setText(textContent: "\n\n\n•     All values are measured in SXE.", textColor: color, font: font)
        textView.setText(textContent: "\n\n•     The function is continuous. The coefficient, k, regulates the growth rate.", textColor: color, font: font)
        textView.setText(textContent: "\n\n•     Y is the value of processing payment cost.", textColor: color, font: font)

        textView.finilize()
        return textView
    }()
    
    // MARK: - Methods
    
    private func registerCells() {
        notificationViewCollection.register(NotificationViewCell.self, forCellWithReuseIdentifier: notificationCellId)
    }
    
    private func setupBaseViews() {
        
        view.addSubview(backgroundImage)
        view.addSubview(coverView)
        view.addSubview(notificationContainer)
        view.addSubview(notificationViewCollection)
        view.addSubview(containerView)
        
        registerCells()
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = coverView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = containerView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = notificationContainer.anchor(view.topAnchor, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 221, heightConstant: 0)
        
        notificationContainer.isHidden = true
        
        _ = notificationViewCollection.anchor(view.topAnchor, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 221, heightConstant: 0)
        
        notificationViewCollection.layer.borderWidth = 0.5
        notificationViewCollection.layer.borderColor = UIColor.darkGray.cgColor
        
        notificationViewCollection.isHidden = true
        
        setupViews()
    }
    
    func setupViews() {
        
        containerView.addSubview(taxInfoContainerView)
        
        _ = taxInfoContainerView.anchor(containerView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 35, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 335, heightConstant: 810)
        _ = taxInfoContainerView.centralizeX(containerView.centerXAnchor)
        
        taxInfoContainerView.addSubview(infoTextView)
        _ = infoTextView.anchor(taxInfoContainerView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 300, heightConstant: 800)
        _ = infoTextView.centralizeX(taxInfoContainerView.centerXAnchor)
    }
    
    // MARK: - CollectionView
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notificationItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: notificationCellId, for: indexPath) as! NotificationViewCell
        cell.ntfItem = notificationItems[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 221, height: 72)
    }
    
    // MARK: - Events
    
    @objc private func onMenuButtonClick() {
        print("Menu Item Clicked")
    }
    
    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setupBaseViews()
    }
    
    override func viewDidLayoutSubviews() {
        taxInfoContainerView.transparency = 0.80
    }
    
    // MARK: - Navigation Bar
    
    private func setupNavBar() {
        
        navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationItem.title = "Processing Fees"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.futura(fontSize: 18, type: .Medium)]
        
        let backButton = UIBarButtonItem(image: UIImage(named: "Back"), style: .plain, target: self, action: #selector(onBack))
        backButton.tintColor = .white
        self.navigationItem.leftBarButtonItem = backButton
        
        let ntfMenu = UIBarButtonItem(image: UIImage(named: "Notification"), style: .plain, target: self, action: #selector(onNtfMenuClick))
        ntfMenu.tintColor = .white
        self.navigationItem.rightBarButtonItem = ntfMenu
    }
    
    @objc private func onBack() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func onNtfMenuClick() {
        ecNtfPanel()
    }
    
    private func ecNtfPanel() {
        let width : CGFloat = (isNotifcationBarOpened) ? 0.0 : 221
        coverView.frame.origin.x = -(width)
        containerView.frame.origin.x = -(width)
        notificationContainer.isHidden = isNotifcationBarOpened
        notificationViewCollection.isHidden = isNotifcationBarOpened
        isNotifcationBarOpened = !isNotifcationBarOpened
    }

}
