//
//  SettingsViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/15/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController,UICollectionViewDelegate,
UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource {

    // MARK: - Declarations
    lazy var backgroundImage: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "SettingsImage")
        //iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    lazy var coverView: UIView = {
        let cv = UIView()
        //cv.contentMode = .scaleAspectFill
        cv.alpha = 0.50
        cv.backgroundColor = .black
        return cv
    }()
    
    lazy var containerView: UIScrollView = {
        let view = UIScrollView()
        view.contentSize.height = 1250
        return view
    }()
    
    lazy var menuContainer : UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.75
        return view
    }()
    
    lazy var menuViewCollection: UICollectionView = {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isPagingEnabled = true
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = .black
        cv.alpha = 0.75
        cv.showsHorizontalScrollIndicator = false
        
        return cv
    }()
    
    lazy var notificationContainer : UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.75
        return view
    }()
    
    lazy var notificationViewCollection : UICollectionView = {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isPagingEnabled = true
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = .clear
        cv.showsHorizontalScrollIndicator = false
        
        return cv
    }()
    
    let notificationItems: [NotificationItem] = {
        
        let first = NotificationItem(title: "A friend has signed up.", message: "Alan Reynolds has registered you as a referral upon signing up.", date: "06/10/2018", isRead: nil)
        let second = NotificationItem(title: "Congratulations!", message: "You’ve chosen the 1008.00 SXE Bonus Bounty. Refer friends to unlock it sooner!", date: "05/10/2018", isRead: nil)
        let third = NotificationItem(title: "Welcome to your Wallet.", message: "Complete your KYC verification to fully unlock it and get ready for your Bonus Bounty!", date: "05/10/2018", isRead: nil)
        
        return [first, second, third]
    }()
    
    var isMenuOpened : Bool = false
    var isNotifcationBarOpened : Bool = false
    let menuViewCellId : String = "menuViewCellId"
    let notificationCellId: String = "notificationCellId"
    let settingsCellId : String = "settingsCellId"
    let settingsSectionId : String = "settingsSectionId"
    
    lazy var fullNameLbl : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "William Almundson"
        label.font = UIFont.lato(fontSize: 16, type: .Bold) //TODO Medium
        return label
    }()

    lazy var emailLbl : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 12, type: .Bold)
        label.text = "walmund@gmail.com"
        return label
    }()
    
    lazy var mainTableView : UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorInset = .zero
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = false
        tableView.backgroundColor = .clear
        return tableView
    }()

    lazy var scrollArrowIcon : UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "arrowDown")
        view.tintColor = UIColor.mainColor()
        return view
    }()

    lazy var turnOnNtfBtn: UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 18
        button.backgroundColor = UIColor.qrButtonColor()
        button.setTitle("Turn on Notifications", for: .normal)
        button.titleLabel?.textColor = .black
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()
    
    lazy var helpTableView : UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorInset = .zero
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = false
        tableView.backgroundColor = .clear
        return tableView
    }()
    
    lazy var logOutBtn : UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 18
        button.backgroundColor = UIColor.qrButtonColor()
        button.setTitle("Log Out", for: .normal)
        button.titleLabel?.textColor = .black
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        button.addTarget(self, action: #selector(onLogOut), for: .touchUpInside)
        return button
    }()
    
    lazy var versionLabel : UILabel = {
        let label = UILabel()
        label.text = "v 4.15 - Swiss Shakti Foundation"
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 10)
        return label
    }()
    
    // MARK: - Methods
    
    private func registerCells() {
        menuViewCollection.register(MenuViewCell.self, forCellWithReuseIdentifier: menuViewCellId)
        notificationViewCollection.register(NotificationViewCell.self, forCellWithReuseIdentifier: notificationCellId)
        mainTableView.register(SettingsTableViewCell.self, forCellReuseIdentifier: settingsCellId)
        mainTableView.register(SettingsSectionTableViewCell.self, forCellReuseIdentifier: settingsSectionId)
        helpTableView.register(SettingsTableViewCell.self, forCellReuseIdentifier: settingsCellId)
        helpTableView.register(SettingsSectionTableViewCell.self, forCellReuseIdentifier: settingsSectionId)
    }
    
    private func setupBaseViews() {
        
        view.addSubview(backgroundImage)
        view.addSubview(coverView)
        view.addSubview(menuContainer)
        view.addSubview(menuViewCollection)
        view.addSubview(notificationContainer)
        view.addSubview(notificationViewCollection)
        view.addSubview(containerView)
        
        registerCells()
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = coverView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        /*_ = containerView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)*/
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.topAnchor.constraint(equalTo: view.topAnchor, constant: topBarHeight).isActive = true
        containerView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        containerView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        _ = menuContainer.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = menuViewCollection.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: nil, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 60, heightConstant: 0)
        
        _ = notificationContainer.anchor(view.topAnchor, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 221, heightConstant: 0)
        
        notificationContainer.isHidden = true
        
        _ = notificationViewCollection.anchor(view.topAnchor, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 221, heightConstant: 0)
        
        menuViewCollection.layer.borderWidth = 0.5
        menuViewCollection.layer.borderColor = UIColor.darkGray.cgColor
        
        notificationViewCollection.layer.borderWidth = 0.5
        notificationViewCollection.layer.borderColor = UIColor.darkGray.cgColor
        
        menuViewCollection.isHidden = true
        notificationViewCollection.isHidden = true
        
        setupViews()
    }
    
    func setupViews() {
        
        containerView.addSubview(fullNameLbl)
        
        _ = fullNameLbl.anchor(containerView.topAnchor, left: containerView.leftAnchor, bottom: nil, right: nil, topConstant: 25, leftConstant: 30, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        containerView.addSubview(emailLbl)
        _ = emailLbl.anchor(fullNameLbl.bottomAnchor, left: fullNameLbl.leftAnchor, bottom: nil, right: nil, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        containerView.addSubview(mainTableView)
        
        mainTableView.translatesAutoresizingMaskIntoConstraints = false
        mainTableView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 75).isActive = true
        mainTableView.widthAnchor.constraint(equalToConstant: 350).isActive = true
        mainTableView.heightAnchor.constraint(equalToConstant: 470).isActive = true
        mainTableView.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 15).isActive = true
        
        containerView.addSubview(scrollArrowIcon)
        
        scrollArrowIcon.translatesAutoresizingMaskIntoConstraints = false
        scrollArrowIcon.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 550).isActive = true
        scrollArrowIcon.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        
        containerView.addSubview(turnOnNtfBtn)
        
        turnOnNtfBtn.translatesAutoresizingMaskIntoConstraints = false
        turnOnNtfBtn.topAnchor.constraint(equalTo: scrollArrowIcon.bottomAnchor, constant: 20).isActive = true
        turnOnNtfBtn.widthAnchor.constraint(equalToConstant: 330).isActive = true
        turnOnNtfBtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        turnOnNtfBtn.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        
        containerView.addSubview(helpTableView)
        
        helpTableView.translatesAutoresizingMaskIntoConstraints = false
        helpTableView.topAnchor.constraint(equalTo: turnOnNtfBtn.bottomAnchor, constant: 15).isActive = true
        helpTableView.widthAnchor.constraint(equalToConstant: 350).isActive = true
        helpTableView.heightAnchor.constraint(equalToConstant: 470).isActive = true
        helpTableView.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 15).isActive = true

        containerView.addSubview(logOutBtn)
        logOutBtn.translatesAutoresizingMaskIntoConstraints = false
        logOutBtn.topAnchor.constraint(equalTo: helpTableView.bottomAnchor, constant: 15).isActive = true
        logOutBtn.widthAnchor.constraint(equalToConstant: 330).isActive = true
        logOutBtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        logOutBtn.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true

        containerView.addSubview(versionLabel)
        versionLabel.translatesAutoresizingMaskIntoConstraints = false
        versionLabel.topAnchor.constraint(equalTo: logOutBtn.bottomAnchor, constant: 15).isActive = true
        versionLabel.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        
        
        //_ = mainTableView.anchor(containerView.topAnchor, left: containerView.leftAnchor, bottom: nil, right: containerView.rightAnchor, topConstant: 1000, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 470)
    }

    // MARK: - TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == mainTableView {
            return 3
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    var mainTableSections = [SettingsTableSection.Account, SettingsTableSection.Security, SettingsTableSection.NotificationPreferences]
    var helpTableSections = [SettingsTableSection.HelpAndPolicies]
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let sectionCell = tableView.dequeueReusableCell(withIdentifier: settingsSectionId) as! SettingsSectionTableViewCell
        
        if tableView == mainTableView {
            sectionCell.setup(section: mainTableSections[section])
        } else {
            sectionCell.setup(section: helpTableSections[section])
        }

        return sectionCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == mainTableView {
            if indexPath.row == 0 {
                let vc = PersonalInfoViewController()
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        if tableView == helpTableView {
            
            if indexPath.row == 0 {
                let vc = HelpViewController()
                self.navigationController?.pushViewController(vc, animated: true)
            } else if indexPath.row == 1 {
                let vc = ApplicationTermsViewController()
                self.navigationController?.pushViewController(vc, animated: true)
            } else if indexPath.row == 2 {
                let vc = PrivacyPolicyViewController()
                self.navigationController?.pushViewController(vc, animated: true)
            } else if indexPath.row == 3 {
                let vc = TaxInfoViewController()
                self.navigationController?.pushViewController(vc, animated: true)
            } else if indexPath.row == 4 {
                let vc = ProcessingFeesViewController()
                self.navigationController?.pushViewController(vc, animated: true)
            } else if indexPath.row == 5 {
                let vc = ContactUsViewController()
                self.navigationController?.pushViewController(vc, animated: true)
            } else if indexPath.row == 6 {
                let vc = ResetPasswordViewController()
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == mainTableView {
        
            switch section {
            case 0:
                return 2
            case 1:
                return 1
            case 2:
                return 4
            default:
                return 0
            }
        } else {
            return 7
        }
    }

    let accountSettings : [Setting] = {
        let personalInfo = Setting(leftText: "Personal Info", rightText: "", isSwitch: false, isDisabled: false)
        let kycVerification = Setting(leftText: "KYC Verification", rightText: "In Progress ", isSwitch: false, isDisabled: false)
        let touchIdFeature = Setting(leftText: "TouchId", rightText: "", isSwitch: true, isDisabled: true)
        return [personalInfo, kycVerification]
    }()
    
    let securitySettings : [Setting] = {
        let touchIdFeature = Setting(leftText: "TouchId", rightText: "", isSwitch: true, isDisabled: true)
        return [touchIdFeature]
    }()

    let notificationSettings : [Setting] = {
        let generalNtf = Setting(leftText: "General Service Notifications ", rightText: "", isSwitch: true, isDisabled: false)
        let paymentsNtf = Setting(leftText: "Receive payments and pay requests ", rightText: "", isSwitch: true, isDisabled: false)
        let refActivityNtf = Setting(leftText: "Referral activity", rightText: "", isSwitch: true, isDisabled: false)
        let tipsNtf = Setting(leftText: "Tips and suggestions", rightText: "", isSwitch: true, isDisabled: true)
        return [generalNtf,paymentsNtf,refActivityNtf,tipsNtf]
    }()
    
    let helpAndPolicySettings : [Setting] = {
        let help = Setting(leftText: "Help", rightText: "", isSwitch: false, isDisabled: false)
        let appTerms = Setting(leftText: "Application Terms", rightText: "", isSwitch: false, isDisabled: false)
        let privacyPolicy = Setting(leftText: "Privacy Policy", rightText: "", isSwitch: false, isDisabled: false)
        let taxesInfo = Setting(leftText: "Taxes Info", rightText: "", isSwitch: false, isDisabled: false)
        let processingFees = Setting(leftText: "Processing Fees", rightText: "", isSwitch: false, isDisabled: false)
        let feedbackContactUs = Setting(leftText: "Feedback / Contact Us", rightText: "", isSwitch: false, isDisabled: false)
        let passwordReset = Setting(leftText: "Password Reset", rightText: "", isSwitch: false, isDisabled: false)
        return [help,appTerms,privacyPolicy,taxesInfo,processingFees,feedbackContactUs,passwordReset]
    }()
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: settingsCellId, for: indexPath as IndexPath) as! SettingsTableViewCell
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        
        if tableView == mainTableView {
            switch (indexPath.section) {
            case 0:
                cell.setupWithRightText(leftText: accountSettings[indexPath.row].leftText, rightText: accountSettings[indexPath.row].rightText, isSwitch: accountSettings[indexPath.row].isSwitch, isDisabled: accountSettings[indexPath.row].isDisabled)
            case 1:
                cell.setupWithRightText(leftText: securitySettings[indexPath.row].leftText, rightText: securitySettings[indexPath.row].rightText, isSwitch: securitySettings[indexPath.row].isSwitch, isDisabled: securitySettings[indexPath.row].isDisabled)
            case 2:
                cell.setupWithRightText(leftText: notificationSettings[indexPath.row].leftText, rightText: notificationSettings[indexPath.row].rightText, isSwitch: notificationSettings[indexPath.row].isSwitch, isDisabled: notificationSettings[indexPath.row].isDisabled)
            default:
                break
            }

        } else {
            
            cell.setup(leftText: helpAndPolicySettings[indexPath.row].leftText, isSwitch: helpAndPolicySettings[indexPath.row].isSwitch, isDisabled: helpAndPolicySettings[indexPath.row].isDisabled)
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }

    
    // MARK: - CollectionView
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == menuViewCollection {
            return 5
        } else {
            return notificationItems.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == menuViewCollection {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: menuViewCellId, for: indexPath) as! MenuViewCell
            cell.menu = Menu(rawValue: indexPath.row+1)
            cell.menuButton.addTarget(self, action: #selector(onMenuButtonClick), for: .touchUpInside)
            if (indexPath.row+1) == Menu.Settings.rawValue {
                cell.menuButton.imageView?.tintColor = UIColor.mainColor()
                cell.itemName.textColor = UIColor.mainColor()
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: notificationCellId, for: indexPath) as! NotificationViewCell
            cell.ntfItem = notificationItems[indexPath.row]
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == menuViewCollection {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if indexPath.row == 0 {
                
                let vc = storyboard.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
                let navigationController = UINavigationController(rootViewController: vc)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = navigationController
                
            } else if indexPath.row == 1 {
                
                let vc = storyboard.instantiateViewController(withIdentifier: "BusinessVaultIntroViewController") as! BusinessVaultIntroViewController
                let navigationController = UINavigationController(rootViewController: vc)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = navigationController
                
            } else if indexPath.row == 2 {
                
                let vc = storyboard.instantiateViewController(withIdentifier: "ShaktiMinerIntroViewController") as! ShaktiMinerIntroViewController
                let navigationController = UINavigationController(rootViewController: vc)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = navigationController
                
            } else if indexPath.row == 3 {
                
                let vc = storyboard.instantiateViewController(withIdentifier: "MyReferralsViewController") as! MyReferralsViewController
                let navigationController = UINavigationController(rootViewController: vc)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = navigationController
                
            } else if indexPath.row == 4 {
                
                //Current
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == menuViewCollection {
            return CGSize(width: 60, height: 60)
        } else {
            return CGSize(width: 221, height: 72)
        }
    }
    
    // MARK: - Events
    
    @objc private func onMenuButtonClick() {
        print("Menu Item Clicked")
    }
    
    @objc private func onLogOut() {
        
        UserSettings.shared().removeUserToken()
        
        let loginVC: LoginViewController = LoginViewController()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = loginVC
    }
    
    // MARK: - ViewContoller
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupBaseViews()
        setupNavBar()
        
        /*let vc = PersonalInfoViewController()
        self.navigationController?.pushViewController(vc, animated: true)*/

    }

    // MARK: - Navigation Bar
    
    private func setupNavBar() {
        
        navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationItem.title = "Settings"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.futura(fontSize: 18, type: .Medium)]
        
        let burgerMenu = UIBarButtonItem(image: UIImage(named: "Menu"), style: .plain, target: self, action: #selector(onBurgerMenuClick))
        burgerMenu.tintColor = .white
        self.navigationItem.leftBarButtonItem = burgerMenu
        
        let ntfMenu = UIBarButtonItem(image: UIImage(named: "Notification"), style: .plain, target: self, action: #selector(onNtfMenuClick))
        ntfMenu.tintColor = .white
        self.navigationItem.rightBarButtonItem = ntfMenu
    }
    
    @objc private func onBurgerMenuClick() {
        if isNotifcationBarOpened {
            ecNtfPanel()
        }
        ecMenu()
    }
    
    @objc private func onNtfMenuClick() {
        if isMenuOpened {
            ecMenu()
        }
        ecNtfPanel()
    }
    
    private func ecMenu() {
        let width: CGFloat = (isMenuOpened) ? 0.0 : 60.0
        coverView.frame.origin.x = width
        containerView.frame.origin.x = width
        
        //menuContainer.frame.size.width = width
        menuViewCollection.isHidden = isMenuOpened
        isMenuOpened = !isMenuOpened
    }
    
    private func ecNtfPanel() {
        let width : CGFloat = (isNotifcationBarOpened) ? 0.0 : 221
        coverView.frame.origin.x = -(width)
        containerView.frame.origin.x = -(width)
        notificationContainer.isHidden = isNotifcationBarOpened
        notificationViewCollection.isHidden = isNotifcationBarOpened
        isNotifcationBarOpened = !isNotifcationBarOpened
    }

}
