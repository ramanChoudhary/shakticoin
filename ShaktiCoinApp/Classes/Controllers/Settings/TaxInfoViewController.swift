//
//  TaxInfoViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/18/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class TaxInfoViewController: UIViewController,UICollectionViewDelegate,
UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    // MARK: - Declarations
    lazy var backgroundImage: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "SettingsImage")
        return iv
    }()
    
    lazy var coverView: UIView = {
        let cv = UIView()
        //cv.contentMode = .scaleAspectFill
        cv.alpha = 0.50
        cv.backgroundColor = .black
        return cv
    }()
    
    lazy var containerView: UIView = {
        let view = UIView()
        return view
    }()
    
    lazy var notificationContainer : UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.75
        return view
    }()
    
    lazy var notificationViewCollection : UICollectionView = {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isPagingEnabled = true
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = .clear
        cv.showsHorizontalScrollIndicator = false
        
        return cv
    }()
    
    let notificationItems: [NotificationItem] = {
        
        let first = NotificationItem(title: "A friend has signed up.", message: "Alan Reynolds has registered you as a referral upon signing up.", date: "06/10/2018", isRead: nil)
        let second = NotificationItem(title: "Congratulations!", message: "You’ve chosen the 1008.00 SXE Bonus Bounty. Refer friends to unlock it sooner!", date: "05/10/2018", isRead: nil)
        let third = NotificationItem(title: "Welcome to your Wallet.", message: "Complete your KYC verification to fully unlock it and get ready for your Bonus Bounty!", date: "05/10/2018", isRead: nil)
        
        return [first, second, third]
    }()
    
    var isNotifcationBarOpened : Bool = false
    let notificationCellId: String = "notificationCellId"

    lazy var taxInfoContainerView : SC2LayerView = {
        let view = SC2LayerView()
        return view
    }()
    
    lazy var infoTextView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        let color = UIColor.white
        let font = UIFont.lato(fontSize: 13)
        textView.setup(textInfo: "The Shakti ecosystem tracks tax implications resulting from transactions.", textColor: color, font: font)
        textView.setText(textContent: "\n\nUnderstanding potential tax exposure and planning for it is essential for mitigating transactional risk, levy cost, and managing cash flows.", textColor: color, font: font)
        textView.setText(textContent: "\n\nAs a crypto wallet holder, you may trigger cross-border transactional taxes when one buy and sell a broad range of consumer products from various industr sectors.", textColor: color, font: font)
        textView.setText(textContent: "\n\nWe will track your tax exposures based on the transaction activities within the wallet you make from the country you shop and as well as with the peers with whom you trade.", textColor: color, font: font)
        textView.setText(textContent: "\n\nThe Shakti Network will not report your tax exposures to any authorities, but we will do so to you and to you only. It is provided to you, so you can plan for your tax liabilities and make informed decisions. and navigate through the implications of each of every transaction.", textColor: color, font: font)
        textView.finilize()
        return textView
    }()
    
    lazy var agreementCheckBox : SCRightCheckBoxWithLabel = {
        let checkBox = SCRightCheckBoxWithLabel()
        checkBox.caption.setup(textInfo: "If you wish to be updated with current", textColor: .white, font: UIFont.lato(fontSize: 12))
        checkBox.caption.setText(textContent: "\ntransactional tax related risks, please", textColor: .white, font: UIFont.lato(fontSize: 12))
        checkBox.caption.setText(textContent: "\ncheck this box for additional information.", textColor: .white, font: UIFont.lato(fontSize: 12))
        checkBox.caption.finilize()
        return checkBox
    }()
    
    lazy var agreeButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Agree", for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14)
        button.titleLabel?.textColor = .white
        button.addTarget(self, action: #selector(onAgreeButtonClick), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Methods
    
    private func registerCells() {
        notificationViewCollection.register(NotificationViewCell.self, forCellWithReuseIdentifier: notificationCellId)
    }
    
    private func setupBaseViews() {
        
        view.addSubview(backgroundImage)
        view.addSubview(coverView)
        view.addSubview(notificationContainer)
        view.addSubview(notificationViewCollection)
        view.addSubview(containerView)
        
        registerCells()
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = coverView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = containerView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = notificationContainer.anchor(view.topAnchor, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 221, heightConstant: 0)
        
        notificationContainer.isHidden = true
        
        _ = notificationViewCollection.anchor(view.topAnchor, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 221, heightConstant: 0)
        
        notificationViewCollection.layer.borderWidth = 0.5
        notificationViewCollection.layer.borderColor = UIColor.darkGray.cgColor
        
        notificationViewCollection.isHidden = true
        
        setupViews()
    }
    
    func setupViews() {
        
        containerView.addSubview(taxInfoContainerView)
        
        _ = taxInfoContainerView.anchor(containerView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 35, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 335, heightConstant: 550)
        _ = taxInfoContainerView.centralizeX(containerView.centerXAnchor)
        
        taxInfoContainerView.addSubview(infoTextView)
        _ = infoTextView.anchor(taxInfoContainerView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 300, heightConstant: 390)
        _ = infoTextView.centralizeX(taxInfoContainerView.centerXAnchor)
        
        taxInfoContainerView.addSubview(agreementCheckBox)
        
        _ = agreementCheckBox.anchor(infoTextView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 250, heightConstant: 56)
        _ = agreementCheckBox.centralizeX(taxInfoContainerView.centerXAnchor)
        
        agreementCheckBox.setCaptionHeightConstant(height: 46)
        
        view.addSubview(agreeButton)
        
        _ = agreeButton.anchor(nil, left: nil, bottom: taxInfoContainerView.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 15, rightConstant: 0, widthConstant: 300, heightConstant: 36)
        _ = agreeButton.centralizeX(taxInfoContainerView.centerXAnchor)
        
    }
    
    // MARK: - CollectionView
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notificationItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: notificationCellId, for: indexPath) as! NotificationViewCell
        cell.ntfItem = notificationItems[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 221, height: 72)
    }
    
    // MARK: - Events
    
    @objc private func onMenuButtonClick() {
        print("Menu Item Clicked")
    }
    
    @objc private func onAgreeButtonClick() {
        print("Agree...")
    }
    
    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setupBaseViews()
    }

    override func viewDidLayoutSubviews() {
        taxInfoContainerView.transparency = 0.80
    }

    // MARK: - Navigation Bar
    
    private func setupNavBar() {
        
        navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationItem.title = "Taxes Info"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.futura(fontSize: 18, type: .Medium)]
        
        let backButton = UIBarButtonItem(image: UIImage(named: "Back"), style: .plain, target: self, action: #selector(onBack))
        backButton.tintColor = .white
        self.navigationItem.leftBarButtonItem = backButton
        
        let ntfMenu = UIBarButtonItem(image: UIImage(named: "Notification"), style: .plain, target: self, action: #selector(onNtfMenuClick))
        ntfMenu.tintColor = .white
        self.navigationItem.rightBarButtonItem = ntfMenu
    }
    
    @objc private func onBack() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func onNtfMenuClick() {
        ecNtfPanel()
    }
    
    private func ecNtfPanel() {
        let width : CGFloat = (isNotifcationBarOpened) ? 0.0 : 221
        coverView.frame.origin.x = -(width)
        containerView.frame.origin.x = -(width)
        notificationContainer.isHidden = isNotifcationBarOpened
        notificationViewCollection.isHidden = isNotifcationBarOpened
        isNotifcationBarOpened = !isNotifcationBarOpened
    }


}
