//
//  ContactUsViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/17/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController,UICollectionViewDelegate,
UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {

    // MARK: - Declarations
    lazy var backgroundImage: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "SettingsImage")
        return iv
    }()
    
    lazy var coverView: UIView = {
        let cv = UIView()
        //cv.contentMode = .scaleAspectFill
        cv.alpha = 0.50
        cv.backgroundColor = .black
        return cv
    }()
    
    lazy var containerView: UIView = {
        let view = UIView()
        return view
    }()

    lazy var notificationContainer : UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.75
        return view
    }()
    
    lazy var notificationViewCollection : UICollectionView = {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isPagingEnabled = true
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = .clear
        cv.showsHorizontalScrollIndicator = false
        
        return cv
    }()
    
    let notificationItems: [NotificationItem] = {
        
        let first = NotificationItem(title: "A friend has signed up.", message: "Alan Reynolds has registered you as a referral upon signing up.", date: "06/10/2018", isRead: nil)
        let second = NotificationItem(title: "Congratulations!", message: "You’ve chosen the 1008.00 SXE Bonus Bounty. Refer friends to unlock it sooner!", date: "05/10/2018", isRead: nil)
        let third = NotificationItem(title: "Welcome to your Wallet.", message: "Complete your KYC verification to fully unlock it and get ready for your Bonus Bounty!", date: "05/10/2018", isRead: nil)
        
        return [first, second, third]
    }()
    
    var isNotifcationBarOpened : Bool = false
    let notificationCellId: String = "notificationCellId"

    lazy var titleHr : SCLineTitle = {
        let lineTitle = SCLineTitle()
        lineTitle.Title = "What would you like to say?"
        return lineTitle
    }()

    lazy var reasonsTableView : UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        return tableView
    }()
    
    lazy var reasonToContactDropDown: SCGroupBox = {
        let groupView = SCGroupBox()
        groupView.backgroundColor = .black
        groupView.alpha = 0.85
        groupView.isHidden = true
        groupView.CloseButton.addTarget(self, action: #selector(onCloseDropDownList), for: .touchUpInside)
        return groupView
    }()
    
    lazy var reasonToContact : SCTextField = {
        let textField = TextFieldPrototypes.RegularTextField
        textField.allowEdit = false
        textField.RightIcon = UIImage(named: "Dropdown")
        textField.RightViewButton.tag = 1
        textField.RightViewButton.addTarget(self, action: #selector(onDropDownIconClick(_button:)), for: .touchUpInside)
        textField.delegate = self
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Reason To Contact", font: UIFont.lato(fontSize: 14))
        return textField
    }()
    
    lazy var nameTextFieldPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var nameTextField : SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 80
        textField.delegate = self
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Name", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        return textField
    }()
 
    lazy var emailAddressTextFieldPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()

    lazy var emailAddressTextField : SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 117
        textField.delegate = self
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Email Address", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        textField.keyboardType = .emailAddress
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        return textField
    }()
    
    lazy var mobileNumberTextFieldPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()

    lazy var mobileNumberTextField : SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 122
        textField.delegate = self
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Mobile Number", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        return textField
    }()
    
    lazy var messageTextFieldPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()

    lazy var messageTextField : SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 90
        textField.delegate = self
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Message", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        return textField
    }()

    lazy var sendMessageBtn : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Send Message", for: .normal)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        button.addTarget(self, action: #selector(onSendMessage), for: .touchUpInside)
        return button
    }()
    
    lazy var cancelLbl : UILabel = {
        let label = UILabel()
        label.text = "Cancel"
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 14)
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(onCancel))
        label.addGestureRecognizer(tap)
        return label
    }()

    let reasonToContactTT = SCToolTip()
    let nameTT = SCToolTip()
    let emailTT = SCToolTip()
    let mobileTT = SCToolTip()
    let messageTT = SCToolTip()
    
    let reasonCellId : String = "reasonCellId"
    let listOfReasons = ["Reason 1", "Reason 2", "Reason 3", "Reason 4", "Reason 5"]
    
    
    // MARK: - Methods
    
    private func registerCells() {
        notificationViewCollection.register(NotificationViewCell.self, forCellWithReuseIdentifier: notificationCellId)
        reasonsTableView.register(CountryTableViewCell.self, forCellReuseIdentifier: reasonCellId)
    }
    
    private func setupBaseViews() {
        
        view.addSubview(backgroundImage)
        view.addSubview(coverView)
        view.addSubview(notificationContainer)
        view.addSubview(notificationViewCollection)
        view.addSubview(containerView)
        
        registerCells()
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = coverView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = containerView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = notificationContainer.anchor(view.topAnchor, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 221, heightConstant: 0)
        
        notificationContainer.isHidden = true
        
        _ = notificationViewCollection.anchor(view.topAnchor, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 221, heightConstant: 0)
        
        notificationViewCollection.layer.borderWidth = 0.5
        notificationViewCollection.layer.borderColor = UIColor.darkGray.cgColor
        notificationViewCollection.isHidden = true
        
        setupViews()
    }
    
    func setupViews() {
        
        containerView.addSubview(titleHr)
        _ = titleHr.anchor(containerView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 30, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = titleHr.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(reasonToContact)
        _ = reasonToContact.anchor(titleHr.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = reasonToContact.centralizeX(containerView.centerXAnchor)
         
        containerView.addSubview(nameTextFieldPanel)
        _ = nameTextFieldPanel.anchor(reasonToContact.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = nameTextFieldPanel.centralizeX(containerView.centerXAnchor)
        
        nameTextFieldPanel.ContainerView.addSubview(nameTextField)
        nameTextField.parentPanel = nameTextFieldPanel
        Util.fullyAnchor(_parnetControl: nameTextFieldPanel.ContainerView, _childControl: nameTextField)
        
        containerView.addSubview(emailAddressTextFieldPanel)
        _ = emailAddressTextFieldPanel.anchor(nameTextFieldPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = emailAddressTextFieldPanel.centralizeX(containerView.centerXAnchor)
        
        emailAddressTextFieldPanel.ContainerView.addSubview(emailAddressTextField)
        emailAddressTextField.parentPanel = emailAddressTextFieldPanel
        Util.fullyAnchor(_parnetControl: emailAddressTextFieldPanel.ContainerView, _childControl: emailAddressTextField)
        
        containerView.addSubview(mobileNumberTextFieldPanel)
        _ = mobileNumberTextFieldPanel.anchor(emailAddressTextFieldPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = mobileNumberTextFieldPanel.centralizeX(containerView.centerXAnchor)
        
        mobileNumberTextFieldPanel.ContainerView.addSubview(mobileNumberTextField)
        mobileNumberTextField.parentPanel = mobileNumberTextFieldPanel
        Util.fullyAnchor(_parnetControl: mobileNumberTextFieldPanel.ContainerView, _childControl: mobileNumberTextField)
        
        containerView.addSubview(messageTextFieldPanel)
        _ = messageTextFieldPanel.anchor(mobileNumberTextFieldPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 140)
        _ = messageTextFieldPanel.centralizeX(containerView.centerXAnchor)
        
        messageTextFieldPanel.ContainerView.addSubview(messageTextField)
        messageTextField.parentPanel = messageTextFieldPanel
        Util.fullyAnchor(_parnetControl: messageTextFieldPanel.ContainerView, _childControl: messageTextField)
        
        containerView.addSubview(sendMessageBtn)
        _ = sendMessageBtn.anchor(messageTextFieldPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 25, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 36)
        _ = sendMessageBtn.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(cancelLbl)
        _ = cancelLbl.anchor(sendMessageBtn.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = cancelLbl.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(reasonToContactDropDown)
        _ = reasonToContactDropDown.anchor(reasonToContact.topAnchor, left: reasonToContact.leftAnchor, bottom: mobileNumberTextFieldPanel.topAnchor, right: reasonToContact.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    func validate() -> Bool {
        
        var isValid : Bool = true
        
        if reasonToContact.isEmpty() {
            
            containerView.addSubview(reasonToContactTT)
            reasonToContactTT.setRelativeControl(uiControl: reasonToContact)
            reasonToContactTT.setMessage(message: "Please fill out the reason")
            isValid = false
        }
        
        if nameTextField.isEmpty() {
            
            containerView.addSubview(nameTT)
            nameTT.setRelativeXControl(uiControl: nameTextField)
            nameTT.setMessage(message: "Please fill out Name")
            isValid = false
        }
        
        if emailAddressTextField.isEmpty() {
            
            containerView.addSubview(emailTT)
            emailTT.setRelativeXControl(uiControl: emailAddressTextField)
            emailTT.setMessage(message: "Please fill out Email Address")
            isValid = false
            
        } else {
            
            if !Validator.shared().isValidEmail(email: emailAddressTextField.text!) {
                
                containerView.addSubview(emailTT)
                emailTT.setRelativeXControl(uiControl: emailAddressTextField)
                emailTT.setMessage(message: "Uh-oh - Invalid address")
                isValid = false
            }
        }
        
        if mobileNumberTextField.isEmpty() {
            
            containerView.addSubview(mobileTT)
            mobileTT.setRelativeXControl(uiControl: mobileNumberTextField)
            mobileTT.setMessage(message: "Please fill out Mobile Number")
            isValid = false
        }
        
        if messageTextField.isEmpty() {
            
            containerView.addSubview(messageTT)
            messageTT.setRelativeXControl(uiControl: messageTextField)
            messageTT.setMessage(message: "Please fill out Message")
            isValid = false
        }
        
        return isValid
    }
    
    private func setupToolTips() {
        reasonToContact.setToolTip(scTT: reasonToContactTT)
        nameTextField.setToolTip(scTT: nameTT)
        emailAddressTextField.setToolTip(scTT: emailTT)
        mobileNumberTextField.setToolTip(scTT: mobileTT)
        messageTextField.setToolTip(scTT: messageTT)
    }

    // MARK: - CollectionView
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notificationItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: notificationCellId, for: indexPath) as! NotificationViewCell
        cell.ntfItem = notificationItems[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 221, height: 72)
    }
    
    // MARK: - TableView
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        reasonToContact.text = listOfReasons[indexPath.row]
        reasonToContactDropDown.isHidden = true
        reasonToContact.removeToolTip()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listOfReasons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reasonCellId, for: indexPath as IndexPath) as! CountryTableViewCell
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        cell.countryName.text = listOfReasons[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }

    // MARK: - Events
    
    var isDropDownAdded : Bool = false
    
    @objc private func onDropDownIconClick(_button: UIButton) {
        
        if reasonToContactDropDown.isHidden {
            
            reasonToContactDropDown.isHidden = false
            
            if !isDropDownAdded {
                
                reasonToContactDropDown.layer.zPosition = 1
                reasonToContactDropDown.ContainerView.addSubview(reasonsTableView)
                
                _ = reasonsTableView.anchor(reasonToContactDropDown.ContainerView.topAnchor, left: reasonToContactDropDown.ContainerView.leftAnchor, bottom: reasonToContactDropDown.ContainerView.bottomAnchor, right: reasonToContactDropDown.ContainerView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
                isDropDownAdded = true
            }

            reasonsTableView.reloadData()
            reasonToContact.removeToolTip()
        }
    }

    @objc private func onCloseDropDownList(_button : UIButton) {
        reasonToContactDropDown.isHidden = true
    }
    
    @objc private func onMenuButtonClick() {
        print("Menu Item Clicked")
    }
    
    @objc private func onCancel() {
        view.endEditing(true)
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func onSendMessage(_button: SCButton) {

        if !validate() {
            return
        }
        
        view.endEditing(true)
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setupBaseViews()
        setupToolTips()
    }
    
    override func viewDidLayoutSubviews() {
        titleHr.update()
        //reasonToContactPanel.setup(label: "Reason to Contact")
        nameTextFieldPanel.setup(label: "Name")
        emailAddressTextFieldPanel.setup(label: "Email Address")
        mobileNumberTextFieldPanel.setup(label: "Mobile Number")
        messageTextFieldPanel.setup(label: "Message")
        reasonToContactDropDown.setup(label: "Reason To Contact")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    // MARK: - Navigation Bar
    
    private func setupNavBar() {
        
        navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationItem.title = "Contact Us"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.futura(fontSize: 18, type: .Medium)]
        
        let backButton = UIBarButtonItem(image: UIImage(named: "Back"), style: .plain, target: self, action: #selector(onBack))
        backButton.tintColor = .white
        self.navigationItem.leftBarButtonItem = backButton
        
        let ntfMenu = UIBarButtonItem(image: UIImage(named: "Notification"), style: .plain, target: self, action: #selector(onNtfMenuClick))
        ntfMenu.tintColor = .white
        self.navigationItem.rightBarButtonItem = ntfMenu
    }
    
    @objc private func onBack() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func onNtfMenuClick() {
        ecNtfPanel()
    }

    private func ecNtfPanel() {
        let width : CGFloat = (isNotifcationBarOpened) ? 0.0 : 221
        coverView.frame.origin.x = -(width)
        containerView.frame.origin.x = -(width)
        notificationContainer.isHidden = isNotifcationBarOpened
        notificationViewCollection.isHidden = isNotifcationBarOpened
        isNotifcationBarOpened = !isNotifcationBarOpened
    }
    
    // MARK: - TextField
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }

    @objc func textFieldDidChange(_ textField: SCXTextField) {
        if !textField.isEmpty() {
            textField.removeToolTip()
            textField.parentPanel.setTopLabel(widthValue: textField.floatingLabelValue)
        } else {
            textField.parentPanel.setTopLabel(widthValue: 0, hideText: true)
        }
    }

}
