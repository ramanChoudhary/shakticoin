//
//  PersonalInfoViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/16/19.
//  Copyright © 2019 Garnik Giloyan. All rights reserved.
//

//
//

import UIKit

class PersonalInfoViewController: SingleBaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Declarations
    
    lazy var tableView : UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.sectionHeaderHeight = 30.0

        return tableView
    }()
    
    let personalInfoCellId : String = "personalInfoCellId"
    var userProfileValue = [UserProfile]()
    
    // MARK: - Methods
    private func initUI() {
        backgroundImage.image = UIImage(named: "SettingsImage")
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        containerView.addSubview(tableView)
        _ = tableView.anchor(containerView.topAnchor, left: containerView.leftAnchor, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, topConstant: topBarHeight+35, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    private func registerCells() {
        tableView.register(PersonalInfoTableViewCell.self, forCellReuseIdentifier: personalInfoCellId)
    }
    
    private func getUserData() {
                
        let service = AccountService()
        
        showLoader()

//        DispatchQueue.main.async {
//            service.get(authorization: AuthorizationDataManager.shared.authorizationToken) { (output, statusCode) in
//                
//                hideLoader()
//                
//                print(statusCode)
//                
//                let fullName: String = (output["first_name"] as? String ?? "") + (output["last_name"] as? String ?? "")
//                let date_of_birth: String = (output["date_of_birth"] as? String ?? "May 16, 1900")
//                
//                self.userProfileValue.append(UserProfile(index: 0, rowTitle: fullName, rowValue: "", rowHeight: 20))
//                self.userProfileValue.append(UserProfile(index: 0, rowTitle: "Date of Birth", rowValue: date_of_birth, rowHeight: 50))
//
//                for resItem in output["residence"] as! [[String:Any]] {
//                    let fullAddress = (resItem["address_line_1"] as? String ?? "") + " " + (resItem["address_line_2"] as? String ?? "")
//                    let city_ = "\n" +  (resItem["city"] as? String ?? "")
//                    let zipCode = "\n" + (resItem["zip_code"] as? String ?? "") + " - " + (resItem["country_name"] as? String ?? "")
//                    self.userProfileValue.append(UserProfile(index: 0, rowTitle: "Address", rowValue: fullAddress + city_ + zipCode, rowHeight: 70))
//
//                }
//                
//                print(output)
//                
//                let email_address: String = (output["email"] as? String ?? "")
//                self.userProfileValue.append(UserProfile(index: 0, rowTitle: "Email", rowValue: email_address, rowHeight: 50))
//
//                DispatchQueue.main.async {
//                    self.tableView.reloadData()
//                }
//                
//            }
//        }
    }
    
    // MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.userProfileValue.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: personalInfoCellId, for: indexPath as IndexPath) as! PersonalInfoTableViewCell
        cell.rowTitle.text = userProfileValue[indexPath.row].rowTitle
        cell.rowValue.text = userProfileValue[indexPath.row].rowValue
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(userProfileValue[indexPath.row].rowHeight)
    }

    
    // MARK: - NavBar
    private func initNavBar() {
        self.navigationItem.title = "Personal Info"
    }
    
    // MARK: - ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        initNavBar()
        registerCells()
        initUI()
        getUserData()
    }
    

}
