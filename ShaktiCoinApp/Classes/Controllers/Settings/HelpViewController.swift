//
//  HelpViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/18/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController,UICollectionViewDelegate,
UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    // MARK: - Declarations
    lazy var backgroundImage: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "SettingsImage")
        return iv
    }()
    
    lazy var coverView: UIView = {
        let cv = UIView()
        //cv.contentMode = .scaleAspectFill
        cv.alpha = 0.50
        cv.backgroundColor = .black
        return cv
    }()
    
    lazy var containerView: UIScrollView = {
        let view = UIScrollView()
        view.contentSize.height = 1000
        return view
    }()
    
    lazy var notificationContainer : UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.75
        return view
    }()
    
    lazy var notificationViewCollection : UICollectionView = {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isPagingEnabled = true
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = .clear
        cv.showsHorizontalScrollIndicator = false
        
        return cv
    }()
    
    let notificationItems: [NotificationItem] = {
        
        let first = NotificationItem(title: "A friend has signed up.", message: "Alan Reynolds has registered you as a referral upon signing up.", date: "06/10/2018", isRead: nil)
        let second = NotificationItem(title: "Congratulations!", message: "You’ve chosen the 1008.00 SXE Bonus Bounty. Refer friends to unlock it sooner!", date: "05/10/2018", isRead: nil)
        let third = NotificationItem(title: "Welcome to your Wallet.", message: "Complete your KYC verification to fully unlock it and get ready for your Bonus Bounty!", date: "05/10/2018", isRead: nil)
        
        return [first, second, third]
    }()
    
    var isNotifcationBarOpened : Bool = false
    let notificationCellId: String = "notificationCellId"
    
    lazy var taxInfoContainerView : SC2LayerView = {
        let view = SC2LayerView()
        return view
    }()
    
    lazy var infoTextView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        
        let color = UIColor.white
        let font = UIFont.lato(fontSize: 13)
        let bold = UIFont.lato(fontSize: 13, type: .Bold)
        
        textView.setup(textInfo: "Contact Shakti: Support, Help, Client Service, and Much More!", textColor: color, font: bold)
        textView.setText(textContent: "\n\nNeed help? Customer service is here for you. Simply send an email to helpdesk@shakticoin.com", textColor: color, font: font)
        textView.setText(textContent: "\n\nWe’ll help you with any questions or issues including:", textColor: color, font: font)
        textView.setText(textContent: "\n•    Resetting your password", textColor: color, font: font)
        textView.setText(textContent: "\n•    Lost mobile device", textColor: color, font: font)
        textView.setText(textContent: "\n•    Referring a friend", textColor: color, font: font)
        textView.setText(textContent: "\n•    Registering your child at school", textColor: color, font: font)
        textView.setText(textContent: "\n\nIf your mobile device is stolen, send an email and we’ll immediately freeze your wallet. Just click here:", textColor: color, font: font)
        textView.setText(textContent: "\n\nDevice Stolen", textColor: color, font: font)
        textView.setText(textContent: "\n\nWe will take it from there.", textColor: color, font: bold)
        
        textView.setText(textContent: "\n\nPLEASE NOTE: ", textColor: color, font: bold)
        textView.setText(textContent: "Currently, Shakti does not have a phone number you can call. Simply send an email to our helpdesk. The Shakti helpdesk will find a way to resolve your issue as soon as possible.", textColor: color, font: font)
        
        textView.setText(textContent: "\n\nFAQs 101 ", textColor: color, font: bold)
        textView.setText(textContent: "on the Shakti website will be helpful. Please review it before you contact Shakti for greater efficiency and to save money.", textColor: color, font: font)
        
        textView.setText(textContent: "\n\nHelpdesk is Cost Recovery Service:", textColor: color, font: bold)
        textView.setText(textContent: "\n\nShakti ecosystem is owned and operated by Shakti Coin holders. When a Shakti wallet holder contacts helpdesk for customer support it costs everyone to provide the services. This cost is paid by the user, that is how we make everyone accountable.", textColor: color, font: font)
        textView.setText(textContent: "\n\nSo, depending on your specific inquiry, you’ll receive anything from a relatively generic Shakti customer service response to an individualized response addressing your question in detail.", textColor: color, font: bold)
        textView.setText(textContent: "\n\nIf your Shakti contact provides an answer to your question, but you have additional questions, be sure to respond to that same email thread, instead of contacting Shakti helpdesk service again by using the general helpdesk support email address.", textColor: color, font: font)
        
        textView.setText(textContent: "\n\nAs a general rule, asking help from Shakti wallet holders is sensible before contacting helpdesk, but remember NOT to share your password or surrender your mobile device to strangers.", textColor: color, font: font)
        
        textView.setLink(key: "_DeviceStolen", linkedText: "Device Stolen")
        textView.setLinkStyles(linkColor: UIColor.mainColor(), underlineColor: UIColor.mainColor())
        
        textView.finilize()
        return textView
    }()

    
    // MARK: - Methods
    
    private func registerCells() {
        notificationViewCollection.register(NotificationViewCell.self, forCellWithReuseIdentifier: notificationCellId)
    }
    
    private func setupBaseViews() {
        
        view.addSubview(backgroundImage)
        view.addSubview(coverView)
        view.addSubview(notificationContainer)
        view.addSubview(notificationViewCollection)
        view.addSubview(containerView)
        
        registerCells()
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = coverView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = containerView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = notificationContainer.anchor(view.topAnchor, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 221, heightConstant: 0)
        
        notificationContainer.isHidden = true
        
        _ = notificationViewCollection.anchor(view.topAnchor, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 221, heightConstant: 0)
        
        notificationViewCollection.layer.borderWidth = 0.5
        notificationViewCollection.layer.borderColor = UIColor.darkGray.cgColor
        
        notificationViewCollection.isHidden = true
        
        setupViews()
    }
    
    func setupViews() {
        
        containerView.addSubview(taxInfoContainerView)
        
        _ = taxInfoContainerView.anchor(containerView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 35, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 335, heightConstant: 950)
        _ = taxInfoContainerView.centralizeX(containerView.centerXAnchor)
        
        taxInfoContainerView.addSubview(infoTextView)
        _ = infoTextView.anchor(taxInfoContainerView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 300, heightConstant: 940)
        _ = infoTextView.centralizeX(taxInfoContainerView.centerXAnchor)
    }
    
    // MARK: - CollectionView
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notificationItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: notificationCellId, for: indexPath) as! NotificationViewCell
        cell.ntfItem = notificationItems[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 221, height: 72)
    }
    
    // MARK: - Events
    
    @objc private func onMenuButtonClick() {
        print("Menu Item Clicked")
    }
    
    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setupBaseViews()
    }
    
    override func viewDidLayoutSubviews() {
        taxInfoContainerView.transparency = 0.80
    }
    
    // MARK: - Navigation Bar
    
    private func setupNavBar() {
        
        navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationItem.title = "Help"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.futura(fontSize: 18, type: .Medium)]
        
        let backButton = UIBarButtonItem(image: UIImage(named: "Back"), style: .plain, target: self, action: #selector(onBack))
        backButton.tintColor = .white
        self.navigationItem.leftBarButtonItem = backButton
        
        let ntfMenu = UIBarButtonItem(image: UIImage(named: "Notification"), style: .plain, target: self, action: #selector(onNtfMenuClick))
        ntfMenu.tintColor = .white
        self.navigationItem.rightBarButtonItem = ntfMenu
    }
    
    @objc private func onBack() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func onNtfMenuClick() {
        ecNtfPanel()
    }
    
    private func ecNtfPanel() {
        let width : CGFloat = (isNotifcationBarOpened) ? 0.0 : 221
        coverView.frame.origin.x = -(width)
        containerView.frame.origin.x = -(width)
        notificationContainer.isHidden = isNotifcationBarOpened
        notificationViewCollection.isHidden = isNotifcationBarOpened
        isNotifcationBarOpened = !isNotifcationBarOpened
    }
}
