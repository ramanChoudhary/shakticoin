//
//  WelcomeViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 10/22/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    // MARK: - Declarations
    
    let welcomeCellId = "WelcomeCellId"
    
    let welcomePages: [WelcomePage] = {
        
        let signUp = WelcomePage(index: 1, title: "CreateId".localized, description: "SayHello".localized, image: "Checkmark.png", buttonTitle: nil)
        let bonusBounty = WelcomePage(index: 2, title: "ReceiveBounty".localized, description: "ReceiveBountyDesc".localized, image: "Bounty.png", buttonTitle: nil)
        let friends = WelcomePage(index: 3, title: "Referfriends".localized, description: "ReferfriendsDesc".localized, image: "Friends.png", buttonTitle: "LetsGo".localized)
        
        return [signUp, bonusBounty, friends]
    }()
    
    lazy var welcomeCollectionView: UICollectionView = {
       
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isPagingEnabled = true
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = .clear
        cv.showsHorizontalScrollIndicator = false
        
        return cv
    }()
    
    let backgroundImage: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "Welcome Tour Image.png")
        return iv
    }()
    
    let backgroundCover: UIView = {
        let view = UIView()
        view.contentMode = .scaleAspectFill
        view.alpha = 0.75
        view.backgroundColor = UIColor.black
        return view
    }()
    
    let pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.backgroundColor = .red
        pc.numberOfPages = 3
        pc.currentPage = 0
        pc.addTarget(self, action: #selector(pageControlClicked), for: .touchUpInside)
        return pc
    }()
    
    let gotoLogin : SCButton = {
        let btn = SCButton()
        btn.setTitle("LetsGo".localized, for: .normal)
        btn.accessibilityIdentifier = "LetsGo"
        btn.titleLabel?.font = UIFont.futura(fontSize: 14, type: FuturaPT.Medium)
        btn.CornerRadius = 18
        btn.isHidden = true
        btn.addTarget(self, action: #selector(gotoLoginClicked), for: .touchUpInside)
        return btn
    }()
    
    // MARK: - Methods
    private func registerCells() {
        welcomeCollectionView.register(WelcomeViewCell.self, forCellWithReuseIdentifier: welcomeCellId)
    }
    
    @objc private func pageControlClicked() { }
    
    @objc private func gotoLoginClicked() {
        
        let loginVC: LoginViewController = LoginViewController()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = loginVC
    }
    
    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()

        /*self.tempApiCalls { (output, statusCode) in
            print(output)
        }*/
         
        view.backgroundColor = .white
        
        view.addSubview(backgroundImage)
        view.addSubview(backgroundCover)
        view.addSubview(welcomeCollectionView)
        //view.addSubview(pageControl)
        view.addSubview(gotoLogin)
 
 
        /*_ = iconImage.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 86, heightConstant: 86)
        _ = iconImage.centralizeX(view.centerXAnchor)
        _ = iconImage.centralizeY(view.centerYAnchor, constant: -50)
        
        view.addSubview(textView)
        
        _ = textView.anchor(iconImage.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 85, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 150)*/
        
        //welcomeCollectionView.isHidden = true
        //welcomeCollectionView.backgroundColor = .red
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = backgroundCover.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = welcomeCollectionView.anchor(nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 105, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = welcomeCollectionView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5).isActive = true
        
        _ = gotoLogin.anchor(welcomeCollectionView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 5, leftConstant: 0, bottomConstant: -15, rightConstant: 0, widthConstant: 189, heightConstant: 36)
        
        _ = gotoLogin.centralizeX(view.centerXAnchor)
        
        /*view.addSubview(pageControl)
        
        _ = pageControl.anchor(nil, left: nil, bottom: view.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 50, rightConstant: 0, widthConstant: 0, heightConstant: 16)
        _ = pageControl.centralizeX(view.centerXAnchor)*/
        
        registerCells()
    }
    
    // MARK: - CollectionView
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return welcomePages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: welcomeCellId, for: indexPath) as! WelcomeViewCell
        let currentPage = welcomePages[indexPath.item]
        cell.pageControl.addTarget(self, action: #selector(pageControlClicked), for: .touchUpInside)
        cell.pageControl.currentPage = indexPath.row
        cell.page = currentPage
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }

    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let pageNumber = Int(targetContentOffset.pointee.x / view.frame.width)
        pageControl.currentPage = pageNumber
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        if pageNumber == welcomePages.count - 1 {
            self.gotoLogin.alpha = 0.0
            self.gotoLogin.isHidden = false
            UIView.animate(withDuration: 0.5, delay: 0.2, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.gotoLogin.alpha = 1.0
            }, completion: nil)
        } else {
            self.gotoLogin.isHidden = true
        }
    }
}


extension Data {
    var prettyPrintedJSONString: NSString? { /// NSString gives us a nice sanitized debugDescription
        guard let object = try? JSONSerialization.jsonObject(with: self, options: []),
              let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
              let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else { return nil }

        return prettyPrintedString
    }
}
