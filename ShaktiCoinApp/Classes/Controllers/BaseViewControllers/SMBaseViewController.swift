//
//  SMBaseViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 2/27/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import UIKit

class SMBaseViewController: UIViewController, UICollectionViewDelegate,
UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

     // MARK: - Declarations

    lazy var backgroundImage: UIImageView = {
       let iv = UIImageView()
       iv.image = UIImage(named: "Wallet Image")
       //iv.contentMode = .scaleAspectFit
       return iv
    }()

    lazy var coverView: UIView = {
       let cv = UIView()
       //cv.contentMode = .scaleAspectFill
       cv.alpha = 0.75
       cv.backgroundColor = .black
       return cv
    }()

    lazy var containerView : UIView = {
       let view = UIView()
       view.backgroundColor = .clear
       return view
    }()

    lazy var selectionMenu : SC2LayerView = {
        let view = SC2LayerView()
        view.backgroundColor = .clear
        view.transparency = 0.40
        return view
    }()
    
    lazy var selectionMenuViewCollection : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isPagingEnabled = true
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = .clear
        cv.showsHorizontalScrollIndicator = false
        return cv
    }()

    let selectionMenuCellId = "selectionMenuCellId"
    
    var menuItems : [SelectionMenuItem] = {
        let personalInfo = SelectionMenuItem(caption: "Personal Info", isSmall: false, isSelected: true)
        let personalInfo2 = SelectionMenuItem(caption: "", isSmall: true, isSelected: false)
        let additionalInfo = SelectionMenuItem(caption: "Additional Info", isSmall: false, isSelected: false)
        let additionalInfo2 = SelectionMenuItem(caption: "", isSmall: true, isSelected: false)
        let kycValidation = SelectionMenuItem(caption: "KYC Validation", isSmall: false, isSelected: false)
        return [personalInfo, personalInfo2, additionalInfo, additionalInfo2, kycValidation]
    }()

    // MARK: - Methods

    private func setupViews() {
               
        view.addSubview(backgroundImage)
        view.addSubview(coverView)
        view.addSubview(containerView)


        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

        _ = coverView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

        _ = containerView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        containerView.addSubview(selectionMenu)
        
        _ = selectionMenu.anchor(containerView.topAnchor, left: containerView.leftAnchor, bottom: nil, right: containerView.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 46)
        
        
        selectionMenu.addSubview(selectionMenuViewCollection)
        
        _ = selectionMenuViewCollection.anchor(selectionMenu.topAnchor, left: nil, bottom: selectionMenu.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 350, heightConstant: 0)
        
        _ = selectionMenuViewCollection.centralizeX(selectionMenu.centerXAnchor)
        
    }

    private func registerCells() {
        selectionMenuViewCollection.register(CircleMenuViewCell.self, forCellWithReuseIdentifier: selectionMenuCellId)
    }

    // MARK: - Navigation Bar

    private func setupNavBar() {
       
        navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationItem.title = "[]"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.futura(fontSize: 18, type: .Medium)]
    }

    // MARK: - CollectionView

    func numberOfSections(in collectionView: UICollectionView) -> Int {
       return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuItems.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: selectionMenuCellId, for: indexPath) as! CircleMenuViewCell
        cell.setup(caption: menuItems[indexPath.row].caption, isSmall: menuItems[indexPath.row].isSmall, isSelected:    menuItems[indexPath.row].isSelected)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 70, height: 60)
    }

    // MARK: - ViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setupViews()
        registerCells()
        applyMenu()
        applyNotifications()
    }

}
