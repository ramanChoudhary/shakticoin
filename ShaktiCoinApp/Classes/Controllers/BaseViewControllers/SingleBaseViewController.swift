//
//  SingleBaseViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/6/19.
//  Copyright © 2019 Garnik Giloyan. All rights reserved.
//

import UIKit

class SingleBaseViewController: UIViewController, UICollectionViewDelegate,
UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    // MARK: - Declarations
    
    lazy var backgroundImage: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "Wallet Image")
        //iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    lazy var coverView: UIView = {
        let cv = UIView()
        //cv.contentMode = .scaleAspectFill
        cv.alpha = 1.0
        cv.backgroundColor = UIColor(red:0, green:0, blue:0, alpha:0.7)
        return cv
    }()

    lazy var containerView : UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    var ntfMenu = UIBarButtonItem()
    let notificationItems: [NotificationItem] = {
        
        let first = NotificationItem(title: "_FirstTitle".localized, message: "_FirstMessage".localized, date: "06/10/2018", isRead: nil)
        let second = NotificationItem(title: "_SecondTitle".localized, message: "_SecondMessage".localized, date: "05/10/2018", isRead: nil)
        let third = NotificationItem(title: "_ThirdTitle".localized, message: "_ThirdMessage".localized, date: "05/10/2018", isRead: nil)
        
        return [first, second, third]
    }()

    lazy var notificationContainer : UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.75
        return view
    }()
    
    lazy var notificationViewCollection : UICollectionView = {

        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isPagingEnabled = true
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = .clear
        cv.showsHorizontalScrollIndicator = false
        
        return cv
    }()

    
    var isNotifcationBarOpened : Bool = false
    let notificationCellId = "notificationCellId"

    // MARK: - Methods
    
    private func setupViews() {
        
        view.addSubview(backgroundImage)
        view.addSubview(coverView)
        view.addSubview(notificationContainer)
        view.addSubview(notificationViewCollection)
        view.addSubview(containerView)
        
        registerCells()
        //'statusBarFrame' was deprecated in iOS 13.0: Use the statusBarManager property of the window scene instead.
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = coverView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = containerView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = notificationContainer.anchor(view.topAnchor, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 221, heightConstant: 0)
        
        notificationContainer.isHidden = true
        
        _ = notificationViewCollection.anchor(view.topAnchor, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 221, heightConstant: 0)
        
        notificationViewCollection.layer.borderWidth = 0.5
        notificationViewCollection.layer.borderColor = UIColor.darkGray.cgColor
        notificationViewCollection.isHidden = true
    }
    
    private func registerCells() {
        notificationViewCollection.register(NotificationViewCell.self, forCellWithReuseIdentifier: notificationCellId)
    }
    
    // MARK: - Navigation Bar

    private func setupNavBar() {
        
        navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationItem.title = "[Title]"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.futura(fontSize: 18, type: .Medium)]
        
        let backButton = UIBarButtonItem(image: UIImage(named: "Back"), style: .plain, target: self, action: #selector(onBack))
        backButton.tintColor = .white
        self.navigationItem.leftBarButtonItem = backButton
        
        let ntfMenu = UIBarButtonItem(image: UIImage(named: "Notification"), style: .plain, target: self, action: #selector(onNtfMenuClick))
        ntfMenu.tintColor = .white
        self.navigationItem.rightBarButtonItem = ntfMenu
    }
    
    @objc private func onBack() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func onNtfMenuClick() {
        ecNtfPanel()
    }

    private func ecNtfPanel() {
        let width : CGFloat = (isNotifcationBarOpened) ? 0.0 : 221
        
        coverView.frame.origin.x = -(width)
        containerView.frame.origin.x = -(width)
        
        notificationContainer.isHidden = isNotifcationBarOpened
        notificationViewCollection.isHidden = isNotifcationBarOpened
        
        ntfMenu.tintColor = (isNotifcationBarOpened == true) ? UIColor.white : UIColor.mainColor()
        isNotifcationBarOpened = !isNotifcationBarOpened
    }
    
    // MARK: - CollectionView
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notificationItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: notificationCellId, for: indexPath) as! NotificationViewCell
        cell.ntfItem = notificationItems[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 221, height: 72)
    }

    // MARK: - Events
    
    @objc private func onMenuButtonClick() {
        print("Menu Item Clicked")
    }
   
    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setupViews()
    }
}

