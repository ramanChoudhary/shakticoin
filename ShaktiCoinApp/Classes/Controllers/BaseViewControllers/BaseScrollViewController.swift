//
//  BaseScrollViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 2/24/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import UIKit

class BaseScrollViewController: UIViewController, MenuApplicable, NotificationsApplicable {

    // MARK: - Declarations

    lazy var backgroundImage: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "Wallet Image")
        //iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    lazy var coverView: UIView = {
        let cv = UIView()
        //cv.contentMode = .scaleAspectFill
        cv.alpha = 0.75
        cv.backgroundColor = .black
        return cv
    }()

    lazy var containerView : UIScrollView = {
        let view = UIScrollView()
        view.contentSize.height = 1250
        return view
    }()
    
    lazy var menuView: MenuView = {
        let view = MenuView()
        view.viewController = self
        return view
    }()
    
    lazy var notificationsView: NotificationsView = {
        let view = NotificationsView()
        view.viewController = self
        return view
    }()

    // MARK: - Methods
    
    private func setupViews() {
        view.addSubview(backgroundImage)
        view.addSubview(coverView)
        view.addSubview(containerView)

        //'statusBarFrame' was deprecated in iOS 13.0: Use the statusBarManager property of the window scene instead.
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = coverView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.topAnchor.constraint(equalTo: view.topAnchor, constant: topBarHeight).isActive = true
        containerView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        containerView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    // MARK: - Navigation Bar

    private func setupNavBar() {
        navigationController?.navigationBar.barTintColor = UIColor.black
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.futura(fontSize: 18, type: .Medium)]
    }
   
    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setupViews()
        applyMenu()
        applyNotifications()
    }
    
    func addContentView(_ viewController: UIViewController) {
        ViewHelper.configure(viewController.view, in: view)
        view.sendSubviewToBack(viewController.view)
        view.sendSubviewToBack(coverView)
        view.sendSubviewToBack(backgroundImage)
        view.sendSubviewToBack(containerView)
        addChild(viewController)
    }
}
