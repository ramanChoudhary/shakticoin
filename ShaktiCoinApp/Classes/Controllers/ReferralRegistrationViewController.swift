//
//  ReferralRegistrationViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 10/29/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit
import NKVPhonePicker

class ReferralRegistrationViewController: UIViewController, UITextFieldDelegate, CountriesViewControllerDelegate {
        

    // MARK: - Declarations
    
    lazy var backgroundImage: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "Register Referral Image")
        //iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    lazy var coverView: UIView = {
        let cv = UIView()
        //cv.contentMode = .scaleAspectFill
        cv.alpha = 0.75
        cv.backgroundColor = UIColor.black
        return cv
    }()

    lazy var lineTitle : SCLineTitle = {
        let lt = SCLineTitle()
        lt.Title = "OrType".localized
        return lt
    }()

    lazy var pageTitle : UILabel = {
        let label = UILabel()
        label.text = "WhoReferredYou".localized
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 24, type: .Light)
        return label
    }()

    lazy var textInfo : UITextView = {
        let textView = UITextView()
        textView.backgroundColor = .clear
        textView.isUserInteractionEnabled = false
        textView.isEditable = false
        
        let attributedText = NSMutableAttributedString(string: "_RRTextInfoLikeAnElephant".localized, attributes: [NSAttributedString.Key.font: UIFont.lato(fontSize: 14, type: .Bold), NSAttributedString.Key.foregroundColor: UIColor.white])
        
        attributedText.append(NSAttributedString(string: "_RRTextInfoAlwaysRemember".localized, attributes: [NSAttributedString.Key.font: UIFont.lato(fontSize: 14, type: .Bold), NSAttributedString.Key.foregroundColor: UIColor.white]))
        
        attributedText.append(NSAttributedString(string: "_RRTextInfoTellUs".localized, attributes: [NSAttributedString.Key.font: UIFont.lato(fontSize: 14, type: .Bold), NSAttributedString.Key.foregroundColor: UIColor.white]))
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        let length = attributedText.string.count
        attributedText.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: length))
        textView.attributedText = attributedText

        return textView
    }()
    
    lazy var qrButton : UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "QR"), for: .normal)
        button.setTitle("ScanQrCode".localized, for: .normal)
        button.backgroundColor = UIColor.qrButtonColor()
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        button.setTitleColor(UIColor.black, for: .normal)
        button.layer.cornerRadius = 20
        button.titleEdgeInsets.left = 25
        return button
    }()
   
    lazy var emailAddressPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var emailAddressField : SCXTextField = {
        let textField = SCXTextField()
        textField.delegate = self
        textField.floatingLabelValue = 120
        textField.font = UIFont.lato(fontSize: 14)
        textField.keyboardType = UIKeyboardType.emailAddress
        textField.textColor = .white
        textField.attributedPlaceholder = Util.NSMAttrString(text: "EmailAddress".localized, font: UIFont.lato(fontSize: 14))
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        return textField
    }()
    
    lazy var mobileNumberPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var mobileNumberField: SCXMobileField = {
        let textField = SCXMobileField(frame: .zero)
        textField.delegate = self
        textField.favoriteCountriesLocaleIdentifiers = ["CHE"]
        textField.phonePickerDelegate = self
        textField.countryPickerDelegate = self
        textField.textColor = .white
        //let country = Country.country(for: NKVSource(countryCode: Util.getCurrentCountryCode()))
        let country = Country.country(for: NKVSource(countryCode: "US"))
        textField.country = country
        return textField
    }()

    let emailAddressTT = SCToolTip()
    let mobileNumberTT = SCToolTip()
    
    lazy var rewardReferralButton : SCButton = {
        let button = SCButton()
        button.setTitle("RewardRefferal".localized, for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        button.CornerRadius = 18
        button.addTarget(self, action: #selector(onRewardReferral), for: .touchUpInside)
        return button
    }()
    
    lazy var dontRemeberReferral : UILabel = {
        let label = UILabel()
        label.text = "IdontRemeber".localized
        label.textColor = .white
        label.font = UIFont.futura(fontSize: 14, type: .Medium)
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(onCancel))
        label.addGestureRecognizer(tap)
        //label.underline()
        return label
    }()
    
    // MARK: - Methods
    
    private func setupToolTip() {
        emailAddressField.setToolTip(scTT: emailAddressTT)
    }
    
    fileprivate func observeKeyboardNotifications(){
        
       // NotificationCenter.default.addObserver(self, selector: #selector(KeyboardShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(KeyboardHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func KeyboardShow() {
        
        var y : CGFloat = -230
        if UIDevice.current.screenType == .iPhone_XR ||
            UIDevice.current.screenType == .iPhone_XSMax ||
            UIDevice.current.screenType == .iPhones_6Plus_6sPlus_7Plus_8Plus {
            y = -85
        }

        UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.view.frame = CGRect(x: 0, y: y, width: self.view.frame.width, height: self.view.frame.height)
        }, completion: nil)
    }
    
    @objc func KeyboardHide() {
        UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        }, completion: nil)
    }

    private func setupViews() {
        
        observeKeyboardNotifications()
        view.addSubview(backgroundImage)
        view.addSubview(coverView)
        view.addSubview(lineTitle)
        view.addSubview(pageTitle)
        view.addSubview(textInfo)
        view.addSubview(qrButton)
        
        view.addSubview(emailAddressPanel)
        view.addSubview(mobileNumberPanel)
        view.addSubview(rewardReferralButton)
        view.addSubview(dontRemeberReferral)
                
        _ = backgroundImage.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

        _ = coverView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
 
        _ = pageTitle.anchor(view.safeAreaLayoutGuide.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 50, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = pageTitle.centralizeX(view.centerXAnchor)
        
        _ = textInfo.anchor(pageTitle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 380, heightConstant: 85)
        _ = textInfo.centralizeX(view.centerXAnchor)
        
        _ = qrButton.anchor(textInfo.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = qrButton.centralizeX(view.centerXAnchor)
        
        _ = lineTitle.anchor(qrButton.bottomAnchor, left: nil
            , bottom: nil, right: nil, topConstant: 22, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: (view.frame.width * 87)/100, heightConstant: 15)
        _ = lineTitle.centralizeX(view.centerXAnchor)
        
        /*_ = fullNamePanel.anchor(lineTitle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 25, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = fullNamePanel.centralizeX(view.centerXAnchor)
        
        fullNamePanel.ContainerView.addSubview(fullNameField)
        fullNameField.parentPanel = fullNamePanel
        Util.fullyAnchor(_parnetControl: fullNamePanel.ContainerView, _childControl: fullNameField)*/
        
        _ = emailAddressPanel.anchor(lineTitle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = emailAddressPanel.centralizeX(view.centerXAnchor)
        
        emailAddressPanel.ContainerView.addSubview(emailAddressField)
        emailAddressField.parentPanel = emailAddressPanel
        Util.fullyAnchor(_parnetControl: emailAddressPanel.ContainerView, _childControl: emailAddressField)
        
        _ = mobileNumberPanel.anchor(emailAddressPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = mobileNumberPanel.centralizeX(view.centerXAnchor)
        
        mobileNumberPanel.ContainerView.addSubview(mobileNumberField)
        mobileNumberField.parentPanel = mobileNumberPanel
        Util.fullyAnchor(_parnetControl: mobileNumberPanel.ContainerView, _childControl: mobileNumberField)
        
        /*_ = referralCodePanel.anchor(mobileNumberPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = referralCodePanel.centralizeX(view.centerXAnchor)
        
        referralCodePanel.ContainerView.addSubview(referralCodeField)
        referralCodeField.parentPanel = referralCodePanel
        Util.fullyAnchor(_parnetControl: referralCodePanel.ContainerView, _childControl: referralCodeField)*/
        
        _ = rewardReferralButton.anchor(mobileNumberPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 45, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 36)
        _ = rewardReferralButton.centralizeX(view.centerXAnchor)
        
        _ = dontRemeberReferral.anchor(rewardReferralButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 30, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = dontRemeberReferral.centralizeX(view.centerXAnchor)
    }
    
    private func validate() -> Bool {
        
        var isValid : Bool = true
        emailAddressField.removeToolTip()
        
        if emailAddressField.isEmpty() && mobileNumberField.text.count <= mobileNumberField.code!.count {
            
            view.addSubview(emailAddressTT)
            emailAddressTT.setRelativeXControl(uiControl: emailAddressField)
            emailAddressTT.setMessage(message: "EmailOrPhoneRequired".localized)
            
            /*view.addSubview(mobileNumberTT)
            mobileNumberTT.setRelativeXControlMobile(uiControl: mobileNumberField)
            mobileNumberTT.setMessage(message: "Email Address or Phone Number is required")*/
            
            isValid = false
            
        } else {
            
            if !emailAddressField.isEmpty() {
                
                if !Validator.shared().isValidEmail(email: emailAddressField.text!) {
                    view.addSubview(emailAddressTT)
                    emailAddressTT.setRelativeXControl(uiControl: emailAddressField)
                    emailAddressTT.setMessage(message: "EmailInvalid".localized)
                    isValid = false
                }
            }
        }
        
        return isValid
    }
    
    // MARK: - Events
    
    @objc private func onRewardReferral() {
        self.view.endEditing(true)
        if validate() {
            let thankYouVC: ThankYouViewController = ThankYouViewController()
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = thankYouVC
        }
    }
    
    @objc private func onCancel() {
        self.view.endEditing(true)
        AppDelegate.shared?.perform(.myWallet)
    }
    
    // MARK: - TextField
    
    func countriesViewControllerDidCancel(_ sender: CountriesViewController) {
    }
    
    func countriesViewController(_ sender: CountriesViewController, didSelectCountry country: Country) {
        print(mobileNumberField.text.count)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
    @objc func textFieldDidChange(_ textField: SCXTextField) {
        if !textField.isEmpty() {
            textField.removeToolTip()
            textField.parentPanel.setTopLabel(widthValue: textField.floatingLabelValue)
        } else {
            textField.parentPanel.setTopLabel(widthValue: 0, hideText: true)
        }
    }

    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupToolTip()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    override func viewDidLayoutSubviews() {
        lineTitle.update()
        emailAddressPanel.setup(label: "EmailAddress".localized)
        mobileNumberPanel.setup(label: "MobileNumber".localized)
    }
}
