//
//  BusinessVaultIntroViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/15/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class BusinessVaultIntroViewController: BaseScrollViewController {

    // MARK: - Declarations
    
    lazy var titleView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.setup(textInfo: "Add-On Advantage", textColor: UIColor.white, font: UIFont.lato(fontSize: 24, type: .Light))
        textView.setText(textContent: "\nBusiness Vault", textColor: UIColor.white, font: UIFont.lato(fontSize: 24, type: .Light))
        textView.setText(textContent: "\n\nYou are eligible to Sign Up for SXE Vault.", textColor: UIColor.white, font: UIFont.lato(fontSize: 12, type: .Bold))
        textView.finilizeWithCenterlize()
        return textView
    }()

    lazy var descriptionView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.setup(textInfo: "If you have a business or you are planning to start a business within the next 6-months, you can sign up for SXE Vault.", textColor: UIColor(red:0.85, green:0.85, blue:0.85, alpha:1), font: UIFont.lato(fontSize: 14))
        textView.setText(textContent: "\n\nShakti Coin helps you become a one-world business leader and make important changes. So, enjoy new financial freedom for you and your loved ones.", textColor: UIColor(red:0.85, green:0.85, blue:0.85, alpha:1), font: UIFont.lato(fontSize: 14))
        textView.finilize()
        return textView
    }()

    lazy var limitedVaultsLbl : UILabel = {
        let label = UILabel()
        label.text = "Limited vaults for qualified candidates."
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 10)
        return label
    }()
    
    lazy var vaultBtn : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("I want my SXE Vault", for: .normal)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        button.addTarget(self, action: #selector(onSxeVaultClick(_button:)), for: .touchUpInside)
        return button
    }()
    
    lazy var cancelLbl : UILabel = {
        let label = UILabel()
        label.text = "No, I don’t want a vault for my business"
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 14)
        label.underline()
        return label
    }()
    
    lazy var bottomText : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.backgroundColor = .black
        textView.setup(textInfo: "The Bonus Bounty is granted to you as an introductory offer on a", textColor: .white, font: UIFont.lato(fontSize: 10))
        textView.setText(textContent: "\nfirst come, first serve basis. This is intended for early adopters who", textColor: .white, font: UIFont.lato(fontSize: 10))
        textView.setText(textContent: "\ndirect their efforts to refer other early adopters.", textColor: .white, font: UIFont.lato(fontSize: 10))
        textView.setText(textContent: "\nThe Shakti Foundation may end this promotional", textColor: .white, font: UIFont.lato(fontSize: 10))
        textView.setText(textContent: "\noffer without any notice.", textColor: .white, font: UIFont.lato(fontSize: 10))
        textView.contentInset = UIEdgeInsets(top: 25, left: 10, bottom: 0, right: 10)
        textView.finilizeWithCenterlize()
        return textView
    }()
    
    // MARK: - Methods
    
    func initViews() {
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)

        containerView.addSubview(titleView)
        _ = titleView.anchor(containerView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: topBarHeight+30, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 235, heightConstant: 90)
        _ = titleView.centralizeX(containerView.centerXAnchor)

        containerView.addSubview(descriptionView)
        _ = descriptionView.anchor(titleView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 45, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 315, heightConstant: 150)
        _ = descriptionView.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(limitedVaultsLbl)
        _ = limitedVaultsLbl.anchor(descriptionView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 35, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = limitedVaultsLbl.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(vaultBtn)
        _ = vaultBtn.anchor(limitedVaultsLbl.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 36)
        _ = vaultBtn.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(cancelLbl)
        _ = cancelLbl.anchor(vaultBtn.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = cancelLbl.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(bottomText)
        _ = bottomText.anchor(nil, left: containerView.leftAnchor, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 115)
    }
    
    // MARK: - Events
    
    @objc private func onSxeVaultClick(_button : UIButton) {
        
        let vc = BusinessVaultAddOnViewController()
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    // MARK: - ViewContoller
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuView.currentMenuItem = .vault
        initViews()
        initNavBar()
    }
    
    // MARK: - Navigation Bar
    
    private func initNavBar() {
        self.navigationItem.title = "Business Vault"
    }
}
