//
//  BusinessVaultAddOnAdvantageViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/15/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class BusinessVaultAddOnViewController: BaseScrollViewController {

    // MARK: - Declarations
    
    lazy var titleView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.setup(textInfo: "Add-On Advantage", textColor: UIColor.white, font: UIFont.lato(fontSize: 24, type: .Light))
        textView.setText(textContent: "\nBusiness Vault", textColor: UIColor.white, font: UIFont.lato(fontSize: 24, type: .Light))
        textView.setText(textContent: "\n\nChoose your vault type:", textColor: UIColor.white, font: UIFont.lato(fontSize: 12, type: .Bold))
        textView.finilizeWithCenterlize()
        return textView
    }()
    
    lazy var optionsView: UIView = {
        let view = UIView()
        return view
    }()
    
    lazy var stairView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.spacing = 5
        return sv
    }()
    
    lazy var leftCircleViewContainer : UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.30
        return view
    }()
    
    lazy var leftCircleImageView : UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "Merchant")
        view.tintColor = UIColor.mainColor()
        return view
    }()
    
    lazy var rightViewContainer : UIView = {
        let view = UIView()
        return view
    }()
    
    lazy var rightTextView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        let color = UIColor(red:0.85, green:0.85, blue:0.85, alpha:1)
        textView.setup(textInfo: "Merchant Features33333", textColor: color, font: UIFont.lato(fontSize: 12, type: .Bold))
        textView.setText(textContent: "\n\n• Reduce banking charges.", textColor: color, font: UIFont.lato(fontSize: 12))
        textView.setText(textContent: "\n\n• Eliminate banking time.", textColor: color, font: UIFont.lato(fontSize: 12))
        textView.setText(textContent: "\n\n• Sell goods and services using universal currency.", textColor: color, font: UIFont.lato(fontSize: 12))
        textView.setText(textContent: "\n\n• Track tax obligations in real-time.", textColor: color, font: UIFont.lato(fontSize: 12))
        textView.finilize()
        return textView
    }()
    
    lazy var bottomTextView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.setup(textInfo: "Merchant ", textColor: .white, font: UIFont.lato(fontSize: 24, type: .Light))
        textView.setText(textContent: "\n\nImprove your business with a Merchant Vault, a value of $25,000 USD. You will be eligible for a Bonus Bounty of 5,000 SXE.", textColor: .white, font: UIFont.lato(fontSize: 14))
        textView.finilizeWithCenterlize()
        return textView
    }()
    
    lazy var actionButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Let’s get started", for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        button.titleLabel?.textColor = .white
        return button
    }()
    
    var listOfBusinessVaults : [BusinessVault]!
    let imageList = ["Merchant", "PowerMiner", "Shape", "Global", "Dev"]
    
    // MARK: - Methods
    
    private func getVaults() {
        
//        typealias businessVaultObjects = [BusinessVault]
//        
//        let service = VaultService()
//        showLoader()
//        DispatchQueue.main.async {
//            service.get(authorization: AuthorizationDataManager.shared.authorizationToken) { (output, statusCode) in
//                
//                hideLoader()
//                
//                self.listOfBusinessVaults = output
//                DispatchQueue.main.async {
//                    self.setupOptionsView()
//                }
//            }
//        }
    }
        
    func initViews() {
        
        containerView.addSubview(titleView)
        containerView.addSubview(optionsView)
        containerView.addSubview(bottomTextView)
        containerView.addSubview(actionButton)
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)

        _ = titleView.anchor(containerView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: topBarHeight + 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 95)
        _ = titleView.centralizeX(containerView.centerXAnchor)
        
        _ = optionsView.anchor(titleView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 280)
        
        _ = bottomTextView.anchor(optionsView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 345, heightConstant: 125)
        _ = bottomTextView.centralizeX(containerView.centerXAnchor)
        
        _ = actionButton.anchor(bottomTextView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 280, heightConstant: 36)
        
        _ = actionButton.centralizeX(containerView.centerXAnchor)
        
        getVaults()
    }
    
    func setupOptionsView () {
        
        optionsView.addSubview(stairView)
        optionsView.addSubview(leftCircleViewContainer)
        optionsView.addSubview(rightViewContainer)
        
        _ = leftCircleViewContainer.anchor(nil, left: optionsView.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 18, bottomConstant: 0, rightConstant: 0, widthConstant: 132, heightConstant: 132)
        _ = leftCircleViewContainer.centralizeY(optionsView.centerYAnchor)
        
        leftCircleViewContainer.addSubview(leftCircleImageView)
        _ = leftCircleImageView.centralizeX(leftCircleViewContainer.centerXAnchor)
        _ = leftCircleImageView.centralizeY(leftCircleViewContainer.centerYAnchor)
        
        _ = rightViewContainer.anchor(optionsView.topAnchor, left: nil, bottom: optionsView.bottomAnchor, right: optionsView.rightAnchor, topConstant: 35, leftConstant: 0, bottomConstant: 35, rightConstant: 10, widthConstant: 140, heightConstant: 0)
        
        rightViewContainer.addSubview(rightTextView)
        _ = rightTextView.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 125, heightConstant: 160)
        _ = rightTextView.centralizeX(rightViewContainer.centerXAnchor)
        _ = rightTextView.centralizeY(rightViewContainer.centerYAnchor)
        
        _ = stairView.widthAnchor.constraint(equalToConstant: 60).isActive = true
        _ = stairView.centralizeY(optionsView.centerYAnchor)
        _ = stairView.centralizeX(optionsView.centerXAnchor)
        
        for i in 1...self.listOfBusinessVaults.count {
            
            let optionButton = SMButton()
            let selectedImage: String = (i == 1) ? "Active" : "Inactive"
            optionButton.seqNo = i
            optionButton.isActive = (i == 1) ? true : false
            optionButton.setImage(UIImage(named: selectedImage), for: .normal)
            optionButton.addTarget(self, action: #selector(onOptionButtonClick(_:)), for: .touchUpInside)
            stairView.addArrangedSubview(optionButton)
        }
        
        setActiveText(seqNo: 1)
    }
    
    private func changeTheOption(seqNo: Int) {
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            
            self.leftCircleViewContainer.frame.origin.y -= 100
            self.leftCircleViewContainer.alpha = 0.0
            
            self.rightViewContainer.frame.origin.y += 100
            self.rightViewContainer.alpha = 0.0
            
        }, completion: { (finished: Bool) in
            
            self.leftCircleImageView.isHidden = true
            self.leftCircleImageView.image = UIImage(named: self.imageList[seqNo-1])
            
            self.resetValues(seqNo: seqNo)
        })
    }
    
    private func resetValues(seqNo : Int) {
        
        self.leftCircleViewContainer.frame.origin.y += 130
        self.rightViewContainer.frame.origin.y -= 130
        
        self.leftCircleImageView.isHidden = false
        setActiveText(seqNo: seqNo)
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            
            self.leftCircleViewContainer.frame.origin.y -= 30
            self.leftCircleViewContainer.alpha = 0.5
            
            self.rightViewContainer.frame.origin.y += 30
            self.rightViewContainer.alpha = 0.5
            
        }, completion: nil)
    }
    
    private func setActiveText(seqNo : Int) {
        
        
        let color = UIColor(red:0.85, green:0.85, blue:0.85, alpha:1)
        rightTextView.reset()
        bottomTextView.reset()
        
        rightTextView.setup(textInfo: listOfBusinessVaults[(seqNo - 1)].name, textColor: color, font: UIFont.lato(fontSize: 12, type: .Bold))
        for _f in listOfBusinessVaults[(seqNo - 1)].features {
            rightTextView.setText(textContent: "\n\n• " + _f, textColor: color, font: UIFont.lato(fontSize: 12))
        }

        bottomTextView.setup(textInfo: listOfBusinessVaults[(seqNo - 1)].name, textColor: .white, font: UIFont.lato(fontSize: 24, type: .Light))
        bottomTextView.setText(textContent: "\n\n" + (listOfBusinessVaults[(seqNo - 1)].bonus?.description ?? ""), textColor: .white, font: UIFont.lato(fontSize: 14))
        actionButton.setTitle(listOfBusinessVaults[(seqNo - 1)].transition_button_label, for: .normal)

        rightTextView.finilize()
        bottomTextView.finilizeWithCenterlize()
    }
    
    // MARK: - Events
    
    @objc private func onOptionButtonClick(_ button: SMButton) {
        
        if button.isActive { return }
        
        button.isActive = true
        button.setImage(UIImage(named: "Active"), for: .normal)
        for case let eachButton as SMButton in stairView.subviews {
            
            if eachButton.seqNo != button.seqNo {
                eachButton.isActive = false
                eachButton.setImage(UIImage(named: "Inactive"), for: .normal)
            }
        }
        
        changeTheOption(seqNo: button.seqNo)
    }
    
    
    // MARK: - ViewContoller
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViews()
        initNavBar()
    }
    
    override func viewDidLayoutSubviews() {
        
        leftCircleViewContainer.layer.cornerRadius = leftCircleViewContainer.frame.width/2
        leftCircleViewContainer.clipsToBounds = true
    }
    
    // MARK: - Navigation Bar
    
    private func initNavBar() {
        
        self.navigationItem.title = "Business Vault"
    }
    
    @objc private func onBack() {
        navigationController?.popViewController(animated: false)
    }
}
