//
//  MyReferralsFinalViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/14/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class MyReferralsFinalViewController: UIViewController,UICollectionViewDelegate,
UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource {

    // MARK: - Declarations
    
    lazy var backgroundImage: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "ReferralsImage")
        //iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    lazy var coverView: UIView = {
        let cv = UIView()
        cv.contentMode = .scaleAspectFill
        cv.alpha = 0.80
        cv.backgroundColor = .black
        return cv
    }()
    
    lazy var containerView: UICollectionView = {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isPagingEnabled = true
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = .clear
        cv.showsVerticalScrollIndicator = true
        
        return cv
    }()
    
    let notificationItems: [NotificationItem] = {
        
        let first = NotificationItem(title: "A friend has signed up.", message: "Alan Reynolds has registered you as a referral upon signing up.", date: "06/10/2018", isRead: nil)
        let second = NotificationItem(title: "Congratulations!", message: "You’ve chosen the 1008.00 SXE Bonus Bounty. Refer friends to unlock it sooner!", date: "05/10/2018", isRead: nil)
        let third = NotificationItem(title: "Welcome to your Wallet.", message: "Complete your KYC verification to fully unlock it and get ready for your Bonus Bounty!", date: "05/10/2018", isRead: nil)
        
        return [first, second, third]
    }()
    
    lazy var notificationContainer : UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.75
        return view
    }()
    
    lazy var notificationViewCollection : UICollectionView = {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isPagingEnabled = true
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = .clear
        cv.showsHorizontalScrollIndicator = false
        
        return cv
    }()
    
    lazy var tableView : UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = 32
        tableView.separatorInset = .zero
        //tableView.separatorStyle = .none
        //tableView.isScrollEnabled = false
        tableView.backgroundColor = .clear
        return tableView
    }()

    var isNotifcationBarOpened : Bool = false
    let notificationCellId: String = "notificationCellId"
    let viewCellId : String = "ViewCellId"
    let contactViewCellId : String = "contactViewCellId"
    
    // MARK: - Methods
    
    private func registerCells() {
        notificationViewCollection.register(NotificationViewCell.self, forCellWithReuseIdentifier: notificationCellId)
        containerView.register(MyReferralsFinalViewCell.self, forCellWithReuseIdentifier: viewCellId)
        tableView.register(ContactTableViewCell.self, forCellReuseIdentifier: contactViewCellId)
        tableView.register(ContactSectionTableViewCell.self, forCellReuseIdentifier: "Section")
    }
    
    private func setupBaseViews() {
        
        view.addSubview(backgroundImage)
        view.addSubview(coverView)
        view.addSubview(notificationContainer)
        view.addSubview(notificationViewCollection)
        view.addSubview(containerView)
        
        registerCells()
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = coverView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = containerView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

        _ = notificationContainer.anchor(view.topAnchor, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 221, heightConstant: 0)
        
        notificationContainer.isHidden = true
        
        _ = notificationViewCollection.anchor(view.topAnchor, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 221, heightConstant: 0)
        
        notificationViewCollection.layer.borderWidth = 0.5
        notificationViewCollection.layer.borderColor = UIColor.darkGray.cgColor
        
        notificationViewCollection.isHidden = true
    }
    
    // MARK: - TableView
    
    var sectionAList : [Contact] = {
        let andrea = Contact(profilePicture : "pp1", fullName: "Andrea Collins", email: "creynolds@josefina.biz", mobile: "+1 (809)909-9876")
        return [andrea]
    }()
    
    var sectionBList : [Contact] = {
        let ann = Contact(profilePicture : "pp2", fullName: "Ann Smith", email: "ann@josefina.biz", mobile: "+1 (809)406-8965")
        let jesica = Contact(profilePicture : "pp1", fullName: "Jesica Doe", email: "jesica@josefina.biz", mobile: "+1 (904)365-8965")
        return [ann, jesica]
    }()
    
    var sectionCList : [Contact] = {
        let jena = Contact(profilePicture : "pp3", fullName: "Jena Smith", email: "dena@josefina.biz", mobile: "+1 (809)406-1198")
        let deni = Contact(profilePicture : "pp2", fullName: "Deni Doe", email: "deni@josefina.biz", mobile: "+1 (904)365-5689")
        return [jena, deni]
    }()
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let  sectionCell = tableView.dequeueReusableCell(withIdentifier: "Section") as! ContactSectionTableViewCell
        sectionCell.setup(index: section)
        return sectionCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 2
        case 2:
            return 2
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: contactViewCellId, for: indexPath as IndexPath) as! ContactTableViewCell
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        
        switch (indexPath.section) {
        case 0:
            cell.profilePic.image = UIImage(named: sectionAList[indexPath.row].profilePicture)
            cell.fullName.text = sectionAList[indexPath.row].fullName
            cell.email.text = sectionAList[indexPath.row].email
            cell.mobile.text = sectionAList[indexPath.row].mobile
        case 1:
            
            cell.profilePic.image = UIImage(named: sectionBList[indexPath.row].profilePicture)
            cell.fullName.text = sectionBList[indexPath.row].fullName
            cell.email.text = sectionBList[indexPath.row].email
            cell.mobile.text = sectionBList[indexPath.row].mobile
            
        case 2:
            
            cell.profilePic.image = UIImage(named: sectionCList[indexPath.row].profilePicture)
            cell.fullName.text = sectionCList[indexPath.row].fullName
            cell.email.text = sectionCList[indexPath.row].email
            cell.mobile.text = sectionCList[indexPath.row].mobile
            
        default:
            cell.textLabel?.text = "Other"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    // MARK: - Navigation Bar
    
    private func setupNavBar() {
        
        navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationItem.title = "My Referrals"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.futura(fontSize: 18, type: .Medium)]
        
        let backButton = UIBarButtonItem(image: UIImage(named: "Back"), style: .plain, target: self, action: #selector(onBack))
        backButton.tintColor = .white
        self.navigationItem.leftBarButtonItem = backButton

        let ntfMenu = UIBarButtonItem(image: UIImage(named: "Notification"), style: .plain, target: self, action: #selector(onNtfMenuClick))
        ntfMenu.tintColor = .white
        self.navigationItem.rightBarButtonItem = ntfMenu
    }
    
    @objc private func onBack() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func onNtfMenuClick() {
        ecNtfPanel()
    }

    private func ecNtfPanel() {
        let width : CGFloat = (isNotifcationBarOpened) ? 0.0 : 221
        coverView.frame.origin.x = -(width)
        containerView.frame.origin.x = -(width)
        notificationContainer.isHidden = isNotifcationBarOpened
        notificationViewCollection.isHidden = isNotifcationBarOpened
        isNotifcationBarOpened = !isNotifcationBarOpened
    }
    
    // MARK: - CollectionView
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notificationItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == notificationViewCollection {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: notificationCellId, for: indexPath) as! NotificationViewCell
            cell.ntfItem = notificationItems[indexPath.row]
            return cell
            
        } else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: viewCellId, for: indexPath) as! MyReferralsFinalViewCell
            cell.setup(seqNo: SeqNo(rawValue: indexPath.row)!)
            if indexPath.row == 1 {
                
                cell.tableContainer.addSubview(tableView)
                _ = tableView.anchor(cell.tableContainer.topAnchor, left: cell.tableContainer.leftAnchor, bottom: cell.tableContainer.bottomAnchor, right: cell.tableContainer.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
 
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == notificationViewCollection {
            return CGSize(width: 221, height: 72)
        } else {
            if indexPath.row == 0 {
                return CGSize(width: view.frame.width, height: view.frame.height)
            } else {
                return CGSize(width: view.frame.width, height: (view.frame.height/2)+25)
            }
        }
    }
    
    // MARK: - Events
    
    @objc private func onMenuButtonClick() {
        print("Menu Item Clicked")
    }
    
    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setupBaseViews()
    }
    
    override func viewDidLayoutSubviews() {
        if containerView.visibleCells.count > 0 {
            (containerView.visibleCells[0] as! MyReferralsFinalViewCell).finilizeMe()
        }
    }
}
