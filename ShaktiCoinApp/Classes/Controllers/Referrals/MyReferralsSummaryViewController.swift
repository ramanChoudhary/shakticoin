//
//  MyReferralsSummaryViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/11/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class MyReferralsSummaryViewController: BaseScrollViewController {

    // MARK: - Declarations
    
    lazy var contentView: UICollectionView = {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isPagingEnabled = true
//        cv.delegate = self
//        cv.dataSource = self
        cv.backgroundColor = .clear
        cv.showsVerticalScrollIndicator = true
        
        return cv
    }()
    
    let viewCellId : String = "ViewCellId"
    
    // MARK: - Methods
    
    private func registerCells() {
        contentView.register(MyReferralsSummaryViewCell.self, forCellWithReuseIdentifier: viewCellId)
    }
    
    private func setupBaseViews() {
        view.addSubview(contentView)
        
        registerCells()
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        _ = contentView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    // MARK: - Navigation Bar
    
    private func setupNavBar() {
        navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationItem.title = "My Referrals"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.futura(fontSize: 18, type: .Medium)]
    }

    @objc private func onAddReferralClick() {
        let vc = AddReferralViewController()
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @objc private func onEffortRateClick() {
        let vc = EffortRatesViewController()
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setupBaseViews()
    }
    
    override func viewDidLayoutSubviews() {
        if contentView.visibleCells.count > 0 {
            (contentView.visibleCells[0] as! MyReferralsSummaryViewCell).finilizeMe()
        }
    }
}
