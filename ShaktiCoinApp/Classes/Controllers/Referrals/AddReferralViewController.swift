//
//  AddReferralViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/12/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit
import NKVPhonePicker
import ContactsUI

class AddReferralViewController: BaseScrollViewController, UITextFieldDelegate,
    CountriesViewControllerDelegate,
    CNContactViewControllerDelegate {
    
    // MARK: - Declarations
    
    let pageTitle : String = "My Referrals"
    
    lazy var balanceView : _CurrentBalance = {
        let view = _CurrentBalance()
        view.unlockQuestion.Title = "Fill in the referral info below"
        view.lockedLink.text = "30 months to unlock"
        return view
    }()
    
    lazy var firstNamePanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var firstNameTextField : SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 100
        textField.font = UIFont.lato(fontSize: 14)
        textField.textColor = .white
        textField.attributedPlaceholder = Util.NSMAttrString(text: "First Name", font: UIFont.lato(fontSize: 14))
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField.delegate = self
        textField.autocorrectionType = .no
        return textField
    }()
    
    lazy var lastNamePanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var lastNameTextField : SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 100
        textField.font = UIFont.lato(fontSize: 14)
        textField.textColor = .white
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Last Name", font: UIFont.lato(fontSize: 14))
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField.delegate = self
        textField.autocorrectionType = .no
        return textField
    }()
    
    lazy var emailAddressPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var emailAddressTextField : SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 120
        textField.font = UIFont.lato(fontSize: 14)
        textField.textColor = .white
        textField.keyboardType = .emailAddress
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Email Address", font: UIFont.lato(fontSize: 14))
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField.delegate = self
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        return textField
    }()
    
    lazy var mobileNumberPanel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()

    lazy var mobileNumberTextField: NKVPhonePickerTextField = {
        let textField = NKVPhonePickerTextField(frame: .zero)
        textField.font = UIFont.lato(fontSize: 14)
        textField.favoriteCountriesLocaleIdentifiers = ["CHE"]
        textField.phonePickerDelegate = self
        textField.countryPickerDelegate = self
        textField.textColor = .white
        let country = Country.country(for: NKVSource(countryCode: Util.getCurrentCountryCode()))
        textField.country = country
        return textField
    }()
    
    lazy var addFromContactsButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Add from Contacts List", for: .normal)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.futura(fontSize: 14)
        button.addTarget(self, action: #selector(onAddFromContacts(_sender:)), for: .touchUpInside)
        return button
    }()
    
    lazy var addReferralButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Add Referral Info", for: .normal)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.futura(fontSize: 14)
        button.addTarget(self, action: #selector(onAddReferral(_sender:)), for: .touchUpInside)
        return button
    }()
    
    lazy var waitingIndicator:UIActivityIndicatorView = {
        let wi: UIActivityIndicatorView = UIActivityIndicatorView()
        wi.frame = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: 40.0, height: 40.0));
        wi.center = self.view.center
        wi.hidesWhenStopped = true
        if #available(iOS 13.0, *) {
            wi.style = UIActivityIndicatorView.Style.large
        } else {
            // Fallback on earlier versions
        }
        return wi
    }()
    
    let firstNameTT = SCToolTip()
    let lastNameTT = SCToolTip()
    let emailAddressTT = SCToolTip()
    
    // MARK: - Methods
    
    private func setupToolTips() {
        firstNameTextField.setToolTip(scTT: firstNameTT)
        lastNameTextField.setToolTip(scTT: lastNameTT)
        emailAddressTextField.setToolTip(scTT: emailAddressTT)
    }

    func startWI() {
        waitingIndicator.startAnimating()
        view.isUserInteractionEnabled = false
    }
    
    func stopWI() {
        waitingIndicator.stopAnimating()
        view.isUserInteractionEnabled = false
    }
    
    func initViews() {
        
        containerView.addSubview(waitingIndicator)
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)

        containerView.addSubview(balanceView)
        _ = balanceView.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: topBarHeight+5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 200)
        
        containerView.addSubview(firstNamePanel)
        containerView.addSubview(lastNamePanel)
        containerView.addSubview(emailAddressPanel)
        containerView.addSubview(mobileNumberPanel)
        
        _ = firstNamePanel.anchor(balanceView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = firstNamePanel.centralizeX(view.centerXAnchor)
        
        firstNamePanel.ContainerView.addSubview(firstNameTextField)
        firstNameTextField.parentPanel = firstNamePanel
        Util.fullyAnchor(_parnetControl: firstNamePanel.ContainerView, _childControl: firstNameTextField)

        _ = lastNamePanel.anchor(firstNamePanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 18, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = lastNamePanel.centralizeX(balanceView.centerXAnchor)
        
        lastNamePanel.ContainerView.addSubview(lastNameTextField)
        lastNameTextField.parentPanel = lastNamePanel
        Util.fullyAnchor(_parnetControl: lastNamePanel.ContainerView, _childControl: lastNameTextField)
        
        _ = emailAddressPanel.anchor(lastNamePanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 18, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = emailAddressPanel.centralizeX(balanceView.centerXAnchor)
        
        emailAddressPanel.ContainerView.addSubview(emailAddressTextField)
        emailAddressTextField.parentPanel = emailAddressPanel
        Util.fullyAnchor(_parnetControl: emailAddressPanel.ContainerView, _childControl: emailAddressTextField)
        
        _ = mobileNumberPanel.anchor(emailAddressPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 18, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = mobileNumberPanel.centralizeX(balanceView.centerXAnchor)
        
        mobileNumberPanel.ContainerView.addSubview(mobileNumberTextField)
        Util.fullyAnchor(_parnetControl: mobileNumberPanel.ContainerView, _childControl: mobileNumberTextField)
        
        containerView.addSubview(addFromContactsButton)
        
        _ = addFromContactsButton.anchor(mobileNumberPanel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 45, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 280, heightConstant: 36)
        _ = addFromContactsButton.centralizeX(view.centerXAnchor)
        
        containerView.addSubview(addReferralButton)
        
        _ = addReferralButton.anchor(addFromContactsButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 280, heightConstant: 36)
        _ = addReferralButton.centralizeX(view.centerXAnchor)
    }
    
    private func Validate() -> Bool {
        
        var isValid: Bool = true
        
        if firstNameTextField.isEmpty() {
            containerView.addSubview(firstNameTT)
            firstNameTT.setRelativeXControl(uiControl: firstNameTextField)
            firstNameTT.setMessage(message: "Please fill first name")
            isValid = false
        }

        if lastNameTextField.isEmpty() {
            containerView.addSubview(lastNameTT)
            lastNameTT.setRelativeXControl(uiControl: lastNameTextField)
            lastNameTT.setMessage(message: "Please fill last name")
            isValid = false
        }
        
        if emailAddressTextField.isEmpty() {
            containerView.addSubview(emailAddressTT)
            emailAddressTT.setRelativeXControl(uiControl: emailAddressTextField)
            emailAddressTT.setMessage(message: "Please fill email address")
            isValid = false
            
        } else {
            
            if !Validator.shared().isValidEmail(email: emailAddressTextField.text!) {
                containerView.addSubview(emailAddressTT)
                emailAddressTT.setRelativeXControl(uiControl: emailAddressTextField)
                emailAddressTT.setMessage(message: "Uh-oh - Invalid address")
                isValid = false
            }
        }
        return isValid
    }
    
    // MARK: - Events

    @objc private func onAddReferral(_sender: SCButton) {
        if Validate() {
        
//            let referral = Referral(firstName: firstNameTextField.text!, lastName: lastNameTextField.text!, email: emailAddressTextField.text!, phone: 0, referrer: "", media: "EMAIL", createdDate: nil, socialMedia: nil, status: nil)
//            
//            showLoader()

//            DispatchQueue.main.async {
//                let referralService = ReferralService()
//                referralService.addReferral(authorization: AuthorizationDataManager.shared.authorizationToken, referral: referral) { (statusCode) in
//                    hideLoader()
//
//                    print(statusCode)
//                    DispatchQueue.main.async {
//                        if statusCode == 201 {
//                            let alert = UIAlertController(title: self.pageTitle, message: "The referral successfully added.", preferredStyle: UIAlertController.Style.alert)
//                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
//                            self.present(alert, animated: true, completion: nil)
//                        } else {
//                            let alert = UIAlertController(title: self.pageTitle, message: "Something went wrong", preferredStyle: UIAlertController.Style.alert)
//                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
//                            self.present(alert, animated: true, completion: nil)
//                        }
//                    }
//                }
//            }
            
        }
        
    }
    
    @objc private func onAddFromContacts(_sender: SCButton) {
        
        /*let contactsViewController = CNContactViewController()
        contactsViewController.delegate = self
        self.navigationController?.pushViewController(contactsViewController, animated: false)*/
    }
    
    // MARK: - ContactViewController Delegates
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        print(contact.phoneNumbers)
        let numbers = contact.phoneNumbers.first
        print((numbers?.value)?.stringValue ?? "")
    }

    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        self.dismiss(animated: true, completion: nil)
    }

    
    // MARK: - TextField Delegates
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
    @objc func textFieldDidChange(_ textField: SCXTextField) {
        if !textField.isEmpty() {
            textField.removeToolTip()
            textField.parentPanel.setTopLabel(widthValue: textField.floatingLabelValue)
        } else {
            textField.parentPanel.setTopLabel(widthValue: 0, hideText: true)
        }
    }

    func countriesViewControllerDidCancel(_ sender: CountriesViewController) { }
    
    func countriesViewController(_ sender: CountriesViewController, didSelectCountry country: Country) {}

    // MARK: - ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        initNavBar()
        initViews()
        setupToolTips()
    }
    
    override func viewDidLayoutSubviews() {
        balanceView.finalizeMe()
        firstNamePanel.setup(label: "First Name")
        lastNamePanel.setup(label: "Last Name")
        emailAddressPanel.setup(label: "Email Address")
        mobileNumberPanel.setup(label: "Mobile Number")
    }
    
    // MARK: - Navigation Bar
    
    private func initNavBar() {
        self.navigationItem.title = pageTitle
    }
    
    @objc private func onBack() {
        navigationController?.popViewController(animated: true)
    }
}
