//
//  MyReferralsViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/7/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit


class MyReferralsViewController: UIViewController,UICollectionViewDelegate,
UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    // MARK: - Declarations
    
    lazy var backgroundImage: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "ReferralsImage")
        //iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    lazy var coverView: UIView = {
        let cv = UIView()
        cv.contentMode = .scaleAspectFill
        cv.alpha = 0.80
        cv.backgroundColor = .black
        return cv
    }()

    lazy var containerView: UICollectionView = {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isPagingEnabled = true
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = .clear
        cv.showsVerticalScrollIndicator = true
        return cv
    }()
    
    lazy var menuContainer : UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.75
        return view
    }()
    
    lazy var menuViewCollection: UICollectionView = {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isPagingEnabled = true
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = .black
        cv.alpha = 0.75
        cv.showsHorizontalScrollIndicator = false
        
        return cv
    }()
    
    let notificationItems: [NotificationItem] = {
        
        let first = NotificationItem(title: "A friend has signed up.", message: "Alan Reynolds has registered you as a referral upon signing up.", date: "06/10/2018", isRead: nil)
        let second = NotificationItem(title: "Congratulations!", message: "You’ve chosen the 1008.00 SXE Bonus Bounty. Refer friends to unlock it sooner!", date: "05/10/2018", isRead: nil)
        let third = NotificationItem(title: "Welcome to your Wallet.", message: "Complete your KYC verification to fully unlock it and get ready for your Bonus Bounty!", date: "05/10/2018", isRead: nil)
        
        return [first, second, third]
    }()
    
    lazy var notificationContainer : UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.75
        return view
    }()
    
    lazy var notificationViewCollection : UICollectionView = {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isPagingEnabled = true
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = .clear
        cv.showsHorizontalScrollIndicator = false
        
        return cv
    }()
    
    var isMenuOpened : Bool = false
    var isNotifcationBarOpened : Bool = false
    let menuViewCellId : String = "menuViewCellId"
    let notificationCellId: String = "notificationCellId"
    
    let viewCellId : String = "ViewCellId"
    
    // MARK: - Methods
    
    private func registerCells() {
        menuViewCollection.register(MenuViewCell.self, forCellWithReuseIdentifier: menuViewCellId)
        notificationViewCollection.register(NotificationViewCell.self, forCellWithReuseIdentifier: notificationCellId)
        containerView.register(MyReferralsViewCell.self, forCellWithReuseIdentifier: viewCellId)
    }
    
    private func setupBaseViews() {
        
        view.addSubview(backgroundImage)
        view.addSubview(coverView)
        view.addSubview(menuContainer)
        view.addSubview(menuViewCollection)
        view.addSubview(notificationContainer)
        view.addSubview(notificationViewCollection)
        view.addSubview(containerView)
        
        registerCells()
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = coverView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = containerView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = menuContainer.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = menuViewCollection.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: nil, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 60, heightConstant: 0)
        
        _ = notificationContainer.anchor(view.topAnchor, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 221, heightConstant: 0)
        
        notificationContainer.isHidden = true
        
        _ = notificationViewCollection.anchor(view.topAnchor, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 221, heightConstant: 0)
        
        menuViewCollection.layer.borderWidth = 0.5
        menuViewCollection.layer.borderColor = UIColor.darkGray.cgColor
        
        notificationViewCollection.layer.borderWidth = 0.5
        notificationViewCollection.layer.borderColor = UIColor.darkGray.cgColor
        
        menuViewCollection.isHidden = true
        notificationViewCollection.isHidden = true
        
    }

    // MARK: - Navigation Bar
    
    private func setupNavBar() {
        
        navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationItem.title = "My Referrals"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.futura(fontSize: 18, type: .Medium)]
        
        let burgerMenu = UIBarButtonItem(image: UIImage(named: "Menu"), style: .plain, target: self, action: #selector(onBurgerMenuClick))
        burgerMenu.tintColor = .white
        self.navigationItem.leftBarButtonItem = burgerMenu
        
        let ntfMenu = UIBarButtonItem(image: UIImage(named: "Notification"), style: .plain, target: self, action: #selector(onNtfMenuClick))
        ntfMenu.tintColor = .white
        self.navigationItem.rightBarButtonItem = ntfMenu
    }
    
    @objc private func onBurgerMenuClick() {
        if isNotifcationBarOpened {
            ecNtfPanel()
        }
        ecMenu()
    }
    
    @objc private func onNtfMenuClick() {
        if isMenuOpened {
            ecMenu()
        }
        ecNtfPanel()
    }
    
    private func ecMenu() {
        let width: CGFloat = (isMenuOpened) ? 0.0 : 60.0
        coverView.frame.origin.x = width
        containerView.frame.origin.x = width
        
        //menuContainer.frame.size.width = width
        menuViewCollection.isHidden = isMenuOpened
        isMenuOpened = !isMenuOpened
    }
    
    private func ecNtfPanel() {
        let width : CGFloat = (isNotifcationBarOpened) ? 0.0 : 221
        coverView.frame.origin.x = -(width)
        containerView.frame.origin.x = -(width)
        notificationContainer.isHidden = isNotifcationBarOpened
        notificationViewCollection.isHidden = isNotifcationBarOpened
        isNotifcationBarOpened = !isNotifcationBarOpened
    }
    
    // MARK: - CollectionView
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == menuViewCollection {
            return 5
        } else if collectionView == notificationViewCollection {
            return notificationItems.count
        } else {
            return 2
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == menuViewCollection {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: menuViewCellId, for: indexPath) as! MenuViewCell
            cell.menu = Menu(rawValue: indexPath.row+1)
            cell.menuButton.addTarget(self, action: #selector(onMenuButtonClick), for: .touchUpInside)
            if (indexPath.row+1) == Menu.Referrals.rawValue {
                cell.menuButton.imageView?.tintColor = UIColor.mainColor()
                cell.itemName.textColor = UIColor.mainColor()
            }
            return cell
            
        } else if collectionView == notificationViewCollection {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: notificationCellId, for: indexPath) as! NotificationViewCell
            cell.ntfItem = notificationItems[indexPath.row]
            return cell
            
        } else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: viewCellId, for: indexPath) as! MyReferralsViewCell
            cell.setup(seqNo: SeqNo(rawValue: indexPath.row)!)
            cell.startReferringButton.addTarget(self, action: #selector(onSummaryReferral), for: .touchUpInside)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == menuViewCollection {
            return CGSize(width: 60, height: 60)
        } else if collectionView == notificationViewCollection {
            return CGSize(width: 221, height: 72)
        } else {
            if indexPath.row == 0 {
                return CGSize(width: view.frame.width, height: view.frame.height)
            } else {
                return CGSize(width: view.frame.width, height: view.frame.height/2)
            }
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == menuViewCollection {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if indexPath.row == 0 {
                
                let vc = storyboard.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
                let navigationController = UINavigationController(rootViewController: vc)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = navigationController
                
            } else if indexPath.row == 1 {
                
                let vc = storyboard.instantiateViewController(withIdentifier: "BusinessVaultIntroViewController") as! BusinessVaultIntroViewController
                let navigationController = UINavigationController(rootViewController: vc)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = navigationController
                
            } else if indexPath.row == 2 {
                
                let vc = storyboard.instantiateViewController(withIdentifier: "ShaktiMinerIntroViewController") as! ShaktiMinerIntroViewController
                let navigationController = UINavigationController(rootViewController: vc)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = navigationController
                
            } else if indexPath.row == 3 {
                
                //Current
                
            } else if indexPath.row == 4 {
            
                let vc = storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
                let navigationController = UINavigationController(rootViewController: vc)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = navigationController
            }
        }
    }
    
    // MARK: - Events
    
    @objc private func onMenuButtonClick() {
        print("Menu Item Clicked")
    }

    @objc private func onSummaryReferral() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MyReferralsSummaryViewController") as! MyReferralsSummaryViewController
        let navigationController = UINavigationController(rootViewController: vc)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navigationController
    }
    
    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setupBaseViews()
    }
    
    override func viewDidLayoutSubviews() {
        if containerView.visibleCells.count > 0 {
            (containerView.visibleCells[0] as! MyReferralsViewCell).finilizeMe()
        }
    }
}
