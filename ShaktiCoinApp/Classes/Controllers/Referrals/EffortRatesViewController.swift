//
//  EffortRatesViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/13/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class EffortRatesViewController: UIViewController,UICollectionViewDelegate,
UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource {
    
    // MARK: - Declarations
    
    lazy var backgroundImage: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "ReferralsImage")
        return iv
    }()
    
    lazy var coverView: UIView = {
        let cv = UIView()
        cv.contentMode = .scaleAspectFill
        cv.alpha = 0.80
        cv.backgroundColor = .black
        return cv
    }()
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    lazy var notificationContainer : UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.75
        return view
    }()
    
    lazy var notificationViewCollection : UICollectionView = {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isPagingEnabled = true
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = .clear
        cv.showsHorizontalScrollIndicator = false
        
        return cv
    }()
    
    let notificationItems: [NotificationItem] = {
        
        let first = NotificationItem(title: "A friend has signed up.", message: "Alan Reynolds has registered you as a referral upon signing up.", date: "06/10/2018", isRead: nil)
        let second = NotificationItem(title: "Congratulations!", message: "You’ve chosen the 1008.00 SXE Bonus Bounty. Refer friends to unlock it sooner!", date: "05/10/2018", isRead: nil)
        let third = NotificationItem(title: "Welcome to your Wallet.", message: "Complete your KYC verification to fully unlock it and get ready for your Bonus Bounty!", date: "05/10/2018", isRead: nil)
        
        return [first, second, third]
    }()
    
    var isNotifcationBarOpened : Bool = false
    let notificationCellId: String = "notificationCellId"
    let rateTableCellId : String = "rateCellId"
    
    lazy var topTitleLabel : UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red:0.85, green:0.85, blue:0.85, alpha:1)
        label.font = UIFont.lato(fontSize: 12)
        label.text = "Tap the progress buttons to see the social specific leads."
        return label
    }()
    
    lazy var smGroupView : _SMRatesGroupView = {
        let view = _SMRatesGroupView()
        return view
    }()
    
    lazy var tableView : UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = 32
        tableView.separatorInset = .zero
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = false
        tableView.backgroundColor = .clear
        return tableView
    }()
    
    let socialRates : [EffortRate] = {
        let facebook = EffortRate(index: 0, icon: "Facebook", converted: 64, progressing: 20, influenced: 16)
        let instagram = EffortRate(index: 1, icon: "Instagram", converted: 34, progressing: 29, influenced: 37)
        let google = EffortRate(index: 2, icon: "GooglePlus", converted: 64, progressing: 20, influenced: 16)
        let linkedIn = EffortRate(index: 3, icon: "LinkedIn", converted: 45, progressing: 15, influenced: 40)
        let twitter = EffortRate(index: 4,icon: "Twitter", converted: 45, progressing: 15, influenced: 40)
        let pinterest = EffortRate(index: 5,icon: "Instagram", converted: 45, progressing: 15, influenced: 40)
        let skype = EffortRate(index: 6,icon: "Skype", converted: 45, progressing: 15, influenced: 40)
        let vk = EffortRate(index: 7,icon: "WK", converted: 20, progressing: 25, influenced: 55)
        let weChat = EffortRate(index: 8,icon: "WeChat", converted: 45, progressing: 15, influenced: 40)
        let tumblr = EffortRate(index: 9,icon: "Tumblr", converted: 45, progressing: 15, influenced: 40)
        let email = EffortRate(index: 10,icon: "Email", converted: 45, progressing: 15, influenced: 40)
        let other = EffortRate(index: 11,icon: "Other", converted: 45, progressing: 15, influenced: 40)
        return [facebook,instagram,google,linkedIn,twitter,pinterest,skype,vk,weChat,tumblr,email,other]
    }()
    
    // MARK: - Methods
    private func registerCells() {
        notificationViewCollection.register(NotificationViewCell.self, forCellWithReuseIdentifier: notificationCellId)
        tableView.register(EffortRateTableViewCell.self, forCellReuseIdentifier: rateTableCellId)
    }
    
    private func setupBaseViews() {
        
        view.addSubview(backgroundImage)
        view.addSubview(coverView)
        view.addSubview(notificationContainer)
        view.addSubview(notificationViewCollection)
        view.addSubview(containerView)
        
        registerCells()
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = coverView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = containerView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = notificationContainer.anchor(view.topAnchor, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 221, heightConstant: 0)
        
        notificationContainer.isHidden = true
        
        _ = notificationViewCollection.anchor(view.topAnchor, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 221, heightConstant: 0)
        
        notificationViewCollection.layer.borderWidth = 0.5
        notificationViewCollection.layer.borderColor = UIColor.darkGray.cgColor
        
        notificationViewCollection.isHidden = true
        
        setupViews()
    }
    
    func setupViews() {
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        containerView.addSubview(topTitleLabel)
        
        _ = topTitleLabel.anchor(view.topAnchor, left: nil, bottom: nil, right: nil, topConstant: topBarHeight + 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = topTitleLabel.centralizeX(view.centerXAnchor)
        
        containerView.addSubview(smGroupView)
        
        _ = smGroupView.anchor(topTitleLabel.bottomAnchor, left: containerView.leftAnchor, bottom: nil, right: containerView.rightAnchor, topConstant: 10, leftConstant: 55, bottomConstant: 0, rightConstant: 55, widthConstant: 0, heightConstant: 80)
        
        smGroupView.progressingCircle.addTarget(self, action: #selector(onContactOpen), for: .touchUpInside)
        smGroupView.influencedCircle.addTarget(self, action: #selector(onFinalOpen), for: .touchUpInside)
        
        containerView.addSubview(tableView)
        _ = tableView.anchor(smGroupView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: (UIScreen.main.bounds.width*90)/100, heightConstant: 450)
        _ = tableView.centralizeX(containerView.centerXAnchor)
    }
    
    // MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return socialRates.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: rateTableCellId, for: indexPath) as! EffortRateTableViewCell
        cell.setup(rate: socialRates[indexPath.row])
        return cell
    }

    
    // MARK: - CollectionView
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notificationItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: notificationCellId, for: indexPath) as! NotificationViewCell
        cell.ntfItem = notificationItems[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 221, height: 72)
    }
    
    // MARK: - Events
    
    @objc private func onMenuButtonClick() {
        print("Menu Item Clicked")
    }

    @objc private func onContactOpen() {
        let vc = ERContactsViewController()
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @objc private func onFinalOpen() {
        let vc = MyReferralsFinalViewController()
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    // MARK: - ViewContoller
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setupBaseViews()
    }
    

    // MARK: - Navigation Bar
    
    private func setupNavBar() {
        
        navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationItem.title = "Effort Rates"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.futura(fontSize: 18, type: .Medium)]
        
        let backButton = UIBarButtonItem(image: UIImage(named: "Back"), style: .plain, target: self, action: #selector(onBack))
        backButton.tintColor = .white
        self.navigationItem.leftBarButtonItem = backButton

        let ntfMenu = UIBarButtonItem(image: UIImage(named: "Notification"), style: .plain, target: self, action: #selector(onNtfMenuClick))
        ntfMenu.tintColor = .white
        self.navigationItem.rightBarButtonItem = ntfMenu
    }
    
    @objc private func onBack() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func onNtfMenuClick() {
        ecNtfPanel()
    }
    
    private func ecNtfPanel() {
        let width : CGFloat = (isNotifcationBarOpened) ? 0.0 : 221
        coverView.frame.origin.x = -(width)
        containerView.frame.origin.x = -(width)
        notificationContainer.isHidden = isNotifcationBarOpened
        notificationViewCollection.isHidden = isNotifcationBarOpened
        isNotifcationBarOpened = !isNotifcationBarOpened
    }
}
