//
//  ERContactsViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/13/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit
 


class ERContactsViewController: UIViewController,UICollectionViewDelegate,
UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Declarations
    
    lazy var backgroundImage: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "ReferralsImage")
        //iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    lazy var coverView: UIView = {
        let cv = UIView()
        cv.contentMode = .scaleAspectFill
        cv.alpha = 0.80
        cv.backgroundColor = .black
        return cv
    }()
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    lazy var notificationContainer : UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.75
        return view
    }()
    
    lazy var notificationViewCollection : UICollectionView = {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isPagingEnabled = true
        cv.delegate = self
        cv.dataSource = self
        cv.backgroundColor = .clear
        cv.showsHorizontalScrollIndicator = false
        
        return cv
    }()
    
    let notificationItems: [NotificationItem] = {
        
        let first = NotificationItem(title: "A friend has signed up.", message: "Alan Reynolds has registered you as a referral upon signing up.", date: "06/10/2018", isRead: nil)
        let second = NotificationItem(title: "Congratulations!", message: "You’ve chosen the 1008.00 SXE Bonus Bounty. Refer friends to unlock it sooner!", date: "05/10/2018", isRead: nil)
        let third = NotificationItem(title: "Welcome to your Wallet.", message: "Complete your KYC verification to fully unlock it and get ready for your Bonus Bounty!", date: "05/10/2018", isRead: nil)
        
        return [first, second, third]
    }()
    
    var isMenuOpened : Bool = false
    var isNotifcationBarOpened : Bool = false
    let menuViewCellId : String = "MenuCell"
    let notificationCellId: String = "notificationCellId"
    let contactViewCellId: String = "contactTableViewCellId"
    
    lazy var progressingView : UIView = {
        let view = UIView()
        view.backgroundColor = .gray
        return view
    }()
    
    lazy var labelProgressing : UILabel = {
        let label = UILabel()
        label.text = "30 Progressing"
        label.font = UIFont.lato(fontSize: 14)
        label.textColor = .white
        return label
    }()
    
    lazy var nudgeFriendsButton : UIButton = {
        let button = UIButton()
        button.setTitle("Nudge Friends", for: .normal)
        button.backgroundColor = .clear
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.futura(fontSize: 14)
        return button
    }()
    
    lazy var tableView : UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = 32
        tableView.separatorInset = .zero
        //tableView.separatorStyle = .none
        tableView.isScrollEnabled = false
        tableView.backgroundColor = .clear
        return tableView
    }()
    
    
    
    // MARK: - Methods

    private func registerCells() {
        notificationViewCollection.register(NotificationViewCell.self, forCellWithReuseIdentifier: notificationCellId)
        tableView.register(ContactTableViewCell.self, forCellReuseIdentifier: contactViewCellId)
        tableView.register(ContactSectionTableViewCell.self, forCellReuseIdentifier: "Section")
    }

    private func setupBaseViews() {
        
        view.addSubview(backgroundImage)
        view.addSubview(coverView)
        view.addSubview(notificationContainer)
        view.addSubview(notificationViewCollection)
        view.addSubview(containerView)
        
        registerCells()
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = coverView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = containerView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = notificationContainer.anchor(view.topAnchor, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 221, heightConstant: 0)
        
        notificationContainer.isHidden = true
        
        _ = notificationViewCollection.anchor(view.topAnchor, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 221, heightConstant: 0)
        
        notificationViewCollection.layer.borderWidth = 0.5
        notificationViewCollection.layer.borderColor = UIColor.darkGray.cgColor

        notificationViewCollection.isHidden = true
        
        setupViews()
    }
    
    func setupViews() {
        
        containerView.addSubview(progressingView)
        _ = progressingView.anchor(containerView.topAnchor, left: containerView.leftAnchor, bottom: nil, right: nil, topConstant: 20, leftConstant: 25, bottomConstant: 0, rightConstant: 0, widthConstant: 28, heightConstant: 28)
        
        containerView.addSubview(labelProgressing)
        _ = labelProgressing.anchor(progressingView.topAnchor, left: progressingView.rightAnchor, bottom: nil, right: nil, topConstant: 5, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        containerView.addSubview(nudgeFriendsButton)
        _ = nudgeFriendsButton.anchor(containerView.topAnchor, left: nil, bottom: nil, right: containerView.rightAnchor, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 15, widthConstant: 126, heightConstant: 36)
        
        containerView.addSubview(tableView)
        
        _ = tableView.anchor(progressingView.bottomAnchor, left: containerView.leftAnchor, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, topConstant: 40, leftConstant: 15, bottomConstant: 15, rightConstant: 15, widthConstant: 0, heightConstant: 0)
        
    }

    // MARK: - TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let  sectionCell = tableView.dequeueReusableCell(withIdentifier: "Section") as! ContactSectionTableViewCell
        sectionCell.setup(index: section)
        return sectionCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 2
        case 2:
            return 2
        default:
            return 0
        }
    }
    
    var sectionAList : [Contact] = {
        let andrea = Contact(profilePicture : "pp1", fullName: "Andrea Collins", email: "creynolds@josefina.biz", mobile: "+1 (809)909-9876")
        return [andrea]
    }()
    
    var sectionBList : [Contact] = {
        let ann = Contact(profilePicture : "pp2", fullName: "Ann Smith", email: "ann@josefina.biz", mobile: "+1 (809)406-8965")
        let jesica = Contact(profilePicture : "pp1", fullName: "Jesica Doe", email: "jesica@josefina.biz", mobile: "+1 (904)365-8965")
        return [ann, jesica]
    }()
    
    var sectionCList : [Contact] = {
        let jena = Contact(profilePicture : "pp3", fullName: "Jena Smith", email: "dena@josefina.biz", mobile: "+1 (809)406-1198")
        let deni = Contact(profilePicture : "pp2", fullName: "Deni Doe", email: "deni@josefina.biz", mobile: "+1 (904)365-5689")
        return [jena, deni]
    }()
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: contactViewCellId, for: indexPath as IndexPath) as! ContactTableViewCell
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        
        switch (indexPath.section) {
        case 0:
            cell.profilePic.image = UIImage(named: sectionAList[indexPath.row].profilePicture)
            cell.fullName.text = sectionAList[indexPath.row].fullName
            cell.email.text = sectionAList[indexPath.row].email
            cell.mobile.text = sectionAList[indexPath.row].mobile
        case 1:
            
            cell.profilePic.image = UIImage(named: sectionBList[indexPath.row].profilePicture)
            cell.fullName.text = sectionBList[indexPath.row].fullName
            cell.email.text = sectionBList[indexPath.row].email
            cell.mobile.text = sectionBList[indexPath.row].mobile
            
        case 2:
            
            cell.profilePic.image = UIImage(named: sectionCList[indexPath.row].profilePicture)
            cell.fullName.text = sectionCList[indexPath.row].fullName
            cell.email.text = sectionCList[indexPath.row].email
            cell.mobile.text = sectionCList[indexPath.row].mobile
            
        default:
            cell.textLabel?.text = "Other"
        }
        
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    // MARK: - CollectionView
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notificationItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: notificationCellId, for: indexPath) as! NotificationViewCell
        cell.ntfItem = notificationItems[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 221, height: 72)
    }
    
    // MARK: - Events
    
    @objc private func onMenuButtonClick() {
        print("Menu Item Clicked")
    }
    
    // MARK: - ViewContoller

    override func viewDidLoad() {
        super.viewDidLoad()

        setupBaseViews()
        setupNavBar()
    }
    
    override func viewDidLayoutSubviews() {
        progressingView.layer.cornerRadius = 28/2
        progressingView.clipsToBounds = true
        
        nudgeFriendsButton.layer.borderColor = UIColor.mainColor().cgColor
        nudgeFriendsButton.layer.borderWidth = 0.5
        nudgeFriendsButton.layer.cornerRadius = 17.5
    }
    

    // MARK: - Navigation Bar
    
    private func setupNavBar() {
        
        navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationItem.title = "Effort Rates"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.futura(fontSize: 18, type: .Medium)]
        
        let backButton = UIBarButtonItem(image: UIImage(named: "Back"), style: .plain, target: self, action: #selector(onBack))
        backButton.tintColor = .white
        self.navigationItem.leftBarButtonItem = backButton

        let ntfMenu = UIBarButtonItem(image: UIImage(named: "Notification"), style: .plain, target: self, action: #selector(onNtfMenuClick))
        ntfMenu.tintColor = .white
        self.navigationItem.rightBarButtonItem = ntfMenu
    }
    
    @objc private func onBack() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func onNtfMenuClick() {
        ecNtfPanel()
    }
    
    private func ecNtfPanel() {
        let width : CGFloat = (isNotifcationBarOpened) ? 0.0 : 221
        coverView.frame.origin.x = -(width)
        containerView.frame.origin.x = -(width)
        notificationContainer.isHidden = isNotifcationBarOpened
        notificationViewCollection.isHidden = isNotifcationBarOpened
        isNotifcationBarOpened = !isNotifcationBarOpened
    }
}
