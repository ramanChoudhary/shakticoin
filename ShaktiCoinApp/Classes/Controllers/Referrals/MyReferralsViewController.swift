//
//  MyReferralsViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/7/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit


class MyReferralsViewController: BaseScrollViewController {
    // MARK: - Declarations

    lazy var contentView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isPagingEnabled = true
//        cv.delegate = self
//        cv.dataSource = self
        cv.backgroundColor = UIColor().hexStringToUIColor("#0C0C0C")
        cv.showsVerticalScrollIndicator = true
        return cv
    }()

    
    let viewCellId : String = "ViewCellId"
    
    // MARK: - Methods
    
    private func setupBaseViews() {
        view.addSubview(containerView)
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        _ = containerView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }

    // MARK: - Navigation Bar
    
    private func setupNavBar() {
        
        navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationItem.title = "My Referrals"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.futura(fontSize: 18, type: .Medium)]
    }
    
    @objc private func onSummaryReferral() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MyReferralsSummaryViewController") as! MyReferralsSummaryViewController
        let navigationController = UINavigationController(rootViewController: vc)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navigationController
    }
    
    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuView.currentMenuItem = .referrals
        setupNavBar()
        setupBaseViews()
    }
}
