//
//  PNPickerViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 1/20/19.
//  Copyright © 2019 Garnik Giloyan. All rights reserved.
//

import UIKit

class PNPickerViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    // MARK: - Declarations
    
    private var phoneCodePicker: UIPickerView!
    var signUpDelegate: SignUpDelegates?
    
    // MARK: - ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBarButtons()
        initData()
        setupPicker()
    }
    
    var listOfPhoneCodes : [PhoneCode]?
    
    // MARK: - Methods
    
    private func initData() {
        listOfPhoneCodes = Util.loadPhoneCodes()
        /*listOfPhoneCodes?.forEach({ (value) in
            print(value.dial_code)
        })*/
    }
    
    private func setupPicker() {
        
        phoneCodePicker = UIPickerView()
        phoneCodePicker.dataSource = self
        phoneCodePicker.delegate = self
        view.addSubview(phoneCodePicker)
        
        _ = phoneCodePicker.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    // MARK: - PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return listOfPhoneCodes?.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let currentCodeInfo = listOfPhoneCodes![row]
        return currentCodeInfo.dial_code + " " + currentCodeInfo.name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
    // MARK: - Navigation
    
    private func setupNavBarButtons() {
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.mainColor()]
        
        self.navigationItem.title = "Phone Code"
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(navBarCancelTapped))
        cancelButton.tintColor = UIColor.mainColor()
        self.navigationItem.leftBarButtonItem = cancelButton
        
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(navBarDoneTapped))
        doneButton.tintColor = UIColor.mainColor()
        self.navigationItem.rightBarButtonItem = doneButton
    }
    
    @objc private func navBarCancelTapped() {
        dismiss(animated: false, completion: nil)
    }

    @objc private func navBarDoneTapped() {
        let selectedCountry: String
        selectedCountry = listOfPhoneCodes?[phoneCodePicker.selectedRow(inComponent: 0)].dial_code ?? ""
        signUpDelegate?.countryCodePickedUp(value: selectedCountry)
        dismiss(animated: true, completion: nil)
    }
}
