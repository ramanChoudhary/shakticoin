//
//  FamilyTreeViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 2/25/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import UIKit

class FamilyTreeViewController: SingleBaseViewController, UITableViewDataSource, UITableViewDelegate {
   

    // MARK: - Declarations

    lazy var treeImageView : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "familyTree")
        return iv
    }()
    
    lazy var familyMembers : SCLineTitle = {
        let lineTitle = SCLineTitle()
        lineTitle.Title = "Family Members"
        return lineTitle
    }()

    lazy var tableView : UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = .clear
        tableView.rowHeight = 52
        tableView.separatorInset = .zero
        tableView.separatorStyle = .none
        return tableView
    }()
    
    let contactViewCellId: String = "contactViewCellId"
    var listOfContacts : [Contact] = {
        let andrea = Contact(profilePicture : "pp1", fullName: "Andrea Collins", email: "creynolds@josefina.biz", mobile: "+1 (809)909-9876")
        let john = Contact(profilePicture : "pp2", fullName: "John Smith", email: "john@josefina.biz", mobile: "+1 (809)808-3424")
        let bruce = Contact(profilePicture : "pp3", fullName: "Bruce Lee", email: "bruce@josefina.biz", mobile: "+1 (809)705-5446")
        return [andrea, john, bruce]
    }()

    // MARK: - Methods
    
    private func initViews() {
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        containerView.addSubview(treeImageView)
        containerView.addSubview(familyMembers)
        containerView.addSubview(tableView)
        
        _ = treeImageView.anchor(containerView.topAnchor, left: containerView.leftAnchor, bottom: nil, right: containerView.rightAnchor, topConstant: topBarHeight + 20, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 280)
        
        _ = familyMembers.anchor(treeImageView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = familyMembers.centralizeX(containerView.centerXAnchor)

        _ = tableView.anchor(familyMembers.bottomAnchor, left: containerView.leftAnchor, bottom: nil, right: containerView.rightAnchor, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 170)
        
    }
    
    private func registerCells() {
        tableView.register(FtreeTableViewCell.self, forCellReuseIdentifier: contactViewCellId)
    }
    
    // MARK: - UITableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        listOfContacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: contactViewCellId, for: indexPath as IndexPath) as! FtreeTableViewCell
        cell.backgroundColor = .clear
        cell.selectionStyle = .none

        cell.profilePic.image = UIImage(named: listOfContacts[indexPath.row].profilePicture)
        cell.fullName.text = listOfContacts[indexPath.row].fullName
        cell.email.text = listOfContacts[indexPath.row].email
        cell.mobile.text = listOfContacts[indexPath.row].mobile
        
        return cell
    }
    
    // MARK: - Events
    
    // MARK: - ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        initNavBar()
        initViews()
    }
    
    override func viewDidLayoutSubviews() {
        familyMembers.updateWithSpecificWidth(width: UIScreen.main.bounds.width, lineSize: 24)
    }
    
    // MARL: - NavBar
    
    private func initNavBar() {
        self.navigationItem.title = "Family Tree"
    }
    
}
