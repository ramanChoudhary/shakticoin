//
//  SViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 1/7/19.
//  Copyright © 2019 Garnik Giloyan. All rights reserved.
//

import UIKit



class SViewController: UIViewController {
    
    // Student Entity
    // Courses, English, Mathematics, Bilogy, Pysics, IT
    // Name of Student
    // Student Account
    // When student has courses in profile then student status is active
    // Status: Active, Revoke
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .blue
        
        //isZipCodeValid(country: "UK")
        
        /*jsonToObj()
        if isValidZipCode(country: "US", zipCode: "L-1317  ") {
            print("VALID")
        } else {
            print("NOT VALID")
        }*/
    }
    
    func loadJson(filename fileName: String) -> [CountryISO]? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode([CountryISO].self, from: data)
                return jsonData
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }
    
    func isZipCodeValid11(country: String) {
        
        let listOfCountries = loadJson(filename: "postal-codes")
        let selectedCountry = listOfCountries?.filter{$0.ISO == country}.first
        
        print(selectedCountry?.Regex)
        
    }
    
    
    func jsonToObj() {
        
        /*let countryList = loadJson(filename: "postal-codes")
        countryList?.forEach({ (value) in
            print(value.Regex)
        })*/

        /*do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                print(jsonResult)
        
         } catch {
                // handle error
                print("error")
         }*/
       
        
    }
    
    //NNNNN - ^((?!(0))[0-9]{5})$
    
    // UK - ([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9][A-Za-z]?))))\s?[0-9][A-Za-z]{2})
    
    // Canada - ^(?!.*[DFIOQU])[A-VXY][0-9][A-Z] ?[0-9][A-Z][0-9]$
    
    
    // Portugal - ^\\d{4}-\\d{3}$ // Needs to be tested
    
    
    // Luxembourg - (L\s*(-|—|–))\s*?[\d]{4}
    
    /*func isValidZipCode(country: String, zipCode: String) -> Bool {
        let zipCodeRegEx = "(L\\s*(-|—|–))\\s*?[\\d]{4}"
        
        let zipCodeTest = NSPredicate(format:"SELF MATCHES[c] %@", zipCodeRegEx)
        return zipCodeTest.evaluate(with: zipCode)
    }*/

 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
