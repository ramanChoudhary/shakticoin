//
//  PoEServiceProviderViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 3/7/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import UIKit

class PoEServiceProviderViewController: BaseScrollViewController {

    //MARK: - Declarations
    
    lazy var whereYouWorkView : _PoESPWhereYouWork = {
        let view = _PoESPWhereYouWork()
        view.nextButton.addTarget(self, action: #selector(nextButtonClicked(_sender:)), for: .touchUpInside)
        view.cancelButton.addTarget(self, action: #selector(onCancel), for: .touchUpInside)
        return view
    }()

    //view.nextButton.addTarget(self, action: #selector(nextButton(_sender:)), for: .touchUpInside)
    
    lazy var serviceProvideDetail : _PoEServiceProviderDetail = {
        let view = _PoEServiceProviderDetail()
        view.isHidden = true
        //view.nextButton.addTarget(self, action: #selector(nextButtonClicked(_sender:)), for: .touchUpInside)
        return view
    }()
    
    //MARK: - Methods

    private func initViews() {
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)

        containerView.addSubview(whereYouWorkView)
         _ = whereYouWorkView.anchor(containerView.topAnchor, left: nil, bottom: containerView.bottomAnchor, right: nil, topConstant: topBarHeight+15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 340, heightConstant: 60)
        _ = whereYouWorkView.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(serviceProvideDetail)
         _ = serviceProvideDetail.anchor(containerView.topAnchor, left: nil, bottom: containerView.bottomAnchor, right: nil, topConstant: topBarHeight+15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 340, heightConstant: 60)
        _ = serviceProvideDetail.centralizeX(containerView.centerXAnchor)
    }
    
    //MARK: - Events
    
    @objc func nextButtonClicked(_sender: SCButton) {
        
        if _sender.tag == 1 {
            
            whereYouWorkView.isHidden = true
            serviceProvideDetail.isHidden = false
            
        } else if _sender.tag == 2 {
            
        }
    }

    @objc func onCancel() {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initNavBar()
        initViews()
    }
    
    override func viewDidLayoutSubviews() {
        whereYouWorkView.finalizeMe()
        serviceProvideDetail.finalizeMe()
    }
    

    //MARK: - NavBar
    
    private func initNavBar() {
        self.navigationItem.title = "On-board a Service Provider"
    }

}
