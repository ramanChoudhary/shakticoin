//
//  PoEParticipantsViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 3/6/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import UIKit

class PoEParticipantsViewController: SingleBaseViewController {

    // MARK: - Declarations
    
    lazy var childrenTitle : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()
    
    lazy var childName1Panel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var childName1: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 70
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Child Name #1", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()

    lazy var childName2Panel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var childName2: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 70
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Child Name #2", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    //School
    
    lazy var schoolTitle : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()

    lazy var school1Panel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var school1: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 70
        textField.attributedPlaceholder = Util.NSMAttrString(text: "School Name #1", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var school2Panel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var school2: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 70
        textField.attributedPlaceholder = Util.NSMAttrString(text: "School Name #2", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    //Service Provider
    
    lazy var providerTitle : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()

    lazy var provider1Panel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var provider1: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 70
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Service Providers Name #1", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    lazy var provider2Panel: SCTextFieldPanel = {
        let panel = SCTextFieldPanel()
        panel.backgroundColor = .clear
        return panel
    }()
    
    lazy var provider2: SCXTextField = {
        let textField = SCXTextField()
        textField.floatingLabelValue = 70
        textField.attributedPlaceholder = Util.NSMAttrString(text: "Service Providers Name #2", font: UIFont.lato(fontSize: 14))
        textField.setRegularity()
        return textField
    }()
    
    // Verifier
    
    lazy var verifierTitle : SCLineTitle = {
       let lineTitle = SCLineTitle()
       return lineTitle
   }()

   lazy var verifier1Panel: SCTextFieldPanel = {
       let panel = SCTextFieldPanel()
       panel.backgroundColor = .clear
       return panel
   }()
   
   lazy var verifier1: SCXTextField = {
       let textField = SCXTextField()
       textField.floatingLabelValue = 70
       textField.attributedPlaceholder = Util.NSMAttrString(text: "Independent Verifier #1", font: UIFont.lato(fontSize: 14))
       textField.setRegularity()
       return textField
   }()
   
   lazy var verifier2Panel: SCTextFieldPanel = {
       let panel = SCTextFieldPanel()
       panel.backgroundColor = .clear
       return panel
   }()
   
   lazy var verifier2: SCXTextField = {
       let textField = SCXTextField()
       textField.floatingLabelValue = 70
       textField.attributedPlaceholder = Util.NSMAttrString(text: "Independent Verifier #2", font: UIFont.lato(fontSize: 14))
       textField.setRegularity()
       return textField
   }()

    // MARK: - Methods
    
    private func initViews() {
        
        containerView.addSubview(childrenTitle)
        childrenTitle.Title = "On-boarded School Children"
        schoolTitle.Title = "On-boarded Schools"
        providerTitle.Title = "On-boarded Service Providers"
        verifierTitle.Title = "On-boarded Independent Verifier"
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        // Children
        
        _ = childrenTitle.anchor(containerView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: topBarHeight+15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = childrenTitle .centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(childName1Panel)
        
        _ = childName1Panel.anchor(childrenTitle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = childName1Panel.centralizeX(containerView.centerXAnchor)
        
        childName1Panel.ContainerView.addSubview(childName1)
        childName1.parentPanel = childName1Panel
        Util.fullyAnchor(_parnetControl: childName1Panel.ContainerView, _childControl: childName1)

        containerView.addSubview(childName2Panel)
        
        _ = childName2Panel.anchor(childName1Panel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = childName2Panel.centralizeX(containerView.centerXAnchor)
        
        childName2Panel.ContainerView.addSubview(childName2)
        childName2.parentPanel = childName2Panel
        Util.fullyAnchor(_parnetControl: childName2Panel.ContainerView, _childControl: childName2)
        
        //School
        
        containerView.addSubview(schoolTitle)
        
        _ = schoolTitle.anchor(childName2Panel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = schoolTitle.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(school1Panel)
        
        _ = school1Panel.anchor(schoolTitle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = school1Panel.centralizeX(containerView.centerXAnchor)
        
        school1Panel.ContainerView.addSubview(school1)
        school1.parentPanel = school1Panel
        Util.fullyAnchor(_parnetControl: school1Panel.ContainerView, _childControl: school1)
        
        containerView.addSubview(school2Panel)
        
        _ = school2Panel.anchor(school1Panel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = school2Panel.centralizeX(containerView.centerXAnchor)
        
        school2Panel.ContainerView.addSubview(school2)
        school2.parentPanel = school2Panel
        Util.fullyAnchor(_parnetControl: school2Panel.ContainerView, _childControl: school2)
        
        //Service Provider
        
        containerView.addSubview(providerTitle)
        
        _ = providerTitle.anchor(school2Panel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = providerTitle.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(provider1Panel)

        _ = provider1Panel.anchor(providerTitle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = provider1Panel.centralizeX(containerView.centerXAnchor)
        
        provider1Panel.ContainerView.addSubview(provider1)
        provider1.parentPanel = provider1Panel
        Util.fullyAnchor(_parnetControl: provider1Panel.ContainerView, _childControl: provider1)
        
        containerView.addSubview(provider2Panel)
        
        _ = provider2Panel.anchor(provider1Panel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = provider2Panel.centralizeX(containerView.centerXAnchor)
        
        provider2Panel.ContainerView.addSubview(provider2)
        provider2.parentPanel = provider2Panel
        Util.fullyAnchor(_parnetControl: provider2Panel.ContainerView, _childControl: provider2)

        // Verifier
        
        containerView.addSubview(verifierTitle)
        
        _ = verifierTitle.anchor(provider2Panel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = verifierTitle.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(verifier1Panel)

        _ = verifier1Panel.anchor(verifierTitle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = verifier1Panel.centralizeX(containerView.centerXAnchor)
        
        verifier1Panel.ContainerView.addSubview(verifier1)
        verifier1.parentPanel = verifier1Panel
        Util.fullyAnchor(_parnetControl: verifier1Panel.ContainerView, _childControl: verifier1)
        
        containerView.addSubview(verifier2Panel)
        
        _ = verifier2Panel.anchor(verifier1Panel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = verifier2Panel.centralizeX(containerView.centerXAnchor)
        
        verifier2Panel.ContainerView.addSubview(verifier2)
        verifier2.parentPanel = verifier2Panel
        Util.fullyAnchor(_parnetControl: verifier2Panel.ContainerView, _childControl: verifier2)

        
    }
    
    
    // MARK: - ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        initNavBar()
        initViews()
    }
    
    override func viewDidLayoutSubviews() {
        childrenTitle.update()
        childName1Panel.setup(label: "Child Name #1")
        childName2Panel.setup(label: "Child Name #2")
        schoolTitle.update()
        school1Panel.setup(label: "School Name #1")
        school2Panel.setup(label: "School Name #2")
        providerTitle.update()
        provider1Panel.setup(label: "Service Providers Name #1")
        provider2Panel.setup(label: "Service Providers Name #2")
        verifierTitle.update()
        verifier1Panel.setup(label: "Independent Verifier #1")
        verifier2Panel.setup(label: "Independent Verifier #2")
    }
    
    // MARK: - NavBar
    private func initNavBar() {
        self.navigationItem.title = "PoE Participants"
    }

}
