//
//  PoEIndVerifierViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 3/13/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import UIKit

class PoEIndVerifierViewController: SingleBaseViewController {

    //MARK: - Declarations
    
    lazy var whoAreYou : _PoEIndependentVerifierMain = {
        let view = _PoEIndependentVerifierMain()
        //view.nextButton.addTarget(self, action: #selector(nextButtonClicked(_sender:)), for: .touchUpInside)
        return view
    }()

    //MARK: - Methods

    private func initViews() {
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)

        containerView.addSubview(whoAreYou)
         _ = whoAreYou.anchor(containerView.topAnchor, left: nil, bottom: containerView.bottomAnchor, right: nil, topConstant: topBarHeight+15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 340, heightConstant: 60)
        _ = whoAreYou.centralizeX(containerView.centerXAnchor)
        
    }
    
    //MARK: - Events
    
    @objc func nextButtonClicked(_sender: SCButton) {
        
    }
    
    //MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initNavBar()
        initViews()
    }
    
    override func viewDidLayoutSubviews() {
        whoAreYou.finalizeMe()
    }
    

    //MARK: - NavBar
    
    private func initNavBar() {
        self.navigationItem.title = "On-boarding Independent Verifier"
    }


}
