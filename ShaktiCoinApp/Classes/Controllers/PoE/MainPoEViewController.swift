//
//  MainPoEViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 3/6/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import UIKit

class MainPoEViewController: BaseScrollViewController, UITextViewDelegate {

    
    // MARK: - Declarations
    
    lazy var subTitle : SCLineTitle = {
        let lineTitle = SCLineTitle()
        return lineTitle
    }()

    let poeList = ["Feats of Child", "Feats of School", "Feats of Service Providers", "Feats of Independent-Validators"]

    lazy var poeInfo : SCAttributedTextView = {
        
        let textView = SCAttributedTextView()
        textView.delegate = self
        
        let textContent: String = "PoE. One small step for one,"
        textView.setup(textInfo: textContent, textColor: UIColor.white, font: UIFont.lato(fontSize: 14))
        textView.setText(textContent: "a giant leap for everyone.", textColor: UIColor.white, font: UIFont.lato(fontSize: 14))
        textView.finilizeWithCenterlize()
        
        return textView
    }()

    lazy var cancelButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Next".localized, for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        button.tag = 1
        return button
    }()

    
    // MARK: - Methods
    
    private func initViews() {
    
        containerView.addSubview(subTitle)
        subTitle.Title = "Add a PoE Feat"
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)

        _ = subTitle.anchor(containerView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: topBarHeight+20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = subTitle.centralizeX(containerView.centerXAnchor)

        var topConstValue: CGFloat = 60
        var tagId : Int = 1
        
        for item in poeList {
        
            let viewItem = PoEFeatItemView()
            viewItem.backgroundColor = .black
            containerView.addSubview(viewItem)
            
            viewItem.tag = tagId

            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(openIconClicked(tapGestureRecognizer:)))
            viewItem.addIcon.isUserInteractionEnabled = true
            viewItem.addGestureRecognizer(tapGestureRecognizer)
            
            viewItem.itemTitle.text = item
            
            _ = viewItem.anchor(subTitle.topAnchor, left: containerView.leftAnchor, bottom: nil, right: containerView.rightAnchor, topConstant: topConstValue, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0)
            
            topConstValue = topConstValue + 80
            
            tagId = tagId + 1
        }
        
        containerView.addSubview(poeInfo)
        _ = poeInfo.anchor(nil, left: nil, bottom: containerView.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 130, rightConstant: 0, widthConstant: 200, heightConstant: 45)
        _ = poeInfo.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(cancelButton)
        _ = cancelButton.anchor(poeInfo.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 25, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 300, heightConstant: 36)
        _ = cancelButton.centralizeX(containerView.centerXAnchor)
    }
    
    // MARK: - Events
    
    @objc func openIconClicked(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tag: Int = tapGestureRecognizer.view?.tag ?? 0
        if tag == 1 {
            
            let vc = PoEParticipantsViewController()
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else if tag == 2 {

            //TODO

        } else if tag == 3 {
            
            let vc = PoEServiceProviderViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        
        } else if tag == 4 {
            
            let vc = PoEIndVerifierViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initNavBar()
        initViews()
        
    }
    
    override func viewDidLayoutSubviews() {
        subTitle.update()
    }
    
    // MARK: - NavBar
    
    private func initNavBar() {
        self.navigationItem.title = "Feats of PoE"
    }
    

}
