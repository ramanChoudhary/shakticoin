//
//  SelfyidEndpoint.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 27/10/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation


extension Endpoint {
    enum SelfyidEndpoint: EndpointProtocol {
        case search(String)
        
        var baseUrl: String {
            return ShaktiHost.current(.selfyid)
          //https://selfyid-stg.shakticoin.com/selfyid-service/api/v1/selfyid/wallet
        }
        
        var value: String {
            switch self {
            case .search(let keyword):
                return "/api/v1/selfyid/\(keyword)"
            }
        }
        
        var method: HTTPMethod {
            switch self {
            case .search:
                return .get
            }
        }
        
        var authenticated: Bool {
            return true
        }
        
        var isAuthorizingCall: Bool {
            return false
        }
        
        var contentType: APIRequestType {
            return .json
        }
    }
}
