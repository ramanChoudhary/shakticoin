//
//  OnboardEndpoint.swift
//  ShaktiCoinApp
//
//  Created by sperev on 10/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

extension Endpoint {
    enum Onboard: EndpointProtocol {
        case createUser
        case changePassword
        case createWallet
        case getPasswordRecoveryStatus
        
        var baseUrl: String {
            return ShaktiHost.current(.onboard)
        }
        
        var value: String {
            switch self {
            case .createUser:
                return "/api/v1/onboardShakti/users"
            case .changePassword:
                return "/api/v1/onboardShakti/users/password/change"
            case .createWallet:
                return "/api/v1/onboardShakti/wallet"
            case .getPasswordRecoveryStatus:
                return "/api/v1/onboardShakti/users/password/status"
            }
        }
        
        var method: HTTPMethod {
            switch self {
            case .getPasswordRecoveryStatus:
                return .get
            default:
                return .post
            }
        }
        
        var authenticated: Bool {
            switch self {
            case .createUser: return false
            case .changePassword: return true
            case .createWallet: return true
            case .getPasswordRecoveryStatus: return true
            }
        }
        
        var isAuthorizingCall: Bool {
            return false
        }
        
        var contentType: APIRequestType {
            return .json
        }
    }
}
