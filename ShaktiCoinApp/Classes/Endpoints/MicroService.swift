//
//  MicroService.swift
//  ShaktiCoinApp
//
//  Created by sperev on 07/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation


enum MicroService: Int {
    case emailVerification
    case phoneVerification
    case onboard
    case bountyReferral
    case walletService
    case iam
    case kycuser
    case license
    case bizVault
    case selfyid
    
    var host: String {
        switch self {
        case .emailVerification: return "emailotpservice"
        case .phoneVerification: return "mobileotpservice"
        case .onboard: return "onboardshakti"
        case .bountyReferral: return "referral"
        case .walletService: return "walletservice"
        case .iam: return "iam"
        case .kycuser: return "kycuser"
        case .license: return "licenseservice"
        case .bizVault: return "bizvault"
        case .selfyid: return "selfyid"
        }
    }
    
    var path: String {
        switch self {
        case .emailVerification: return "/email-otp-service"
        case .phoneVerification: return "/sms-otp-service"
        case .onboard: return "/onboardshakti-service"
        case .bountyReferral: return "/bountyservice"
        case .walletService: return "/walletservice"
        case .iam: return ""
        case .kycuser: return "/kyc-user-service"
        case .license: return "/license-service"
        case .bizVault: return "/bizvault"
        case .selfyid: return "/selfyid-service"
        }
    }
}

