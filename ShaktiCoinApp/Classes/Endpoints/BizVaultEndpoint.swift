//
//  BizVaultEndpoint.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 09/10/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

extension Endpoint {
    enum BizVaultEndpoint: EndpointProtocol {
        case checkBizVault
        
        var baseUrl: String {
            return ShaktiHost.current(.bizVault)
           // https://bizvault-stg.shakticoin.com/bizvault/api/v1/bizvaults/verify/bizvaultid
        }
        
        var value: String {
            switch self {
            case .checkBizVault:
                return "/api/v1/bizvaults/verify/bizvaultid"
            }
        }
        
        var method: HTTPMethod {
            switch self {
            case .checkBizVault:
                return .get
            }
        }
        
        var authenticated: Bool {
            return true
        }
        
        var isAuthorizingCall: Bool {
            return false
        }
        
        var contentType: APIRequestType {
            return .json
        }
    }
}
