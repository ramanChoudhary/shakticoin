//
//  BountyReferralEndpoint.swift
//  ShaktiCoinApp
//
//  Created by sperev on 08/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

extension Endpoint {
    enum BountyReferralEndpoint: EndpointProtocol {
        case allOffer
        case setGenesisBoutyGrantOption(String)
        case claimBonusBounty
        case bonusBounties
        case genesisBountyGrantOptions(String)
        case registerReferral
    
        var baseUrl: String {
            return ShaktiHost.current(.bountyReferral)
        }
    
        var value: String {
            switch self {
            case .allOffer:
                return "/api/v1/offers/all"
            case .setGenesisBoutyGrantOption(let optionId):
                return "/api/v1/bounties/grantoptions/\(optionId)"
            case .genesisBountyGrantOptions(let optionId):
                return "/api/v1/bounties/grantoptions/\(optionId)"
            case .claimBonusBounty, .bonusBounties:
                return "/api/v1/bounties"
            case .registerReferral:
                return "/api/v1/bounties/registerreferral/"
            }
        }
    
        var method: HTTPMethod {
            switch self {
            case .allOffer, .genesisBountyGrantOptions: return .get
            case .setGenesisBoutyGrantOption: return .put
            case .claimBonusBounty: return .post
            case .bonusBounties: return .get
            case .registerReferral: return .put
            }
        }
    
        var authenticated: Bool {
            return true
        }
        
        var isAuthorizingCall: Bool {
            return false
        }
        
        var contentType: APIRequestType {
            return .json
        }
    }
}
