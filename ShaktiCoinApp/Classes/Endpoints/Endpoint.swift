//
//  Endpoint.swift
//  ShaktiCoinApp
//
//  Created by sperev on 10/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
  case options = "OPTIONS"
  case get = "GET"
  case head = "HEAD"
  case post = "POST"
  case put = "PUT"
  case patch = "PATCH"
  case delete = "DELETE"
  case trace = "TRACE"
  case connect = "CONNECT"
}

protocol EndpointProtocol {
    var baseUrl: String { get }
    var value: String { get }
    var method: HTTPMethod { get }
    var contentType: APIRequestType { get }
    var authenticated: Bool { get }
    var isAuthorizingCall: Bool { get }
}

enum Endpoint {}
