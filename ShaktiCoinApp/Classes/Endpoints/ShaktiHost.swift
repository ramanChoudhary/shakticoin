//
//  ShaktiHost.swift
//  ShaktiCoinApp
//
//  Created by sperev on 07/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

struct ShaktiHost {
    static func current(_ service: MicroService) -> String {
        return baseUrl(type: .current, service: service)
    }
    
    static func baseUrl(type: Environment, service: MicroService) -> String {
        let type = type.value
        let host = service.host
        let path = service.path
        
        return "https://\(host)-\(type).shakticoin.com\(path)"
    }
}

