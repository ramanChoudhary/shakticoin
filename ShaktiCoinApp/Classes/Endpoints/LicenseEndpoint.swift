//
//  LicenseEndpoint.swift
//  ShaktiCoinApp
//
//  Created by sperev on 30/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

extension Endpoint {
    enum LicenseEndpoint: EndpointProtocol {
        case miningLicenses
        case nodeOperator
        
        var baseUrl: String {
            return ShaktiHost.current(.license)
        }
        
        var value: String {
            switch self {
            case .miningLicenses:
                return "/api/v1/licenses/mining"
            case .nodeOperator:
                return "/api/v1/node-operator"
            }
        }
        
        var method: HTTPMethod {
            switch self {
            case .miningLicenses, .nodeOperator:
                return .get
            }
        }
        
        var authenticated: Bool {
            return true
        }
        
        var isAuthorizingCall: Bool {
            return false
        }
        
        var contentType: APIRequestType {
            return .json
        }
    }
}
