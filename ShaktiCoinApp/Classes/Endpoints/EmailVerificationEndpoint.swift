//
//  EmailVerificationEndpoint.swift
//  ShaktiCoinApp
//
//  Created by sperev on 10/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

extension Endpoint {
    enum EmailVerification: EndpointProtocol {
        case status
        case request
        case verify
        case confirm
        case forgotPassword
        
        var baseUrl: String {
            return ShaktiHost.current(.emailVerification)
        }
        
        var value: String {
            switch self {
            case .verify:
                return "/api/v1/otp/verify"
            case .request:
                return "/api/v1/registration/request"
            case .status:
                return "/api/v1/inquiry/email"
            case .confirm:
                return "/api/v1/registration/confirm-registration"
            case .forgotPassword:
                return "/api/v1/otp/request"
            }
        }
        
        var method: HTTPMethod {
            switch self {
            case .confirm: return .get
            default: return .post
            }
        }
        
        var authenticated: Bool {
            return false
        }
        
        var isAuthorizingCall: Bool {
            return false
        }
        
        var contentType: APIRequestType {
            return .json
        }
    }
}
