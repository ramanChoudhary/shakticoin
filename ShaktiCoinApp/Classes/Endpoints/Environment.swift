//
//  Environment.swift
//  ShaktiCoinApp
//
//  Created by sperev on 07/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum Environment: Int {
    case staging
    case qualityAssurance
    case production
    
    var value: String {
        switch self {
        case .staging: return "stg"
        case .qualityAssurance: return "qa"
        case .production: return "prod"
        }
    }
    
    static var current: Environment {
        let rawValue = UserDefaults.standard.integer(forKey: "enviroment_selection")
        return Environment(rawValue: rawValue) ?? Environment.production
    }
}
