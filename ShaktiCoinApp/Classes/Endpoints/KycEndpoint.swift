//
//  KycEndpoint.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 27/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation


extension Endpoint {
    enum KycEndpoint: EndpointProtocol {
        case getWallet
        case details
        case getStatus
        
        var baseUrl: String {
            return ShaktiHost.current(.kycuser)
            ///return "https://kycuser-stg.shakticoin.com/kyc-user-service/api/v1"
        }
        
        var value: String {
            switch self {
            case .getWallet:
                return "/api/v1/kyc/wallet"
            case .details:
                return "/api/v1/kyc/details/"
            case .getStatus:
                return "/api/v1/kyc/status"
            }
        }
        
        var method: HTTPMethod {
            return .get
        }
        
        var authenticated: Bool {
            return true
        }
        
        var isAuthorizingCall: Bool {
            return false
        }
        
        var contentType: APIRequestType {
            return .json
        }
    }
}
