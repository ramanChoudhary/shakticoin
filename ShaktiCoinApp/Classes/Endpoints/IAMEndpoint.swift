//
//  IAMEndpoint.swift
//  ShaktiCoinApp
//
//  Created by sperev on 13/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

extension Endpoint {
    enum IAMEndpoint: EndpointProtocol {
        case token
        
        var baseUrl: String {
            return ShaktiHost.current(.iam)
        }
        
        var value: String {
            switch self {
            case .token:
                return "/oxauth/restv1/token"
            }
        }
        
        var method: HTTPMethod {
            switch self {
            default: return .post
            }
        }
        
        var authenticated: Bool {
            return false
        }
        
        var isAuthorizingCall: Bool {
            return true
        }
        
        var contentType: APIRequestType {
            return .formUrlencoded
        }
    }
}
