//
//  WalletServiceEndpoint.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 27/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation


extension Endpoint {
    enum WalletServiceEndpoint: EndpointProtocol {
        case getWalletBalance
        case getBalanceHistory
        case createSession
        case transferCoins
        case blockData
        
        var baseUrl: String {
            return ShaktiHost.current(.walletService)
        }
        
        var value: String {
            switch self {
            case .getWalletBalance:
                return "/api/v1/users/wallet/mybalance"
            case .getBalanceHistory:
                return "/api/v1/wallet/transaction/history/bytime"
            case .createSession:
                return "/api/v1/users/session/"
            case .transferCoins:
                return "/api/v1/coins/"
            case .blockData:
                return "/api/v1/sxeledger/blockdata"
            }
        }
        
        var method: HTTPMethod {
            return .post
        }
        
        var authenticated: Bool {
            return true
        }
        
        var isAuthorizingCall: Bool {
            return false
        }
        
        var contentType: APIRequestType {
            return .json
        }
    }
}

//https://walletservice-stg.shakticoin.com/walletservice/api/v1/users/session/
