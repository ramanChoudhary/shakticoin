//
//  ContactSectionTableViewCell.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/14/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class ContactSectionTableViewCell: UITableViewCell {

    // MARK: - Declarations
    
    lazy var sectionLetter : UILabel = {
        let label = UILabel()
        label.backgroundColor = .gray
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 16)
        label.textAlignment = .center
        return label
    }()
    
    let aeView = UILabel()
    let fjView = UILabel()
    let koView = UILabel()
    let ptView = UILabel()
    let uzView = UILabel()
    
    // MARK: - Methods
    
    func setup(index: Int) {
        
        if index == 0 {
            setupFilterView()
        }
        
        switch index {
        case 0:
            sectionLetter.text = "A"
        case 1:
            sectionLetter.text = "B"
        case 2:
            sectionLetter.text = "C"
        default:
            break
        }
        
    }
    
    func setupFilterView() {
        
        aeView.backgroundColor = .clear
        aeView.text = "A-E"
        aeView.font = UIFont.lato(fontSize: 14)
        aeView.textColor = .white
        aeView.textAlignment = .center
        addSubview(aeView)
        
        fjView.backgroundColor = .clear
        fjView.text = "F-J"
        fjView.font = UIFont.lato(fontSize: 14)
        fjView.textColor = .white
        fjView.textAlignment = .center
        addSubview(fjView)
        
        koView.backgroundColor = .clear
        koView.text = "K-O"
        koView.font = UIFont.lato(fontSize: 14)
        koView.textColor = .white
        koView.textAlignment = .center
        addSubview(koView)
        
        ptView.backgroundColor = .clear
        ptView.text = "P-T"
        ptView.font = UIFont.lato(fontSize: 14)
        ptView.textColor = .white
        ptView.textAlignment = .center
        addSubview(ptView)
        
        uzView.backgroundColor = .clear
        uzView.text = "U-Z"
        uzView.font = UIFont.lato(fontSize: 14)
        uzView.textColor = .white
        uzView.textAlignment = .center
        addSubview(uzView)
        
        _ = aeView.anchor(nil, left: sectionLetter.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 52, heightConstant: 30)
        _ = aeView.centralizeY(centerYAnchor)
        
        _ = fjView.anchor(nil, left: aeView.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 7, bottomConstant: 0, rightConstant: 0, widthConstant: 52, heightConstant: 30)
        _ = fjView.centralizeY(centerYAnchor)
        
        _ = koView.anchor(nil, left: fjView.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 7, bottomConstant: 0, rightConstant: 0, widthConstant: 52, heightConstant: 30)
        _ = koView.centralizeY(centerYAnchor)
        
        _ = ptView.anchor(nil, left: koView.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 7, bottomConstant: 0, rightConstant: 0, widthConstant: 52, heightConstant: 30)
        _ = ptView.centralizeY(centerYAnchor)
        
        _ = uzView.anchor(nil, left: ptView.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 7, bottomConstant: 0, rightConstant: 0, widthConstant: 52, heightConstant: 30)
        _ = uzView.centralizeY(centerYAnchor)
    }
    
    func finilizeMe() { }
    
    // MARK: - TableViewCell
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(sectionLetter)
        _ = sectionLetter.anchor(nil, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 30, heightConstant: 30)
        _ = sectionLetter.centralizeY(centerYAnchor)
        
    }
    
    override func layoutSubviews() {
        
        sectionLetter.layer.cornerRadius = 15
        sectionLetter.clipsToBounds = true
        
        aeView.layer.cornerRadius = 15
        aeView.clipsToBounds = true
        aeView.layer.borderColor = UIColor.white.cgColor
        aeView.layer.borderWidth = 0.8
        
        fjView.layer.cornerRadius = 15
        fjView.clipsToBounds = true
        fjView.layer.borderColor = UIColor.white.cgColor
        fjView.layer.borderWidth = 0.8
        
        koView.layer.cornerRadius = 15
        koView.clipsToBounds = true
        koView.layer.borderColor = UIColor.white.cgColor
        koView.layer.borderWidth = 0.8
        
        ptView.layer.cornerRadius = 15
        ptView.clipsToBounds = true
        ptView.layer.borderColor = UIColor.white.cgColor
        ptView.layer.borderWidth = 0.8
        
        uzView.layer.cornerRadius = 15
        uzView.clipsToBounds = true
        uzView.layer.borderColor = UIColor.white.cgColor
        uzView.layer.borderWidth = 0.8
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:)")
    }


}
