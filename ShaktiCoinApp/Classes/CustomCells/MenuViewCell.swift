//
//  MenuViewCell.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 10/30/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class MenuViewCell: UICollectionViewCell {
    
    // MARK: - Declarations
    
    var menu: Menu? {
        didSet {
            
            guard let menu = menu else {
                return
            }
            
            switch menu {
            case .wallet:
                menuButton.setImage(UIImage(named: "WalletMenu"), for: .normal)
                itemName.text = "Wallet".localized
            case .vault:
                menuButton.setImage(UIImage(named: "VaultMenu"), for: .normal)
                itemName.text = "Vault".localized
            case .miner:
                menuButton.setImage(UIImage(named: "Miner"), for: .normal)
                itemName.text = "Miner".localized
            case .referrals:
                menuButton.setImage(UIImage(named: "Refer"), for: .normal)
                itemName.text = "Referrals".localized
            case .claimBonusBounty:
                menuButton.setImage(UIImage(named: "Refer"), for: .normal)
                itemName.text = "ClaimYourBounty".localized
            case .grabBonusBounty:
                menuButton.setImage(UIImage(named: "Refer"), for: .normal)
                itemName.text = "GrabYourBounty".localized
            case .settings:
                menuButton.setImage(UIImage(named: "Settings"), for: .normal)
                itemName.text = "Settings".localized
            case .home:
                menuButton.setImage(UIImage(named: "Home"), for: .normal)
                itemName.text = "Home"
            case .familyTree:
                menuButton.setImage(UIImage(named: "family"), for: .normal)
                itemName.text = "Family"
            case .company:
                menuButton.setImage(UIImage(named: "company_m"), for: .normal)
                itemName.text = "Company"
            case .poe:
                menuButton.setImage(UIImage(named: "company_m"), for: .normal)
                itemName.text = "Company"
            case .addFriendsAndFamily:
                menuButton.setImage(UIImage(named: "company_m"), for: .normal)
                itemName.text = "Company"
            }
         }
    }
    
    let menuButton : UIButton = {
        let button = UIButton()
        button.imageView?.tintColor = UIColor.white
        return button
    }()
    
    let itemName : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 9, type: .Bold) //TODO SemiBold
        return label
    }()
   
    // MARK: - Methods
    
    func setupViews() {
        
        addSubview(menuButton)
        addSubview(itemName)
        
        _ = menuButton.centralizeX(centerXAnchor)
        _ = menuButton.centralizeY(centerYAnchor, constant: -10)
        
        _ = itemName.anchor(menuButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = itemName.centralizeX(centerXAnchor)
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0.0, y: frame.height, width: frame.width, height: 2.0)
        bottomLine.backgroundColor = UIColor.darkGray.cgColor
        layer.addSublayer(bottomLine)
    }

    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
