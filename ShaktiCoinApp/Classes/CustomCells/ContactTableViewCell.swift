//
//  ContactTableViewCell.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/14/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class ContactTableViewCell : UITableViewCell {
    
    // MARK: - Declarations
    
    lazy var profilePic : UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
    lazy var fullName : UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.font = UIFont.lato(fontSize: 16, type: .Bold)
        return label
    }()
    
    lazy var email : UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red:0.85, green:0.85, blue:0.85, alpha:1)
        label.font = UIFont.lato(fontSize: 12)
        return label
    }()
    
    lazy var mobile : UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red:0.85, green:0.85, blue:0.85, alpha:1)
        label.font = UIFont.lato(fontSize: 12)
        return label
    }()
    
    lazy var indicatorView : UIView = {
        let view = UIView()
        view.backgroundColor = .gray
        return view
    }()
    
    // MARK: - Methods
    
    func setupViews() {
        
        addSubview(profilePic)
        _ = profilePic.anchor(nil, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 0, widthConstant: 30, heightConstant: 30)
        _ = profilePic.centralizeY(centerYAnchor)
        
        addSubview(fullName)
        
        _ = fullName.anchor(profilePic.topAnchor, left: profilePic.rightAnchor, bottom: nil, right: nil, topConstant: -8, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        addSubview(email)
        
        _ = email.anchor(profilePic.bottomAnchor, left: profilePic.rightAnchor, bottom: nil, right: nil, topConstant: -8, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        addSubview(mobile)
        
        _ = mobile.anchor(profilePic.bottomAnchor, left: email.rightAnchor, bottom: nil, right: nil, topConstant: -8, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        addSubview(indicatorView)
        
        _ = indicatorView.anchor(nil, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 5, widthConstant: 20, heightConstant: 20)
        _ = indicatorView.centralizeY(centerYAnchor)
    }
    
    func finilizeMe() { }
    
    // MARK: - TableViewCell
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    override func layoutSubviews() {
        profilePic.layer.cornerRadius = 30/2
        profilePic.clipsToBounds = true
        
        indicatorView.layer.cornerRadius = 20/2
        indicatorView.clipsToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:)")
    }
}
