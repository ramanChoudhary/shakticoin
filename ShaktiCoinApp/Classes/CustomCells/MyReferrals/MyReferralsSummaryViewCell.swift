//
//  MyReferralsSummaryViewCell.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/11/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class MyReferralsSummaryViewCell: UICollectionViewCell {
    
    // MARK: - First cell Declarations
    
    lazy var balanceInfo : _CurrentBalance = {
        let view = _CurrentBalance()
        view.unlockQuestion.Title = "Refer others to unlock SXE"
        view.lockedLink.text = "30 months to unlock"
        return view
    }()
    
    lazy var referredCount : UILabel = {
        let label = UILabel()
        label.text = "0 Referred"
        label.font = UIFont.lato(fontSize: 36)
        label.textColor = .white
        return label
    }()
    
    lazy var friendSMInfo : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.setup(textInfo: "Refer friends by entering their info, or by importing \ncontacts from your social media accounts.", textColor: UIColor(red:0.85, green:0.85, blue:0.85, alpha:1), font: UIFont.lato(fontSize: 14))
        textView.finilizeWithCenterlize()
        return textView
    }()
    
    lazy var addReferralButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Add a Referral", for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14)
        button.titleLabel?.textColor = .white
        return button
    }()
    
    lazy var myEffortRateButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("My Effort Rates", for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14)
        button.titleLabel?.textColor = .white
        return button
    }()
    
    lazy var alternativeLineTitle : SCLineTitle = {
        let lineTitle = SCLineTitle()
        lineTitle.Title = "Or alternatively"
        return lineTitle
    }()
    
    lazy var importFromLabel : UILabel = {
        let label = UILabel()
        label.text = "Import from:"
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 12, type: .Bold)
        return label
    }()
    
    lazy var scrollArrowIcon : UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "arrowDown")
        view.tintColor = UIColor.mainColor()
        return view
    }()

    
    // MARK: - Second cell Declarations
    
    lazy var socialIconsView : _SocialIconsContainerView = {
        let view = _SocialIconsContainerView()
        return view
    }()
    
    
    // MARK: - Methods
    
    func setup(seqNo: SeqNo) {
        
        switch seqNo {
        case .First:
            setupTopView()
        case .Second:
            setupBottomView()
        default:
            break
        }
    }
    
    private func setupContactsTable() {
        backgroundColor = .blue
    }
    
    private func setupTopView() {
        
        addSubview(balanceInfo)
        addSubview(referredCount)
        addSubview(friendSMInfo)
        addSubview(addReferralButton)
        addSubview(myEffortRateButton)
        addSubview(alternativeLineTitle)
        addSubview(importFromLabel)
        addSubview(scrollArrowIcon)
        
        _ = balanceInfo.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 200)
        
        _ = referredCount.anchorToTop(balanceInfo.bottomAnchor, topConstant: 10)
        _ = referredCount.centralizeX(centerXAnchor)
        
        _ = friendSMInfo.anchor(referredCount.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 34)
        _ = friendSMInfo.centralizeX(centerXAnchor)
        
        _ = addReferralButton.anchor(friendSMInfo.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 30, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 280, heightConstant: 36)
        _ = addReferralButton.centralizeX(centerXAnchor)
        
        _ = myEffortRateButton.anchor(addReferralButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 30, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 280, heightConstant: 36)
        _ = myEffortRateButton.centralizeX(centerXAnchor)
     
        _ = alternativeLineTitle.anchor(myEffortRateButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 25, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = alternativeLineTitle.centralizeX(centerXAnchor)
        
        _ = importFromLabel.anchor(alternativeLineTitle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 45, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = importFromLabel.centralizeX(centerXAnchor)
        
        _ = scrollArrowIcon.anchor(importFromLabel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = scrollArrowIcon.centralizeX(centerXAnchor)
    }
    
    
    private func setupBottomView() {
        
        backgroundColor = .black
        addSubview(socialIconsView)
        
        _ = socialIconsView.anchor(topAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 100)
        _ = socialIconsView.centralizeX(centerXAnchor)
        
        
    }
    
    
    func finilizeMe() {
        balanceInfo.finalizeMe()
        alternativeLineTitle.updateWithSpecificWidth(width: 330, lineSize: 30)
    }
    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
