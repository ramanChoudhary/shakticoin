//
//  EffortRateTableViewCell.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/13/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class EffortRateTableViewCell: UITableViewCell {

    // MARK: - Declarations
    
    lazy var iconImageView : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "Facebook")
        return imageView
    }()
    
    lazy var viewConverted: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.mainColor()
        return view
    }()
    
    lazy var viewProgressed : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.gray
        return view
    }()

    lazy var viewInfluenced : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.darkGray
        return view
    }()
    
    lazy var verticalLine : UIView = {
        let view = UIView()
        view.alpha = 0.70
        view.backgroundColor = UIColor(red:0.61, green:0.61, blue:0.61, alpha:1)
        return view
    }()
    
    // MARK: - Methods
    
    func setup(rate: EffortRate) {
        
        addSubview(iconImageView)
        addSubview(viewConverted)
        addSubview(viewProgressed)
        addSubview(viewInfluenced)
        addSubview(verticalLine)
        
        _ = iconImageView.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 2, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 19, heightConstant: 19)
        
        iconImageView.image = UIImage(named: rate.icon)
        
        _ = viewConverted.anchor(topAnchor, left: iconImageView.rightAnchor, bottom: nil, right: nil, topConstant: 2, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: (layer.bounds.width*CGFloat(rate.converted)/100), heightConstant: 15)
        
        let convertedLbl = UILabel()
        convertedLbl.text = String(rate.converted) + "%"
        convertedLbl.textColor = .black
        convertedLbl.font = UIFont.lato(fontSize: 9)
        
        viewConverted.addSubview(convertedLbl)
        _ = convertedLbl.centralizeX(viewConverted.centerXAnchor)
        
        _ = viewProgressed.anchor(topAnchor, left: viewConverted.rightAnchor, bottom: nil, right: nil, topConstant: 2, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: (layer.bounds.width*CGFloat(rate.progressing)/100), heightConstant: 15)
        
        let progressedLbl = UILabel()
        progressedLbl.text = String(rate.progressing) + "%"
        progressedLbl.textColor = .white
        progressedLbl.font = UIFont.lato(fontSize: 9)
        
        viewProgressed.addSubview(progressedLbl)
        _ = progressedLbl.centralizeX(viewProgressed.centerXAnchor)
        
        _ = viewInfluenced.anchor(topAnchor, left: viewProgressed.rightAnchor, bottom: nil, right: nil, topConstant: 2, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: (layer.bounds.width*CGFloat(rate.influenced)/100), heightConstant: 15)
        
        let influencedLbl = UILabel()
        influencedLbl.text = String(rate.influenced) + "%"
        influencedLbl.textColor = .white
        influencedLbl.font = UIFont.lato(fontSize: 9)
        
        viewInfluenced.addSubview(influencedLbl)
        _ = influencedLbl.centralizeX(viewInfluenced.centerXAnchor)
        
        if rate.index == 11 {
            _ = verticalLine.anchor(viewConverted.topAnchor, left: viewConverted.leftAnchor, bottom: viewConverted.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0.5, heightConstant: 0)
        } else {
            _ = verticalLine.anchor(viewConverted.topAnchor, left: viewConverted.leftAnchor, bottom: bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0.5, heightConstant: 0)
        }
    }

    
    // MARK: - TableViewCell
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:)")
    }
}
