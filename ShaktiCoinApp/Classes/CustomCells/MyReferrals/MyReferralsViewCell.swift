//
//  MyReferralsSVViewCell.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/10/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class MyReferralsViewCell: UICollectionViewCell {
    
    // MARK: - First cell Declarations
 
    lazy var balanceInfo : _CurrentBalance = {
        let view = _CurrentBalance()
        return view
    }()
    
    lazy var bonusBountyView : _SubView = {
        let view = _SubView()
        view.icon.image = UIImage(named: "Time")
        return view
    }()
    
    lazy var shaktiWorldView : _SubView = {
        let view = _SubView()
        view.icon.image = UIImage(named: "FriendsReferral")
        return view
    }()
    
    lazy var mixItUpView : _SubView = {
        let view = _SubView()
        view.icon.image = UIImage(named: "Mixing")
        return view
    }()
    
    lazy var scrollArrowIcon : UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "arrowDown")
        view.tintColor = UIColor.mainColor()
        return view
    }()

    
    // MARK: - Second cell Declarations
    
    lazy var howFastLineTitle : SCLineTitle = {
        let view = SCLineTitle()
        view.Title = "How fast can you unlock your Bounty?"
        return view
    }()
    
    lazy var referCountText : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.setup(textInfo: "I will refer", textColor: .white, font: UIFont.lato(fontSize: 18))
        textView.setText(textContent: " 3 ", textColor: UIColor.mainColor(), font: UIFont.lato(fontSize: 18))
        textView.setText(textContent: "people per day, based on ", textColor: UIColor.white, font: UIFont.lato(fontSize: 18))
        textView.setText(textContent: "\nmy effort I will unlock my bonus bounty", textColor: UIColor.white, font: UIFont.lato(fontSize: 18))
        textView.setText(textContent: "\nwithin ", textColor: UIColor.white, font: UIFont.lato(fontSize: 18))
        textView.setText(textContent: "X ", textColor: UIColor.mainColor(), font: UIFont.lato(fontSize: 18))
        textView.setText(textContent: "days", textColor: UIColor.white, font: UIFont.lato(fontSize: 18))
        textView.finilizeWithCenterlize()
        return textView
    }()
 
    lazy var bottomTextInfo : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        let color = UIColor(red:0.85, green:0.85, blue:0.85, alpha:1)
        let font = UIFont.lato(fontSize: 14)
        textView.setup(textInfo: "We will track your effort to unlock your bounty and ", textColor: color, font: font)
        textView.setText(textContent: "continue to forecast your efforts. Remember it is your ", textColor: color, font: font)
        textView.setText(textContent: "effort that counts.", textColor: color, font: font)
        textView.finilizeWithCenterlize()
        return textView
    }()
    
    lazy var startReferringButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Start referring friends and family", for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 14)
        return button
    }()
    
    // MARK: - Methods
    
    func setup(seqNo: SeqNo) {
        
        switch seqNo {
        case .First:
            setupTopView()
        case .Second:
            setupBottomView()
        case .Third:
            break
        }
    }
    
    private func setupTopView() {
        
        addSubview(balanceInfo)
        addSubview(bonusBountyView)
        addSubview(shaktiWorldView)
        addSubview(mixItUpView)
        addSubview(scrollArrowIcon)

        _ = balanceInfo.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 250)
        
        _ = bonusBountyView.anchor(balanceInfo.unlockQuestion.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 335, heightConstant: 110)
        _ = bonusBountyView.centralizeX(centerXAnchor)
        
        _ = shaktiWorldView.anchor(bonusBountyView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 18, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 335, heightConstant: 105)
        _ = shaktiWorldView.centralizeX(centerXAnchor)
        
        _ = mixItUpView.anchor(shaktiWorldView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 18, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 335, heightConstant: 110)
        _ = mixItUpView.centralizeX(centerXAnchor)
        
        _ = scrollArrowIcon.anchor(mixItUpView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = scrollArrowIcon.centralizeX(centerXAnchor)
        
        let color = UIColor(red:0.85, green:0.85, blue:0.85, alpha:1)
        let font = UIFont.lato(fontSize: 12)
        let bold = UIFont.lato(fontSize: 12, type: .Bold)
        
        bonusBountyView.textInfo.setup(textInfo: "1. Your Bonus Bounty ", textColor: color, font: bold)
        bonusBountyView.textInfo.setText(textContent: "will be automatically unlocked after 7 years of using your wallet at least once a month.", textColor: color, font: font)
        bonusBountyView.textInfo.finilize()

        shaktiWorldView.textInfo.setup(textInfo: "2. You can speed up ", textColor: color, font: bold)
        shaktiWorldView.textInfo.setText(textContent: "this process by referring friends and family to join the Shakti World.", textColor: color, font: font)
        shaktiWorldView.textInfo.finilize()

        mixItUpView.textInfo.setup(textInfo: "3. Mix it up: ", textColor: color, font: bold)
        mixItUpView.textInfo.setText(textContent: "Use your wallet at least once a month and make your friends early adopters at the same time. In no time, you wil be able to unlock your bounty.", textColor: color, font: font)
        mixItUpView.textInfo.finilize()
    }
    
    
    private func setupBottomView() {
        
        backgroundColor = .black
        addSubview(howFastLineTitle)
        
        _ = howFastLineTitle.anchor(topAnchor, left: nil, bottom: nil, right: nil, topConstant: 30, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = howFastLineTitle.centralizeX(centerXAnchor)
        
        addSubview(referCountText)
        
        _ = referCountText.anchor(howFastLineTitle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 345, heightConstant: 75)
        _ = referCountText.centralizeX(centerXAnchor)
        
        addSubview(bottomTextInfo)
        
        _ = bottomTextInfo.anchor(referCountText.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 25, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 60)
        _ = bottomTextInfo.centralizeX(centerXAnchor)
        
        addSubview(startReferringButton)
        
        _ = startReferringButton.anchor(bottomTextInfo.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 25, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 280, heightConstant: 36)
        _ = startReferringButton.centralizeX(centerXAnchor)
    }
    
    
    func finilizeMe() {
        balanceInfo.finalizeMe()
        howFastLineTitle.updateWithSpecificWidth(width: 330, lineSize: 10)
    }
    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
