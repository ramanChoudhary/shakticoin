//
//  CompanyDetailTableViewCell.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 3/5/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import UIKit

class CompanyDetailTableViewCell: UITableViewCell {

    // MARK: - Declarations
       
    lazy var fieldLbl : UILabel = {
        let label = UILabel()
        label.font = UIFont.lato(fontSize: 14)
        label.textColor = UIColor.white
        return label
    }()
       
    lazy var vaultLbl : UILabel = {
        let label = UILabel()
        label.font = UIFont.lato(fontSize: 14)
        label.textColor = UIColor.white
        return label
    }()
    
    // MARK: - Methods
    
    private func setupViews() {
               
        addSubview(fieldLbl)
        _ = fieldLbl.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 6, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
                
        addSubview(vaultLbl)
        _ = vaultLbl.anchor(topAnchor, left: nil, bottom: nil, right: rightAnchor, topConstant: 6, leftConstant: 0, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0)
    }
    
    // MARK: - TableViewCell
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:)")
    }


}
