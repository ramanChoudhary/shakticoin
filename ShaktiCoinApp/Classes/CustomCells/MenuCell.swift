//
//  MenuCell.swift
//  ShaktiCoinApp
//
//  Created by Pravin Gawale on 23/06/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import UIKit

class MenuCell: UICollectionViewCell {
    
    @IBOutlet weak var bgView: UIView!

    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var dropDownImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    // MARK: - Declarations
    
    var menu: Menu? {
        didSet {
            
            guard let menu = menu else {
                return
            }
            
            self.layer.borderColor = ColorStyle.gold.cgColor
            self.layer.borderWidth = 0.1
            iconImage.tintColor = ColorStyle.gold.color
            
            dropDownImage.isHidden = true
            
            switch menu {
            case .wallet:
                iconImage.image = UIImage(named: "WalletMenu")
                titleLabel.text = "Wallet".localized
            case .vault:
                iconImage.image = UIImage(named: "VaultMenu")
                titleLabel.text = "Vault".localized
            case .miner:
                iconImage.image = UIImage(named: "Miner")
                titleLabel.text = "Miner".localized
                dropDownImage.isHidden = false
                dropDownImage.image = UIImage(named: "Dropdown-down")
            case .poe:
                iconImage.image = nil
                titleLabel.text = "PoE"
            case .addFriendsAndFamily:
                iconImage.image = nil
                titleLabel.text = "Add Friends & Family"
            case .referrals:
                iconImage.image = UIImage(named: "Refer")
                titleLabel.text = "Referrals".localized
            case .claimBonusBounty:
                iconImage.image = UIImage(named: "Refer")
                titleLabel.text = "ClaimYourBounty".localized
            case .grabBonusBounty:
                iconImage.image = UIImage(named: "Refer")
                titleLabel.text = "GrabYourBounty".localized
            case .settings:
                iconImage.image = UIImage(named: "Settings")
                titleLabel.text = "Settings".localized
            case .home:
                iconImage.image = UIImage(named: "Home")
                titleLabel.text = "Home"
            case .familyTree:
                iconImage.image = UIImage(named: "family")
                titleLabel.text = "Family Tree"
            case .company:
                iconImage.image = UIImage(named: "company_m")
                titleLabel.text = "Company"
            }
         }
    }
    

   
    // MARK: - Methods
    
}
