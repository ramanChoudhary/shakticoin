//
//  SettingsSectionTableViewCell.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/16/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class SettingsSectionTableViewCell: UITableViewCell {

    // MARK: - Declarations
    
    lazy var sectionTitle : UILabel = {
        let label = UILabel()
        label.font = UIFont.lato(fontSize: 24, type: .Light)
        label.textColor = .white
        return label
    }()
    
    // MARK: - Methods
    
    func setup(section: SettingsTableSection) {
 
        addSubview(sectionTitle)
        _ = sectionTitle.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 5, leftConstant: 12, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        switch section {
        case .Account:
            sectionTitle.text = "Account"
        case .Security:
            sectionTitle.text = "Security"
        case .NotificationPreferences:
            sectionTitle.text = "Notification Preferences"
        case .HelpAndPolicies:
            sectionTitle.text = "Help & Policies"
        }
    }
    
    func finilizeMe() { }
    
    // MARK: - TableViewCell
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:)")
    }
}
