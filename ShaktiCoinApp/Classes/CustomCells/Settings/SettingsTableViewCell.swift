//
//  SettingsTableViewCell.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/16/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    // MARK: - Declarations
    
    lazy var arrowImageView : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ArrowForward")
        imageView.tintColor = .white
        return imageView
    }()
    
    lazy var cellSwitch : UISwitch = {
        let _switch = UISwitch()
        _switch.tintColor = UIColor.mainColor()
        _switch.onTintColor = UIColor.mainColor()
        return _switch
    }()
    
    lazy var leftTextLbl : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 14, type: .Bold)
        return label
    }()
    
    lazy var rightTextLbl : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 14)
        return label
    }()
    
    lazy var tobeReleasedLbl : UILabel = {
        let label = UILabel()
        label.text = "Feature yet to be released."
        label.font = UIFont.lato(fontSize: 9)
        label.textColor = .white
        label.alpha = 0.5
        return label
    }()
    
    // MARK: - Methods
    
    func setup(leftText: String, isSwitch: Bool = false, isDisabled: Bool = false) {
        leftTextLbl.text = leftText
        if isDisabled {
            leftTextLbl.alpha = 0.5
            arrowImageView.alpha = 0.5
            cellSwitch.alpha = 0.5
            cellSwitch.isUserInteractionEnabled = false
            
            addSubview(tobeReleasedLbl)
            _ = (tobeReleasedLbl).anchor(leftTextLbl.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 4, leftConstant: 12, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
            
        }
        if isSwitch {
            setupSwitch()
        } else {
            setupArrow()
        }
    }
    
    func setupWithRightText(leftText: String, rightText: String, isSwitch: Bool = false, isDisabled: Bool = false) {
        leftTextLbl.text = leftText
        rightTextLbl.text = rightText
        if isDisabled {
            leftTextLbl.alpha = 0.5
            rightTextLbl.alpha = 0.5
            arrowImageView.alpha = 0.5
            cellSwitch.alpha = 0.5
            cellSwitch.isUserInteractionEnabled = false
            
            addSubview(tobeReleasedLbl)
            _ = (tobeReleasedLbl)
                .anchor(leftTextLbl.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 4, leftConstant: 12, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        }
        
        if isSwitch {
            setupSwitch()
        } else {
            setupArrow()
        }
    }
    
    private func setupSwitch() {
        
        addSubview(cellSwitch)
        _ = cellSwitch.anchor(nil, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 0)
        _ = cellSwitch.centralizeY(centerYAnchor)
        
    }
    
    private func setupArrow() {
        
        addSubview(arrowImageView)
        _ = arrowImageView.anchor(nil, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0)
        _ = arrowImageView.centralizeY(centerYAnchor)
    }
    
    // MARK: - TableViewCell
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        
        addSubview(leftTextLbl)
        _ = leftTextLbl.anchor(nil, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 12, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = leftTextLbl.centralizeY(centerYAnchor)
        
        addSubview(rightTextLbl)
        _ = rightTextLbl.anchor(nil, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 55, widthConstant: 0, heightConstant: 0)
        _ = rightTextLbl.centralizeY(centerYAnchor)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:)")
    }

}
