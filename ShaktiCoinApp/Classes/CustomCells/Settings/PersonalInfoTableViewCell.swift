//
//  PersonalInfoTableViewCell.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 1/8/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import UIKit

class PersonalInfoTableViewCell: UITableViewCell {

    // MARK: - Declarations
    
    lazy var rowTitle: UILabel = {
        let label = UILabel()
        label.font = .lato(fontSize: 16, type: .Bold) //TODO Medium
        label.textColor = .white
        return label
    }()
    
    lazy var rowValue: UILabel = {
        let label = UILabel()
        label.font = .lato(fontSize: 16)
        label.textColor = .white
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    lazy var arrowImageView : UIImageView = {
        let iv = UIImageView(image: UIImage(named: "ArrowForward"))
        iv.tintColor = .white
        return iv
    }()
    
    lazy var editLink : UILabel = {
        let label = UILabel()
        label.text = "Edit"
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 14)
        return label
    }()
    
    // MARK: - TableViewCell
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        
        addSubview(rowTitle)
        _ = rowTitle.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 20, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        addSubview(rowValue)
        _ = rowValue.anchor(rowTitle.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 3, leftConstant: 20, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        addSubview(arrowImageView)
        _ = arrowImageView.anchor(topAnchor, left: nil, bottom: nil, right: rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 35, widthConstant: 10, heightConstant: 16)
        
        addSubview(editLink)
        _ = editLink.anchor(topAnchor, left: nil, bottom: nil, right: arrowImageView.leftAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:)")
    }

}
