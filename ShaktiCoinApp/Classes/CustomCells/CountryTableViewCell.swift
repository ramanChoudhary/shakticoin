//
//  CountryTableViewCell.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/19/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class CountryTableViewCell: UITableViewCell {

    // MARK: - Declarations

    lazy var countryName : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 14)
        return label
    }()
    
    // MARK: - Methods
    func setupViews() {
        
        addSubview(countryName)
        _ = countryName.anchor(nil, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = countryName.centralizeY(centerYAnchor)
    }
    
    func finilizeMe() { }
    
    // MARK: - TableViewCell
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:)")
    }

}
