//
//  FtreeTableViewCell.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 2/25/20.
//  Copyright © 2020 Garnik Giloyan. All rights reserved.
//

import UIKit

class FtreeTableViewCell: UITableViewCell {

    // MARK: - Declarations
    
    lazy var profilePic : UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
    lazy var fullName : UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.font = UIFont.lato(fontSize: 16, type: .Bold)
        return label
    }()
    
    lazy var email : UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red:0.85, green:0.85, blue:0.85, alpha:1)
        label.font = UIFont.lato(fontSize: 12)
        return label
    }()
    
    lazy var mobile : UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red:0.85, green:0.85, blue:0.85, alpha:1)
        label.font = UIFont.lato(fontSize: 12)
        return label
    }()
    
    // MARK: - Methods
    private func setupViews() {
    
        addSubview(profilePic)
        _ = profilePic.anchor(nil, left: leftAnchor, bottom: nil, right: nil, topConstant: 5, leftConstant: 5, bottomConstant: 0, rightConstant: 0, widthConstant: 30, heightConstant: 30)
        _ = profilePic.centralizeY(centerYAnchor)
        
        addSubview(fullName)
        
        _ = fullName.anchor(profilePic.topAnchor, left: profilePic.rightAnchor, bottom: nil, right: nil, topConstant: -8, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        addSubview(email)
        
        _ = email.anchor(profilePic.bottomAnchor, left: profilePic.rightAnchor, bottom: nil, right: nil, topConstant: -15, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        addSubview(mobile)
        
        _ = mobile.anchor(profilePic.bottomAnchor, left: email.rightAnchor, bottom: nil, right: nil, topConstant: -15, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    // MARK: - TableViewCell
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
   
    override func layoutSubviews() {
        profilePic.layer.cornerRadius = 30/2
        profilePic.clipsToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:)")
    }

}
