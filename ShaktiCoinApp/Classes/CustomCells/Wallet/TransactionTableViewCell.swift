//
//  TransactionTableViewCell.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/9/19.
//  Copyright © 2019 Garnik Giloyan. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {

    // MARK: - Declarations
    
    lazy var trsTitle : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.numberOfLines = 3
        label.font = UIFont.lato(fontSize: 14, type: .Bold)
        return label
    }()
    
    lazy var dateTimeLbl : UILabel = {
        let label = UILabel()
        label.text = "05/09/19 16:30"
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 12, type: .Light)
        return label
    }()
    
    lazy var priceLbl : UILabel = {
        let label = UILabel()
        label.text = "+ X.XX"
        label.textColor = .mainColor()
        label.font = .lato(fontSize: 14)
        return label
    }()
    
    lazy var arrowImageView : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ArrowForward")
        imageView.tintColor = .white
        return imageView
    }()
    
    lazy var sepView : UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray
        return view
    }()
    
    // MARK: - Methods
    private func setupViews() {
        
        addSubview(trsTitle)
        addSubview(dateTimeLbl)
        addSubview(sepView)
        addSubview(priceLbl)
        addSubview(arrowImageView)
        
        _ = trsTitle.anchor(topAnchor, left: leftAnchor, bottom: nil, right: priceLbl.leftAnchor, topConstant: 7, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0)
        
        _ = dateTimeLbl.anchor(trsTitle.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 7, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = priceLbl.anchor(nil, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 45, widthConstant: 0, heightConstant: 0)
        _ = priceLbl.centralizeY(centerYAnchor)
        
        _ = arrowImageView.anchor(nil, left: priceLbl.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 8, heightConstant: 14)
        _ = arrowImageView.centralizeY(centerYAnchor)
        
        _ = sepView.anchor(nil, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.7)
        
    }
    
    // MARK: - TableViewCell
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:)")
    }
}
