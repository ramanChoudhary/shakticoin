//
//  AdminTableViewCell.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/7/19.
//  Copyright © 2019 Garnik Giloyan. All rights reserved.
//

import UIKit

class AdminTableViewCell: UITableViewCell {

    // MARK: - Declarations
    
    var isColumn : Bool = false
    lazy var titleLbl : UILabel = {
        let label = UILabel()
        label.font = UIFont.lato(fontSize: 14) //Lato-Light
        label.textColor = UIColor.lightGray //White
        return label
    }()
    
    lazy var walletLbl : UIButton = {
        let button = UIButton(type: .custom)
        button.titleLabel?.font = UIFont.lato(fontSize: 14)
        button.titleLabel?.textColor = UIColor.white
        button.isUserInteractionEnabled = true
        button.isEnabled = true
        return button
        
//        let label = UILabel()
//        label.font = UIFont.lato(fontSize: 14)
//        label.textColor = UIColor.white
//        return label
    }()
    
    lazy var wIcon : UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    lazy var vaultLbl : UILabel = {
        let label = UILabel()
        label.font = UIFont.lato(fontSize: 14)
        label.textColor = UIColor.white
        label.backgroundColor = .clear
        label.numberOfLines = 2
        return label
    }()
    
    lazy var vIcon : UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    // MARK: - Methods
    
    private func setupViews() {
        
        addSubview(titleLbl)
        _ = titleLbl.anchor(nil, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        titleLbl.centralizeY(self.centerYAnchor)
        
        addSubview(walletLbl)
        _ = walletLbl.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 60, heightConstant: 0)
        walletLbl.centralizeY(self.centerYAnchor)
        
        addSubview(wIcon)
        _ = wIcon.anchor(nil, left: walletLbl.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 22, heightConstant: 22)
        wIcon.centralizeY(self.centerYAnchor)
        
        addSubview(vaultLbl)
        _ = vaultLbl.anchor(nil, left: wIcon.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 40, bottomConstant: 0, rightConstant: 0, widthConstant: 60, heightConstant: 0)
        vaultLbl.centralizeY(self.centerYAnchor)
        
        addSubview(vIcon)
        _ = vIcon.anchor(nil, left: vaultLbl.rightAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 6, widthConstant: 22, heightConstant: 22)
        vIcon.centralizeY(self.centerYAnchor)
    }
    
    // MARK: - TableViewCell
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:)")
    }

}

