//
//  CircleMenuViewCell.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/5/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class CircleMenuViewCell: UICollectionViewCell {
    
    // Declarations
    
    lazy var circleView : UIView = {
        let view = UIView()
        view.backgroundColor = .red
        return view
    }()

    lazy var captionLbl : UILabel = {
        let label = UILabel()
        label.font = UIFont.lato(fontSize: 10)
        return label
    }()
    
    // MARK: - Methods
    
    func setup(caption: String, isSmall: Bool, isSelected: Bool) {
        
        addSubview(circleView)
        
        let whSize : CGFloat = (isSmall) ? 12 : 16
        let circleColor: UIColor = (isSelected) ? UIColor.white : UIColor.gray
        
        _ = circleView.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: whSize, heightConstant: whSize)
        _ = circleView.centralizeX(centerXAnchor)
        _ = circleView.centralizeY(centerYAnchor, constant: -5)
        
        circleView.backgroundColor = circleColor
        circleView.layer.cornerRadius = whSize/2
        
        addSubview(captionLbl)
        
        captionLbl.textColor = (isSelected) ? UIColor.white : UIColor.gray
        captionLbl.text = caption
        
        _ = captionLbl.anchor(circleView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 7, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = captionLbl.centralizeX(centerXAnchor)
        
    }
    
    // MARK: - Initializations

    override init(frame: CGRect) {
        super.init(frame: frame)

        //layer.borderColor = UIColor.blue.cgColor
        //layer.borderWidth = 1
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
