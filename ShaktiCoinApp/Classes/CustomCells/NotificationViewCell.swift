//
//  NotificationViewCell.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/2/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class NotificationViewCell: UICollectionViewCell {
 
    // MARK: - Declarations
    
    var ntfItem: NotificationItem? {
        didSet {
            
            guard let ntfItem = ntfItem else {
                return
            }
            
            title.text = ntfItem.title
            message.text = ntfItem.message
            date.text = ntfItem.date
        }
    }
    
    lazy var title : UILabel = {
        let label = UILabel()
        label.font = UIFont.lato(fontSize: 10, type: .Bold)
        label.textColor = UIColor.mainColor()
        return label
    }()
    
    lazy var message : UITextView = {
        let textView = UITextView()
        textView.backgroundColor = .clear
        textView.isEditable = false
        textView.showsVerticalScrollIndicator = false
        textView.font = UIFont.lato(fontSize: 9)
        textView.textColor = .white
        textView.textContainerInset = UIEdgeInsets.zero
        textView.textContainer.lineFragmentPadding = 0
        return textView
    }()
    
    lazy var date : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 9)
        return label
    }()
    
    // MARK: - Methods
    
    func setupViews() {

        addSubview(title)
        addSubview(message)
        addSubview(date)
        
        _ = title.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = message.anchor(title.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 191, heightConstant: 35)
        
        _ = date.anchor(topAnchor, left: nil, bottom: nil, right: rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0.0, y: frame.height, width: frame.width, height: 2.0)
        bottomLine.backgroundColor = UIColor.darkGray.cgColor
        layer.addSublayer(bottomLine)
    }
    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
