//
//  WelcomeViewCell.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 10/22/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class WelcomeViewCell: UICollectionViewCell {
    
    // MARK: - Welcome Page constructor
    
    var page: WelcomePage? {
        didSet {
            
            guard let page = page else {
                return
            }
            
            print(icon.frame)

            icon.image = UIImage(named: page.image)
            
            let attributedText = NSMutableAttributedString(string: page.title, attributes: [NSAttributedString.Key.font: UIFont.lato(fontSize: 24, type: .Light), NSAttributedString.Key.foregroundColor: UIColor.mainColor()])
            
            attributedText.append(NSAttributedString(string: "\n\n\(page.description)", attributes: [NSAttributedString.Key.font: UIFont.lato(fontSize: 14), NSAttributedString.Key.foregroundColor: UIColor.white]))
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = .center
            
            let length = attributedText.string.count
            attributedText.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: length))
            
            textView.attributedText = attributedText
        }
    }
    
    // MARK: - Declarations
       
    let icon : UIImageView = {
        let iv = UIImageView(frame: CGRect(x: 0, y: 0, width: 64, height: 64))
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        //iv.sizeToFit()
        return iv
    }()
    
    let pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.numberOfPages = 3
        pc.currentPage = 0
        //pc.addTarget(self, action: #selector(pageControlClicked), for: .touchUpInside)
        return pc
    }()

    let textView : UITextView = {
        let tv = UITextView()
        tv.backgroundColor = UIColor.clear
        tv.isEditable = false
        tv.contentInset = UIEdgeInsets(top: 24, left: 0, bottom: 0, right: 0)
        return tv
    }()
    
    let vvv : UIView = {
        let view = UIView()
        return view
    }()
    
    // MARK: - Methods
    
    func setupViews() {
        
        addSubview(icon)
        addSubview(textView)
        addSubview(pageControl)
 
        _ = icon.centralizeX(centerXAnchor)
        _ = icon.centralizeY(centerYAnchor, constant: -134)
        
        _ = pageControl.anchor(icon.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 35, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 16)
        _ = pageControl.centralizeX(centerXAnchor)
        
        _ = textView.anchor(icon.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 50, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: (frame.size.height * 0.2))
    }
    
    // MARK: - Initializations
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
