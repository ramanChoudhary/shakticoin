//
//  PaymentOptionsViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 9/17/19.
//  Copyright © 2019 Garnik Giloyan. All rights reserved.
//

import UIKit

class PaymentOptionsView: StackView, PaymentOptionsViewProtocol, MenuApplicable, UIGestureRecognizerDelegate {
    var presenter: PaymentOptionsPresenterProtocol?
    
    var licenses: [LicenseModel] = []
    var options: [LicenseModel] = []
    var activeLicensePlanCode: String?
    var selectedOption: Int = 1
    var selectedPlanType: String? {
        didSet {
            options = self.licenses.filter { $0.planType == selectedPlanType }
        }
    }
    
    var selectedSavingsOption: LicenseModel? {
        return options.first(where: { $0.modeType == .weekly })
    }
    
    private var optionButtons: [SMButton] = []
    
    @IBOutlet weak var optionsView: UIView!
    @IBOutlet weak var stairView: UIStackView!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var rightView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var planInfoView: UIView!
    @IBOutlet weak var planLicenseLabel: UILabel!
    @IBOutlet weak var planLicenseNameLabel: UILabel!
    
    @IBOutlet weak var planSubtitleLabel: UILabel!
    @IBOutlet weak var planPriceLabel: UILabel!
    @IBOutlet weak var planPriceValueLabel: UILabel!
    
    @IBOutlet weak var nextLicenseButton: UIButton!
    @IBOutlet weak var prevLicenseButton: UIButton!
    
    lazy var menuView: MenuView = {
        let view = MenuView()
        view.viewController = self
        view.currentMenuItem = .miner
        return view
    }()
    
    // MARK: - Declarations
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.setup(TextViewModel(style: TextStyle.latoLight24.alignment(.center), value: "Payment\nOptions"))
        return label
    }()
    
    lazy var descLabel: UILabel = {
        let label = UILabel()
        label.setup(TextViewModel(style: TextStyle.latoBold12.alignment(.center), value: "Choose the payment type that fits best for you.\nSwipe left or right to compare mining license fees."))
        return label
    }()
    
    private var optionMonthlySavingsLabel = UILabel()
    private var optionAnnualySavingsLabel = UILabel()
    
    lazy var costPlanWeekly: UILabel = {
        let label = UILabel()
        label.text = "Weekly"
        label.font = TextStyle.latoLight24.font
        label.textColor = .white
        return label
    }()
    
    lazy var costPlanMonthly: UILabel = {
        let label = UILabel()
        label.text = "Monthly"
        label.font = TextStyle.latoLight24.font
        label.textColor = .white
        return label
    }()
    
    lazy var costPlanAnnual: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = TextStyle.latoLight24.font
        label.text = "Annualy"
        return label
    }()
    
    private let actionButton = GoldButtonModule()

    // MARK: - ViewContoller
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prevLicenseButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        
        setupViews()
        setupNavBar()
        
        applyMenu()
        applyNotifications()
        
        let view = self.view!
        
        view.isUserInteractionEnabled = true
        
        var gesture = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction))
        gesture.direction = .right
        gesture.delegate = self
        view.addGestureRecognizer(gesture)
        
        gesture = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction))
        gesture.direction = .left
        gesture.delegate = self
        view.addGestureRecognizer(gesture)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        leftView.layer.cornerRadius = leftView.frame.width/2
        leftView.clipsToBounds = true
    }
    
    // MARK: - Methods
    
    private func setupViews() {
        addView(titleLabel, edgeInsets: .top(25))
        addView(descLabel, edgeInsets: .top(20))
        addView(optionsView, edgeInsets: UIEdgeInsets.horizontal(15))
        addView(planInfoView, edgeInsets: .top(15))
        
        addView(actionButton, edgeInsets: UIEdgeInsets.vertical(25).horizontal(15))
        
        setupOptionsView()
        
        onOptionButtonClick(optionButtons[0])
    }
    
    func setupOptionsView () {
        rightView.addSubview(costPlanWeekly)
        costPlanWeekly.anchorToLeft(rightView.leftAnchor)
        
        rightView.addSubview(costPlanMonthly)
        costPlanMonthly.anchorToLeft(rightView.leftAnchor)
        
        rightView.addSubview(costPlanAnnual)
        costPlanAnnual.anchorToLeft(rightView.leftAnchor)
        
        for (index, option) in options.enumerated() {
            
            let view = UIView()
            view.backgroundColor = .clear
            view.translatesAutoresizingMaskIntoConstraints = false
            view.heightAnchor.constraint(equalToConstant: 50).isActive = true
            view.widthAnchor.constraint(equalToConstant: 50).isActive = true
            stairView.addArrangedSubview(view)
            
            let optionButton = SMButton()
            optionButton.seqNo = index
            optionButton.setImage(UIImage(named: "Inactive"), for: .normal)
            optionButton.addTarget(self, action: #selector(onOptionButtonClick(_:)), for: .touchUpInside)
            view.addSubview(optionButton)
            optionButton.centralizeX(view.centerXAnchor)
            optionButton.centralizeY(view.centerYAnchor)
            optionButtons.append(optionButton)
            
            var label: UILabel?
            var savingsLabel: UILabel?
            
            if option.modeType == .weekly {
                label = costPlanWeekly
            } else if option.modeType == .monthly {
                label = costPlanMonthly
                savingsLabel = optionMonthlySavingsLabel
            } else if option.modeType == .yearly {
                label = costPlanAnnual
                savingsLabel = optionAnnualySavingsLabel
            }
            
            label?.centralizeY(view.centerYAnchor)
            
            let style = TextStyle.latoBold10.color(ColorStyle.gold.color)
            let model = TextViewModel(style: style, value: "")
            savingsLabel?.setup(model)
            if let savingsLabel = savingsLabel {
                rightView.addSubview(savingsLabel)
                savingsLabel.anchorToTop(label?.bottomAnchor)
                savingsLabel.anchorToLeft(label?.leftAnchor)
            }
        }
    }
    
    private func setActiveText(seqNo : Int) {
        guard selectedOption < options.count else { return }
        
        let option = options[selectedOption]
        
        planLicenseNameLabel.setup(TextViewModel(style: TextStyle.latoBold24.color(.white), value: option.licName))
        
        planLicenseLabel.setup(TextViewModel(style: TextStyle.latoLight24.color(.white), value: "License"))
        
        costPlanWeekly.font = TextStyle.latoLight24.font
        costPlanMonthly.font = TextStyle.latoLight24.font
        costPlanAnnual.font = TextStyle.latoLight24.font
        
        var duration = ""
        let style = TextStyle.latoBold10.color(ColorStyle.gold.color)
        switch option.modeType {
        case .monthly:
            duration = "Monthly"
            costPlanMonthly.font = TextStyle.latoBold24.font
        case .yearly:
            duration = "Annualy"
            costPlanAnnual.font = TextStyle.latoBold24.font
        default:
            duration = "Weekly"
            costPlanWeekly.font = TextStyle.latoBold24.font
        }
        
        if let option = selectedSavingsOption {
            var model = TextViewModel(style: style, value: "Save \(option.save.monthly)%")
            optionMonthlySavingsLabel.setup(model)
            optionMonthlySavingsLabel.isHidden = option.save.monthly == 0
            
            model = TextViewModel(style: style, value: "Save \(option.save.yearly)%")
            optionAnnualySavingsLabel.setup(model)
            optionAnnualySavingsLabel.isHidden = option.save.yearly == 0
        }
        
        planSubtitleLabel.setup(TextViewModel(style: TextStyle.lato14.color(.white).alignment(.center), value: "Paid for in weekly installments."))

        planPriceLabel.setup(TextViewModel(style: TextStyle.latoLight16.color(.white), value: "For only"))
        
        planPriceValueLabel.setup(TextViewModel(style: TextStyle.latoMedium18.color(.white), value: "$\(option.price) USD/\(duration)"))
        
        prevLicenseButton.isHidden = licenses.filter({ $0.planType < option.planType }).count == 0
        nextLicenseButton.isHidden = licenses.filter({ $0.planType > option.planType }).count == 0
        
        if option.planCode == activeLicensePlanCode {
            actionButton.populate(model: ButtonModuleModel(title: "Already Purchased", enabled: false, callback: nil))
        } else {
            actionButton.populate(model: ButtonModuleModel(title: "Pay with One-time fee", enabled: true, callback: { [weak self] in
                self?.onActionButtonClick()
            }))
        }
    }
    
    // MARK: - Events
    
    @objc private func onOptionButtonClick(_ button: SMButton) {
        if button.isActive { return }

        button.isActive = true
        button.setImage(UIImage(named: "Active"), for: .normal)
        for eachButton in optionButtons {
            if eachButton.seqNo != button.seqNo {
                eachButton.isActive = false
                eachButton.setImage(UIImage(named: "Inactive"), for: .normal)
            }
        }

        UIView.animate(withDuration: CATransaction.animationDuration()) {
            self.optionsView.layoutIfNeeded()
        }

        //changeTheOption(seqNo: button.seqNo)
        selectedOption = button.seqNo
        setActiveText(seqNo: button.seqNo)
    }
    
    private func onActionButtonClick() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ShaktiMinerPaymentViewController") as! ShaktiMinerPaymentViewController
        let navigationController = UINavigationController(rootViewController: vc)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navigationController
    }

    @objc private func onGoToWallet(_button : SCButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "M101LicenseViewController") as! M101LicenseViewController
        let navigationController = UINavigationController(rootViewController: vc)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navigationController
    }
    
    // MARK: - Navigation Bar
    private func setupNavBar() {
        
        navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationItem.title = "Shakti Miner"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.futura(fontSize: 18, type: .Medium)]
    }

    func populate(_ state: PaymentOptionsState) {
        hideLoader()
        switch state {
        case .error(let text):
            showAlert(errorMessage: text)
        case .showLoader:
            showLoader()
        }
    }
    
    @IBAction func prevLicense(_ sender: UIButton) {
        guard let selectedPlan = selectedPlanType else { return }
        let models = licenses.filter({ $0.planType < selectedPlan }).sorted(by: { $0.planType < $1.planType })
        if let newPlanType = models.last?.planType {
            selectedPlanType =  newPlanType
            setActiveText(seqNo: selectedOption)
            
            nextLicenseButton.isHidden = false
            let models = licenses.filter({ $0.planType < newPlanType })
            sender.isHidden = models.count == 0
        }
    }
    
    @IBAction func nextLicense(_ sender: UIButton) {
        guard let selectedPlan = selectedPlanType else { return }
        let models = licenses.filter({ $0.planType > selectedPlan }).sorted(by: { $0.planType < $1.planType })
        if let newPlanType = models.first?.planType {
            selectedPlanType =  newPlanType
            setActiveText(seqNo: selectedOption)
            
            prevLicenseButton.isHidden = false
            let models = licenses.filter({ $0.planType > newPlanType })
            sender.isHidden = models.count == 0
        }
    }
    
    @objc func swipeAction(_ sender: UISwipeGestureRecognizer) {
        switch sender.direction {
        case .left:
            nextLicense(nextLicenseButton)
        case .right:
            prevLicense(prevLicenseButton)
        default: break
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return true
    }
}
