//
//  PaymentOptionsInteractor.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/10/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class PaymentOptionsInteractor: PaymentOptionsInteractorInputProtocol {
    
    weak var presenter: PaymentOptionsInteractorOutputProtocol?
    var repository: PaymentOptionsRepositoryProtocol?
    
    init() {}

    // MARK: PaymentOptionsInteractorInputProtocol implementation
    func `do`(_ job: PaymentOptionsJob) {
        
    }
}
