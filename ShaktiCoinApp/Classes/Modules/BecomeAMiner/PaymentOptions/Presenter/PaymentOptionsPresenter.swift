//
//  PaymentOptionsPresenter.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/10/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class PaymentOptionsPresenter: PaymentOptionsPresenterProtocol, PaymentOptionsInteractorOutputProtocol {
    
    weak var view: PaymentOptionsViewProtocol?
    var interactor: PaymentOptionsInteractorInputProtocol?
    var wireframe: PaymentOptionsWireframeProtocol?
    
    func perform(_ action: PaymentOptionsViewAction) {
//        switch action {
//        case .load:
//            // Populate view components
//            break
//        }
    }
    
    func handle(_ result: PaymentOptionsInteractorResult) {
//        switch result {
//        default:
//            break
//        }
    }
}
