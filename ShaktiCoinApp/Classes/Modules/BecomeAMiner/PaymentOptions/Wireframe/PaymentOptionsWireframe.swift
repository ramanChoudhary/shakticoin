//
//  PaymentOptionsWireframe.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/10/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit

final class PaymentOptionsWireframe: PaymentOptionsWireframeProtocol {
    
    weak var view: UIViewController?
    
    static func createModule(options: [LicenseModel], planType: String,  activeLicensePlanCode: String?) -> UIViewController {
        // Generating module components
        let view = PaymentOptionsView()
        view.licenses = options
        view.selectedPlanType = planType
        view.activeLicensePlanCode = activeLicensePlanCode
        
        let presenter: PaymentOptionsPresenterProtocol & PaymentOptionsInteractorOutputProtocol = PaymentOptionsPresenter()
        let interactor: PaymentOptionsInteractorInputProtocol = PaymentOptionsInteractor()
        let APIDataManager: PaymentOptionsAPIDataManagerInputProtocol = PaymentOptionsAPIDataManager()
        let localDataManager: PaymentOptionsLocalDataManagerInputProtocol = PaymentOptionsLocalDataManager()
        let wireframe: PaymentOptionsWireframeProtocol = PaymentOptionsWireframe()
        let repository: PaymentOptionsRepositoryProtocol = PaymentOptionsRepository(APIDataManager, localDataManager)
        
        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireframe = wireframe
        presenter.interactor = interactor
        interactor.presenter = presenter
        interactor.repository = repository
        
        wireframe.view = view
        
        return UINavigationController(rootViewController: view)
    }
    
    func navigate(to page: PaymentOptionsPage) {
        switch page {
        case .close:
            close()
        }
    }
    
    private func close() {
        
    }
}
