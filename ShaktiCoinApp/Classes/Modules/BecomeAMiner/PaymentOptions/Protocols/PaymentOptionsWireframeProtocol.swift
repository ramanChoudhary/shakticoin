//
//  PaymentOptionsWireframeProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/10/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

enum PaymentOptionsPage {
    case close
}

protocol PaymentOptionsWireframeProtocol: class {
    
    var view: UIViewController? { get set }
    
    static func createModule(options: [LicenseModel], planType: String,  activeLicensePlanCode: String?) -> UIViewController
    /**
     * Add here your methods for communication PRESENTER -> WIREFRAME
     */
    func navigate(to page: PaymentOptionsPage)
}
