//
//  PaymentOptionsAPIDataManagerInputProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/10/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

protocol PaymentOptionsAPIDataManagerInputProtocol: class {
    /**
     * Add here your methods for communication REPOSITORY -> DATAMANAGER
     */
}
