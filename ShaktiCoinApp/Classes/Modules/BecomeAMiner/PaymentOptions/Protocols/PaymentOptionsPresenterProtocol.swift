//
//  PaymentOptionsPresenterProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/10/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

enum PaymentOptionsViewAction {
    case load
}

protocol PaymentOptionsPresenterProtocol: class {
    var view: PaymentOptionsViewProtocol? { get set }
    var interactor: PaymentOptionsInteractorInputProtocol? { get set }
    var wireframe: PaymentOptionsWireframeProtocol? { get set }
    /**
     * Add here your methods for communication VIEW -> PRESENTER
     */
    func perform(_ action: PaymentOptionsViewAction)
}
