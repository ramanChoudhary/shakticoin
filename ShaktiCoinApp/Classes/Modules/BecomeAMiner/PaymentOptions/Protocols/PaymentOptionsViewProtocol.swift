//
//  PaymentOptionsViewProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/10/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

enum PaymentOptionsState {
    case error(text: String)
    case showLoader
}

protocol PaymentOptionsViewProtocol: class {
    
    var presenter: PaymentOptionsPresenterProtocol? { get set }
    /**
     * Add here your methods for communication PRESENTER -> VIEW
     */
    func populate(_ state: PaymentOptionsState)
}
