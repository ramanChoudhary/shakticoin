//
//  PaymentOptionsInteractorInputProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/10/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

enum PaymentOptionsJob {
}

protocol PaymentOptionsInteractorInputProtocol: class {
    var presenter: PaymentOptionsInteractorOutputProtocol? { get set }
    var repository: PaymentOptionsRepositoryProtocol? { get set }
    
    /**
     * Add here your methods for communication PRESENTER -> INTERACTOR
     */
    func `do`(_ job: PaymentOptionsJob)
}
