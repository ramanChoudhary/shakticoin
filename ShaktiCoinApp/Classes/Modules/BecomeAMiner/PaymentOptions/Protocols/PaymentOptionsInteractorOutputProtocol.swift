//
//  PaymentOptionsInteractorOutputProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/10/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

enum PaymentOptionsInteractorResult {
}

protocol PaymentOptionsInteractorOutputProtocol: class {
    /**
     * Add here your methods for communication INTERACTOR -> PRESENTER
     */
    func handle(_ result: PaymentOptionsInteractorResult)
}
