//
//  PaymentOptionsRepository.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/10/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class PaymentOptionsRepository: PaymentOptionsRepositoryProtocol {
    private var apiDataManager: PaymentOptionsAPIDataManagerInputProtocol?
    private var localDataManager: PaymentOptionsLocalDataManagerInputProtocol?
    
    init(_ apiDataManager: PaymentOptionsAPIDataManagerInputProtocol?, _ localDataManager: PaymentOptionsLocalDataManagerInputProtocol?) {
        self.apiDataManager = apiDataManager
        self.localDataManager = localDataManager
    }
}
