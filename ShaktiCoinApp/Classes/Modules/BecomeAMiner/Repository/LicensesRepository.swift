//
//  LicensesRepository.swift
//  ShaktiCoinApp
//
//  Created by sperev on 30/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

protocol LicensesRepositoryProtocol {
    func minerLicenses(callback: @escaping (Result<[LicenseModel], ErrorModel>) -> Void)
    func nodeOperator(callback: @escaping (Result<NodeOperatorModel?, ErrorModel>) -> Void)
}

class LicensesRepository: LicensesRepositoryProtocol {
    var dataManager: LicensesDataManagerProtocol?
    
    func minerLicenses(callback: @escaping (Result<[LicenseModel], ErrorModel>) -> Void) {
        dataManager?.minerLicenses(callback: {
            switch $0 {
            case .success(let array):
                let models = Mapper<LicenseModel>.map(array: array)
                callback(.success(models))
            case .failure(let error):
                callback(.failure(error))
            }
        })
    }
    
    func nodeOperator(callback: @escaping (Result<NodeOperatorModel?, ErrorModel>) -> Void) {
        dataManager?.nodeOperator(callback: {
            switch $0 {
            case .success(let object):
                let model = try? Mapper<NodeOperatorModel>.map(object: object)
                callback(.success(model))
            case .failure(let error): callback(.failure(error))
            }
        })
    }
}
