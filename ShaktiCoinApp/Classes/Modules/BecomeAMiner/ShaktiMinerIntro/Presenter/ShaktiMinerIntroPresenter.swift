//  Copyright © 2020 ShaktiCoinApp. All rights reserved.

import UIKit

protocol ShaktiMinerIntroPresenting: AnyObject {
  func onGoToMiningRegistrationPressed()
}

final class ShaktiMinerIntroPresenter {
  private weak var view: ShaktiMinerIntroView?
  var interactor: ShaktiMinerIntroInteracting?
  private let router: ShaktiMinerIntroRouting
  private let planCode = "M101W" // TODO: Jazz: Remove these hardcoded values
  private let address = AddressModel(
    country: "India",
    countryCode: "IN",
    province: "Tamil Nadu",
    city: "Chennai",
    street: "101",
    zipCode: "600001"
  )

  init(interface: ShaktiMinerIntroView, interactor: ShaktiMinerIntroInteracting?, router: ShaktiMinerIntroRouting) {
        self.view = interface
        self.interactor = interactor
        self.router = router
  }
}

extension ShaktiMinerIntroPresenter: ShaktiMinerIntroPresenting {
    func onGoToMiningRegistrationPressed() {
        self.router.showPowerMinerScreen()
    }
}
