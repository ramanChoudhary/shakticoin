//  Copyright © 2020 ShaktiCoinApp. All rights reserved.

import UIKit

protocol ShaktiMinerIntroRouting: AnyObject {
  func showPowerMinerScreen()
}

final class ShaktiMinerIntroRouter {
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = ShaktiMinerIntroViewController()
        let interactor = ShaktiMinerIntroInteractor()
        let router = ShaktiMinerIntroRouter()
        let presenter = ShaktiMinerIntroPresenter(interface: view, interactor: interactor, router: router)

        view.presenter = presenter
        router.viewController = view

        return view
      
    }
}

extension ShaktiMinerIntroRouter: ShaktiMinerIntroRouting {
    func showPowerMinerScreen() {
        let vc = PowerMinerRouter.createModule()
        let navigationController = UINavigationController(rootViewController: vc)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navigationController
    }
}
