//  Copyright © 2020 ShaktiCoinApp. All rights reserved.

import UIKit

protocol ShaktiMinerIntroView: AnyObject {
  var presenter: ShaktiMinerIntroPresenting? { get set }
}

class ShaktiMinerIntroViewController: BaseScrollViewController, ShaktiMinerIntroView {
  var presenter: ShaktiMinerIntroPresenting?

  // MARK: - Declarations

  lazy var becomeMinerTextView: SCAttributedTextView = {
    let textView = SCAttributedTextView()
    textView.setup(textInfo: "Become a Shakti Miner", textColor: .white, font: UIFont.lato(fontSize: 24, type: .Light))
    textView
      .setText(
        textContent: "\n\nEarn an additional income.",
        textColor: .white,
        font: UIFont.lato(fontSize: 12, type: .Bold)
      )
    textView.finilizeWithCenterlize()
    return textView
  }()

  lazy var licenseInfo: SCAttributedTextView = {
    let textView = SCAttributedTextView()
    textView
      .setup(
        textInfo: "A licensed miner earns income ",
        textColor: .white,
        font: UIFont.lato(fontSize: 14, type: .Bold)
      )
    textView
      .setText(
        textContent: "by mining Shakti coins. We will provide you with a mining software application to access the SXE Network to start mining.",
        textColor: .white,
        font: UIFont.lato(fontSize: 14)
      )
    textView
      .setText(
        textContent: "\n\nSign up now to maximize your success in the new digital currency world. There are limited Power Mining slots, and they will go fast!",
        textColor: .white,
        font: UIFont.lato(fontSize: 14)
      )
    textView
      .setText(
        textContent: "\n\nGet an extra SXE 10,000 by upgrading now!",
        textColor: .white,
        font: UIFont.lato(fontSize: 14, type: .Bold)
      )
    textView
      .setText(
        textContent: "\n\nM101 License Features:",
        textColor: .white,
        font: UIFont.lato(fontSize: 14, type: .Bold)
      )
    textView
      .setText(
        textContent: "\n\n• Mine Shakti Coins from anywhere.",
        textColor: .white,
        font: UIFont.lato(fontSize: 14)
      )
    textView
      .setText(
        textContent: "\n• No business registration is required.",
        textColor: .white,
        font: UIFont.lato(fontSize: 14)
      )
    textView.finilize()
    return textView
  }()

  lazy var forOnlyPrice: SCAttributedTextView = {
    let textView = SCAttributedTextView()
    textView.setup(textInfo: "For only ", textColor: .white, font: UIFont.lato(fontSize: 16, type: .Light))
    textView.setText(textContent: "$49.95 USD", textColor: .white, font: UIFont.lato(fontSize: 18, type: .Bold))
    textView.finilize()
    return textView
  }()

    lazy var gotoMyWallet: SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Cool, take me to mining registration!", for: .normal)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        button.addTarget(self, action: #selector(onGoToMiningRegistration(_button:)), for: .touchUpInside)
        return button
  }()

  lazy var noMining: UILabel = {
    let label = UILabel()
    label.text = "No, I don’t want to earn a mining income"
    label.font = UIFont.lato(fontSize: 14)
    label.textColor = .white
    label.underline()
    return label
  }()

  lazy var scrollArrowIcon: UIImageView = {
    let view = UIImageView()
    view.image = UIImage(named: "arrowDown")
    view.tintColor = UIColor.mainColor()
    return view
  }()

  lazy var lic101Container: UIView = {
    let view = UIView()
    view.backgroundColor = .black
    return view
  }()

  lazy var lic101TextView: SCAttributedTextView = {
    let textView = SCAttributedTextView()
    textView
      .setup(
        textInfo: "The M101 license allows you to mine SXE coins. You must provide the hardware and connectivity necessary to mine coins. Refer to the Terms and Conditions of the Shakti Network and the Bonus Bounty Offer before you sign up.\n\nYour Bonus Bounty varies depending on the type of business you own or are planning to start in the near future. Your bonus capital could range from $5,000 to $125,000 USD in equivalent SXE for early adopters.",
        textColor: .white,
        font: .lato(fontSize: 10)
      )
    textView.finilize()
    return textView
  }()

  // MARK: - Methods

  func initViews() {

    let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
      (navigationController?.navigationBar.frame.height ?? 0.0)

    containerView.addSubview(becomeMinerTextView)
    _ = becomeMinerTextView
      .anchor(
        containerView.topAnchor,
        left: nil,
        bottom: nil,
        right: nil,
        topConstant: topBarHeight + 25,
        leftConstant: 0,
        bottomConstant: 0,
        rightConstant: 0,
        widthConstant: 250,
        heightConstant: 80
      )
    _ = becomeMinerTextView.centralizeX(containerView.centerXAnchor)

    containerView.addSubview(licenseInfo)
    _ = licenseInfo
      .anchor(
        becomeMinerTextView.bottomAnchor,
        left: nil,
        bottom: nil,
        right: nil,
        topConstant: 5,
        leftConstant: 0,
        bottomConstant: 0,
        rightConstant: 0,
        widthConstant: 320,
        heightConstant: 300
      )
    _ = licenseInfo.centralizeX(containerView.centerXAnchor)

    containerView.addSubview(forOnlyPrice)
    _ = forOnlyPrice
      .anchor(
        licenseInfo.bottomAnchor,
        left: nil,
        bottom: nil,
        right: nil,
        topConstant: 10,
        leftConstant: 0,
        bottomConstant: 0,
        rightConstant: 0,
        widthConstant: 185,
        heightConstant: 45
      )
    _ = forOnlyPrice.centralizeX(containerView.centerXAnchor)

    containerView.addSubview(gotoMyWallet)
    _ = gotoMyWallet
      .anchor(
        forOnlyPrice.bottomAnchor,
        left: nil,
        bottom: nil,
        right: nil,
        topConstant: 15,
        leftConstant: 0,
        bottomConstant: 0,
        rightConstant: 0,
        widthConstant: 280,
        heightConstant: 36
      )
    _ = gotoMyWallet.centralizeX(containerView.centerXAnchor)

    containerView.addSubview(noMining)
    _ = noMining
      .anchor(
        gotoMyWallet.bottomAnchor,
        left: nil,
        bottom: nil,
        right: nil,
        topConstant: 15,
        leftConstant: 0,
        bottomConstant: 0,
        rightConstant: 0,
        widthConstant: 0,
        heightConstant: 0
      )
    _ = noMining.centralizeX(containerView.centerXAnchor)

    containerView.addSubview(scrollArrowIcon)
    _ = scrollArrowIcon
      .anchor(
        noMining.bottomAnchor,
        left: nil,
        bottom: nil,
        right: nil,
        topConstant: 15,
        leftConstant: 0,
        bottomConstant: 0,
        rightConstant: 0,
        widthConstant: 0,
        heightConstant: 0
      )
    _ = scrollArrowIcon.centralizeX(containerView.centerXAnchor)

    containerView.addSubview(lic101Container)

    _ = lic101Container
      .anchor(
        scrollArrowIcon.bottomAnchor,
        left: nil,
        bottom: nil,
        right: nil,
        topConstant: 15,
        leftConstant: 0,
        bottomConstant: 0,
        rightConstant: 0,
        widthConstant: 400,
        heightConstant: 150
      )
    _ = lic101Container.centralizeX(containerView.centerXAnchor)

    lic101Container.addSubview(lic101TextView)
    _ = lic101TextView
      .anchor(
        lic101Container.topAnchor,
        left: lic101Container.leftAnchor,
        bottom: lic101Container.bottomAnchor,
        right: lic101Container.rightAnchor,
        topConstant: 15,
        leftConstant: 25,
        bottomConstant: 15,
        rightConstant: 30,
        widthConstant: 0,
        heightConstant: 0
      )
    _ = lic101Container.centralizeX(lic101Container.centerXAnchor)
    _ = lic101Container.centralizeY(lic101Container.centerYAnchor)
  }

  // MARK: - Events

    @objc private func onGoToMiningRegistration(_button: SCButton) {
        presenter?.onGoToMiningRegistrationPressed()
    }

  // MARK: - ViewContoller

  override func viewDidLoad() {
    super.viewDidLoad()

    initViews()
    initNavBar()
  }

  // MARK: - Navigation Bar

  private func initNavBar() {
    navigationItem.title = "Shakti Miner"
  }
}
