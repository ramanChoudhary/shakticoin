//
//  ShaktiMinerPaymentViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/16/19.
//  Copyright © 2019 Garnik Giloyan. All rights reserved.
//

import UIKit

class ShaktiMinerPaymentViewController: BaseScrollViewController {

    // MARK: - Declarations
    
    lazy var infoTextView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        
        textView.setup(textInfo: "Power Miner", textColor: .white, font: .lato(fontSize: 24, type: .Light))
        textView.setText(textContent: "\nMover. Shaker. Digital Banker.", textColor: .white, font: .lato(fontSize: 24, type: .Light))
        
        textView.setText(textContent: "\n\nFinish the payment to complete registration.", textColor: .white, font: .lato(fontSize: 12, type: .Bold))
        
        textView.setText(textContent: "\n\n\nM101 ", textColor: .white, font: .lato(fontSize: 24, type: .Bold))
        textView.setText(textContent: "License", textColor: .white, font: .lato(fontSize: 24, type: .Light))
        textView.setText(textContent: "\nWeekly", textColor: .white, font: .lato(fontSize: 12, type: .Bold))
        
        textView.setText(textContent: "\n\n\n$XX.XX USD/Week", textColor: .white, font: .lato(fontSize: 18, type: .Bold)) //TODO Medium
        textView.finilizeWithCenterlize()
        
        return textView
    }()
    
    lazy var ccTextField : SCXTextField = {
        let textField = SCXTextField()
        textField.CornerRadius = 3
        textField.attributedPlaceholder = Util.NSAttrString(text: "Card Number", font: .lato(fontSize: 14), color: .white)
        textField.font = UIFont.lato(fontSize: 14)
        textField.textColor = .white
        textField.keyboardType = .numberPad
        return textField
    }()
    
    lazy var expirationTextField : SCXTextField = {
        let textField = SCXTextField()
        textField.CornerRadius = 3
        textField.attributedPlaceholder = Util.NSAttrString(text: "MM/YY", font: .lato(fontSize: 14), color: .white)
        textField.font = UIFont.lato(fontSize: 14)
        textField.textColor = .white
        return textField
    }()
    
    lazy var cvvTextField : SCXTextField = {
        let textField = SCXTextField()
        textField.CornerRadius = 3
        textField.attributedPlaceholder = Util.NSAttrString(text: "CVV", font: .lato(fontSize: 14), color: .white)
        textField.font = UIFont.lato(fontSize: 14)
        textField.textColor = .white
        return textField
    }()
    
    lazy var zipTextField : SCXTextField = {
        let textField = SCXTextField()
        textField.CornerRadius = 3
        textField.attributedPlaceholder = Util.NSAttrString(text: "ZIP", font: .lato(fontSize: 14), color: .white)
        textField.font = UIFont.lato(fontSize: 14)
        textField.textColor = .white
        return textField
    }()
    
    lazy var payByStripe : SCButton = {
        let button = SCButton()
        button.CornerRadius = 15
        button.setTitle("Pay by STRIPE", for: .normal)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        button.addTarget(self, action: #selector(onPayStripe), for: .touchUpInside)
        return button
    }()
    
    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("Cancel", for: .normal)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        return button
    }()

    // MARK: - Methods
    private func initUI() {

        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)

        containerView.addSubview(infoTextView)
        containerView.addSubview(ccTextField)
        containerView.addSubview(expirationTextField)
        containerView.addSubview(cvvTextField)
        containerView.addSubview(zipTextField)
        containerView.addSubview(payByStripe)
        containerView.addSubview(cancelButton)
        
        _ = infoTextView.anchor(containerView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: topBarHeight + 40, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 303, heightConstant: 280)
        _ = infoTextView.centralizeX(containerView.centerXAnchor)
        
        _ = ccTextField.anchor(infoTextView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 35, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 40)
        _ = ccTextField.centralizeX(containerView.centerXAnchor)
        
        _ = expirationTextField.anchor(ccTextField.bottomAnchor, left: ccTextField.leftAnchor, bottom: nil, right: nil, topConstant: 7, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 100, heightConstant: 40)
        
        _ = zipTextField.anchor(ccTextField.bottomAnchor, left: nil, bottom: nil, right: ccTextField.rightAnchor, topConstant: 7, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 100, heightConstant: 40)
        
        _ = cvvTextField.anchor(ccTextField.bottomAnchor, left: expirationTextField.rightAnchor, bottom: nil, right: nil, topConstant: 7, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 100, heightConstant: 40)
        
        _ = payByStripe.anchor(cvvTextField.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 45, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 36)
        
        _ = payByStripe.centralizeX(containerView.centerXAnchor)
        
        _ = cancelButton.anchor(payByStripe.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = cancelButton.centralizeX(containerView.centerXAnchor)
    }
    
    // MARK: - Events
    
    @objc private func onPayStripe() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "BusinessVaultIntroViewController") as! BusinessVaultIntroViewController
        let navigationController = UINavigationController(rootViewController: vc)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navigationController
    }
    
    // MARK: - NavBar
    private func initNavBar() {
        self.navigationItem.title = "Shakti Miner"
    }
    
    // MARK: - ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        initNavBar()
        initUI()
    }
    
    override func viewDidLayoutSubviews() {
        ccTextField.layer.cornerRadius = 3
        ccTextField.layer.borderColor = UIColor.mainColor().cgColor
        ccTextField.layer.borderWidth = 1
        
        expirationTextField.layer.cornerRadius = 3
        expirationTextField.layer.borderColor = UIColor.mainColor().cgColor
        expirationTextField.layer.borderWidth = 1

        cvvTextField.layer.cornerRadius = 3
        cvvTextField.layer.borderColor = UIColor.mainColor().cgColor
        cvvTextField.layer.borderWidth = 1
        
        zipTextField.layer.cornerRadius = 3
        zipTextField.layer.borderColor = UIColor.mainColor().cgColor
        zipTextField.layer.borderWidth = 1
    }
}
