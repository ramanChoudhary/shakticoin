//
//  M101LicenseViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/14/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class M101LicenseViewController: UIViewController {

    // MARK: - Declarations

    lazy var backgroundImage: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "MinerImage")
        //iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    lazy var coverView: UIView = {
        let cv = UIView()
        cv.contentMode = .scaleAspectFill
        cv.alpha = 0.80
        cv.backgroundColor = .black
        return cv
    }()
    
    lazy var containerView: UIView = {
        let view = UIScrollView()
        view.contentSize.height = 700
        return view
    }()
    
    lazy var m101License : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.setup(textInfo: "You Own a M101 License", textColor: .white, font: UIFont.lato(fontSize: 24, type: .Light))
        textView.setText(textContent: "\n\nYou now have a M101 Basic Miner account.", textColor: .white, font: UIFont.lato(fontSize: 12, type: .Bold))
        textView.setText(textContent: "\n\nCongratulations! You chose to become a Shakti Miner. You're on your way to financial success.", textColor: .white, font: UIFont.lato(fontSize: 12))
        textView.finilizeWithCenterlize()
        return textView
    }()
    
    lazy var nextStepsLbl : UILabel = {
        let label = UILabel()
        label.text = "Next Steps:"
        label.font = UIFont.lato(fontSize: 16, type: .Light)
        label.textColor = .white
        return label
    }()
    
    lazy var unlockViewCircle : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.mainColor()
        return view
    }()
    
    lazy var installAppCircle : UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    lazy var yourAccountCircle : UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    lazy var startMiningCircle : UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    lazy var unlockWalletTextView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.setup(textInfo: "Unlock your Wallet", textColor: .white, font: UIFont.lato(fontSize: 16))
        textView.setLink(key: "_unlockWallet", linkedText: "Wallet")
        textView.setLinkStyles(linkColor: .white, underlineColor: .white)
        textView.finilize()
        return textView
    }()
    
    lazy var installAppTextView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.setup(textInfo: "Install the Shakti Mining App", textColor: .white, font: UIFont.lato(fontSize: 16))
        textView.setLink(key: "_installApp", linkedText: "Shakti Mining App")
        textView.setLinkStyles(linkColor: .white, underlineColor: .white)
        textView.finilize()
        return textView
    }()
    
    lazy var accountTextView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.setup(textInfo: "Configure your account", textColor: .white, font: UIFont.lato(fontSize: 16))
        textView.finilize()
        return textView
    }()
    
    lazy var startMiningTextView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.setup(textInfo: "Start mining!", textColor: .white, font: UIFont.lato(fontSize: 16))
        textView.finilize()
        return textView
    }()
    
    lazy var orAlternativeTitle : SCLineTitle = {
        let view = SCLineTitle()
        view.Title = "Or alternaively"
        return view
    }()
    
    lazy var powerMinerButton : SCButton = {
        let button = SCButton()
        button.CornerRadius = 18
        button.setTitle("Become a Power Miner", for: .normal)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
        button.addTarget(self, action: #selector(onBecomeMiner), for: .touchUpInside)
        return button
    }()

    lazy var scrollArrowIcon : UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "arrowDown")
        view.tintColor = UIColor.mainColor()
        return view
    }()
    
    lazy var transformTheWorld : SCAttributedTextView = {
        let view = SCAttributedTextView()
        view.setup(textInfo: "Want to be a Power Miner and", textColor: .white, font: UIFont.lato(fontSize: 18))
        view.setText(textContent: "\nTransform the World?", textColor: .white, font: UIFont.lato(fontSize: 18))
        view.finilizeWithCenterlize()
        return view
    }()
    
    lazy var additionalBonusContainer : UIView = {
        let view = UIView()
        view.backgroundColor = .black
        return view
    }()
    
    lazy var additionalBonusTextView : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.setup(textInfo: "Get an additional bonus bounty and start developing your exclusive territory.", textColor: .white, font: .lato(fontSize: 14))
        textView.setText(textContent: "\n\n\nYou must submit a business registration and activate your SXE Wallet to become a Power Miner.", textColor: .white, font: .lato(fontSize: 10))
        textView.finilizeWithCenterlize()
        return textView
    }()

    // MARK: - Methods
    
    private func setupBaseViews() {
        
        view.addSubview(backgroundImage)
        view.addSubview(coverView)
        view.addSubview(containerView)
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = coverView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = containerView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: topBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        setupViews()
    }
    
    func setupViews() {
        
        containerView.addSubview(m101License)
        _ = m101License.anchor(containerView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 25, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 300, heightConstant: 150)
        _ = m101License.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(nextStepsLbl)
        
        _ = nextStepsLbl.anchor(m101License.bottomAnchor, left: m101License.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        containerView.addSubview(unlockViewCircle)
        _ = unlockViewCircle.anchor(nextStepsLbl.bottomAnchor, left: nextStepsLbl.leftAnchor, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 20, heightConstant: 20)
        
        let checkImage = UIImageView()
        checkImage.image = UIImage(named: "CheckmarkSmall")
        checkImage.tintColor = .white
        
        unlockViewCircle.addSubview(checkImage)
        
        _ = checkImage.centralizeX(unlockViewCircle.centerXAnchor)
        _ = checkImage.centralizeY(unlockViewCircle.centerYAnchor)
        
        containerView.addSubview(unlockWalletTextView)
        _ = unlockWalletTextView.anchor(unlockViewCircle.topAnchor, left: unlockViewCircle.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 160, heightConstant: 20)
        
        containerView.addSubview(installAppCircle)
        _ = installAppCircle.anchor(unlockViewCircle.bottomAnchor, left: nextStepsLbl.leftAnchor, bottom: nil, right: nil, topConstant: 12, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 20, heightConstant: 20)
        
        containerView.addSubview(installAppTextView)
        _ = installAppTextView.anchor(installAppCircle.topAnchor, left: installAppCircle.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 160, heightConstant: 20)
        
        containerView.addSubview(yourAccountCircle)
        _ = yourAccountCircle.anchor(installAppCircle.bottomAnchor, left: nextStepsLbl.leftAnchor, bottom: nil, right: nil, topConstant: 12, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 20, heightConstant: 20)
        
        containerView.addSubview(accountTextView)
        _ = accountTextView.anchor(yourAccountCircle.topAnchor, left: yourAccountCircle.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 180, heightConstant: 20)
        
        containerView.addSubview(startMiningCircle)
        _ = startMiningCircle.anchor(yourAccountCircle.bottomAnchor, left: nextStepsLbl.leftAnchor, bottom: nil, right: nil, topConstant: 12, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 20, heightConstant: 20)
        
        containerView.addSubview(startMiningTextView)
        _ = startMiningTextView.anchor(startMiningCircle.topAnchor, left: startMiningCircle.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 160, heightConstant: 20)
        
        containerView.addSubview(orAlternativeTitle)
        
        _ = orAlternativeTitle.anchor(startMiningCircle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 25, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 330, heightConstant: 24)
        _ = orAlternativeTitle.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(transformTheWorld)
        
        _ = transformTheWorld.anchor(orAlternativeTitle.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 250, heightConstant: 50)
        _ = transformTheWorld.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(powerMinerButton)
        _ = powerMinerButton.anchor(transformTheWorld.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 25, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 280, heightConstant: 36)
        _ = powerMinerButton.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(scrollArrowIcon)
        _ = scrollArrowIcon.anchor(powerMinerButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = scrollArrowIcon.centralizeX(containerView.centerXAnchor)
        
        containerView.addSubview(additionalBonusContainer)
        
        _ = additionalBonusContainer.anchor(scrollArrowIcon.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 400, heightConstant: 150)
        _ = additionalBonusContainer.centralizeX(containerView.centerXAnchor)
        
        additionalBonusContainer.addSubview(additionalBonusTextView)
        _ = additionalBonusTextView.anchor(additionalBonusContainer.topAnchor, left: additionalBonusContainer.leftAnchor, bottom: additionalBonusContainer.bottomAnchor, right: additionalBonusContainer.rightAnchor, topConstant: 15, leftConstant: 25, bottomConstant: 15, rightConstant: 30, widthConstant: 0, heightConstant: 0)
        
        _ = additionalBonusTextView.centralizeX(additionalBonusContainer.centerXAnchor)
        _ = additionalBonusTextView.centralizeY(additionalBonusContainer.centerYAnchor)
    }
    
    // MARK: - ViewContoller
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupBaseViews()
        setupNavBar()
        
        applyMenu()
        applyNotifications()
    }
    
    override func viewDidLayoutSubviews() {
        unlockViewCircle.layer.cornerRadius = 10
        unlockViewCircle.clipsToBounds = true
        
        installAppCircle.layer.cornerRadius = 10
        installAppCircle.layer.borderColor = UIColor.mainColor().cgColor
        installAppCircle.layer.borderWidth = 0.8
        
        yourAccountCircle.layer.cornerRadius = 10
        yourAccountCircle.layer.borderColor = UIColor.mainColor().cgColor
        yourAccountCircle.layer.borderWidth = 0.8
        
        startMiningCircle.layer.cornerRadius = 10
        startMiningCircle.layer.borderColor = UIColor.mainColor().cgColor
        startMiningCircle.layer.borderWidth = 0.8
        
        orAlternativeTitle.update()
    }
    
    // MARK: - Navigation Bar
    
    private func setupNavBar() {
        navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationItem.title = "Shakti Miner"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.futura(fontSize: 18, type: .Medium)]
    }
    
    @objc private func onBecomeMiner() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PowerMinerViewController") as! PowerMinerViewController
        let navigationController = UINavigationController(rootViewController: vc)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navigationController
    }
}
