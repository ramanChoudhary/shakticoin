//
//  LicensesDataManager.swift
//  ShaktiCoinApp
//
//  Created by sperev on 30/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

protocol LicensesDataManagerProtocol {
    func minerLicenses(callback: @escaping (Result<JSONArray, ErrorModel>) -> Void)
    func nodeOperator(callback: @escaping (Result<JSONObject, ErrorModel>) -> Void)
}

class LicensesDataManager: LicensesDataManagerProtocol {
    var dataSource = RemoteDataSource.shared
    func minerLicenses(callback: @escaping (Result<JSONArray, ErrorModel>) -> Void) {
        var parameters: JSONObject = [:]
        parameters = [:]
        dataSource.requestArray(forEndpoint: Endpoint.LicenseEndpoint.miningLicenses, parameters: parameters, result: callback)
    }
    
    func nodeOperator(callback: @escaping (Result<JSONObject, ErrorModel>) -> Void) {
        var parameters: JSONObject = [:]
        parameters = [:]
        dataSource.requestObject(forEndpoint: Endpoint.LicenseEndpoint.nodeOperator, parameters: parameters, result: callback)
    }
}
