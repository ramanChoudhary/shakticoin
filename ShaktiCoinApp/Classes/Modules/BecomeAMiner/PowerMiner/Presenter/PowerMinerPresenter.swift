//  Copyright © 2020 ShaktiCoinApp. All rights reserved.

import UIKit

enum PowerMinerAction {
    case viewDidLoad
    case upgrageTo(_ planType: String)
}

protocol PowerMinerPresenting: AnyObject {
    func perform(_ action: PowerMinerAction)
}

final class PowerMinerPresenter: PowerMinerPresenting {
    private weak var view: PowerMinerView?
    var interactor: PowerMinerInteracting?
    private let router: PowerMinerRouting
    private var models: [LicenseModel] = []
    private var model: NodeOperatorModel?
    init(interface: PowerMinerView, interactor: PowerMinerInteracting?, router: PowerMinerRouting) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }
    
    func perform(_ action: PowerMinerAction) {
        switch  action {
        case .viewDidLoad:
            view?.populate(.loader(true))
            fetchPurchasedLicense()
        case .upgrageTo(let planType):
            router.openPaymentOptionsScreen(models, planType: planType, activeLicensePlanCode: model?.activeLicense?.planCode)
        }
    }
    
    private func fetchPurchasedLicense() {
        interactor?.fetchNodeOperator { [weak self] result in
            switch result {
            case .success(let model):
                self?.model = model
                self?.fetchLicences()
            case .failure(let model):
                ViewDispatcher.shared.execute {
                    self?.view?.populate(.loader(false))
                    self?.view?.populate(.error(model.localizedDescription))
                }
            }
        }
    }
    
    private func fetchLicences() {
        interactor?.fetchLicenses { [weak self] result in
            switch result {
            case .success(let models):
                self?.models = models
                var grouped: [LicenseModel] = []
                var activePlanType: String?
                for item in models { // weekly prices
                    if item.planCode == self?.model?.activeLicense?.planCode {
                        activePlanType = item.planType
                    }
                    if item.modeType == .weekly, !grouped.contains(where: { $0.planType == item.planType }) {
                        grouped.append(item)
                    }
                }
                
                let plans = grouped.map { LicenseViewModel(model: $0, active: $0.planType == activePlanType) }
                ViewDispatcher.shared.execute {
                    self?.view?.populate(.loader(false))
                    self?.view?.populate(.viewModel(PowerMinerViewModel(plans: plans)))
                }
                
            case .failure(let model):
                ViewDispatcher.shared.execute {
                    self?.view?.populate(.loader(false))
                    self?.view?.populate(.error(model.localizedDescription))
                }
            }
        }
    }
}
