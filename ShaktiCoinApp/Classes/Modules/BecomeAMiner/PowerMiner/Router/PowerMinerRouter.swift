//  Copyright © 2020 ShaktiCoinApp. All rights reserved.

import UIKit

protocol PowerMinerRouting: AnyObject {
    func openPaymentOptionsScreen(_ licences: [LicenseModel], planType: String, activeLicensePlanCode: String?)
}

final class PowerMinerRouter {
    weak var viewController: UIViewController?

    static func createModule() -> UIViewController {
        // Change to get view from storyboard if not using xib
        let bundle = Bundle(for: PowerMinerViewController.self)
        let view = PowerMinerViewController(nibName: "PowerMinerViewController", bundle: bundle)
        let repository = LicensesRepository()
        repository.dataManager = LicensesDataManager()
        let interactor = PowerMinerInteractor(licenseRepository: repository)
        let router = PowerMinerRouter()
        let presenter = PowerMinerPresenter(interface: view, interactor: interactor, router: router)

        view.presenter = presenter
        router.viewController = view

        return view
    }
}

extension PowerMinerRouter: PowerMinerRouting {
    func openPaymentOptionsScreen(_ options: [LicenseModel], planType: String, activeLicensePlanCode: String?) {
        let navigationController = PaymentOptionsWireframe.createModule(options: options, planType: planType, activeLicensePlanCode: activeLicensePlanCode)
        AppDelegate.shared?.window?.rootViewController = navigationController
    }
}
