//
//  LicenseViewModel.swift
//  ShaktiCoinApp
//
//  Created by sperev on 13/10/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

struct LicenseViewModel {
    var name: String
    var price: Double
    var bonus: String
    var features: [String]
    var planType: String
    var active: Bool
    var licenseId: String
    
    init(model: LicenseModel, active: Bool) {
        name = model.licName
        price = model.price
        bonus = model.bonus
        features = model.licFeatures
        planType = model.planType
        licenseId = model.licenseId
        self.active = active
    }
}
