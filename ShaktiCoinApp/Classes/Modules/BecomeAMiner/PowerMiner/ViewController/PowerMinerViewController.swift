//
//  PowerMinerViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/15/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

enum PowerMinerViewState {
    case viewModel(PowerMinerViewModel)
    case error(String)
    case loader(Bool)
}

protocol PowerMinerView: AnyObject {
    var presenter: PowerMinerPresenting? { get set }
    func populate(_ state: PowerMinerViewState)
}

final class PowerMinerViewController: BaseScrollViewController, PowerMinerView {
    // MARK: - Declarations
    var presenter: PowerMinerPresenting?

    lazy var titleView: SCAttributedTextView = {
      let textView = SCAttributedTextView()
      textView.setup(textInfo: "Power Miner", textColor: UIColor.white, font: UIFont.lato(fontSize: 24, type: .Light))
      textView
        .setText(
          textContent: "\nMover. Shaker. Digital Banker.",
          textColor: UIColor.white,
          font: UIFont.lato(fontSize: 24, type: .Light)
        )
      textView
        .setText(
          textContent: "\n\nChoose your license type:",
          textColor: UIColor.white,
          font: UIFont.lato(fontSize: 12, type: .Bold)
        )
      textView.finilizeWithCenterlize()
      return textView
    }()

    lazy var optionsView: UIView = {
      let view = UIView()
      return view
    }()

    lazy var stairView: UIStackView = {
      let sv = UIStackView()
      sv.axis = .vertical
      sv.spacing = 5
      return sv
    }()

    lazy var leftCircleViewContainer: UIView = {
      let view = UIView()
      view.backgroundColor = .black
      view.alpha = 0.30
      return view
    }()

    lazy var leftCircleImageView: UIImageView = {
      let view = UIImageView()
      view.image = UIImage(named: "ShovelLarge")
      view.tintColor = UIColor.mainColor()
      return view
    }()

    lazy var rightViewContainer: UIView = {
      let view = UIView()
      return view
    }()

    lazy var rightTextView: SCAttributedTextView = {
      let textView = SCAttributedTextView()
      let color = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1)
      textView.setup(textInfo: "M101 Features", textColor: color, font: UIFont.lato(fontSize: 12, type: .Bold))
      textView.setText(textContent: "\n\n• Individual Mining License", textColor: color, font: UIFont.lato(fontSize: 12))
      textView
        .setText(
          textContent: "\n\n• Mine SXE Coins from home or wherever you work from.",
          textColor: color,
          font: UIFont.lato(fontSize: 12)
        )
      textView
        .setText(
          textContent: "\n\n• Priority upgrade option to Power Mining License.",
          textColor: color,
          font: UIFont.lato(fontSize: 12)
        )
      textView.finilize()
      return textView
    }()

    lazy var bottomTextView: SCAttributedTextView = {
      let textView = SCAttributedTextView()
      textView.setup(textInfo: "T400 ", textColor: .white, font: UIFont.lato(fontSize: 24, type: .Bold))
      textView.setText(textContent: "License", textColor: .white, font: UIFont.lato(fontSize: 24))
      textView
        .setText(
          textContent: "\n\nGet an extra SXE 10,000 bonus bounty",
          textColor: .white,
          font: UIFont.lato(fontSize: 14)
        )
      textView.setText(textContent: "\nthat is $50,000 USD", textColor: .white, font: UIFont.lato(fontSize: 14))
      textView
        .setText(
          textContent: "\n\nFor a one time payment of ",
          textColor: .white,
          font: UIFont.lato(fontSize: 16, type: .Light)
        )
      textView.setText(textContent: "$999 USD", textColor: .white, font: UIFont.lato(fontSize: 18, type: .Bold))
      textView.finilizeWithCenterlize()
      return textView
    }()

    lazy var actionButton: SCButton = {
      let button = SCButton()
      button.CornerRadius = 18
      button.titleLabel?.font = UIFont.futura(fontSize: 14, type: .Medium)
      button.titleLabel?.textColor = .white
      button.addTarget(self, action: #selector(onActionButtonClick(_:)), for: .touchUpInside)
      return button
    }()

    let imageList = ["ShovelLarge", "Pick", "PickAndShovel", "Shovels", "Picks"]

    // MARK: - Methods

    private func setupViews() {
      containerView.addSubview(titleView)
      containerView.addSubview(optionsView)
      containerView.addSubview(bottomTextView)
      containerView.addSubview(actionButton)

      _ = titleView
        .anchor(
          containerView.topAnchor,
          left: nil,
          bottom: nil,
          right: nil,
          topConstant: 20,
          leftConstant: 0,
          bottomConstant: 0,
          rightConstant: 0,
          widthConstant: 330,
          heightConstant: 95
        )
      _ = titleView.centralizeX(containerView.centerXAnchor)

      _ = optionsView
        .anchor(
          titleView.bottomAnchor,
          left: view.leftAnchor,
          bottom: nil,
          right: view.rightAnchor,
          topConstant: 5,
          leftConstant: 0,
          bottomConstant: 0,
          rightConstant: 0,
          widthConstant: 0,
          heightConstant: 280
        )

      _ = bottomTextView
        .anchor(
          optionsView.bottomAnchor,
          left: nil,
          bottom: nil,
          right: nil,
          topConstant: 5,
          leftConstant: 0,
          bottomConstant: 0,
          rightConstant: 0,
          widthConstant: 345,
          heightConstant: 125
        )
      _ = bottomTextView.centralizeX(containerView.centerXAnchor)

      _ = actionButton
        .anchor(
          bottomTextView.bottomAnchor,
          left: nil,
          bottom: nil,
          right: nil,
          topConstant: 15,
          leftConstant: 0,
          bottomConstant: 0,
          rightConstant: 0,
          widthConstant: 280,
          heightConstant: 36
        )

      _ = actionButton.centralizeX(containerView.centerXAnchor)

      setupOptionsView()
    }

    func setupOptionsView() {
        optionsView.addSubview(stairView)
        optionsView.addSubview(leftCircleViewContainer)
        optionsView.addSubview(rightViewContainer)

        _ = leftCircleViewContainer
          .anchor(
            nil,
            left: optionsView.leftAnchor,
            bottom: nil,
            right: nil,
            topConstant: 0,
            leftConstant: 18,
            bottomConstant: 0,
            rightConstant: 0,
            widthConstant: 132,
            heightConstant: 132
          )
        _ = leftCircleViewContainer.centralizeY(optionsView.centerYAnchor)

        leftCircleViewContainer.addSubview(leftCircleImageView)
        _ = leftCircleImageView.centralizeX(leftCircleViewContainer.centerXAnchor)
        _ = leftCircleImageView.centralizeY(leftCircleViewContainer.centerYAnchor)

        _ = rightViewContainer
          .anchor(
            optionsView.topAnchor,
            left: nil,
            bottom: optionsView.bottomAnchor,
            right: optionsView.rightAnchor,
            topConstant: 35,
            leftConstant: 0,
            bottomConstant: 35,
            rightConstant: 10,
            widthConstant: 140,
            heightConstant: 0
          )

        rightViewContainer.addSubview(rightTextView)
        _ = rightTextView
          .anchor(
            nil,
            left: nil,
            bottom: nil,
            right: nil,
            topConstant: 0,
            leftConstant: 0,
            bottomConstant: 0,
            rightConstant: 0,
            widthConstant: 125,
            heightConstant: 160
          )
        _ = rightTextView.centralizeX(rightViewContainer.centerXAnchor)
        _ = rightTextView.centralizeY(rightViewContainer.centerYAnchor)

        _ = stairView.widthAnchor.constraint(equalToConstant: 60).isActive = true
        _ = stairView.centralizeY(optionsView.centerYAnchor)
        _ = stairView.centralizeX(optionsView.centerXAnchor)

        guard let viewModel = viewModel else { return }
        for i in 1...viewModel.plans.count {
            let optionButton = SMButton()
            let selectedImage: String = (i == 1) ? "Active" : "Inactive"
            optionButton.seqNo = i
            optionButton.isActive = (i == 1) ? true : false
            optionButton.setImage(UIImage(named: selectedImage), for: .normal)
            optionButton.addTarget(self, action: #selector(onOptionButtonClick(_:)), for: .touchUpInside)
            stairView.addArrangedSubview(optionButton)
        }
    }

    private func changeTheOption(seqNo: Int) {
      UIView.animate(
        withDuration: 0.5,
        delay: 0.0,
        options: UIView.AnimationOptions.curveEaseOut,
        animations: {
          self.leftCircleViewContainer.frame.origin.y -= 100
          self.leftCircleViewContainer.alpha = 0.0

          self.rightViewContainer.frame.origin.y += 100
          self.rightViewContainer.alpha = 0.0

        },
        completion: { (_: Bool) in
            self.leftCircleImageView.isHidden = true
            self.leftCircleImageView.image = UIImage(named: self.imageList[seqNo % self.imageList.count])

            self.resetValues(seqNo: seqNo)
        }
      )
    }

    private func resetValues(seqNo: Int) {
      leftCircleViewContainer.frame.origin.y += 130
      rightViewContainer.frame.origin.y -= 130

      leftCircleImageView.isHidden = false
      setActiveText(seqNo: seqNo)

      UIView.animate(
        withDuration: 0.5,
        delay: 0.0,
        options: UIView.AnimationOptions.curveEaseOut,
        animations: {
          self.leftCircleViewContainer.frame.origin.y -= 30
          self.leftCircleViewContainer.alpha = 0.5

          self.rightViewContainer.frame.origin.y += 30
          self.rightViewContainer.alpha = 0.5

        },
        completion: nil
      )
    }

    private var viewModel: PowerMinerViewModel?
    private var selected: LicenseViewModel?
    
    private var options: [ClaimBonusOptionView] = []
    
    private func setActiveText(seqNo: Int) {
        let color = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1)
        rightTextView.reset()
        bottomTextView.reset()

        guard let viewModel = viewModel else { return }
        
        let model = viewModel.plans[seqNo-1]
        selected = model
        rightTextView.setup(textInfo: "\(model.name) Features", textColor: color, font: UIFont.lato(fontSize: 12, type: .Bold))
        for feature in model.features {
            rightTextView.setText(textContent: "\n\n• \(feature)", textColor: color, font: UIFont.lato(fontSize: 12))
        }
        
        bottomTextView.setup(textInfo: "\(model.name) ", textColor: .white, font: UIFont.lato(fontSize: 24, type: .Bold))
        //bottomTextView.setText(textContent: "License", textColor: .white, font: UIFont.lato(fontSize: 24))
        bottomTextView
          .setText(
            textContent: "\n\n\(model.bonus)",
            textColor: .white,
            font: UIFont.lato(fontSize: 14)
          )
//        bottomTextView.setText(textContent: "\nthat is $5,000 USD", textColor: .white, font: UIFont.lato(fontSize: 14))
        bottomTextView
          .setText(textContent: "\n\nFor only ", textColor: .white, font: UIFont.lato(fontSize: 16, type: .Light))
        bottomTextView.setText(textContent: "$\(model.price) USD", textColor: .white, font: UIFont.lato(fontSize: 18, type: .Bold))

        if model.active {
            actionButton.setTitle("Already Purchased", for: .normal)
            actionButton.isEnabled = false
        } else {
            actionButton.setTitle("Upgrade to \(model.planType)", for: .normal)
            actionButton.isEnabled = true
        }

        rightTextView.finilize()
        bottomTextView.finilizeWithCenterlize()
      
    }

    // MARK: - Events

    @objc private func onActionButtonClick(_: SCButton) {
        guard let selected = selected else { return }
        presenter?.perform(.upgrageTo(selected.planType))
    }

    @objc private func onOptionButtonClick(_ button: SMButton) {
        if button.isActive { return }

        button.isActive = true
        button.setImage(UIImage(named: "Active"), for: .normal)
        for case let eachButton as SMButton in stairView.subviews {
          if eachButton.seqNo != button.seqNo {
            eachButton.isActive = false
            eachButton.setImage(UIImage(named: "Inactive"), for: .normal)
          }
        }

        changeTheOption(seqNo: button.seqNo)
    }

    // MARK: - ViewContoller

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.perform(.viewDidLoad)
    }

    override func viewDidLayoutSubviews() {
      leftCircleViewContainer.layer.cornerRadius = leftCircleViewContainer.frame.width / 2
      leftCircleViewContainer.clipsToBounds = true
    }
    
    func populate(_ state: PowerMinerViewState) {
        switch state {
        case .loader(let show): show ? showLoader() : hideLoader()
        case .viewModel(let model):
            self.viewModel = model
            setupViews()
            setActiveText(seqNo: 1)
        case .error(let text): showAlert(errorMessage: text)
        }
    }
}
