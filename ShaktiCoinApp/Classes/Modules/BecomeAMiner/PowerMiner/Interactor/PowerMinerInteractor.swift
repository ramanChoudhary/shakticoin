//  Copyright © 2020 ShaktiCoinApp. All rights reserved.

import UIKit

protocol PowerMinerInteracting: AnyObject {
    func fetchLicenses(completion: @escaping (Result<[LicenseModel], ErrorModel>) -> Void)
    func fetchNodeOperator(completion: @escaping (Result<NodeOperatorModel?, ErrorModel>) -> Void)
}

final class PowerMinerInteractor {
    private let licenseRepository: LicensesRepository

    init(licenseRepository: LicensesRepository) {
      self.licenseRepository = licenseRepository
    }
  
}

extension PowerMinerInteractor: PowerMinerInteracting {
    func fetchLicenses(completion: @escaping (Result<[LicenseModel], ErrorModel>) -> Void) {
        licenseRepository.minerLicenses(callback: {
            switch $0 {
            case .success(let models):
                completion(.success(models.sorted(by: { $0.orderNumber < $1.orderNumber })))
            case .failure(let error): completion(.failure(error))
            }
        })
    }

    func fetchNodeOperator(completion: @escaping (Result<NodeOperatorModel?, ErrorModel>) -> Void) {
        licenseRepository.nodeOperator(callback: {
            switch $0 {
            case .success(let model):
                completion(.success(model))
            case .failure(let error): completion(.failure(error))
            }
        })
    }
  
}
