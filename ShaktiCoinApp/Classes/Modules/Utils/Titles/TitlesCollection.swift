

import UIKit

protocol TitlesCollectionDelegate {
    func didSelect(item:IndexPath)
}

//MARK: - TITLES COLLECTION CELL
class TitlesCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentBgView: UIView!
    
    
    func setTitle(string:String, isSelected:Bool,theme:TitlesCollection.Theme) {
        self.titleLabel.cornerRadius = 15
        self.titleLabel.borderWidth(1)
        
        if isSelected {
            self.titleLabel.setLabel(string, #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), UIFont(name: "Lato-Bold", size: 16) ?? UIFont.systemFont(ofSize: 14))
            self.titleLabel.backgroundColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
            self.titleLabel.borderColor(#colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1))
        } else {
            self.titleLabel.setLabel(string,theme == .dark ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1): #colorLiteral(red: 0.537254902, green: 0.537254902, blue: 0.537254902, alpha: 1), UIFont(name: "Lato-Regular", size: 14) ?? UIFont.systemFont(ofSize: 14))
            self.titleLabel.backgroundColor = theme == .dark ? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1): #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.titleLabel.borderColor(#colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8549019608, alpha: 1))
        }
    }
    
}

//MARK: - Titles Collection
class TitlesCollection: UICollectionView {
    enum ViewType {
        case contact
    }
    
    enum Theme {
        case dark
        case light
    }
    
    var titles: [String] = []
    var selectedTitleIndex:IndexPath = IndexPath(item: 0, section: 0)
    var titlesCollectionViewDelegate: TitlesCollectionDelegate?
    var collectionType: TitlesCollection.ViewType =  .contact
    var collectionTheme: TitlesCollection.Theme = .light
    
    func setTitlesCollection(titles: [String], delegate: TitlesCollectionDelegate?, type: TitlesCollection.ViewType, theme:TitlesCollection.Theme) {
        self.titles = titles
        self.titlesCollectionViewDelegate = delegate
        self.collectionType = type
        self.collectionTheme = theme
        self.reloadData()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.registerCell(identifier: cellIdentifier.titlesCollectionCell)
        self.delegate = self
        self.dataSource = self
    }
    
}

//MARK:- COLLECTION VIEW DELEGATE DATA SOURCE
extension TitlesCollection: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.titles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier.titlesCollectionCell, for: indexPath) as! TitlesCollectionCell
        switch self.collectionType {
        case .contact:
            cell.setTitle(string: self.titles[indexPath.item], isSelected: self.selectedTitleIndex == indexPath, theme: self.collectionTheme)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = self.frame.size
        switch self.collectionType {
        case .contact:
            let text = self.titles[indexPath.item]
            let width = text.widthWithConstrainedHeight(height: 30, font: UIFont(name: "Lato-Regular", size: 16) ?? UIFont.systemFont(ofSize: 16)).size.width + 20
            return CGSize(width: width, height: 30)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard self.selectedTitleIndex != indexPath else {return}
        let index = self.selectedTitleIndex
        self.selectedTitleIndex = indexPath
        self.reloadItems(at: [index, self.selectedTitleIndex])
        self.titlesCollectionViewDelegate?.didSelect(item: indexPath)
    }
}
