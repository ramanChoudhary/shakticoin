//
//  Constants.swift
//  CullintonsCustomer
//
//  Created by Rakesh Kumar on 30/03/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import UIKit

let SCREEN_SIZE = UIScreen.main.bounds
let walletBytes = "fhctFR+Dj4G72BgCqR6VgXemQUP9V2W2jC65SEecJNNVnciL6F/Bz3fxs7DWAzwtnsNXGQECMLqUQbvBk0KDfDt0vbEY5SFdoRYQ39FhJEznr9H+DC0eN8WT/qcnW+NNwycLsvNJW/m0PgeSuwT3aLjwKhld0GFoLo/BTxiuNezokMU4GZIuDf3/jcfWSrdti6nKYjv0BZe9srs5vAMY3Q"
let walletAddress = "sxe-#1cfpmy40bx6br7jmammpxb4702rep0mhp1rhx"
let shakiId = "06db9523-7c0b-472c-a4b2-b74a58b32fb7"

struct STORYBOARD_ID {
    static let createWalletController = "CreateWalletController"
    static let walletViewController = "WalletViewController"
}

struct AppStrings {
    static let merchant = "Merchant"
    static let merchantFeaturePoints = "• Reduce banking charges.\n\n• Eliminate banking time.\n\n• Sell goods and services using universal currency.\n\n• Track tax obligations in real-time."
    static let merchantDesc = "Improve your business with a Merchant Vault, a value of $25,000 USD. You will be eligible for a Bonus Bounty of 5,000 SXE."
    
    static let powerMiner = "Power Miner"
    static let powerMinerPoints = "• Limited number of node operators.\n\n• Node Operator."
    static let powerMinerDesc = "Upgrade your account to Power Miner.Expand your territory. This vault type is eligible for a Bonus Bounty of 10,000 SXE."
    
    static let feducia = "Feducia"
    static let feduciaPoints = "• Tripartite custody of SXE assets.\n\n• Sale of asset recorded in blockchain.\n\n• Worry-free, paper-less back-office.\n\n• Instant generation of accounting records at the time of settlor executing the TRUST Deed."
    static let feduciaDesc = "Become a Shakti World Trustee. A Feducia vault is eligible for a Bonus Bounty up to 10,000 SXE. - a value of $50,000 USD!"
    
    static let shaktiExpress = "Shakti Express"
    static let shaktiExpressPoints = "• Move money instantly for cents worldwide.\n\n• Built-in KYC, AML and CFT compliance.\n\n• You just UBER the money.\n\n• In-built trace path eliminates fraud.\n\n• Save time and money in cumbersome paperwork."
    static let shaktiExpressDesc = "Say hello to new way to move money. No office. No staff. No paperwork. No fraud. Bonus Bounty of up to SXE 20,000 - a value of $100,000 USD for early adopters."
    
    static let devDot = "DevDot"
    static let devDotPoints = "• Get selected and be paid to code.\n\n• Annual tenure.\n\n• Evaluated by peers.\n\n• It’s time to reward and value the Open Source Community."
    static let devDotDesc = "This wallet comes with Bonus Bounty of 30,000 SXE - a value of $150,000 USD. Amazing opportunity for OSC developers to get their hands-dirty."
    
    // Setting strings
    
    static let taxesInfo = "Taxes Info"
    static let taxesInfoDesc = "The Shakti ecosystem tracks tax implications resulting from transactions.\n\nUnderstanding potential tax exposure and planning for it is essential for mitigating transactional risk, levy cost, and managing cash flows.\n\nAs a crypto wallet holder, you may trigger cross-border transactional taxes when one buy and sell a broad range of consumer products from various industry sectors.\n\nWe will track your tax exposures based on the transaction activities within the wallet you make from the country you shop and as well as with the peers with whom you trade.\n\nThe Shakti Network will not report your tax exposures to any authorities, but we will do so to you and to you only. It is provided to you, so you can plan for your tax liabilities and make informed decisions. and navigate through the implications of each of every transaction."
    static let taxesInfoAgreeMsg = "If you wish to be updated with current transactional tax related risks, please check this box for additional information."
    
    static let applicationTerms = "Application Terms"
    static let privacyPolicy = "Privacy Policy"
    
    static let privacyPolicyDesc = "Lorem ipsum dolor sit amet, his dolores quaestio cu, per tritani epicuri inimicus ex. Eu mea fugit tollit senserit, in eros suscipit accusamus mei, pri no malorum erroribus. Sed cu atqui novum, graeci molestie pro ne. Te feugiat fuisset copiosae vis, liber urbanitas mel et, sint eruditi eu vix.\n\nCum amet malis verterem cu. Viderer sapientem eam ea, aliquam graecis in sea. His ad natum erant mucius, cu mei intellegat constituto sententiae, qui no aliquam dolores conceptam. Vel clita tibique apeirian at, per ad molestie democritum voluptatum. At agam illum ridens sit.\n\nAd mel ullum malorum ornatus, ne vim senserit patrioque, per at dicat nemore. Vivendo qualisque vim ad, an sea possit facete. Mei ne veniam munere accusam. Ex usu diam eius posidonium, inani iusto albucius vix no. Usu minim ludus cetero et. Nec ex vocent repudiare, te sea nominavi luptatum. Quo ut inimicus delicatissimi."
    
    static let applicationTermsDesc = "Lorem ipsum dolor sit amet, his dolores quaestio cu, per tritani epicuri inimicus ex. Eu mea fugit tollit senserit, in eros suscipit accusamus mei, pri no malorum erroribus. Sed cu atqui novum, graeci molestie pro ne. Te feugiat fuisset copiosae vis, liber urbanitas mel et, sint eruditi eu vix.\n\nCum amet malis verterem cu. Viderer sapientem eam ea, aliquam graecis in sea. His ad natum erant mucius, cu mei intellegat constituto sententiae, qui no aliquam dolores conceptam. Vel clita tibique apeirian at, per ad molestie democritum voluptatum. At agam illum ridens sit.\n\nAd mel ullum malorum ornatus, ne vim senserit patrioque, per at dicat nemore. Vivendo qualisque vim ad, an sea possit facete. Mei ne veniam munere accusam. Ex usu diam eius posidonium, inani iusto albucius vix no. Usu minim ludus cetero et. Nec ex vocent repudiare, te sea nominavi luptatum. Quo ut inimicus delicatissimi."
    
    static let processingFees = "Processing Fees"
    static let processingFeesDesc = "Miners with SXE Network IDs earn fees for validating transactions by lending their computing power (mining) to the Shakti Network. For that service, they are compensated with fixed transaction processing fees.\n\nTransaction processing fees are earned by the initiating miner and the first set of confirming miners. Fees are paid in Toshi, a microdenomination of SXE Coin.\n\nThe first miner who initiates the consensus earn 5% of the transaction fee and the set of first miners who confirm the consensus earn the remainder of the fees at the rate of 25 Toshi (0.00005 SXE or US 0.025¢) per consensus until the sum of fees earned reaches 90% of the total.\n\nThe final 10% of total fees is paid to the Shakti Network to enforce Global Compliance and SNMP Policy and continue to further propagate the SXE monetary policy and to promote Shakti Coin adoption globally.\n\nAny payment up to and including two (2) Shakti Coins is considered a micro-transaction. The cost of a micro-transaction is fixed and it is set at500 Toshi (= US 2.5¢).\n\nThe processing payment cost (Y) is calculated as Y = SXE 0.005 when transaction size is SXE ≤ 2; and Y = k*Log2(SXE) when SXE > 2.\n1 Chai (=US 5¢) is the minimum amount that any node can transact over the Shakti Network.\n\n•     All values are measured in SXE.\n•     The function is continuous. The coefficient, k, regulates the growth rate.\n•     Y is the value of processing payment cost."
}

struct cellIdentifier {
    static let titlesCollectionCell  = "TitlesCollectionCell"
    static let myReferralTableCell = "MyReferralTableCell"
    static let importFromCollectionCell = "ImportFromCollectionCell"
    static let unlockBonusCell = "UnlockBonusCell"
    static let featOfPoeTableCell = "FeatOfPoeTableCell"
    static let childInfoCell = "ChildInfoCell"
    static let ratingHistoryCell = "RatingHistoryCell"
    static let settingsTableCell = "SettingsTableCell"
}

struct NIB_NAME {
    static let myReferralSectionHeader = "MyReferralSectionHeader"
    static let contactTableHeaderView = "ContactTableHeaderView"
    static let myReferralTableHeader = "MyReferralTableHeader"
    static let myReferralTableFooter = "MyReferralTableFooter"
    static let effortRateSectionHeader = "EffortRateSectionHeader"
    static let familyTreeHeaderView = "FamilyTreeHeaderView"
    static let featOfPoEFooterView = "FeatOfPoEFooterView"
    static let settingTableHeaderView = "SettingTableHeaderView"
}

struct STORYBOARD {
    static let wallet = "Wallet"
}


