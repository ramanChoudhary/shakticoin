//
//  CustomOutlinedTextField.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 02/05/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents.MaterialTextFields

@IBDesignable
class FloatingPlaceholderTextField: UIView{
    
    private var textInput: MDCTextField!
    private var controller: MDCTextInputControllerOutlined!
    private var textColor:UIColor  = .clear // Dynamic dark & light color created in the assets folder
    private var placeHolderText = ""
    
    @IBInspectable var setPlaceholder: String{
        get{
            return placeHolderText
        }
        set(str){
            placeHolderText = str
        }
    }
    
    @IBInspectable var setTextColor: UIColor{
        get{
            return textColor
        }
        set(color){
            textColor = color
        }
    }
    
    override func layoutSubviews() {
       
        setupInputView()
        setupContoller()
           
    }
    
    private func setupInputView(){
        //MARK: Text Input Setup
        
        if let _ = self.viewWithTag(1){return}
        
        textInput = MDCTextField()
        
        textInput.tag = 1
        
        textInput.translatesAutoresizingMaskIntoConstraints = false
               
        self.addSubview(textInput)
        
        textInput.placeholder = placeHolderText

        textInput.delegate = self
        
        textInput.textColor = textColor
        
        
        NSLayoutConstraint.activate([
            (textInput.topAnchor.constraint(equalTo: self.topAnchor)),
            (textInput.bottomAnchor.constraint(equalTo: self.bottomAnchor)),
            (textInput.leadingAnchor.constraint(equalTo: self.leadingAnchor)),
            (textInput.trailingAnchor.constraint(equalTo: self.trailingAnchor))
        ])
    }
    
    private func setupContoller(){
        // MARK: Text Input Controller Setup
        
        controller = MDCTextInputControllerOutlined(textInput: textInput)
        
        controller.activeColor = textColor
        controller.normalColor = textColor
        controller.textInput?.textColor = .white
        controller.inlinePlaceholderColor = .white
        controller.textInput?.font = UIFont().setLatoRegularFont(size: 14)
        controller.floatingPlaceholderActiveColor = textColor
        controller.floatingPlaceholderNormalColor = .white
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    
}

extension FloatingPlaceholderTextField: UITextFieldDelegate {
    
}
