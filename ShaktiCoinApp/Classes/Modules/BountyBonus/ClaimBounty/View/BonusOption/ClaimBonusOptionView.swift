//
//  ClaimBonusOptionView.swift
//  ShaktiCoinApp
//
//  Created by sperev on 16/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

struct ClaimBonusOptionViewModel {
    var imageName: String
    var titleViewModel: TextViewModel
    var textViewModel: TextViewModel
    var checkboxViewModel: IconTextViewModel
}

class ClaimBonusOptionView: StackViewModule {
    @IBOutlet weak var imageView: UIImageView?
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var textLabel: UILabel?
    @IBOutlet weak var checkbox: IconTextModule?
    
    func populate(_ viewModel: ClaimBonusOptionViewModel) {
        imageView?.image = UIImage(named: viewModel.imageName)
        titleLabel?.setup(viewModel.titleViewModel)
        textLabel?.setup(viewModel.textViewModel)
        checkbox?.populate(viewModel.checkboxViewModel)
    }
}
