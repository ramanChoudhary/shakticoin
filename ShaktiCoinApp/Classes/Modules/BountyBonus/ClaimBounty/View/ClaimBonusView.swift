//
//  ClaimBonusView.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

final class ClaimBonusView: StackView, ClaimBonusViewProtocol {
    var presenter: ClaimBonusPresenterProtocol?
    private var options: [ClaimBonusOptionView] = []
    private var spaceInsets = UIEdgeInsets.horizontal(20).vertical(5)
    private var topInsets = UIEdgeInsets(top: 25, horizontal: 20, bottom: 5)
    private var textInsets = UIEdgeInsets(top: 5, horizontal: 20, bottom: 20)
    private var bottomInsets = UIEdgeInsets(top: 0, horizontal: 50, bottom: 5)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.perform(.loaded)
    }
    
    func populate(_ state: ClaimBonusViewState) {
        switch state {
        case .loaded: break
        case .loading: break
        case .addTitle(let viewModel):
            let module = UILabel()
            module.setup(viewModel)
            addView(module, edgeInsets: topInsets)
        case .addSubtitle(let viewModel):
            let module = UILabel()
            module.setup(viewModel)
            addView(module, edgeInsets: textInsets)
        case .options(let items):
            for (index, item) in items.enumerated() {
                var optionView: ClaimBonusOptionView
                if index < options.count {
                    optionView = options[index]
                } else {
                    optionView = ClaimBonusOptionView()
                    options.append(optionView)
                    addView(optionView, edgeInsets: spaceInsets)
                }
                optionView.populate(item)
            }
            
            if items.count < options.count {
                options[items.count...].forEach {
                    stack.removeView($0.superview!)
                    stack.removeArrangedSubview($0.superview!)
                }
            }
        case .addText(let viewModel):
            let module = UILabel()
            module.setup(viewModel)
            addView(module, edgeInsets: bottomInsets)
        case .addSpace(let height):
            let module = UIView()
            module.backgroundColor = .clear
            module.translatesAutoresizingMaskIntoConstraints = false
            module.heightAnchor.constraint(equalToConstant: CGFloat(height)).isActive = true
            addView(module)
        case .addButton(let viewModel):
            let module = GoldButtonModule()
            module.populate(model: viewModel)
            addView(module, edgeInsets: textInsets)
        case .error(let string):
            showAlert(errorMessage: string)
        case .claimed:
            showAlert(errorMessage: "Claimed".localized)
        }
    }
}
