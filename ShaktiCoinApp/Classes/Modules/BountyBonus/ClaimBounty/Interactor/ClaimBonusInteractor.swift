//
//  ClaimBonusInteractor.swift
//  ShaktiCoinApp
//
//  Created by sperev on 21/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

class ClaimBonusInteractor: ClaimBonusInteractorProtocol {
    weak var presenter: ClaimBonusInteractorOuputProtocol?
    var repository: ClaimBonusRepositoryProtocol?
    func `do`(job: ClaimBonusJob) {
        switch job {
        case .claim(let option):
            repository?.claimBonus(optionId: option) {
                switch $0 {
                case .success:
                    self.presenter?.handle(result: .claimed)
                case .failure(let error):
                    self.presenter?.handle(result: .error(error))
                }
            }
        }
    }
}
