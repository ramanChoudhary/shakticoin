//
//  ClaimBonusRepository.swift
//  ShaktiCoinApp
//
//  Created by sperev on 21/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

class ClaimBonusRepository: ClaimBonusRepositoryProtocol {
    var dataManager: BountyBonusDataManagerProtocol?
    var localDataManager: BountyBonusLocalDataManagerProtocol?
    
    func claimBonus(optionId: String, callback: @escaping (Result<DataModel<GenesisBonusModel>, ErrorModel>) -> Void) {
        guard let model = localDataManager?.genesisBonusModel else { return }
        dataManager?.setGrantOption(genesisBonusId: model.bonusId, optionId: optionId) {
            switch $0 {
            case .success(let json):
                if let model = try? Mapper<DataModel<GenesisBonusModel>>.map(object: json) {
                    callback(.success(model))
                } else {
                    callback(.failure(ErrorModel(MappingError.unknown)))
                }
            case .failure(let error):
                callback(.failure(error))
            }
        }
    }
}
