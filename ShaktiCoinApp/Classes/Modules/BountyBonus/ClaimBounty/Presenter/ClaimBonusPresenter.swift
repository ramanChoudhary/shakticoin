//
//  ClaimBonusPresenter.swift
//  ShaktiCoinApp
//
//  Created by sperev on 16/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

private struct Info {
    static var title = TextViewModel(style: TextStyle.latoMedium36.color(.white).alignment(.center), value: "GrabYourBounty".localized)
    static var subtitle = TextViewModel(style: TextStyle.lato14.color(.white).alignment(.center), value: "Select one method to unlock your bounty".localized)
    
    static var option1Image = "Time"
    static var option1Title = TextViewModel(style: TextStyle.latoBold14.color(ColorStyle.gold.color), value: "Refer and earn money")
    static var option1Text = TextViewModel(style: .lato12, value: "You will earn USD 25.00 (five SXE coins) towards your birthday for each person you refer to, and it will be unlocked just-in-time for your birthday celebration.")
    
    static func selectedCheckbox(_ action: @escaping () -> Void) -> IconTextViewModel {
        return IconTextViewModel(text: TextViewModel(style: .latoSemiBold12, value: "Select this option"), icon: .passwordChecked, selectAction: action)
    }
    
    static func unselectedCheckbox(_ action: @escaping () -> Void) -> IconTextViewModel {
        return IconTextViewModel(text: TextViewModel(style: .latoSemiBold12, value: "Select this option"), icon: .passwordUnchecked, selectAction: action)
    }
    
    static func option1(_ selected: Int?, action: @escaping () -> Void) -> ClaimBonusOptionViewModel {
        return ClaimBonusOptionViewModel(imageName: option1Image, titleViewModel: option1Title, textViewModel: option1Text, checkboxViewModel: selected == 1 ? selectedCheckbox(action) : unselectedCheckbox(action))
    }
    
    static var option2Image = "Bounty"
    static var option2Title = TextViewModel(style: TextStyle.latoBold14.color(ColorStyle.gold.color), value: "Refer & Cash-Out Fast")
    static var option2Text = TextViewModel(style: .lato12, value: "Refer 84 participants to unlock your bounty and ensure at least five participants are from the merchant community.")
    
    static func option2(_ selected: Int?, action: @escaping () -> Void) -> ClaimBonusOptionViewModel {
        return ClaimBonusOptionViewModel(imageName: option2Image, titleViewModel: option2Title, textViewModel: option2Text, checkboxViewModel: selected == 2 ? selectedCheckbox(action) : unselectedCheckbox(action))
    }
    
    static var option3Image = "Friends"
    static var option3Title = TextViewModel(style: TextStyle.latoBold14.color(ColorStyle.gold.color), value: "Just use it")
    static var option3Text = TextViewModel(style: .lato12, value: "Just use the wallet once a month to unlock your bounty.")
    
    static func option3(_ selected: Int?, action: @escaping () -> Void) -> ClaimBonusOptionViewModel {
        return ClaimBonusOptionViewModel(imageName: option3Image, titleViewModel: option3Title, textViewModel: option3Text, checkboxViewModel: selected == 3 ? selectedCheckbox(action) : unselectedCheckbox(action))
    }
    
    static var noteTitle = TextViewModel(style: TextStyle.lato10.alignment(.center), value: "NOTE")
    static var noteText = TextViewModel(style: TextStyle.lato12.alignment(.center), value: "Your quantum genesis bonus bounty is released on your birthday only, whether it is a partial or full payout when one of the above conditions are met.")
    
    static func button(_ action: @escaping () -> Void) -> ButtonModuleModel {
        return ButtonModuleModel(title: "Claim Your Bounty", enabled: true, callback: action)
    }
}

class ClaimBonusPresenter: ClaimBonusPresenterProtocol, ClaimBonusInteractorOuputProtocol {
    weak var view: ClaimBonusViewProtocol?
    var interactor: ClaimBonusInteractorProtocol?
    
    var selectedOption: Int?
    
    func perform(_ action: ClaimBonusViewAction) {
        switch action {
        case .loaded:
            view?.populate(.addTitle(Info.title))
            view?.populate(.addSubtitle(Info.subtitle))
            updateSelectedOption()
            view?.populate(.addSpace(20))
            view?.populate(.addButton(Info.button { [weak self] in
                guard let selectedOption = self?.selectedOption else { return }
                var option = ""
                switch selectedOption {
                case 1: option = "ReferAndEarnInstantBounty"
                case 2: option = "ReferNParticipents"
                default: option = "WaitForNMonths"
                }
                self?.view?.populate(.loading)
                self?.interactor?.do(job: .claim(option))
            }))
            view?.populate(.addSpace(10))
            view?.populate(.addText(Info.noteTitle))
            view?.populate(.addText(Info.noteText))
            view?.populate(.addSpace(20))
        }
    }
    
    private func updateSelectedOption() {
        view?.populate(.options([
        Info.option1(selectedOption) { [weak self] in
            self?.selectedOption = self?.selectedOption != 1 ? 1 : nil
            self?.updateSelectedOption()
        },
        Info.option2(selectedOption) { [weak self] in
            self?.selectedOption = self?.selectedOption != 2 ? 2 : nil
            self?.updateSelectedOption()
        },
        Info.option3(selectedOption) { [weak self] in
            self?.selectedOption = self?.selectedOption != 3 ? 3 : nil
            self?.updateSelectedOption()
        }]))
    }
    
    func handle(result: ClaimBonusResult) {
        switch result {
        case .claimed:
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
                self.view?.populate(.claimed)
            }
        case .error(let error):
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
                self.view?.populate(.error(error.value.string))
            }
        case .options: break
        }
    }
}
