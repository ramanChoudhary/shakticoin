//
//  ClaimBonusWireframe.swift
//  ShaktiCoinApp
//
//  Created by sperev on 16/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

final class ClaimBonusWireframe {
    static func createModule() -> UIViewController {
        let view = ClaimBonusView()
        let presenter = ClaimBonusPresenter()
        let interactor = ClaimBonusInteractor()
        let repository = ClaimBonusRepository()
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        interactor.repository = repository
        interactor.presenter = presenter
        repository.dataManager = BountyBonusDataManager()
        repository.localDataManager = BountyBonusLocalDataManager.shared
        return view
    }
}
