//
//  ClaimBonusPresenterProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum ClaimBonusViewAction {
    case loaded
}

protocol ClaimBonusPresenterProtocol {
    func perform(_ action: ClaimBonusViewAction)
}
