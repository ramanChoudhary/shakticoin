//
//  ClaimBonusViewProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum ClaimBonusViewState {
    case loading
    case loaded
    case addTitle(TextViewModel)
    case addSubtitle(TextViewModel)
    case addText(TextViewModel)
    case options([ClaimBonusOptionViewModel])
    case addSpace(Float)
    case addButton(ButtonModuleModel)
    case error(String)
    case claimed
}

protocol ClaimBonusViewProtocol: class {
    func populate(_ state: ClaimBonusViewState)
}
