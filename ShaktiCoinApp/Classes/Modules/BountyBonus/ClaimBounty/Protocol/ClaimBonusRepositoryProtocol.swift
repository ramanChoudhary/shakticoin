//
//  ClaimBonusRepositoryProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 21/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

protocol ClaimBonusRepositoryProtocol {
    func claimBonus(optionId: String, callback: @escaping (Result<DataModel<GenesisBonusModel>, ErrorModel>) -> Void)
}
