//
//  ClaimBonusInteractorProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 21/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum ClaimBonusJob {
    case claim(String)
}

enum ClaimBonusResult {
    case claimed
    case options
    case error(ErrorModel)
}

protocol ClaimBonusInteractorProtocol {
    func `do`(job: ClaimBonusJob)
}

protocol ClaimBonusInteractorOuputProtocol: class {
    func handle(result: ClaimBonusResult)
}
