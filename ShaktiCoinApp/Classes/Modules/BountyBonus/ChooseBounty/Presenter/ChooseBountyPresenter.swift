//
//  ChooseBountyPresenter.swift
//  ShaktiCoinApp
//
//  Created by sperev on 21/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

class ChooseBountyPresenter: ChooseBountyPresenterProtocol, ChooseBountyInteractorOutputProtocol {
    weak var view: ChooseBountyViewProtocol?
    var interactor: ChooseBountyInteractorProtocol?
    var wireframe: ChooseBountyWireframeProtocol?
    
    func perform(_ action: ChooseBountyAction) {
        switch action {
        case .loaded: interactor?.do(.loadAllOffers)
        case .claimSxeBonus(let offer, let birthday):
            view?.populate(.loading)
            interactor?.do(.claimSxeBonus(offer, birthday))
        case .claimLater:
            wireframe?.navigate(.claimLater)
        }
    }
    
    func handle(_ result: ChooseBountyResult) {
        switch result {
        case .error(let model):
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
                self.view?.populate(.error(model))
            }
        case .offers(let model):
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
                self.view?.populate(.offers(model))
            }
        case .claimed(let model):
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
                self.wireframe?.navigate(.claimingConfirmation(model.promisedGenisisBounty))
            }
        }
    }
}
