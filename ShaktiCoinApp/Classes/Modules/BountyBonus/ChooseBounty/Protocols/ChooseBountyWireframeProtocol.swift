//
//  ChooseBountyWireframeProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 22/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum ChooseBountyRoute {
    case claimingConfirmation(Int?)
    case claimLater
}

protocol ChooseBountyWireframeProtocol {
    func navigate(_ route: ChooseBountyRoute)
}
