//
//  ChooseBountyViewProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 21/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum ChooseBountyState {
    case error(ErrorModel)
    case offers(DataArrayModel<BonusModel>)
    case loading
    case loaded
}

protocol ChooseBountyViewProtocol: class {
    func populate(_ state: ChooseBountyState)
}
