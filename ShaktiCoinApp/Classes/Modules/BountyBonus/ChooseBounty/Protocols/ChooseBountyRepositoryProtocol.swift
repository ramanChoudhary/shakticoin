//
//  ChooseBountyRepositoryProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 21/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

protocol ChooseBountyRepositoryProtocol {
    func requestAllOffers(callback: @escaping (Result<DataArrayModel<BonusModel>, ErrorModel>) -> Void)
    func claimBonusBounty(genesisBonusId: String, birthday: Date, callback: @escaping (Result<GenesisBonusModel, ErrorModel>) -> Void)
    func bonusBounty(callback: @escaping (Result<GenesisBonusModel, ErrorModel>) -> Void)
    var bountyBonus: GenesisBonusModel? { get }
}
