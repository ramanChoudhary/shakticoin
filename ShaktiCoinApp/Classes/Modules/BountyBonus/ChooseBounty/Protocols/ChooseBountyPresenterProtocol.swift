//
//  ChooseBountyPresenterProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 21/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum ChooseBountyAction {
    case loaded
    case claimSxeBonus(String, Date)
    case claimLater
}

protocol ChooseBountyPresenterProtocol {
    func perform(_ action: ChooseBountyAction)
}
