//
//  ChooseBountyInteractorProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 21/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum ChooseBountyJob {
    case loadAllOffers
    case claimSxeBonus(String, Date)
}

protocol ChooseBountyInteractorProtocol {
    func `do`(_ job: ChooseBountyJob)
}

enum ChooseBountyResult {
    case error(ErrorModel)
    case offers(DataArrayModel<BonusModel>)
    case claimed(GenesisBonusModel)
}

protocol ChooseBountyInteractorOutputProtocol: class {
    func handle(_ result: ChooseBountyResult)
}
