//
//  BountyBonusDataManagerProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 08/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

protocol BountyBonusDataManagerProtocol {
    func requestAllOffers(callback: @escaping APIDataManagerObjectResultBlock)
    func setGrantOption(genesisBonusId: String, optionId: String, callback: @escaping APIDataManagerObjectResultBlock)
    func claimBonusBounty(genesisBonusId: String, birthday: Date, callback: @escaping APIDataManagerObjectResultBlock)
    func grantOptions(genesisBonusId: String, callback: @escaping APIDataManagerObjectResultBlock)
    func bonusBounties(callback: @escaping APIDataManagerObjectResultBlock)
}
