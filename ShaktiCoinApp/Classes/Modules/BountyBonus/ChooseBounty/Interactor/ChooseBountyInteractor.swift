//
//  ChooseBountyInteractor.swift
//  ShaktiCoinApp
//
//  Created by sperev on 21/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

class ChooseBountyInteractor: ChooseBountyInteractorProtocol {
    weak var presenter: ChooseBountyInteractorOutputProtocol?
    var repository: ChooseBountyRepositoryProtocol?
    
    func `do`(_ job: ChooseBountyJob) {
        switch job {
        case .loadAllOffers:
            repository?.requestAllOffers(callback: { [weak self] result in
                switch result {
                case .success(let model):
                    self?.presenter?.handle(.offers(model))
                case .failure(let error):
                    self?.presenter?.handle(.error(error))
                }
            })
        case .claimSxeBonus(let genesisBonusId, let birthday):
            repository?.claimBonusBounty(genesisBonusId: genesisBonusId, birthday: birthday) { [weak self] result in
                switch result {
                case .success(let model):
                    self?.presenter?.handle(.claimed(model))
                case .failure(let error):
                    self?.presenter?.handle(.error(error))
                }
            }
        }
    }
}
