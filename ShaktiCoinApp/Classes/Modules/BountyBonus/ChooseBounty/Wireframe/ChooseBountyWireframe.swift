//
//  ChooseBountyWireframe.swift
//  ShaktiCoinApp
//
//  Created by sperev on 21/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit
import SideMenu

class ChooseBountyWireframe: ChooseBountyWireframeProtocol {
    class func createModule() -> UIViewController {
        let view = ChooseBountyView()
        let presenter = ChooseBountyPresenter()
        let interactor = ChooseBountyInteractor()
        let repository = ChooseBountyRepository.shared
        let wireframe = ChooseBountyWireframe()
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.wireframe = wireframe
        interactor.presenter = presenter
        interactor.repository = repository
        return view
    }
    
    func navigate(_ route: ChooseBountyRoute) {
        switch route {
        case .claimLater:
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = SideMenuNavigationController(rootViewController: MyWalletRouter.createMyWalletModule())
        case .claimingConfirmation(let amount):
            let congratsVC = CongratsViewController()
            congratsVC.bonusAmount = amount
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = congratsVC
        }
    }
}
