//
//  BountyBonusLocalDataManager.swift
//  ShaktiCoinApp
//
//  Created by sperev on 22/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation
import KeychainAccess

protocol BountyBonusLocalDataManagerProtocol {
    var genesisBonusModel: GenesisBonusModel? { get }
    func set(_ genesisBonusModel: GenesisBonusModel)
}

final class BountyBonusLocalDataManager: BountyBonusLocalDataManagerProtocol {
    private let genesisKeychainKey = "genesis_bounty_bonus"
    private var service = Keychain(service: "shakti_ids")
    
    static let shared = BountyBonusLocalDataManager()
    
    // MARK: - Methods
    private(set) var genesisBonusModel: GenesisBonusModel?
    
    init() {
        do {
            if let data = try service.getData(genesisKeychainKey),
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? JSONObject  {
                genesisBonusModel = try? Mapper<GenesisBonusModel>.map(object: json)
            }
        } catch let error {
            print(error)
        }
    }
    
    func set(_ genesisBonusModel: GenesisBonusModel) {
        self.genesisBonusModel = genesisBonusModel
        if let data = try? JSONSerialization.data(withJSONObject: genesisBonusModel.json, options: .prettyPrinted) {
            do {
                try service.set(data, key: genesisKeychainKey)
            } catch let error {
                print(error)
            }
        }
    }
    
    func reset() {
        try? service.remove(genesisKeychainKey)
    }
}
