//
//  BountyBonusDataManager.swift
//  ShaktiCoinApp
//
//  Created by sperev on 08/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class BountyBonusDataManager: BountyBonusDataManagerProtocol {
    var dataSource = RemoteDataSource.shared
    func requestAllOffers(callback: @escaping APIDataManagerObjectResultBlock) {
        let params: JSONObject = [:]
        dataSource.requestObject(forEndpoint: Endpoint.BountyReferralEndpoint.allOffer, parameters: params, result: callback)
    }
    
    func setGrantOption(genesisBonusId: String, optionId: String, callback: @escaping APIDataManagerObjectResultBlock) {
        let params: JSONObject = ["bountyGrantOption": optionId]
        dataSource.requestObject(forEndpoint: Endpoint.BountyReferralEndpoint.setGenesisBoutyGrantOption(genesisBonusId), parameters: params, result: callback)
    }
    
    func claimBonusBounty(genesisBonusId: String, birthday: Date, callback: @escaping APIDataManagerObjectResultBlock) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let birthday = formatter.string(from: birthday)
        
        let params: JSONObject = [
            "offerID": genesisBonusId,
            "dob": birthday
        ]
        
        dataSource.requestObject(forEndpoint: Endpoint.BountyReferralEndpoint.claimBonusBounty, parameters: params, result: callback)
    }
    
    func grantOptions(genesisBonusId: String, callback: @escaping APIDataManagerObjectResultBlock) {
        let params: JSONObject = [:]
        dataSource.requestObject(forEndpoint: Endpoint.BountyReferralEndpoint.genesisBountyGrantOptions(genesisBonusId), parameters: params, result: callback)
    }
    
    func bonusBounties(callback: @escaping APIDataManagerObjectResultBlock) {
        dataSource.requestObject(forEndpoint: Endpoint.BountyReferralEndpoint.bonusBounties, result: callback)
    }
}
