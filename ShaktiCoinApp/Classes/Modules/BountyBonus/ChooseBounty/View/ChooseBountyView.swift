//
//  ChooseBountyView.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 10/25/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class ChooseBountyView: StackView, ChooseBountyViewProtocol {
    var presenter: ChooseBountyPresenterProtocol?
    
    @IBOutlet weak var datePickerBottom: NSLayoutConstraint?
    @IBOutlet weak var datePicker: UIDatePicker?
    @IBOutlet weak var datePickerBackground: UIView?
    
    @IBOutlet weak var pickerDoneButton: UIButton?
    @IBOutlet weak var pickerCancelButton: UIButton?
    
    // MARK: - Declarations
    
    @IBOutlet weak var titleLabel: UILabel? {
        didSet {
            titleLabel?.text = "ClaimBounty".localized
            titleLabel?.numberOfLines = 0
            titleLabel?.lineBreakMode = .byTruncatingTail // or .byWrappingWord
            titleLabel?.font = UIFont.lato(fontSize: 24, type: .Light)
            titleLabel?.textColor = .white
            titleLabel?.textAlignment = .center
        }
    }
    
    @IBOutlet weak var subtitleLabel: UILabel? {
        didSet {
            subtitleLabel?.text = "ChooseYourBegining".localized
            subtitleLabel?.font = UIFont.lato(fontSize: 12, type: .Bold)
            subtitleLabel?.textColor = .white
            subtitleLabel?.textAlignment = .center
        }
    }
    
    lazy var optionsView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(greaterThanOrEqualToConstant: 245).isActive = true
        
        view.addSubview(stairView)
        view.addSubview(leftCircleViewContainer)
        view.addSubview(rightCircleViewContainer)
        
        _ = leftCircleViewContainer.anchor(nil, left: view.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 18, bottomConstant: 0, rightConstant: 0, widthConstant: 132, heightConstant: 132)
        _ = leftCircleViewContainer.centralizeY(view.centerYAnchor)
        
        _ = rightCircleViewContainer.anchor(nil, left: nil, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 18, widthConstant: 132, heightConstant: 132)
        _ = rightCircleViewContainer.centralizeY(view.centerYAnchor)
        
        _ = stairView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        _ = stairView.centralizeX(view.centerXAnchor)
        stairView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        stairView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        return view
    }()
    
    lazy var stairView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.spacing = 0
        return sv
    }()
    
    lazy var claimBountyBtn = GoldButtonModule()
    
    lazy var bottomText: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = UIColor.clear
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.isUserInteractionEnabled = false
        return textView
    }()
    
    lazy var viewSpecialLbl: UILabel = {
        let label = UILabel()
        label.text = "ViewSpecial".localized + " "
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 10)
        return label
    }()
    
    lazy var promotionDetailLink: UILabel = {
        let label = UILabel()
        label.text = "PromotionDetails".localized.lowercased()
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 10)
        label.underline()
        return label
    }()

    lazy var leftCircleViewContainer : UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.30
        return view
    }()
    
    lazy var rightCircleViewContainer : UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.30
        return view
    }()
    
    
    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        hideDatePicker()
        
        if let date = Calendar.current.date(byAdding: .year, value: -18, to: Date()) {
            datePicker?.date = date
            datePicker?.maximumDate = date
        }
        
        pickerDoneButton?.setTitle("Done".localized, for: .normal)
        pickerCancelButton?.setTitle("Cancel".localized, for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showLoader()
        presenter?.perform(.loaded)
    }
    
    override func viewDidLayoutSubviews() {
        
        leftCircleViewContainer.layer.cornerRadius = leftCircleViewContainer.frame.width/2
        leftCircleViewContainer.clipsToBounds = true
        
        rightCircleViewContainer.layer.cornerRadius = rightCircleViewContainer.frame.width/2
        rightCircleViewContainer.clipsToBounds = true
    }
    
    // MARK: - Methods
    @objc private func onOptionButtonClick(_ button: SMButton) {

        if button.isActive { return }
        
        button.isActive = true
        button.setImage(UIImage(named: "Active"), for: .normal)
           
        selected = data[button.seqNo]
        
        for case let eachButton as SMButton in stairView.subviews {
            if eachButton.seqNo != button.seqNo {
                eachButton.isActive = false
                eachButton.setImage(UIImage(named: "Inactive"), for: .normal)
            }
        }
        
        UIView.animate(withDuration: CATransaction.animationDuration()) {
            self.stairView.layoutIfNeeded()
        }
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.leftCircleViewContainer.frame.origin.y -= 100
            self.leftCircleViewContainer.alpha = 0.0
            
            self.rightCircleViewContainer.frame.origin.y += 100
            self.rightCircleViewContainer.alpha = 0.0
        }, completion: { (finished: Bool) in
            self.updateLabelsWithSelected()
            self.resetValues()
        })
    }
      
       private func resetValues() {
           
           self.leftCircleViewContainer.frame.origin.y += 130
           self.rightCircleViewContainer.frame.origin.y -= 130
           
           UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
               
               self.leftCircleViewContainer.frame.origin.y -= 30
               self.leftCircleViewContainer.alpha = 0.5
               
               self.rightCircleViewContainer.frame.origin.y += 30
               self.rightCircleViewContainer.alpha = 0.5

           }, completion: nil)
       }
    
    private func addVerticalSpace(_ height: CGFloat) {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.heightAnchor.constraint(equalToConstant: height).isActive = true
        addView(view)
    }
    
    private let insets = UIEdgeInsets.horizontal(22)
    private func setupViews() {
        
        addVerticalSpace(65)
        addView(titleLabel!, edgeInsets: insets)
        addVerticalSpace(20)
        addView(subtitleLabel!, edgeInsets: insets)
        addVerticalSpace(30)
        addView(optionsView)
        addVerticalSpace(40)
        addView(bottomText, edgeInsets: insets)
        addVerticalSpace(28)
        addView(claimBountyBtn, edgeInsets: insets)
        addVerticalSpace(24)
        
        let viewModel = ButtonModuleModel(title: "ClaimThisBounty".localized, enabled: false, callback: nil)
        
        claimBountyBtn.populate(model: viewModel)
        
        let left = UIView()
        left.backgroundColor = .clear
        
        let right = UIView()
        right.backgroundColor = .clear
        
        let bottomView = UIStackView()
        bottomView.distribution = .fill
        bottomView.axis = .horizontal
        bottomView.addArrangedSubview(left)
        bottomView.addArrangedSubview(viewSpecialLbl)
        bottomView.addArrangedSubview(promotionDetailLink)
        bottomView.addArrangedSubview(right)
        
        right.widthAnchor.constraint(equalTo: left.widthAnchor, multiplier: 1).isActive = true
        
        addView(bottomView, edgeInsets: insets)
        
        addVerticalSpace(15)
        
        let module = TextButtonModule()
        module.populate(claimLaterLink)
        addView(module, edgeInsets: UIEdgeInsets.horizontal(22))
        
        addVerticalSpace(28)
        
        let leftValueOfLabel = UILabel()
        leftValueOfLabel.text = "ValueOf".localized
        leftValueOfLabel.textColor = .white
        leftValueOfLabel.font = UIFont.lato(fontSize: 10)
        
        let rightExpiresOnLabel = UILabel()
        rightExpiresOnLabel.text = "ExpiresOn".localized
        rightExpiresOnLabel.textColor = .white
        rightExpiresOnLabel.font = UIFont.lato(fontSize: 10)
        
        leftCircleViewContainer.addSubview(leftValueOfLabel)
        rightCircleViewContainer.addSubview(rightExpiresOnLabel)
        
        _ = leftValueOfLabel.anchorToTop(leftCircleViewContainer.topAnchor, topConstant: 25)
        _ = leftValueOfLabel.centralizeX(leftCircleViewContainer.centerXAnchor)
        
        _ = rightExpiresOnLabel.anchorToTop(rightCircleViewContainer.topAnchor, topConstant: 25)
        _ = rightExpiresOnLabel.centralizeX(rightCircleViewContainer.centerXAnchor)
        
        leftMoneyLabel.textColor = UIColor.mainColor()
        leftMoneyLabel.font = UIFont.lato(fontSize: 24, type: .Bold)
        
        rightDateLabel.textColor = UIColor.mainColor()
        rightDateLabel.font = UIFont.lato(fontSize: 24, type: .Bold)
        
        leftCircleViewContainer.addSubview(leftMoneyLabel)
        rightCircleViewContainer.addSubview(rightDateLabel)
        
        _ = leftMoneyLabel.centralizeX(leftCircleViewContainer.centerXAnchor)
        _ = leftMoneyLabel.centralizeY(leftCircleViewContainer.centerYAnchor)
        
        _ = rightDateLabel.centralizeX(rightCircleViewContainer.centerXAnchor)
        _ = rightDateLabel.centralizeY(rightCircleViewContainer.centerYAnchor)
        
        leftSXELabel.textColor = .white
        leftSXELabel.font = UIFont.lato(fontSize: 10)
        
        rightYearLabel.textColor = .white
        rightYearLabel.font = UIFont.lato(fontSize: 10)
        
        leftCircleViewContainer.addSubview(leftSXELabel)
        rightCircleViewContainer.addSubview(rightYearLabel)
        
        _ = leftSXELabel.anchorToBottom(leftCircleViewContainer.bottomAnchor, bottomConstant: -25)
        _ = leftSXELabel.centralizeX(leftCircleViewContainer.centerXAnchor)
        
        _ = rightYearLabel.anchorToBottom(rightCircleViewContainer.bottomAnchor, bottomConstant: -25)
        _ = rightYearLabel.centralizeX(rightCircleViewContainer.centerXAnchor)
    }

    private var claimLaterLink: TextButtonModel {
        let title = TextViewModel(style: TextStyle.lato10.underline(true).color(.white), value: "Claim Bounty Bonus later".localized)
        return TextButtonModel(title: title, enabled: true) { [weak self] in
            self?.presenter?.perform(.claimLater)
        }
    }
    
    // MARK: - Events
    
    func populate(_ state: ChooseBountyState) {
        switch state {
        case .error(let model):
            hideLoader()
            showAlert(errorMessage: model.value.string)
        case .offers(let model):
            hideLoader()
            setupOptionView(model)
        case .loaded: hideLoader()
        case .loading: showLoader()
        }
    }
    
    private var data: [BonusModel] = []
    private var selected: BonusModel?
    private let leftMoneyLabel = UILabel()
    private let leftSXELabel = UILabel()
    private let rightDateLabel = UILabel()
    private let rightYearLabel = UILabel()
    
    private func setupOptionView(_ model: DataArrayModel<BonusModel>) {
        data = model.data.sorted(by: { $0.expiryDate < $1.expiryDate })
        
        if selected == nil {
            selected = data.first
        }
        
        stairView.removeAllViews()
        
        for (index, item) in data.enumerated() {
            let current = item.bonusId == selected?.bonusId
            let optionButton = SMButton()
            let selectedImage: String = current ? "Active" : "Inactive"
            optionButton.seqNo = index
            optionButton.isActive = current
            optionButton.setImage(UIImage(named: selectedImage), for: .normal)
            optionButton.addTarget(self, action: #selector(onOptionButtonClick(_:)), for: .touchUpInside)
            stairView.addArrangedSubview(optionButton)
        }
        
        updateLabelsWithSelected()
    }
    
    private func updateLabelsWithSelected() {
        leftMoneyLabel.text = ""
        rightDateLabel.text = ""
        leftSXELabel.text = ""
        rightYearLabel.text = ""
        
        guard let selected = selected else { return }
        
        let viewModel = ButtonModuleModel(title: "ClaimThisBounty".localized, enabled: !selected.expired) { [weak self] in
            self?.askBirthday(offerId: selected.bonusId)
        }
        
        claimBountyBtn.populate(model: viewModel)
        
        leftSXELabel.text = "\(selected.shaktiCoins).00 SXE"
        leftMoneyLabel.text = "$\(selected.dollarAmount)"
        
        let format = DateFormatter()
        format.dateFormat = "YYYY-MM-dd"
        if let date = format.date(from: selected.expiryDate) {
            format.dateFormat = "MMM d"
            rightDateLabel.text = format.string(from: date)
            format.dateFormat = "YYYY"
            rightYearLabel.text = format.string(from: date)
        }
        
        let attributedText = Util.NSMAttrString(text: "_BBTextInfo_SpecialLimited".localized, font: UIFont.lato(fontSize: 14, type: .Regular))
        
        let string = String(format: "_BBTextInfo_CreateYourId".localized, "$\(selected.dollarAmount).00")
        attributedText.append(Util.NSAttrString(text: "\n\n" + string, font: UIFont.lato(fontSize: 14)))
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        
        let length = attributedText.string.count
        attributedText.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: length))
        
        bottomText.attributedText = attributedText
    }
    
    private func hideDatePicker() {
        datePickerBottom?.constant = ((datePickerBackground?.frame.height ?? 0) +
        (UIApplication.shared.windows.first?.safeAreaInsets.bottom ?? 0)) * -1
    }
    
    func askBirthday(offerId: String) {
        //Formate Date
        datePickerBottom?.constant = 0
        UIView.animate(withDuration: CATransaction.animationDuration()) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func onDateDone() {
        //For date formate
        hideDatePicker()
        
        UIView.animate(withDuration: CATransaction.animationDuration(), animations: {
            self.view.layoutIfNeeded()
        }, completion: { _ in
            guard let selected = self.selected,
                let date = self.datePicker?.date else { return }
            self.presenter?.perform(.claimSxeBonus(selected.bonusId, date))
        })
    }
    
    @IBAction func onDateCancel() {
        hideDatePicker()
        UIView.animate(withDuration: CATransaction.animationDuration()) {
            self.view.layoutIfNeeded()
        }
    }
}
