//
//  ChooseBountyRepository.swift
//  ShaktiCoinApp
//
//  Created by sperev on 21/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class ChooseBountyRepository: ChooseBountyRepositoryProtocol {
    var dataManager: BountyBonusDataManagerProtocol?
    var localDataManager: BountyBonusLocalDataManagerProtocol?
    
    static var shared: ChooseBountyRepository = {
        let repository = ChooseBountyRepository()
        repository.dataManager = BountyBonusDataManager()
        repository.localDataManager = BountyBonusLocalDataManager.shared
        return repository
    }()
    
    var bountyBonus: GenesisBonusModel? {
        return localDataManager?.genesisBonusModel
    }
    
    func requestAllOffers(callback: @escaping (Result<DataArrayModel<BonusModel>, ErrorModel>) -> Void) {
        dataManager?.requestAllOffers {
            switch $0 {
            case .success(let obj):
                if let model = try? Mapper<DataArrayModel<BonusModel>>.map(object: obj) {
                    callback(.success(model))
                } else {
                    callback(.failure(ErrorModel(ShaktiError.General.missingData)))
                }
            case .failure(let error):
                callback(.failure(error))
            }
        }
    }
    
    func claimBonusBounty(genesisBonusId: String, birthday: Date, callback: @escaping (Result<GenesisBonusModel, ErrorModel>) -> Void) {
        dataManager?.claimBonusBounty(genesisBonusId: genesisBonusId, birthday: birthday) { [weak self] in
            switch $0 {
            case .success(let obj):
                if let model = try? Mapper<DataModel<GenesisBonusModel>>.map(object: obj) {
                    self?.localDataManager?.set(model.data)
                    callback(.success(model.data))
                } else {
                    callback(.failure(ErrorModel(ShaktiError.General.missingData)))
                }
            case .failure(let error):
                callback(.failure(error))
            }
        }
    }
    
    func bonusBounty(callback: @escaping (Result<GenesisBonusModel, ErrorModel>) -> Void) {
        if let model = localDataManager?.genesisBonusModel {
            callback(.success(model))
            return
        }
        
        dataManager?.bonusBounties(callback: { [weak self] in
            switch $0 {
            case .success(let obj):
                if let model = try? Mapper<DataModel<GenesisBonusModel>>.map(object: obj) {
                    self?.localDataManager?.set(model.data)
                    callback(.success(model.data))
                } else {
                    callback(.failure(ErrorModel(ShaktiError.General.missingData)))
                }
            case .failure(let error):
                callback(.failure(error))
            }
        })
    }
}
