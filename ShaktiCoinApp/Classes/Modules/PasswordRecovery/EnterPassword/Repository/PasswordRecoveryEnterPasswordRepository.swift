//
//  PasswordRecoveryEnterPasswordRepository.swift
//  ShaktiCoinApp
//
//  Created by sperev on 17/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class PasswordRecoveryEnterPasswordRepository: PasswordRecoveryEnterPasswordRepositoryProtocol {
    var dataManager: PasswordRecoveryEnterPasswordDataManagerProtocol?
    
    func set(password: String, callback: @escaping (Result<ResponseModel, ErrorModel>) -> Void) {
        guard let phone = CurrentUserDataManager.shared.phone else {
            callback(.failure(ErrorModel(ShaktiError.General.unknown)))
            return
        }
        
        guard let email = CurrentUserDataManager.shared.email else {
            callback(.failure(ErrorModel(ShaktiError.General.unknown)))
            return
        }
        
        dataManager?.createUser(email: email, phone: phone, password: password, callback: {
            switch $0 {
            case .success(let json):
                if let model = try? Mapper<ResponseModel>.map(object: json) {
                    callback(.success(model))
                } else {
                    callback(.failure(ErrorModel(MappingError.unknown)))
                }
            case .failure(let error):
                callback(.failure(error))
            }
        })
    }
}
