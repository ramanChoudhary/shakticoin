//
//  PasswordRecoveryEnterPasswordPresenter.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

final class PasswordRecoveryEnterPasswordPresenter: PasswordRecoveryEnterPasswordPresenterProtocol, PasswordRecoveryEnterPasswordInteractorOutputProtocol {
    weak var view: PasswordRecoveryEnterPasswordViewProtocol?
    var interactor: PasswordRecoveryEnterPasswordInteractorProtocol?
    var wireframe: PasswordRecoveryEnterPasswordWireframeProtocol?
    
    private var mainButton: ButtonModuleModel {
        return ButtonModuleModel(title: "password_recovery.enter_password.save".localized, enabled: true, callback: nil)
    }
    
    private var textButton: TextButtonModel {
        let title = TextViewModel(style: TextStyle.latoSemiBold14.color(.white), value: "password_recovery.enter_password.cancel".localized)
        return TextButtonModel(title: title, enabled: true) { [weak self] in
            self?.wireframe?.navigate(.close)
        }
    }
    
    private var title: TextViewModel {
        return TextViewModel(style: TextStyle.latoLight24.alignment(.center), value: "password_recovery.enter_password.title".localized)
    }
    
    private var subtitle: TextViewModel {
        let subtitle = "password_recovery.enter_password.subtitle"
        return TextViewModel(style: TextStyle.latoSemiBold14.alignment(.center), value: subtitle.localized)
    }
    
    private var imageName: String {
        return "Lock Icon"
    }
    
    private var pass1TextViewModel: TextFieldViewModel {
        let textStyle = TextStyle.lato14.color(ColorStyle.white.color)
        return TextFieldViewModel(textStyle: textStyle, textLocalized: "", placeholderStyle: textStyle, placeholderLocalized: "password_recovery.enter_password.current_password".localized, keyboardType: .alphabet, isSecureTextEntry: true)
    }
    
    private var pass2TextViewModel: TextFieldViewModel {
        let textStyle = TextStyle.lato14.color(ColorStyle.white.color)
        return TextFieldViewModel(textStyle: textStyle, textLocalized: "", placeholderStyle: textStyle, placeholderLocalized: "password_recovery.enter_password.new_password".localized, keyboardType: .alphabet, isSecureTextEntry: true)
    }
    
    func perform(_ action: PasswordRecoveryEnterPasswordViewAction) {
        switch action {
        case .loaded:
            
            populate(resend: false)
            
            
            view?.populate(.setPass1TextField(pass1TextViewModel))
            view?.populate(.setPass2TextField(pass2TextViewModel))
            view?.populate(.setPasswordStrength(.enterPassword(upperCharacters: false, numbers: false, lowerCharacters: false, symbols: false)))
            
        case .continue(let pass1, let pass2):
            view?.populate(.loading)
            interactor?.do(.setPassword(pass1, pass2))
        case .passwordChanged(let text):
            interactor?.do(.checkPasswordStrength(text))
        }
    }
    
    private func populate(resend: Bool) {
        view?.populate(.setSaveButton(mainButton))
        view?.populate(.setCancelButton(textButton))
        view?.populate(.setImage(imageName))
        view?.populate(.setTitle(title))
        view?.populate(.setSubtitle(subtitle))
    }
    
    func handle(_ result: PasswordRecoveryEnterPasswordResult) {
        switch result {
        case .passwordSet:
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
                self.wireframe?.navigate(.login)
            }
        case .failed(let error):
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
                self.view?.populate(.error(error.localizedDescription))
            }
        case .passwordStrength(let upp, let num, let low, let sym):
            self.view?.populate(.setPasswordStrength(.enterPassword(upperCharacters: upp, numbers: num, lowerCharacters: low, symbols: sym)))
        }
    }
}
