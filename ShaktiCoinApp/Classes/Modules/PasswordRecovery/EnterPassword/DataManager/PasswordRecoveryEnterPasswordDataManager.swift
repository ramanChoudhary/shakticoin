//
//  PasswordRecoveryEnterPasswordDataManager.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class PasswordRecoveryEnterPasswordDataManager: PasswordRecoveryEnterPasswordDataManagerProtocol {
    var dataSource = RemoteDataSource.shared
    func createUser(email: String, phone: String, password: String, callback: @escaping APIDataManagerObjectResultBlock) {
        let params: JSONObject = [
            "mobileNo": phone,
            "email": email,
            "password": password
        ]
        dataSource.requestObject(forEndpoint: Endpoint.Onboard.changePassword, parameters: params, result: callback)
    }
}
