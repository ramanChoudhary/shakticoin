//
//  PasswordRecoveryEnterPhoneCodeView.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

final class PasswordRecoveryEnterPasswordView: UIViewController, PasswordRecoveryEnterPasswordViewProtocol {
    var presenter: PasswordRecoveryEnterPasswordPresenterProtocol?
    
    @IBOutlet weak var imageView: UIImageView?
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var subtitleLabel: UILabel?
    @IBOutlet weak var pass1FieldModule: TextFieldModule?
    @IBOutlet weak var pass2FieldModule: TextFieldModule?
    @IBOutlet weak var passwordStrengthModule: PasswordStrengthModule?
    @IBOutlet weak var cancelButtonModule: TextButtonModule?
    @IBOutlet weak var saveButtonModule: GoldButtonModule?
    
    private var insets = UIEdgeInsets.horizontal(22).vertical(12)
    private var spaceInsets = UIEdgeInsets.horizontal(22).vertical(6)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.perform(.loaded)
    }
    
    func populate(_ state: PasswordRecoveryEnterPasswordViewState) {
        switch state {
        case .loaded: hideLoader()
        case .loading: showLoader()
        case .setTitle(let viewModel):
            titleLabel?.setup(viewModel)
        case .setSubtitle(let viewModel):
            subtitleLabel?.setup(viewModel)
        case .setSaveButton(var viewModel):
            viewModel.callback = { [weak self] in
                guard let textField1 = self?.pass1FieldModule?.textField,
                    let pass1 = textField1.text else { return }
                guard let textField2 = self?.pass2FieldModule?.textField,
                    let pass2 = textField2.text else { return }
                
                textField1.resignFirstResponder()
                textField2.resignFirstResponder()
                
                self?.presenter?.perform(.continue(pass1, pass2))
            }
            
            saveButtonModule?.populate(model: viewModel)
        case .setCancelButton(let viewModel):
            cancelButtonModule?.populate(viewModel)
        case .setPass1TextField(let viewModel):
            pass1FieldModule?.populate(viewModel)
            pass1FieldModule?.textWillChangedCallback = { [weak self] in
                self?.presenter?.perform(.passwordChanged($0))
            }
            
        case .setPass2TextField(let viewModel):
            pass2FieldModule?.populate(viewModel)
            
        case .error(let localizedError):
            showAlert(errorMessage: localizedError)
        case .alert(let model):
            showAlert(model)
        case .setPasswordStrength(let viewModel):
            passwordStrengthModule?.populate(viewModel)
        case .setImage(let name):
            imageView?.image = UIImage(named: name)
        }
    }
}
