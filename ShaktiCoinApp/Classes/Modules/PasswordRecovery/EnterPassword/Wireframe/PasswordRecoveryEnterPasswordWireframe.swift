//
//  PasswordRecoveryEnterPasswordWireframe.swift
//  ShaktiCoinApp
//
//  Created by sperev on 17/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

final class PasswordRecoveryEnterPasswordWireframe: PasswordRecoveryEnterPasswordWireframeProtocol {
    class func createModule() -> UIViewController {
        
        let view = PasswordRecoveryEnterPasswordView()
        let presenter =  PasswordRecoveryEnterPasswordPresenter()
        let interactor = PasswordRecoveryEnterPasswordInteractor()
        let repository = PasswordRecoveryEnterPasswordRepository()
        let dataManager = PasswordRecoveryEnterPasswordDataManager()
        let wireframe = PasswordRecoveryEnterPasswordWireframe()
        
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.wireframe = wireframe
        interactor.presenter = presenter
        interactor.repository = repository
        repository.dataManager = dataManager
        
        return view
    }
    
    func navigate(_ route: PasswordRecoveryEnterPasswordRoute) {
        switch route {
        case .login, .close:
            let delegate = UIApplication.shared.delegate as? AppDelegate
            delegate?.window?.rootViewController = LoginViewController()
        }
    }
}
