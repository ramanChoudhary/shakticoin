//
//  PasswordRecoveryEnterPasswordWireframeProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 17/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

enum PasswordRecoveryEnterPasswordRoute {
    case login
    case close
}

protocol PasswordRecoveryEnterPasswordWireframeProtocol {
    func navigate(_ route: PasswordRecoveryEnterPasswordRoute)
}
