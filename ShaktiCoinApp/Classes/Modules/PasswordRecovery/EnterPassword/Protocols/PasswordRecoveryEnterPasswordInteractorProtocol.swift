//
//  PasswordRecoveryEnterPasswordInteractorProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 17/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum PasswordRecoveryEnterPasswordJob {
    case setPassword(String, String)
    case checkPasswordStrength(String?)
}

enum PasswordRecoveryEnterPasswordResult {
    case passwordSet
    case failed(ErrorModel)
    case passwordStrength(Bool, Bool, Bool, Bool)
}

protocol PasswordRecoveryEnterPasswordInteractorProtocol {
    func `do`(_ job: PasswordRecoveryEnterPasswordJob)
}

protocol PasswordRecoveryEnterPasswordInteractorOutputProtocol: class {
    func handle(_ result: PasswordRecoveryEnterPasswordResult)
}
