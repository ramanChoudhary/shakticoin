//
//  PasswordRecoveryEnterPasswordViewProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 17/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum PasswordRecoveryEnterPasswordViewState {
    case loading
    case loaded
    
    case error(String)
    case alert(AlertModel)
    
    case setImage(String)
    case setTitle(TextViewModel)
    case setSubtitle(TextViewModel)
    
    case setPass1TextField(TextFieldViewModel)
    case setPass2TextField(TextFieldViewModel)
    
    case setPasswordStrength(PasswordStrengthViewModel)
    case setSaveButton(ButtonModuleModel)
    case setCancelButton(TextButtonModel)
}

protocol PasswordRecoveryEnterPasswordViewProtocol: class {
    func populate(_ state: PasswordRecoveryEnterPasswordViewState)
}
