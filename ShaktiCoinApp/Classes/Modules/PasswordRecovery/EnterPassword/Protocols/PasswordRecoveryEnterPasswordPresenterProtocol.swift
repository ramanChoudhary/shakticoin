//
//  PasswordRecoveryEnterPasswordPresenterProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 17/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum PasswordRecoveryEnterPasswordViewAction {
    case loaded
    case `continue`(String, String)
    case passwordChanged(String?)
}

protocol PasswordRecoveryEnterPasswordPresenterProtocol {
    func perform(_ action: PasswordRecoveryEnterPasswordViewAction)
}
