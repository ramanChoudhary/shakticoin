//
//  PasswordRecoveryEnterPasswordDataManagerProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 17/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

protocol PasswordRecoveryEnterPasswordDataManagerProtocol {
    func createUser(email: String, phone: String, password: String, callback: @escaping APIDataManagerObjectResultBlock)
}
