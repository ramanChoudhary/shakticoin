//
//  PasswordRecoveryEnterPasswordInteractor.swift
//  ShaktiCoinApp
//
//  Created by sperev on 17/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class PasswordRecoveryEnterPasswordInteractor: PasswordRecoveryEnterPasswordInteractorProtocol {
    weak var presenter: PasswordRecoveryEnterPasswordInteractorOutputProtocol?
    var repository: PasswordRecoveryEnterPasswordRepositoryProtocol?
    
    func `do`(_ job: PasswordRecoveryEnterPasswordJob) {
        switch job {
        case .setPassword(let pass1, let pass2):
            let strength = pass1.passwordStrength
            guard strength.0 && strength.1 && strength.2 && strength.3 &&
                pass1 == pass2 else {
                let error = ErrorModel(ShaktiError.General.unknown)
                presenter?.handle(.failed(error))
                return
            }
            request(pass1)
            
        case .checkPasswordStrength(let text):
            var upp = false
            var low = false
            var num = false
            var sym = false
            
            if let text = text {
                (upp, num, low, sym) = text.passwordStrength
            }
            
            presenter?.handle(.passwordStrength(upp, num, low, sym))
        }
    }
    
    private func request(_ password: String) {
        repository?.set(password: password, callback: { [weak self] in
            switch $0 {
            case .success:
                self?.presenter?.handle(.passwordSet)
            case .failure(let error):
                self?.presenter?.handle(.failed(error))
            }
        })            
    }
}
