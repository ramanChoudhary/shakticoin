//
//  PasswordRecoveryEnterEmailPresenter.swift
//  ShaktiCoinApp
//
//  Created by sperev on 01/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

final class PasswordRecoveryEnterEmailPresenter: PasswordRecoveryEnterEmailPresenterProtocol, PasswordRecoveryEnterEmailInteractorOutputProtocol {
    weak var view: PasswordRecoveryEnterEmailViewProtocol?
    var interactor: PasswordRecoveryEnterEmailInteractorProtocol?
    var wireframe: PasswordRecoveryEnterEmailWireframeProtocol?
    
    private func button(resend: Bool) -> ButtonModuleModel {
        return ButtonModuleModel(title: (resend ? "password_recovery.enter_email.resend": "password_recovery.enter_email.reset_password").localized, enabled: true, callback: nil)
    }
    
    private var emailTextViewModel: TextFieldViewModel {
        let textStyle = TextStyle.lato14.color(ColorStyle.white.color)
        return TextFieldViewModel(textStyle: textStyle, textLocalized: interactor?.savedEmail ?? "", placeholderStyle: textStyle, placeholderLocalized: "password_recovery.enter_email.enter_email".localized, keyboardType: .emailAddress, isSecureTextEntry: false)
    }
    
    private var textButton: TextButtonModel {
        let title = TextViewModel(style: TextStyle.latoSemiBold14.color(.white), value: "password_recovery.enter_email.cancel".localized)
        return TextButtonModel(title: title, enabled: true) { [weak self] in
            self?.wireframe?.navigate(.close)
        }
    }
    
    private func title(resend: Bool = false) -> TextViewModel {
        let title = resend ? "password_recovery.enter_email.title_resend" : "password_recovery.enter_email.title"
        return TextViewModel(style: TextStyle.latoLight24.alignment(.center), value: title.localized)
    }
    
    private func subtitle(resend: Bool = false) -> TextViewModel {
        let subtitle = resend ? "password_recovery.enter_email.subtitle_resend" : "password_recovery.enter_email.subtitle"
        return TextViewModel(style: TextStyle.latoSemiBold14.alignment(.center), value: subtitle.localized)
    }
    
    private func image(resend: Bool = false) -> String {
        return resend ? "Lock Icon" : "Sent"
    }
    
    func perform(_ action: PasswordRecoveryEnterEmailViewAction) {
        switch action {
        case .loaded:
            populate(resend: false)
            view?.populate(.setTextField(emailTextViewModel))
            view?.populate(.setTextButton(textButton))
        case .continue(let email):
            view?.populate(.loading)
            interactor?.do(.request(email))
        }
    }
    
    private func populate(resend: Bool) {
        view?.populate(.setSendButton(button(resend: resend)))
        view?.populate(.setImage(image(resend: resend)))
        view?.populate(.setTitle(title(resend: resend)))
        view?.populate(.setSubtitle(subtitle(resend: resend)))
    }
    
    func handle(_ result: PasswordRecoveryEnterEmailResult) {
        switch result {
        case .requested:
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
                self.populate(resend: true)
            }
        case .failed(let error):
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
                self.view?.populate(.error(error.localizedDescription))
            }
        }
    }
}
