//
//  PasswordRecoveryEnterEmailView.swift
//  ShaktiCoinApp
//
//  Created by sperev on 01/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

final class PasswordRecoveryEnterEmailView: UIViewController, PasswordRecoveryEnterEmailViewProtocol {
    var presenter: PasswordRecoveryEnterEmailPresenterProtocol?
    
    @IBOutlet weak var imageView: UIImageView?
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var subtitleLabel: UILabel?
    @IBOutlet weak var textFieldModule: TextFieldModule?
    @IBOutlet weak var cancelButtonModule: TextButtonModule?
    @IBOutlet weak var sendButtonModule: GoldButtonModule?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.perform(.loaded)
    }
    
    func populate(_ state: PasswordRecoveryEnterEmailViewState) {
        switch state {
        case .loaded: hideLoader()
        case .loading: showLoader()
        case .setSendButton(var viewModel):
            viewModel.callback = { [weak self] in
                guard let textField = self?.textFieldModule?.textField,
                    let text = textField.text else { return }
                textField.resignFirstResponder()
                self?.presenter?.perform(.continue(text))
            }
            
            sendButtonModule?.populate(model: viewModel)
        case .setTextField(let viewModel):
            textFieldModule?.populate(viewModel)
        case .error(let localizedError):
            showAlert(errorMessage: localizedError)
        case .alert(let model):
            showAlert(model)
        case .setTextButton(let viewModel):
            cancelButtonModule?.populate(viewModel)
        case .setImage(let imageName):
            imageView?.image = UIImage(named: imageName)
        case .setTitle(let viewModel):
            titleLabel?.setup(viewModel)
        case .setSubtitle(let viewModel):
            subtitleLabel?.setup(viewModel)
        }
    }
}
