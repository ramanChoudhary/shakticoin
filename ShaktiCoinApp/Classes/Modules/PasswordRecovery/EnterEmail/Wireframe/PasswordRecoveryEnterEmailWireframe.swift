//
//  PasswordRecoveryEnterEmailWireframe.swift
//  ShaktiCoinApp
//
//  Created by sperev on 01/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

final class PasswordRecoveryEnterEmailWireframe: PasswordRecoveryEnterEmailWireframeProtocol {
    class func createModule() -> UIViewController {
        
        let view = PasswordRecoveryEnterEmailView()
        let presenter =  PasswordRecoveryEnterEmailPresenter()
        let interactor = PasswordRecoveryEnterEmailInteractor()
        let repository = PasswordRecoveryEnterEmailRepository()
        let dataManager = PasswordRecoveryEnterEmailDataManager()
        let wireframe = PasswordRecoveryEnterEmailWireframe()
        
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.wireframe = wireframe
        interactor.presenter = presenter
        interactor.repository = repository
        repository.dataManager = dataManager
        
        return view
    }
    
    func navigate(_ route: PasswordRecoveryEnterEmailRoute) {
        switch route {
        case .close:
            let delegate = UIApplication.shared.delegate as? AppDelegate
            delegate?.window?.rootViewController = LoginViewController()
        }
    }
}
