//
//  PasswordRecoveryEnterEmailRepository.swift
//  ShaktiCoinApp
//
//  Created by sperev on 01/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class PasswordRecoveryEnterEmailRepository: PasswordRecoveryEnterEmailRepositoryProtocol {
    
    var savedEmail: String? {
        return CurrentUserDataManager.shared.email
    }
    
    var dataManager: PasswordRecoveryEnterEmailDataManagerProtocol?
    
    func request(email: String, callback: @escaping (Result<ResponseModel, ErrorModel>) -> Void) {
        dataManager?.request(email: email, callback: {
            switch $0 {
            case .success(let json):
                if let model = try? Mapper<ResponseModel>.map(object: json) {
                    callback(.success(model))
                } else {
                    callback(.failure(ErrorModel(MappingError.unknown)))
                }
            case .failure(let error):
                callback(.failure(error))
            }
        })
    }
}
