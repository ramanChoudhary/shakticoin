//
//  PasswordRecoveryEnterEmailDataManager.swift
//  ShaktiCoinApp
//
//  Created by sperev on 01/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class PasswordRecoveryEnterEmailDataManager: PasswordRecoveryEnterEmailDataManagerProtocol {
    var dataSource = RemoteDataSource.shared
    func request(email: String, callback: @escaping APIDataManagerObjectResultBlock) {
        let params: JSONObject = ["email": email]
        dataSource.requestObject(forEndpoint: Endpoint.EmailVerification.forgotPassword, parameters: params, result: callback)
    }
}
