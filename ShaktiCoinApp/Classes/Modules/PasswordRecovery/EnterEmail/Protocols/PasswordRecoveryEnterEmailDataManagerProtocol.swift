//
//  PasswordRecoveryEnterEmailDataManagerProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 01/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

protocol PasswordRecoveryEnterEmailDataManagerProtocol {
    func request(email: String, callback: @escaping APIDataManagerObjectResultBlock)
}
