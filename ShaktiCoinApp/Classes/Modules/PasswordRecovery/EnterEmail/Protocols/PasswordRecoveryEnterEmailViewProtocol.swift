//
//  PasswordRecoveryEnterEmailViewProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 01/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum PasswordRecoveryEnterEmailViewState {
    case loading
    case loaded
    case setSendButton(ButtonModuleModel)
    case setTextField(TextFieldViewModel)
    case error(String)
    case alert(AlertModel)
    case setTextButton(TextButtonModel)
    case setTitle(TextViewModel)
    case setSubtitle(TextViewModel)
    case setImage(String)
}

protocol PasswordRecoveryEnterEmailViewProtocol: class {
    func populate(_ state: PasswordRecoveryEnterEmailViewState)
}
