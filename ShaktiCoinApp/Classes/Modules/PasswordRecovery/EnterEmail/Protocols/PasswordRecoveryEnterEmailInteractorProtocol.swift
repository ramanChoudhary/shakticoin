//
//  PasswordRecoveryEnterEmailInteractorProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 01/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum PasswordRecoveryEnterEmailJob {
    case request(String)
}

enum PasswordRecoveryEnterEmailResult {
    case requested
    case failed(ErrorModel)
}

protocol PasswordRecoveryEnterEmailInteractorProtocol {
    var savedEmail: String? { get }
    func `do`(_ job: PasswordRecoveryEnterEmailJob)
}

protocol PasswordRecoveryEnterEmailInteractorOutputProtocol: class {
    func handle(_ result: PasswordRecoveryEnterEmailResult)
}
