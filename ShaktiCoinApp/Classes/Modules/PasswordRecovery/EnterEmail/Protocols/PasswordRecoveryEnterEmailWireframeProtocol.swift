//
//  PasswordRecoveryEnterEmailWireframeProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 01/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

enum PasswordRecoveryEnterEmailRoute {
    case close
}

protocol PasswordRecoveryEnterEmailWireframeProtocol {
    func navigate(_ route: PasswordRecoveryEnterEmailRoute)
}
