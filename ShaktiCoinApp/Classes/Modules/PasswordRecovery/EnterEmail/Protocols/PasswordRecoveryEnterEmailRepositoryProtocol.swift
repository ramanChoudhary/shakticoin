//
//  PasswordRecoveryEnterEmailRepositoryProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 01/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

protocol PasswordRecoveryEnterEmailRepositoryProtocol {
    var savedEmail: String? { get }
    func request(email: String, callback: @escaping (Result<ResponseModel, ErrorModel>) -> Void)
}
