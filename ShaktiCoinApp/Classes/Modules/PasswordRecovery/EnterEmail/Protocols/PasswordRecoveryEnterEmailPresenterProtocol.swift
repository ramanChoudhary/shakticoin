//
//  PasswordRecoveryEnterEmailPresenterProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 01/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum PasswordRecoveryEnterEmailViewAction {
    case loaded
    case `continue`(String)
}

protocol PasswordRecoveryEnterEmailPresenterProtocol {
    func perform(_ action: PasswordRecoveryEnterEmailViewAction)
}
