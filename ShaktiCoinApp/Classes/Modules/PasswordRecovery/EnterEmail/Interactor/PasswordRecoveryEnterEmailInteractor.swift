//
//  PasswordRecoveryEnterEmailInteractor.swift
//  ShaktiCoinApp
//
//  Created by sperev on 01/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class PasswordRecoveryEnterEmailInteractor: PasswordRecoveryEnterEmailInteractorProtocol {
    weak var presenter: PasswordRecoveryEnterEmailInteractorOutputProtocol?
    var repository: PasswordRecoveryEnterEmailRepositoryProtocol?
    
    var savedEmail: String? {
        return repository?.savedEmail
    }
    
    func `do`(_ job: PasswordRecoveryEnterEmailJob) {
        switch job {
        case .request(let email):
            request(email: email)
        }
    }
    
    private func request(email: String) {
        repository?.request(email: email, callback: { [weak self] in
            switch $0 {
            case .success(let model):
                if model.responseCode == 200 {
                    self?.presenter?.handle(.requested)
                } else {
                    let error = ErrorModel(ShaktiError.General.unknown)
                    self?.presenter?.handle(.failed(error))
                }
            case .failure(let error):
                self?.presenter?.handle(.failed(error))
            }
        })
    }
}
