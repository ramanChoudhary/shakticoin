//
//  GradientView.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

class GradientView: UIView {
    
    override public class var layerClass: Swift.AnyClass {
        return CAGradientLayer.self
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setGradientColors(_ colors: [Any], startPoint: CGPoint = CGPoint(x: 0, y: 0), endPoint: CGPoint = CGPoint(x: 1, y: 0)) {
        guard let gradientLayer = self.layer as? CAGradientLayer else { return }
        gradientLayer.colors = colors
        gradientLayer.startPoint = startPoint
        gradientLayer.endPoint = endPoint
        setNeedsDisplay()
    }
}
