//
//  LoaderModule.swift
//  ShaktiCoinApp
//
//  Created by sperev on 01/09/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit
import JGProgressHUD

final class LoaderModule {
    static var shared = LoaderModule()
    private let hud = JGProgressHUD(style: .dark)
    weak var owner: UIView?
    
    func show(in view: UIView) {
        hide()
        hud.show(in: view)
        view.isUserInteractionEnabled = false
        owner = view
    }
    
    func hide() {
        hud.dismiss()
        owner?.isUserInteractionEnabled = true
    }
}
