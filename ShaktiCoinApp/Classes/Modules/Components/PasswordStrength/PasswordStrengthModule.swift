//
//  PasswordStrengthModule.swift
//  ShaktiCoinApp
//
//  Created by sperev on 28/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

struct PasswordStrengthViewModel {
    var title: TextViewModel
    var topLeft: IconTextViewModel
    var topRight: IconTextViewModel
    var bottomLeft: IconTextViewModel
    var bottomRight: IconTextViewModel
    
    static func enterPassword(upperCharacters: Bool, numbers: Bool, lowerCharacters: Bool, symbols: Bool) -> PasswordStrengthViewModel {
        
        let title = TextViewModel(style: TextStyle.latoSemiBold12.alignment(.center), value: "Password strenght".uppercased().localized)
        
        let topLeft = IconTextViewModel(text: TextViewModel(style: TextStyle.lato14.color(.white), value: "Uppercase characters".localized), icon: upperCharacters ? .passwordChecked : .passwordUnchecked)
        
        let topRight = IconTextViewModel(text: TextViewModel(style: TextStyle.lato14.color(.white), value: "Numbers".localized), icon: numbers ? .passwordChecked : .passwordUnchecked)
        
        let bottomLeft = IconTextViewModel(text: TextViewModel(style: TextStyle.lato14.color(.white), value: "Lowercase characters".localized), icon: lowerCharacters ? .passwordChecked : .passwordUnchecked)
        
        let bottomRight = IconTextViewModel(text: TextViewModel(style: TextStyle.lato14.color(.white), value: "Symbols".localized), icon: symbols ? .passwordChecked : .passwordUnchecked)
        return PasswordStrengthViewModel(title: title, topLeft: topLeft, topRight: topRight, bottomLeft: bottomLeft, bottomRight: bottomRight)
    }
}

final class PasswordStrengthModule: StackViewModule {
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var topLeft: IconTextModule?
    @IBOutlet weak var topRight: IconTextModule?
    @IBOutlet weak var bottomLeft: IconTextModule?
    @IBOutlet weak var bottomRight: IconTextModule?
    
    func populate(_ viewModel: PasswordStrengthViewModel) {
        titleLabel?.setup(viewModel.title)
        topLeft?.populate(viewModel.topLeft)
        topRight?.populate(viewModel.topRight)
        bottomLeft?.populate(viewModel.bottomLeft)
        bottomRight?.populate(viewModel.bottomRight)
    }
}
