//
//  CustomButton.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

protocol ButtonConfiguratorProtocol {
    func setup(_ button: UIButton, highlighted: Bool)
    func setup(_ button: UIButton, selected: Bool)
    func setup(_ button: UIButton, enabled: Bool)
    func setup(_ button: UIButton, gradient: Bool)
    func setupInitial(_ button: UIButton)
}

class CustomButton: UIButton {
    @IBInspectable var isTabStyle: Bool = false
    var inset: CGFloat = 8
    
    var configurator: ButtonConfiguratorProtocol? {
        didSet {
            configurator?.setupInitial(self)
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            configurator?.setup(self, highlighted: isHighlighted)
        }
    }
    
    override var isSelected: Bool {
        didSet {
            configurator?.setup(self, selected: isSelected)
        }
    }
    
    override var isEnabled: Bool {
        didSet {
            configurator?.setup(self, enabled: isEnabled)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        guard isTabStyle else { return }
        
        let imageSize: CGSize = imageView?.image?.size ?? .zero
        var titleSize: CGSize = .zero
        
        if let label = titleLabel {
            let labelString = label.text ?? ""
            titleSize = labelString.size(withAttributes: [NSAttributedString.Key.font: label.font ?? TextStyle.futura14.font])
        }
        
        var top = bounds.size.height - inset - titleSize.height
        titleEdgeInsets = UIEdgeInsets(top: top, left: -imageSize.width, bottom: inset, right: 0.0)
        
        top = (bounds.size.height - 2*inset - titleSize.height) / 2 - bounds.size.height / 2
        
        imageEdgeInsets = UIEdgeInsets(top: top, left: 0.0, bottom: 0.0, right: -titleSize.width)
    }
}
