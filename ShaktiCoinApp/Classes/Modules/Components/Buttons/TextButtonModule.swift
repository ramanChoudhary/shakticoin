//
//  LinkButton.swift
//  ShaktiCoinApp
//
//  Created by sperev on 18/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

struct TextButtonModel {
    var title: TextViewModel
    var titleHighlighted: TextViewModel
    var enabled: Bool
    var callback: (() -> Void)?
    init(title: TextViewModel, enabled: Bool, callback: (() -> Void)?) {
        self.title = title
        
        var titleHighlighted = title
        titleHighlighted.style = title.style.color(title.style.color.withAlphaComponent(0.5))
        
        self.titleHighlighted = titleHighlighted
        self.enabled = enabled
        self.callback = callback
    }
}

final class TextButtonModule: StackViewModule {
    @IBOutlet weak var button: UIButton?
    var callback: (() -> Void)?
    func populate(_ viewModel: TextButtonModel) {
        button?.setup(viewModel.title, state: .normal)
        button?.setup(viewModel.title, state: .focused)
        button?.setup(viewModel.title, state: .selected)
        button?.setup(viewModel.titleHighlighted, state: .highlighted)
        button?.isEnabled = viewModel.enabled
        callback = viewModel.callback
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        callback?()
    }
}
