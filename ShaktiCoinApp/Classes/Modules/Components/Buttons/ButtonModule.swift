//
//  ButtonModule.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

struct ButtonModuleModel {
    var title: String
    var image: String?
    var enabled: Bool
    var callback: (() -> Void)?
    var accessibilityIdentifier: String?
    var configurator: ButtonConfiguratorProtocol?
    
    init(title: String, image: String? = nil, enabled: Bool = true, accessibilityIdentifier: String? = nil, configurator: ButtonConfiguratorProtocol? = nil, callback: (() -> Void)? = nil) {
        self.title = title
        self.image = image
        self.enabled = enabled
        self.callback = callback
        self.accessibilityIdentifier = accessibilityIdentifier
        self.configurator = configurator
    }
}

protocol ButtonModuleProtocol: class {
    func populate(model: ButtonModuleModel)
    func set(enabled: Bool)
}

class ButtonModule: StackViewModule, ButtonModuleProtocol {
    var model: ButtonModuleModel?
    var style: TextStyle?
    
    @IBOutlet weak var button: UIButton!
    @IBInspectable var buttonFill: Bool = false {
        didSet {
            setNeedsLayout()
            layoutIfNeeded()
        }
    }
    
    @IBOutlet weak var buttonWidthConstraint: NSLayoutConstraint?
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        model?.callback?()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let width = buttonFill ? bounds.width : button.intrinsicContentSize.width
        buttonWidthConstraint?.constant = min(width, bounds.width)
    }
    
    func populate(model: ButtonModuleModel) {
        self.model = model
        
        button.isEnabled = model.enabled
        button.setTitle(model.title, for: .normal)
        button.isAccessibilityElement = true
        if let identifier = model.accessibilityIdentifier {
            button.accessibilityIdentifier = identifier
        }
        
        if let name = model.image,
            let image = UIImage(named: name) {
            button.setImage(image, for: .normal)
        }
    }
    
    func set(enabled: Bool) {
        model?.enabled = enabled
        button.isEnabled = enabled
    }
}
