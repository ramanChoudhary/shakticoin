//
//  TextFieldModule.swift
//  ShaktiCoinApp
//
//  Created by sperev on 13/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

struct TextFieldViewModel {
    var text: TextViewModel
    var placeholder: TextViewModel
    
    var keyboardType: UIKeyboardType
    var isSecureTextEntry: Bool
    
    init(text: TextViewModel, placeholder: TextViewModel, keyboardType: UIKeyboardType, isSecureTextEntry: Bool) {
        self.text = text
        self.placeholder = placeholder
        self.keyboardType = keyboardType
        self.isSecureTextEntry = isSecureTextEntry
    }
    
    init(textStyle: TextStyle, textLocalized: String, placeholderStyle: TextStyle, placeholderLocalized: String, keyboardType: UIKeyboardType, isSecureTextEntry: Bool) {
        text = TextViewModel(style: textStyle, value: textLocalized)
        placeholder = TextViewModel(style: placeholderStyle, value: placeholderLocalized)
        self.keyboardType = keyboardType
        self.isSecureTextEntry = isSecureTextEntry
    }
}

final class TextFieldModule: StackViewModule, UITextFieldDelegate {
    @IBOutlet weak var textField: UITextField?
    @IBOutlet weak var showPasswordButton: UIButton?
    
    var textWillChangedCallback: ((String?) -> Void)?
    
    override func configureView() {
        super.configureView()
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 3
        view.layer.borderWidth = 1
        view.layer.borderColor = ColorStyle.gold.cgColor
        textField?.autocapitalizationType = .none
    }
    
    func populate(_ viewModel: TextFieldViewModel) {
        textField?.font = viewModel.text.style.font
        textField?.textColor = viewModel.text.style.color
        textField?.text = viewModel.text.value
        textField?.isSecureTextEntry = viewModel.isSecureTextEntry
        textField?.keyboardType = viewModel.keyboardType
        textField?.attributedPlaceholder = Util.NSMAttrString(text: viewModel.placeholder.value, font: viewModel.placeholder.style.font)
        
        showPasswordButton?.isHidden = !viewModel.isSecureTextEntry
    }
    
    // MARK: - TextField
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        textWillChangedCallback?(text)
        
        return true
    }
    
    @IBAction func showPasswordPressed(_ sender: UIButton) {
        textField?.isSecureTextEntry = !(textField?.isSecureTextEntry ?? false)
    }
}
