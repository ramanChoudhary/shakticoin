//
//  CountryCodeTextFieldModule.swift
//  ShaktiCoinApp
//
//  Created by sperev on 24/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit
import NKVPhonePicker

struct CountryCodeTextFieldViewModel {
    weak var viewController: UIViewController?
    
    var text: TextViewModel
    var favoriteCountriesLocaleIdentifiers: [String]
    init(text: TextViewModel, favoriteCountriesLocaleIdentifiers: [String] = [], viewController: UIViewController? = nil) {
        self.text = text
        self.favoriteCountriesLocaleIdentifiers = favoriteCountriesLocaleIdentifiers
        self.viewController = viewController
    }
}

class CountryCodeTextFieldModule: StackViewModule, UITextFieldDelegate {
    
    lazy var textField: NKVPhonePickerTextField = {
        let textField = NKVPhonePickerTextField(frame: .zero)
        textField.delegate = self
        textField.flagSize = CGSize(width: 28, height: 16)
        textField.favoriteCountriesLocaleIdentifiers = ["CH", "US"]
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        textField.setCurrentCountryInitially = true
        textField.keyboardType = .numberPad
        return textField
    }()
    
    override func configureView() {
        super.configureView()
        ViewHelper.configure(textField, in: view, edgeInstes: .horizontal(12))
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 3
        view.layer.borderWidth = 1
        view.layer.borderColor = ColorStyle.gold.cgColor
    }
    
    var selectedCountryPhoneCode: String? {
        return textField.code
    }
    
    func populate(_ viewModel: CountryCodeTextFieldViewModel) {
        if let first = viewModel.favoriteCountriesLocaleIdentifiers.first {
            textField.country = Country.country(for: NKVSource(countryCode: first))
        }
        
        textField.phonePickerDelegate = viewModel.viewController
        
        textField.font = viewModel.text.style.font
        textField.textColor = viewModel.text.style.color
        textField.attributedPlaceholder = Util.NSMAttrString(text: viewModel.text.value, font: viewModel.text.style.font)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return false
    }

}
