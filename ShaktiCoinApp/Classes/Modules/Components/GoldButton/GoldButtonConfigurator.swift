//
//  GoldButtonConfigurator.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

final class GoldButtonConfigurator: ButtonConfiguratorProtocol {
    
    private let cornerRadiusNumber: CGFloat = 18.0
    private let textMargin: CGFloat = 16.0
    
    private var leftColor: UIColor {
        if !isEnabled {
            return ColorStyle.neutral.color
        } else if isHighlighted || isSelected {
            return ColorStyle.middleGold.color
        }
        return ColorStyle.darkGold.color
    }
    
    private var rightColor: UIColor {
        return (isGradient && isEnabled) ? ColorStyle.lightGold.color : leftColor
    }
    
    private var isGradient: Bool = false
    private var isHighlighted: Bool = false
    private var isSelected: Bool = false
    private var isEnabled: Bool = true
    
    private let style: TextStyle
    init(style: TextStyle) {
        self.style = style
    }
    
    func setup(_ button: UIButton, highlighted: Bool) {
        isHighlighted = highlighted
        button.titleLabel?.alpha = 1
        setupColors(button)
    }
    
    func setup(_ button: UIButton, selected: Bool) {
        isSelected = selected
        setupColors(button)
    }
    
    func setup(_ button: UIButton, enabled: Bool) {
        isEnabled = enabled
        setupColors(button)
    }
    
    func setup(_ button: UIButton, gradient: Bool) {
        isGradient = gradient
        setupColors(button)
    }
    
    func setupInitial(_ button: UIButton) {
        setupColors(button)
        button.roundedCorners(withRadius: cornerRadiusNumber)
        
        button.titleLabel?.font = style.font
        button.titleLabel?.tintColor = .clear
        
        button.setTitleColor(style.color, for: .normal)
        button.setTitleColor(style.color, for: .highlighted)
        button.setTitleColor(style.color, for: .selected)
        button.setTitleColor(ColorStyle.white.color, for: .disabled)
        
        button.contentEdgeInsets = UIEdgeInsets.horizontal(textMargin)
    }
    
    private func setupColors(_ button: UIButton) {
        (button as? GradientButton)?.setGradientColors([leftColor.cgColor, rightColor.cgColor])
    }
}
