//
//  GradientButtonModule.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

final class GoldButtonModule: ButtonModule { }
class GoldButton: GradientButton {
    @IBInspectable var isGradient: Bool = false {
        didSet {
            configurator?.setup(self, gradient: isGradient)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        set(style: TextStyle.futura14.alignment(.center).color(ColorStyle.white.color).numberOfLines(1))
    }

    fileprivate func set(style: TextStyle) {
        configurator = GoldButtonConfigurator(style: style)
        configurator?.setup(self, gradient: isGradient)
    }
}
