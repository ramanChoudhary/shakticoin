//
//  TextViewModel.swift
//  ShaktiCoinApp
//
//  Created by sperev on 18/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

struct TextViewModel {
    var style: TextStyle
    var value: String
}
