//
//  IconTextModule.swift
//  ShaktiCoinApp
//
//  Created by sperev on 28/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

struct IconTextViewModel {
    struct IconModel {
        var name: String?
        var background: ColorStyle?
        var corner: ColorStyle?
        
        static var passwordChecked: IconModel {
            return IconModel(name: "CheckmarkSmall", background: .gold, corner: nil)
        }
        
        static var passwordUnchecked: IconModel {
            return IconModel(name: nil, background: nil, corner: .gold)
        }
    }
    
    var text: TextViewModel
    var icon: IconModel
    var selectAction: (() -> Void)?
}

class IconTextModule: StackViewModule {
    @IBOutlet weak var textLabel: UILabel?
    @IBOutlet weak var iconImageView: UIImageView?
    @IBOutlet weak var iconSize: NSLayoutConstraint?
    var selectAction: (() -> Void)?
    
    func populate(_ viewModel: IconTextViewModel) {
        textLabel?.setup(viewModel.text)
        
        iconImageView?.image = nil
        if let icon = viewModel.icon.name {
            iconImageView?.image = UIImage(named: icon)
        }
        
        if let color = viewModel.icon.background {
            iconImageView?.backgroundColor = color.color
        } else {
            iconImageView?.backgroundColor = .clear
        }
        
        iconImageView?.layer.masksToBounds = false
        if let color = viewModel.icon.corner {
            iconImageView?.layer.borderColor = color.cgColor
            iconImageView?.layer.borderWidth = 1
        } else {
            iconImageView?.layer.borderWidth = 0
        }
        
        iconSize?.constant = iconImageView?.image?.size.width ?? 0
        selectAction = viewModel.selectAction
    }
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        selectAction?()
    }
}
