//
//  TextStyle.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

struct TextStyle: Equatable {
    let font: UIFont
    let lineHeight: CGFloat
    let alignment: NSTextAlignment
    let color: UIColor
    let numberOfLines: Int
    let underlined: Bool
    
    init(font: UIFont, lineHeight: CGFloat, alignment: NSTextAlignment = .left, color: UIColor = ColorStyle.white.color, numberOfLines: Int = 0, underlined: Bool = false) {
        self.font = font
        self.lineHeight = lineHeight
        self.alignment = alignment
        self.color = color
        self.numberOfLines = numberOfLines
        self.underlined = underlined
    }

    init(font: UIFont, lineHeight: CGFloat) {
        self.font = font
        self.lineHeight = lineHeight
        self.alignment = .left
        self.color = ColorStyle.white.color
        self.numberOfLines = 0
        self.underlined = false
    }

    var attributes: [NSAttributedString.Key: Any] {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.minimumLineHeight = lineHeight
        paragraphStyle.alignment = alignment
        paragraphStyle.lineBreakMode = .byTruncatingTail
        var attributes: [NSAttributedString.Key: Any] = [.foregroundColor: color, .paragraphStyle: paragraphStyle, .font: font]
        if underlined {
            attributes[.underlineStyle] = NSUnderlineStyle.single.rawValue
        }
        return attributes
    }

    func color(_ value: UIColor) -> TextStyle {
        return TextStyle(font: font, lineHeight: lineHeight, alignment: alignment, color: value, numberOfLines: numberOfLines, underlined: underlined)
    }

    func numberOfLines(_ value: Int) -> TextStyle {
        return TextStyle(font: font, lineHeight: lineHeight, alignment: alignment, color: color, numberOfLines: value, underlined: underlined)
    }

    func alignment(_ value: NSTextAlignment) -> TextStyle {
        return TextStyle(font: font, lineHeight: lineHeight, alignment: value, color: color, numberOfLines: numberOfLines, underlined: underlined)
    }
    
    func underline(_ value: Bool) -> TextStyle {
        return TextStyle(font: font, lineHeight: lineHeight, alignment: alignment, color: color, numberOfLines: numberOfLines, underlined: value)
    }

    static let futura14 = TextStyle(font: UIFont.futura(fontSize: 14, type: .Medium), lineHeight: 19)
    static let lato14 = TextStyle(font: UIFont.lato(fontSize: 14), lineHeight: 19)
    static let lato12 = TextStyle(font: UIFont.lato(fontSize: 12, type: .Regular), lineHeight: 17)
    static let lato10 = TextStyle(font: UIFont.lato(fontSize: 10), lineHeight: 12)
    static let latoBold14 = TextStyle(font: UIFont.lato(fontSize: 14, type: .Bold), lineHeight: 24)
    static let latoBold12 = TextStyle(font: UIFont.lato(fontSize: 12, type: .Bold), lineHeight: 22)
    static let latoBold10 = TextStyle(font: UIFont.lato(fontSize: 12, type: .Bold), lineHeight: 18)
    static let latoBold24 = TextStyle(font: UIFont.lato(fontSize: 24, type: .Bold), lineHeight: 26)
    static let latoSemiBold14 = TextStyle(font: UIFont.lato(fontSize: 14, type: .Bold), lineHeight: 24)
    static let latoSemiBold12 = TextStyle(font: UIFont.lato(fontSize: 12, type: .Bold), lineHeight: 22)
    
    static let latoLight16 = TextStyle(font: UIFont.lato(fontSize: 16, type: .Light), lineHeight: 24)
    static let latoLight24 = TextStyle(font: UIFont.lato(fontSize: 24, type: .Light), lineHeight: 26)
    
    static let latoMedium18 = TextStyle(font: UIFont.lato(fontSize: 18, type: .Regular), lineHeight: 24)
    static let latoMedium36 = TextStyle(font: UIFont.lato(fontSize: 36, type: .Regular), lineHeight: 44)
}
