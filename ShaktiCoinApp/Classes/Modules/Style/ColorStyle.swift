//
//  ColorStyle.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

enum ColorStyle: Int {
    case white
    case neutral
    case gold
    case darkGold
    case lightGold
    case middleGold
    case transparent

    init() {
        self = .white
    }

    init(value: Int) {
        guard let color = ColorStyle(rawValue: value) else {
            self = ColorStyle.white
            return
        }
        self = color
    }

    var color: UIColor {
        switch self {
        case .white: return UIColor.white
        case .neutral: return UIColor.lightGray
        case .darkGold: return UIColor.hex("#8D7A55")
        case .lightGold: return UIColor.hex("#D0C19C")
        case .middleGold: return UIColor.hex("#AE9F7A")
        case .gold: return UIColor(named: "Gold") ?? .hex("#D8C295")
        case .transparent: return .clear
        }
    }

    var cgColor: CGColor {
        return self.color.cgColor
    }
}
