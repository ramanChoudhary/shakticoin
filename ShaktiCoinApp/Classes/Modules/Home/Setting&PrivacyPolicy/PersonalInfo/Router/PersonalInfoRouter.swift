//
//  PersonalInfoRouter.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 02/05/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit

class PersonalInfoRouter: PersonalInfoRouterProtocol {
    static func createPersonalInfoModule() -> UIViewController {
        let view = PersonalInfoController()
        let presenter:PersonalInfoPresenterProtocol = PersonalInfoPresenter()
        let router:PersonalInfoRouterProtocol = PersonalInfoRouter()
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        return view
    }
    
    func presentNotification(from view: PersonalInfoViewProtocol?) {
        
    }
    
    func backAction(from view: PersonalInfoViewProtocol?) {
        
    }
    
}
