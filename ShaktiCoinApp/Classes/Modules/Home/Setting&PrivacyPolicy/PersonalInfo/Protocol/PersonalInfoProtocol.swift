//
//  PersonalInfoProtocol.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 02/05/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit

protocol PersonalInfoViewProtocol: class {
    var presenter:PersonalInfoPresenterProtocol? {get set}
}

protocol PersonalInfoPresenterProtocol: class {
    var view:PersonalInfoViewProtocol?{ get set }
    var router:PersonalInfoRouterProtocol? {get set}
    //View =========> Presenter
       func backAction()
       func presentNotification()

}

protocol PersonalInfoRouterProtocol: class {
    static func createPersonalInfoModule() -> UIViewController
    func backAction(from view:PersonalInfoViewProtocol?)
    func presentNotification(from view:PersonalInfoViewProtocol?)
}
