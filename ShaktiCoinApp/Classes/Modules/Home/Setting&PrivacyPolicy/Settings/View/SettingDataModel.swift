//
//  SettingDataModel.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 01/05/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class SettingDataModel: NSObject {

    var title:String?
    var desc:String?
    var isEnable:Bool?
    var rightButtonTitle:String?
    var switchState:Bool?
    var isHideRightButton:Bool?
    
    init(title:String,desc:String?,isEnable:Bool,buttonTitle:String?,switchState:Bool?,hideRightBtn:Bool) {
        self.title = title
        self.desc = desc
        self.isEnable = isEnable
        self.rightButtonTitle = buttonTitle
        self.switchState = switchState
        self.isHideRightButton = hideRightBtn
    }
}


class SettingsData: NSObject {
    static var accountSettingDataArr:[SettingDataModel] = [
        SettingDataModel(title: "Personal Info", desc: nil, isEnable: true, buttonTitle: nil, switchState: nil, hideRightBtn: false),
        SettingDataModel(title: "KYC Verification", desc: nil, isEnable: true, buttonTitle: "In Progress", switchState: nil, hideRightBtn: false)
    ]
    
    static var securitySettingDataArr:[SettingDataModel] = [
        SettingDataModel(title: "TouchID", desc: "Feature yet to be released.", isEnable: false, buttonTitle: nil, switchState: nil, hideRightBtn: false)
    ]
    
    static var notificationSettingDataArr:[SettingDataModel] = [
        SettingDataModel(title: "General Service Notifications", desc: nil, isEnable: true, buttonTitle: nil, switchState: false, hideRightBtn: true),
        SettingDataModel(title: "Receive payments and pay requests", desc: nil, isEnable: true, buttonTitle: nil, switchState: false, hideRightBtn: true),
        SettingDataModel(title: "Refferal activity", desc: nil, isEnable: true, buttonTitle: nil, switchState: false, hideRightBtn: true),
        SettingDataModel(title: "Tips and suggestions", desc: "Feature yet to be released.", isEnable: false, buttonTitle: nil, switchState: false, hideRightBtn: true),
    ]
    
    static var helpSettingDataArr:[SettingDataModel] = [
        SettingDataModel(title: "Help", desc: nil, isEnable: true, buttonTitle: nil, switchState: nil, hideRightBtn: false),
        SettingDataModel(title: "Appliction Terms", desc: nil, isEnable: true, buttonTitle: nil, switchState: nil, hideRightBtn: false),
        SettingDataModel(title: "Privacy Policy", desc: nil, isEnable: true, buttonTitle: nil, switchState: nil, hideRightBtn: false),
        SettingDataModel(title: "Taxes Info", desc: nil, isEnable: true, buttonTitle: nil, switchState: nil, hideRightBtn: false),
        SettingDataModel(title: "Processing Fees", desc: nil, isEnable: true, buttonTitle: nil, switchState: nil, hideRightBtn: false),
        SettingDataModel(title: "Feedback/ContactUs", desc: nil, isEnable: true, buttonTitle: nil, switchState: nil, hideRightBtn: false),
        SettingDataModel(title: "Password Rest", desc: nil, isEnable: true, buttonTitle: nil, switchState: nil, hideRightBtn: false),
    ]
    
}
