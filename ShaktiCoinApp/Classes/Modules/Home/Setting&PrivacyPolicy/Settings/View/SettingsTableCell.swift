//
//  SettingsTableCell.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 01/05/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class SettingsTableCell: UITableViewCell {

    @IBOutlet weak var rightSwitch: UISwitch!
    @IBOutlet weak var rightbutton: UIButton!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    func setCellData(data:SettingDataModel){
        self.rightSwitch.isHidden = data.switchState == nil
        self.rightbutton.isHidden = data.isHideRightButton ?? false
        self.rightbutton.setTitle(data.rightButtonTitle, for: .normal)
        self.descLabel.isHidden = data.desc == nil
        self.titleLabel.setLabel(data.title, data.isEnable ?? true ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0):#colorLiteral(red: 0.537254902, green: 0.537254902, blue: 0.537254902, alpha: 1), UIFont().setLatoBoldFont(size: 14))
        self.descLabel.setLabel(data.desc, data.isEnable ?? true ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0):#colorLiteral(red: 0.537254902, green: 0.537254902, blue: 0.537254902, alpha: 1), UIFont().setLatoRegularFont(size: 10))
        self.rightSwitch.isOn = data.switchState ?? false
        self.rightbutton.tintColor = data.isEnable ?? true ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0):#colorLiteral(red: 0.537254902, green: 0.537254902, blue: 0.537254902, alpha: 1)
        self.rightSwitch.thumbTintColor = data.isEnable ?? true ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0):#colorLiteral(red: 0.537254902, green: 0.537254902, blue: 0.537254902, alpha: 1)
    }
}
