//
//  SettingsController.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 01/05/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class SettingsController: UIViewController {

    @IBOutlet weak var settingTable: UITableView!
    
    var accountSecArr = SettingsData.accountSettingDataArr
    var securitySecArr = SettingsData.securitySettingDataArr
    var notificationSecArr = SettingsData.notificationSettingDataArr
    var helpSecArr = SettingsData.helpSettingDataArr
    var secHeaderTitleArr = ["Account","Security","Notification Preferences","Help & Policies"]
    var presenter:SettingsPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setSettingTable()
    }

    
    func setSettingTable(){
        self.settingTable.delegate = self
        self.settingTable.dataSource = self
        self.settingTable.registerCell(identifier: cellIdentifier.settingsTableCell)
        self.setTableHeader()
    }
    
    
    func setTableHeader(){
        let header = UINib(nibName: NIB_NAME.settingTableHeaderView, bundle: nil).instantiate(withOwner: self, options: nil).first as? SettingTableHeaderView
        header?.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: 100)
        header?.nameLabel.text = "William Almundson"
        header?.emailLabel.text = "walmun@gmail.com"
        self.settingTable.tableHeaderView = header
    }

}

extension SettingsController:UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return accountSecArr.count
        case 1:
            return securitySecArr.count
        case 2:
            return notificationSecArr.count
        case 3:
            return helpSecArr.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let secHeader = UIView(frame: CGRect(x: 0, y: 0, width:SCREEN_SIZE.width, height: 50))
        let headerLabel = UILabel(frame: CGRect(x: 15, y: 0, width: SCREEN_SIZE.width - 40, height: 50))
        headerLabel.setLabel(self.secHeaderTitleArr[section], #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), UIFont().setLatoLightFont(size: 24))
        secHeader.addSubview(headerLabel)
        return secHeader
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let secFooter = UIView(frame: CGRect(x: 0, y: 0, width:SCREEN_SIZE.width, height: 50))
        let button = UIButton(frame: CGRect(x: 20, y: 0, width: SCREEN_SIZE.width - 40, height: 36))
        if section == 2{
            button.setCustomButtonWithBgColor(title: "Turn on Notifications", titleColor: .white, bgColor: #colorLiteral(red: 0.537254902, green: 0.537254902, blue: 0.537254902, alpha: 1), font: UIFont().setFuturaMediumFont(size: 14))
        }
        
        if section == 3{
            button.setCustomButtonWithBgColor(title: "Log Out", titleColor: .white, bgColor: #colorLiteral(red: 0.537254902, green: 0.537254902, blue: 0.537254902, alpha: 1), font: UIFont().setFuturaMediumFont(size: 14))
        }
        
        button.cornerRadius = 18
        button.addTarget(self, action: #selector(self.secFooterBtnAction(_:)), for: .touchUpInside)
        secFooter.addSubview(button)
        if section == 2 || section == 3{
            return secFooter
        }else{
            return UIView()
        }
    }
    
    @objc func secFooterBtnAction(_ sender:UIButton){
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 2 || section == 3{
            return 50
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.settingsTableCell, for: indexPath) as? SettingsTableCell
        switch indexPath.section {
        case 0:
            cell?.setCellData(data: self.accountSecArr[indexPath.row])
        case 1:
            cell?.setCellData(data: self.securitySecArr[indexPath.row])
        case 2:
            cell?.setCellData(data: self.notificationSecArr[indexPath.row])
        case 3:
            cell?.setCellData(data: self.helpSecArr[indexPath.row])
        default:
            break
        }
        return cell!
    }
}

extension SettingsController:SettingsViewProtocol{
    
}
