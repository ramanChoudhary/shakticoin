//
//  SettingsRouter.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 02/05/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit

class SettingsRouter: SettingsRouterProtocol {
    static func createSettingsModule() -> UIViewController {
        let view = SettingsController()
        let presenter:SettingsPresenterProtocol = SettingsPresenter()
        let router:SettingsRouterProtocol = SettingsRouter()
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        return view
    }
    
    func presentNotification(from view: SettingsViewProtocol?) {
        
    }
    
    func menuAction(from view: SettingsViewProtocol?) {
        
    }
    
}
