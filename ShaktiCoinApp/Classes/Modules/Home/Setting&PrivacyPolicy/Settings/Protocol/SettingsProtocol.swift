//
//  SettingsProtocol.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 02/05/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit

protocol SettingsViewProtocol: class {
    var presenter:SettingsPresenterProtocol? {get set}
}

protocol SettingsPresenterProtocol: class {
    var view:SettingsViewProtocol?{ get set }
    var router:SettingsRouterProtocol? {get set}
    //View =========> Presenter
       func menuAction()
       func presentNotification()

}

protocol SettingsRouterProtocol: class {
    static func createSettingsModule() -> UIViewController
    func menuAction(from view:SettingsViewProtocol?)
    func presentNotification(from view:SettingsViewProtocol?)
}
