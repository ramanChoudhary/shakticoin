//
//  TaxesInfoRouter.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 02/05/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit

class TaxesInfoRouter: TaxesInfoRouterProtocol {
    static func createTaxesInfoModule() -> UIViewController {
        let view = TaxesInfoController()
        let presenter:TaxesInfoPresenterProtocol = TaxesInfoPresenter()
        let router:TaxesInfoRouterProtocol = TaxesInfoRouter()
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        return view
    }
    
    func presentNotification(from view: TaxesInfoViewProtocol?) {
        
    }
    
    func backAction(from view: TaxesInfoViewProtocol?) {
        
    }
    
}
