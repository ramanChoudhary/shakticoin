//
//  TaxesInfoController.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 30/04/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class TaxesInfoController: UIViewController {

    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var agreeMessageLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var agreeButtonBgView: UIView!
    @IBOutlet weak var bottomArrowButton: UIButton!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var bottomCheckableView: UIView!
    
    var presenter:TaxesInfoPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setControllerForTaxesInfo()
        self.checkButton.addTarget(self, action: #selector(self.checkAction(_:)), for: .touchUpInside)
        
    }
    
    @objc func checkAction(_ sender: UIButton){
        self.checkButton.isSelected = !sender.isSelected
        self.checkButton.setImage(self.checkButton.isSelected ? #imageLiteral(resourceName: "Valid"):#imageLiteral(resourceName: "Unmarked"), for: .normal)
    }
    
    func setControllerForTaxesInfo(){
        self.titleLabel.text = AppStrings.taxesInfo
        self.descLabel.text = AppStrings.taxesInfoDesc
        self.agreeMessageLabel.text = AppStrings.taxesInfoAgreeMsg
        self.bottomCheckableView.isHidden = false
        agreeButtonBgView.isHidden = false
    }
    
    func setControllerForPrivacyPolicy(){
        self.titleLabel.text = AppStrings.privacyPolicy
        self.descLabel.text = AppStrings.privacyPolicyDesc
        self.bottomCheckableView.isHidden = true
        agreeButtonBgView.isHidden = true
    }
    
    func setControllerForApplicationTerm(){
        self.titleLabel.text = AppStrings.applicationTerms
        self.descLabel.text = AppStrings.applicationTermsDesc
        self.bottomCheckableView.isHidden = true
        agreeButtonBgView.isHidden = true
    }
    
    func setControllerForProcessingFees(){
        self.titleLabel.text = AppStrings.processingFees
        self.descLabel.text = AppStrings.processingFeesDesc
        self.bottomCheckableView.isHidden = true
        agreeButtonBgView.isHidden = true
    }

    @IBAction func notificationAction(_ sender: UIButton) {
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        
    }
    
    @IBAction func agreeAction(_ sender: SCButton) {
        
    }
}

extension TaxesInfoController:TaxesInfoViewProtocol{
    
}
