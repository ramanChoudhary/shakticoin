//
//  TaxesInfoProtocol.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 02/05/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit

protocol TaxesInfoViewProtocol: class {
    var presenter:TaxesInfoPresenterProtocol? {get set}
}

protocol TaxesInfoPresenterProtocol: class {
    var view:TaxesInfoViewProtocol?{ get set }
    var router:TaxesInfoRouterProtocol? {get set}
    //View =========> Presenter
       func backAction()
       func presentNotification()

}

protocol TaxesInfoRouterProtocol: class {
    static func createTaxesInfoModule() -> UIViewController
    func backAction(from view:TaxesInfoViewProtocol?)
    func presentNotification(from view:TaxesInfoViewProtocol?)
}
