//
//  ChildInfoCell.swift
//  ShaktiCoinApp
//
//  Created by MAC on 17/03/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class ChildInfoCell: UITableViewCell {
    @IBOutlet weak var addAnotherButton: UIButton!
    @IBOutlet weak var infoField: UITextField!
    @IBOutlet weak var dropDownImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setCellForPersonalInfo(data:FeatsOfChildModel){
        self.addAnotherButton.isHidden = data.isHideAddBtn ?? false
        self.dropDownImage.isHidden = !(data.isDropDown ?? false)
        self.infoField.placeholder = data.placeholder
    }
    
}
