//
//  FeatsOfChildController.swift
//  ShaktiCoinApp
//
//  Created by MAC on 17/03/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class FeatsOfChildController: UIViewController {
    @IBOutlet weak var middelSecondView: UIView!
    @IBOutlet weak var leftTitleLabel: UILabel!
    @IBOutlet weak var rightTitleLabel: UILabel!
    @IBOutlet weak var rightView: UIView!
    @IBOutlet weak var middelTitleLabel: UILabel!
    @IBOutlet weak var middelFirstView: UIView!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var leftSecondView: UIView!
    
    @IBOutlet weak var personalInfoTable: UITableView!
    
    var personalInfodata = ChildPersonalInfoData.personalInfoArr
    var adressInfo = ChildPersonalInfoData.addressInfoArr
    var stepCount:Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setPersonalInfoTable()
        self.setControllerViewForFeatsOfChild()
    }
    
    func setControllerViewForFeatsOfChild(){
        
        self.leftView.backgroundColor = self.stepCount > 0 ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0): #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.3)
        self.leftSecondView.backgroundColor = self.stepCount > 1 ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0): #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.3)
        self.middelFirstView.backgroundColor = self.stepCount > 2 ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0): #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.3)
        self.middelSecondView.backgroundColor = self.stepCount > 3 ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0): #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.3)
        self.rightView.backgroundColor = self.stepCount > 4 ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0): #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.3)
        self.leftTitleLabel.text = "Personal Info"
        self.middelTitleLabel.text = "Additional Info"
        self.rightTitleLabel.text = "KYC Validation"
        self.leftTitleLabel.textColor = self.stepCount > 0 ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0): #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.6)
        self.middelTitleLabel.textColor = self.stepCount > 2 ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0): #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.6)
        self.rightTitleLabel.textColor = self.stepCount > 4 ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0): #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.6)
    }
    
    func setPersonalInfoTable(){
        self.personalInfoTable.delegate = self
        self.personalInfoTable.dataSource = self
        self.personalInfoTable.registerCell(identifier: cellIdentifier.childInfoCell)
        self.setTableHeader()
    }
    
    
    @objc func nextAction(_ sender:SCButton ){
        self.stepCount += 1
        self.setControllerViewForFeatsOfChild()
        self.setTableHeader()
        self.personalInfoTable.reloadData()
    }
    
    @objc func cancelAction(_ sender:UIButton){
        
    }
    
    func setTableHeader(){
        let header = UINib(nibName: NIB_NAME.familyTreeHeaderView, bundle: nil).instantiate(withOwner: self, options: nil).first as? FamilyTreeHeaderView
        switch stepCount {
        case 1:
            header?.titleLabel.text = "Who are you ?"
            case 2:
            header?.titleLabel.text = "Where do you live ?"
        default:
            break
        }
        
        header?.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: 40)
        let view = UIView(frame: header!.bounds)
        view.addSubview(header!)
        self.personalInfoTable.tableHeaderView = view
        self.personalInfoTable.tableFooterView = UIView.footerViewWithTwoBtn(title1: "Next", title2: "Cancel", btn1: #selector(self.nextAction(_:)), btn2: #selector(self.cancelAction(_:)), target: self)
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        
    }
    
    @IBAction func notificationAction(_ sender: UIButton) {
        
    }
}

extension FeatsOfChildController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.stepCount {
        case 1:
            return self.personalInfodata.count
        case 2:
            return self.adressInfo.count
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.childInfoCell, for: indexPath) as? ChildInfoCell
        switch self.stepCount {
        case 1:
            cell?.setCellForPersonalInfo(data: self.personalInfodata[indexPath.row])
        case 2:
            cell?.setCellForPersonalInfo(data: self.adressInfo[indexPath.row])
        default:
            break
        }
        
        return cell!
    }
    
}

