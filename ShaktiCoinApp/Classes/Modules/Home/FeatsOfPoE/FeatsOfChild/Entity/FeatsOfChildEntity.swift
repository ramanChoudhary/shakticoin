//
//  FeatsOfChildEntity.swift
//  ShaktiCoinApp
//
//  Created by MAC on 18/03/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit

class FeatsOfChildModel:NSObject{
    var placeholder:String?
    var text:String?
    var isHideAddBtn:Bool?
    var isDropDown:Bool?
    
    init(placeholder:String,text:String?,isHideAddBtn:Bool,isDropDown:Bool) {
        self.placeholder = placeholder
        self.text = text
        self.isHideAddBtn = isHideAddBtn
        self.isDropDown = isDropDown
    }
}

class ChildPersonalInfoData:NSObject{
    static var personalInfoArr:[FeatsOfChildModel] = [
        FeatsOfChildModel(placeholder: "First Name", text: nil, isHideAddBtn: true, isDropDown: false),
        FeatsOfChildModel(placeholder: "Last Name", text: nil, isHideAddBtn: true, isDropDown: false),
        FeatsOfChildModel(placeholder: "Gender", text: nil, isHideAddBtn: true, isDropDown: true),
        FeatsOfChildModel(placeholder: "Date of Birth", text: nil, isHideAddBtn: true, isDropDown: false),
        FeatsOfChildModel(placeholder: "Language", text: nil, isHideAddBtn: false, isDropDown: true),
        FeatsOfChildModel(placeholder: "Primary Email Address", text: nil, isHideAddBtn: true, isDropDown: false),
        FeatsOfChildModel(placeholder: "Primary Phone Number", text: nil, isHideAddBtn: true, isDropDown: false),
        FeatsOfChildModel(placeholder: "Address Line 1", text: nil, isHideAddBtn: true, isDropDown: false)
        
    ]
    
    static var addressInfoArr:[FeatsOfChildModel] = [
           FeatsOfChildModel(placeholder: "Country of Residence", text: nil, isHideAddBtn: true, isDropDown: true),
           FeatsOfChildModel(placeholder: "Select Your State", text: nil, isHideAddBtn: true, isDropDown: true),
           FeatsOfChildModel(placeholder: "City", text: nil, isHideAddBtn: true, isDropDown: false),
           FeatsOfChildModel(placeholder: "Address Line 1", text: nil, isHideAddBtn: true, isDropDown: false),
           FeatsOfChildModel(placeholder: "Address Line 2", text: nil, isHideAddBtn: true, isDropDown: false),
           FeatsOfChildModel(placeholder: "Zip Code/Postal Code", text: nil, isHideAddBtn: true, isDropDown: false)
           
       ]
}
