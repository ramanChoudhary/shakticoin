//
//  AddPoeFeatModel.swift
//  ShaktiCoinApp
//
//  Created by MAC on 12/03/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class AddPoeFeatModel: NSObject {
    
    var title: String?
    
    init(title:String?) {
        self.title = title
    }
}

class AddPoeFeatData:NSObject{
    static var addPoeDataArr:[AddPoeFeatModel] = [
        AddPoeFeatModel(title: "Feats of Child"),
        AddPoeFeatModel(title: "Feats of School"),
        AddPoeFeatModel(title: "Feats of Service Providers"),
        AddPoeFeatModel(title: "Feats of Independent-Validators")
    ]
}
