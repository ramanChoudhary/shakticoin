//
//  FeatsOfPoeProtocols.swift
//  ShaktiCoinApp
//
//  Created by MAC on 17/03/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

protocol FeatsOfPoeViewProtocol: class {
    var presenter: FeatsOfPoePresenterProtocol? {get set}
}

protocol FeatsOfPoePresenterProtocol:class {
    var view: FeatsOfPoeViewProtocol? {get set}
    var router:FeatsOfPoeRouterProtocol? {get set}
    // view ========= presenter
    func dismissController()
}

protocol FeatsOfPoeRouterProtocol:class {
    static func createFeatsOfPoeModule() -> UIViewController
    func popController(from view: FeatsOfPoeViewProtocol?)
}
