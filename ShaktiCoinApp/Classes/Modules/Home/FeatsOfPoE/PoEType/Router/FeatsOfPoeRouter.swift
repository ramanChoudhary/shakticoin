//
//  FeatsOfPoeRouter.swift
//  ShaktiCoinApp
//
//  Created by MAC on 17/03/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit
class FeatsOfPoeRouter:FeatsOfPoeRouterProtocol{
    static func createFeatsOfPoeModule() -> UIViewController {
        let view = FeatsOfPoEController()
        let presenter:FeatsOfPoePresenterProtocol = FeatsOfPoePresenter()
        let router: FeatsOfPoeRouterProtocol = FeatsOfPoeRouter()
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        return view
    }
    
    func popController(from view: FeatsOfPoeViewProtocol?) {
        
    }
}
