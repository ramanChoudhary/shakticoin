//
//  FeatOfPoEFooterView.swift
//  ShaktiCoinApp
//
//  Created by MAC on 04/03/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class FeatOfPoEFooterView: UIView {

    @IBOutlet weak var descLabel: UILabel!

    @IBOutlet weak var cancelButton: SCButton!
}
