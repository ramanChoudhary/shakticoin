//
//  FeatsOfPoEController.swift
//  ShaktiCoinApp
//
//  Created by MAC on 04/03/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class FeatsOfPoEController: UIViewController {
    @IBOutlet weak var featOfPoETable: UITableView!
    var addData = AddPoeFeatData.addPoeDataArr
    var presenter:FeatsOfPoePresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setFeatTable()
    }
    
    func setFeatTable(){
        self.featOfPoETable.delegate = self
        self.featOfPoETable.dataSource = self
        self.featOfPoETable.registerCell(identifier: cellIdentifier.featOfPoeTableCell)
        self.setFooterView()
    }
    
    func setFooterView(){
        let footer = UINib(nibName: NIB_NAME.featOfPoEFooterView, bundle: nil).instantiate(withOwner: self, options: nil).first as? FeatOfPoEFooterView
        footer?.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: 150)
        let view = UIView(frame: footer!.bounds)
        view.addSubview(footer!)
        self.featOfPoETable.tableFooterView = view
    }
    
    @IBAction func backAction(_ sender: UIButton) {
    
    }
    
    @IBAction func notioficationAction(_ sender: UIButton) {
        
    }
    
}

extension FeatsOfPoEController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.featOfPoeTableCell, for: indexPath) as? FeatOfPoeTableCell
        cell?.titleLabel.text = self.addData[indexPath.row].title
        return cell!
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UINib.init(nibName: NIB_NAME.familyTreeHeaderView, bundle: nil).instantiate(withOwner: self, options: nil).first as? FamilyTreeHeaderView
        view?.titleLabel.text = "Add a PoE Feat"
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
}

extension FeatsOfPoEController:FeatsOfPoeViewProtocol{
    
}
