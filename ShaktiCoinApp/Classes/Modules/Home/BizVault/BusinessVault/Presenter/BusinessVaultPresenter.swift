//
//  BusinessVaultPresenter.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 29/04/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation

class  BusinessVaultPresenter: BusinessVaultPresenterProtocol {
    var view: BusinessVaultViewProtocol?
    var router: BusinessVaultRouterProtocol?
    
    func presentNotification() {
        self.router?.presentNotification(from: view)
    }
    
    func menuAction() {
        self.router?.menuAction(from: view)
    }
    
    func wantVault() {
        self.router?.wantVaultAction(from: view)
    }
}
