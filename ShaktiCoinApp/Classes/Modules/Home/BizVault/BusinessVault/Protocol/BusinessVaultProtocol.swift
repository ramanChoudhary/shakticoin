//
//  BusinessVaultProtocol.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 29/04/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit

protocol BusinessVaultViewProtocol: class {
    var presenter:BusinessVaultPresenterProtocol? {get set}
}

protocol BusinessVaultPresenterProtocol: class {
    var view:BusinessVaultViewProtocol?{ get set }
    var router:BusinessVaultRouterProtocol? {get set}
    //View =========> Presenter
       func menuAction()
       func presentNotification()
       func wantVault()

}

protocol BusinessVaultRouterProtocol: class {
    static func createBusinessVaultModule() -> UIViewController
    func menuAction(from view:BusinessVaultViewProtocol?)
    func presentNotification(from view:BusinessVaultViewProtocol?)
    func wantVaultAction(from view:BusinessVaultViewProtocol?)
}
