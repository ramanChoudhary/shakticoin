//
//  BusinessVaultController.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 28/04/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class BusinessVaultController: UIViewController {
    
    var presenter:BusinessVaultPresenterProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    
    @IBAction func menuAction(_ sender: UIButton) {
        
    }
    
    @IBAction func notificationAction(_ sender: UIButton) {
        
    }
    
    @IBAction func wantVaultAction(_ sender: SCButton) {
        presenter?.wantVault()
    }
    
    @IBAction func notWantVault(_ sender: UIButton) {
        
    }
}

extension BusinessVaultController:BusinessVaultViewProtocol{
    
}
