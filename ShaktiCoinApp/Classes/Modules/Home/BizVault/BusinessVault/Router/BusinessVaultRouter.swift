//
//  BusinessVaultRouter.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 29/04/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit

class BusinessVaultRouter: BusinessVaultRouterProtocol {
    static func createBusinessVaultModule() -> UIViewController {
        let view = BusinessVaultController()
        let presenter:BusinessVaultPresenterProtocol = BusinessVaultPresenter()
        let router:BusinessVaultRouterProtocol = BusinessVaultRouter()
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        return view
    }
    
    func presentNotification(from view: BusinessVaultViewProtocol?) {
        
    }
    
    func menuAction(from view: BusinessVaultViewProtocol?) {
        
    }
    
    func wantVaultAction(from view: BusinessVaultViewProtocol?) {
        if let view = view as? BusinessVaultController{
            let controller = ChooseBizVaultRouter.createChooseBizVaultModule()
            view.navigationController?.pushViewController(controller, animated: true)
        }
       
        
    }
}
