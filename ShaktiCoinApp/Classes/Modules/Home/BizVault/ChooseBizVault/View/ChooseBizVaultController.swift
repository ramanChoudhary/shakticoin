//
//  ChooseBizVaultController.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 28/04/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class ChooseBizVaultController: UIViewController {

    @IBOutlet var selectVaultButtons: [UIButton]!
    @IBOutlet weak var vaultTypeImageView: UIImageView!
    @IBOutlet weak var vaultDescLabel: UILabel!
    @IBOutlet weak var vaultTitleLabel: UILabel!
    @IBOutlet weak var vaultFeatureLabel: UILabel!
    @IBOutlet weak var vaultFeatureTitleLabel: UILabel!
    
    var selectSrNo:Int = 0
    var presenter:ChooseBizVaultPresenterProtocol?
    
    var dataArr = ChooseBizVaultData.chooseBizVaultArr
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setControllerView()
    }
    
    func setControllerView(){
        self.vaultTypeImageView.image = self.dataArr[self.selectSrNo].imageIcon
        self.vaultDescLabel.text = self.dataArr[self.selectSrNo].desc
        self.vaultFeatureTitleLabel.text = "\(self.dataArr[self.selectSrNo].title ?? "") Feature"
        self.vaultFeatureLabel.text =  self.dataArr[self.selectSrNo].points
        self.vaultTitleLabel.text = self.dataArr[self.selectSrNo].title
        self.selectVaultButtons.forEach { (button) in
            button.setImage(button.tag == self.selectSrNo ? #imageLiteral(resourceName: "Active"):#imageLiteral(resourceName: "Inactive"), for: .normal)
        }
        
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        presenter?.backAction()
    }
    
    @IBAction func notificationAction(_ sender: UIButton) {
        
    }
    
    @IBAction func getStartedAction(_ sender: SCButton) {
        
    }
    
    @IBAction func selectVaultTypeAction(_ sender: UIButton) {
        self.selectSrNo = sender.tag
        self.setControllerView()
    }
    
}

extension ChooseBizVaultController:ChooseBizVaultViewProtocol{
    
}
