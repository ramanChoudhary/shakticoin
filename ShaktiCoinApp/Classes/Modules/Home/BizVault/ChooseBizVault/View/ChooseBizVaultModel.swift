//
//  ChooseBizVaultModel.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 29/04/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class ChooseBizVaultModel: NSObject {
    var title:String?
    var points:String?
    var desc:String?
    var imageIcon:UIImage?
    
    init(title:String,points:String,desc:String,icon:UIImage) {
        self.title = title
        self.points = points
        self.desc = desc
        self.imageIcon = icon
    }

}

class ChooseBizVaultData:NSObject{
    static var chooseBizVaultArr = [
        ChooseBizVaultModel(title: AppStrings.merchant, points: AppStrings.merchantFeaturePoints, desc: AppStrings.merchantDesc, icon: #imageLiteral(resourceName: "Merchant")),
        ChooseBizVaultModel(title: AppStrings.powerMiner, points: AppStrings.powerMinerPoints, desc: AppStrings.powerMinerDesc, icon: #imageLiteral(resourceName: "PowerMiner")),
        ChooseBizVaultModel(title: AppStrings.feducia, points: AppStrings.feduciaPoints, desc: AppStrings.feduciaDesc, icon: #imageLiteral(resourceName: "Shape")),
        ChooseBizVaultModel(title: AppStrings.shaktiExpress, points: AppStrings.shaktiExpressPoints, desc: AppStrings.shaktiExpressDesc, icon: #imageLiteral(resourceName: "Global")),
        ChooseBizVaultModel(title: AppStrings.devDot, points: AppStrings.devDotPoints, desc: AppStrings.devDotDesc, icon: #imageLiteral(resourceName: "Dev"))
    ]
}
