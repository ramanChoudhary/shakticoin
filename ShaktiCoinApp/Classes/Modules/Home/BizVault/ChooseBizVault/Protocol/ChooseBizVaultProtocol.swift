//
//  ChooseBizVaultProtocol.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 29/04/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit

protocol ChooseBizVaultViewProtocol: class {
    var presenter:ChooseBizVaultPresenterProtocol? {get set}
}

protocol ChooseBizVaultPresenterProtocol: class {
    var view:ChooseBizVaultViewProtocol?{ get set }
    var router:ChooseBizVaultRouterProtocol? {get set}
    //View =========> Presenter
       func backAction()
       func presentNotification()

}

protocol ChooseBizVaultRouterProtocol: class {
    static func createChooseBizVaultModule() -> UIViewController
    func backAction(from view:ChooseBizVaultViewProtocol?)
    func presentNotification(from view:ChooseBizVaultViewProtocol?)
}
