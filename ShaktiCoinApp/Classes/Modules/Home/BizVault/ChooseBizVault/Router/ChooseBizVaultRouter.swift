//
//  ChooseBizVaultRouter.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 29/04/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit

class ChooseBizVaultRouter: ChooseBizVaultRouterProtocol {
    static func createChooseBizVaultModule() -> UIViewController {
        let view = ChooseBizVaultController()
        let presenter:ChooseBizVaultPresenterProtocol = ChooseBizVaultPresenter()
        let router:ChooseBizVaultRouterProtocol = ChooseBizVaultRouter()
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        return view
    }
    
    func backAction(from view: ChooseBizVaultViewProtocol?) {
        if let view = view as? ChooseBizVaultController{
            view.navigationController?.popViewController(animated: true)
        }
    }
    
    func presentNotification(from view: ChooseBizVaultViewProtocol?) {
        
    }
}
