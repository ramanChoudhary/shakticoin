//
//  ChooseBizVaultPresenter.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 29/04/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
class ChooseBizVaultPresenter: ChooseBizVaultPresenterProtocol {
    var router: ChooseBizVaultRouterProtocol?
    var view: ChooseBizVaultViewProtocol?
    
    func backAction() {
        router?.backAction(from: view)
    }
    
    func presentNotification() {
        router?.presentNotification(from: view)
    }
}
