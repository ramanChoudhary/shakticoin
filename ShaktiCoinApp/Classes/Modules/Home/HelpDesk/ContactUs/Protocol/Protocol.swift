//
//  Protocol.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 02/05/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit

protocol ContactUsViewProtocol: class {
    var presenter:ContactUsPresenterProtocol? {get set}
}

protocol ContactUsPresenterProtocol: class {
    var view:ContactUsViewProtocol?{ get set }
    var router:ContactUsRouterProtocol? {get set}
    //View =========> Presenter
       func backAction()
       func presentNotification()

}

protocol ContactUsRouterProtocol: class {
    static func createContactUsModule() -> UIViewController
    func backAction(from view:ContactUsViewProtocol?)
    func presentNotification(from view:ContactUsViewProtocol?)
}
