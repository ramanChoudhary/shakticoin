//
//  ContactUsRouter.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 02/05/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit

class ContactUsRouter: ContactUsRouterProtocol {
    static func createContactUsModule() -> UIViewController {
        let view = ContactUsController()
        let presenter:ContactUsPresenterProtocol = ContactUsPresenter()
        let router:ContactUsRouterProtocol = ContactUsRouter()
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        return view
    }
    
    func presentNotification(from view: ContactUsViewProtocol?) {
        
    }
    
    func backAction(from view: ContactUsViewProtocol?) {
        
    }
    
}
