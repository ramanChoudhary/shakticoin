//
//  HelpProtocol.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 02/05/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit

protocol HelpViewProtocol: class {
    var presenter:HelpPresenterProtocol? {get set}
}

protocol HelpPresenterProtocol: class {
    var view:HelpViewProtocol?{ get set }
    var router:HelpRouterProtocol? {get set}
    //View =========> Presenter
       func backAction()
       func presentNotification()

}

protocol HelpRouterProtocol: class {
    static func createHelpModule() -> UIViewController
    func backAction(from view:HelpViewProtocol?)
    func presentNotification(from view:HelpViewProtocol?)
}
