//
//  HelpRouter.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 02/05/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit

class HelpRouter: HelpRouterProtocol {
    static func createHelpModule() -> UIViewController {
        let view = HelpController()
        let presenter:HelpPresenterProtocol = HelpPresenter()
        let router:HelpRouterProtocol = HelpRouter()
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        return view
    }
    
    func presentNotification(from view: HelpViewProtocol?) {
        
    }
    
    func backAction(from view: HelpViewProtocol?) {
        
    }
    
}
