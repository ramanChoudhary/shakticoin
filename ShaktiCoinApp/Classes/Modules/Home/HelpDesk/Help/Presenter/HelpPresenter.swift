//
//  HelpPresenter.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 02/05/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
class HelpPresenter: HelpPresenterProtocol {
    var view: HelpViewProtocol?
    
    var router: HelpRouterProtocol?
    
    func backAction() {
        router?.backAction(from: view)
    }
    
    func presentNotification() {
        router?.presentNotification(from: view)
    }
    
    
}
