//
//  ContactListRouter.swift
//  ShaktiCoinApp
//
//  Created by MAC on 20/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import  UIKit

class ContactListRouter: ContactListRouterProtocol {
    func dismissController(from view: ContactListViewProtocol?) {
        if let view = view as? ContactListController{
            view.dismiss(animated: true , completion: nil)
        }
    }
    
    static func createContactListModule() -> UIViewController {
        let view = ContactListController()
        let presenter:ContactListPresenterProtocol = ContactListPresenter()
        let router:ContactListRouterProtocol = ContactListRouter()
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        return view
    }

}
