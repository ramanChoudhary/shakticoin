//
//  ContactlistProtocols.swift
//  ShaktiCoinApp
//
//  Created by MAC on 20/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
 import UIKit

protocol ContactListViewProtocol: class {
    var presenter:ContactListPresenterProtocol? {get set}
}

protocol ContactListPresenterProtocol: class {
    var view:ContactListViewProtocol?{ get set }
    var router:ContactListRouterProtocol? {get set}
  
    //View =========> Presenter
       func dismissController()
       //Interactor ========== Presenter
    
}

protocol ContactListRouterProtocol: class {
    static func createContactListModule() -> UIViewController
    func dismissController(from view:ContactListViewProtocol?)
}
