//
//  ContactTableHeaderView.swift
//  ShaktiCoinApp
//
//  Created by MAC on 13/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class ContactTableHeaderView: UIView {

    @IBOutlet weak var sortContactCollection: TitlesCollection!
    
    var titlesArr:[String] = ["A","A-E","F-J","K-O","P-T","U-Y","Z"]
    
    override func awakeFromNib() {
        self.sortContactCollection.setTitlesCollection(titles:titlesArr , delegate: self, type: .contact, theme: .light)
    }
    
    @IBAction func leftAction(_ sender: UIButton) {
        self.sortContactCollection.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: false)
    }
    
    @IBAction func rightAction(_ sender: UIButton) {
        self.sortContactCollection.scrollToItem(at: IndexPath(item: titlesArr.count - 1, section: 0), at: .right, animated: false)
    }
}

extension ContactTableHeaderView:TitlesCollectionDelegate{
    func didSelect(item: IndexPath) {
        
    }
}
