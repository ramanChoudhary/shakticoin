//
//  ContactListController.swift
//  ShaktiCoinApp
//
//  Created by MAC on 20/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class ContactListController: UIViewController {
    @IBOutlet weak var contactTableView: UITableView!
    
    let cellIdentifier = "ContactTableCell"
    var presenter:ContactListPresenterProtocol?
    var selectedIndexArr = [IndexPath]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setContactTable()
    }
    
    func setContactTable(){
        self.contactTableView.delegate = self
        self.contactTableView.dataSource = self
        self.contactTableView.register(UINib(nibName: self.cellIdentifier, bundle: nil), forCellReuseIdentifier: self.cellIdentifier)
        self.setTableHeader()
        self.setTableFooter()
    }
    
    func setTableFooter(){
        let footer = UIView(frame: CGRect(x: 0, y: 0, width:SCREEN_SIZE.width - 40, height: 100))
        let button = SCButton(frame:CGRect(x: 20, y: 20, width: SCREEN_SIZE.width - 80, height: 40))
        button.cornerRadius = 20
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "Futura-Medium", size: 14)
        button.setTitle("Add Referrals", for: .normal)
        button.addTarget(self, action: #selector(addReferralAction(_:)), for: .touchUpInside)
        footer.addSubview(button)
        self.contactTableView.tableFooterView = footer
        
    }
    
   @objc func addReferralAction(_ sender:UIButton){
        
    }
    
    func setTableHeader(){
        let header = UINib(nibName: NIB_NAME.contactTableHeaderView, bundle: nil).instantiate(withOwner: self, options: nil).first as? ContactTableHeaderView
        header?.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: 60)
        let view = UIView()
        view.frame = header!.bounds
        view.addSubview(header!)
        self.contactTableView.tableHeaderView = view
    }
    
    
    @IBAction func closeAction(_ sender: UIButton) {
        self.presenter?.dismissController()
    }
    
}

extension ContactListController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath) as? ContactTableCell
        cell?.setCellData(isSelected: self.selectedIndexArr.contains(indexPath))
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedIndexArr.contains(indexPath){
            self.selectedIndexArr = selectedIndexArr.filter({$0 != indexPath})
        }else{
            self.selectedIndexArr.append(indexPath)
        }
        tableView.reloadData()
    }
    
}

extension ContactListController: ContactListViewProtocol{

}


    
    


