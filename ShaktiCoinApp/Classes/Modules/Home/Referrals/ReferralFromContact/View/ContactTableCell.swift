//
//  ContactTableCell.swift
//  ShaktiCoinApp
//
//  Created by MAC on 13/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class ContactTableCell: UITableViewCell {

    @IBOutlet weak var contactNumberLabel: UILabel!
    @IBOutlet weak var selectionView: UIView!
    @IBOutlet weak var contactNameLabel: UILabel!
    @IBOutlet weak var contactImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setCellData(isSelected:Bool){
        self.contactImageView.image = #imageLiteral(resourceName: "pp2")
         self.contactNameLabel.setLabel("allem", #colorLiteral(red: 0.537254902, green: 0.537254902, blue: 0.537254902, alpha: 1), UIFont(name: "Lato-Bold", size: 16) ?? UIFont.systemFont(ofSize: 16))
        self.contactNumberLabel.setLabel("+1 (809)909-9876", #colorLiteral(red: 0.537254902, green: 0.537254902, blue: 0.537254902, alpha: 1), UIFont(name: "Lato-Regular", size: 16) ?? UIFont.systemFont(ofSize: 12))
        self.selectionView.backgroundColor = isSelected ? #colorLiteral(red: 0.8470588235, green: 0.7607843137, blue: 0.5843137255, alpha: 1):#colorLiteral(red: 0.537254902, green: 0.537254902, blue: 0.537254902, alpha: 1)
    }
}
