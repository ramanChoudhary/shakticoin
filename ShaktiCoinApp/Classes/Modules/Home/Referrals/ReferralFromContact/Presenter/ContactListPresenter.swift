//
//  ContactListPresenter.swift
//  ShaktiCoinApp
//
//  Created by MAC on 20/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit
class ContactListPresenter: ContactListPresenterProtocol {
    var view: ContactListViewProtocol?
    var router: ContactListRouterProtocol?
    
    func dismissController() {
        router?.dismissController(from: self.view)
    }
}
