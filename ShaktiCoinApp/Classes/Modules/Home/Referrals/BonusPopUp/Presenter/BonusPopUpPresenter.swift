//
//  BonusPopUpPresenter.swift
//  ShaktiCoinApp
//
//  Created by MAC on 23/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class BonusPopUpPresenter: BonusPopUpPresenterProtocol {
    func dismissController() {
        router?.dismissController(from: self.view)
    }
    
    func referFriends() {
        router?.referFriend(from: self.view)
    }
    
    var view: BonusPopUpViewProtocol?
    var router: BonusPopUpRouterProtocol?
    

}
