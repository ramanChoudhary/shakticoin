//
//  BonusPopUpController.swift
//  ShaktiCoinApp
//
//  Created by MAC on 23/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class BonusPopUpController: UIViewController {
    
    @IBOutlet weak var descLabel: UILabel!
    var presenter: BonusPopUpPresenterProtocol?
    var descString = "Use the wallet once a month for 7 years (84 months).\n\nFollowing 84 months of use, your Bonus Bounty will be unlocked on your next birthday as our gift to you.\n\nTo get it faster, refer friends.\nEach friend you refer reduces the waiting time by one month.\n\nIf you plan to reduce your waiting time strictly through referrals, your Bonus Bounty will be unlocked on your next birthday."
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let mutableStr = NSMutableAttributedString(string: self.descString, attributes: [NSAttributedString.Key.font : UIFont(name: "Lato-Regular", size: 14) ?? UIFont.systemFont(ofSize: 14)])
        let str = NSString(string: self.descString)
        let range = str.range(of: "Use the wallet once a month for 7 years (84 months).")
        let range2 = str.range(of: "To get it faster, refer friends.")
        mutableStr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Lato-Bold", size: 14) ?? UIFont.systemFont(ofSize: 14), range: range)
        mutableStr.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Lato-Bold", size: 14) ?? UIFont.systemFont(ofSize: 14), range: range2)
        self.descLabel.attributedText = mutableStr
    }
    
    
    @IBAction func referFriendAction(_ sender: SCButton) {
        self.presenter?.referFriends()
    }
    
}

extension BonusPopUpController:BonusPopUpViewProtocol{
    
}


extension NSMutableAttributedString{
    func setColorForText(_ textToFind: String, with color: UIColor) {
        let range = self.mutableString.range(of: textToFind, options: .caseInsensitive)
        if range.location != NSNotFound {
            addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        }
    }
}
