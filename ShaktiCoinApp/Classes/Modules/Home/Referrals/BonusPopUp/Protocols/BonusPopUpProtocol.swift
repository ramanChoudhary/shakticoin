//
//  BonusPopUpProtocol.swift
//  ShaktiCoinApp
//
//  Created by MAC on 23/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

protocol BonusPopUpViewProtocol: class {
    var presenter: BonusPopUpPresenterProtocol? {get set}
}

protocol BonusPopUpPresenterProtocol:class {
    var view: BonusPopUpViewProtocol? {get set}
    var router: BonusPopUpRouterProtocol? {get set}
    // view ========= presenter
    func dismissController()
    func referFriends()
}

protocol BonusPopUpRouterProtocol:class {
    static func createBonusPopUpModule() -> UIViewController
    func dismissController(from view: BonusPopUpViewProtocol?)
    func referFriend(from view: BonusPopUpViewProtocol?)
}
