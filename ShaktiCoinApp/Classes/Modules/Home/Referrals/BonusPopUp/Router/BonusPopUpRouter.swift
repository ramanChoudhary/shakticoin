//
//  BonusPopUpRouter.swift
//  ShaktiCoinApp
//
//  Created by MAC on 23/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class BonusPopUpRouter: BonusPopUpRouterProtocol {
    static func createBonusPopUpModule() -> UIViewController {
        let view = BonusPopUpController()
        let presenter:BonusPopUpPresenterProtocol = BonusPopUpPresenter()
        let router:BonusPopUpRouterProtocol = BonusPopUpRouter()
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        return view
    }
    
    func dismissController(from view: BonusPopUpViewProtocol?) {
        if let view = view as? BonusPopUpController{
            view.dismiss(animated: true, completion: nil)
        }
    }
    
    func referFriend(from view: BonusPopUpViewProtocol?) {
        
    }
}
