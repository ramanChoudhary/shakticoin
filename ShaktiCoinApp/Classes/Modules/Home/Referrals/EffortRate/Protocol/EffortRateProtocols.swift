//
//  EffortRateProtocols.swift
//  ShaktiCoinApp
//
//  Created by MAC on 27/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

protocol EffortRateViewProtocol: class {
    var presenter: EffortRatePresenterProtocol? {get set}
}

protocol EffortRatePresenterProtocol:class {
    var view: EffortRateViewProtocol? {get set}
    var router: EffortRateRouterProtocol? {get set}
    // view ========= presenter
    func dismissController()
}

protocol EffortRateRouterProtocol:class {
    static func createEffortRateModule() -> UIViewController
    func popController(from view: EffortRateViewProtocol?)
}
