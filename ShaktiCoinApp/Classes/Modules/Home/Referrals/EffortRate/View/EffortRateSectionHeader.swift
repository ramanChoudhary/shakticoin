//
//  EffortRateSectionHeader.swift
//  ShaktiCoinApp
//
//  Created by MAC on 27/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class EffortRateSectionHeader: UIView {
   
    @IBOutlet weak var filterByNameCollection: TitlesCollection!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var friendButton: UIButton!
    
    var titlesArr:[String] = ["A","A-E","F-J","K-O","P-T","U-Z"]
    
    override func awakeFromNib() {
        self.friendButton.setButtonWithBorder(title: "Nudge Friends", titleColor: UIColor.white, titlefont: UIFont(name: "Futura-Medium", size: 12), border_Width: 1, border_Color: #colorLiteral(red: 0.8470588235, green: 0.7607843137, blue: 0.5843137255, alpha: 1) , cornerRadius: 18)
        self.filterByNameCollection.setTitlesCollection(titles: titlesArr, delegate: self, type: .contact, theme: .dark)
        self.progressView.backgroundColor = #colorLiteral(red: 0.537254902, green: 0.537254902, blue: 0.537254902, alpha: 1)
        self.progressLabel.text = "30 Progressing"
    }
}

extension EffortRateSectionHeader: TitlesCollectionDelegate{
    func didSelect(item: IndexPath) {
        
    }
}
