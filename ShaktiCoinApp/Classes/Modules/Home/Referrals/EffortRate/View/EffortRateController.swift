//
//  EffortRateController.swift
//  ShaktiCoinApp
//
//  Created by MAC on 27/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class EffortRateController: UIViewController {
    
    @IBOutlet weak var effortRateTableView: UITableView!
    var presenter:EffortRatePresenterProtocol?
    
    let alphabetArr = (65...90).map({Character(UnicodeScalar($0))})

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setEffortRateTable()
    }
    
    func setEffortRateTable(){
        self.effortRateTableView.delegate = self
        self.effortRateTableView.dataSource = self
        self.effortRateTableView.registerCell(identifier: cellIdentifier.myReferralTableCell)
        self.setTableHeader()
    }
    
    func setTableHeader(){
        let header = UINib.init(nibName: NIB_NAME.effortRateSectionHeader, bundle: nil).instantiate(withOwner: self, options: nil).first as? EffortRateSectionHeader
        header?.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: 160)
        let view = UIView()
        view.frame = header!.bounds
        view.addSubview(header!)
        self.effortRateTableView.tableHeaderView = view
    }

}

extension EffortRateController: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return alphabetArr.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.myReferralTableCell, for: indexPath) as? MyReferralTableCell
        cell?.setCellDataForEffortRate()
        return cell!
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0{
            return UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        }else{
            let view = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: 90))
            let label = UILabel(frame: CGRect(x: 20, y: 30, width: 30, height: 30))
            label.cornerRadius = 15
            label.textAlignment = .center
            label.backgroundColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
            label.text = "\(self.alphabetArr[section])"
            label.textColor = .white
            label.font = UIFont(name: "Lato-Bold", size: 16)
            view.addSubview(label)
            return view
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }else{
            return 80
        }
    }
}



extension EffortRateController:EffortRateViewProtocol{
    
}
