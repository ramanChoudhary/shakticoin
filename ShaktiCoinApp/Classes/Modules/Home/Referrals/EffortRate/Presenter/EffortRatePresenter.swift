//
//  EffortRatePresenter.swift
//  ShaktiCoinApp
//
//  Created by MAC on 27/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class EffortRatePresenter: EffortRatePresenterProtocol {
    var view: EffortRateViewProtocol?
    
    var router: EffortRateRouterProtocol?
    
    func dismissController() {
        router?.popController(from: self.view)
    }
    
}
