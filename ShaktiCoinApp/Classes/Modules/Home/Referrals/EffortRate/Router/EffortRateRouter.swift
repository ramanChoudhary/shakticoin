//
//  EffortRateRouter.swift
//  ShaktiCoinApp
//
//  Created by MAC on 27/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class EffortRateRouter: EffortRateRouterProtocol {
    static func createEffortRateModule() -> UIViewController {
        let view = EffortRateController()
        let presenter:EffortRatePresenterProtocol = EffortRatePresenter()
        let router: EffortRateRouterProtocol = EffortRateRouter()
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        return view
    }
    
    func popController(from view: EffortRateViewProtocol?) {
        
    }
}
