//
//  AddReferralRouter.swift
//  ShaktiCoinApp
//
//  Created by MAC on 02/03/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class AddReferralRouter: AddReferralRouterProtocols {
    func addFromContact(view: AddReferralViewProtocols?) {
        
    }
    
    func addReferralInfo(view: AddReferralViewProtocols?) {
        
    }
    
    static func createAddReferralModule() -> UIViewController {
        let view = AddReferralController()
        let presenter:AddReferralPresenterProtocols = AddReferralPresenter()
        let router:AddReferralRouterProtocols = AddReferralRouter()
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        return view
    }
}
