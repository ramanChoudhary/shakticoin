//
//  AddReferralController.swift
//  ShaktiCoinApp
//
//  Created by MAC on 23/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class AddReferralController: UIViewController {

    @IBOutlet weak var phoneNumberField: UITextField!
    @IBOutlet weak var phoneNumberBgView: UIView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var emailFieldBgView: UIView!
    
    var presenter:AddReferralPresenterProtocols?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        self.phoneNumberBgView.layer.borderColor = ColorStyle.gold.cgColor
        self.phoneNumberBgView.layer.borderWidth = 1.0
        self.emailFieldBgView.layer.borderColor = ColorStyle.gold.cgColor
        self.emailFieldBgView.layer.borderWidth = 1.0
        self.phoneNumberField.attributedPlaceholder = NSAttributedString(string: "Phone number".localized, attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.emailField.attributedPlaceholder = NSAttributedString(string: "Email Address".localized, attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }

    @IBAction func addReferralInfoAction(_ sender: SCButton) {
        presenter?.addReferralInfo()
        
    }
    
    @IBAction func contactListAction(_ sender: SCButton) {
        presenter?.addFromContact()
    }
}

extension AddReferralController:AddReferralViewProtocols{
  
}
