//
//  AddReferralPresenter.swift
//  ShaktiCoinApp
//
//  Created by MAC on 02/03/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class AddReferralPresenter: AddReferralPresenterProtocols {
    var view: AddReferralViewProtocols?
    var router: AddReferralRouterProtocols?
    
    func addFromContact() {
        router?.addFromContact(view: self.view)
    }
    
    func addReferralInfo() {
        router?.addFromContact(view: self.view)
    }
}
