//
//  AddReferralProtocols.swift
//  ShaktiCoinApp
//
//  Created by MAC on 02/03/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

protocol AddReferralViewProtocols:class {
    var presenter:AddReferralPresenterProtocols? {get set}
}

protocol AddReferralPresenterProtocols:class {
    var view:AddReferralViewProtocols? {get set}
    var router:AddReferralRouterProtocols? {get set}
    // view ========= presenter
    func addFromContact()
    func addReferralInfo()
    
}

protocol AddReferralRouterProtocols:class {
    static func createAddReferralModule() -> UIViewController
    func addFromContact(view:AddReferralViewProtocols?)
    func addReferralInfo(view:AddReferralViewProtocols?)
}
