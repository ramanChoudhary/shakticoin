//
//  MyreferralEntity.swift
//  ShaktiCoinApp
//
//  Created by MAC on 27/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class  ImportFromCollectionModel:NSObject{
    var title: String?
    var icon: UIImage?
    
    init(title:String?,icon:UIImage?) {
        self.title = title
        self.icon = icon
    }
}

class ImportFromData:NSObject{
    static var dataArr:[ImportFromCollectionModel]  =  [
        ImportFromCollectionModel(title: "Facebook", icon: #imageLiteral(resourceName: "Facebook")),
        ImportFromCollectionModel(title: "Instagram", icon: #imageLiteral(resourceName: "Instagram")),
        ImportFromCollectionModel(title: "Google", icon: #imageLiteral(resourceName: "GooglePlus")),
        ImportFromCollectionModel(title: "LinkedIn", icon: #imageLiteral(resourceName: "LinkedIn")),
        ImportFromCollectionModel(title: "Twitter", icon: #imageLiteral(resourceName: "Twitter")),
        ImportFromCollectionModel(title: "Pinterest", icon: #imageLiteral(resourceName: "Pinterest")),
        ImportFromCollectionModel(title: "Skype", icon: #imageLiteral(resourceName: "Skype")),
        ImportFromCollectionModel(title: "WK", icon: #imageLiteral(resourceName: "WK")),
        ImportFromCollectionModel(title: "WeChat", icon: #imageLiteral(resourceName: "WeChat")),
        ImportFromCollectionModel(title: "Tumblr", icon: #imageLiteral(resourceName: "Tumblr")),
        ImportFromCollectionModel(title: "Email", icon: #imageLiteral(resourceName: "Email")),
        ImportFromCollectionModel(title: "Other", icon: #imageLiteral(resourceName: "Other"))
    ]
}

class  UnlockBonusModel:NSObject{
    var title: String?
    var icon: UIImage?
    
    init(title:String?,icon:UIImage?) {
        self.title = title
        self.icon = icon
    }
}

class UnlockBonusData:NSObject{
    static var dataArr:[UnlockBonusModel]  =  [
        UnlockBonusModel(title: "1. Your Bonus Bounty will be automatically unlocked after 7 years of using your wallet at least once a month.", icon: #imageLiteral(resourceName: "Time")),
        UnlockBonusModel(title: "2. You can speed up this process by referring friends and family to join the Shakti World.", icon: #imageLiteral(resourceName: "FriendsReferral")),
        UnlockBonusModel(title: "3. Mix it up:  Use your wallet at least once a month and make your friends early adopters at the same time. In no time, you will be able to unlock your bounty. ", icon: #imageLiteral(resourceName: "Mixing"))
    ]
}


