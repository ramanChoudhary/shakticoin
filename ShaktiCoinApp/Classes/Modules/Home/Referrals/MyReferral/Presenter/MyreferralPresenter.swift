//
//  MyreferralPresenter.swift
//  ShaktiCoinApp
//
//  Created by MAC on 11/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit

class MyReferralPresenter:MyReferralPresenterProtocol{

    func sideMenuAction() {
        self.router?.menuAction(from: self.view)
    }
    
    func notificationAction() {
        self.router?.notificationAction(from: self.view)
    }
    
    var view: MyReferralViewProtocol?
    
    var router: MyReferralRouterProtocol?
    
    var interactor: MyReferralInteractorProtocol?
    
    
}
