//
//  MyReferralRouter.swift
//  ShaktiCoinApp
//
//  Created by MAC on 11/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import  UIKit

class MyReferralRouter:MyReferralRouterProtocol{
    
    static func createMyReferralModule() -> UIViewController {
        let view = MyReferralController()
        var presenter:MyReferralPresenterProtocol = MyReferralPresenter()
        let router:MyReferralRouterProtocol = MyReferralRouter()
        let interactor:MyReferralInteractorProtocol = MyReferralInteractor()
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        return view
    }
    
    func menuAction(from view: MyReferralViewProtocol?) {
        
    }
    
    func notificationAction(from view: MyReferralViewProtocol?) {
        
    }
}
