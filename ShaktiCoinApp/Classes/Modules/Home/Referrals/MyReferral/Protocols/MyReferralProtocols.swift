//
//  MyReferralProtocols.swift
//  ShaktiCoinApp
//
//  Created by MAC on 11/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit

protocol MyReferralViewProtocol {
    var presenter:MyReferralPresenterProtocol?{get set}
}

protocol MyReferralPresenterProtocol {
    var view:MyReferralViewProtocol?{get set}
    var router:MyReferralRouterProtocol?{get set}
    var interactor:MyReferralInteractorProtocol?{get set}
    // view ========= presenter
    func sideMenuAction()
    func notificationAction()
}

protocol MyReferralInteractorProtocol {
    var presenter:MyReferralPresenterProtocol?{get set}
}


protocol MyReferralRouterProtocol {
    static func createMyReferralModule()->UIViewController
    func menuAction(from view: MyReferralViewProtocol?)
    func notificationAction(from view: MyReferralViewProtocol?)
}
