//
//  MyReferralController.swift
//  ShaktiCoinApp
//
//  Created by MAC on 11/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class MyReferralController: UIViewController {

    @IBOutlet weak var myReferralTable: UITableView!
    var presenter:MyReferralPresenterProtocol?
    var footerData = ImportFromData.dataArr
    var reffaralListCount = 12
    var isSelectSeemore:Bool = false
    var isUnlockBonusView:Bool = false
    var unlockBonusData = UnlockBonusData.dataArr
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setMyReferralTable()
    }
    
    func setMyReferralTable(){
        self.myReferralTable.delegate = self
        self.myReferralTable.dataSource = self
        self.myReferralTable.registerCell(identifier: cellIdentifier.myReferralTableCell)
        self.myReferralTable.registerCell(identifier: cellIdentifier.unlockBonusCell)
        self.setTableHeader()
        self.setTableFooter()
    }
    
    func setTableHeader(){
        let header = UINib(nibName: NIB_NAME.myReferralTableHeader, bundle: nil).instantiate(withOwner: self, options: nil).first as? MyReferralTableHeader
        header!.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height:self.isUnlockBonusView ? 200 : 590)
        header!.setHeaderData(isUnlockView: self.isUnlockBonusView)
        if self.isUnlockBonusView == false{
            header!.unlockButton.addTarget(self, action: #selector(self.unlockAction(_:)), for: .touchUpInside)
        }
        let view = UIView(frame: header!.bounds)
        view.addSubview(header!)
        self.myReferralTable.tableHeaderView = view
    }
    
    @objc func unlockAction(_ sender:UIButton){
        self.isUnlockBonusView = true
        self.setTableHeader()
        self.setTableFooter()
        self.myReferralTable.reloadData()
    }
    
    func setTableFooter(){
        let footer = UINib(nibName: NIB_NAME.myReferralTableFooter, bundle: nil).instantiate(withOwner: self, options: nil).first as? MyReferralTableFooter
        footer!.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: 600)
        footer?.setFooterData(data: self.footerData, delegate: self,isUnlockView:self.isUnlockBonusView)
        if isUnlockBonusView == true{
            footer?.startReferingButton.addTarget(self, action: #selector(self.startReferingAction(_:)), for: .touchUpInside)
        }
        let view = UIView(frame: footer!.bounds)
        view.addSubview(footer!)
        self.myReferralTable.tableFooterView = view
    }
    
    @objc func startReferingAction(_ sender:UIButton){
        self.isUnlockBonusView = false
        self.setTableHeader()
        self.setTableFooter()
        self.myReferralTable.reloadData()
    }

    @IBAction func menuAction(_ sender: UIButton) {
        presenter?.sideMenuAction()
    }
    
    @IBAction func notificationAction(_ sender: UIButton) {
        presenter?.notificationAction()
    }
}

extension MyReferralController: MyReferralTableFooterDelegate{
    func didSelectItem(index: Int) {
        print(index)
    }
}

extension MyReferralController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isUnlockBonusView == true{
            return self.unlockBonusData.count
        }else{
            if self.reffaralListCount > 4{
                 return self.isSelectSeemore ? self.reffaralListCount : 4
            }else{
              return  self.reffaralListCount
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.isUnlockBonusView == true{
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.unlockBonusCell, for: indexPath) as? UnlockBonusCell
            cell?.imageIcon.image = self.unlockBonusData[indexPath.row].icon
            cell?.descLabel.text = self.unlockBonusData[indexPath.row].title
            return cell!
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.myReferralTableCell, for: indexPath) as? MyReferralTableCell
            cell?.setCellData(selectedIndex: indexPath.row,visibleIndex:self.isSelectSeemore ? self.reffaralListCount : 4)
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if self.isUnlockBonusView == true{return UIView()}

        if self.reffaralListCount <= 4{
            return UIView()
        }
        let view = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: 40))
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 130, height: 36))
        button.center = view.center
        button.setButtonWithBorder(title: self.isSelectSeemore ? "See Less":"See More", titleColor: .white, titlefont: UIFont(name: "Futura-Medium", size: 12),border_Width: 1,border_Color: #colorLiteral(red: 0.8470588235, green: 0.7607843137, blue: 0.5843137255, alpha: 1),cornerRadius: 18)
        button.addTarget(self, action: #selector(self.seeMoreAction(_:)), for: .touchUpInside)
        view.backgroundColor = .clear
        view.addSubview(button)
        return view
    }
    
    @objc func seeMoreAction(_ sender:UIButton){
        self.isSelectSeemore = !self.isSelectSeemore
        self.myReferralTable.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if self.isUnlockBonusView == true{
            return 0
            
        }else{
            return self.reffaralListCount > 4 ? 40 : 0
        }
    }
}

extension MyReferralController:MyReferralViewProtocol{
    
}
