//
//  MyReferralTableCell.swift
//  ShaktiCoinApp
//
//  Created by MAC on 12/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class MyReferralTableCell: UITableViewCell {

    @IBOutlet weak var selectionView: UIView!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    func setCellData(selectedIndex:Int,visibleIndex:Int){
       
        self.userImageView.image = #imageLiteral(resourceName: "pp2")
        self.emailLabel.text = "creynolds@josefina.biz"
        self.nameLabel.textColor = #colorLiteral(red: 0.8470588235, green: 0.7607843137, blue: 0.5843137255, alpha: 1).toColor(#colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1), percentage: CGFloat(((selectedIndex * 100) / visibleIndex)))
        self.selectionView.backgroundColor = #colorLiteral(red: 0.8470588235, green: 0.7607843137, blue: 0.5843137255, alpha: 1).toColor(#colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1), percentage: CGFloat(((selectedIndex * 100) / visibleIndex)))
        self.nameLabel.text = "Alan Reynolds"
        self.phoneNumberLabel.text = "+1 (809)909-9876"
    }
    
    func setCellDataForEffortRate(){
       
        self.userImageView.image = #imageLiteral(resourceName: "pp2")
        self.emailLabel.text = "creynolds@josefina.biz"
        self.nameLabel.textColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        self.selectionView.backgroundColor = #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1)
        self.nameLabel.text = "Alan Reynolds"
        self.phoneNumberLabel.text = "+1 (809)909-9876"
    }
    
}
