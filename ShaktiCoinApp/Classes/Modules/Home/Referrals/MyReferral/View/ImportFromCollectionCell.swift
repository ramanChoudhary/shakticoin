//
//  ImportFromCollectionCell.swift
//  ShaktiCoinApp
//
//  Created by MAC on 24/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class ImportFromCollectionCell: UICollectionViewCell {

    @IBOutlet weak var importFromLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
