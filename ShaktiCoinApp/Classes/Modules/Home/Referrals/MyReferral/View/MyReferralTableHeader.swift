//
//  MyReferralTableHeader.swift
//  ShaktiCoinApp
//
//  Created by MAC on 24/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class MyReferralTableHeader: UIView {

    @IBOutlet weak var bottomTitleLabel: UILabel!
    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var collectionBgView: UIView!
    @IBOutlet weak var balanceView: UIView!
    @IBOutlet weak var referView: UIView!
    @IBOutlet weak var sortContactCollection: TitlesCollection!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var unlockButton: UIButton!
    @IBOutlet weak var referCountLabel: UILabel!
    @IBOutlet weak var effortRateBtn: SCButton!
    @IBOutlet weak var addReferralButton: SCButton!
    
    
    var titlesArr:[String] = ["A","A-E","F-J","K-O","P-T","U-Z"]
    
    override  func awakeFromNib() { 
        self.sortContactCollection.setTitlesCollection(titles:titlesArr , delegate: self, type: .contact, theme: .dark)
       
    }
    
    func setHeaderData(isUnlockView:Bool){
        self.referView.isHidden = isUnlockView
        self.collectionBgView.isHidden = isUnlockView
        if isUnlockView == false{
            self.unlockButton.underline()
            self.unlockButton.setTitle("30 months to unlock", for: .normal)
            self.topTitleLabel.text = "Refer others to unlock SXE"
        }else{
            self.unlockButton.setTitle("Locked", for: .normal)
            self.topTitleLabel.text = "Would you like to unlock it?"
        }
         
    }
}

extension MyReferralTableHeader: TitlesCollectionDelegate{
    func didSelect(item: IndexPath) {
        
    }

}
