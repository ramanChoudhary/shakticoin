//
//  MyReferralTableFooter.swift
//  ShaktiCoinApp
//
//  Created by MAC on 24/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

protocol MyReferralTableFooterDelegate {
    func didSelectItem(index:Int)
}

class MyReferralTableFooter: UIView {
    @IBOutlet weak var unlockReferDescLabel: UILabel!
    @IBOutlet weak var unlockFooterContentView: UIView!
    @IBOutlet weak var ImportfooterContentView: UIView!
    @IBOutlet weak var footerTitleLabel: UILabel!
    @IBOutlet weak var importFromCollection: UICollectionView!
    @IBOutlet weak var startReferingButton: SCButton!
    
    var importFromData:[ImportFromCollectionModel]?
    var delegate:MyReferralTableFooterDelegate?
    var referDesc = "I will refer 3 people per day, based on my effort I will unlock my bonus bounty within X days."
    
    override func awakeFromNib() {
        self.setImportFromCollection()
    }
    
    func setImportFromCollection(){
        self.importFromCollection.delegate = self
        self.importFromCollection.dataSource = self
        self.importFromCollection.registerCell(identifier: cellIdentifier.importFromCollectionCell)
    }
    
    func setFooterData(data:[ImportFromCollectionModel],delegate:MyReferralTableFooterDelegate,isUnlockView:Bool){
        self.unlockFooterContentView.isHidden = !isUnlockView
        self.ImportfooterContentView.isHidden = isUnlockView
        self.setReferDescAttrebutedString()
        self.footerTitleLabel.text = isUnlockView ? "How fast can you unlock your Bounty?":"Or alternatively"
        self.delegate = delegate
        self.importFromData = data
        self.importFromCollection.reloadData()
    }
    
    func setReferDescAttrebutedString(){
        let mutableStr = NSMutableAttributedString(string: self.referDesc)
        let str = NSString(string: self.referDesc)
        let range = str.range(of: " 3 ")
        let range2 = str.range(of: " X ")
        mutableStr.addAttribute(NSAttributedString.Key.foregroundColor, value: #colorLiteral(red: 0.8470588235, green: 0.7607843137, blue: 0.5843137255, alpha: 1) , range: range)
        mutableStr.addAttribute(NSAttributedString.Key.foregroundColor, value: #colorLiteral(red: 0.8470588235, green: 0.7607843137, blue: 0.5843137255, alpha: 1) , range: range2)
        mutableStr.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: range)
        mutableStr.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: range2)
        self.unlockReferDescLabel.attributedText = mutableStr
    }
    
}

extension MyReferralTableFooter:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return importFromData?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier.importFromCollectionCell, for: indexPath) as? ImportFromCollectionCell
        cell?.importFromLabel.text = self.importFromData?[indexPath.item].title
        cell?.imageView.image = self.importFromData?[indexPath.item].icon
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 80 )
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.didSelectItem(index: indexPath.item)
    }
    
}



