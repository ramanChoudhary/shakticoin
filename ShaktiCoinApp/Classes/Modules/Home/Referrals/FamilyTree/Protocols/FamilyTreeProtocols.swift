//
//  FamilyTreeProtocols.swift
//  ShaktiCoinApp
//
//  Created by MAC on 11/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation

import UIKit

protocol FamilyTreeViewProtocol: class {
    var presenter:FamilyTreePresenterProtocol? {get set}
}

protocol FamilyTreePresenterProtocol: class {
    var view:FamilyTreeViewProtocol?{ get set }
    var router:FamilyTreeRouterProtocol? {get set}
    var interactor:FamilyTreeInteractorProtocol? {get set}
    //View =========> Presenter
       func dismissController()
       func addFamilyMember()
       //Interactor ========== Presenter
    func presentNotification()
}

protocol FamilyTreeInteractorProtocol: class {
    var presenter:FamilyTreePresenterProtocol? {get set}
}

protocol FamilyTreeRouterProtocol: class {
    static func createFamilyTreeModule() -> UIViewController
    func popController(from view:FamilyTreeViewProtocol?)
    func presentNotification(from view:FamilyTreeViewProtocol?)
    func addFamilyMember(from view: FamilyTreeViewProtocol?)
}
