//
//  FamilyTreeRouter.swift
//  ShaktiCoinApp
//
//  Created by MAC on 11/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class FamilyTreeRouter: FamilyTreeRouterProtocol {
    static func createFamilyTreeModule() -> UIViewController {
        let view = FamilyTreeController()
        let presenter:FamilyTreePresenterProtocol = FamilyTreePresenter()
        let router:FamilyTreeRouterProtocol = FamilyTreeRouter()
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        return view
    }
    
    func popController(from view: FamilyTreeViewProtocol?) {
         let appDelegate = UIApplication.shared.delegate as! AppDelegate
               appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
    }
    
    func presentNotification(from view: FamilyTreeViewProtocol?) {
        
    }
    
    func addFamilyMember(from view: FamilyTreeViewProtocol?){
        
    }

}
