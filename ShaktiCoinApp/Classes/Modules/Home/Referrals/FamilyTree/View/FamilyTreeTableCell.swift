//
//  FamilyTreeTableCell.swift
//  ShaktiCoinApp
//
//  Created by MAC on 11/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class FamilyTreeTableCell: UITableViewCell {

    @IBOutlet weak var phoneNoLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var memberImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    func setCellData(){
        self.memberImageView.image = #imageLiteral(resourceName: "pp3")
        self.nameLabel.setLabel("allem", #colorLiteral(red: 0.8470588235, green: 0.7607843137, blue: 0.5843137255, alpha: 1), UIFont(name: "Lato-Bold", size: 16) ?? UIFont.systemFont(ofSize: 16))
        self.emailLabel.setLabel("j.reynolds@gmail.com", .white, UIFont(name: "Lato-Regular", size: 12) ?? UIFont.systemFont(ofSize: 12))
        self.phoneNoLabel.setLabel("+1 (809)909-9876", .white, UIFont(name: "Lato-Regular", size: 12) ?? UIFont.systemFont(ofSize: 12))
    }
    
}
