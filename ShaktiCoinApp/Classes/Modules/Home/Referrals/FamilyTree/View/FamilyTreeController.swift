//
//  FamilyTreeController.swift
//  ShaktiCoinApp
//
//  Created by MAC on 11/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class FamilyTreeController: UIViewController {
    @IBOutlet weak var familyTreeTable: UITableView!
    var presenter:FamilyTreePresenterProtocol?
    let cellIdentifier:String = "FamilyTreeTableCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setFamilyTreeTable()

    }
    
    func setFamilyTreeTable(){
        self.familyTreeTable.delegate = self
        self.familyTreeTable.dataSource = self
        self.familyTreeTable.register(UINib(nibName: self.cellIdentifier, bundle: nil), forCellReuseIdentifier: self.cellIdentifier)
        self.setTableHeader()
        self.setTableFooter()
    }
    
    func setTableHeader(){
        let header = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: 300))
        let imageview = UIImageView(frame: header.bounds)
        imageview.image = #imageLiteral(resourceName: "familyTree")
        header.addSubview(imageview)
        self.familyTreeTable.tableHeaderView = header
    }
    
    func setTableFooter(){
        let footer = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: 100))
        let button = SCButton(frame:CGRect(x: 20, y: 20, width: SCREEN_SIZE.width - 40, height: 40))
        button.cornerRadius = 20
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "Futura-Medium", size: 14)
        button.setTitle("Add a Family Member", for: .normal)
        button.addTarget(self, action: #selector(addMemberAction(_:)), for: .touchUpInside)
        footer.addSubview(button)
        self.familyTreeTable.tableFooterView = footer
        
    }
    
    @objc func addMemberAction(_ sender:UIButton){
        self.presenter?.addFamilyMember()
    }
    
   
    
    @IBAction func notificationAction(_ sender: UIButton) {
        self.presenter?.presentNotification()
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.presenter?.dismissController()
    }
}

extension FamilyTreeController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath) as? FamilyTreeTableCell
        cell?.setCellData()
        return cell!
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UINib.init(nibName: NIB_NAME.familyTreeHeaderView, bundle: nil).instantiate(withOwner: self, options: nil).first as? FamilyTreeHeaderView
        view?.titleLabel.text = "Family Members"
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
}

extension FamilyTreeController:FamilyTreeViewProtocol{
    
}
