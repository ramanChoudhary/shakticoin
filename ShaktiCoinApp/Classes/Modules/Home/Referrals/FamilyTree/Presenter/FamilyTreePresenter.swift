//
//  FamilyTreePresenter.swift
//  ShaktiCoinApp
//
//  Created by MAC on 11/02/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//
class FamilyTreePresenter: FamilyTreePresenterProtocol {
    
    
    var view: FamilyTreeViewProtocol?
    
    var router: FamilyTreeRouterProtocol?
    
    var interactor: FamilyTreeInteractorProtocol?
    
    func dismissController() {
        router?.popController(from: self.view)
    }
    
    func presentNotification() {
        router?.presentNotification(from: self.view)
    }
    
    func addFamilyMember() {
        router?.addFamilyMember(from: self.view)
    }
    

}
