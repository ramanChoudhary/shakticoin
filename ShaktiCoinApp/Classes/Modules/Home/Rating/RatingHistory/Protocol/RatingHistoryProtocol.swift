//
//  RatingHistoryProtocol.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 02/05/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit

protocol RatingHistoryViewProtocol: class {
    var presenter:RatingHistoryPresenterProtocol? {get set}
}

protocol RatingHistoryPresenterProtocol: class {
    var view:RatingHistoryViewProtocol?{ get set }
    var router:RatingHistoryRouterProtocol? {get set}
    //View =========> Presenter
       func dismissAction()

}

protocol RatingHistoryRouterProtocol: class {
    static func createRatingHistoryModule() -> UIViewController
    func dismissAction(from view:RatingHistoryViewProtocol?)
}
