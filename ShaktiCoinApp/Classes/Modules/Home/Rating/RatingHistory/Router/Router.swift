//
//  Router.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 02/05/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit

class RatingHistoryRouter: RatingHistoryRouterProtocol {
    static func createRatingHistoryModule() -> UIViewController {
        let view = RatingHistoryController()
        let presenter:RatingHistoryPresenterProtocol = RatingHistoryPresenter()
        let router:RatingHistoryRouterProtocol = RatingHistoryRouter()
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        return view
    }
    
    func dismissAction(from view: RatingHistoryViewProtocol?) {
        
    }
    
}
