//
//  RatingHistoryPresenter.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 02/05/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
class RatingHistoryPresenter: RatingHistoryPresenterProtocol {
    var router: RatingHistoryRouterProtocol?
    var view: RatingHistoryViewProtocol?
    
    func dismissAction() {
        router?.dismissAction(from: view)
    }
   
}
