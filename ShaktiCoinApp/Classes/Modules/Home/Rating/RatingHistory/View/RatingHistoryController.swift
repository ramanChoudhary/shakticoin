//
//  RatingHistoryController.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 30/04/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class RatingHistoryController: UIViewController {

    @IBOutlet weak var ratingHistoryTable: UITableView!
    
    var presenter:RatingHistoryPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setRatingHistoryTable()
    }
    
    func setRatingHistoryTable(){
        self.ratingHistoryTable.delegate = self
        self.ratingHistoryTable.dataSource = self
        self.ratingHistoryTable.registerCell(identifier: cellIdentifier.ratingHistoryCell)
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
        presenter?.dismissAction()
    }
    
}

extension RatingHistoryController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.ratingHistoryCell, for: indexPath) as? RatingHistoryCell
        return cell!
    }
    
}

extension RatingHistoryController:RatingHistoryViewProtocol{
    
}
