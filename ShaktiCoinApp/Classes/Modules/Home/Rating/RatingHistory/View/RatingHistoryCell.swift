//
//  RatingHistoryCell.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 30/04/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class RatingHistoryCell: UITableViewCell {

    @IBOutlet weak var thumbButton: UIButton!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func thumbAction(_ sender: UIButton) {
    }
}
