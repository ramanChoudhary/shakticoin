//
//  RateYourMerchantPresenter.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 02/05/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
class RateYourMerchantPresenter: RateYourMerchantPresenterProtocol {
    var router: RateYourMerchantRouterProtocol?
    var view: RateYourMerchantViewProtocol?
    
    func menuAction() {
        router?.menuAction(from: view)
    }
    
    func presentNotification() {
        router?.presentNotification(from: view)
    }
}
