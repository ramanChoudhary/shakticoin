//
//  RateYourMerchantRouter.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 02/05/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit

class RateYourMerchantRouter: RateYourMerchantRouterProtocol {
    static func createRateYourMerchantModule() -> UIViewController {
        let view = RateYourMerchantController()
        let presenter:RateYourMerchantPresenterProtocol = RateYourMerchantPresenter()
        let router:RateYourMerchantRouterProtocol = RateYourMerchantRouter()
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        return view
    }
    
    func menuAction(from view: RateYourMerchantViewProtocol?) {
        if let view = view as? RateYourMerchantController{
            view.navigationController?.popViewController(animated: true)
        }
    }
    
    func presentNotification(from view: RateYourMerchantViewProtocol?) {
        
    }
}
