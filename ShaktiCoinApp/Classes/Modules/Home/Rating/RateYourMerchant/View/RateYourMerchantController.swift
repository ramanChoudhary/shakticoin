//
//  RateYourMerchantController.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 29/04/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import UIKit

class RateYourMerchantController: UIViewController {

    @IBOutlet weak var thumbDownButton: VerticalButton!
    @IBOutlet weak var thumbUpButton: UIButton!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var rateButton: UIButton!
    
    var presenter:RateYourMerchantPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func thumbUpAction(_ sender: VerticalButton) {
        
    }
    
    @IBAction func thumbDownButton(_ sender: VerticalButton) {
        
    }
    
    @IBAction func rateAction(_ sender: UIButton) {
    }
    
    @IBAction func menuAction(_ sender: UIButton) {
        
    }
    
    @IBAction func notificationAction(_ sender: UIButton) {
        
    }
}

extension RateYourMerchantController:RateYourMerchantViewProtocol{
    
}
