//
//  RateYourMerchantProtocol.swift
//  ShaktiCoinApp
//
//  Created by Stackgeeks on 02/05/21.
//  Copyright © 2021 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit

protocol RateYourMerchantViewProtocol: class {
    var presenter:RateYourMerchantPresenterProtocol? {get set}
}

protocol RateYourMerchantPresenterProtocol: class {
    var view:RateYourMerchantViewProtocol?{ get set }
    var router:RateYourMerchantRouterProtocol? {get set}
    //View =========> Presenter
       func menuAction()
       func presentNotification()

}

protocol RateYourMerchantRouterProtocol: class {
    static func createRateYourMerchantModule() -> UIViewController
    func menuAction(from view:RateYourMerchantViewProtocol?)
    func presentNotification(from view:RateYourMerchantViewProtocol?)
}
