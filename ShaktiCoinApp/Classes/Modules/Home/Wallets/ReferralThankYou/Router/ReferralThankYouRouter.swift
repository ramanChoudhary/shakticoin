//
//  ReferralThankYouRouter.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 30/09/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit


class ReferralThankYouRouter:ReferralThankYouRouterProtocol {

    static func createReferralThankYouModule() -> UIViewController {
        let view = ReferralThankYouController()
        let presenter:ReferralThankYouPresenterProtocol = ReferralThankYouPresenter()
        let router:ReferralThankYouRouterProtocol = ReferralThankYouRouter()
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        return view
    }
    
    func popController(from view: ReferralThankYouViewProtocol?) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
    }
}
