//
//  ReferralThankYouProtocols.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 30/09/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

import UIKit

protocol ReferralThankYouViewProtocol: class {
    var presenter:ReferralThankYouPresenterProtocol? {get set}
}

protocol ReferralThankYouPresenterProtocol: class {
    var view:ReferralThankYouViewProtocol? {get set}
    var router:ReferralThankYouRouterProtocol? {get set}
    
    //View =========> Presenter
    func dismissController()
    //Interactor ========== Presenter
}


protocol ReferralThankYouRouterProtocol: class {
    static func createReferralThankYouModule() -> UIViewController
    //Presenter -> Router
    func popController(from view:ReferralThankYouViewProtocol?)
}
