//
//  ReferralThankYouPresenter.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 30/09/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

class ReferralThankYouPresenter:ReferralThankYouPresenterProtocol {
    var view: ReferralThankYouViewProtocol?
    
    var router: ReferralThankYouRouterProtocol?
    
    func dismissController() {
        router?.popController(from: self.view)
    }
    
    
}
