//
//  ReferralThankYouController.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 30/09/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

class ReferralThankYouController: UIViewController {

    @IBOutlet weak var coolButton: UIButton!
    var presenter: ReferralThankYouPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        self.coolButton.applyGradientColors(colours: [ColorStyle.darkGold.color, ColorStyle.middleGold.color, ColorStyle.lightGold.color])
    }

    @IBAction func coolAction(_ sender: Any) {
        self.presenter?.dismissController()
    }
    
}

extension ReferralThankYouController:ReferralThankYouViewProtocol {
    
}
