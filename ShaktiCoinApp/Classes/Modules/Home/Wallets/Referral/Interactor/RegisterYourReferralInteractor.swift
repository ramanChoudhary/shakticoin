//
//  RegisterYourReferralInteractor.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 29/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

class RegisterYourReferralInteractor: RegisterYourReferralInteractorProtocol {
    var presenter: RegisterYourReferralPresenterProtocol?
    var dataSource = RemoteDataSource.shared
    
    func rewardReferralRequest(emailOrMobile: String, isEmail: Bool, promotionalCode:String) {
        guard emailOrMobile.count > 0 else {
            self.presenter?.requestFailed(message: "Please enter email or mobile number to register referral.")
            return
        }
        
        if isEmail == true {
            guard Validator().isValidEmail(email: emailOrMobile) == true else {
                self.presenter?.requestFailed(message: "Please enter valid email address.")
                return
            }
        } else {
            guard Validator().isValidPhoneNumber(phone: emailOrMobile) == true else {
                self.presenter?.requestFailed(message: "Please enter valid phone number.")
                return
            }
        }
        
        guard promotionalCode.count > 0 else {
            self.presenter?.requestFailed(message: "Please enter the Promotional Code.")
            return
        }
        
        let params: JSONObject = [
            "emailOrMobile": emailOrMobile,
            "emailRegistration":isEmail,
            "promotionalCode": promotionalCode
        ]
        
        dataSource.requestObject(forEndpoint: Endpoint.BountyReferralEndpoint.registerReferral,  parameters: params) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let json):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(json)
                    }
                    self.presenter?.requestCompleted()
                case .failure(let error):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(error.localizedDescription)
                    }
                    self.presenter?.requestFailed(message: error.localizedDescription)
                }
            }
            
        }
    }
}
