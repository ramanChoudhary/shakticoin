//
//  RegisterYourReferralProtocols.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 29/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation


import UIKit

protocol RegisterYourReferralViewProtocol: class {
    var presenter:RegisterYourReferralPresenterProtocol? {get set}
    
    func referralRegistered()
    func showErrorMessage(message:String)
}

protocol RegisterYourReferralPresenterProtocol: class {
    var view:RegisterYourReferralViewProtocol? {get set}
    var router:RegisterYourReferralRouterProtocol? {get set}
    var interactor:RegisterYourReferralInteractorProtocol? {get set}
    
    //View =========> Presenter
    func rewardReferralRequest(emailOrMobile:String, isEmail:Bool, promotionalCode:String)
    func dismissController()
    func showThankYouScreen()
    //Interactor ========== Presenter
    func requestCompleted()
    func requestFailed(message:String)
}

protocol RegisterYourReferralInteractorProtocol: class {
    var presenter:RegisterYourReferralPresenterProtocol? {get set}
    func rewardReferralRequest(emailOrMobile:String, isEmail:Bool, promotionalCode:String)
}

protocol RegisterYourReferralRouterProtocol: class {
    static func createRegisterYourReferralModule() -> UIViewController
    //Presenter -> Router
    func presentThankYouController(from view:RegisterYourReferralViewProtocol?)
    func popController(from view:RegisterYourReferralViewProtocol?)
}
