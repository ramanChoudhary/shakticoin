//
//  RegisterYourReferralRouter.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 29/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

class RegisterYourReferralRouter: RegisterYourReferralRouterProtocol {
    static func createRegisterYourReferralModule() -> UIViewController {
        let view = RegisterYourReferralController()
        let presenter:RegisterYourReferralPresenterProtocol = RegisterYourReferralPresenter()
        let router:RegisterYourReferralRouterProtocol = RegisterYourReferralRouter()
        let interactor:RegisterYourReferralInteractorProtocol = RegisterYourReferralInteractor()
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        return view
    }
    
    func popController(from view: RegisterYourReferralViewProtocol?) {
        if let view = view as? RegisterYourReferralController {
            view.dismiss(animated: true, completion: nil)
        }
    }
    
    func presentThankYouController(from view: RegisterYourReferralViewProtocol?) {
        if let view = view as? RegisterYourReferralController {
            let controller = ReferralThankYouRouter.createReferralThankYouModule()
            controller.modalPresentationStyle = .overFullScreen
            view.present(controller, animated: true, completion: nil)
        }
    }

}
