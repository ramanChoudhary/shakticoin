//
//  RegisterYourReferralController.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 29/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit
import SKCountryPicker


class RegisterYourReferralController: UIViewController {
    
    @IBOutlet weak var promotionalField: UITextField!
    @IBOutlet weak var promotionalView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailAddressField: UITextField!
    @IBOutlet weak var phoneNumberField: UITextField!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var countryCodeView: UIView!
    @IBOutlet weak var countryCodeLabel: UILabel!
    @IBOutlet weak var flag: UIImageView!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var switchButton: UISwitch!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var scanButton: UIButton!
    @IBOutlet weak var rewardButton: UIButton!
    
    var presenter: RegisterYourReferralPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.emailView.isHidden = true
        self.promotionalField.text =  WalletDataManager.shared.promotionalRefferalCode ?? ""
    }
    
    override func viewDidLayoutSubviews() {
        self.countryCodeView.layer.borderColor = ColorStyle.gold.cgColor
        self.countryCodeView.layer.borderWidth = 1.0
        
        self.phoneView.layer.borderColor = ColorStyle.gold.cgColor
        self.phoneView.layer.borderWidth = 1.0
        self.phoneNumberField.attributedPlaceholder = NSAttributedString(string: "Phone number".localized, attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.emailView.layer.borderColor = ColorStyle.gold.cgColor
        self.emailView.layer.borderWidth = 1.0
        self.emailAddressField.attributedPlaceholder = NSAttributedString(string: "Email Address".localized, attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        self.promotionalView.layer.borderColor = ColorStyle.gold.cgColor
        self.promotionalView.layer.borderWidth = 1.0
        self.promotionalField.attributedPlaceholder = NSAttributedString(string: "Promotional Code".localized, attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.rewardButton.applyGradientColors(colours: [ColorStyle.darkGold.color, ColorStyle.middleGold.color, ColorStyle.lightGold.color])
    }
    
    override func viewDidAppear(_ animated: Bool) {
        guard let country = CountryManager.shared.currentCountry else {
            self.countryCodeLabel.text = "Select Country".localized
            return
        }
        
        self.countryCodeLabel.text =  country.dialingCode
        self.flag.image = country.flag
    }
    

    @IBAction func scanAction(_ sender: Any) {
        let controller = ScannerController()
        controller.callBack = { code in
            if code != nil {
                self.showAlert(errorMessage: "Fetched Code: \(code ?? "")")
            }
        }
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func switchAciton(_ sender: Any) {
        if switchButton.isOn == true {
            self.countryCodeView.isHidden = false
            self.phoneView.isHidden = false
            self.emailView.isHidden = true
            self.phoneNumberLabel.textColor = UIColor.white
            self.emailLabel.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
        } else {
            self.countryCodeView.isHidden = true
            self.phoneView.isHidden = true
            self.emailView.isHidden = false
            self.phoneNumberLabel.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
            self.emailLabel.textColor = UIColor.white
        }
    }
    
    @IBAction func dropDownAction(_ sender: Any) {
        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { [weak self] (country: Country) in
          guard let self = self else { return }
          self.flag.image = country.flag
          self.countryCodeLabel.text = country.dialingCode
        }

        // can customize the countryPicker here e.g font and color
        countryController.detailColor = UIColor.red
    }
    
    @IBAction func rewardAction(_ sender: Any) {
        self.view.endEditing(true)
        showLoader()
        if switchButton.isOn == true {
            let code = self.countryCodeLabel.text ?? ""
            let phone = self.phoneNumberField.text ?? ""
            presenter?.rewardReferralRequest(emailOrMobile: "\(code)\(phone)", isEmail: false, promotionalCode:promotionalField.text ?? "")
        } else {
            presenter?.rewardReferralRequest(emailOrMobile: emailAddressField.text ?? "", isEmail: true, promotionalCode:promotionalField.text ?? "")
        }
        
        //presenter?.showThankYouScreen()
    }
    
    @IBAction func dontRememberAction(_ sender: Any) {
        presenter?.dismissController()
    }
}

extension RegisterYourReferralController:RegisterYourReferralViewProtocol {
    func referralRegistered() {
        hideLoader()
        presenter?.showThankYouScreen()
    }
    
    func showErrorMessage(message: String) {
        hideLoader()
        showAlert(errorMessage: message)
    }
    
    
}
