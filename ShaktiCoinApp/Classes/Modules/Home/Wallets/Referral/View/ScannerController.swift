//
//  ScannerController.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 29/09/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

class ScannerController: UIViewController {

    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var scanButton: UIButton!
    @IBOutlet weak var scannerView: ScannerView!
    
    var qrData: String? = nil
    var callBack:((String?) ->())?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scannerView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !scannerView.isRunning {
            scannerView.startScanning()
        }
        let buttonTitle = scannerView.isRunning ? "Scanning" : "SCAN"
        scanButton.setTitle(buttonTitle, for: .normal)
    }
    
    override func viewDidLayoutSubviews() {
        self.scanButton.applyGradientColors(colours: [ColorStyle.darkGold.color, ColorStyle.middleGold.color, ColorStyle.lightGold.color])
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if !scannerView.isRunning {
            scannerView.stopScanning()
        }
    }

    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func scanAction(_ sender: UIButton) {
        scannerView.isRunning ? scannerView.stopScanning() : scannerView.startScanning()
        let buttonTitle = scannerView.isRunning ? "Scanning" : "SCAN"
        sender.setTitle(buttonTitle, for: .normal)
    }
}

extension ScannerController: ScannerViewDelegate {
    func qrScanningDidStop() {
        let buttonTitle = scannerView.isRunning ? "Scanning" : "SCAN"
        scanButton.setTitle(buttonTitle, for: .normal)
    }
    
    func qrScanningDidFail() {
        showAlert(errorMessage: "Scanning Failed. Please try again")
    }
    
    func qrScanningSucceededWithCode(_ str: String?) {
        
        self.dismiss(animated: true, completion: {
            self.callBack?(str)
        })
    }
}
