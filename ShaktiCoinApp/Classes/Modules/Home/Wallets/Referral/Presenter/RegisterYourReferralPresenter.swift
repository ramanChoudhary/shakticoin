//
//  RegisterYourReferralPresenter.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 29/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

class RegisterYourReferralPresenter: RegisterYourReferralPresenterProtocol {
    
    var view: RegisterYourReferralViewProtocol?
    
    var router: RegisterYourReferralRouterProtocol?
    
    var interactor: RegisterYourReferralInteractorProtocol?
    
    func rewardReferralRequest(emailOrMobile: String, isEmail: Bool, promotionalCode:String) {
        interactor?.rewardReferralRequest(emailOrMobile: emailOrMobile, isEmail: isEmail, promotionalCode:promotionalCode)
    }
    
    func dismissController() {
        router?.popController(from: self.view)
    }

    func showThankYouScreen() {
        router?.presentThankYouController(from: self.view)
    }
    
    func requestCompleted() {
        view?.referralRegistered()
    }
    
    func requestFailed(message: String) {
        view?.showErrorMessage(message: message)
    }
}
