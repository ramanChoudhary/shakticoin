//
//  WalletAdministrationViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/6/19.
//  Copyright © 2019 Garnik Giloyan. All rights reserved.
//

import UIKit

class WalletAdministrationViewController: SingleBaseViewController {

    // MARK: - Declarations
    var presenter: AdminPresenterProtocol?
    
    lazy var currentBalanceLbl : UILabel = {
        let label = LabelPrototypes.Caption
        label.text = "Current balance:"
        return label
    }()

    lazy var balanceValue : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "SXE 0.00"
        label.font = UIFont.lato(fontSize: 36, type: .Regular) //TODO Medium
        return label
    }()

    lazy var balanceHistory : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "Balance History"
        label.font = UIFont.lato(fontSize: 12, type: .Bold)
        label.underline()
        return label
    }()
    
    lazy var tableView : UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        return tableView
    }()
    
    lazy var blackBottomView : UIView = {
        let view = UIView()
        view.backgroundColor = .black
        return view
    }()

    lazy var infoText : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.setup(textInfo: "All wallets are locked by default. It is up to the wallet holder to", textColor: .white, font: .lato(fontSize: 10))
        textView.setText(textContent: "\nsuccessfully unlock their wallets by undergoing KYC, AML & CTF", textColor: .white, font: .lato(fontSize: 10))
        textView.setText(textContent: "\ncompliance screening.", textColor: .white, font: .lato(fontSize: 10))
        textView.finilizeWithCenterlize()
        return textView
    }()
    
    let adminWalletCellId: String = "adminWalletCellId"
    
  //  var titleList = ["", "Status", "Bonus Bounty", "KYC Status", "Password Recovery", "Digital Nation", "Date of Birth", "Micro Lock", "Age Lock", "US Validation"]

    var statusArray = [
            StatusData(type: AdminStatus.none, title: "", status: false, wallet: "Wallet", vault: "bizVault"),
            StatusData(type: AdminStatus.status, title: "Status", status: false, wallet: "Inactive", vault: "Inactive"),
            StatusData(type: .bonusBounty, title: "Bonus Bounty", status: true, wallet: "Locked", vault: "Locked"),
            StatusData(type: .kycStatus, title: "KYC Status", status: true, wallet: "Locked", vault: "Locked"),
            StatusData(type: .passwordRecovery, title: "Password Recovery", status: true, wallet: "Locked", vault: "Locked"),
            StatusData(type: .digitalNation, title: "Digital Nation", status: true, wallet: "Locked", vault: "Locked"),
            StatusData(type: .dateOfBirth, title: "Date of Birth", status: true, wallet: "Locked", vault: "Locked"),
            StatusData(type: .microLock, title: "Micro Lock", status: true, wallet: "Locked", vault: "Locked"),
            StatusData(type: .ageLock, title: "Age Lock", status: true, wallet: "Locked", vault: "Locked"),
            StatusData(type: .usValidation, title: "US Validation", status: true, wallet: "Locked", vault: "Locked")
    ]
    
    var isGrayed : Bool = true
    
    
    // MARK: - ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        initNavBar()
        initUI()
        registerCells()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.showLoader()
        self.presenter?.viewDidLoad()
    }
    
    // MARK: - Methods
    private func initUI() {
        //containerView.backgroundColor = .clear
        containerView.addSubview(currentBalanceLbl)
        containerView.addSubview(balanceValue)
       // containerView.addSubview(balanceHistory)
        containerView.addSubview(tableView)
        containerView.addSubview(blackBottomView)
        view.addSubview(blackBottomView)
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        _ = currentBalanceLbl.anchor(containerView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: topBarHeight+30, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = currentBalanceLbl.centralizeX(containerView.centerXAnchor)
        
        _ = balanceValue.anchor(currentBalanceLbl.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = balanceValue.centralizeX(containerView.centerXAnchor)
        
      //  _ = balanceHistory.anchor(balanceValue.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
       // _ = balanceHistory.centralizeX(containerView.centerXAnchor)
        
        _ = tableView.anchor(balanceValue.bottomAnchor, left: containerView.leftAnchor, bottom: blackBottomView.topAnchor, right: containerView.rightAnchor, topConstant: 15, leftConstant: 0, bottomConstant: 5, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = blackBottomView.anchor(nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 80)
        
        blackBottomView.addSubview(infoText)
        _ = infoText.anchor(nil, left: blackBottomView.leftAnchor, bottom: nil, right: blackBottomView.rightAnchor, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 50)
        _ = infoText.centralizeY(blackBottomView.centerYAnchor)
    }
    
    private func registerCells() {
        tableView.register(AdminTableViewCell.self, forCellReuseIdentifier: adminWalletCellId)
    }
    
    // MARK: - NavBar
    private func initNavBar() {
        self.navigationItem.title = "Administration"
    }
    
    @objc func statusAction(sender:UIButton) {
        print(sender.tag)
        let type = AdminStatus(rawValue: sender.tag)
        switch type {
        case .bonusBounty:
            presenter?.showClaimBounty()
            break
        case .kycStatus:
            presenter?.showKycStatus()
        case .ageLock:
            presenter?.showAgeLock()
        case .dateOfBirth:
            presenter?.showDOB()
        case .digitalNation:
            presenter?.showDigitalNation()
        case .microLock:
            presenter?.showMicroLock()
        case .passwordRecovery:
            presenter?.showPasswordRecovery()
        default:
            break
        }
    }
}


extension WalletAdministrationViewController:AdminViewProtocol {
    func showData(data: AdminEntity) {
        self.hideLoader()
        var indexPathArray = [IndexPath]()
        
        for i in 0..<statusArray.count {
            switch statusArray[i].type {
            case .kycStatus:
                statusArray[i].status = data.kycStatus == "LOCKED" ? true : false
                statusArray[i].wallet = data.kycStatus == "LOCKED" ? "Locked" : "Open"
                statusArray[i].vault = data.kycStatus == "LOCKED" ? "Locked" : "Business Open"
                indexPathArray.append(IndexPath(row: i, section:0))
            case .usValidation:
                statusArray[i].status = data.usValidation == "LOCKED" ? true : false
                statusArray[i].wallet = data.usValidation == "LOCKED" ? "Locked" : "Open"
                statusArray[i].vault = data.usValidation == "LOCKED" ? "Locked" : "Business Open"
                indexPathArray.append(IndexPath(row: i, section:0))
            case .dateOfBirth:
                statusArray[i].status = data.dob == "LOCKED" ? true : false
                statusArray[i].wallet = data.dob == "LOCKED" ? "Locked" : "Open"
                statusArray[i].vault = data.dob == "LOCKED" ? "Locked" : "Business Open"
                indexPathArray.append(IndexPath(row: i, section:0))
            case .ageLock:
                statusArray[i].status = data.age == "LOCKED" ? true : false
                statusArray[i].wallet = data.age == "LOCKED" ? "Locked" : "Open"
                statusArray[i].vault = data.age == "LOCKED" ? "Locked" : "Business Open"
                indexPathArray.append(IndexPath(row: i, section:0))
            case .microLock:
                statusArray[i].status = data.micro == "LOCKED" ? true : false
                statusArray[i].wallet = data.micro == "LOCKED" ? "Locked" : "Open"
                statusArray[i].vault = data.micro == "LOCKED" ? "Locked" : "Business Open"
                indexPathArray.append(IndexPath(row: i, section:0))
            case .digitalNation:
                statusArray[i].status = data.digitalNation == "LOCKED" ? true : false
                statusArray[i].wallet = data.digitalNation == "LOCKED" ? "Locked" : "Open"
                statusArray[i].vault = data.digitalNation == "LOCKED" ? "Locked" : "Business Open"
                indexPathArray.append(IndexPath(row: i, section:0))
            default:
                break
            }
        }
        
        self.tableView.reloadRows(at: indexPathArray, with: .automatic)
    }
    
    func showErrorMessage(message: String) {
        self.hideLoader()
        showAlert(errorMessage: message)
    }
    
    func updateStatus(type: AdminStatus, isLocked: Bool) {
        self.hideLoader()
        let isAllStatusesActive = self.statusArray.filter({$0.status == true})
        if isAllStatusesActive.count == 0 {
            statusArray[1].wallet = "Active"
            statusArray[1].vault = "Active"
            self.tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .automatic)
        }
        
        for i in 0..<statusArray.count {
            if statusArray[i].type == type {
                statusArray[i].status = isLocked
                statusArray[i].wallet = isLocked == true ? "Locked" : "Open"
                statusArray[i].vault = isLocked == true ? "Locked" : "Business Open"
                self.tableView.reloadRows(at: [IndexPath(row: type.rawValue, section: 0)], with: .automatic)
                break
            }
        }
    }
}

extension WalletAdministrationViewController:UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var wIcon : UIImage?
        var vIcon : UIImage?
        
        
        
        
        let type = AdminStatus(rawValue: indexPath.row)
        let statusData = statusArray.filter({$0.type == type}).first
        let cell = tableView.dequeueReusableCell(withIdentifier: adminWalletCellId, for: indexPath as IndexPath) as! AdminTableViewCell
        cell.selectionStyle = .none
        //cell.backgroundColor = rowColor
        cell.titleLbl.text = statusData?.title ?? "" //titleList[indexPath.row]
        cell.walletLbl.tag = indexPath.row
        cell.walletLbl.setTitle(statusData?.wallet, for: .normal)
        cell.walletLbl.addTarget(self, action: #selector(self.statusAction(sender:)), for: .touchUpInside)
        
        cell.wIcon.tintColor = (indexPath.row == 0) ? UIColor.mainColor() : .white
        cell.wIcon.contentMode = .center
        cell.vaultLbl.text = statusData?.vault
        
        cell.vIcon.tintColor = (indexPath.row == 0) ? UIColor.mainColor() : .white
        cell.vIcon.contentMode = .center
        
        if indexPath.row == 0 {
            cell.backgroundColor = UIColor(red:0, green:0, blue:0, alpha:0.6)
        } else {
            if indexPath.row % 2 == 1 {
                cell.backgroundColor = .clear
                isGrayed = false
            } else {
                cell.backgroundColor = UIColor(red:0, green:0, blue:0, alpha:0.2)
                isGrayed = true
            }
        }
        
        
        if indexPath.row == 0 {
            //walletValue = "Wallet"
            //vaultValue = "Vault"
            wIcon = UIImage(named: "Wallet")!
            vIcon = UIImage(named: "Vault")!
        } else if indexPath.row == 1 {
            //walletValue = "Inactive"
            //vaultValue = "Inactive"
            wIcon = UIImage()
            vIcon = UIImage()
        } else {
            //walletValue = "Locked"
            //vaultValue = "Locked"
            wIcon = statusData?.status == true ? UIImage(named: "Lock Icon Small")! : nil
            vIcon = statusData?.status == true ? UIImage(named: "Lock Icon Small")! : nil
        }
        
        cell.wIcon.image = wIcon
        cell.vIcon.image = vIcon
        return cell
        
        //UIColor(red:0.00, green:0.00, blue:0.00, alpha:1.0)
        //
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("row tapped")
    }
}
