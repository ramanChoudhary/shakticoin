//
//  AdminInteractor.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 21/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation


class AdminInteractor:AdminInteractorProtocol {
    var presenter: AdminPresenterProtocol?
    var dataSource = RemoteDataSource.shared
    var repository = ChooseBountyRepository.shared
    
    func requestToGetAllStatus() {
        self.getBonusBountyBalance(type: .bonusBounty)
        self.getKYCstatus(type: .kycStatus)
        self.getPasswordRecoveryStatus(type: .passwordRecovery)
    }
    
    func getBonusBountyBalance(type:AdminStatus) {
        repository.bonusBounty { result in
            DispatchQueue.main.async {
                switch result {
                case .success:
                    self.presenter?.statusResponse(type: type, isLocked: false)
                case .failure(let error):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(error.localizedDescription)
                    }
                    self.presenter?.statusResponse(type: type, isLocked: true)
                }
            }
        }
    }
    
    func getKYCstatus(type:AdminStatus) {
        dataSource.requestObject(forEndpoint: Endpoint.KycEndpoint.getStatus) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let json):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(json)
                    }
                    do {
                        let model = try Mapper<AdminEntity>.map(object:json)
                        self.presenter?.kycStatusResponse(adminEntity: model)
                    } catch (let error) {
                        self.presenter?.requestFailed(message: error.localizedDescription)
                    }
                case .failure(let error):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(error.localizedDescription)
                    }
                    self.presenter?.requestFailed(message: error.localizedDescription)
                }
            }
        }
    }
    
    func getPasswordRecoveryStatus(type:AdminStatus) {
        dataSource.requestObject(forEndpoint: Endpoint.Onboard.getPasswordRecoveryStatus) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let json):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(json)
                    }
                    self.presenter?.statusResponse(type: type, isLocked: false)
                case .failure(let error):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(error.localizedDescription)
                    }
                    self.presenter?.statusResponse(type: type, isLocked: true)
                }
            }
        }
    }
    
}
