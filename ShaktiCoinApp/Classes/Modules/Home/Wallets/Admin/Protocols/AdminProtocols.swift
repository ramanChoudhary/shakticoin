//
//  AdminProtocols.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 21/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit

protocol AdminViewProtocol: class {
    var presenter:AdminPresenterProtocol? {get set}
    func showData(data:AdminEntity)
    func showErrorMessage(message:String)
    func updateStatus(type:AdminStatus, isLocked:Bool)
}

protocol AdminPresenterProtocol: class {
    var view:AdminViewProtocol? {get set}
    var router:AdminRouterProtocol? {get set}
    var interactor:AdminInteractorProtocol? {get set}
    
    //View =========> Presenter
    func viewDidLoad()
    func showClaimBounty()
    func showPasswordRecovery()
    func showKycStatus()
    func showDigitalNation()
    func showDOB()
    func showMicroLock()
    func showAgeLock()
    func showUsValidation()
    //Interactor ========== Presenter
    func statusResponse(type:AdminStatus, isLocked:Bool)
    func kycStatusResponse(adminEntity:AdminEntity)
    func requestFailed(message:String)
}

protocol AdminInteractorProtocol: class {
    var presenter:AdminPresenterProtocol? {get set}
    
    func requestToGetAllStatus()
}

protocol AdminRouterProtocol: class {
    static func createAdminModule() -> UIViewController
    //Presenter -> Router
    func presentClaimBountyBonusScreen(from view:AdminViewProtocol?)
    func presentPasswordRecoveryScreen(from view:AdminViewProtocol?)
    func presentKycScreen(from view:AdminViewProtocol?)
    func presentDigitalNation(from view:AdminViewProtocol?)
    func presentDOB(from view:AdminViewProtocol?)
    func presentMicroLockScreen(from view:AdminViewProtocol?)
    func presentAgeLockScreen(from view:AdminViewProtocol?)
    func presentUsValidationScreen(from view:AdminViewProtocol?)
}
