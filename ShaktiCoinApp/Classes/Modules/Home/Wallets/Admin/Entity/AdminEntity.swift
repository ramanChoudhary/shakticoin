//
//  AdminEntity.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 25/09/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation


enum AdminStatus:Int {
    case none = 0
    case status = 1
    case bonusBounty
    case kycStatus
    case passwordRecovery
    case digitalNation
    case dateOfBirth
    case microLock
    case ageLock
    case usValidation
    
}

struct StatusData {
    var type:AdminStatus
    var title:String
    var status:Bool
    var wallet:String
    var vault:String
}

struct AdminEntity : MapRepresentable {
    var kycStatus: String
    var digitalNation:String
    var usValidation: String
    var micro:String
    var age:String
    var dob:String
    
    init(map: Map) throws {
        kycStatus = try map.get("kycStatus")
        digitalNation = try map.get("digitalNation")
        usValidation = try map.get("usValidation")
        micro = try map.get("micro")
        age = try map.get("age")
        dob = try map.get("dob")
    }
}
