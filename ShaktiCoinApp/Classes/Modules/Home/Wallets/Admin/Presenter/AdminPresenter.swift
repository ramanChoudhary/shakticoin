//
//  AdminPresenter.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 21/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation


class AdminPresenter: AdminPresenterProtocol {
    var view: AdminViewProtocol?
    
    var router: AdminRouterProtocol?
    
    var interactor: AdminInteractorProtocol?
    
    func viewDidLoad() {
        interactor?.requestToGetAllStatus()
    }
    
    func showClaimBounty() {
        router?.presentClaimBountyBonusScreen(from: self.view)
    }
    
    func showPasswordRecovery() {
        router?.presentPasswordRecoveryScreen(from: self.view)
    }
    
    func showKycStatus() {
        router?.presentKycScreen(from: self.view)
    }
    
    func showDigitalNation() {
        router?.presentDigitalNation(from: self.view)
    }
    
    func showDOB() {
        router?.presentDOB(from: self.view)
    }
    
    func showMicroLock() {
        router?.presentMicroLockScreen(from: self.view)
    }
    
    func showAgeLock() {
        router?.presentAgeLockScreen(from: self.view)
    }
    
    func showUsValidation() {
        router?.presentUsValidationScreen(from: self.view)
    }
    
    func statusResponse(type: AdminStatus, isLocked: Bool) {
        view?.updateStatus(type: type, isLocked: isLocked)
    }
    
    func requestFailed(message: String) {
        view?.showErrorMessage(message: message)
    }
    
    func kycStatusResponse(adminEntity: AdminEntity) {
        view?.showData(data: adminEntity)
    }
}
