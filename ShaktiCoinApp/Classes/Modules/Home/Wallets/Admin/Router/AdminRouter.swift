//
//  AdminRouter.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 21/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit


class AdminRouter:AdminRouterProtocol {
   
    static func createAdminModule() -> UIViewController {
        let walletAdmin = WalletAdministrationViewController()
        let presenter:AdminPresenterProtocol = AdminPresenter()
        let router:AdminRouterProtocol = AdminRouter()
        let interactor:AdminInteractorProtocol = AdminInteractor()
        walletAdmin.presenter = presenter
        presenter.view = walletAdmin
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        return walletAdmin
    }
    
    func presentClaimBountyBonusScreen(from view: AdminViewProtocol?) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = ChooseBountyWireframe.createModule()
    }
    
    func presentPasswordRecoveryScreen(from view: AdminViewProtocol?) {
        if let view = view as? WalletAdministrationViewController {
            let controller = PasswordRecoveryEnterEmailWireframe.createModule()
            view.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func presentKycScreen(from view: AdminViewProtocol?) {
        if let view = view as? WalletAdministrationViewController {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "KycValidationViewController") as! KycValidationViewController
            view.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func presentDigitalNation(from view: AdminViewProtocol?) {
        
    }
    
    func presentDOB(from view: AdminViewProtocol?) {
        
    }
    
    func presentMicroLockScreen(from view: AdminViewProtocol?) {
        
    }
    
    func presentAgeLockScreen(from view: AdminViewProtocol?) {
        
    }
    
    func presentUsValidationScreen(from view: AdminViewProtocol?) {
        
    }
}
