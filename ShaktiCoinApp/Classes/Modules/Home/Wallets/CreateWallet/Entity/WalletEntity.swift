//
//  Entity.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 19/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation


struct WalletEntity : MapRepresentable {
    var status: Int
    var details:DetailsEntity
    
    init(map: Map) throws {
        status = try map.get("status")
        details = try map.get("details")
    }
}

struct DetailsEntity:MapRepresentable {
    var message:String
    var walletAddress:String
    var walletBytes:String
    var sessionToken:Int
    
    init(map: Map) throws {
        message = try map.get("message")
        walletAddress = try map.get("walletAddress")
        walletBytes = try map.get("walletBytes")
        sessionToken = try map.get("sessionToken")
    }
}
