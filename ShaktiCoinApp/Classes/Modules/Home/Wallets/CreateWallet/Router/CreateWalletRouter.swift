//
//  CreateWalletRouter.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 18/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit


class CreateWalletRouter:CreateWalletRouterProtocol {
    
    static func createNewWalletModule() -> UIViewController {
        let navController = UIStoryboard.rootController(identifier: STORYBOARD_ID.createWalletController)
        
        if let view = navController as? CreateWalletController {
            let presenter:CreateWalletPresenterProtocol = CreateWalletPresenter()
            let router:CreateWalletRouterProtocol = CreateWalletRouter()
            let interactor:CreateWalletInteractorProtocol = CreateWalletInteractor()
            view.presenter = presenter
            presenter.view = view
            presenter.router = router
            presenter.interactor = interactor
            interactor.presenter = presenter
            return navController
        }
        
        return UIViewController()
    }
    
    
}
