//
//  CreateWalletPresenter.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 18/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation


class CreateWalletPresenter:CreateWalletPresenterProtocol{
    var view: CreateWalletViewProtocol?
    var router: CreateWalletRouterProtocol?
    var interactor: CreateWalletInteractorProtocol?
    
    func viewDidLoad() {
        
    }
   
    func validatePasspharase(passPhrase: String?, confirmPassphrase: String?) {
        interactor?.validatePassphrase(passPhrase: passPhrase, confirmPassphrase: confirmPassphrase)
    }
    
    func requestToCreateWallet(passphrase: String) {
        interactor?.request(passphrase: passphrase)
    }
    
    func validationResult(status: Bool, message: String, passPhrase:String) {
        view?.validationResult(status: status, message: message, passPhrase: passPhrase)
    }
    
    func requestFailed(message: String) {
        view?.showErrorMessage(message: message)
    }
    
    func requestCompleted(data: WalletEntity) {
        view?.showData(data: data)
    }
    
}
