//
//  CreateWalletProtocols.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 18/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit


protocol CreateWalletViewProtocol: class {
    var presenter:CreateWalletPresenterProtocol? {get set}
    func validationResult(status:Bool, message:String, passPhrase:String)
    func showData(data:WalletEntity)
    func showErrorMessage(message:String)
}

protocol CreateWalletPresenterProtocol: class {
    var view:CreateWalletViewProtocol? {get set}
    var router:CreateWalletRouterProtocol? {get set}
    var interactor:CreateWalletInteractorProtocol? {get set}
    
    //View =========> Presenter
    func viewDidLoad()
    func validatePasspharase(passPhrase:String?, confirmPassphrase:String?)
    func requestToCreateWallet(passphrase: String)
    
    //Interactor ========== Presenter
    func validationResult(status:Bool, message:String, passPhrase:String)
    func requestCompleted(data:WalletEntity)
    func requestFailed(message:String)
}

protocol CreateWalletInteractorProtocol: class {
    var presenter:CreateWalletPresenterProtocol? {get set}
    
    func validatePassphrase(passPhrase:String?, confirmPassphrase:String?)
    func request(passphrase: String)
}

protocol CreateWalletRouterProtocol: class {
    static func createNewWalletModule() -> UIViewController
    //Presenter -> Router
}
