//
//  CreateWalletController.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 18/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

class CreateWalletController: UIViewController {
    
    @IBOutlet weak var enterPassPhraseView: UIView!
    @IBOutlet weak var enterPassPhraseTxtField: UITextField!
    @IBOutlet weak var confirmPassPhraseView: UIView!
    @IBOutlet weak var confirmPassPhraseTxtField: UITextField!
    @IBOutlet weak var cancelBtn: SCButton!
    @IBOutlet weak var submitBtn: SCButton!

    var presenter: CreateWalletPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    //MARK:- Private Methods
    private func setUpUI() {
        cancelBtn.CornerRadius = 17.5
        submitBtn.CornerRadius = 17.5
        
        self.enterPassPhraseView.layer.cornerRadius = 3
        self.enterPassPhraseView.layer.borderWidth = 1
        self.enterPassPhraseView.layer.borderColor = ColorStyle.gold.cgColor
        
        self.confirmPassPhraseView.layer.cornerRadius = 3
        self.confirmPassPhraseView.layer.borderWidth = 1
        self.confirmPassPhraseView.layer.borderColor = ColorStyle.gold.cgColor
    }
        
    //MARK:- Button Actions
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion:  nil)
    }
    
    @IBAction func submitAction(_ sender: Any) {
        self.view.endEditing(true)
        self.presenter?.validatePasspharase(passPhrase: enterPassPhraseTxtField.text, confirmPassphrase: confirmPassPhraseTxtField.text)
    }
}

extension CreateWalletController:CreateWalletViewProtocol {
    func validationResult(status: Bool, message: String, passPhrase:String) {
        if status == true {
            showLoader()
            presenter?.requestToCreateWallet(passphrase: passPhrase)
        } else {
            showAlert(errorMessage: message)
        }
    }
    
    func showData(data: WalletEntity) {
        //hud.dismiss()
        WalletDataManager.shared.set(wallet: data.details.walletBytes)
        hideLoader()
        self.dismiss(animated: true, completion: nil)
    }
    
    func showErrorMessage(message: String) {
        hideLoader()
        showAlert(errorMessage: message)
    }
}
