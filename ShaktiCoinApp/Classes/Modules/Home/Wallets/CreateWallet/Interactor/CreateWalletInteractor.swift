//
//  CreateWalletInteractor.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 18/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation


class CreateWalletInteractor: CreateWalletInteractorProtocol {
    
    var presenter: CreateWalletPresenterProtocol?
    var dataSource = RemoteDataSource.shared
    
    func validatePassphrase(passPhrase: String?, confirmPassphrase: String?) {
        guard let ppText = passPhrase else {
            presenter?.validationResult(status: false, message: "Enter Passphrase", passPhrase: "")
            return
        }
        guard let confirmppText = confirmPassphrase else {
            presenter?.validationResult(status: false, message: "Enter Confirm Passphrase", passPhrase: "")
            return
        }
        
        if ppText.isEmpty || confirmppText.isEmpty {
            presenter?.validationResult(status: false, message: "Enter Passphrase", passPhrase: "")
        } else if ppText == confirmppText  {
            presenter?.validationResult(status: true, message: "", passPhrase: ppText)
        } else {
            presenter?.validationResult(status: false, message: "Passphrase does not match!", passPhrase: "")
        }
    }
    
    func request(passphrase: String) {
        let params: JSONObject = [
            "authorizationBytes": "",
            "passphrase":passphrase
        ]
        dataSource.requestObject(forEndpoint: Endpoint.Onboard.createWallet,  parameters: params) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let json):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(json)
                    }
                    do {
                        let model = try Mapper<WalletEntity>.map(object:json)
                        self.presenter?.requestCompleted(data: model)
                    } catch (let error) {
                        self.presenter?.requestFailed(message: error.localizedDescription)
                    }
                case .failure(let error):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(error.localizedDescription)
                    }
                   self.presenter?.requestFailed(message: error.localizedDescription)
                }
            }
        }
    }
}
