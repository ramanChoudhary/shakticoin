//
//  PayCoinsProtocols.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 29/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation


import UIKit



protocol PayCoinsViewProtocol: class {
    var presenter:PayCoinsPresenterProtocol? {get set}
    func showData(data:PayCoinsModel)
    func showErrorMessage(message:String)
}

protocol PayCoinsPresenterProtocol: class {
    var view:PayCoinsViewProtocol? {get set}
    var router:PayCoinsRouterProtocol? {get set}
    var interactor:PayCoinsInteractorProtocol? {get set}
    var payType:PayCoinType? {get set}
    //View =========> Presenter
    func sendCoins(email:String, amount:String)
    func dismissPopup()
    //Interactor ========== Presenter
    func requestCompleted(data:PayCoinsModel)
    func requestFailed(message:String)
}

protocol PayCoinsInteractorProtocol: class {
    var presenter:PayCoinsPresenterProtocol? {get set}
    func createSessionRequest(address:String,amount:String)
}

protocol PayCoinsRouterProtocol: class {
    static func createPayCoinsModule(payOptions:PayCoinType) -> UIViewController
    //Presenter -> Router
    func popController(from view:PayCoinsViewProtocol?)
}
