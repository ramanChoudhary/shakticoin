//
//  PayCoinsModel.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 08/10/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum PayCoinType {
    case pay
    case receive
    case bizVault
}

struct PayCoinsModel : MapRepresentable {
    var message: String
    var transactionId:String
    
    init(map: Map) throws {
        message = try map.get("message")
        transactionId = try map.get("transactionId")
    }
}
