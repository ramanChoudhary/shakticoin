//
//  PayCoinsInteractor.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 29/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

class PayCoinsInteractor: PayCoinsInteractorProtocol {
    
    var presenter: PayCoinsPresenterProtocol?
    var dataSource = RemoteDataSource.shared
    
    
    func payCoinsRequest(address:String,amount:String, sessionToken:Int) {
        
        guard Validator.shared().isValidEmail(email: address) == true else {
            self.presenter?.requestFailed(message: "Please enter valid email address")
            return
        }
        
        guard (Double(amount) ?? 0) > 0 else {
             self.presenter?.requestFailed(message: "SXE amount must be greater than 0.0")
            return
        }
        
        let params: JSONObject = [
            "addressNumber": 0,
            "cacheBytes": "",
            "messageForRecipient": "Test message",
            "sessionToken": sessionToken,
            "toEmail": address,
            "valueInToshi": amount
        ]
        
        dataSource.requestObject(forEndpoint: Endpoint.WalletServiceEndpoint.transferCoins,  parameters: params) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let json):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(json)
                    }
                    do {
                        let model = try Mapper<PayCoinsModel>.map(object: json)
                        self.presenter?.requestCompleted(data: model)
                    } catch (let error) {
                        self.presenter?.requestFailed(message: error.localizedDescription)
                    }

                case .failure(let error):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(error.localizedDescription)
                    }
                    self.presenter?.requestFailed(message: error.localizedDescription)
                }
            }
        }
    }
    
    func createSessionRequest(address:String,amount:String) {
        let params: JSONObject = [
            "cacheBytes": "",
            "passphrase": WalletDataManager.shared.passphrase ?? "",
            "walletBytes": WalletDataManager.shared.wallet ?? ""
        ]
        dataSource.requestObject(forEndpoint: Endpoint.WalletServiceEndpoint.createSession,  parameters: params) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let json):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(json)
                    }
                    if let sessionToken = json["sessionToken"] as? String {
                        self.payCoinsRequest(address: address, amount: amount, sessionToken: Int(sessionToken) ?? 0)
                    } else if let sessionToken = json["sessionToken"] as? Int {
                        self.payCoinsRequest(address: address, amount: amount, sessionToken: sessionToken)
                    } else {
                        self.presenter?.requestFailed(message: "Session Token parsing error")
                    }
                case .failure(let error):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(error.localizedDescription)
                    }
                    self.presenter?.requestFailed(message: error.localizedDescription)
                }
            }
        }
    }
    

}
