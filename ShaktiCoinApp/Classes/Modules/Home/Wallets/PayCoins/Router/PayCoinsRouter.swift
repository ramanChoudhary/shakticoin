//
//  PayCoinsRouter.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 29/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

class PayCoinsRouter: PayCoinsRouterProtocol {
    static func createPayCoinsModule(payOptions:PayCoinType) -> UIViewController {
        let view = PayCoinsController()
        let presenter:PayCoinsPresenterProtocol = PayCoinsPresenter()
        let router:PayCoinsRouterProtocol = PayCoinsRouter()
        let interactor:PayCoinsInteractorProtocol = PayCoinsInteractor()
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        presenter.payType = payOptions
        interactor.presenter = presenter
        return view
    }
    
    func popController(from view: PayCoinsViewProtocol?) {
        if let controller = view as? PayCoinsController {
            controller.dismiss(animated: true, completion: nil)
        }
    }
}
