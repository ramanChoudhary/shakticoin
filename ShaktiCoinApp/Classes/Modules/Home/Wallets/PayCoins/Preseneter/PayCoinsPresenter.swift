//
//  PayCoinsPresenter.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 29/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

class PayCoinsPresenter: PayCoinsPresenterProtocol {
   
    var view: PayCoinsViewProtocol?
    var router: PayCoinsRouterProtocol?
    var interactor: PayCoinsInteractorProtocol?
    var payType: PayCoinType?
    
    func sendCoins(email: String, amount: String) {
        interactor?.createSessionRequest(address: email, amount: amount)
    }
    
    func dismissPopup() {
        router?.popController(from: self.view)
    }
    
    func requestFailed(message: String) {
        self.view?.showErrorMessage(message: message)
    }
    
    func requestCompleted(data: PayCoinsModel) {
        self.view?.showData(data: data)
    }
    
}
