//
//  PayCoinsController.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 29/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit
import JGProgressHUD

class PayCoinsController: UIViewController {

    @IBOutlet weak var payReceiveLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var amountField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var paylabel: UILabel!
    var presenter: PayCoinsPresenterProtocol?
    let hud = JGProgressHUD(style: .dark)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch self.presenter?.payType {
        case .pay:
            self.payReceiveLabel.text = "Pay with"
            self.paylabel.text = "SXE"
            break
        case .receive:
            self.payReceiveLabel.text = "Receive with"
            self.paylabel.text = "SXE"
            break
        case .bizVault:
            self.payReceiveLabel.text = "Pay with"
            self.paylabel.text = "bizVault"
            break
        default:
            break
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        self.emailField.layer.borderColor = ColorStyle.gold.cgColor
        self.emailField.layer.borderWidth = 1.0
        
        self.amountField.layer.borderColor = ColorStyle.gold.cgColor
        self.amountField.layer.borderWidth = 1.0
        
        self.sendButton.applyGradientColors(colours: [ColorStyle.darkGold.color, ColorStyle.middleGold.color, ColorStyle.lightGold.color])
        self.cancelButton.applyGradientColors(colours: [ColorStyle.darkGold.color, ColorStyle.lightGold.color])
    }

    @IBAction func closeAction(_ sender: Any) {
        presenter?.dismissPopup()
    }
    
    @IBAction func sendAction(_ sender: Any) {
        self.view.endEditing(true)
        hud.show(in: self.view)
        self.presenter?.sendCoins(email: emailField.text ?? "", amount: (amountField.text ?? "").convertToToshi())
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        presenter?.dismissPopup()
    }
    
}

extension PayCoinsController:PayCoinsViewProtocol {
    func showData(data: PayCoinsModel) {
         hud.dismiss()
        let alert = UIAlertController(title: data.message, message: "Your transaction id is \(data.transactionId)", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: {_ in
            self.presenter?.dismissPopup()
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func showErrorMessage(message: String) {
         hud.dismiss()
        showAlert(errorMessage: message)
    }
    
    
}
