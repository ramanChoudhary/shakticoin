//
//  MyWalletPresenter.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 20/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation


class MyWalletPresenter: MyWalletPresenterProtocol {
    var view: MyWalletViewProtocol?
    var router: MyWalletRouterProtocol?
    var interactor: MyWalletInteractorProtocol?
    
    func viewDidLoad() {
        interactor?.getBonusBountyBalance()
        interactor?.apiRequestToGetBizVaultStatus()
        if WalletDataManager.shared.wallet == nil {
            view?.populate(.loading)
            interactor?.getWalletRequest()
        } else if WalletDataManager.shared.passphrase == nil {
            view?.populate(.loaded)
            view?.promptForPassphrase()
        } else {
            view?.populate(.loading)
            interactor?.createSessionRequest()
        }
    }
    
    func presentCreateWallet() {
        router?.presentCreateWalletView(from: self.view)
    }
    
    @objc func showBalanceHistory() {
        router?.presentBalanceHistory(from: self.view)
    }
    
    func showAdmin() {
        router?.presentAdminScreen(from: self.view)
    }
    
    func showBusinessVault() {
        router?.presentBusinessVault(from: self.view)
    }
    
    func showMyReferrals() {
        router?.presentMyReferralScreen(from: self.view)
    }
    
    func showMiner() {
        router?.presentMinerScreen(from: self.view)
    }
    
    func showClaimBountyBonus() {
        router?.presentClaimBountyBonusScreen(from: self.view)
    }
    
    func payPopup(payWith: PayCoinType) {
        router?.presentPayPopup(from: self.view, payOptions: payWith)
    }
    
    func requestFailed(message: String) {
        view?.populate(.loaded)
        view?.showErrorMessage(message: message)
    }
    
    func requestCompleted(data: MyWalletEntity) {
        view?.populate(.loaded)
        view?.showData(data: data)
    }
    
    func bountyBalanceResponse(isClaimed: Bool, balance: String) {
        view?.populate(.loaded)
        view?.bountyBalance(isClaimed: isClaimed, balance: balance)
    }
    
    func bizVaultStatus(isActivated: Bool) {
        self.view?.updateUIForBizVault(status: isActivated)
    }
    
    func walletBytesReceived(walletIdResponse: WalletIdResponse) {
        if WalletDataManager.shared.passphrase == nil {
            view?.populate(.loaded)
            view?.promptForPassphrase()
        } else {
            view?.populate(.loading)
            interactor?.createSessionRequest()
        }
    }
}
