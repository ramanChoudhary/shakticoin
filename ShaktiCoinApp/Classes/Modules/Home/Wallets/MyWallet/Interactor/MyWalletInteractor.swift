//
//  MyWalletInteractor.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 20/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

class MyWalletInteractor: MyWalletInteractorProtocol {
    var presenter: MyWalletPresenterProtocol?
    var dataSource = RemoteDataSource.shared
    var repository = ChooseBountyRepository.shared
    
    func getWalletRequest() {
        dataSource.requestObject(forEndpoint: Endpoint.KycEndpoint.getWallet) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let json):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(json)
                    }
                    if let walletId = json["walletID"] as? String {
                        self.getWalletBytesRequest(walletId: walletId)
                    }
                case .failure(let error):
                    self.presenter?.requestFailed(message: error.localizedDescription)
                }
            }
            
        }
    }
    
    func getWalletBytesRequest(walletId:String) {
        let encodedWalletId = walletId.addingPercentEncodingForURLQueryValue()
        dataSource.requestObject(forEndpoint: Endpoint.SelfyidEndpoint.search(encodedWalletId!)) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let json):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(json)
                    }
                    do {
                        let model = try Mapper<WalletIdResponse>.map(object: json)
                        WalletDataManager.shared.set(wallet: model.walletBytes ?? "")
                        self.presenter?.walletBytesReceived(walletIdResponse: model)
                    } catch (let error) {
                        self.presenter?.requestFailed(message: error.localizedDescription)
                    }
                case .failure(let error):
                    self.presenter?.requestFailed(message: error.localizedDescription)
                }
            }
            
        }
    }
    
    func getWalletBalanceRequest(sessionToken:Int) {
        let params: JSONObject = [
            "addressNumber": 0,
            "cacheBytes":"",
            "sessionToken": sessionToken
        ]
        
        dataSource.requestObject(forEndpoint: Endpoint.WalletServiceEndpoint.getWalletBalance,  parameters: params) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let json):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(json)
                    }
                    do {
                        let model = try Mapper<MyWalletEntity>.map(object: json)
                        self.presenter?.requestCompleted(data: model)
                    } catch (let error) {
                        self.presenter?.requestFailed(message: error.localizedDescription)
                    }

                case .failure(let error):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(error.localizedDescription)
                    }
                    self.presenter?.requestFailed(message: error.localizedDescription)
                }
            }
        }
    }
    
    func getBonusBountyBalance() {
        if let bonus = repository.bountyBonus {
            self.presenter?.bountyBalanceResponse(isClaimed: true, balance: "\(bonus.lockedGenesisBounty)")
        }
        
        repository.bonusBounty { result in
            DispatchQueue.main.async {
                switch result {
                case .success:
                    if let bonus = self.repository.bountyBonus {
                        self.presenter?.bountyBalanceResponse(isClaimed: true, balance: "\(bonus.lockedGenesisBounty)")
                    } else {
                        self.presenter?.bountyBalanceResponse(isClaimed: false, balance: "")
                    }
                case .failure:
                    self.presenter?.bountyBalanceResponse(isClaimed: false, balance: "")
                }
            }
        }
    }
    
    func createSessionRequest() {
        let params: JSONObject = [
            "cacheBytes": "",
            "passphrase": WalletDataManager.shared.passphrase ?? "",
            "walletBytes": WalletDataManager.shared.wallet ?? ""
        ]
        dataSource.requestObject(forEndpoint: Endpoint.WalletServiceEndpoint.createSession,  parameters: params) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let json):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(json)
                    }
                    if let sessionToken = json["sessionToken"] as? String {
                        self.getWalletBalanceRequest(sessionToken: Int(sessionToken) ?? 0)
                    } else if let sessionToken = json["sessionToken"] as? Int {
                        self.getWalletBalanceRequest(sessionToken: sessionToken)
                    } else {
                        self.presenter?.requestFailed(message: "Session Token parsing error")
                    }
                case .failure(let error):
                    self.presenter?.requestFailed(message: error.localizedDescription)
                }
            }
        }
    }
    
    func apiRequestToGetBizVaultStatus() {
        dataSource.requestObject(forEndpoint: Endpoint.BizVaultEndpoint.checkBizVault) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let json):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(json)
                    }
                    
                    do {
                        let model = try Mapper<BizVaultResponse>.map(object: json)
                        self.presenter?.bizVaultStatus(isActivated: model.details.bizvaultIdStatus)
                    } catch (let error) {
                        self.presenter?.bizVaultStatus(isActivated: false)
                        self.presenter?.requestFailed(message: error.localizedDescription)
                    }
                    
                case .failure(let error):
                    self.presenter?.requestFailed(message: error.localizedDescription)
                    self.presenter?.bizVaultStatus(isActivated: false)
                }
            }
        }
    }
}
