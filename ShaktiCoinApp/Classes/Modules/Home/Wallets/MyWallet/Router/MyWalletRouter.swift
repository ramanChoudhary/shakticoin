//
//  MyWalletRouter.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 20/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit


class MyWalletRouter: MyWalletRouterProtocol {
    static func createMyWalletModule() -> UIViewController {
        let navController = UIStoryboard.rootController(identifier: STORYBOARD_ID.walletViewController)
        //let wallet = navController?.viewControllers.first
        if let view = navController as? WalletViewController {
            let presenter:MyWalletPresenterProtocol = MyWalletPresenter()
            let router:MyWalletRouterProtocol = MyWalletRouter()
            let interactor:MyWalletInteractorProtocol = MyWalletInteractor()
            view.presenter = presenter
            presenter.view = view
            presenter.router = router
            presenter.interactor = interactor
            interactor.presenter = presenter
            return view
        }
        
        return UIViewController()
    }
    
    func presentCreateWalletView(from view: MyWalletViewProtocol?) {
        if let view = view as? WalletViewController {
            let controller = CreateWalletRouter.createNewWalletModule()
            controller.modalPresentationStyle = .overCurrentContext
            view.navigationController?.present(controller, animated: true, completion: nil)
        }
    }
    
    func presentPayPopup(from view: MyWalletViewProtocol?, payOptions: PayCoinType) {
        if let view = view as? WalletViewController {
            let controller = PayCoinsRouter.createPayCoinsModule(payOptions: payOptions)
            controller.modalPresentationStyle = .overCurrentContext
            view.navigationController?.present(controller, animated: true, completion: nil)
        }
    }
    
    func presentBalanceHistory(from view: MyWalletViewProtocol?) {
        if let view = view as? WalletViewController {
            let vc = BalanceHistoryRouter.createBalanceHistoryModule()
            view.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func presentAdminScreen(from view: MyWalletViewProtocol?) {
        if let view = view as? WalletViewController {
            let vc = AdminRouter.createAdminModule()
            view.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func presentBusinessVault(from view: MyWalletViewProtocol?) {
       // if let view = view as? WalletViewController {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "BusinessVaultIntroViewController") as! BusinessVaultIntroViewController
            let navigationController = UINavigationController(rootViewController: vc)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = navigationController
        //}
    }
    
    func presentMyReferralScreen(from view: MyWalletViewProtocol?) {
        if let view = view as? WalletViewController {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let vc = storyboard.instantiateViewController(withIdentifier: "MyReferralsViewController") as! MyReferralsViewController
//            let navigationController = UINavigationController(rootViewController: vc)
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            appDelegate.window?.rootViewController = navigationController
            
//            let referralRegistrationVC: ReferralRegistrationViewController = ReferralRegistrationViewController()
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            appDelegate.window?.rootViewController = referralRegistrationVC
            
           // if let view = view as? WalletViewController {
                let controller = RegisterYourReferralRouter.createRegisterYourReferralModule()
                controller.modalPresentationStyle = .overFullScreen
                view.navigationController?.present(controller, animated: true, completion: nil)
           // }
        }
    }
    
    func presentMinerScreen(from view: MyWalletViewProtocol?) {
        AppDelegate.shared?.perform(.minerIntro)
    }
    
    func presentClaimBountyBonusScreen(from view: MyWalletViewProtocol?) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = ChooseBountyWireframe.createModule()
    }
    
}
