//
//  WalletViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 10/29/18.
//  Copyright © 2018 Garnik Giloyan. All rights reserved.
//

import UIKit

class WalletViewController: BaseScrollViewController {
 
   // MARK: - Declarations
    var presenter: MyWalletPresenterProtocol?
    
    lazy var currentBalanceLbl : UILabel = {
        let label = LabelPrototypes.Caption
        label.text = "CurrentBalance".localized
        return label
    }()

    lazy var balanceValue : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = ""
        label.font = UIFont.lato(fontSize: 36, type: .Regular) //TODO Medium
        return label
    }()

    lazy var balanceHistory : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "BalanceHistory".localized
        label.font = UIFont.lato(fontSize: 12, type: .Bold)
        label.underline()
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(gotoBalanceHistory))
        label.addGestureRecognizer(tap)
        return label
    }()

    lazy var adminImageContainer : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        view.alpha = 0.70
        return view
    }()
    
    lazy var adminImageButton : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "WalletAdmin")
        iv.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(gotoAdmin))
        iv.addGestureRecognizer(tap)
        return iv
    }()
    
    lazy var adminLabel : UILabel = {
        let label = UILabel()
        label.text = "Admin".localized
        label.textColor = .white
        label.font = UIFont.futura(fontSize: 9, type: .Medium)
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(gotoAdmin))
        label.addGestureRecognizer(tap)
        return label
    }()
    
//    lazy var kycViewContainer : UIView = {
//        let view = UIView()
//        view.backgroundColor = .clear
//        return view
//    }()
//
//    lazy var kycLockImage : UIImageView = {
//        let imageView = UIImageView()
//        imageView.image = UIImage(named: "Lock Icon Small")
//        imageView.tintColor = UIColor.mainColor()
//        return imageView
//    }()
//
//    lazy var kycVerificationLink : SCAttributedTextView = {
//        let textView = SCAttributedTextView()
//
//        textView.delegate = self
//
//        let textContent: String = "_TTKYCVerification".localized
//        textView.setText(textContent: textContent, textColor: UIColor.white, font: UIFont.lato(fontSize: 14))
//        textView.setLink(key: "_unlockKyc_Link", linkedText: "_unlockKyc_Link".localized)
//        textView.setLinkStyles(linkColor: UIColor.white, underlineColor: UIColor.white)
//        textView.finilize()
//
//        return textView
//    }()
    
    lazy var buttonsContainer: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.5)
        return view
    }()
    
    lazy var buttonsStackContainer: UIStackView = {
        let stackView = UIStackView()
        stackView.backgroundColor = .clear
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.alignment = .center
        stackView.spacing = 15;
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
        
    }()

    
    lazy var btnRecieveSxe : UIButton = {
        let button = GradientButton()
        button.setTitle("Receive SXE", for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 12, type: .Medium)
        button.titleLabel?.textColor = .white
        button.backgroundColor = .clear
        button.setGradientColors([ColorStyle.darkGold.cgColor, ColorStyle.middleGold.cgColor, ColorStyle.lightGold.cgColor])
        button.addTarget(self, action: #selector(receiveWithWallet), for: .touchUpInside)
        return button
    }()
    
    lazy var btnPayWithSxe : UIButton = {
        let button = GradientButton()
        button.setTitle("Pay/Receive SXE", for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 12, type: .Medium)
        button.titleLabel?.textColor = .white
        button.setGradientColors([ColorStyle.darkGold.cgColor, ColorStyle.middleGold.cgColor, ColorStyle.lightGold.cgColor])
        button.backgroundColor = UIColor.mainColor()
        button.addTarget(self, action: #selector(payWithWallet), for: .touchUpInside)
        return button
    }()
    
    lazy var btnPayWithBizVault : UIButton = {
        let button = GradientButton()
        button.setTitle("Pay/Receive Biz Vault", for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 12, type: .Medium)
        button.titleLabel?.textColor = .white
        button.setGradientColors([ColorStyle.darkGold.cgColor, ColorStyle.middleGold.cgColor, ColorStyle.lightGold.cgColor])
        button.backgroundColor = UIColor.mainColor()
        button.addTarget(self, action: #selector(payWithBizVault), for: .touchUpInside)
        return button
    }()
    
    
    lazy var rfViewContainer : UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    lazy var rfLockImage : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "Lock Icon Small")
        imageView.tintColor = UIColor.mainColor()
        return imageView
    }()
    
    lazy var rfReferralLink : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        
        textView.delegate = self
        
        let textContent = "_TTUnlockIt".localized
        textView.setup(textInfo: textContent, textColor: UIColor.white, font: UIFont.lato(fontSize: 14))
        textView.setLink(key: "_referrFriends_Link", linkedText: "_referrFriends_Link".localized)
        textView.setLinkStyles(linkColor: UIColor.white, underlineColor: UIColor.white)
        textView.finilize()

        return textView
    }()
    
    
    lazy var bonusBountyStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.backgroundColor = .clear
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.spacing = 15;
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    
    lazy var bonusBountyContainer : UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    lazy var bonusBountyLockImage : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "Lock Icon Small")
        imageView.tintColor = UIColor.mainColor()
        return imageView
    }()
    
    lazy var bonusBountyReferralLink : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        
        textView.delegate = self
        
        let textContent = "_BBUnlockIt".localized
        textView.setup(textInfo: textContent, textColor: UIColor.white, font: UIFont.lato(fontSize: 14))
        textView.setLink(key: "_bonusBounty_Link", linkedText: "_bonusBounty_Link".localized)
        textView.setLinkStyles(linkColor: UIColor.white, underlineColor: UIColor.white)
        textView.finilize()

        return textView
    }()
    
    lazy var bonusBountyBalanceView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    
    lazy var bonusBBlbl : UILabel = {
        let label = LabelPrototypes.Caption
        label.text = "BonusBountyBalance".localized
        return label
    }()
    
    lazy var bonusBountyBalance : UILabel = {
        let label = LabelPrototypes.Caption24
        label.text = "SXE 1008.00"
        return label
    }()
    
    lazy var minerView: SC2LayerView = {
        let view = SC2LayerView()
        view.backgroundColor = .clear
        return view
    }()
    
    lazy var businessVault: SC2LayerView = {
        let view = SC2LayerView()
        view.backgroundColor = .clear
        return view
    }()
    
    lazy var becomeMiner : UILabel = {
        let label = LabelPrototypes.Caption18
        label.text = "BecomeMiner".localized
        return label
    }()
    
    lazy var minerImageView : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "Miner")
        imageView.tintColor = UIColor.mainColor()
        return imageView
    }()
    
    lazy var minerTextInfo : SCAttributedTextView = {
        let textView = SCAttributedTextView()
        textView.delegate = self
        let textContent = "_TTGetMore".localized
        textView.setup(textInfo: textContent, textColor: UIColor.white, font: UIFont.lato(fontSize: 10))
        textView.setLink(key: "_basicMiner_Link", linkedText: "_basicMiner_Link".localized)
        textView.setLinkStyles(linkColor: .mainColor(), underlineColor: .mainColor())
        textView.finilize()
        return textView
    }()
   
    lazy var gainBusinessLbl : UILabel = {
        let label = LabelPrototypes.CaptionLight16
        label.text = "GainBusiness".localized
        return label
    }()
    
    lazy var businessVaultLbl : UILabel = {
        let label = UILabel()
        
        let attributedText = NSMutableAttributedString(string: "AddA".localized, attributes: [NSAttributedString.Key.font: UIFont.lato(fontSize: 16, type: .Light), NSAttributedString.Key.foregroundColor: UIColor.white])
        
        attributedText.append(NSAttributedString(string: "BusinessVault".localized, attributes: [NSAttributedString.Key.font: UIFont.lato(fontSize: 18, type: .Regular), NSAttributedString.Key.foregroundColor: UIColor.white])) //TODO Medium
        
        label.attributedText = attributedText
        
        return label
    }()
    
    lazy var businessVaultImageView : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "Vault")
        imageView.tintColor = UIColor.mainColor()
        return imageView
    }()
    
    lazy var businessVaultTextInfo : SCAttributedTextView = {
        
        let textView = SCAttributedTextView()
        textView.delegate = self
        
        let textContent: String = "_TTBVTextInfoBecome".localized
        textView.setup(textInfo: textContent, textColor: UIColor.white, font: UIFont.lato(fontSize: 10))
        textView.setText(textContent: "_TTBVTextInfoYour".localized, textColor: UIColor.white, font: UIFont.lato(fontSize: 10))
        textView.setLink(key: "_businessVault_Link", linkedText: "_businessVault_Link".localized)
        textView.setLinkStyles(linkColor: UIColor.mainColor(), underlineColor: UIColor.mainColor())
        textView.finilize()
        
        return textView
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        initNavBar()
        initViews()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.bonusBountyBalanceView.isHidden = true
        self.btnRecieveSxe.isHidden = true
        self.btnPayWithBizVault.isHidden = true
//        if WalletDataManager.shared.wallet == nil {
//            loadBalance()
//        } else {
            presenter?.viewDidLoad()
        //}
    }
    
    override func viewDidLayoutSubviews() {
        btnRecieveSxe.layer.cornerRadius = 18
        btnRecieveSxe.layer.borderWidth = 1
        btnRecieveSxe.layer.borderColor = UIColor.mainColor().cgColor
        btnPayWithSxe.layer.cornerRadius = 18
        btnPayWithBizVault.layer.cornerRadius = 18
    }

    // MARK: - Methods
    private func initViews() {
        
        coverView.addSubview(currentBalanceLbl)
        coverView.addSubview(balanceValue)
        containerView.addSubview(balanceHistory)
        containerView.addSubview(adminImageContainer)
        containerView.addSubview(adminLabel)
        containerView.addSubview(buttonsContainer)
        //containerView.addSubview(kycViewContainer)
        containerView.addSubview(bonusBountyStackView)
        containerView.addSubview(bonusBountyBalanceView)
        containerView.addSubview(bonusBountyContainer)
        containerView.addSubview(rfViewContainer)
        coverView.addSubview(bonusBBlbl)
        coverView.addSubview(bonusBountyBalance)
        containerView.addSubview(minerView)
        containerView.addSubview(businessVault)
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        _ = currentBalanceLbl.anchor(coverView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: topBarHeight+30, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = currentBalanceLbl.centralizeX(coverView.centerXAnchor)
        
        _ = balanceValue.anchor(currentBalanceLbl.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = balanceValue.centralizeX(coverView.centerXAnchor)
        
        _ = balanceHistory.anchor(balanceValue.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = balanceHistory.centralizeX(coverView.centerXAnchor)
        
        _ = adminImageContainer.anchor(balanceValue.bottomAnchor, left: nil, bottom: nil, right: coverView.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 20, widthConstant: 32, heightConstant: 32)
        
        _ = adminLabel.anchor(adminImageContainer.bottomAnchor, left: nil, bottom: nil, right: coverView.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 0)
        
        adminImageContainer.layer.cornerRadius = 32/2

        adminImageContainer.addSubview(adminImageButton)
        _ = adminImageButton.centralizeX(adminImageContainer.centerXAnchor)
        _ = adminImageButton.centralizeY(adminImageContainer.centerYAnchor)
        
        // 2LawyerViews
        
        /*let kycViewAlpha = UIView()
        kycViewContainer.addSubview(kycViewAlpha)
        
        kycViewAlpha.backgroundColor = .black
        kycViewAlpha.alpha = 0.50
        
        _ = kycViewAlpha.anchor(kycViewContainer.topAnchor, left: kycViewContainer.leftAnchor, bottom: kycViewContainer.bottomAnchor, right: kycViewContainer.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        kycViewContainer.addSubview(kycLockImage)
        kycViewContainer.addSubview(kycVerificationLink)*/
        
        _ = bonusBountyStackView.anchor(buttonsContainer.bottomAnchor, left: coverView.leftAnchor, bottom: rfViewContainer.topAnchor, right: coverView.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: 0, heightConstant: 0)
               
               bonusBountyStackView.addArrangedSubview(bonusBountyBalanceView)
               bonusBountyStackView.addArrangedSubview(bonusBountyContainer)
               
               bonusBountyStackView.translatesAutoresizingMaskIntoConstraints = false;
               
               bonusBountyBalanceView.addSubview(bonusBBlbl)
               bonusBountyBalanceView.addSubview(bonusBountyBalance)
        
        let bonusBountyAlpha = UIView()
        bonusBountyContainer.addSubview(bonusBountyAlpha)
        bonusBountyAlpha.backgroundColor = .black
        bonusBountyAlpha.alpha = 0.50
        _ = bonusBountyAlpha.anchor(bonusBountyContainer.topAnchor, left: bonusBountyContainer.leftAnchor, bottom: bonusBountyContainer.bottomAnchor, right: bonusBountyContainer.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        bonusBountyContainer.addSubview(bonusBountyLockImage)
        bonusBountyContainer.addSubview(bonusBountyReferralLink)
        
        _ = bonusBountyContainer.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 45)
        
        _ = bonusBountyLockImage.anchor(nil, left: bonusBountyContainer.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = bonusBountyLockImage.centralizeY(bonusBountyContainer.centerYAnchor)
        
        _ = bonusBountyReferralLink.anchor(nil, left: bonusBountyLockImage.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 300, heightConstant: 20)
        _ = bonusBountyReferralLink.centralizeY(bonusBountyContainer.centerYAnchor)
        
        
        _ = bonusBountyBalanceView.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 45)
        
        _ = bonusBBlbl.anchor(nil, left: bonusBountyBalanceView.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 45, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = bonusBBlbl.centralizeY(bonusBountyBalanceView.centerYAnchor)
        
        _ = bonusBountyBalance.anchor(nil, left: bonusBBlbl.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = bonusBountyBalance.centralizeY(bonusBountyBalanceView.centerYAnchor)
        
        
        
        let rfViewAlpha = UIView()
        rfViewContainer.addSubview(rfViewAlpha)
        
        rfViewAlpha.backgroundColor = .black
        rfViewAlpha.alpha = 0.50
        
        _ = rfViewAlpha.anchor(rfViewContainer.topAnchor, left: rfViewContainer.leftAnchor, bottom: rfViewContainer.bottomAnchor, right: rfViewContainer.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        rfViewContainer.addSubview(rfLockImage)
        rfViewContainer.addSubview(rfReferralLink)
        
       /* _ = kycViewContainer.anchor(balanceHistory.bottomAnchor, left: coverView.leftAnchor, bottom: nil, right: coverView.rightAnchor, topConstant: 30, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 45)
        
        _ = kycLockImage.anchor(nil, left: kycViewContainer.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = kycLockImage.centralizeY(kycViewContainer.centerYAnchor)
        
        _ = kycVerificationLink.anchor(nil, left: kycLockImage.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 300, heightConstant: 20)
        _ = kycVerificationLink.centralizeY(kycViewContainer.centerYAnchor)*/
        
        
        
        
       _ = buttonsContainer.anchor(balanceHistory.bottomAnchor, left: coverView.leftAnchor, bottom: nil, right: coverView.rightAnchor, topConstant: 30, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 45)
               
        buttonsContainer.addSubview(buttonsStackContainer)
        
        _ = buttonsStackContainer.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 45)
        buttonsStackContainer.centralizeX(self.buttonsContainer.centerXAnchor)
        buttonsStackContainer.centralizeY(self.buttonsContainer.centerYAnchor)
              
        
        // buttonsContainer.addSubview(btnRecieveSxe)
             //  buttonsContainer.addSubview(btnPayWithSxe)
        
        
        buttonsStackContainer.addArrangedSubview(btnPayWithSxe)
        buttonsStackContainer.addArrangedSubview(btnRecieveSxe)
        buttonsStackContainer.addArrangedSubview(btnPayWithBizVault)
        buttonsStackContainer.translatesAutoresizingMaskIntoConstraints = false;
        
        _ = btnRecieveSxe.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 125, heightConstant: 36)
        _ = btnPayWithSxe.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 125, heightConstant: 36)
        _ = btnPayWithBizVault.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 125, heightConstant: 36)
               
//               _ = btnRecieveSxe.anchor(nil, left: buttonsContainer.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 60, bottomConstant: 0, rightConstant: 0, widthConstant: 125, heightConstant: 36)
//               _ = btnRecieveSxe.centralizeY(buttonsContainer.centerYAnchor)
//
//               _ = btnPayWithSxe.anchor(nil, left: nil, bottom: nil, right: buttonsContainer.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 60, widthConstant: 125, heightConstant: 36)
//               _ = btnPayWithSxe.centralizeY(buttonsContainer.centerYAnchor)
        
        
        
        
        
        _ = rfViewContainer.anchor(bonusBountyStackView.bottomAnchor, left: coverView.leftAnchor, bottom: nil, right: coverView.rightAnchor, topConstant: 30, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 45)
        
        _ = rfLockImage.anchor(nil, left: rfViewContainer.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = rfLockImage.centralizeY(rfViewContainer.centerYAnchor)
        
        _ = rfReferralLink.anchor(nil, left: rfLockImage.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 300, heightConstant: 20)
        _ = rfReferralLink.centralizeY(rfViewContainer.centerYAnchor)
        
        // End 2LawyerViews ...
        
        _ = minerView.anchor(rfViewContainer.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 335, heightConstant: 100)
        _ = minerView.centralizeX(coverView.centerXAnchor)
        
        _ = businessVault.anchor(minerView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 18, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 335, heightConstant: 125)
        _ = businessVault.centralizeX(coverView.centerXAnchor)
        
        businessVault.transparency = 0.40
        
        minerView.addSubview(becomeMiner)
        minerView.addSubview(minerImageView)
        minerView.addSubview(minerTextInfo)
        
        _ = becomeMiner.anchor(minerView.topAnchor, left: minerView.leftAnchor, bottom: nil, right: nil, topConstant: 20, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = minerImageView.anchor(minerView.topAnchor, left: becomeMiner.rightAnchor, bottom: nil, right: nil, topConstant: 15, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = minerTextInfo.anchor(becomeMiner.bottomAnchor, left: minerView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 315, heightConstant: 40)
        
        businessVault.addSubview(gainBusinessLbl)
        businessVault.addSubview(businessVaultLbl)
        businessVault.addSubview(businessVaultImageView)
        businessVault.addSubview(businessVaultTextInfo)
        
        _ = gainBusinessLbl.anchor(businessVault.topAnchor, left: businessVault.leftAnchor, bottom: nil, right: nil, topConstant: 20, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = businessVaultLbl.anchor(gainBusinessLbl.bottomAnchor, left: businessVault.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = businessVaultImageView.anchor(gainBusinessLbl.bottomAnchor, left: businessVaultLbl.rightAnchor, bottom: nil, right: nil, topConstant: 7, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = businessVaultTextInfo.anchor(businessVaultLbl.bottomAnchor, left: businessVault.leftAnchor, bottom: nil, right: nil, topConstant: 12, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 300, heightConstant: 30)
    }
    
    private func showPopUp(popUp: PopUp) {
        let popUpVC = UIStoryboard(name: "Main", bundle:
            nil).instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
        self.addChild(popUpVC)

        switch popUp {
        case PopUp.BasicMiner:
            popUpForBasicMiner(vc: popUpVC)
        case PopUp.BusinessVault:
            popUpForBusinessVault(vc: popUpVC)
        case PopUp.IndividualWallet:
            popUpForReferrals(vc: popUpVC)
            break
        }
        
        popUpVC.view.frame = self.view.frame
        self.view.addSubview(popUpVC.view)
        popUpVC.didMove(toParent: self)
        
        switch popUp {
        case PopUp.BasicMiner:
            popUpVC.setupViews(value: 200)
        case PopUp.BusinessVault:
            popUpVC.setupViews(value: 200)
        case PopUp.IndividualWallet:
            popUpVC.setupViews(value: 200)
        }
    }
    
    private func popUpForReferrals(vc: PopUpViewController) {
        
        vc.iconImageView.tintColor = .mainColor()
        vc.iconImageView.image = UIImage(named: "Wallet")
        vc.titleTextView.setup(textInfo: "_WMODAL_Grab".localized, textColor: UIColor.darkGray, font: UIFont.lato(fontSize: 18, type: .Light))
        vc.titleTextView.setText(textContent: "_WMODAL_IndividualWallet".localized, textColor: UIColor(red:0.69, green:0.61, blue:0.46, alpha:1), font: .futura(fontSize: 18, type: .Medium))
        vc.titleTextView.finilize()
        vc.contentTextView.setup(textInfo: "_WMODAL_TheShaktiSystem".localized, textColor: .darkGray, font: UIFont.lato(fontSize: 14))
        vc.contentTextView.setText(textContent: "_WMODAL_HowFastYouUnlock".localized, textColor: .darkGray, font: .lato(fontSize: 14))
        vc.contentTextView.setText(textContent: "_WMODAL_YouCanAlso".localized, textColor: .darkGray, font: UIFont.lato(fontSize: 14))
        vc.contentTextView.finilize()
        vc.submitButton.setTitle("_WMODAL_GoToRef".localized, for: .normal)
        vc.submitButton.addTarget(self, action: #selector(onGoToMyRefferals), for: .touchUpInside)
    }
    
    private func popUpForBasicMiner(vc: PopUpViewController) {
        
        vc.titleTextView.setup(textInfo: "_WMODAL_Become".localized, textColor: UIColor.darkGray, font: UIFont.lato(fontSize: 18, type: .Light))
        vc.titleTextView.setText(textContent: "_WMODAL_M101".localized, textColor: UIColor(red:0.69, green:0.61, blue:0.46, alpha:1), font: .futura(fontSize: 18, type: .Medium))
        vc.titleTextView.finilize()
        
        vc.contentTextView.setup(textInfo: "_WMODAL_Earn".localized, textColor: .darkGray, font: UIFont.lato(fontSize: 14, type: .Bold))
        vc.contentTextView.setText(textContent: "_WMODAL_MinerLicense".localized, textColor: .darkGray, font: .lato(fontSize: 14))
        vc.contentTextView.setText(textContent: "_WMODAL_YouCan".localized, textColor: .darkGray, font: UIFont.lato(fontSize: 14))
        vc.contentTextView.setText(textContent: "_WMODAL_SXECoins".localized, textColor: .darkGray, font: .lato(fontSize: 14, type: .Bold))
        vc.contentTextView.setText(textContent: "_WMODAL_DontKnow".localized, textColor: .darkGray, font: UIFont.lato(fontSize: 14, type: .Bold))
        vc.contentTextView.setText(textContent: "_WMODAL_DontWorry".localized, textColor: .darkGray, font: UIFont.lato(fontSize: 14))
        vc.contentTextView.setText(textContent: "_WMODAL_GetAnExtra".localized, textColor: .darkGray, font: UIFont.lato(fontSize: 14, type: .Bold))
        vc.contentTextView.finilize()
        
        vc.submitButton.setTitle("_WMODAL_LearnAbout101".localized, for: .normal)
        vc.submitButton.addTarget(self, action: #selector(onLearnMoreAbout101), for: .touchUpInside)
    }
    
    private func popUpForBusinessVault(vc: PopUpViewController) {
        
        vc.iconImageView.tintColor = .mainColor()
        vc.iconImageView.image = UIImage(named: "Vault")
        vc.titleTextView.setup(textInfo: "_WMODAL_Get".localized, textColor: UIColor.darkGray, font: UIFont.lato(fontSize: 18, type: .Light))
        vc.titleTextView.setText(textContent: "BusinessVault".localized, textColor: UIColor(red:0.69, green:0.61, blue:0.46, alpha:1), font: .futura(fontSize: 18, type: .Medium))
        vc.titleTextView.setText(textContent: "_WMODAL_Adv".localized, textColor: UIColor.darkGray, font: UIFont.lato(fontSize: 18, type: .Light))
        vc.titleTextView.finilize()
        
        vc.contentTextView.setup(textInfo: "_WMODAL_ASLeader".localized, textColor: .darkGray, font: UIFont.lato(fontSize: 14, type: .Bold))
        vc.contentTextView.setText(textContent: "_WMODAL_IfYou".localized, textColor: .darkGray, font: .lato(fontSize: 14))
        vc.contentTextView.setText(textContent: "_WMODAL_Receive".localized, textColor: .darkGray, font: UIFont.lato(fontSize: 14, type: .Bold))
        vc.contentTextView.setText(textContent: "_WMODAL_GetFrom".localized, textColor: .darkGray, font: UIFont.lato(fontSize: 14))
        vc.contentTextView.setText(textContent: "_WMODAL_SignUp".localized, textColor: .darkGray, font: UIFont.lato(fontSize: 14))
        vc.contentTextView.setText(textContent: "_WMODAL_ThisOffer".localized, textColor: .darkGray, font: UIFont.lato(fontSize: 14))
        vc.contentTextView.finilize()
        
        vc.submitButton.setTitle("_WMODAL_Enter".localized, for: .normal)
        vc.submitButton.addTarget(self, action: #selector(onEnterTheShaktiBusiness), for: .touchUpInside)
    }
    
    // MARK: - Navigation Bar

    private func initNavBar() {
        self.navigationItem.title = "MyWallet".localized
    }

    // MARK: - Events
    
    @objc private func gotoBalanceHistory() {
        self.presenter?.showBalanceHistory()
    }
    
    @objc private func gotoAdmin() {
        self.presenter?.showAdmin()
    }

    @objc private func onEnterTheShaktiBusiness() {
        self.presenter?.showBusinessVault()
    }
    
    @objc private func onGoToMyRefferals() {
        self.presenter?.showMyReferrals()
    }
    
    @objc private func onLearnMoreAbout101() {
        self.presenter?.showMiner()
    }
    
    @objc private func payWithWallet() {
        self.presenter?.payPopup(payWith: .pay)
    }
    
    @objc private func receiveWithWallet() {
        self.presenter?.payPopup(payWith: .receive)
    }
    
    @objc private func payWithBizVault() {
        self.presenter?.payPopup(payWith: .bizVault)
    }
    
    // MARK: - ViewController
    func loadBalance() {
        self.presenter?.presentCreateWallet()
    }
}

extension WalletViewController:MyWalletViewProtocol {
    func showData(data: MyWalletEntity) {
        WalletDataManager.shared.set(walletBalance: data.walletBalance)
        self.balanceValue.text = "SXE \(data.walletBalance)"
    }
    
    func showErrorMessage(message: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func bountyBalance(isClaimed: Bool, balance: String) {
        if isClaimed == true {
            self.bonusBountyBalanceView.isHidden = false
            self.bonusBountyContainer.isHidden = true
            self.bonusBountyBalance.text = "SXE \(balance)"
        } else {
            self.bonusBountyBalanceView.isHidden = true
            self.bonusBountyContainer.isHidden = false
        }
    }
    
    func promptForPassphrase() {
        let ac = UIAlertController(title: "Enter your passphrase", message: nil, preferredStyle: .alert)
        ac.addTextField { (textField) in
            textField.isSecureTextEntry = true
        }

        let submitAction = UIAlertAction(title: "Submit", style: .default) { [unowned ac] _ in
            let answer = ac.textFields![0]
            if let pass = answer.text, pass.count > 0 {
                WalletDataManager.shared.set(passphrase: pass)
                self.presenter?.viewDidLoad()
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.promptForPassphrase()
                }
            }
            // do something interesting with "answer" here
        }

        ac.addAction(submitAction)

        present(ac, animated: true)
    }
    
    func populate(_ state: MyWalletViewState) {
        switch state {
        case .loaded:
            hideLoader()
        case .loading:
            showLoader()
        }
    }
    
    func updateUIForBizVault(status: Bool) {
        self.btnPayWithBizVault.isHidden = !status
        self.btnRecieveSxe.isHidden = status
        self.btnPayWithSxe.setTitle(status == true ? "Pay/Receive SXE" : "Pay SXE", for: .normal)
    }
}

extension WalletViewController: UITextViewDelegate {
    // MARK: - Delegates
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        
        if URL.absoluteString == "_basicMiner_Link" {
            showPopUp(popUp: .BasicMiner)
        } else if URL.absoluteString == "_businessVault_Link" {
            showPopUp(popUp: .BusinessVault)
        } else if URL.absoluteString == "_unlockKyc_Link" {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "KycValidationViewController") as! KycValidationViewController
            let navigationController = UINavigationController(rootViewController: vc)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = navigationController
            
        } else if URL.absoluteString == "_referrFriends_Link" {
            self.presenter?.showMyReferrals()
            //showPopUp(popUp: .IndividualWallet)
        } else if URL.absoluteString == "_bonusBounty_Link" {
            self.presenter?.showClaimBountyBonus()
        }
        
        return false
    }
}
