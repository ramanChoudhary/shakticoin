//
//  MyWalletProtocols.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 20/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation
import UIKit

enum MyWalletViewState {
    case loaded
    case loading
}

protocol MyWalletViewProtocol: class {
    var presenter:MyWalletPresenterProtocol? {get set}
    func showData(data:MyWalletEntity)
    func showErrorMessage(message:String)
    func promptForPassphrase()
    func bountyBalance(isClaimed:Bool, balance:String)
    func populate(_ state: MyWalletViewState)
    func updateUIForBizVault(status:Bool)
}

protocol MyWalletPresenterProtocol: class {
    var view:MyWalletViewProtocol? {get set}
    var router:MyWalletRouterProtocol? {get set}
    var interactor:MyWalletInteractorProtocol? {get set}
    
    //View =========> Presenter
    func viewDidLoad()
    func presentCreateWallet()
    func showBalanceHistory()
    func showAdmin()
    func showBusinessVault()
    func showMyReferrals()
    func showMiner()
    func payPopup(payWith:PayCoinType)
    func showClaimBountyBonus()
    
    //Interactor ========== Presenter
    func requestCompleted(data:MyWalletEntity)
    func requestFailed(message:String)
    func bountyBalanceResponse(isClaimed:Bool, balance:String)
    func bizVaultStatus(isActivated:Bool)
    func walletBytesReceived(walletIdResponse:WalletIdResponse)
}

protocol MyWalletInteractorProtocol: class {
    var presenter:MyWalletPresenterProtocol? {get set}
    
    func getWalletRequest()
    func createSessionRequest()
    func getBonusBountyBalance()
    func apiRequestToGetBizVaultStatus()
}

protocol MyWalletRouterProtocol: class {
    static func createMyWalletModule() -> UIViewController
    //Presenter -> Router
    func presentCreateWalletView(from view:MyWalletViewProtocol?)
    func presentBalanceHistory(from view:MyWalletViewProtocol?)
    func presentAdminScreen(from view:MyWalletViewProtocol?)
    func presentBusinessVault(from view:MyWalletViewProtocol?)
    func presentMyReferralScreen(from view:MyWalletViewProtocol?)
    func presentMinerScreen(from view:MyWalletViewProtocol?)
    func presentPayPopup(from view:MyWalletViewProtocol?, payOptions:PayCoinType)
    func presentClaimBountyBonusScreen(from view:MyWalletViewProtocol?)
}

