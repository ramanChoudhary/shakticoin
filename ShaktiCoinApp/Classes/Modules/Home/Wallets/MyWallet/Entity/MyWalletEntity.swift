//
//  MyWalletEntity.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 08/09/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

struct MyWalletEntity : MapRepresentable {
    var message: String
    var walletBalance:String
    
    init(map: Map) throws {
        message = try map.get("message")
        walletBalance = try map.get("walletBalance")
    }
}

struct WalletIdResponse : MapRepresentable {
    var shaktiID: String?
    var accountType: String?
    var walletBytes: String?
    var walletID:String?
    var active: Bool
    
    init(map: Map) throws {
        shaktiID = try map.get("shaktiID")
        accountType = try map.get("accountType")
        walletBytes = try map.get("walletBytes")
        walletID = try map.get("walletID")
        active = try map.get("active")
    }
}

struct BizVaultResponse : MapRepresentable {
    var status: Int
    var message: String
    var details:BizVaultStatus
    
    init(map: Map) throws {
        status = try map.get("status")
        message = try map.get("message")
        details = try map.get("details")
    }
}

struct BizVaultStatus : MapRepresentable {
    var message: String
    var bizvaultIdStatus:Bool
    
    init(map: Map) throws {
        message = try map.get("message")
        bizvaultIdStatus = try map.get("bizvaultIdStatus")
    }
}
