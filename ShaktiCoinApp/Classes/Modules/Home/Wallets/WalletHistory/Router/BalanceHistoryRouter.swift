//
//  BalanceHistoryRouter.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 28/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

class BalanceHistoryRouter: BalanceHistoryRouterProtocol {
    
    static func createBalanceHistoryModule() -> UIViewController {
        let view = BalanceHistoryController()
        let presenter:BalanceHistoryPresenterProtocol = BalanceHistoryPresenter()
        let router:BalanceHistoryRouterProtocol = BalanceHistoryRouter()
        let interactor:BalanceHistoryInteractorProtocol = BalanceHistoryInteractor()
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        return view
    }
    
    func presentAdminScreen(from view: BalanceHistoryViewProtocol?) {
        if let view = view as? BalanceHistoryController {
            let vc = AdminRouter.createAdminModule()
            view.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func popToWalletScreen(from view: BalanceHistoryViewProtocol?) {
        if let view = view as? BalanceHistoryController {
            view.navigationController?.popViewController(animated: true)
        }
    }
    
    func presentPayPopup(from view: BalanceHistoryViewProtocol?, payOptions: PayCoinType) {
        if let view = view as? BalanceHistoryController {
            let controller = PayCoinsRouter.createPayCoinsModule(payOptions: payOptions)
            controller.modalPresentationStyle = .overCurrentContext
            view.navigationController?.present(controller, animated: true, completion: nil)
        }
    }
}
