//
//  BalanceHistoryPresenter.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 28/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation


class BalanceHistoryPresenter: BalanceHistoryPresenterProtocol {
    var view: BalanceHistoryViewProtocol?
    
    var router: BalanceHistoryRouterProtocol?
    
    var interactor: BalanceHistoryInteractorProtocol?
    
    var transactionDetails: [TransactionDetails]?
    
    func viewDidLoad() {
        interactor?.createSessionRequest()
        interactor?.apiRequestToGetBizVaultStatus()
    }
    
    func showAdmin() {
        router?.presentAdminScreen(from: self.view)
    }
    
    func showWallet() {
        router?.popToWalletScreen(from: self.view)
    }
    
    func payPopup(payWith: PayCoinType) {
        router?.presentPayPopup(from: self.view, payOptions: payWith)
    }
    
    func requestFailed(message: String) {
        self.view?.showErrorMessage(message: message)
    }
    
    func requestCompleted(data: WalletHistoryEntity) {
        self.view?.showData(data: data)
    }
    
    func bizVaultStatus(isActivated: Bool) {
        self.view?.updateUIForBizVault(status: isActivated)
    }
}
