//
//  BalanceViewController.swift
//  ShaktiCoinApp
//
//  Created by Garnik Giloyan on 11/8/19.
//  Copyright © 2019 Garnik Giloyan. All rights reserved.
//

import UIKit

class BalanceHistoryController: SingleBaseViewController {
    var presenter:BalanceHistoryPresenterProtocol?
    // MARK: - Declarations
    
    lazy var currentBalanceLbl : UILabel = {
        let label = LabelPrototypes.Caption
        label.text = "Current balance:"
        return label
    }()

    lazy var balanceValue : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = ""
        label.font = UIFont.lato(fontSize: 36, type: .Regular) //TODO Medium
        return label
    }()

    lazy var mainWallet : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "Main Wallet"
        label.font = UIFont.lato(fontSize: 12, type: .Bold)
        label.underline()
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(backToWallet))
        label.addGestureRecognizer(tap)
        return label
    }()
    
    lazy var adminImageContainer : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        view.alpha = 0.70
        return view
    }()
    
    lazy var adminImageButton : UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "WalletAdmin")
        iv.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(gotoAdmin))
        iv.addGestureRecognizer(tap)
        return iv
    }()
    
    lazy var adminLabel : UILabel = {
        let label = UILabel()
        label.text = "Admin"
        label.textColor = .white
        label.font = UIFont.futura(fontSize: 9, type: .Medium)
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(gotoAdmin))
        label.addGestureRecognizer(tap)
        return label
    }()
    
    lazy var buttonsContainer: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.5)
        return view
    }()
    
    lazy var buttonsStackContainer: UIStackView = {
        let stackView = UIStackView()
        stackView.backgroundColor = .clear
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.alignment = .center
        stackView.spacing = 15;
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
        
    }()

    
    lazy var btnRecieveSxe : UIButton = {
        let button = GradientButton()
        button.setTitle("Receive SXE", for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 12, type: .Medium)
        button.titleLabel?.textColor = .white
        button.backgroundColor = .clear
        button.setGradientColors([ColorStyle.darkGold.cgColor, ColorStyle.middleGold.cgColor, ColorStyle.lightGold.cgColor])
        button.addTarget(self, action: #selector(receiveWithWallet), for: .touchUpInside)
        return button
    }()
    
    lazy var btnPayWithSxe : UIButton = {
        let button = GradientButton()
        button.setTitle("Pay/Receive SXE", for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 12, type: .Medium)
        button.titleLabel?.textColor = .white
        button.setGradientColors([ColorStyle.darkGold.cgColor, ColorStyle.middleGold.cgColor, ColorStyle.lightGold.cgColor])
        button.backgroundColor = UIColor.mainColor()
        button.addTarget(self, action: #selector(payWithWallet), for: .touchUpInside)
        return button
    }()
    
    lazy var btnPayWithBizVault : UIButton = {
        let button = GradientButton()
        button.setTitle("Pay/Receive Biz Vault", for: .normal)
        button.titleLabel?.font = UIFont.futura(fontSize: 12, type: .Medium)
        button.titleLabel?.textColor = .white
        button.setGradientColors([ColorStyle.darkGold.cgColor, ColorStyle.middleGold.cgColor, ColorStyle.lightGold.cgColor])
        button.backgroundColor = UIColor.mainColor()
        button.addTarget(self, action: #selector(payWithBizVault), for: .touchUpInside)
        return button
    }()
    
    lazy var tableView : UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        return tableView
    }()

    lazy var allTransLabel : UILabel = {
        let label = UILabel()
        label.text = "See all transactions"
        label.textColor = .white
        label.font = UIFont.lato(fontSize: 10, type: .Bold)
        return label
    }()
    
    let transactionCellId: String = "transactionCellId"
    let transactionSectionViewId : String = "transactionSectionViewId"
    
    // MARK: - ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        initNavBar()
        initUI()
        registerCells()
    }
    
    override func viewDidLayoutSubviews() {
        btnRecieveSxe.layer.cornerRadius = 18
        btnRecieveSxe.layer.borderWidth = 1
        btnRecieveSxe.layer.borderColor = UIColor.mainColor().cgColor
        btnPayWithSxe.layer.cornerRadius = 18
        btnPayWithBizVault.layer.cornerRadius = 18
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.btnRecieveSxe.isHidden = true
        self.btnPayWithBizVault.isHidden = true
        showLoader()
        self.presenter?.viewDidLoad()
        balanceValue.text = "SXE \(WalletDataManager.shared.walletBalance ?? "")"
    }
    
    // MARK: - Methods
    private func registerCells() {
        tableView.register(TransactionTableViewCell.self, forCellReuseIdentifier: transactionCellId)
    }
    
    private func initUI() {
        
        containerView.addSubview(currentBalanceLbl)
        containerView.addSubview(balanceValue)
        containerView.addSubview(mainWallet)
        containerView.addSubview(adminImageContainer)
        containerView.addSubview(adminLabel)
        containerView.addSubview(buttonsContainer)
        containerView.addSubview(tableView)
        //containerView.addSubview(allTransLabel)
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        _ = currentBalanceLbl.anchor(coverView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: topBarHeight+30, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        _ = currentBalanceLbl.centralizeX(coverView.centerXAnchor)
        
        _ = balanceValue.anchor(currentBalanceLbl.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = balanceValue.centralizeX(coverView.centerXAnchor)
        
        _ = mainWallet.anchor(balanceValue.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        _ = mainWallet.centralizeX(coverView.centerXAnchor)
        
        _ = adminImageContainer.anchor(balanceValue.bottomAnchor, left: nil, bottom: nil, right: coverView.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 20, widthConstant: 32, heightConstant: 32)
        
        _ = adminLabel.anchor(adminImageContainer.bottomAnchor, left: nil, bottom: nil, right: coverView.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 0)
        
        adminImageContainer.layer.cornerRadius = 32/2

        adminImageContainer.addSubview(adminImageButton)
        _ = adminImageButton.centralizeX(adminImageContainer.centerXAnchor)
        _ = adminImageButton.centralizeY(adminImageContainer.centerYAnchor)
        
        _ = buttonsContainer.anchor(adminLabel.bottomAnchor, left: coverView.leftAnchor, bottom: nil, right: coverView.rightAnchor, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 45)
        
        buttonsContainer.addSubview(buttonsStackContainer)
        
        _ = buttonsStackContainer.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 45)
        buttonsStackContainer.centralizeX(self.buttonsContainer.centerXAnchor)
        buttonsStackContainer.centralizeY(self.buttonsContainer.centerYAnchor)
              
        
        // buttonsContainer.addSubview(btnRecieveSxe)
             //  buttonsContainer.addSubview(btnPayWithSxe)
        
        
        buttonsStackContainer.addArrangedSubview(btnPayWithSxe)
        buttonsStackContainer.addArrangedSubview(btnRecieveSxe)
        buttonsStackContainer.addArrangedSubview(btnPayWithBizVault)
        buttonsStackContainer.translatesAutoresizingMaskIntoConstraints = false;
        
        _ = btnRecieveSxe.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 125, heightConstant: 36)
        _ = btnPayWithSxe.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 125, heightConstant: 36)
        _ = btnPayWithBizVault.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 125, heightConstant: 36)
        
        
       /* buttonsContainer.addSubview(btnRecieveSxe)
        buttonsContainer.addSubview(btnPayWithSxe)
        
        _ = btnRecieveSxe.anchor(nil, left: buttonsContainer.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 60, bottomConstant: 0, rightConstant: 0, widthConstant: 125, heightConstant: 36)
        _ = btnRecieveSxe.centralizeY(buttonsContainer.centerYAnchor)
        
        _ = btnPayWithSxe.anchor(nil, left: nil, bottom: nil, right: buttonsContainer.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 60, widthConstant: 125, heightConstant: 36)
        _ = btnPayWithSxe.centralizeY(buttonsContainer.centerYAnchor)*/
        
        _ = tableView.anchor(buttonsContainer.bottomAnchor, left: coverView.leftAnchor, bottom: self.view.safeAreaLayoutGuide.bottomAnchor, right: coverView.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
//        _ = allTransLabel.anchor(tableView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
//        _ = allTransLabel.centralizeX(containerView.centerXAnchor)
    }


    // MARK: - Events
    @objc private func gotoAdmin() {
        self.presenter?.showAdmin()
    }
    
    @objc private func backToWallet() {
        self.presenter?.showWallet()
    }
    
    @objc private func payWithWallet() {
        self.presenter?.payPopup(payWith:.pay)
    }
    
    @objc private func payWithBizVault() {
        self.presenter?.payPopup(payWith: .bizVault)
    }
    
    @objc private func receiveWithWallet() {
        self.presenter?.payPopup(payWith: .receive)
    }
    // MARK: - NavBar
    private func initNavBar() {
        self.navigationItem.title = "Balance History"
    }

}

extension BalanceHistoryController:BalanceHistoryViewProtocol {
    
    func showData(data:WalletHistoryEntity) {
        hideLoader()
        self.presenter?.transactionDetails = data.transactionDetails
        print(data.transactionDetails.count)
        self.tableView.reloadData()
    }
    
    func showErrorMessage(message:String) {
        hideLoader()
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func updateUIForBizVault(status: Bool) {
        self.btnPayWithBizVault.isHidden = !status
        self.btnRecieveSxe.isHidden = status
        self.btnPayWithSxe.setTitle(status == true ? "Pay/Receive SXE" : "Pay SXE", for: .normal)
    }
}

extension BalanceHistoryController: UITableViewDelegate, UITableViewDataSource {
       // MARK: - TableView
       func numberOfSections(in tableView: UITableView) -> Int {
            return self.presenter?.transactionDetails?.count ?? 0
       }
       
       func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           
           let returnedView = UIView(frame: CGRect(x:0, y:0, width: 150, height: 25))
           returnedView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
           
           let label = UILabel()
           returnedView.addSubview(label)
           
           _ = label.anchor(nil, left: returnedView.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
           _ = label.centralizeY(returnedView.centerYAnchor)
        
        
        if section == 0 {
            let data = self.presenter?.transactionDetails?[section]
            label.text = (data?.timestamp ?? 0).convertTimeIntervalToDate(format:"MMMM YYYY")
        } else {
            let currentTransactionData = self.presenter?.transactionDetails?[section]
            let previousTransactionData = self.presenter?.transactionDetails?[section - 1]
            let currentDate = (currentTransactionData?.timestamp ?? 0).convertTimeIntervalToDate(format:"MMMM YYYY")
            let previousDate = (previousTransactionData?.timestamp ?? 0).convertTimeIntervalToDate(format:"MMMM YYYY")
            if currentDate != previousDate {
                label.text = currentDate
            } else {
                label.text = ""
            }
        }
            
          // label.text = (section == 0) ? "September 2019" : "August 2019"
           label.textColor = .white
           label.font = UIFont.lato(fontSize: 14)
           
           returnedView.addSubview(label)

           return returnedView
       }

       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return 1
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
           let cell = tableView.dequeueReusableCell(withIdentifier: transactionCellId, for: indexPath as IndexPath) as! TransactionTableViewCell
            let data = self.presenter?.transactionDetails?[indexPath.section]
           cell.selectionStyle = .none
           cell.backgroundColor = .clear
        cell.trsTitle.text = "*****\(String(data?.identifier.suffix(9) ?? "Transaction Name"))"
            cell.priceLbl.text = "\(data?.amountInToshi ?? 0)"
        let timestamp = data?.timestamp ?? 0
        cell.dateTimeLbl.text = timestamp.convertTimeIntervalToDate(format: "dd/MM/yy HH:mm")
        return cell
    }


    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
           return 30
        } else {
            let currentTransactionData = self.presenter?.transactionDetails?[section]
            let previousTransactionData = self.presenter?.transactionDetails?[section - 1]
            let currentDate = (currentTransactionData?.timestamp ?? 0).convertTimeIntervalToDate(format:"MMMM YYYY")
            let previousDate = (previousTransactionData?.timestamp ?? 0).convertTimeIntervalToDate(format:"MMMM YYYY")
            if currentDate != previousDate {
                return 30
            } else {
                return 0
            }
        }
    }
       
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 50
       }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
