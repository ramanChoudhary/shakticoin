//
//  WalletHistoryEntity.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 16/09/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation


//struct WalletHistoryEntity : MapRepresentable {
//    var message: String
//    var blockByTime:String
//    
//    init(map: Map) throws {
//        message = try map.get("message")
//        blockByTime = try map.get("blockByTime")
//    }
//}

struct WalletHistoryEntity: MapRepresentable {
    var identifier:String
    var index: Int
    var network:String
    var nonce:String
    var previousIdentifier:String
    var timestamp:Int
    var version:String
    var transactionDetails:[TransactionDetails]
    
    
    init(map: Map) throws {
        identifier = try map.get("identifier")
        index = try map.get("index")
        network = try map.get("network")
        nonce = try map.get("nonce")
        previousIdentifier = try map.get("previousIdentifier")
        timestamp = try map.get("timestamp")
        version = try map.get("version")
        transactionDetails = try map.get("transactionDetails")
    }
}

struct TransactionDetails: MapRepresentable {
    var identifier:String
    var amountInToshi: Int
    var timestamp:Int
    
    
    init(map: Map) throws {
        identifier = try map.get("identifier")
        amountInToshi = try map.get("amountInToshi")
        timestamp = try map.get("timestamp")
    }
}
