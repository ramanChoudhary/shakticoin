//
//  BalanceHistoryProtocols.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 28/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

import UIKit

protocol BalanceHistoryViewProtocol: class {
    var presenter:BalanceHistoryPresenterProtocol? {get set}
    func showData(data:WalletHistoryEntity)
    func showErrorMessage(message:String)
    func updateUIForBizVault(status:Bool)
}

protocol BalanceHistoryPresenterProtocol: class {
    var view:BalanceHistoryViewProtocol? {get set}
    var router:BalanceHistoryRouterProtocol? {get set}
    var interactor:BalanceHistoryInteractorProtocol? {get set}
    var transactionDetails:[TransactionDetails]? {get set}
    //View =========> Presenter
    func viewDidLoad()
    func showAdmin()
    func showWallet()
    func payPopup(payWith:PayCoinType)
    //Interactor ========== Presenter
    func requestCompleted(data:WalletHistoryEntity)
    func requestFailed(message:String)
    func bizVaultStatus(isActivated:Bool)
}

protocol BalanceHistoryInteractorProtocol: class {
    var presenter:BalanceHistoryPresenterProtocol? {get set}
    func createSessionRequest()
    func apiRequestToGetBizVaultStatus()
}

protocol BalanceHistoryRouterProtocol: class {
    static func createBalanceHistoryModule() -> UIViewController
    //Presenter -> Router
    func presentAdminScreen(from view:BalanceHistoryViewProtocol?)
    func popToWalletScreen(from view:BalanceHistoryViewProtocol?)
    func presentPayPopup(from view:BalanceHistoryViewProtocol?, payOptions:PayCoinType)
}
