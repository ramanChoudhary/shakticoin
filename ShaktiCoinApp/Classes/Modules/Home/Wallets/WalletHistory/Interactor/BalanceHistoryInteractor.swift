//
//  BalanceHistoryInteractor.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 28/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

class BalanceHistoryInteractor:BalanceHistoryInteractorProtocol {
    var presenter: BalanceHistoryPresenterProtocol?
    var dataSource = RemoteDataSource.shared
    
    func getBalanceHistoryRequest(sessionToken:Int) {
        let timestamp = Date().timeIntervalSince1970
        let params: JSONObject = [
            "includeTransactionDetails": 1,
            "sessionToken": sessionToken,
            "timestamp":timestamp
        ]
        
        dataSource.requestObject(forEndpoint: Endpoint.WalletServiceEndpoint.getBalanceHistory,  parameters: params) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let json):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(json)
                    }
                    if let blockByTime = json["blockByTime"] as? [String:Any]   {
                        //if let data = blockByTime.data(using: .utf8) {
                            //do {
//                                let jsonData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? JSONObject
//                                guard let data = jsonData else {
//                                    self.presenter?.requestFailed(message: "Invalid Data")
//                                    return
//                                }
                                do {
                                    let model = try Mapper<WalletHistoryEntity>.map(object: blockByTime)
                                    self.presenter?.requestCompleted(data: model)
                                } catch (let error) {
                                    self.presenter?.requestFailed(message: error.localizedDescription)
                                }
//                            } catch (let error) {
//                                self.presenter?.requestFailed(message: error.localizedDescription)
//                            }
                        //}
                    }
                case .failure(let error):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(error.localizedDescription)
                    }
                    self.presenter?.requestFailed(message: error.localizedDescription)
                }
            }
        }
    }
    
    func getBlockDataRequest(sessionToken:Int) {
        let params: JSONObject = [
            "includeTransactionDetails": 0,
            "index": 0,
            "sessionToken": sessionToken
        ]
        
        dataSource.requestObject(forEndpoint: Endpoint.WalletServiceEndpoint.blockData,  parameters: params) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let json):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(json)
                    }
                    //                    do {
                    //                        let model = try Mapper<WalletEntity>.map(object:json)
                    //                        self.presenter?.requestCompleted(data: model)
                    //                    } catch (let error) {
                    //                        self.presenter?.requestFailed(message: error.localizedDescription)
                //                    }
                case .failure(let error):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(error.localizedDescription)
                    }
                    self.presenter?.requestFailed(message: error.localizedDescription)
                }
            }
        }
    }
    
    func createSessionRequest() {
        let params: JSONObject = [
            "cacheBytes": "",
            "passphrase": WalletDataManager.shared.passphrase ?? "",
            "walletBytes": WalletDataManager.shared.wallet ?? ""
        ]
        dataSource.requestObject(forEndpoint: Endpoint.WalletServiceEndpoint.createSession,  parameters: params) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let json):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(json)
                    }
                    if let sessionToken = json["sessionToken"] as? String {
                        self.getBalanceHistoryRequest(sessionToken: Int(sessionToken) ?? 0)
                    } else if let sessionToken = json["sessionToken"] as? Int {
                        self.getBalanceHistoryRequest(sessionToken: sessionToken)
                    } else {
                        self.presenter?.requestFailed(message: "Session Token parsing error")
                    }
                case .failure(let error):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(error.localizedDescription)
                    }
                    self.presenter?.requestFailed(message: error.localizedDescription)
                }
            }
        }
    }
    
    func apiRequestToGetBizVaultStatus() {
        dataSource.requestObject(forEndpoint: Endpoint.BizVaultEndpoint.checkBizVault) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let json):
                    if LocalFlagManager.shared.value(.logApiCalls) {
                        print(json)
                    }
                    
                    do {
                        let model = try Mapper<BizVaultResponse>.map(object: json)
                        self.presenter?.bizVaultStatus(isActivated: model.details.bizvaultIdStatus)
                    } catch (let error) {
                        self.presenter?.bizVaultStatus(isActivated: false)
                        self.presenter?.requestFailed(message: error.localizedDescription)
                    }
                    
                case .failure(let error):
                    self.presenter?.requestFailed(message: error.localizedDescription)
                    self.presenter?.bizVaultStatus(isActivated: false)
                }
            }
        }
    }
}

