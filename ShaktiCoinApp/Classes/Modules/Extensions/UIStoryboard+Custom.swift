//
//  UIStoryboard.swift
//  CullintonsCustomer
//
//  Created by Rakesh Kumar on 30/03/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: STORYBOARD.wallet, bundle: Bundle.main)
    }
    
    static func rootController(identifier:String) -> UIViewController {
        return mainStoryboard.instantiateViewController(withIdentifier: identifier)
    }
}
