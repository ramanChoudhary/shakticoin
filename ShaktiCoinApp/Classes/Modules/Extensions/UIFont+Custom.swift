//
//  UIFont.swift
//  CullintonsCustomer
//
//  Created by Rakesh Kumar on 31/03/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    open func setLatoBoldFont(size:CGFloat) -> UIFont {
        if let font = UIFont(name: "Lato-Bold", size: size) {
            return font
        } else {
            return UIFont.systemFont(ofSize: size)
        }
    }
    
    open func setLatoRegularFont(size:CGFloat) -> UIFont {
        if let font = UIFont(name: "Lato-Regular", size: size) {
            return font
        } else {
            return UIFont.systemFont(ofSize: size)
        }
    }
    
    open func setLatoMediumFont(size:CGFloat) -> UIFont {
        if let font = UIFont(name: "Lato-Medium", size: size) {
            return font
        } else {
            return UIFont.systemFont(ofSize: size)
        }
    }
    open func setLatoLightFont(size:CGFloat) -> UIFont {
        if let font = UIFont(name: "Lato-Light", size: size) {
            return font
        } else {
            return UIFont.systemFont(ofSize: size)
        }
    }
    
    open func setFuturaMediumFont(size:CGFloat) -> UIFont {
        if let font = UIFont(name: "Futura-Medium", size: size) {
            return font
        } else {
            return UIFont.systemFont(ofSize: size)
        }
    }
}


