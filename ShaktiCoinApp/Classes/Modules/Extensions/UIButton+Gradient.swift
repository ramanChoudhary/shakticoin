//
//  UIButton+Gradient.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 29/08/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

extension UIButton {
//    func gradientColor(_ colors: [Any]) {
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.colors = colors
//        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
//        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
//        setNeedsDisplay()
//    }
    
    func applyGradientColors(colours: [UIColor]) -> Void {
           let gradient:CAGradientLayer = CAGradientLayer()
           gradient.frame = self.bounds
           gradient.colors = colours.map { $0.cgColor }
           //gradient.locations = locations
           gradient.name = "gradientLayer"
           gradient.startPoint = CGPoint(x: 0.0, y: 0.5)// CGPointMake(0.0, 0.5)
           gradient.endPoint = CGPoint(x: 1.0, y: 0.5)//CGPointMake(1.0, 0.5)
           self.layer.insertSublayer(gradient, at: 0)
           self.clipsToBounds = true
           
       }
    
    func setButtonWithBorder(title:String,titleColor:UIColor,titlefont:UIFont?,border_Width:CGFloat = 0, border_Color:UIColor = .clear,cornerRadius:CGFloat = 0){
        self.borderWidth(border_Width)
        self.borderColor(border_Color)
        self.cornerRadius = cornerRadius
        self.setTitle(title, for: .normal)
        self.setTitleColor(titleColor, for: .normal)
        self.titleLabel?.font = titlefont
    }
    
    func setCustomButtonWithBgColor(title:String?, titleColor:UIColor, bgColor:UIColor, font: UIFont , borderWidth:CGFloat = 0, borderColor:UIColor = .clear,cornerRadius:CGFloat = 0) {
        self.backgroundColor = bgColor
        self.setTitle(title, for: .normal)
        self.setTitleColor(titleColor, for: .normal)
        self.titleLabel?.font = font
        self.borderWidth(borderWidth)
        self.borderColor(borderColor)
        self.cornerRadius = cornerRadius
    }
}
