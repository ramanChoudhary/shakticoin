//
//  String+Custom.swift
//  ShaktiCoinApp
//
//  Created by Rakesh Kumar on 18/09/20.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation


extension Int {
    func convertTimeIntervalToDate(format:String) -> String {
        let date = Date(timeIntervalSince1970:TimeInterval(self))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = format//"dd/MM/yy HH:mm" //Your date format
        let date1 = dateFormatter.string(from: date)
        return "\(date1)"
    }
}

