//
//  MenuView.swift
//  ShaktiCoinApp
//
//  Created by sperev on 20/10/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

protocol MenuApplicable: class {
    var menuView: MenuView { get }
}

class MenuView: StackViewModule, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    var isOpened = false
    
    var currentMenuItem: Menu = .wallet {
        didSet {
            collectionView?.reloadData()
        }
    }
    
    private let cellId : String = "MenuCell"
    
    private var menuItems: [Menu] = []
    private var menusExpanded = false
    private var bonusBounty: Bool = false
    
    weak var viewController: UIViewController?

    var constraint: NSLayoutConstraint?
    
    @IBOutlet weak var collectionView: UICollectionView? {
        didSet {
            collectionView?.backgroundColor = UIColor().hexStringToUIColor("#0C0C0C")
            collectionView?.layer.borderWidth = 0.5
            collectionView?.layer.borderColor = UIColor.darkGray.cgColor
        }
    }
    
    override func configureView() {
        super.configureView()
        
        let cellNib = UINib(nibName: "MenuCell", bundle: nil)
        collectionView?.register(cellNib, forCellWithReuseIdentifier: cellId)
        
        updateMenuRows()
        
        ChooseBountyRepository.shared.bonusBounty(callback: { [weak self] in
            switch $0 {
            case .success:
                ViewDispatcher.shared.execute {
                    self?.bonusBounty = true
                    self?.updateMenuRows()
                }
            case .failure: break
            }
        })
    }
    
    private func updateMenuRows() {
        var rows: [Menu] = [.wallet, .vault, .miner]
        if menusExpanded {
            rows.append(contentsOf: [.poe,.addFriendsAndFamily])
        }
        rows.append(bonusBounty ? .grabBonusBounty : .claimBonusBounty)
        rows.append(contentsOf: [.settings, .home,
        .familyTree, .company])
        menuItems = rows

        collectionView?.reloadData()
    }
    
    // MARK: - CollectionView
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MenuCell
                    
        cell.menu = menuItems[indexPath.row]
        cell.menuButton.tag = menuItems[indexPath.row].rawValue
        
        cell.menuButton.addTarget(self, action: #selector(onMenuButtonClick(_:)), for: .touchUpInside)
        
        if menuItems[indexPath.row] == currentMenuItem {
            cell.iconImage.tintColor = UIColor.mainColor()
            cell.titleLabel.textColor = UIColor.mainColor()
        } else {
            cell.iconImage.tintColor = UIColor.white
            cell.titleLabel.textColor = UIColor.white
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    // MARK: - Events
    
    @objc private func onMenuButtonClick( _ sender: UIButton) {
        print("Menu Item Clicked sender = \(sender.tag)")

        let index = sender.tag - 1
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if index == 0 {
            AppDelegate.shared?.perform(.myWallet)
        } else if index == 1 {
            
            let vc = storyboard.instantiateViewController(withIdentifier: "BusinessVaultIntroViewController") as! BusinessVaultIntroViewController
            let navigationController = UINavigationController(rootViewController: vc)
            AppDelegate.shared?.window?.rootViewController = navigationController
            
        } else if index == 2 {
            menusExpanded = !menusExpanded
            updateMenuRows()
        } else if index == 3 {
        } else if index == 4 {
        }
        
        else if index == 5 {
            
            let vc = storyboard.instantiateViewController(withIdentifier: "MyReferralsViewController") as! MyReferralsViewController
            let navigationController = UINavigationController(rootViewController: vc)
            AppDelegate.shared?.window?.rootViewController = navigationController
            
        } else if index == 6 {
            
            let vc = storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            let navigationController = UINavigationController(rootViewController: vc)
            AppDelegate.shared?.window?.rootViewController = navigationController
            
        } else if index == 7 {
            
            //TODO
            
        } else if index == 8 {
            let vc = FamilyTreeViewController()
            viewController?.navigationController?.pushViewController(vc, animated: true)
        } else if index == 9 {
            
            let vc = storyboard.instantiateViewController(withIdentifier: "CompanyInfoViewController") as! CompanyInfoViewController
            let navigationController = UINavigationController(rootViewController: vc)
            AppDelegate.shared?.window?.rootViewController = navigationController
        } else if index == 10 {
            let container = BaseScrollViewController()
            container.menuView.currentMenuItem = .grabBonusBounty
            let navigationController = UINavigationController(rootViewController: container)
            AppDelegate.shared?.window?.rootViewController = navigationController
            container.addContentView(ClaimBonusWireframe.createModule())
            container.navigationItem.title = "GrabYourBounty".localized
        } else if index == 11 {
            AppDelegate.shared?.window?.rootViewController = ChooseBountyWireframe.createModule()
        }
    }
}

extension UIViewController {
    func applyMenu() {
        guard let self = self as? MenuApplicable else { return }
        let button = UIBarButtonItem(image: UIImage(named: "Menu"), style: .plain, target: self, action: #selector(onBurgerMenuClick))
        button.tintColor = .white
        navigationItem.leftBarButtonItem = button
        
        view.addSubview(self.menuView)
        view.bringSubviewToFront(self.menuView)
        
        self.menuView.constraint = self.menuView.rightAnchor.constraint(equalTo: view.leftAnchor)
        self.menuView.constraint?.isActive = true
        
        self.menuView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        self.menuView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    @objc private func onBurgerMenuClick() {
        guard let self = self as? MenuApplicable else { return }
        self.menuView.isOpened = !self.menuView.isOpened
        let width = self.menuView.isOpened ? self.menuView.bounds.width : 0
        self.menuView.constraint?.constant = width
        UIView.animate(withDuration: CATransaction.animationDuration(), animations: {
            self.menuView.superview?.layoutIfNeeded()
        })
    }
}
