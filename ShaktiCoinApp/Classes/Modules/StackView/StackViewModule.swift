//
//  StackModule.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

class StackViewModule: UIView {
    
    @IBOutlet var view: UIView!
    
    init(withoutXib: Bool) {
        super.init(frame: .zero)
        if !withoutXib {
            configureView()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureView()
    }
    
    func configureView() {
        translatesAutoresizingMaskIntoConstraints = false
        Bundle(for: StackViewModule.self).loadNibNamed(self.className, owner: self, options: nil)
        ViewHelper.configure(view, in: self)
    }
}
