//
//  StackView.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

class StackView: BaseView {

    var scrollView: UIScrollView!
    let stack: UIStackView = UIStackView()
    var stackTopInset: NSLayoutConstraint!
    let horizontalMargin: CGFloat = 22
    
    @IBOutlet weak var stackViewContainer: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        scrollView = UIScrollView(frame: view.bounds)

        stack.axis = .vertical
        stack.distribution = .fill
        stack.alignment = .fill

        stack.translatesAutoresizingMaskIntoConstraints = false
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        
        ViewHelper.configure(scrollView, in: stackViewContainer)
        
        stackViewContainer.backgroundColor = .clear
        scrollView.addSubview(stack)

        stackTopInset = stack.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0)
        
        NSLayoutConstraint.activate([
            stack.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stack.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            stackTopInset,
            stack.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0),
            stack.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: 0)
        ])
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.updateContentView()
    }

    public func addViewToScrollTopRight(_ myView: UIView) {
        scrollView.addSubview(myView)
        myView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            myView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            myView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 32)
            ])

        scrollView.bringSubviewToFront(myView)
    }

    var spaceBetweenSections: CGFloat = 0 {
        didSet {
            stack.spacing = spaceBetweenSections
        }
    }
    
    public func addView(_ view: UIView, below subview: UIView? = nil, edgeInsets: UIEdgeInsets = UIEdgeInsets.zero, backgroundColor: UIColor = .clear) {
        stack.addView(view, below: subview, edgeInsets: edgeInsets, backgroundColor: backgroundColor)
        
        scrollView.setNeedsLayout()
        scrollView.layoutIfNeeded()
    }
    
    public func addBottomView(_ view: UIView, edgeInsets: UIEdgeInsets = UIEdgeInsets.zero, backgroundColor: UIColor = .clear) {
        addView(view, edgeInsets: edgeInsets, backgroundColor: backgroundColor)
        
        view.setNeedsLayout()
        view.layoutIfNeeded()
        
        stack.setNeedsLayout()
        stack.layoutIfNeeded()
        
        let spaceLeft = stackViewContainer.bounds.size.height - stack.bounds.size.height
        
        if spaceLeft > 0 {
            let spaceView = UIView()
            spaceView.backgroundColor = .clear
            spaceView.translatesAutoresizingMaskIntoConstraints = false
            spaceView.heightAnchor.constraint(equalToConstant: spaceLeft).isActive = true
            addView(spaceView, above: view)
        }
    }
    
    public func addView(_ view: UIView, above subview: UIView?, edgeInsets: UIEdgeInsets = UIEdgeInsets.zero, backgroundColor: UIColor = .clear) {
        stack.addView(view, above: subview, edgeInsets: edgeInsets, backgroundColor: backgroundColor)
    }

    public func removeView(_ view: UIView) {
        if let superview = view.superview {
            stack.removeArrangedSubview(superview)
            superview.removeFromSuperview()
        }
    }
    
    public func removeAllViews() {
        stack.arrangedSubviews.forEach { (view) in
            stack.removeArrangedSubview(view)
            view.removeFromSuperview()
        }
    }

    public func addInsets(top: CGFloat, bottom: CGFloat) {
        scrollView.contentInset = UIEdgeInsets(top: top, left: 0, bottom: bottom, right: 0)
    }
    
    public func containsView(_ view: UIView) -> Bool {
        return stack.containsView(view)
    }

    func updateLayout(_ animated: Bool) {
        let options = UIView.AnimationOptions.curveEaseIn
        let duration = animated ? CATransaction.animationDuration() : 0

        UIView.animate(withDuration: duration, delay: 0, options: options, animations: {
            self.view.layoutIfNeeded()
        })
    }
}

extension UIScrollView {
    func updateContentView() {
        contentSize.height = subviews
            .filter({ !($0 is UIImageView) })
            .sorted(by: { $0.frame.maxY < $1.frame.maxY }).last?.frame.maxY ?? contentSize.height
    }
}

extension UIViewController {
    func showAlert(errorMessage: String) {
        let alert = UIAlertController(title: "", message: errorMessage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func showAlert(_ model: AlertModel) {
        let alert = UIAlertController(title: model.title, message: model.message, preferredStyle: model.style)
        model.actions.forEach {
            alert.addAction($0)
        }
        present(alert, animated: true, completion: nil)
    }
    
    func showLoader() {
        LoaderModule.shared.show(in: view)
    }
    
    func hideLoader() {
        LoaderModule.shared.hide()
    }
}

