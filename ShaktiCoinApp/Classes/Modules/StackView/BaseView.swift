//
//  BaseController.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

class BaseView: UIViewController {
    var arrayTextFields: [(UITextField, UIView)]?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        modalPresentationStyle = .overFullScreen
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        modalPresentationStyle = .overFullScreen
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNeedsStatusBarAppearanceUpdate()

        let backButton = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButton
    }
    
    // MARK: TextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let arrayTextFields = arrayTextFields {
            for (text, view) in arrayTextFields where textField == text {
                view.backgroundColor = UIColor.gray
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let arrayTextFields = arrayTextFields {
            for (text, view) in arrayTextFields where textField == text {
                view.backgroundColor = UIColor.lightGray
            }
        }
    }
}
