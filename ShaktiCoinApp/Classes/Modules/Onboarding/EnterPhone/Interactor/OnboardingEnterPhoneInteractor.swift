//
//  OnboardingEnterPhoneInteractor.swift
//  ShaktiCoinApp
//
//  Created by sperev on 14/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class OnboardingEnterPhoneInteractor: OnboardingEnterPhoneInteractorProtocol {
    weak var presenter: OnboardingEnterPhoneInteractorOutputProtocol?
    var repository: OnboardingEnterPhoneRepositoryProtocol?
    
    func `do`(_ job: OnboardingEnterPhoneJob) {
        switch job {
        case .request(let code, let phone):
            request(code: code, phone: phone)
        }
    }
    
    private func request(code: String, phone: String) {
        repository?.request(countryCode: code, phone: phone, callback: { [weak self] in
            switch $0 {
            case .success(let model):
                if model.responseCode == 200 {
                    self?.presenter?.handle(.requested)
                } else {
                    let error = ErrorModel(ShaktiError.General.unknown)
                    self?.presenter?.handle(.failed(error))
                }
            case .failure(let error):
                if case let ShaktiError.API.response(model) = error.value, model.responseCode == 409 {
                    self?.presenter?.handle(.exist)
                }
                
                self?.presenter?.handle(.failed(error))
            }
        })
    }
}
