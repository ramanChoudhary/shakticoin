//
//  OnboardingEnterPhonePresenter.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class OnboardingEnterPhonePresenter: OnboardingEnterPhonePresenterProtocol, OnboardingEnterPhoneInteractorOutputProtocol {
    weak var view: OnboardingEnterPhoneViewProtocol?
    var interactor: OnboardingEnterPhoneInteractorProtocol?
    var wireframe: OnboardingEnterPhoneWireframeProtocol?
    
    private var continueButton: ButtonModuleModel {
        return ButtonModuleModel(title: "onboarding.enter_phone.button.continue".localized, enabled: true, callback: nil)
    }
    
    private var phoneTextViewModel: TextFieldViewModel {
        let textStyle = TextStyle.lato14.color(ColorStyle.white.color)
        return TextFieldViewModel(textStyle: textStyle, textLocalized: "", placeholderStyle: textStyle, placeholderLocalized: "onboarding.enter_phone.text_field_placeholder".localized, keyboardType: .phonePad, isSecureTextEntry: false)
    }
    
    private var codeTextViewModel: CountryCodeTextFieldViewModel {
        let textStyle = TextStyle.lato14.color(ColorStyle.white.color)
        let text = TextViewModel(style: textStyle, value: "PhoneNumber".localized)
        return CountryCodeTextFieldViewModel(text: text, favoriteCountriesLocaleIdentifiers: ["CH", "US"])
    }
    
    private var backLink: TextButtonModel {
        let title = TextViewModel(style: TextStyle.latoSemiBold14.underline(true).color(.white), value: "onboarding.enter_phone.button.back".localized)
        return TextButtonModel(title: title, enabled: true) { [weak self] in
            self?.wireframe?.navigate(.back)
        }
    }
    
    private var emailConfirmed: TextViewModel {
        return TextViewModel(style: TextStyle.latoLight24.alignment(.center), value: "onboarding.enter_phone.emailConfirmed".localized)
    }
    
    private var willSendSMS: TextViewModel {
        return TextViewModel(style: TextStyle.latoSemiBold14.alignment(.center), value: "onboarding.enter_phone.willSendSMS".localized)
    }
    
    func perform(_ action: OnboardingEnterPhoneViewAction) {
        switch action {
        case .loaded:
            let model = OnboardingHeaderViewModel.onboardingHeader(.enterMobile)
            view?.populate(.addHeader(model))
            view?.populate(.addSpace)
            view?.populate(.addText(emailConfirmed))
            view?.populate(.addText(willSendSMS))
            view?.populate(.addSpace)
            view?.populate(.addCountryCodeTextField(codeTextViewModel))
            view?.populate(.addSpace)
            view?.populate(.addTextField(phoneTextViewModel))
            view?.populate(.addSpace)
            view?.populate(.addButton(continueButton))
            view?.populate(.addSpace)
            view?.populate(.addLink(backLink))
        case .continue(let code, let phone):
            view?.populate(.loading)
            interactor?.do(.request(code, phone))
        }
    }
    
    func handle(_ result: OnboardingEnterPhoneResult) {
        switch result {
        case .requested:
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
                self.wireframe?.navigate(.phoneCodeVerification)
            }
        case .failed(let error):
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
                self.view?.populate(.error(error.localizedDescription))
            }
        case .exist: break
        }
    }
}
