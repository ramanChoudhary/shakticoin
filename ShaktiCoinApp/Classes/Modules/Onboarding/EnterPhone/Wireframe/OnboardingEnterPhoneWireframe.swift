//
//  OnboardingEnterPhoneWireframe.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

final class OnboardingEnterPhoneWireframe: OnboardingEnterPhoneWireframeProtocol {
    class func createModule() -> UIViewController {
        
        let view = OnboardingEnterPhoneView()
        let presenter =  OnboardingEnterPhonePresenter()
        let interactor = OnboardingEnterPhoneInteractor()
        let repository = OnboardingEnterPhoneRepository()
        let dataManager = OnboardingEnterPhoneDataManager()
        let wireframe = OnboardingEnterPhoneWireframe()
        
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.wireframe = wireframe
        
        interactor.presenter = presenter
        interactor.repository = repository
        repository.dataManager = dataManager
        
        return view
    }
    
    func navigate(_ route: OnboardingEnterPhoneRoute) {
        switch route {
        case .phoneCodeVerification:
            let delegate = UIApplication.shared.delegate as? AppDelegate
            delegate?.window?.rootViewController = OnboardingEnterPhoneCodeWireframe.createModule()
        case .back:
            let delegate = UIApplication.shared.delegate as? AppDelegate
            delegate?.window?.rootViewController = OnboardingEnterEmailWireframe.createModule()
        }
    }
}
