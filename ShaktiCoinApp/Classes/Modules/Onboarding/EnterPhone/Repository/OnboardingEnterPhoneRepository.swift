//
//  OnboardingEnterPhoneRepository.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class OnboardingEnterPhoneRepository: OnboardingEnterPhoneRepositoryProtocol {
    var dataManager: OnboardingEnterPhoneDataManagerProtocol?
    
    func request(countryCode: String, phone: String, callback: @escaping (Result<ResponseModel, ErrorModel>) -> Void) {
        CurrentUserDataManager.shared.phone = phone
        CurrentUserDataManager.shared.countryCode = countryCode
        dataManager?.request(countryCode: countryCode, phone: phone, callback: {
            switch $0 {
            case .success(let json):
                if let model = try? Mapper<ResponseModel>.map(object: json) {
                    callback(.success(model))
                } else {
                    callback(.failure(ErrorModel(MappingError.unknown)))
                }
            case .failure(let error):
                callback(.failure(error))
            }
        })
    }
}
