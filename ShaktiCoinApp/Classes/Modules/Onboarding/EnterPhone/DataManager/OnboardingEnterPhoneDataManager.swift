//
//  OnboardingEnterPhoneDataManager.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class OnboardingEnterPhoneDataManager: OnboardingEnterPhoneDataManagerProtocol {
    var dataSource = RemoteDataSource.shared
    func request(countryCode: String, phone: String, callback: @escaping APIDataManagerObjectResultBlock) {
        let params: JSONObject = [
            "mobileNo": phone,
            "countryCode": countryCode
        ]
        dataSource.requestObject(forEndpoint: Endpoint.PhoneVerification.request, parameters: params, result: callback)
    }
}
