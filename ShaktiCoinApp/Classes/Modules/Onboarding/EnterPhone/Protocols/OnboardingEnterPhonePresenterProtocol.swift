//
//  OnboardingEnterPhonePresenterProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum OnboardingEnterPhoneViewAction {
    case loaded
    case `continue`(String, String)
}

protocol OnboardingEnterPhonePresenterProtocol {
    func perform(_ action: OnboardingEnterPhoneViewAction)
}
