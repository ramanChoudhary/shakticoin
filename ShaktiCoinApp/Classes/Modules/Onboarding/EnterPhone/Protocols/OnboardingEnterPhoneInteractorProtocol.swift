//
//  OnboardingEnterPhoneInteractorProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 14/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum OnboardingEnterPhoneJob {
    case request(String, String)
}

enum OnboardingEnterPhoneResult {
    case requested
    case exist
    case failed(ErrorModel)
}

protocol OnboardingEnterPhoneInteractorProtocol {
    func `do`(_ job: OnboardingEnterPhoneJob)
}

protocol OnboardingEnterPhoneInteractorOutputProtocol: class {
    func handle(_ result: OnboardingEnterPhoneResult)
}
