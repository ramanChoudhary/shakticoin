//
//  OnboardingEnterPhoneWireframeProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

enum OnboardingEnterPhoneRoute {
    case phoneCodeVerification
    case back
}

protocol OnboardingEnterPhoneWireframeProtocol {
    func navigate(_ route: OnboardingEnterPhoneRoute)
}
