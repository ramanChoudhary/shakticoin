//
//  OnboardingEnterPhoneView.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

final class OnboardingEnterPhoneView: StackView, OnboardingEnterPhoneViewProtocol {
    var presenter: OnboardingEnterPhonePresenterProtocol?
    
    private weak var textFieldModule: TextFieldModule?
    private weak var countryCodeTextFieldModule: CountryCodeTextFieldModule?
    private var insets = UIEdgeInsets.horizontal(22).vertical(12)
    private var textInsets = UIEdgeInsets.horizontal(22).vertical(6)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.perform(.loaded)
    }
    
    func populate(_ state: OnboardingEnterPhoneViewState) {
        switch state {
        case .loaded: hideLoader()
        case .loading: showLoader()
        case .addButton(var viewModel):
            viewModel.callback = { [weak self] in
                guard let textField = self?.textFieldModule?.textField,
                    let text = textField.text, let codeTextField = self?.countryCodeTextFieldModule, let code = codeTextField.selectedCountryPhoneCode else { return }
                textField.resignFirstResponder()
                self?.presenter?.perform(.continue("+" + code, text))
            }
            
            let module = GoldButtonModule()
            module.populate(model: viewModel)
            addView(module, edgeInsets: textInsets)
        case .addTextField(let viewModel):
            let module = TextFieldModule()
            module.populate(viewModel)
            addView(module, edgeInsets: insets)
            textFieldModule = module
        case .error(let localizedError):
            showAlert(errorMessage: localizedError)
        case .addText(let viewModel):
            let module = UILabel()
            module.setup(viewModel)
            addView(module, edgeInsets: textInsets)
        case .addSpace:
            let module = UIView()
            module.backgroundColor = .clear
            module.translatesAutoresizingMaskIntoConstraints = false
            module.heightAnchor.constraint(equalToConstant: 1).isActive = true
            addView(module, edgeInsets: textInsets)
        case .addLink(let viewModel):
            let module = TextButtonModule()
            module.populate(viewModel)
            addView(module, edgeInsets: textInsets)
        case .addCountryCodeTextField(let viewModel):
            var newViewModel = viewModel
            newViewModel.viewController = self
            let module = CountryCodeTextFieldModule()
            module.populate(newViewModel)
            addView(module, edgeInsets: textInsets)
            countryCodeTextFieldModule = module
        case .addHeader(let viewModel):
            let module = OnboardingHeaderModule()
            module.populate(viewModel)
            addView(module)
        }
    }
}
