//
//  OnboardingEnterEmailInteractor.swift
//  ShaktiCoinApp
//
//  Created by sperev on 14/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class OnboardingEnterEmailInteractor: OnboardingEnterEmailInteractorProtocol {
    weak var presenter: OnboardingEnterEmailInteractorOutputProtocol?
    var repository: OnboardingEnterEmailRepositoryProtocol?
    
    func `do`(_ job: OnboardingEnterEmailJob) {
        switch job {
        case .request(let email):
            request(email: email)
        }
    }
    
    private func request(email: String) {
        repository?.request(email: email, callback: { [weak self] in
            switch $0 {
            case .success(let model):
                if model.responseCode == 200 {
                    self?.presenter?.handle(.requested)
                } else {
                    let error = ErrorModel(ShaktiError.General.unknown)
                    self?.presenter?.handle(.failed(error))
                }
            case .failure(let error):
                if case let ShaktiError.API.response(model) = error.value, model.responseCode == 409 {
                    self?.presenter?.handle(.registered)
                } else if case let ShaktiError.API.response(model) = error.value, model.responseCode == 422 {
                    self?.presenter?.handle(.verified)
                } else {
                    self?.presenter?.handle(.failed(error))
                }
            }
        })
    }
}
