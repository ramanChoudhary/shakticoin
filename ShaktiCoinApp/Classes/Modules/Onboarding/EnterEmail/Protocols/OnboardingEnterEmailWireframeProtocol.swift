//
//  OnboardingEnterEmailWireframeProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

enum OnboardingEnterEmailRoute {
    case emailVerificationRequested
    case enterPhoneNumber
    case login
}

protocol OnboardingEnterEmailWireframeProtocol {
    func navigate(_ route: OnboardingEnterEmailRoute)
}
