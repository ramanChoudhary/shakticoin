//
//  OnboardingEnterEmailInteractorProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 14/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum OnboardingEnterEmailJob {
    case request(String)
}

enum OnboardingEnterEmailResult {
    case requested
    case verified
    case registered
    case failed(ErrorModel)
}

protocol OnboardingEnterEmailInteractorProtocol {
    func `do`(_ job: OnboardingEnterEmailJob)
}

protocol OnboardingEnterEmailInteractorOutputProtocol: class {
    func handle(_ result: OnboardingEnterEmailResult)
}
