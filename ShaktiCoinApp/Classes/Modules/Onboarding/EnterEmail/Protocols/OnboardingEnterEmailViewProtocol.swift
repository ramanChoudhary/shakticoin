//
//  OnboardingEnterEmailViewProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum OnboardingEnterEmailViewState {
    case loading
    case loaded
    case addButton(ButtonModuleModel)
    case addTextField(TextFieldViewModel)
    case error(String)
    case alert(AlertModel)
    case addSpace
    case addLink(TextButtonModel)
    case addHeader(OnboardingHeaderViewModel)
}

protocol OnboardingEnterEmailViewProtocol: class {
    func populate(_ state: OnboardingEnterEmailViewState)
}
