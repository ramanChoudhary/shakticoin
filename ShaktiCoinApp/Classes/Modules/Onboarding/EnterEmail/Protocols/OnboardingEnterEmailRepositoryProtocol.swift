//
//  OnboardingEnterEmailRepositoryProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

protocol OnboardingEnterEmailRepositoryProtocol {
    func request(email: String, callback: @escaping (Result<ResponseModel, ErrorModel>) -> Void)
}
