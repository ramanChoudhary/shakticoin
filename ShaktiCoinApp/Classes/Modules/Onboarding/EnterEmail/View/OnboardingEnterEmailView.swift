//
//  OnboardingEnterEmailView.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

final class OnboardingEnterEmailView: StackView, OnboardingEnterEmailViewProtocol {
    var presenter: OnboardingEnterEmailPresenterProtocol?
    
    private weak var textFieldModule: TextFieldModule?
    
    private var insets = UIEdgeInsets.horizontal(22).vertical(12)
    private var textInsets = UIEdgeInsets.horizontal(22).vertical(6)
    private var space: CGFloat = 6
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.perform(.loaded)
    }
    
    func populate(_ state: OnboardingEnterEmailViewState) {
        switch state {
        case .loaded: hideLoader()
        case .loading: showLoader()
        case .addButton(var viewModel):
            viewModel.callback = { [weak self] in
                guard let textField = self?.textFieldModule?.textField,
                    let text = textField.text else { return }
                textField.resignFirstResponder()
                self?.presenter?.perform(.continue(text))
            }
            
            let module = GoldButtonModule()
            module.populate(model: viewModel)
            module.setNeedsLayout()
            module.layoutIfNeeded()
            addView(module, edgeInsets: textInsets)
        case .addTextField(let viewModel):
            let module = TextFieldModule()
            module.populate(viewModel)
            addView(module, edgeInsets: insets)
            textFieldModule = module
        case .error(let localizedError):
            showAlert(errorMessage: localizedError)
        case .alert(let model):
            showAlert(model)
        case .addSpace:
            let module = UIView()
            module.backgroundColor = .clear
            module.translatesAutoresizingMaskIntoConstraints = false
            module.heightAnchor.constraint(equalToConstant: 1).isActive = true
            addView(module, edgeInsets: textInsets)
        case .addLink(let viewModel):
            let module = TextButtonModule()
            module.populate(viewModel)
            addView(module, edgeInsets: textInsets)
        case .addHeader(let viewModel):
            let module = OnboardingHeaderModule()
            module.populate(viewModel)
            addView(module)
        }
    }
}
