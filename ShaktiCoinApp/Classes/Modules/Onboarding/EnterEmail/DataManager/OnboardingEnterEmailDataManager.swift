//
//  OnboardingEnterEmailDataManager.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class OnboardingEnterEmailDataManager: OnboardingEnterEmailDataManagerProtocol {
    var dataSource = RemoteDataSource.shared
    func request(email: String, callback: @escaping APIDataManagerObjectResultBlock) {
        let params: JSONObject = ["email": email]
        dataSource.requestObject(forEndpoint: Endpoint.EmailVerification.request, parameters: params, result: callback)
    }
}
