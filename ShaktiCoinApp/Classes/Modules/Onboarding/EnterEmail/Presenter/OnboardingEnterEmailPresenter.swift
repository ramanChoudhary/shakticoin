//
//  OnboardingEnterEmailPresenter.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

final class OnboardingEnterEmailPresenter: OnboardingEnterEmailPresenterProtocol, OnboardingEnterEmailInteractorOutputProtocol {
    weak var view: OnboardingEnterEmailViewProtocol?
    var interactor: OnboardingEnterEmailInteractorProtocol?
    var wireframe: OnboardingEnterEmailWireframeProtocol?
    
    private var continueButton: ButtonModuleModel {
        return ButtonModuleModel(title: "onboarding.enter_email.button.continue".localized, enabled: true, callback: nil)
    }
    
    private var emailTextViewModel: TextFieldViewModel {
        let textStyle = TextStyle.lato14.color(ColorStyle.white.color)
        return TextFieldViewModel(textStyle: textStyle, textLocalized: "", placeholderStyle: textStyle, placeholderLocalized: "onboarding.enter_email.text_field_placeholder".localized, keyboardType: .emailAddress, isSecureTextEntry: false)
    }
    
    private var registeredAlert: AlertModel {
        let login = UIAlertAction(title: "onboarding.enter_email.alert.login".localized, style: .default) { _ in
            self.wireframe?.navigate(.login)
        }
        
        return AlertModel(title: "", message: "onboarding.enter_email.alert.message_registered".localized, style: .alert, actions: [login])
    }
    
    private var verifiedAlert: AlertModel {
        let login = UIAlertAction(title: "onboarding.enter_email.alert.login".localized, style: .default) { _ in
            self.wireframe?.navigate(.login)
        }
        
        let `continue` = UIAlertAction(title: "onboarding.enter_email.alert.continue".localized, style: .default) { _ in
            self.wireframe?.navigate(.enterPhoneNumber)
        }
        
        return AlertModel(title: "", message: "onboarding.enter_email.alert.message_verified".localized, style: .alert, actions: [login, `continue`])
    }
    
    private var loginLink: TextButtonModel {
        let title = TextViewModel(style: TextStyle.latoSemiBold14.underline(true).color(.white), value: "onboarding.enter_email.button.login".localized)
        return TextButtonModel(title: title, enabled: true) { [weak self] in
            self?.wireframe?.navigate(.login)
        }
    }
    
    func perform(_ action: OnboardingEnterEmailViewAction) {
        switch action {
        case .loaded:
            let header = OnboardingHeaderViewModel.onboardingHeader(.email)
            view?.populate(.addHeader(header))
            view?.populate(.addSpace)
            view?.populate(.addTextField(emailTextViewModel))
            view?.populate(.addSpace)
            view?.populate(.addButton(continueButton))
            view?.populate(.addLink(loginLink))
        case .continue(let email):
            view?.populate(.loading)
            interactor?.do(.request(email))
        }
    }
    
    func handle(_ result: OnboardingEnterEmailResult) {
        switch result {
        case .requested:
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
                self.wireframe?.navigate(.emailVerificationRequested)
            }
        case .failed(let error):
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
                self.view?.populate(.error(error.localizedDescription))
            }
        case .registered:
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
                self.view?.populate(.alert(self.registeredAlert))
            }
        case .verified:
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
                self.view?.populate(.alert(self.verifiedAlert))
            }
        }
    }
}
