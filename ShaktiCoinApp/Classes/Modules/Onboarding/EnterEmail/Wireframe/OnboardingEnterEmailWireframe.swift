//
//  OnboardingEnterEmailWireframe.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

final class OnboardingEnterEmailWireframe: OnboardingEnterEmailWireframeProtocol {
    class func createModule() -> UIViewController {
        
        let view = OnboardingEnterEmailView()
        let presenter =  OnboardingEnterEmailPresenter()
        let interactor = OnboardingEnterEmailInteractor()
        let repository = OnboardingEnterEmailRepository()
        let dataManager = OnboardingEnterEmailDataManager()
        let wireframe = OnboardingEnterEmailWireframe()
        
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.wireframe = wireframe
        interactor.presenter = presenter
        interactor.repository = repository
        repository.dataManager = dataManager
        
        return view
    }
    
    func navigate(_ route: OnboardingEnterEmailRoute) {
        switch route {
        case .emailVerificationRequested:
            let delegate = UIApplication.shared.delegate as? AppDelegate
            delegate?.window?.rootViewController = OnboardingVerifyEmailWireframe.createModule()
        case .enterPhoneNumber:
            let delegate = UIApplication.shared.delegate as? AppDelegate
            delegate?.window?.rootViewController = OnboardingEnterPhoneWireframe.createModule()
        case .login:
            let delegate = UIApplication.shared.delegate as? AppDelegate
            delegate?.window?.rootViewController = LoginViewController()
        }
    }
}
