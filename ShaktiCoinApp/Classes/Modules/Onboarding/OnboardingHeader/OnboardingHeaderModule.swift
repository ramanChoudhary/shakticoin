//
//  OnboardingHeaderModule.swift
//  ShaktiCoinApp
//
//  Created by sperev on 25/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

struct OnboardingHeaderViewModel {
    var title: TextViewModel
    var subtitle: TextViewModel
    var progress: OnboardingProgressModel
    
    static func onboardingHeader(_ progress: OnboardingProgressModel) -> OnboardingHeaderViewModel {
        let title = TextViewModel(style: TextStyle.latoLight24.alignment(.center), value: "onboarding.title".localized)
        let subtitle = TextViewModel(style: TextStyle.latoBold14.alignment(.center), value: "onboarding.subtitle".localized)
        return OnboardingHeaderViewModel(title: title, subtitle: subtitle, progress: progress)
    }
}

class OnboardingHeaderModule: StackViewModule {
    @IBOutlet weak var titleView: UILabel?
    @IBOutlet weak var subtitleView: UILabel?
    @IBOutlet weak var stackView: UIStackView?
    @IBOutlet weak var progressView: OnboardingProgressModule?
    
    func populate(_ viewModel: OnboardingHeaderViewModel) {
        titleView?.setup(viewModel.title)
        subtitleView?.setup(viewModel.subtitle)
        progressView?.populate(viewModel.progress)
    }
}
