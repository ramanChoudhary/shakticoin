//
//  OnboardingProgressModule.swift
//  ShaktiCoinApp
//
//  Created by sperev on 18/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

enum OnboardingProgress {
    case email
}

struct OnboardingProgressModel {
    var peaces: [OnboardingProgressPeaceModel]
    static var active = TextStyle.lato10
    static var activated = TextStyle.lato10
    static var disabled = TextStyle.lato10.color(UIColor.white.withAlphaComponent(0.6))
    
    static func active(_ text: String) -> OnboardingProgressPeaceModel {
        return OnboardingProgressPeaceModel(state: .active, title: TextViewModel(style: active, value: text))
    }
    
    static func disabled(_ text: String) -> OnboardingProgressPeaceModel {
        return OnboardingProgressPeaceModel(state: .disabled, title: TextViewModel(style: disabled, value: text))
    }
    
    static func activated(_ text: String) -> OnboardingProgressPeaceModel {
        return OnboardingProgressPeaceModel(state: .activated, title: TextViewModel(style: disabled, value: text))
    }
    
    static var email: OnboardingProgressModel {
        let peaces = [
            active("onboarding.progress.enterEmail".localized),
            disabled("onboarding.progress.verifyEmail".localized),
            disabled("onboarding.progress.enterMobile".localized),
            disabled("onboarding.progress.verifyMobile".localized),
            disabled("onboarding.progress.setPassword".localized)
        ]
        return OnboardingProgressModel(peaces: peaces)
    }
    
    static var emailVerification: OnboardingProgressModel {
        let peaces = [
            activated("onboarding.progress.enterEmail".localized),
            active("onboarding.progress.verifyEmail".localized),
            disabled("onboarding.progress.enterMobile".localized),
            disabled("onboarding.progress.verifyMobile".localized),
            disabled("onboarding.progress.setPassword".localized)
        ]
        return OnboardingProgressModel(peaces: peaces)
    }
    
    static var enterMobile: OnboardingProgressModel {
        let peaces = [
            activated("onboarding.progress.enterEmail".localized),
            activated("onboarding.progress.verifyEmail".localized),
            active("onboarding.progress.enterMobile".localized),
            disabled("onboarding.progress.verifyMobile".localized),
            disabled("onboarding.progress.setPassword".localized)
        ]
        
        return OnboardingProgressModel(peaces: peaces)
    }
    
    static var enterMobileCode: OnboardingProgressModel {
        let peaces = [
            activated("onboarding.progress.enterEmail".localized),
            activated("onboarding.progress.verifyEmail".localized),
            activated("onboarding.progress.enterMobile".localized),
            active("onboarding.progress.verifyMobile".localized),
            disabled("onboarding.progress.setPassword".localized)
        ]
        
        return OnboardingProgressModel(peaces: peaces)
    }
    
    static var enterPassword: OnboardingProgressModel {
        let peaces = [
            activated("onboarding.progress.enterEmail".localized),
            activated("onboarding.progress.verifyEmail".localized),
            activated("onboarding.progress.enterMobile".localized),
            activated("onboarding.progress.verifyMobile".localized),
            active("onboarding.progress.setPassword".localized)
        ]
        
        return OnboardingProgressModel(peaces: peaces)
    }
}

final class OnboardingProgressModule: StackViewModule {
    @IBOutlet weak var circle1: UIView?
    @IBOutlet weak var circle2: UIView?
    @IBOutlet weak var circle3: UIView?
    @IBOutlet weak var circle4: UIView?
    
    @IBOutlet weak var peace1: OnboardingProgressPeace?
    @IBOutlet weak var peace2: OnboardingProgressPeace?
    @IBOutlet weak var peace3: OnboardingProgressPeace?
    @IBOutlet weak var peace4: OnboardingProgressPeace?
    @IBOutlet weak var peace5: OnboardingProgressPeace?
    
    override func configureView() {
        super.configureView()
        [circle1, circle2, circle3, circle4].forEach {
            $0?.roundedCorners()
        }
    }
    
    func populate(_ model: OnboardingProgressModel) {
        peace1?.populate(model.peaces[0])
        peace2?.populate(model.peaces[1])
        peace3?.populate(model.peaces[2])
        peace4?.populate(model.peaces[3])
        peace5?.populate(model.peaces[4])
    }
}
