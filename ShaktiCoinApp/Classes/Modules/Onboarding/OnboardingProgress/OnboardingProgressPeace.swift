//
//  OnboardingProgressPeace.swift
//  ShaktiCoinApp
//
//  Created by sperev on 18/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

enum OnboardingProgressPeaceState {
    case active
    case activated
    case disabled
}

struct OnboardingProgressPeaceModel {
    var state: OnboardingProgressPeaceState
    var title: TextViewModel
}

final class OnboardingProgressPeace: StackViewModule {
    @IBOutlet weak var circleView: GradientView?
    @IBOutlet weak var titleLabel: UILabel?
    
    override func configureView() {
        super.configureView()
        circleView?.layer.masksToBounds = false
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        guard let circleView = circleView else { return }
        circleView.layer.cornerRadius = circleView.bounds.height / 2
    }
    
    func populate(_ viewModel: OnboardingProgressPeaceModel) {
        titleLabel?.setup(viewModel.title)
        switch viewModel.state {
        case .active:
            circleView?.setGradientColors([
                UIColor.hex("#D8CBAD").cgColor,
                UIColor.hex("#B09C75").cgColor
            ])
        case .activated:
            circleView?.backgroundColor = UIColor.white
        case .disabled:
            circleView?.backgroundColor = UIColor.white.withAlphaComponent(0.3)
        }
    }
}
