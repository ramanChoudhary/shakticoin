//
//  OnboardingEnterPasswordWireframe.swift
//  ShaktiCoinApp
//
//  Created by sperev on 17/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

final class OnboardingEnterPasswordWireframe: OnboardingEnterPasswordWireframeProtocol {
    class func createModule() -> UIViewController {
        
        let view = OnboardingEnterPasswordView()
        let presenter =  OnboardingEnterPasswordPresenter()
        let interactor = OnboardingEnterPasswordInteractor()
        let repository = OnboardingEnterPasswordRepository()
        let dataManager = OnboardingEnterPasswordDataManager()
        let wireframe = OnboardingEnterPasswordWireframe()
        
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.wireframe = wireframe
        interactor.presenter = presenter
        interactor.repository = repository
        repository.dataManager = dataManager
        
        return view
    }
    
    func navigate(_ route: OnboardingEnterPasswordRoute) {
        switch route {
        case .login:
            let delegate = UIApplication.shared.delegate as? AppDelegate
            delegate?.window?.rootViewController = LoginViewController()
        }
    }
}
