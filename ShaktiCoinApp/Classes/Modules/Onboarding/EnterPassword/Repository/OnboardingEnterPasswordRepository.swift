//
//  OnboardingEnterPasswordRepository.swift
//  ShaktiCoinApp
//
//  Created by sperev on 17/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class OnboardingEnterPasswordRepository: OnboardingEnterPasswordRepositoryProtocol {
    var dataManager: OnboardingEnterPasswordDataManagerProtocol?
    
    func set(password: String, callback: @escaping (ErrorModel?) -> Void) {
        guard let phone = CurrentUserDataManager.shared.phone else {
            callback(ErrorModel(ShaktiError.General.missingPhone))
            return
        }
        
        guard let email = CurrentUserDataManager.shared.email else {
            callback(ErrorModel(ShaktiError.General.missingEmail))
            return
        }
        
        guard let countryCode = CurrentUserDataManager.shared.countryCode else {
            callback(ErrorModel(ShaktiError.General.missingCountryCode))
            return
        }
        
        dataManager?.createUser(email: email, countryCode: countryCode, phone: phone, password: password, callback: {
            switch $0 {
            case .success(let json):
                callback(nil)
            case .failure(let error):
                callback(error)
            }
        })
    }
}
