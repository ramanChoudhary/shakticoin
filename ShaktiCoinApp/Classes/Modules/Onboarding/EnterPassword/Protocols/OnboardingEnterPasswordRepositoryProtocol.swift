//
//  OnboardingEnterPasswordRepositoryProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 17/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

protocol OnboardingEnterPasswordRepositoryProtocol {
    func set(password: String, callback: @escaping (ErrorModel?) -> Void)
}
