//
//  OnboardingEnterPasswordViewProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 17/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum OnboardingEnterPasswordViewState {
    case loading
    case loaded
    case addButton(ButtonModuleModel)
    case addPass1TextField(TextFieldViewModel)
    case addPass2TextField(TextFieldViewModel)
    case error(String)
    case alert(AlertModel)
    case addHeader(OnboardingHeaderViewModel)
    case addText(TextViewModel)
    case addSpace
    case setPasswordStrength(PasswordStrengthViewModel)
}

protocol OnboardingEnterPasswordViewProtocol: class {
    func populate(_ state: OnboardingEnterPasswordViewState)
}
