//
//  OnboardingEnterPasswordDataManagerProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 17/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

protocol OnboardingEnterPasswordDataManagerProtocol {
    func createUser(email: String, countryCode: String,  phone: String, password: String, callback: @escaping APIDataManagerObjectResultBlock)
}
