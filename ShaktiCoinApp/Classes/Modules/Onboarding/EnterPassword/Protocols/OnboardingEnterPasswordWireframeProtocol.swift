//
//  OnboardingEnterPasswordWireframeProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 17/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

enum OnboardingEnterPasswordRoute {
    case login
}

protocol OnboardingEnterPasswordWireframeProtocol {
    func navigate(_ route: OnboardingEnterPasswordRoute)
}
