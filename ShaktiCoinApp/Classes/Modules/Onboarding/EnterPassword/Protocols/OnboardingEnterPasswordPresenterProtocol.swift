//
//  OnboardingEnterPasswordPresenterProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 17/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum OnboardingEnterPasswordViewAction {
    case loaded
    case `continue`(String, String)
    case passwordChanged(String?)
}

protocol OnboardingEnterPasswordPresenterProtocol {
    func perform(_ action: OnboardingEnterPasswordViewAction)
}
