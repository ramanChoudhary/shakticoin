//
//  OnboardingEnterPasswordInteractorProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 17/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum OnboardingEnterPasswordJob {
    case setPassword(String, String)
    case checkPasswordStrength(String?)
}

enum OnboardingEnterPasswordResult {
    case passwordSet
    case failed(ErrorModel)
    case onboardFailed(OnboardApiErrorModel)
    case passwordStrength(Bool, Bool, Bool, Bool)
}

protocol OnboardingEnterPasswordInteractorProtocol {
    func `do`(_ job: OnboardingEnterPasswordJob)
}

protocol OnboardingEnterPasswordInteractorOutputProtocol: class {
    func handle(_ result: OnboardingEnterPasswordResult)
}
