//
//  OnboardingEnterPasswordPresenter.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

final class OnboardingEnterPasswordPresenter: OnboardingEnterPasswordPresenterProtocol, OnboardingEnterPasswordInteractorOutputProtocol {
    weak var view: OnboardingEnterPasswordViewProtocol?
    var interactor: OnboardingEnterPasswordInteractorProtocol?
    var wireframe: OnboardingEnterPasswordWireframeProtocol?
    
    private var continueButton: ButtonModuleModel {
        return ButtonModuleModel(title: "onboarding.enter_password.button.continue".localized, enabled: true, callback: nil)
    }
    
    private var pass1TextViewModel: TextFieldViewModel {
        let textStyle = TextStyle.lato14.color(ColorStyle.white.color)
        return TextFieldViewModel(textStyle: textStyle, textLocalized: "", placeholderStyle: textStyle, placeholderLocalized: "onboarding.enter_code.text_field1_placeholder".localized, keyboardType: .alphabet, isSecureTextEntry: true)
    }
    
    private var pass2TextViewModel: TextFieldViewModel {
        let textStyle = TextStyle.lato14.color(ColorStyle.white.color)
        return TextFieldViewModel(textStyle: textStyle, textLocalized: "", placeholderStyle: textStyle, placeholderLocalized: "onboarding.enter_code.text_field2_placeholder".localized, keyboardType: .alphabet, isSecureTextEntry: true)
    }
    
    private var textViewModel: TextViewModel {
        return TextViewModel(style: TextStyle.latoLight24.alignment(.center), value: "onboarding.enter_password.set_password".localized)
    }
    
    private var hintTextViewModel: TextViewModel {
        return TextViewModel(style: TextStyle.latoSemiBold14.alignment(.center), value: "onboarding.enter_password.time_to_set_password".localized)
    }
    
    func perform(_ action: OnboardingEnterPasswordViewAction) {
        switch action {
        case .loaded:
            let model = OnboardingHeaderViewModel.onboardingHeader(.enterPassword)
            view?.populate(.addHeader(model))
            view?.populate(.addSpace)
            view?.populate(.addText(textViewModel))
            view?.populate(.addText(hintTextViewModel))
            view?.populate(.addSpace)
            view?.populate(.addPass1TextField(pass1TextViewModel))
            view?.populate(.addPass2TextField(pass2TextViewModel))
            view?.populate(.addButton(continueButton))
            view?.populate(.setPasswordStrength(.enterPassword(upperCharacters: false, numbers: false, lowerCharacters: false, symbols: false)))
            
        case .continue(let pass1, let pass2):
            view?.populate(.loading)
            interactor?.do(.setPassword(pass1, pass2))
        case .passwordChanged(let text):
            interactor?.do(.checkPasswordStrength(text))
        }
    }
    
    func handle(_ result: OnboardingEnterPasswordResult) {
        switch result {
        case .passwordSet:
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
                self.wireframe?.navigate(.login)
            }
        case .failed(let error):
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
                self.view?.populate(.error(error.localizedDescription))
            }
        case .onboardFailed(let model):
            let ok = UIAlertAction(title: "OK".localized, style: .default)
            let login = UIAlertAction(title: "Login".localized, style: .cancel) { _ in
                self.wireframe?.navigate(.login)
            }
            let alert = AlertModel(title: model.message, message: model.details, style: .alert, actions: [login, ok])
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
                self.view?.populate(.alert(alert))
            }
        case .passwordStrength(let upp, let num, let low, let sym):
            self.view?.populate(.setPasswordStrength(.enterPassword(upperCharacters: upp, numbers: num, lowerCharacters: low, symbols: sym)))
        }
    }
}
