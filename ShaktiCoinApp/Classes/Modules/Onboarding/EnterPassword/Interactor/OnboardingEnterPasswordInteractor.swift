//
//  OnboardingEnterPasswordInteractor.swift
//  ShaktiCoinApp
//
//  Created by sperev on 17/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class OnboardingEnterPasswordInteractor: OnboardingEnterPasswordInteractorProtocol {
    weak var presenter: OnboardingEnterPasswordInteractorOutputProtocol?
    var repository: OnboardingEnterPasswordRepositoryProtocol?
    
    func `do`(_ job: OnboardingEnterPasswordJob) {
        switch job {
        case .setPassword(let pass1, let pass2):
            let strength = pass1.passwordStrength
            guard pass1 == pass2 else {
                let error = ErrorModel(ShaktiError.General.passwordsNotMatch)
                presenter?.handle(.failed(error))
                return
            }
            
            guard strength.0 && strength.1 && strength.2 && strength.3 else {
                let error = ErrorModel(ShaktiError.General.passwordTooWeak)
                presenter?.handle(.failed(error))
                return
            }
            request(pass1)
            
        case .checkPasswordStrength(let text):
            var upp = false
            var low = false
            var num = false
            var sym = false
            
            if let text = text {
                (upp, num, low, sym) = text.passwordStrength
            }
            
            presenter?.handle(.passwordStrength(upp, num, low, sym))
        }
    }
    
    private func request(_ password: String) {
        repository?.set(password: password, callback: { [weak self] in
            if let error = $0 {
                if case let ShaktiError.API.json(json) = error.value,
                    let model = try? Mapper<OnboardApiErrorModel>.map(object: json) {
                    self?.presenter?.handle(.onboardFailed(model))
                } else {
                    self?.presenter?.handle(.failed(error))
                }
            } else {
                self?.presenter?.handle(.passwordSet)
            }
        })            
    }
}
