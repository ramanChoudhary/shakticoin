//
//  OnboardingEnterPhoneCodeView.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

final class OnboardingEnterPasswordView: StackView, OnboardingEnterPasswordViewProtocol {
    var presenter: OnboardingEnterPasswordPresenterProtocol?
    
    private weak var pass1FieldModule: TextFieldModule?
    private weak var pass2FieldModule: TextFieldModule?
    private weak var passwordStrengthModule: PasswordStrengthModule?
    
    private var insets = UIEdgeInsets.horizontal(22).vertical(12)
    private var spaceInsets = UIEdgeInsets.horizontal(22).vertical(6)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.perform(.loaded)
    }
    
    func populate(_ state: OnboardingEnterPasswordViewState) {
        switch state {
        case .loaded: hideLoader()
        case .loading: showLoader()
        case .addText(let viewModel):
            let module = UILabel()
            module.setup(viewModel)
            addView(module, edgeInsets: spaceInsets)
        case .addSpace:
            let module = UIView()
            module.backgroundColor = .clear
            module.translatesAutoresizingMaskIntoConstraints = false
            module.heightAnchor.constraint(equalToConstant: 1).isActive = true
            addView(module, edgeInsets: spaceInsets)
            
        case .addButton(var viewModel):
            viewModel.callback = { [weak self] in
                guard let textField1 = self?.pass1FieldModule?.textField,
                    let pass1 = textField1.text else { return }
                guard let textField2 = self?.pass2FieldModule?.textField,
                    let pass2 = textField2.text else { return }
                
                textField1.resignFirstResponder()
                textField2.resignFirstResponder()
                
                self?.presenter?.perform(.continue(pass1, pass2))
            }
            
            let module = GoldButtonModule()
            module.populate(model: viewModel)
            addView(module, edgeInsets: insets)
        case .addPass1TextField(let viewModel):
            
            if pass1FieldModule == nil {
                let module = TextFieldModule()
                addView(module, edgeInsets: insets)
                pass1FieldModule = module
            }
            
            pass1FieldModule?.populate(viewModel)
            pass1FieldModule?.textWillChangedCallback = { [weak self] in
                self?.presenter?.perform(.passwordChanged($0))
            }
            
        case .addPass2TextField(let viewModel):
            if pass2FieldModule == nil {
                let module = TextFieldModule()
                addView(module, edgeInsets: insets)
                pass2FieldModule = module
            }
            
            pass2FieldModule?.populate(viewModel)
            
        case .error(let localizedError):
            showAlert(errorMessage: localizedError)
        case .alert(let model):
            showAlert(model)
        case .addHeader(let viewModel):
            let module = OnboardingHeaderModule()
            module.populate(viewModel)
            addView(module)
        case .setPasswordStrength(let viewModel):
            if passwordStrengthModule == nil {
                let module = PasswordStrengthModule()
                addView(module, edgeInsets: spaceInsets)
                passwordStrengthModule = module
            }
            passwordStrengthModule?.populate(viewModel)
        }
    }
}
