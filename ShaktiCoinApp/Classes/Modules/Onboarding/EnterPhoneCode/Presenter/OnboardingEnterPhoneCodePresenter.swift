//
//  OnboardingEnterPhonePresenter.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class OnboardingEnterPhoneCodePresenter: OnboardingEnterPhoneCodePresenterProtocol, OnboardingEnterPhoneCodeInteractorOutputProtocol {
    weak var view: OnboardingEnterPhoneCodeViewProtocol?
    var interactor: OnboardingEnterPhoneCodeInteractorProtocol?
    var wireframe: OnboardingEnterPhoneCodeWireframeProtocol?
    
    private var continueButton: ButtonModuleModel {
        return ButtonModuleModel(title: "onboarding.enter_phone_code.button.continue".localized, enabled: true, callback: nil)
    }
    
    private var codeTextViewModel: TextFieldViewModel {
        let textStyle = TextStyle.lato14.color(ColorStyle.white.color)
        return TextFieldViewModel(textStyle: textStyle, textLocalized: "", placeholderStyle: textStyle, placeholderLocalized: "enter_phone_code.text_field_placeholder".localized, keyboardType: .alphabet, isSecureTextEntry: false)
    }
    
    private var backLink: TextButtonModel {
        let title = TextViewModel(style: TextStyle.latoSemiBold14.underline(true).color(.white), value: "onboarding.enter_phone_code.button.back".localized)
        return TextButtonModel(title: title, enabled: true) { [weak self] in
            self?.wireframe?.navigate(.back)
        }
    }
    
    private var resendLink: TextButtonModel {
        let title = TextViewModel(style: TextStyle.latoSemiBold14.underline(true).color(.white), value: "onboarding.enter_phone_code.button.resend_sms_code".localized)
        return TextButtonModel(title: title, enabled: true) { [weak self] in
            self?.view?.populate(.loading)
            self?.interactor?.do(.resendPhoneCode)
        }
    }
    
    private var enterVerificationCode: TextViewModel {
        return TextViewModel(style: TextStyle.latoLight24.alignment(.center), value: "onboarding.enter_phone_code.enter_verification_code".localized)
    }
    
    func perform(_ action: OnboardingEnterPhoneCodeViewAction) {
        switch action {
        case .loaded:
            let model = OnboardingHeaderViewModel.onboardingHeader(.enterMobileCode)
            view?.populate(.addHeader(model))
            view?.populate(.addSpace)
            view?.populate(.addText(enterVerificationCode))
            view?.populate(.addTextField(codeTextViewModel))
            view?.populate(.addSpace)
            view?.populate(.addButton(continueButton))
            view?.populate(.addSpace)
            view?.populate(.addLink(resendLink))
            view?.populate(.addSpace)
            view?.populate(.addLink(backLink))
        case .continue(let code):
            view?.populate(.loading)
            interactor?.do(.verifyPhone(code))
        }
    }
    
    func handle(_ result: OnboardingEnterPhoneCodeResult) {
        switch result {
        case .verified:
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
                self.wireframe?.navigate(.passwordSetup)
            }
        case .failed(let error):
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
                self.view?.populate(.error(error.localizedDescription))
            }
        case .phoneCodeRequested:
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
            }
        case .phoneExist:
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
            }
        }
    }
}
