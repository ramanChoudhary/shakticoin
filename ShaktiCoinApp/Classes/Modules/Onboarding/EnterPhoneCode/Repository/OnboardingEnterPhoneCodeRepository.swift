//
//  OnboardingEnterPhoneCodeRepository.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class OnboardingEnterPhoneCodeRepository: OnboardingEnterPhoneCodeRepositoryProtocol {
    var dataManager: OnboardingEnterPhoneCodeDataManagerProtocol?
    
    func request(code: String, callback: @escaping (Result<ResponseModel, ErrorModel>) -> Void) {
        guard let phone = CurrentUserDataManager.shared.phone, !phone.isEmpty,
            let countryCode = CurrentUserDataManager.shared.countryCode, !countryCode.isEmpty else {
            callback(.failure(ErrorModel(ShaktiError.General.unknown)))
            return
        }
        
        dataManager?.request(countryCode: countryCode, phone: phone, code: code, callback: {
            switch $0 {
            case .success(let json):
                if let model = try? Mapper<ResponseModel>.map(object: json) {
                    callback(.success(model))
                } else {
                    callback(.failure(ErrorModel(MappingError.unknown)))
                }
            case .failure(let error):
                callback(.failure(error))
            }
        })
    }
    
    func requestPhoneCode(callback: @escaping (Result<ResponseModel, ErrorModel>) -> Void) {
        guard let phone = CurrentUserDataManager.shared.phone, !phone.isEmpty, let countryCode = CurrentUserDataManager.shared.countryCode, !countryCode.isEmpty else {
            callback(.failure(ErrorModel(ShaktiError.General.unknown)))
            return
        }
        
        dataManager?.request(countryCode: countryCode, phone: phone, callback: {
            switch $0 {
            case .success(let json):
                if let model = try? Mapper<ResponseModel>.map(object: json) {
                    callback(.success(model))
                } else {
                    callback(.failure(ErrorModel(MappingError.unknown)))
                }
            case .failure(let error):
                callback(.failure(error))
            }
        })
    }
}
