//
//  OnboardingEnterPhoneCodeInteractorProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 14/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum OnboardingEnterPhoneCodeJob {
    case verifyPhone(String)
    case resendPhoneCode
}

enum OnboardingEnterPhoneCodeResult {
    case verified
    case failed(ErrorModel)
    case phoneCodeRequested
    case phoneExist
}

protocol OnboardingEnterPhoneCodeInteractorProtocol {
    func `do`(_ job: OnboardingEnterPhoneCodeJob)
}

protocol OnboardingEnterPhoneCodeInteractorOutputProtocol: class {
    func handle(_ result: OnboardingEnterPhoneCodeResult)
}
