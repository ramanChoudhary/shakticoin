//
//  OnboardingEnterPhoneCodeDataManagerProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

protocol OnboardingEnterPhoneCodeDataManagerProtocol {
    func request(countryCode: String, phone: String, code: String, callback: @escaping APIDataManagerObjectResultBlock)
    func request(countryCode: String, phone: String, callback: @escaping APIDataManagerObjectResultBlock)
}
