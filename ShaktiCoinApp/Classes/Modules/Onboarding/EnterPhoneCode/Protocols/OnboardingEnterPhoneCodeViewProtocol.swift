//
//  OnboardingEnterPhoneCodeViewProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum OnboardingEnterPhoneCodeViewState {
    case loading
    case loaded
    case addButton(ButtonModuleModel)
    case addTextField(TextFieldViewModel)
    case error(String)
    case addText(TextViewModel)
    case addSpace
    case addLink(TextButtonModel)
    case addHeader(OnboardingHeaderViewModel)
}

protocol OnboardingEnterPhoneCodeViewProtocol: class {
    func populate(_ state: OnboardingEnterPhoneCodeViewState)
}
