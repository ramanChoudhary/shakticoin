//
//  OnboardingEnterPhoneCodePresenterProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum OnboardingEnterPhoneCodeViewAction {
    case loaded
    case `continue`(String)
}

protocol OnboardingEnterPhoneCodePresenterProtocol {
    func perform(_ action: OnboardingEnterPhoneCodeViewAction)
}
