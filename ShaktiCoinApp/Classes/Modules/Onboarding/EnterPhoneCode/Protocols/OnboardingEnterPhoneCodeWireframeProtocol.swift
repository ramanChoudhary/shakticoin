//
//  OnboardingEnterPhoneCodeWireframeProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

enum OnboardingEnterPhoneCodeRoute {
    case passwordSetup
    case back
}

protocol OnboardingEnterPhoneCodeWireframeProtocol {
    func navigate(_ route: OnboardingEnterPhoneCodeRoute)
}
