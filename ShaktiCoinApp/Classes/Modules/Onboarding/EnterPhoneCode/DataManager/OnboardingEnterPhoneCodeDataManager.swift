//
//  OnboardingEnterPhoneCodeDataManager.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class OnboardingEnterPhoneCodeDataManager: OnboardingEnterPhoneCodeDataManagerProtocol {
    var dataSource = RemoteDataSource.shared
    func request(countryCode: String, phone: String, code: String, callback: @escaping APIDataManagerObjectResultBlock) {
        let params: JSONObject = [
            "mobileNo": phone,
            "otp": code,
            "countryCode": countryCode
        ]
        dataSource.requestObject(forEndpoint: Endpoint.PhoneVerification.confirm, parameters: params, result: callback)
    }
    
    func request(countryCode: String, phone: String, callback: @escaping APIDataManagerObjectResultBlock) {
        let params: JSONObject = [
            "mobileNo": phone,
            "countryCode": countryCode
        ]
        dataSource.requestObject(forEndpoint: Endpoint.PhoneVerification.request, parameters: params, result: callback)
    }
}
