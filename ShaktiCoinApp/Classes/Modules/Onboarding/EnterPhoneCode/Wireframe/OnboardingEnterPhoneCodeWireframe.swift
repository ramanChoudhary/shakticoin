//
//  OnboardingEnterPhoneCodeWireframe.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

final class OnboardingEnterPhoneCodeWireframe: OnboardingEnterPhoneCodeWireframeProtocol {
    class func createModule() -> UIViewController {
        
        let view = OnboardingEnterPhoneCodeView()
        let presenter =  OnboardingEnterPhoneCodePresenter()
        let interactor = OnboardingEnterPhoneCodeInteractor()
        let repository = OnboardingEnterPhoneCodeRepository()
        let dataManager = OnboardingEnterPhoneCodeDataManager()
        let wireframe = OnboardingEnterPhoneCodeWireframe()
        
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.wireframe = wireframe
        interactor.presenter = presenter
        interactor.repository = repository
        repository.dataManager = dataManager
        
        return view
    }
    
    func navigate(_ route: OnboardingEnterPhoneCodeRoute) {
        switch route {
        case .passwordSetup:
            let delegate = UIApplication.shared.delegate as? AppDelegate
            delegate?.window?.rootViewController = OnboardingEnterPasswordWireframe.createModule()
        case .back:
            let delegate = UIApplication.shared.delegate as? AppDelegate
            delegate?.window?.rootViewController = OnboardingEnterPhoneWireframe.createModule()
        }
    }
}
