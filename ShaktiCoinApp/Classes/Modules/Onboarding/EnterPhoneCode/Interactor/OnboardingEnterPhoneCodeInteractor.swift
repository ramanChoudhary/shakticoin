//
//  OnboardingEnterPhoneCodeInteractor.swift
//  ShaktiCoinApp
//
//  Created by sperev on 14/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class OnboardingEnterPhoneCodeInteractor: OnboardingEnterPhoneCodeInteractorProtocol {
    weak var presenter: OnboardingEnterPhoneCodeInteractorOutputProtocol?
    var repository: OnboardingEnterPhoneCodeRepositoryProtocol?
    
    func `do`(_ job: OnboardingEnterPhoneCodeJob) {
        switch job {
        case .verifyPhone(let code):
            request(code: code)
        case .resendPhoneCode:
            requestPhoneCode()
        }
    }
    
    private func request(code: String) {
        repository?.request(code: code, callback: { [weak self] in
            switch $0 {
            case .success:
                self?.presenter?.handle(.verified)
            case .failure(let error):
                if case let ShaktiError.API.response(model) = error.value, model.responseCode == 409 {
                    self?.presenter?.handle(.verified)
                } else {
                    self?.presenter?.handle(.failed(error))
                }
            }
        })            
    }
    
    private func requestPhoneCode() {
        repository?.requestPhoneCode(callback: { [weak self] in
            switch $0 {
            case .success(let model):
                if model.responseCode == 200 {
                    self?.presenter?.handle(.phoneCodeRequested)
                } else {
                    let error = ErrorModel(ShaktiError.General.unknown)
                    self?.presenter?.handle(.failed(error))
                }
            case .failure(let error):
                if case let ShaktiError.API.response(model) = error.value, model.responseCode == 409 {
                    self?.presenter?.handle(.phoneExist)
                }
                
                self?.presenter?.handle(.failed(error))
            }
        })
    }
}
