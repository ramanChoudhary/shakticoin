//
//  OnboardingVerifyEmailViewProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum OnboardingVerifyEmailViewState {
    case loading
    case loaded
    case error(String)
    case alert(AlertModel)
    case addSpace
    case addLink(TextButtonModel)
    case addBottomTextButton(TextButtonModel)
    case addHeader(OnboardingHeaderViewModel)
    case addText(TextViewModel)
}

protocol OnboardingVerifyEmailViewProtocol: class {
    func populate(_ state: OnboardingVerifyEmailViewState)
}
