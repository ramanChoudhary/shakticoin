//
//  OnboardingVerifyEmailPresenterProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum OnboardingVerifyEmailViewAction {
    case loaded
    case confirmEmail(String)
}

protocol OnboardingVerifyEmailPresenterProtocol {
    func perform(_ action: OnboardingVerifyEmailViewAction)
}
