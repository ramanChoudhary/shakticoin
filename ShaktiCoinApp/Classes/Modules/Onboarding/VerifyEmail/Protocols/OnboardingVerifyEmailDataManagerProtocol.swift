//
//  OnboardingVerifyEmailDataManagerProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

protocol OnboardingVerifyEmailDataManagerProtocol {
    func requestVerificationStatus(email: String, callback: @escaping APIDataManagerObjectResultBlock)
    func sendConfirmation(token: String, callback: @escaping APIDataManagerObjectResultBlock)
}
