//
//  OnboardingVerifyEmailRepositoryProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

protocol OnboardingVerifyEmailRepositoryProtocol {
    func requestVerificationEmail(callback: @escaping (Result<ResponseModel, ErrorModel>) -> Void)
    func checkEmailVerification(callback: @escaping (Result<ResponseModel, ErrorModel>) -> Void)
    func sendEmailVerification(_ token: String, callback: @escaping (Result<ResponseModel, ErrorModel>) -> Void)
}
