//
//  OnboardingVerifyEmailInteractorProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 14/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

enum OnboardingVerifyEmailJob {
    case startCheckingVerification
    case requestVerificationEmail
    case sendConfirmation(String)
}

enum OnboardingVerifyEmailResult {
    case verified
    case expired
    case notFound
    case requested
    case startedVerification
    case failed(ErrorModel)
}

protocol OnboardingVerifyEmailInteractorProtocol {
    func `do`(_ job: OnboardingVerifyEmailJob)
}

protocol OnboardingVerifyEmailInteractorOutputProtocol: class {
    func handle(_ result: OnboardingVerifyEmailResult)
}
