//
//  OnboardingVerifyEmailWireframeProtocol.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

enum OnboardingVerifyEmailRoute {
    case enterPhoneNumber
    case enterEmail
}

protocol OnboardingVerifyEmailWireframeProtocol {
    func navigate(_ route: OnboardingVerifyEmailRoute)
}
