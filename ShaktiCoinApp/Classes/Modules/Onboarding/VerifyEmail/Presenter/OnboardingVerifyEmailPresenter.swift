//
//  OnboardingVerifyEmailPresenter.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

final class OnboardingVerifyEmailPresenter: OnboardingVerifyEmailPresenterProtocol, OnboardingVerifyEmailInteractorOutputProtocol {
    weak var view: OnboardingVerifyEmailViewProtocol?
    var interactor: OnboardingVerifyEmailInteractorProtocol?
    var wireframe: OnboardingVerifyEmailWireframeProtocol?
    
    private var continueButton: ButtonModuleModel {
        return ButtonModuleModel(title: "onboarding.enter_email.button.continue".localized, enabled: true, callback: nil)
    }
    
    private var emailTextViewModel: TextFieldViewModel {
        let textStyle = TextStyle.lato14.color(ColorStyle.white.color)
        return TextFieldViewModel(textStyle: textStyle, textLocalized: "", placeholderStyle: textStyle, placeholderLocalized: "onboarding.enter_email.text_field_placeholder".localized, keyboardType: .emailAddress, isSecureTextEntry: false)
    }
    
    private var changeEmailLink: TextButtonModel {
        let title = TextViewModel(style: TextStyle.latoSemiBold14.underline(true).color(.white), value: "onboarding.verify_email.button.back".localized)
        return TextButtonModel(title: title, enabled: true) { [weak self] in
            self?.wireframe?.navigate(.enterEmail)
        }
    }
    
    private var resendEmailLink: TextButtonModel {
        let title = TextViewModel(style: TextStyle.latoSemiBold14.underline(true).color(.white), value: "onboarding.verify_email.button.resend_email".localized)
        return TextButtonModel(title: title, enabled: true) { [weak self] in
            self?.view?.populate(.loading)
            self?.interactor?.do(.requestVerificationEmail)
        }
    }
    
    private var textViewModel: TextViewModel {
        return TextViewModel(style: TextStyle.latoLight24.alignment(.center), value: "onboarding.verify_email.thank_you".localized)
    }
    
    private var hintTextViewModel: TextViewModel {
        return TextViewModel(style: TextStyle.latoSemiBold14.alignment(.center), value: "onboarding.verify_email.email_sent".localized)
    }
    
    private var expiredAlert: AlertModel {
        let resend = UIAlertAction(title: "onboarding.verify_email.expired_alert.resend".localized, style: .default) { _ in
            self.view?.populate(.loading)
            self.interactor?.do(.requestVerificationEmail)
        }
        
        let cancel = UIAlertAction(title: "onboarding.verify_email.expired_alert.cancel".localized, style: .default) { _ in }
        
        return AlertModel(title: "", message: "onboarding.verify_email.expired_alert.message".localized, style: .alert, actions: [resend, cancel])
    }
    
    private var requestedAlert: AlertModel {
        let ok = UIAlertAction(title: "onboarding.verify_email.requested_alert.ok".localized, style: .default) { _ in }
        
        return AlertModel(title: "", message: "onboarding.verify_email.requested_alert.message".localized, style: .alert, actions: [ok])
    }
    
    func perform(_ action: OnboardingVerifyEmailViewAction) {
        switch action {
        case .loaded:
            let header = OnboardingHeaderViewModel.onboardingHeader(.emailVerification)
            view?.populate(.addHeader(header))
            view?.populate(.addSpace)
            view?.populate(.addText(textViewModel))
            view?.populate(.addText(hintTextViewModel))
            view?.populate(.addSpace)
            view?.populate(.addLink(resendEmailLink))
            view?.populate(.addSpace)
            view?.populate(.addBottomTextButton(changeEmailLink))
            interactor?.do(.startCheckingVerification)
        case .confirmEmail(let token):
            view?.populate(.loading)
            interactor?.do(.sendConfirmation(token))
        }
    }
    
    func handle(_ result: OnboardingVerifyEmailResult) {
        switch result {
        case .verified:
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
                self.wireframe?.navigate(.enterPhoneNumber)
            }
        case .requested:
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
                self.view?.populate(.alert(self.requestedAlert))
            }
        case .failed(let error):
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
                self.view?.populate(.error(error.localizedDescription))
            }
        case .expired:
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
                self.view?.populate(.alert(self.expiredAlert))
            }
        case .notFound:
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
            }
        case .startedVerification:
            ViewDispatcher.shared.execute {
                self.view?.populate(.loading)
            }
        }
    }
}
