//
//  OnboardingVerifyEmailView.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

final class OnboardingVerifyEmailView: StackView, OnboardingVerifyEmailViewProtocol {
    var presenter: OnboardingVerifyEmailPresenterProtocol?
    
    private var insets = UIEdgeInsets.horizontal(22).vertical(12)
    private var space: CGFloat = 6
    private var spaceInsets = UIEdgeInsets.horizontal(22).vertical(6)
    private var loaded = false
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !loaded {
            loaded = true
            presenter?.perform(.loaded)
        }
    }
    
    func populate(_ state: OnboardingVerifyEmailViewState) {
        switch state {
        case .loaded: hideLoader()
        case .loading: showLoader()
        case .error(let localizedError):
            showAlert(errorMessage: localizedError)
        case .alert(let model):
            showAlert(model)
        case .addSpace:
            let module = UIView()
            module.backgroundColor = .clear
            module.translatesAutoresizingMaskIntoConstraints = false
            module.heightAnchor.constraint(equalToConstant: 1).isActive = true
            addView(module, edgeInsets: spaceInsets)
        case .addLink(let viewModel):
            let module = TextButtonModule()
            module.populate(viewModel)
            addView(module, edgeInsets: spaceInsets)
        case .addHeader(let viewModel):
            let module = OnboardingHeaderModule()
            module.populate(viewModel)
            addView(module)
        case .addText(let viewModel):
            let module = UILabel()
            module.setup(viewModel)
            addView(module, edgeInsets: spaceInsets)
        case .addBottomTextButton(let viewModel):
            let module = TextButtonModule()
            module.populate(viewModel)
            addBottomView(module, edgeInsets: .bottom(35))
        }
    }
    
    func sendEmailVerification(_ token: String) {
        presenter?.perform(.confirmEmail(token))
    }
}
