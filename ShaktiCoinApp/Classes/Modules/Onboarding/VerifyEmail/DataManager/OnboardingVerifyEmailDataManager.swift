//
//  OnboardingVerifyEmailDataManager.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class OnboardingVerifyEmailDataManager: OnboardingVerifyEmailDataManagerProtocol {
    var dataSource = RemoteDataSource.shared
    func requestVerificationStatus(email: String, callback: @escaping APIDataManagerObjectResultBlock) {
        let params: JSONObject = ["email": email]
        dataSource.requestObject(forEndpoint: Endpoint.EmailVerification.status, parameters: params, result: callback)
    }
    
    func sendConfirmation(token: String, callback: @escaping APIDataManagerObjectResultBlock) {
        let params: JSONObject = [
            "token": token
        ]
        dataSource.requestObject(forEndpoint: Endpoint.EmailVerification.confirm, parameters: params, result: callback)
    }
}
