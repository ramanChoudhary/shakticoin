//
//  OnboardingVerifyEmailInteractor.swift
//  ShaktiCoinApp
//
//  Created by sperev on 14/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class OnboardingVerifyEmailInteractor: OnboardingVerifyEmailInteractorProtocol {
    weak var presenter: OnboardingVerifyEmailInteractorOutputProtocol?
    var repository: OnboardingVerifyEmailRepositoryProtocol?
    private var token: String?
    
    init(token: String? = nil) {
        self.token = token
    }
    
    func `do`(_ job: OnboardingVerifyEmailJob) {
        switch job {
        case .requestVerificationEmail:
            requestVerificationEmail()
        case .sendConfirmation(let token):
            self.token = token
            sendVerification(token)
        case .startCheckingVerification:
            if let token = token {
                sendVerification(token)
            }
            
            let time = DispatchTime.now() + 10 + Double(arc4random_uniform(10))
            DispatchQueue.main.asyncAfter(deadline: time) { [weak self] in
                self?.checkVerification()
            }
        }
    }
    
    private func sendVerification(_ token: String) {
        presenter?.handle(.startedVerification)
        repository?.sendEmailVerification(token, callback: { [weak self] in
            switch $0 {
            case .success:
                self?.presenter?.handle(.verified)
            case .failure(let error):
                if case let ShaktiError.API.response(model) = error.value, model.responseCode == 409 {
                    self?.presenter?.handle(.verified)
                } else if case let ShaktiError.API.response(model) = error.value, model.responseCode == 410 {
                    self?.token = nil
                    self?.presenter?.handle(.expired)
                } else {
                    self?.token = nil
                    self?.presenter?.handle(.failed(error))
                    self?.do(.startCheckingVerification)
                }
            }
        })
    }
    
    private func checkVerification() {
        guard token == nil else { return }
        
        repository?.checkEmailVerification { [weak self] in
            switch $0 {
            case .success(let model):
                print(model)
                self?.presenter?.handle(.verified)
            case .failure(let error):
                if case let ShaktiError.API.response(model) = error.value, model.responseCode == 410 {
                    self?.presenter?.handle(.expired)
                } else if case let ShaktiError.API.response(model) = error.value, model.responseCode == 404 {
                    self?.presenter?.handle(.notFound)
                    self?.do(.startCheckingVerification)
                } else {
                    self?.presenter?.handle(.failed(error))
                    self?.do(.startCheckingVerification)
                }
            }
        }
    }
    
    private func requestVerificationEmail() {
        repository?.requestVerificationEmail(callback: { [weak self] in
            switch $0 {
            case .success(let model):
                if model.responseCode == 200 {
                    self?.presenter?.handle(.requested)
                } else {
                    let error = ErrorModel(ShaktiError.General.unknown)
                    self?.presenter?.handle(.failed(error))
                }
            case .failure(let error):
                self?.presenter?.handle(.failed(error))
            }
        })
    }
}
