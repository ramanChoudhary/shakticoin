//
//  OnboardingVerifyEmailWireframe.swift
//  ShaktiCoinApp
//
//  Created by sperev on 11/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

final class OnboardingVerifyEmailWireframe: OnboardingVerifyEmailWireframeProtocol {
    class func createModule(_ token: String? = nil) -> UIViewController {
        let view = OnboardingVerifyEmailView()
        let presenter =  OnboardingVerifyEmailPresenter()
        let interactor = OnboardingVerifyEmailInteractor(token: token)
        let repository = OnboardingVerifyEmailRepository()
        let dataManager = OnboardingVerifyEmailDataManager()
        let enterEmailDataManager = OnboardingEnterEmailDataManager()
        let wireframe = OnboardingVerifyEmailWireframe()
        
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        presenter.wireframe = wireframe
        interactor.presenter = presenter
        interactor.repository = repository
        repository.dataManager = dataManager
        repository.enterEmailDataManager = enterEmailDataManager
        
        return view
    }
    
    func navigate(_ route: OnboardingVerifyEmailRoute) {
        switch route {
        case .enterPhoneNumber:
            let delegate = UIApplication.shared.delegate as? AppDelegate
            delegate?.window?.rootViewController = OnboardingEnterPhoneWireframe.createModule()
        case .enterEmail:
            let delegate = UIApplication.shared.delegate as? AppDelegate
            delegate?.window?.rootViewController = OnboardingEnterEmailWireframe.createModule()
        }
    }
}
