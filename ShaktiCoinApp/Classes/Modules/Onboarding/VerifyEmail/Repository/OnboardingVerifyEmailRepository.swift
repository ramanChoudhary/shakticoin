//
//  OnboardingVerifyEmailRepository.swift
//  ShaktiCoinApp
//
//  Created by sperev on 15/08/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import Foundation

final class OnboardingVerifyEmailRepository: OnboardingVerifyEmailRepositoryProtocol {
    var dataManager: OnboardingVerifyEmailDataManagerProtocol?
    var enterEmailDataManager: OnboardingEnterEmailDataManagerProtocol?
    
    func requestVerificationEmail(callback: @escaping (Result<ResponseModel, ErrorModel>) -> Void) {
        guard let email = CurrentUserDataManager.shared.email else {
            callback(.failure(ErrorModel(ShaktiError.General.unknown)))
            return
        }
        enterEmailDataManager?.request(email: email) {
            switch $0 {
            case .success(let json):
                if let model = try? Mapper<ResponseModel>.map(object: json) {
                    callback(.success(model))
                } else {
                    callback(.failure(ErrorModel(MappingError.unknown)))
                }
            case .failure(let error):
                callback(.failure(error))
            }
        }
    }
    
    func checkEmailVerification(callback: @escaping (Result<ResponseModel, ErrorModel>) -> Void) {
        guard let email = CurrentUserDataManager.shared.email else {
            callback(.failure(ErrorModel(ShaktiError.General.unknown)))
            return
        }
        
        dataManager?.requestVerificationStatus(email: email) {
            switch $0 {
            case .success(let json):
                if let model = try? Mapper<ResponseModel>.map(object: json) {
                    callback(.success(model))
                } else {
                    callback(.failure(ErrorModel(MappingError.unknown)))
                }
            case .failure(let error):
                callback(.failure(error))
            }
        }
    }
    
    func sendEmailVerification(_ token: String, callback: @escaping (Result<ResponseModel, ErrorModel>) -> Void) {
        dataManager?.sendConfirmation(token: token) {
            switch $0 {
            case .success(let json):
                if let model = try? Mapper<ResponseModel>.map(object: json) {
                    callback(.success(model))
                } else {
                    callback(.failure(ErrorModel(MappingError.unknown)))
                }
            case .failure(let error):
                callback(.failure(error))
            }
        }
    }
}
