//
//  NotificationsView.swift
//  ShaktiCoinApp
//
//  Created by sperev on 20/10/2020.
//  Copyright © 2020 ShaktiCoinApp. All rights reserved.
//

import UIKit

protocol NotificationsApplicable: class {
    var notificationsView: NotificationsView { get }
}

class NotificationsView: StackViewModule, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    weak var viewController: UIViewController?
    var isOpened = false
    private let cellId = "notificationCellId"
    
    let notificationItems: [NotificationItem] = {
        let first = NotificationItem(title: "_FirstTitle".localized, message: "_FirstMessage".localized, date: "06/10/2018", isRead: nil)
        let second = NotificationItem(title: "_SecondTitle".localized, message: "_SecondMessage".localized, date: "05/10/2018", isRead: nil)
        let third = NotificationItem(title: "_ThirdTitle".localized, message: "_ThirdMessage".localized, date: "05/10/2018", isRead: nil)
        return [first, second, third]
    }()
    
    var constraint: NSLayoutConstraint?
    
    @IBOutlet weak var collectionView: UICollectionView? {
        didSet {
            collectionView?.backgroundColor = UIColor().hexStringToUIColor("#0C0C0C")
            collectionView?.layer.borderWidth = 0.5
            collectionView?.layer.borderColor = UIColor.darkGray.cgColor
        }
    }
    
    override func configureView() {
        super.configureView()
        collectionView?.register(NotificationViewCell.self, forCellWithReuseIdentifier: cellId)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notificationItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! NotificationViewCell
        cell.ntfItem = notificationItems[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 221, height: 72)
    }
}

extension UIViewController {
    func applyNotifications() {
        guard let self = self as? NotificationsApplicable else { return }
        let button = UIBarButtonItem(image: UIImage(named: "Notification"), style: .plain, target: self, action: #selector(onNotificationsClick))
        button.tintColor = .white
        navigationItem.rightBarButtonItem = button
        
        view.addSubview(self.notificationsView)
        view.bringSubviewToFront(self.notificationsView)
        
        self.notificationsView.constraint = self.notificationsView.leftAnchor.constraint(equalTo: view.rightAnchor)
        self.notificationsView.constraint?.isActive = true
        
        self.notificationsView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        self.notificationsView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    @objc private func onNotificationsClick() {
        guard let self = self as? NotificationsApplicable else { return }
        self.notificationsView.isOpened = !self.notificationsView.isOpened
        let width = self.notificationsView.isOpened ? -self.notificationsView.bounds.width : 0
        self.notificationsView.constraint?.constant = width
        UIView.animate(withDuration: CATransaction.animationDuration(), animations: {
            self.notificationsView.superview?.layoutIfNeeded()
        })
    }
}
