//___FILEHEADER___

import Foundation
import UIKit

// ++++++++++++++++++++++++++++++++++++++++++++++
// +++++++++++++++ Implementation +++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++

final class ___FILEBASENAME___: BaseViewController, ___FILEBASENAME___Protocol {
    
    var presenter: ___VARIABLE_productName:identifier___PresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter?.perform(.load)
    }
    
    func populate(_ state: ___VARIABLE_productName:identifier___State) {
        hideLoader()
        switch state {
        case .loaded: break
            //Populate view for this state
        case .error(let text):
            show(error: text)
        case .showLoader:
            showLoader()
        }
    }
}
