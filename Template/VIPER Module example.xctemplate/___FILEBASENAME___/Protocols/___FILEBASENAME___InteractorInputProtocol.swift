//___FILEHEADER___

enum ___VARIABLE_productName:identifier___Job {
    case requestExample
}

protocol ___FILEBASENAME___: class {
    var presenter: ___VARIABLE_productName:identifier___InteractorOutputProtocol? { get set }
    var repository: ___VARIABLE_productName:identifier___RepositoryProtocol? { get set }
    
    /**
     * Add here your methods for communication PRESENTER -> INTERACTOR
     */
    func `do`(_ job: ___VARIABLE_productName:identifier___Job)
}
