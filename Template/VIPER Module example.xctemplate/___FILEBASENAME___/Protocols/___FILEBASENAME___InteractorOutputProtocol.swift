//___FILEHEADER___

enum ___VARIABLE_productName:identifier___InteractorResult {
    case requestExampleSucceed
    case requestExampleFailed(error: String)
}

protocol ___FILEBASENAME___: class {
    /**
     * Add here your methods for communication INTERACTOR -> PRESENTER
     */
    func handle(_ result: ___VARIABLE_productName:identifier___InteractorResult)
}
