//___FILEHEADER___

enum ___VARIABLE_productName:identifier___State {
    case loaded
    case error(text: String)
    case showLoader
}

protocol ___FILEBASENAME___: class {
    
    var presenter: ___VARIABLE_productName:identifier___PresenterProtocol? { get set }
    /**
     * Add here your methods for communication PRESENTER -> VIEW
     */
    func populate(_ state: ___VARIABLE_productName:identifier___State)
}
