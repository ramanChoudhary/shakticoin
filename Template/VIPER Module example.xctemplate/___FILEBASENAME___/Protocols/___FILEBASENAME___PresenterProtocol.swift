//___FILEHEADER___

enum ___VARIABLE_productName:identifier___ViewAction {
    case load
}

protocol ___FILEBASENAME___: class {
    var view: ___VARIABLE_productName:identifier___ViewProtocol? { get set }
    var interactor: ___VARIABLE_productName:identifier___InteractorInputProtocol? { get set }
    var wireframe: ___VARIABLE_productName:identifier___WireframeProtocol? { get set }
    /**
     * Add here your methods for communication VIEW -> PRESENTER
     */
    func perform(_ action: ___VARIABLE_productName:identifier___ViewAction)
}
