//___FILEHEADER___

typealias APIDataManagerExampleResultBlock = (Result<ExampleEntity, ErrorModel>) -> Void

protocol ___FILEBASENAME___: class {
    /**
     * Add here your methods for communication INTERACTOR -> REPOSITORY
     */
    
    /*** Remove this code ***/
    func requestExample(resultBlock: @escaping APIDataManagerExampleResultBlock)
    /************************/
}
