//___FILEHEADER___

enum ___VARIABLE_productName:identifier___Page {
    case close
    case nextPage
}

protocol ___FILEBASENAME___: BaseWireFrameProtocol {
    
    var view: UIViewController? { get set }
    
    static func createModule() -> UIViewController
    /**
     * Add here your methods for communication PRESENTER -> WIREFRAME
     */
    func navigate(to page: ___VARIABLE_productName:identifier___Page)
}
