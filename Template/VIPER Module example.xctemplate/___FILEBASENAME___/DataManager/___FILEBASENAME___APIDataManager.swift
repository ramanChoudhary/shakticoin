//___FILEHEADER___

import Foundation
import Result

final class ___FILEBASENAME___: ___FILEBASENAME___InputProtocol {
    
    init() {
    }
    
    // MARK: ___FILEBASENAME___InputProtocol implementation
    
    /*** Remove this code ***/
    func exampleCall(resultBlock: @escaping APIDataManagerObjectResultBlock) {
        // APIDataManager make API request
    }
    /************************/
}
