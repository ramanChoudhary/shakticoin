//___FILEHEADER___

import Foundation

final class ___FILEBASENAME___: BaseWireFrame, ___FILEBASENAME___Protocol {
    
    weak var view: UIViewController?
    
    static func createModule() -> UIViewController {
        // Generating module components
        let view = ___VARIABLE_productName:identifier___View(nibName: "___VARIABLE_productName:identifier___View", bundle: nil)
        let presenter: ___VARIABLE_productName:identifier___PresenterProtocol & ___VARIABLE_productName:identifier___InteractorOutputProtocol = ___VARIABLE_productName:identifier___Presenter()
        let interactor: ___VARIABLE_productName:identifier___InteractorInputProtocol = ___VARIABLE_productName:identifier___Interactor()
        let APIDataManager: ___VARIABLE_productName:identifier___APIDataManagerInputProtocol = ___VARIABLE_productName:identifier___APIDataManager()
        let localDataManager: ___VARIABLE_productName:identifier___LocalDataManagerInputProtocol = ___VARIABLE_productName:identifier___LocalDataManager()
        let wireframe: ___VARIABLE_productName:identifier___WireframeProtocol = ___VARIABLE_productName:identifier___Wireframe()
        let repository: ___VARIABLE_productName:identifier___RepositoryProtocol = ___VARIABLE_productName:identifier___Repository(APIDataManager, localDataManager)
        
        // Connecting
        view.presenter = presenter
        presenter.view = view
        presenter.wireframe = wireframe
        presenter.interactor = interactor
        interactor.presenter = presenter
        interactor.repository = repository
        
        wireframe.view = view
        
        return view
    }
    
    func navigate(to page: ___VARIABLE_productName:identifier___Page) {
        switch page {
        case .close:
            close()
        case .nextPage:
            navigateToNextPage()
        }
    }
    
    private func close() {
        dismiss(viewController: view?.navigationController)
    }
    
    private func navigateToNextPage() {
//        let nextPage = NextPageWireframe.createModule()
//        ViewDispatcher.shared.execute {
//            self.view?.navigationController?.pushViewController(nextPage, animated: true)
//        }
    }
}
