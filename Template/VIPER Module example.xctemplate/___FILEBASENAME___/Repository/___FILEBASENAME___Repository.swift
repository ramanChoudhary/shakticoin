//___FILEHEADER___

import Foundation
import Result

final class ___FILEBASENAME___: ___FILEBASENAME___Protocol {
    private var apiDataManager: ___VARIABLE_productName:identifier___APIDataManagerInputProtocol?
    private var localDataManager: ___VARIABLE_productName:identifier___LocalDataManagerInputProtocol?
    
    init(_ apiDataManager: ___VARIABLE_productName:identifier___APIDataManagerInputProtocol?, _ localDataManager: ___VARIABLE_productName:identifier___LocalDataManagerInputProtocol?) {
        self.apiDataManager = apiDataManager
        self.localDataManager = localDataManager
    }
    
    // MARK: ___FILEBASENAME___Protocol implementation
    
    /*** Remove this code ***/
    func requestExample(resultBlock: @escaping APIDataManagerExampleResultBlock) {
        apiDataManager?.exampleCall(resultBlock: { result in
            switch result {
            case .success(let jsonObject):
                // Map json object
                guard let model = try? Mapper<ExampleEntity>.map(object: jsonObject) else {
                    resultBlock(.failure(ErrorModel(APIError.General.invalidParams)))
                    return
                }
                resultBlock(.success(model))
            case .failure(let error):
                resultBlock(.failure(error))
            }
        })
    }
    /************************/
}
