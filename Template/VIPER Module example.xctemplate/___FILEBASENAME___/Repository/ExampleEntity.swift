//___FILEHEADER___

import Foundation

final class ExampleEntity: MapRepresentable {
    var id: Int
    var title: String
    
    init(map: Map) throws {
        id = try map.get("id")
        title = (try? map.get("title")) ?? ""
    }
    
    init(with id: Int, and title: String) {
        self.id = id
        self.title = title
    }
}
