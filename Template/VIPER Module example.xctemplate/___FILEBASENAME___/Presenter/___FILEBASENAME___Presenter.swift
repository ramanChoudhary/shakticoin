//___FILEHEADER___

import Foundation

final class ___FILEBASENAME___: ___FILEBASENAME___Protocol, ___VARIABLE_productName:identifier___InteractorOutputProtocol {
    
    weak var view: ___VARIABLE_productName:identifier___ViewProtocol?
    var interactor: ___VARIABLE_productName:identifier___InteractorInputProtocol?
    var wireframe: ___VARIABLE_productName:identifier___WireframeProtocol?
    
    // MARK: ___FILEBASENAME___Protocol implementation
    
    func perform(_ action: ___VARIABLE_productName:identifier___ViewAction) {
        switch action {
        case .load:
            /*** Remove this code ***/
            view?.populate(.showLoader)
            interactor?.do(___VARIABLE_productName:identifier___Job.requestExample)
            /************************/
        }
    }
    
    func handle(_ result: ___VARIABLE_productName:identifier___InteractorResult) {
        switch result {
        case .requestExampleSucceed:
            // Update view
            ViewDispatcher.shared.execute {
                self.view?.populate(.loaded)
            }
        case .requestExampleFailed(let error): 
            // Show error
            view?.populate(.error(text: error))
        }
    }
}
