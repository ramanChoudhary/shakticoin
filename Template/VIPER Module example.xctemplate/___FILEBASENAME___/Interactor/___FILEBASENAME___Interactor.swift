//___FILEHEADER___

import Foundation

final class ___FILEBASENAME___: ___FILEBASENAME___InputProtocol {
    
    weak var presenter: ___FILEBASENAME___OutputProtocol?
    var repository: ___VARIABLE_productName:identifier___RepositoryProtocol?
    
    init() {}

    // MARK: ___FILEBASENAME___InputProtocol implementation
    
    func `do`(_ job: ___VARIABLE_productName:identifier___Job) {
        switch job {
        case .requestExample:
            requestExample()
        }
    }
    
    /*** Remove this code ***/
    private func requestExample() {
        repository?.requestExample(resultBlock: { [weak self] result in
            switch result {
            case .success(let entity):
                self?.presenter?.handle(___FILEBASENAME___Result.requestExampleSucceed)
            case .failure(let error):
                self?.presenter?.handle(___FILEBASENAME___Result.requestExampleFailed(error: error.value.string))
            }
        })
    }
    /************************/
}
